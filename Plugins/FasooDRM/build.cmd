@echo off

setlocal EnableDelayedExpansion
set PACKAGE_VERSION=1.1.%1
echo PACKAGE_VERSION is !PACKAGE_VERSION!

if "%CONAN_USERNAME%" == "" (
    echo ## please input your username ##
    set /p CONAN_USERNAME=
)

if "%CONAN_PASSWD%" == "" (
    echo ## please input your password ##
    set /p CONAN_PASSWD=
)

REM register remote repo
REM conan remote add foxit-conan-center https://conan.foxitsoftware.com:8083/artifactory/api/conan/fxlib-repo-local true --force
conan remote add foxit-conan-center http://conan.foxitsoftware.com:8081/artifactory/api/conan/normex-repo-local true --force
conan user -p %CONAN_PASSWD% -r foxit-conan-center %CONAN_USERNAME%
if %errorlevel% neq 0 goto :cmdErr

REM collect git log 
RM --force FasooDRM.gitlog
git log --max-count=5 > FasooDRM.gitlog

REM clean build env
RMDIR /Q /S FasooDRM\bin
RMDIR /Q /S FasooDRM\temp
conan remove --force FasooDRM/!PACKAGE_VERSION!@foxit/stable

REM compile and package and upload
conan install -s arch=x86 -s arch_build=x86 -s compiler.runtime=MT .
if %errorlevel% neq 0 goto :cmdErr
conan build .
if %errorlevel% neq 0 goto :cmdErr
REM wmic csproduct list full | findstr UUID
REM c:\vagrant\bin\remote-sign.exe sign --sign-file=c:\vagrant\Plugins\FasooDRM\FasooDRM\bin\FasooDRM\Win32\Release\v141\FasooDRM.fpi  --description="Foxit Software Incorporated"
REM if %errorlevel% neq 0 goto :cmdErr
conan export-pkg -f conanfile.py FasooDRM/!PACKAGE_VERSION!@foxit/stable -s build_type=Release
if %errorlevel% neq 0 goto :cmdErr
conan upload FasooDRM/!PACKAGE_VERSION!@foxit/stable -r foxit-conan-center --all --force
if %errorlevel% neq 0 goto :cmdErr

endlocal
exit /b 0

:cmdErr
endlocal
exit /b 1
