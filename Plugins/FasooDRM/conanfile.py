from conans import ConanFile, MSBuild, tools
import os
import codecs

def get_encoding(file):
    byte_string = file.read(2)
    if byte_string.startswith(codecs.BOM_UTF16_LE):
        return 'utf_16_le'
    if byte_string.startswith(codecs.BOM_UTF16_BE):
        return 'utf_16_be'

    return 'ascii'

def get_build_version(package_version):
    package_version_list = str(package_version).split('.')
    return (package_version_list.pop() if package_version_list else None)

def modify_line(line, find_str, build_version, delimiter='.', version_encloser=None):
    start = line.find(find_str)
    if start == -1:
        return None
        
    version = line[start + len(find_str):]
    if version_encloser != None:
        version_start = version.find(version_encloser)
        if (version_start == -1):
            return None;
        version_end = version.find(version_encloser, version_start + 1)
        if (version_end == -1):
            return None;
        version = version[version_start + 1:version_end]

    version_list = version.split(delimiter)
    if len(version_list) != 4:
        return None

    major = int(version_list[0])
    minor = int(version_list[1])
    patch = int(version_list[2])

    line = line[:start] + find_str + ' '
    if (version_encloser != None):
        line += version_encloser
    line += str(major) + delimiter + str(minor) + delimiter + str(patch) + delimiter + str(build_version)
    if (version_encloser != None):
        line += version_encloser
    line += '\r\n'
    return line

def set_rc_build_number(file_path, build_version):
    print('Changing RC file: ' + file_path)
    # Read byte BOM and get propriate encoding
    encoding = None
    with open(file_path, "rb") as file:
        encoding = get_encoding(file)

    # Read all lines from from rc file
    with codecs.open(file_path, 'r', encoding=encoding) as file:
        if encoding == 'utf_16_le' or encoding == 'utf_16_be':
            file.read(1)
        lines = file.readlines()

    # Modify version number
    for i in range(len(lines)):
        line = modify_line(lines[i], 'FILEVERSION', build_version, ',')
        if line == None:
            line = modify_line(lines[i], 'PRODUCTVERSION', build_version, ',')
        if line == None:
            line = modify_line(lines[i], 'VALUE "FileVersion",', build_version, '.', '"')
        if line == None:
            line = modify_line(lines[i], 'VALUE "ProductVersion",', build_version, '.', '"')
        if line != None:
            print('Modified rc line: "' + lines[i].rstrip() + ' -> ' + line.rstrip())
            lines[i] = line

    # Write all lines back to rc file
    with codecs.open(file_path, 'w', encoding=encoding) as file:
        if encoding == 'utf_16_le':
            file.write('\ufeff')
        elif encoding == 'utf_16_be':
            file.write('\ufeff')
        for line in lines:
            file.write(line)

class FasoodrmConan(ConanFile):
    name = "FasooDRM"
    version = os.environ.get('PACKAGE_VERSION', '1.1')
    author = "Foxit Software"
    url = "www.foxitsoftware.com"
    description = "FasooDRM plugin"
    settings = "os", "compiler", "build_type", "arch"
    generators = "visual_studio"

    def build(self):
        if self.settings.compiler == "Visual Studio":
            print('Building using Visual Studio compiler')
            build_version = get_build_version(self.version)
            if build_version:
                set_rc_build_number('FasooDRM\FasooDRM.rc', build_version)
            msbuild = MSBuild(self)
            msbuild.build("FasooDRM.sln", upgrade_project=False)
        else:
            raise Exception("Unsupported compiler. This project should be compiled by Visual Studio")

    def package(self):
        if self.settings.build_type == "Release":
            self.copy("FasooDRM\\bin\\FasooDRM\\Win32\\Release\\v141\\FasooDRM.fpi", dst="plugin_package", keep_path=False)
        elif self.settings.build_type == "Debug":
            self.copy("FasooDRM\\bin\\FasooDRM\\Win32\\Debug\\v141\\FasooDRM.fpi", dst="plugin_package", keep_path=False)
        else:
            raise Exception("Unknown build_type. Should be Release or Debug")
        self.copy("FasooDRM\\InstallFasooDRM.xml", dst="plugin_package", keep_path=False)
        self.copy("FasooDRM\\res\\Lang\\ko-KR\\FasooDRMlang_ko-KR.xml", dst="plugin_package\\Lang\\ko-KR", keep_path=False)
        self.copy("FasooDRM_SDK\\x86\\f_extadk.dll", dst="plugin_package\\Windows\\SysWOW64", keep_path=False)
        self.copy("FasooDRM_SDK\\f_extadk_0161.fac", dst="plugin_package\\Windows\\SysWOW64", keep_path=False)
        self.copy("*.gitlog", dst="plugin_package", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["plugin_package"]

