#include "stdafx.h"
#include "FasooDRM.h"
#include "AppEventProvider.h"
#include "FasooHelpers.h"
#include "MultiLang.h"

extern CFasooDRMApp theApp;

// ctor
AppEventProvider::AppEventProvider() 
    : app_callbacks_(new FR_AppEventCallbacksRec)    
{
    std::memset(app_callbacks_.get(), 0, sizeof(FR_AppEventCallbacksRec));
    
    app_callbacks_->lStructSize = sizeof(FR_AppEventCallbacksRec);
    app_callbacks_->clientData = nullptr; 

    app_callbacks_->FRAppOnOpenDocument = OnOpenDocument;
    app_callbacks_->FRAppOnLangUIChange = OnLangUIChange;
    app_callbacks_->FROnAppDidCombineMultiFile = OnDidCombineMultiFile;
#ifdef _DEBUG
    app_callbacks_->FRAppWillConvertPDF = WillConvertPDF;
#endif
    app_callbacks_->FRAppDidConvertPDF = DidConvertPDF;
}

// callbacks
FS_BOOL AppEventProvider::OnOpenDocument(FS_LPVOID clientData, FS_LPCWSTR lpszFilePath) {
    if (!ADKIsSecureContentByPath(lpszFilePath))
        return FALSE;

    if (!ADKIsLogonStatus(NULL)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_NOT_LOGGED_IN, MB_OK | MB_ICONERROR);
        return TRUE;
    }

    HCONTENT content = ADKOpenContent(lpszFilePath, FALSE, FALSE);
    // hotfix: ADKOpenContent may return NULL because of error 0x201 (fac file). Second call passes
    if (!content)
        content = ADKOpenContent(lpszFilePath, FALSE, FALSE);
    // end of hotfix

    if (!content) {
        auto err = ADKGetLastError();
        if (err == E_ADK_CONTENT_INVALID_LICENSE)
            MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_OPEN_DENIED, MB_OK | MB_ICONERROR);
        else
            MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_OPEN_FILE, MB_OK | MB_ICONERROR);
        return TRUE;
    }

    if (!ADKIsLicenseValid(content, ADK_PURPOSE_VIEW)) {
        ADKCloseContent(content);
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_OPEN_FILE, MB_OK | MB_ICONERROR);
        return TRUE;
    }

    if (!ADKSetUsage(content, ADK_PURPOSE_VIEW)) {
        ADKCloseContent(content);
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_OPEN_FILE, MB_OK | MB_ICONERROR);
        return TRUE;
    }

    DWORD content_size = ADKGetContentSize(content);
    if (content_size == INVALID_CONTENT_SIZE) {
        ADKCloseContent(content);
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_OPEN_FILE, MB_OK | MB_ICONERROR);
        return TRUE;
    }

    std::unique_ptr<char[]> content_buffer = std::make_unique<char[]>(content_size);
    if (!ADKReadContent(content, content_buffer.get(), content_size, &content_size)) {
        ADKCloseContent(content);
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_OPEN_FILE, MB_OK | MB_ICONERROR);
        return TRUE;
    }

    theApp.doc_manager_->AddFasooDoc(lpszFilePath, content, std::move(content_buffer), content_size);

    return FALSE;
}

void AppEventProvider::OnLangUIChange(FS_LPVOID clientData) {
    if (theApp.fr_lang_.get())
        FRLanguageChange(theApp.fr_lang_.get(), FRLanguageGetCurrentID());
}

void AppEventProvider::OnDidCombineMultiFile(FS_LPVOID clientData, FS_WideString outFile, const FS_WideStringArray usedFile) {
    // check if list of files used in combine contains Fasoo DRM file
    FS_INT32 files_count = FSWideStringArrayGetSize(usedFile);
    HCONTENT fasoo_content = NULL;

    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> file_ptr(FSWideStringNew(), FSWideStringDestroy);
    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> current_sec_ptr(FSWideStringNew(), FSWideStringDestroy);
    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> fasoo_lbl_ptr(FSWideStringNew3(L"Fasoo", 5), FSWideStringDestroy);
    
    FS_WideString file = file_ptr.get();
    FS_WideString current_sec = current_sec_ptr.get();
    
    FR_Document doc = nullptr;
    bool other_sec = false;

    for (FS_INT32 idx = 0; idx < files_count; ++idx) {
        FSWideStringArrayGetAt(usedFile, idx, &file);
        FS_LPCWSTR f = FSWideStringCastToLPCWSTR(file);

        if (f && !fasoo_content && ADKIsSecureContentByPath(f)) {
            fasoo_content = ADKOpenContent(f, FALSE, FALSE);           
        }

        doc = FRDocOpenFromFile(f, "", FALSE, FALSE);
        if (!doc) {
            other_sec = true;
            FRDocClose(doc, FALSE, FALSE, FALSE);
            doc = nullptr;
            continue;
        }

        FRDocGetCurrentSecurityMethodName(doc, &current_sec);
       
        if (FSWideStringGetLength(current_sec) > 0 && FSWideStringFind(current_sec, fasoo_lbl_ptr.get(), 0) == -1) {
            other_sec = true;
        }
        
        FRDocClose(doc, FALSE, FALSE, FALSE);
        doc = nullptr;

        if (other_sec)
            break;
    }

    if (!other_sec && fasoo_content != 0) {
        DWORD written_bytes = 0;
        ULONGLONG size = 0;
        std::unique_ptr<char[]> buf;

        auto file_name = FSWideStringCastToLPCWSTR(outFile);
        if (!file_name)
            return;

        CFile file;
            
        if (file.Open(file_name, CFile::modeRead) == 0)
            return;

        size = file.GetLength();
        if (size > static_cast<DWORD>(-1)) {
            MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
            ADKCloseContent(fasoo_content);
            fasoo_content = 0;

            return;
        }
        buf = std::make_unique<char[]>(static_cast<DWORD>(size));
        file.Read(buf.get(), static_cast<DWORD>(size));
        file.Close();
 
        HCONTENT save_content = ADKOpenContent(file_name, TRUE, TRUE);
            
        if (!save_content) {
            MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
            ADKCloseContent(fasoo_content);
            fasoo_content = 0;

            return;
        }

        if (!ADKPackContent(save_content, fasoo_content)) {
            MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
            ADKCloseContent(fasoo_content);
            ADKCloseContent(save_content);
            fasoo_content = 0;

            return;
        }
            
        if (!ADKWriteContent(save_content, buf.get(), static_cast<DWORD>(size), &written_bytes)) {
            MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
            ADKCloseContent(save_content);

            return;
        }

        if (!ADKSetEndOfContent(save_content)) {
            MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
            ADKCloseContent(fasoo_content);
            ADKCloseContent(save_content);
            fasoo_content = 0;

            return;
        }

        if (!ADKSetUsage(save_content, ADK_PURPOSE_SECURE_SAVE)) {
            MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
            ADKCloseContent(fasoo_content);
            ADKCloseContent(save_content);
            fasoo_content = 0;

            return;
        }

        ADKCloseContent(save_content);
        
        fasoo_content = 0;
    }
}

#ifdef _DEBUG
void AppEventProvider::WillConvertPDF(FS_LPVOID clientData, FS_LPCWSTR inPath, FS_LPCWSTR outPath)
{
    TRACE(_T("FasooDRM: WillConvertPDF\n"));
}
#endif

void AppEventProvider::DidConvertPDF(FS_LPVOID clientData, FS_LPCWSTR inPath, FS_LPCWSTR outPath)
{
    // do DRM protection here for outPaht (already converted file in one of office formats)
    FR_Document fr_doc = FRAppGetActiveDocOfPDDoc();
    if (!fr_doc)
        return;

    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(fr_doc);
    if (!fasoo_doc)
        return;

#ifdef _DEBUG
    if (!outPath)
        AfxMessageBox(_T("DidConvertPDF - outPath == nullptr\n"));
    else {
        std::wstring errMsg = std::wstring(L"DidConvertPDF - outPath == ") + outPath;
        AfxMessageBox(errMsg.c_str());
    }
#endif

    // read buffer here
    CFile office_file;
    if (!office_file.Open(outPath, CFile::modeRead)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_PROTECT_OFFICE_FILE, MB_OK | MB_ICONERROR);
        ::DeleteFile(outPath);
        return;
    }

    auto office_size = office_file.GetLength();
    if (office_size > static_cast<DWORD>(-1)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_PROTECT_OFFICE_FILE, MB_OK | MB_ICONERROR);
        ::DeleteFile(outPath);
        return;
    }
    std::unique_ptr<char[]> buf(new char[static_cast<DWORD>(office_size)]);

    auto office_read = office_file.Read(buf.get(), static_cast<DWORD>(office_size));
    if (office_read != static_cast<DWORD>(office_size)) {
        office_file.Close();
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_PROTECT_OFFICE_FILE, MB_OK | MB_ICONERROR);
        ::DeleteFile(outPath);
        return;
    }

    office_file.Close();
   
    HCONTENT content = ADKOpenContent(outPath, TRUE, TRUE);

    if (!content) {
        auto err = ADKGetLastError();
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_PROTECT_OFFICE_FILE, MB_OK | MB_ICONERROR);
        ::DeleteFile(outPath);
        return;
    }

    if (!ADKPackContent(content, fasoo_doc->GetContent())) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_PROTECT_OFFICE_FILE, MB_OK | MB_ICONERROR);
        ::DeleteFile(outPath);
        ADKCloseContent(content);
        return;
    }

    DWORD written_bytes = 0;
    if (!ADKWriteContent(content, buf.get(), static_cast<DWORD>(office_size), &written_bytes)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_PROTECT_OFFICE_FILE, MB_OK | MB_ICONERROR);
        ::DeleteFile(outPath);
        ADKCloseContent(content);
        return;
    }

    if (!ADKSetEndOfContent(content)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_PROTECT_OFFICE_FILE, MB_OK | MB_ICONERROR);
        ::DeleteFile(outPath);
        ADKCloseContent(content);
        return;
    }

    if (!ADKSetUsage(content, ADK_PURPOSE_SECURE_SAVE)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_PROTECT_OFFICE_FILE, MB_OK | MB_ICONERROR);
        ::DeleteFile(outPath);
        ADKCloseContent(content);
        return;
    }

    ADKCloseContent(content);
}
