#pragma once

#include <memory>

class PageOrganizeProvider final
{
public:
    PageOrganizeProvider();
    ~PageOrganizeProvider() {};

    FR_POEventCallbacks GetPageOrganizeEventCallbacks() const noexcept { return this->page_organize_callbacks_.get(); }

private:
    static void OnAfterExtractPageEx(FS_LPVOID clientData, FR_Document frDoc, FS_WordArray arrSrcPages, const FS_WideString& wsDestfilepath);

private:
    std::unique_ptr<FR_POEventCallbacksRec> page_organize_callbacks_;
};
