#pragma once

#include "f_extadk.h"

#include <string>

bool FasooEncryptFile(FS_LPCWSTR file_path, HCONTENT template_content);
std::wstring FasooDecryptFile(FS_LPCWSTR file_path);
