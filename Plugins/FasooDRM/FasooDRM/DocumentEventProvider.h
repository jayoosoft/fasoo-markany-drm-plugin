#pragma once

#include "StdAfx.h"
#include <memory>

class DocumentEventProvider final
{
public:
    DocumentEventProvider();
    ~DocumentEventProvider() {}

    struct DocumentEventProviderData {
        void* fasoo_doc_ = nullptr;
    };

    FR_DocEventCallbacks GetDocEventCallbacks() const noexcept { return de_callbacks_.get(); }

    static void OnFRDocOnActivate(FS_LPVOID clientData, FR_Document doc);
    static void OnFRDocOnDeactivate(FS_LPVOID clientData, FR_Document doc);
    static void OnFRDocWillPrint(FS_LPVOID clientData, FR_Document doc);
    static void OnFRDocDidPrint(FS_LPVOID clientData, FR_Document doc);
    static void OnFRDocDidSave(FS_LPVOID clientData, FR_Document doc, FS_BOOL bSaveAs);
    static void OnFRDocOnFrameCreate(FS_LPVOID clientData, FR_Document doc, HWND hFrameWnd);
    static void OnFRDocOnChange(FS_LPVOID clientData, FR_Document doc);
    static BOOL OnFRDocOwnerSaveAs(FS_LPVOID clientData, FR_Document doc, FS_LPCWSTR wszPathName);
    static FS_BOOL OnFRDocSaveAsBeforeReopen(FS_LPVOID clientData, FS_LPCWSTR wsFileName);

private:
    std::unique_ptr<FR_DocEventCallbacksRec> de_callbacks_;
    DocumentEventProviderData client_data_;
};
