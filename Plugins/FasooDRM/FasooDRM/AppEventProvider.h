#pragma once
#include "StdAfx.h"
#include <memory>

class AppEventProvider final
{
public:
    AppEventProvider();
    ~AppEventProvider() {}

    FR_AppEventCallbacks GetAppEventCallbacks() const noexcept { return const_cast<FR_AppEventCallbacks>(app_callbacks_.get()); }

    static FS_BOOL OnOpenDocument(FS_LPVOID clientData, FS_LPCWSTR lpszFilePath);
    static void OnLangUIChange(FS_LPVOID clientData);
    static void OnDidCombineMultiFile(FS_LPVOID clientData, FS_WideString outFile, const FS_WideStringArray usedFile);
#ifdef _DEBUG
    static void WillConvertPDF(FS_LPVOID clientData, FS_LPCWSTR inPath, FS_LPCWSTR outPath);
#endif
    static void DidConvertPDF(FS_LPVOID clientData, FS_LPCWSTR inPath, FS_LPCWSTR outPath);

private:
    std::unique_ptr<FR_AppEventCallbacksRec> app_callbacks_;   
};
