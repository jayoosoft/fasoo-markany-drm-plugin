//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FasooDRM.rc
//
#define IDD_FORMVIEW                    101
#define IDD_PERMS_SHOW_DLG              101
#define IDS_SAVE_NOT_ALLOWED            1000
#define IDC_STATIC_VIEW_DOCUMENT        1000
#define IDC_STATIC_VIEW_DOC_PERM        1000
#define IDS_CANNOT_CREATE_FILE          1001
#define IDC_STATIC_SAVE_DOCUMENT        1001
#define IDC_STATIC_SAVE_DOC_PERM        1001
#define IDS_SECURITY_METHOD_NAME        1002
#define IDC_STATIC_EDIT_DOCUMENT        1002
#define IDC_STATIC_EDIT_DOC_PERM        1002
#define IDS_SECURITY_METHOD_TITLE       1003
#define FASOODRM_ICON_16                1003
#define IDC_STATIC_DOCUMENT_EXTRACTION  1003
#define IDC_STATIC_DOC_EXTRACTION_PERM  1003
#define FASOODRM_ICON_24                1004
#define IDS_VIRTUAL_PRINTER_DISABLED_PRINT 1004
#define IDC_STATIC_PRINTING             1004
#define IDC_STATIC_PRINT_PERM           1004
#define FASOODRM_ICON_32                1005
#define IDS_OPEN_DENIED                 1005
#define IDC_STATIC_PRINT_SCREEN         1005
#define IDC_STATIC_PRINTSCREEN_PERM     1005
#define IDB_PNG4                        1006
#define FASOODRM_ICON_48                1006
#define IDS_FASOODRM_DO_SETTINGS        1006
#define IDC_STATIC_TRANSFER             1006
#define IDC_STATIC_TRANSFER_DOC_PERM    1006
#define IDS_SECURITY_METHOD_DESCRIPTION 1007
#define IDC_STATIC_SAVE_SECURE          1007
#define IDC_STATIC_SECURE_SAVE_PERM     1007
#define IDS_START_PRINT_SECURED         1008
#define IDC_STATIC_SECURE_PRINT         1008
#define IDC_STATIC_SECURE_PRINT_DOC_PERM 1008
#define IDS_END_PRITN_SECURED           1009
#define IDC_STATIC_SUMMARY              1009
#define IDS_END_PRINT_SECURED           1009
#define IDC_STATIC_VIEW_DOC             1010
#define IDS_ALLOWED                     1010
#define IDC_STATIC_SAVE_DOC             1011
#define IDS_NOT_ALLOWED                 1011
#define IDC_STATIC_EDIT_DOC             1012
#define IDS_DOC_RESTRICTIONS_SUMMARY    1012
#define IDC_STATIC_DOC_EXTRACTION       1013
#define IDS_VIEW_DOCUMENT               1013
#define IDC_STATIC_PRINT                1014
#define IDS_SAVE_DOCUMENT               1014
#define IDC_STATIC_PRINTSCREEN          1015
#define IDS_EDIT_DOCUMENT               1015
#define IDC_STATIC_TRANSFER_DOC         1016
#define IDS_DOCUMENT_EXTRACTION         1016
#define IDC_STATIC_SECURE_SAVE          1017
#define IDS_PRINTING                    1017
#define IDC_STATIC_SECURE_PRINT_DOC     1018
#define IDS_PRINTSCREEN                 1018
#define IDS_TRANSFER                    1019
#define IDS_SECURE_SAVE                 1020
#define IDS_SECURE_PRINT                1021
#define IDS_NOT_LOGGED_IN               1022
#define IDS_BULB_TITLE                  1023
#define IDS_BULB_MESSAGE                1024
#define IDS_BULB_VIEW_PERMISSIONS       1025
#define IDS_BULB_VIEW_PERMS             1025
#define IDS_CANNOT_OPEN_FILE            1026
#define IDS_CANNOT_EXTRACT_PAGES        1027
#define IDS_CANNOT_PROTECT_OFFICE_FILE  1028

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1008
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           1000
#endif
#endif
