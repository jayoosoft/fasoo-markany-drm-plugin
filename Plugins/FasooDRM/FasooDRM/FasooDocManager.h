#pragma once

#include "StdAfx.h"
#include "FasooDocument.h"
#include <memory>
#include <map>

using FasooDocs = std::map<FR_Document, std::unique_ptr<FasooDocument>>;

class FasooDocManager
{
public:
    FasooDocManager() {}
    ~FasooDocManager() {}

    void AddFasooDoc(const std::wstring& path, HCONTENT content, std::unique_ptr<char[]>&& data, std::size_t data_size);
    void RemoveFasooDoc(FR_Document fr_doc);
    const FasooDocument* UpdateFasooDoc(const FS_LPCWSTR path, FR_Document new_fr_doc);

    const FasooDocument* GetFasooDoc(FR_Document fr_doc) const;
    FasooDocument* GetFasooDoc(FR_Document fr_doc);

    const FasooDocument* GetFasooDoc(const FS_WideString path) const;
    FasooDocument* GetFasooDoc(const FS_WideString path);

private:
    //!< std::map of all opened documents sorted by FR_Document
    FasooDocs docs_;

    //!< Document that pass via AppEventProvider::OnOpenDocument and waiting to
    //   process by ContentProvider::OnFileOpen
    std::unique_ptr<FasooDocument> unopened_doc_;
};
