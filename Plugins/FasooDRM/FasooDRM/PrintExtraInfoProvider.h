#pragma once

#include "StdAfx.h"

#include <memory>

class PrintExtraInfoProvider
{
public:
    PrintExtraInfoProvider();
    ~PrintExtraInfoProvider() {}

    PrintExtraInfoProvider(const PrintExtraInfoProvider&) = delete;
    PrintExtraInfoProvider(PrintExtraInfoProvider&&) = delete;
    
    PrintExtraInfoProvider& operator=(const PrintExtraInfoProvider&) = delete;
    PrintExtraInfoProvider& operator=(PrintExtraInfoProvider&&) = delete;

    FR_ExtraPrintInfoProviderCallbacks GetExtraPrintInfoCallbacks() const { return pei_callbacks_.get(); }

    static FS_BOOL OnFRToMast(FS_LPVOID clientData, FR_Document frDoc);
    static FS_BOOL OnFRCanBePrinted2(FS_LPVOID clientData,
        FR_Document frDoc,
        FS_INT32 copies,
        FS_BOOL bTotalPages,
        FS_DWordArray arrPages,
        FS_BOOL bPostScript,
        FS_BOOL bLocal,
        FS_BOOL bNetWork,
        FS_BOOL bShared,
        FS_WideString Printer,
        FS_ByteString PrinterModel,
        FS_ByteString bsSubset,
        FS_ByteString PrinterDriver,
        FS_ByteString PrinterData,
        FS_ByteString PrinterProt);

private:
    std::unique_ptr<FR_ExtraPrintInfoProviderCallbackRec> pei_callbacks_;
};
