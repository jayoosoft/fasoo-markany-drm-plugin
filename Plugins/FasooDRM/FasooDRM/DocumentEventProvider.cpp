#include "stdafx.h"
#include "DocumentEventProvider.h"
#include "FasooDRM.h"
#include "Logger.h"
#include "MultiLang.h"
#include "ContentProvider.h"
#include "Resource.h"

#include <sstream>

extern CFasooDRMApp theApp;

// ctors
DocumentEventProvider::DocumentEventProvider() : de_callbacks_(new FR_DocEventCallbacksRec) {
    std::memset(de_callbacks_.get(), 0, sizeof(FR_DocEventCallbacksRec));

    de_callbacks_->clientData = &client_data_;
    de_callbacks_->lStructSize = sizeof(FR_DocEventCallbacksRec);

    de_callbacks_->FRDocOnActivate = OnFRDocOnActivate;
    de_callbacks_->FRDocOnDeactivate = OnFRDocOnDeactivate;
    de_callbacks_->FRDocWillPrint = OnFRDocWillPrint;
    de_callbacks_->FRDocDidPrint = OnFRDocDidPrint;
    de_callbacks_->FRDocDidSave = OnFRDocDidSave;
    de_callbacks_->FRDocOnFrameCreate = OnFRDocOnFrameCreate;
    de_callbacks_->FRDocOnChange = OnFRDocOnChange;
    de_callbacks_->FRDocOwnerSaveAs = OnFRDocOwnerSaveAs;
    de_callbacks_->FRDocSaveAsBeforeReopen = OnFRDocSaveAsBeforeReopen;
}

// dtor

// methods
void DocumentEventProvider::OnFRDocOnActivate(FS_LPVOID clientData, FR_Document doc) {
    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(doc);

    if (fasoo_doc) {
        HCONTENT content = fasoo_doc->GetContent();
        
        if (content && !ADKIsLicenseValid(content, ADK_PURPOSE_PRINT_SCREEN)) {
            HWND hwnd = FRDocGetDocFrameHandler(doc);
            
            if (hwnd)
                ADKBlockCapture(content, hwnd, TRUE);
        }
    }
}

void DocumentEventProvider::OnFRDocOnDeactivate(FS_LPVOID clientData, FR_Document doc) {
    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(doc);

    if (fasoo_doc) {
        HCONTENT content = fasoo_doc->GetContent();

        if (content) {
            HWND hwnd = FRDocGetDocFrameHandler(doc);

            if (hwnd)
                ADKBlockCapture(content, hwnd, FALSE);
        }
    }
}

void DocumentEventProvider::OnFRDocWillPrint(FS_LPVOID clientData, FR_Document doc) {
    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(doc);

    if (fasoo_doc) {
        HCONTENT content = fasoo_doc->GetContent();

        if (content) {
            BOOL started_print = ADKStartPrint(content);
            fasoo_doc->StartPrint();

            if (!started_print) {
                std::wstringstream stream;
                stream << L"Secured print failed, error code: " << ADKGetLastError();
                Logger::LogMessage(FR_ERROR, stream.str().c_str(), __FILE__, __LINE__, __FUNCTION__);

                MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_START_PRINT_SECURED, MB_OK | MB_ICONERROR);                
            }
        }
    }
}

void DocumentEventProvider::OnFRDocDidPrint(FS_LPVOID clientData, FR_Document doc) {
    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(doc);

    if (fasoo_doc) {
        HCONTENT content = fasoo_doc->GetContent();

        if (content && fasoo_doc->IsStartedPrint()) {
            BOOL secured_print = FALSE;
            BOOL print_success = ADKEndPrint(content, &secured_print);

            if (print_success) {
                if (secured_print) {
                    ADKSetUsage(content, ADK_PURPOSE_SECURE_PRINT);
                }
                else {
                    ADKSetUsage(content, ADK_PURPOSE_PRINT);
                }
            }
            else {
                std::wstringstream stream;
                stream << L"Print failed, error code: " << ADKGetLastError();
                Logger::LogMessage(FR_ERROR, stream.str().c_str(), __FILE__, __LINE__, __FUNCTION__);

                MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_END_PRINT_SECURED, MB_OK | MB_ICONERROR);
            }

            fasoo_doc->EndPrint();
        }
    }
}

void DocumentEventProvider::OnFRDocDidSave(FS_LPVOID clientData, FR_Document doc, FS_BOOL bSaveAs) {
    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(doc);
    if (!fasoo_doc)
        return;

    // reopen Fasoo document
    if (bSaveAs) {
        if (fasoo_doc->GetContent() == NULL) {
            HCONTENT content = ADKOpenContent(fasoo_doc->GetDocPath().c_str(), FALSE, FALSE);

            if (!content) {
                std::wstringstream stream;
                stream << L"OnFRDocDidSave (reopen content) faield, error code: " << ADKGetLastError();
                Logger::LogMessage(FR_ERROR, stream.str().c_str(), __FILE__, __LINE__, __FUNCTION__);
            }

            fasoo_doc->SetContent(content);
        }
    }
    else {
        HCONTENT content = fasoo_doc->GetContent();

        if (content) {
            ADKCloseContent(content);
            content = NULL;
        }

        const std::wstring& doc_path = fasoo_doc->GetDocPath();
        content = ADKOpenContent(doc_path.c_str(), FALSE, FALSE);

        if (!content) {
            std::wstringstream stream;
            stream << L"OnFRDocDidSave (reopen content) faield, error code: " << ADKGetLastError();
            Logger::LogMessage(FR_ERROR, stream.str().c_str(), __FILE__, __LINE__, __FUNCTION__);
            return;
        }

        fasoo_doc->SetContent(content);
    }
}

void DocumentEventProvider::OnFRDocOnFrameCreate(FS_LPVOID clientData, FR_Document doc, HWND hFrameWnd) {
    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(doc);

    if (fasoo_doc) {
        theApp.ShowInfoBulb(doc);
    }
}

void DocumentEventProvider::OnFRDocOnChange(FS_LPVOID clientData, FR_Document doc) {
    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(doc);

    if (fasoo_doc) {
        HCONTENT content = fasoo_doc->GetContent();

        if (content) {
            ADKSetUsage(content, ADK_PURPOSE_EDIT);
        }
    }
}

BOOL DocumentEventProvider::OnFRDocOwnerSaveAs(FS_LPVOID clientData, FR_Document doc, FS_LPCWSTR wszPathName) {
    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(doc);

    if (fasoo_doc) {
        DocumentEventProviderData* provider_data = static_cast<DocumentEventProviderData*> (clientData);
        provider_data->fasoo_doc_ = static_cast<void*> (fasoo_doc);
    }

    return FALSE;
}

FS_BOOL DocumentEventProvider::OnFRDocSaveAsBeforeReopen(FS_LPVOID clientData, FS_LPCWSTR wsFileName) {
    DocumentEventProviderData* provider_data = static_cast<DocumentEventProviderData*> (clientData);
    
    if (provider_data->fasoo_doc_) {
        FasooDocument* fasoo_doc = static_cast<FasooDocument*> (provider_data->fasoo_doc_);

        ContentProvider::SaveProtectedContentAs(fasoo_doc, wsFileName, (unsigned char*) fasoo_doc->GetContentBuffer(), fasoo_doc->GetContentSize());

        provider_data->fasoo_doc_ = nullptr;
    }

    return FALSE;
}
