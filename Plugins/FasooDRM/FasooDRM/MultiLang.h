#pragma once

#include "StdAfx.h"

class MultiLang final
{
private:
    MultiLang() {}

public:
    ~MultiLang() {}

    static bool LoadTranslatedString(UINT id, FS_WideString str);
    static int ShowMessageBox(HWND parent, UINT string_id, UINT type);
};
