#pragma once

#include "StdAfx.h"
#include "f_extadk.h"
#include "SecurityPropertiesDlg.h"

#include <string>
#include <memory>
#include <optional>

class FasooDocManager;

class FasooDocument final
{
    friend class FasooDocManager;

private:
    FasooDocument(const std::wstring& path, HCONTENT content, std::unique_ptr<char[]>&& data, std::size_t data_size);

public:
    ~FasooDocument();

    void UpdateData(char* data, std::size_t data_size);

    std::size_t GetContentSize() const noexcept { return data_size_; }
    const char* GetContentBuffer() const noexcept { return data_.get(); }

    unsigned long GetPermissions() const noexcept { return perms_; }
    void SetPermissions(unsigned long p) noexcept { perms_ = p; }

    HCONTENT GetContent() const noexcept { return hcontent_; }
    void SetContent(HCONTENT content) noexcept { hcontent_ = content; }

    const std::optional<bool>& CanBeSaved() const noexcept { return can_save_; }
    void SetCanBeSaved(bool s) noexcept { can_save_ = s; }

    const std::wstring& GetDocPath() const noexcept { return doc_path_; }

    bool RemoveSecurity();
    bool IsSecurityRemoved() const noexcept { return security_removed_; }

    bool IsPrintEnabled() const;
    bool IsStartedPrint() const noexcept { return started_print_; }
    void StartPrint() { started_print_ = true; }
    void EndPrint() { started_print_ = false; }

    void SetSecurityPropertiesDlg(SecurityPropertiesDlg* dlg) noexcept{ sec_prop_dlg_ = dlg; }
    SecurityPropertiesDlg* GetSecurityPropertiesDlg() const noexcept { return sec_prop_dlg_; }

private:
    std::wstring doc_path_;
    HCONTENT hcontent_{ NULL };
    std::unique_ptr<char[]> data_;
    std::size_t data_size_;
    unsigned long perms_ = 0;
    std::optional<bool> can_save_;
    bool security_removed_{ false };
    bool started_print_{ false };
    SecurityPropertiesDlg* sec_prop_dlg_{ nullptr };
};
