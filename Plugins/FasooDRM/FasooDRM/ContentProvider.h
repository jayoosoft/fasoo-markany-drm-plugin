#pragma once

#include "StdAfx.h"

#include "FasooDocument.h"

#include <memory>
#include <map>

#include "f_extadk.h"

class ContentProvider final
{
public:
    ContentProvider();
    ~ContentProvider();

    FR_ContentProviderCallbacks GetContentProviderCallbacks() const noexcept { return cp_callbacks_.get(); }

    static FS_BOOL SaveProtectedContentAs(FasooDocument* fasoo_doc, FS_LPCWSTR file_path, unsigned char* buf, unsigned long size);
    static FS_BOOL SaveProtectedContent(FasooDocument* fasoo_doc, FS_LPCWSTR file_path, unsigned char* buf, unsigned long size);
    
private:
    ContentProvider(const ContentProvider&) = delete;
    ContentProvider(ContentProvider&&) = delete;
    ContentProvider& operator=(const ContentProvider&) = delete;
    ContentProvider& operator=(ContentProvider&&) = delete;

    static FS_BOOL OnFileOpen(FS_LPVOID clientData, FR_Document doc, FS_LPCWSTR lpszSource, FS_BOOL bIsAttachment);
    static void OnFileClose(FS_LPVOID clientData, FR_Document doc);
    static FS_BOOL ContentProvider::OnGetContentSize(FS_LPVOID clientData, /*IN*/ FR_Document doc,/*OUT*/ unsigned long * pTotalSize);
    static FS_BOOL OnReadContent(FS_LPVOID clientData, /*IN*/ FR_Document doc, /*IN*/ DWORD pos, /*OUT*/ unsigned char * pBuf, /*IN*/ unsigned long size);
    static FS_BOOL OnWriteContent(FS_LPVOID clientData, /*IN*/ FR_Document doc, /*IN*/ unsigned char * pBuf, /*IN*/ unsigned long size, /*IN*/ FS_LPCWSTR lpSaveFilePath);
    static unsigned long OnGetPermissions(FS_LPVOID clientData, /*IN*/ FR_Document doc, /*IN*/ unsigned long pdfselfPermissions);
    static FS_BOOL OnCanBeSaved(FS_LPVOID clientData, FR_Document frDoc);
    static FS_BOOL OnBackFillContent(FS_LPVOID clientData, FR_Document doc, unsigned char* pBuf, unsigned long size, FS_LPCWSTR lpSaveFilePath);
    
    std::unique_ptr<FR_ContentProviderCallbacksRec> cp_callbacks_;    
};
