// Starter.h : main header file for the FASOODRM DLL
//

#if !defined(AFX_STARTER_H__AF29C9AF_EA18_4B72_A11C_667186A0D1E2__INCLUDED_)
#define AFX_STARTER_H__AF29C9AF_EA18_4B72_A11C_667186A0D1E2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#include "ContentProvider.h"
#include "FasooDocManager.h"
#include "SecurityMethodProvider.h"
#include "DocumentEventProvider.h"
#include "PrintExtraInfoProvider.h"
#include "AppEventProvider.h"
#include "PageOrganizeProvider.h"

#include <memory>

/////////////////////////////////////////////////////////////////////////////
// CFasooDRMApp
// See Starter.cpp for the implementation of this class
//

class CFasooDRMApp : public CWinApp
{
private:
    const char* bulb_name_ = "FasooDRMBulb";

public:
	CFasooDRMApp();

    std::unique_ptr<ContentProvider> content_provider_{ nullptr };
    std::unique_ptr<FasooDocManager> doc_manager_{ nullptr };
    std::unique_ptr<SecurityMethodProvider> sm_provider_{ nullptr };
    std::unique_ptr<DocumentEventProvider> de_provider_{ nullptr };
    std::unique_ptr<PrintExtraInfoProvider> pei_provider_{ nullptr };
    std::unique_ptr<AppEventProvider> app_event_provider_{ nullptr };
    std::unique_ptr<PageOrganizeProvider> page_organize_provider_{ nullptr };

    std::unique_ptr<_t_FR_Language, decltype(FRLanguageRelease)> fr_lang_{ nullptr, nullptr};

    void ShowInfoBulb(FR_Document doc);
    void HideInfoBulb(FR_Document doc);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFasooDRMApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CFasooDRMApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STARTER_H__AF29C9AF_EA18_4B72_A11C_667186A0D1E2__INCLUDED_)
