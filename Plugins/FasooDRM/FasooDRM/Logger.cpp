#include "stdafx.h"
#include "Logger.h"

#include <memory>

void Logger::LogMessage(FR_LOG_LEVEL level, const std::wstring& msg, const char* file, int line, const char* function) {
    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> fs_str(FSWideStringNew3(msg.c_str(), msg.length()), FSWideStringDestroy);

    FRAppAddLog(FR_ERROR, fs_str.get(), file, line, function);
}
