#pragma once

#include <fstream>
#include <string>

// Utility class for logging
class Logger
{
private:
    Logger() {};

public:
    ~Logger() {};

    static void LogMessage(FR_LOG_LEVEL level, const std::wstring& msg, const char* file, int line, const char* function);
};
