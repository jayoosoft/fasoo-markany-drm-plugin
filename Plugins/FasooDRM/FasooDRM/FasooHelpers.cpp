#include "StdAfx.h"
#include "FasooHelpers.h"
#include "Logger.h"

#include <exception>
#include <sstream>

class FasooEncryptException : public std::exception {
public:
    FasooEncryptException(const char* file, int line, const char* function, DWORD error_code);

    const char* what() const throw() override {
        return error_msg_.c_str();
    }

private:
    std::string error_msg_;
};

FasooEncryptException::FasooEncryptException(const char * file, int line, const char * function, DWORD error_code)
{
    std::wstringstream stream;
    stream << L"Fasoo encryption failed, error code: " << error_code;
    Logger::LogMessage(FR_ERROR, stream.str().c_str(), file, line, function);
}

// Helper function to encrypt file
bool FasooEncryptFile(FS_LPCWSTR file_path, HCONTENT template_content)
{
    wchar_t protected_file_path[MAX_PATH + 1] = L"";
    ::GetTempPathW(MAX_PATH + 1, protected_file_path);
    ::GetTempFileNameW(protected_file_path, nullptr, 0, protected_file_path);
    HCONTENT content = NULL;

    try {
        CFile unprotected_file(file_path, CFile::modeRead | CFile::typeBinary);

        content = ADKOpenContent(protected_file_path, TRUE, TRUE);
        if (!content)
            throw FasooEncryptException(__FILE__, __LINE__, "ADKOpenContent", ADKGetLastError());

        if (!ADKPackContent(content, template_content))
            throw FasooEncryptException(__FILE__, __LINE__, "ADKPackContent", ADKGetLastError());

        const UINT buffer_size = 4096;
        UINT in_bytes = 0;
        char buffer[buffer_size];
        while ((in_bytes = unprotected_file.Read(buffer, buffer_size)) != 0)
        {
            DWORD out_bytes = 0;
            if (!ADKWriteContent(content, buffer, in_bytes, &out_bytes))
                throw FasooEncryptException(__FILE__, __LINE__, "ADKWriteContent", ADKGetLastError());
        }

        if (!ADKSetEndOfContent(content))
            throw FasooEncryptException(__FILE__, __LINE__, "ADKSetEndOfContent", ADKGetLastError());

        ADKCloseContent(content);
        content = NULL;
        unprotected_file.Close();

        if (::MoveFileExW(protected_file_path, file_path, MOVEFILE_COPY_ALLOWED | MOVEFILE_REPLACE_EXISTING) == FALSE)
            throw FasooEncryptException(__FILE__, __LINE__, "MoveFileExW", ::GetLastError());

        return true;
    }
    catch (...) {
        ::DeleteFileW(file_path);
        if (content != NULL) ADKCloseContent(content);

        return false;
    }
}

class FasooDecryptException : public std::exception {
public:
    FasooDecryptException(const char* file, int line, const char* function, DWORD error_code);

    const char* what() const throw() override {
        return error_msg_.c_str();
    }

private:
    std::string error_msg_;
};

FasooDecryptException::FasooDecryptException(const char * file, int line, const char * function, DWORD error_code)
{
    std::wstringstream stream;
    stream << L"Fasoo decryption failed, error code: " << error_code;
    Logger::LogMessage(FR_ERROR, stream.str().c_str(), file, line, function);
}

// Helper function to decrypt file
std::wstring FasooDecryptFile(FS_LPCWSTR file_path)
{
    std::wstring copy_file_path;
    HCONTENT content = NULL;

    try {
        {
            wchar_t temp_file_path[MAX_PATH + 1] = L"";
            ::GetTempPathW(MAX_PATH + 1, temp_file_path);
            ::GetTempFileNameW(temp_file_path, nullptr, 0, temp_file_path);

            if (::MoveFileExW(file_path, temp_file_path, MOVEFILE_COPY_ALLOWED | MOVEFILE_REPLACE_EXISTING) == FALSE)
                throw FasooDecryptException(__FILE__, __LINE__, "MoveFileExW", ::GetLastError());

            copy_file_path = temp_file_path;
        }

        CFile unprotected_file(file_path, CFile::modeWrite | CFile::typeBinary);

        content = ADKOpenContent(copy_file_path.c_str(), FALSE, FALSE);
        if (!content)
            throw FasooDecryptException(__FILE__, __LINE__, "ADKOpenContent", ADKGetLastError());

        const UINT buffer_size = 4096;
        DWORD in_bytes = 0;
        char buffer[buffer_size];
        while (ADKReadContent(content, buffer, buffer_size, &in_bytes) && in_bytes != 0)
            unprotected_file.Write(buffer, in_bytes);

        unprotected_file.Close();
        
    }
    catch (...) {
        if (content != NULL) ADKCloseContent(content);
        if (!copy_file_path.empty()) {
            ::MoveFileExW(copy_file_path.c_str(), file_path, MOVEFILE_COPY_ALLOWED | MOVEFILE_REPLACE_EXISTING);
            copy_file_path.clear();
        }
    }

    return std::move(copy_file_path);
}
