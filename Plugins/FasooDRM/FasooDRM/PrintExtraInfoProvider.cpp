#include "stdafx.h"
#include "PrintExtraInfoProvider.h"
#include "FasooDRM.h"
#include "MultiLang.h"
#include "Resource.h"

extern CFasooDRMApp theApp;

// ctor
PrintExtraInfoProvider::PrintExtraInfoProvider() : pei_callbacks_(new FR_ExtraPrintInfoProviderCallbackRec) {
    std::memset(pei_callbacks_.get(), 0, sizeof(FR_ExtraPrintInfoProviderCallbackRec));

    pei_callbacks_->clientData = nullptr;
    pei_callbacks_->lStructSize = sizeof(FR_ExtraPrintInfoProviderCallbackRec);
    pei_callbacks_->FRToMast = OnFRToMast;
    pei_callbacks_->FRCanBePrinted2 = OnFRCanBePrinted2;
}

// dtor

// methods
FS_BOOL PrintExtraInfoProvider::OnFRToMast(FS_LPVOID clientData, FR_Document frDoc) {
    return theApp.doc_manager_->GetFasooDoc(frDoc) != nullptr;
}

FS_BOOL PrintExtraInfoProvider::OnFRCanBePrinted2(FS_LPVOID clientData,
    FR_Document frDoc,
    FS_INT32 copies,
    FS_BOOL bTotalPages,
    FS_DWordArray arrPages,
    FS_BOOL bPostScript,
    FS_BOOL bLocal,
    FS_BOOL bNetWork,
    FS_BOOL bShared,
    FS_WideString Printer,
    FS_ByteString PrinterModel,
    FS_ByteString bsSubset,
    FS_ByteString PrinterDriver,
    FS_ByteString PrinterData,
    FS_ByteString PrinterProt) 
{
    FasooDocument* doc = theApp.doc_manager_->GetFasooDoc(frDoc);

    if (!doc)
        return TRUE;

    if (doc->IsPrintEnabled()) {
        // check for virtual printer
        FS_LPCWSTR printer_name = FSWideStringCastToLPCWSTR(Printer);
        
        std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> printer_driver_str(FSWideStringNew(), FSWideStringDestroy);
        FSWideStringConvertFrom(printer_driver_str.get(), PrinterDriver, NULL);

        FS_LPCWSTR printer_driver = FSWideStringCastToLPCWSTR(printer_driver_str.get());

        if (ADKIsVirtualPrinter(NULL, const_cast<FS_LPWSTR> (printer_name), const_cast<FS_LPWSTR> (printer_driver)) == 0) {
            MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_VIRTUAL_PRINTER_DISABLED_PRINT, MB_OK | MB_ICONERROR);
            return FALSE;
        }

        return TRUE;
    }

    return FALSE;
}
