#pragma once

// SecurityPropertiesDlg dialog

class SecurityPropertiesDlg : public CDialogEx
{
	DECLARE_DYNAMIC(SecurityPropertiesDlg)

public:
	SecurityPropertiesDlg(HCONTENT content, CWnd* pParent = nullptr);   // standard constructor
	virtual ~SecurityPropertiesDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PERMS_SHOW_DLG };
#endif

protected:
    virtual BOOL OnInitDialog();

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
    HCONTENT content_;
    FR_RibbonStyleStatic summary_ { nullptr };
    FR_RibbonStyleStatic view_doc_ { nullptr };
    FR_RibbonStyleStatic view_doc_perm_ { nullptr };
    FR_RibbonStyleStatic save_doc_ { nullptr };
    FR_RibbonStyleStatic save_doc_perm_ { nullptr };
    FR_RibbonStyleStatic edit_doc_ { nullptr };
    FR_RibbonStyleStatic edit_doc_perm_ { nullptr };
    FR_RibbonStyleStatic doc_extraction_ { nullptr };
    FR_RibbonStyleStatic doc_extraction_perm_ { nullptr };
    FR_RibbonStyleStatic printing_ { nullptr };
    FR_RibbonStyleStatic printing_perm_ { nullptr };
    FR_RibbonStyleStatic print_screen_ { nullptr };
    FR_RibbonStyleStatic print_screen_perm_ { nullptr };
    FR_RibbonStyleStatic transfer_ { nullptr };
    FR_RibbonStyleStatic transfer_perm_ { nullptr };
    FR_RibbonStyleStatic secure_save_ { nullptr };
    FR_RibbonStyleStatic secure_save_perm_ { nullptr };
    FR_RibbonStyleStatic secure_print_ { nullptr };
    FR_RibbonStyleStatic secure_print_perm_ { nullptr };

    void LoadStaticStrings();
    void SetRibbonStatics();
    void LoadPermissions();
    void ReleaseRibbonStatics();
    FR_RibbonStyleStatic SetSmallFontStatic(UINT static_id);
    FR_RibbonStyleStatic SetLargeFontStatic(UINT static_id);
};
