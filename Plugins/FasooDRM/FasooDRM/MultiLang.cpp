#include "stdafx.h"
#include "FasooDRM.h"
#include "MultiLang.h"

extern CFasooDRMApp theApp;

bool MultiLang::LoadTranslatedString(UINT id, FS_WideString str) {
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    if (str == nullptr)
        return false;

    bool ret = false;
    if (theApp.fr_lang_.get()) {
        int len = FRLanguageLoadString(theApp.fr_lang_.get(), id, nullptr, 0);

        if (len > 0) {
            LPWSTR temp = new WCHAR[len + 1];

            if (temp) {
                std::memset(temp, 0, (len + 1) * sizeof(WCHAR));
                FRLanguageLoadString(theApp.fr_lang_.get(), id, temp, len);
                FSWideStringFill(str, (FS_LPCWSTR)temp);
                delete[] temp;

                ret = true;
            }
        }
    }

    return ret;
}

int MultiLang::ShowMessageBox(HWND parent, UINT string_id, UINT type) {
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> msg(FSWideStringNew(), FSWideStringDestroy);
    
    if (LoadTranslatedString(string_id, msg.get())) {
        return FRSysShowMessageBox(FSWideStringCastToLPCWSTR(msg.get()), type, NULL, NULL, parent);
    }

    return IDCANCEL;
}
