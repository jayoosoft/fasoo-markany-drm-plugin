#include "stdafx.h"
#include "Resource.h"

#include "FasooDRM.h"
#include "SecurityMethodProvider.h"
#include "SecurityPropertiesDlg.h"
#include "MultiLang.h"

extern CFasooDRMApp theApp;

// ctors
SecurityMethodProvider::SecurityMethodProvider() : 
    method_name_(FSWideStringNew(), FSWideStringDestroy),
    method_title_(FSWideStringNew(), FSWideStringDestroy),
    method_description_(FSWideStringNew(), FSWideStringDestroy),

    sm_callbacks_(new FR_SecurityMethodCallbacksRec) 
{
    MultiLang::LoadTranslatedString(IDS_SECURITY_METHOD_NAME, method_name_.get());
    MultiLang::LoadTranslatedString(IDS_SECURITY_METHOD_TITLE, method_title_.get());
    MultiLang::LoadTranslatedString(IDS_SECURITY_METHOD_DESCRIPTION, method_description_.get());
    
    std::memset(sm_callbacks_.get(), 0, sizeof(FR_SecurityMethodCallbacksRec));

    sm_callbacks_->lStructSize = sizeof(FR_SecurityMethodCallbacksRec);
    sm_callbacks_->clientData = nullptr;

    sm_callbacks_->FRSecurityMethodGetName = OnSecurityMethodGetName;
    sm_callbacks_->FRSecurityMethodGetTitle = OnSecurityMethodGetTitle;
    sm_callbacks_->FRSecurityMethodIsMyMethod = OnSecurityMethodIsMyMethod;
    sm_callbacks_->FRSecurityMethodCanBeModified = OnSecurityMethodCanBeModified;
    sm_callbacks_->FRSecurityMethodRemoveSecurityInfo = OnSecurityMethodRemoveSecurityInfo;
    sm_callbacks_->FRSecurityMethodGetShowIco = OnSecurityMethodGetShowIco;
    sm_callbacks_->FRSecurityMethodCheckModuleLicense = OnSecurityMethodCheckModuleLicense;
    sm_callbacks_->FRSecurityMethodDoSetting = OnSecurityMethodDoSetting;
    sm_callbacks_->FRSecurityMethodDescription = OnSecurityMethodDescription;
    sm_callbacks_->FRSecurityMethodCreatePermSubDlg = OnSecurityMethodCreatePermSubDlg;
    sm_callbacks_->FRSecurityMethodDestroyPermSubDlg = OnSecurityMethodDestroyPermSubDlg;
}
// dtors
SecurityMethodProvider::~SecurityMethodProvider() {

}

// methods
FS_LPWSTR SecurityMethodProvider::OnSecurityMethodGetName(FS_LPVOID clientData) {
    return theApp.sm_provider_->GetMethodName();
}

FS_LPWSTR SecurityMethodProvider::OnSecurityMethodGetTitle(FS_LPVOID clientData) {
    return theApp.sm_provider_->GetMethodTitle();
}

FS_BOOL SecurityMethodProvider::OnSecurityMethodIsMyMethod(FS_LPVOID clientData, FR_Document doc) {
    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(doc);

    if (fasoo_doc && !fasoo_doc->IsSecurityRemoved()) 
        return TRUE;
    
    return FALSE;
}

FS_BOOL SecurityMethodProvider::OnSecurityMethodCanBeModified(FS_LPVOID clientData) {
    FR_Document fr_doc = FRAppGetActiveDocOfPDDoc();
    if (!fr_doc)
        return FALSE;

    FasooDocument* doc = theApp.doc_manager_->GetFasooDoc(fr_doc);
    if (!doc)
        return FALSE;

    HCONTENT doc_content = doc->GetContent();
    if (ADKIsLicenseValid(doc_content, ADK_PURPOSE_SAVE)) // remove Fasoo protection (security) and use another security type
        return TRUE;

    return FALSE;
}

FS_BOOL SecurityMethodProvider::OnSecurityMethodRemoveSecurityInfo(FS_LPVOID clientData) {
    FR_Document fr_doc = FRAppGetActiveDocOfPDDoc();
    if (!fr_doc)
        return FALSE;

    FasooDocument* doc = theApp.doc_manager_->GetFasooDoc(fr_doc);
    if (!doc)
        return FALSE;

    HCONTENT content = doc->GetContent();
    if (!content)
        return FALSE;

    if (!ADKIsLicenseValid(content, ADK_PURPOSE_SAVE))
        return FALSE;

    bool ret = doc->RemoveSecurity();
    
    if (ret) {
        theApp.HideInfoBulb(fr_doc);
        FRDocSetDocEncrypted(fr_doc, FALSE);        
        FRDocSetChangeMark(fr_doc);
    }

    return ret;
}

FS_DIBitmap SecurityMethodProvider::OnSecurityMethodGetShowIco(FS_LPVOID clientData, FS_FLOAT dbScale) {
    return FSDIBitmapLoadFromPNGIcon3(AfxGetInstanceHandle(), (FS_LPCWSTR)MAKEINTRESOURCE(FASOODRM_ICON_32));
}

FS_BOOL SecurityMethodProvider::OnSecurityMethodCheckModuleLicense(FS_LPVOID clientData) {
    return TRUE;
}

void SecurityMethodProvider::OnSecurityMethodDoSetting(FS_LPVOID clientData, HWND hWnd, FS_BOOL* bSuc) {
    MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_FASOODRM_DO_SETTINGS, MB_OK | MB_ICONINFORMATION);
    
    *bSuc = FALSE;
}

FS_LPWSTR SecurityMethodProvider::OnSecurityMethodDescription(FS_LPVOID clientData) {
    return theApp.sm_provider_->GetMethodDescription();
}

HWND SecurityMethodProvider::OnSecurityMethodCreatePermSubDlg(FS_LPVOID clientData, HWND hParent) {
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    if (FRAppIsRibbonMode()) {
        FR_Document fr_doc = FRAppGetActiveDocOfPDDoc();
        if (!fr_doc)
            return NULL;

        FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(fr_doc);
        if (!fasoo_doc)
            return NULL;

        SecurityPropertiesDlg* dlg = fasoo_doc->GetSecurityPropertiesDlg();
        if (dlg) {
            // Fix: probably bug in Phantom. Handle of window is somewhere deleted, so we must recreate dlg againg
            auto dlg_hwnd = dlg->GetSafeHwnd();

            if (dlg_hwnd == NULL) {
                dlg->DestroyWindow();
                delete dlg;
            }
            else
                return dlg_hwnd;
        }
        
        dlg = new SecurityPropertiesDlg(fasoo_doc->GetContent(), CWnd::FromHandle(hParent));
        if (!dlg)
            return NULL;

        if (!dlg->Create(IDD_PERMS_SHOW_DLG, CWnd::FromHandle(hParent))) {
            delete dlg;
            return NULL;
        }

        fasoo_doc->SetSecurityPropertiesDlg(dlg);
        return dlg->GetSafeHwnd();
    }

    return NULL;
}

void SecurityMethodProvider::OnSecurityMethodDestroyPermSubDlg(FS_LPVOID clientData, HWND hWnd) {
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    if (FRAppIsRibbonMode()) {
        FR_Document fr_doc = FRAppGetActiveDocOfPDDoc();
        if (!fr_doc)
            return;

        FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(fr_doc);
        if (!fasoo_doc)
            return;

        SecurityPropertiesDlg* dlg = fasoo_doc->GetSecurityPropertiesDlg();
        if (dlg) {
            dlg->DestroyWindow();
            delete dlg;
            fasoo_doc->SetSecurityPropertiesDlg(nullptr);
        }
    }
}
