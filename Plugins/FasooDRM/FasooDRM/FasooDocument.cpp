#include "stdafx.h"
#include "FasooDocument.h"

// ctor
FasooDocument::FasooDocument(const std::wstring& path, HCONTENT content, std::unique_ptr<char[]>&& data, std::size_t data_size)
    : doc_path_(path)
    , hcontent_(content)
    , data_(std::move(data))
    , data_size_(data_size) 
{
}

// dtor
FasooDocument::~FasooDocument() {
    if (hcontent_) {
        ADKCloseContent(hcontent_);
        hcontent_ = NULL;
    }
}

void FasooDocument::UpdateData(char* data, std::size_t data_size) {
    data_.reset(new char[data_size]);
    data_size_ = data_size;

    std::memcpy(data_.get(), data, data_size_);
}

bool FasooDocument::RemoveSecurity() {
    if (!ADKSetUsage(hcontent_, ADK_PURPOSE_SAVE))
        return false;

    if (!ADKCloseContent(hcontent_)) 
        return false;

    hcontent_ = 0;
    security_removed_ = true;
    perms_ = -1;

    return true;    
}

bool FasooDocument::IsPrintEnabled() const {
    if (ADKIsLicenseValid(hcontent_, ADK_PURPOSE_PRINT) || ADKIsLicenseValid(hcontent_, ADK_PURPOSE_SECURE_PRINT))
        return true;

    return false;
}
