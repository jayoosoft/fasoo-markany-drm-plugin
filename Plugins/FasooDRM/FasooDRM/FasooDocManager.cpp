#include "stdafx.h"
#include "FasooDocManager.h"
#include "Logger.h"

#include <sstream>
// ctors

// dtor

// methods
void FasooDocManager::AddFasooDoc(const std::wstring&path, HCONTENT content, std::unique_ptr<char[]>&& data, std::size_t data_size) {
    this->unopened_doc_.reset(new FasooDocument(path, content, std::move(data), data_size));
}

void FasooDocManager::RemoveFasooDoc(FR_Document fr_doc) {
    docs_.erase(fr_doc);
}

const FasooDocument* FasooDocManager::UpdateFasooDoc(const FS_LPCWSTR path, FR_Document fr_doc)
{
    if (!this->unopened_doc_)
        return nullptr;

    assert(this->unopened_doc_->GetDocPath() == path);
    assert(docs_.find(fr_doc) == docs_.end());

    const auto& ins_pair = docs_.emplace(fr_doc, std::move(this->unopened_doc_)).first;
    return ins_pair->second.get();
}

const FasooDocument* FasooDocManager::GetFasooDoc(FR_Document fr_doc) const {
    const auto& doc = docs_.find(fr_doc);
    return doc != docs_.end() ? doc->second.get() : nullptr;
}

FasooDocument* FasooDocManager::GetFasooDoc(FR_Document fr_doc) {
    auto& doc = docs_.find(fr_doc);
    return doc != docs_.end() ? doc->second.get() : nullptr;
}

const FasooDocument* FasooDocManager::GetFasooDoc(const FS_WideString path) const {
    for (const auto& it : docs_) {
        //FR_Document fr_doc = it.first;
        std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> doc_path_ptr(FSWideStringNew(), FSWideStringDestroy);
        FS_WideString doc_path = doc_path_ptr.get();

        FRDocGetCreateDocSourceFilePath(it.first, &doc_path);

        if (FSWideStringCompare2(doc_path, path) == 0) {
            return it.second.get();
        }
    }

    return nullptr;
}

FasooDocument* FasooDocManager::GetFasooDoc(const FS_WideString path){
    for (const auto& it : docs_) {
        std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> doc_path_ptr(FSWideStringNew(), FSWideStringDestroy);
        FS_WideString doc_path = doc_path_ptr.get();

        FRDocGetCreateDocSourceFilePath(it.first, &doc_path);

        if (FSWideStringCompare2(doc_path, path) == 0) {
            return it.second.get();
        }
    }

    return nullptr;
}
