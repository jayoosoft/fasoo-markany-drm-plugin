#include "stdafx.h"
#include "PageOrganizeProvider.h"
#include "FasooDRM.h"
#include "FasooDocument.h"
#include "FasooHelpers.h"
#include "MultiLang.h"

extern CFasooDRMApp theApp;

PageOrganizeProvider::PageOrganizeProvider()
    : page_organize_callbacks_(std::make_unique< FR_POEventCallbacksRec>())
{
    page_organize_callbacks_->lStructSize = sizeof(FR_POEventCallbacksRec);

    page_organize_callbacks_->FRPOOnAfterExtractPageEx = OnAfterExtractPageEx;
}

void PageOrganizeProvider::OnAfterExtractPageEx(FS_LPVOID clientData, FR_Document frDoc, FS_WordArray arrSrcPages, const FS_WideString& wsDestfilepath)
{
    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(frDoc);
    if (!fasoo_doc)
        return;

    if (fasoo_doc->GetPermissions() == -1)
        return;
    
    if (!FasooEncryptFile(FSWideStringCastToLPCWSTR(wsDestfilepath), fasoo_doc->GetContent()))
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_EXTRACT_PAGES, MB_OK | MB_ICONERROR);
}
