// SecurityPropertiesDlg.cpp : implementation file
//

#include "StdAfx.h"
#include "afxdialogex.h"
#include "resource.h"

#include "FasooDRM.h"
#include "MultiLang.h"
#include "SecurityPropertiesDlg.h"

#include <memory>

extern CFasooDRMApp theApp;

// SecurityPropertiesDlg dialog

IMPLEMENT_DYNAMIC(SecurityPropertiesDlg, CDialogEx)

SecurityPropertiesDlg::SecurityPropertiesDlg(HCONTENT content, CWnd* pParent /*=nullptr*/)
    : content_{ content }, 
    CDialogEx(IDD_PERMS_SHOW_DLG, pParent)
{

}

SecurityPropertiesDlg::~SecurityPropertiesDlg()
{
    ReleaseRibbonStatics();
}

BOOL SecurityPropertiesDlg::OnInitDialog() {
    CDialogEx::OnInitDialog();

    HWND hwnd = GetSafeHwnd();

    FRLanguageTranslateDialog(theApp.fr_lang_.get(), hwnd, (FS_LPCWSTR)MAKEINTRESOURCE(IDD_PERMS_SHOW_DLG));
    FRSysInstallDialogSkinTheme(hwnd);

    SetRibbonStatics();
    LoadStaticStrings();
    LoadPermissions();

    return TRUE;
}

void SecurityPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

void SecurityPropertiesDlg::SetRibbonStatics() {
    summary_ = SetLargeFontStatic(IDC_STATIC_SUMMARY);
    view_doc_ = SetSmallFontStatic(IDC_STATIC_VIEW_DOC);
    view_doc_perm_ = SetSmallFontStatic(IDC_STATIC_VIEW_DOC_PERM);
    save_doc_ = SetSmallFontStatic(IDC_STATIC_SAVE_DOC);
    save_doc_perm_ = SetSmallFontStatic(IDC_STATIC_SAVE_DOC_PERM);
    edit_doc_ = SetSmallFontStatic(IDC_STATIC_EDIT_DOC);
    edit_doc_perm_ = SetSmallFontStatic(IDC_STATIC_EDIT_DOC_PERM);
    doc_extraction_ = SetSmallFontStatic(IDC_STATIC_DOC_EXTRACTION);
    doc_extraction_perm_ = SetSmallFontStatic(IDC_STATIC_DOC_EXTRACTION_PERM);
    printing_ = SetSmallFontStatic(IDC_STATIC_PRINT);
    printing_perm_ = SetSmallFontStatic(IDC_STATIC_PRINT_PERM);
    print_screen_ = SetSmallFontStatic(IDC_STATIC_PRINTSCREEN);
    print_screen_perm_ = SetSmallFontStatic(IDC_STATIC_PRINTSCREEN_PERM);
    transfer_ = SetSmallFontStatic(IDC_STATIC_TRANSFER_DOC);
    transfer_perm_ = SetSmallFontStatic(IDC_STATIC_TRANSFER_DOC_PERM);
    secure_save_ = SetSmallFontStatic(IDC_STATIC_SECURE_SAVE);
    secure_save_perm_ = SetSmallFontStatic(IDC_STATIC_SECURE_SAVE_PERM);
    secure_print_ = SetSmallFontStatic(IDC_STATIC_SECURE_PRINT_DOC);
    secure_print_perm_ = SetSmallFontStatic(IDC_STATIC_SECURE_PRINT_DOC_PERM);
}

void SecurityPropertiesDlg::LoadStaticStrings() {
    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> fs_str_ptr(FSWideStringNew(), FSWideStringDestroy);
    FS_WideString fs_str = fs_str_ptr.get();

    MultiLang::LoadTranslatedString(IDS_DOC_RESTRICTIONS_SUMMARY, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_SUMMARY)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));
    
    MultiLang::LoadTranslatedString(IDS_VIEW_DOCUMENT, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_VIEW_DOC)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));

    MultiLang::LoadTranslatedString(IDS_SAVE_DOCUMENT, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_SAVE_DOC)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));

    MultiLang::LoadTranslatedString(IDS_EDIT_DOCUMENT, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_EDIT_DOC)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));

    MultiLang::LoadTranslatedString(IDS_DOCUMENT_EXTRACTION, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_DOC_EXTRACTION)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));

    MultiLang::LoadTranslatedString(IDS_PRINTING, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_PRINT)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));

    MultiLang::LoadTranslatedString(IDS_PRINTSCREEN, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_PRINTSCREEN)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));

    MultiLang::LoadTranslatedString(IDS_TRANSFER, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_TRANSFER_DOC)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));

    MultiLang::LoadTranslatedString(IDS_SECURE_SAVE, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_SECURE_SAVE)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));

    MultiLang::LoadTranslatedString(IDS_SECURE_PRINT, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_SECURE_PRINT_DOC)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));
}

void SecurityPropertiesDlg::LoadPermissions() {
    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> allowed_str(FSWideStringNew(), FSWideStringDestroy);
    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> not_allowed_str(FSWideStringNew(), FSWideStringDestroy);

    FS_WideString allowed = allowed_str.get();
    FS_WideString not_allowed = not_allowed_str.get();

    MultiLang::LoadTranslatedString(IDS_ALLOWED, allowed);
    MultiLang::LoadTranslatedString(IDS_NOT_ALLOWED, not_allowed);

    if (ADKIsLicenseValid(content_, ADK_PURPOSE_VIEW))
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_VIEW_DOC_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(allowed));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_VIEW_DOC_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(not_allowed));

    if (ADKIsLicenseValid(content_, ADK_PURPOSE_SAVE))
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_SAVE_DOC_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(allowed));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_SAVE_DOC_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(not_allowed));

    if (ADKIsLicenseValid(content_, ADK_PURPOSE_EDIT))
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_EDIT_DOC_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(allowed));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_EDIT_DOC_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(not_allowed));

    if (ADKIsLicenseValid(content_, ADK_PURPOSE_EXTRACT))
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_DOC_EXTRACTION_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(allowed));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_DOC_EXTRACTION_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(not_allowed));

    if (ADKIsLicenseValid(content_, ADK_PURPOSE_PRINT))
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_PRINT_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(allowed));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_PRINT_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(not_allowed));

    if (ADKIsLicenseValid(content_, ADK_PURPOSE_PRINT_SCREEN))
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_PRINTSCREEN_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(allowed));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_PRINTSCREEN_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(not_allowed));

    if (ADKIsLicenseValid(content_, ADK_PURPOSE_TRANSFER))
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_TRANSFER_DOC_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(allowed));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_TRANSFER_DOC_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(not_allowed));

    if (ADKIsLicenseValid(content_, ADK_PURPOSE_SECURE_SAVE))
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_SECURE_SAVE_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(allowed));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_SECURE_SAVE_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(not_allowed));

    if (ADKIsLicenseValid(content_, ADK_PURPOSE_SECURE_PRINT))
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_SECURE_PRINT_DOC_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(allowed));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_SECURE_PRINT_DOC_PERM)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(not_allowed));
}

void SecurityPropertiesDlg::ReleaseRibbonStatics() {
    if (summary_)
        FRRibbonStyleStaticRelease(summary_);

    if (view_doc_)
        FRRibbonStyleStaticRelease(view_doc_);

    if (view_doc_perm_)
        FRRibbonStyleStaticRelease(view_doc_perm_);

    if (save_doc_)
        FRRibbonStyleStaticRelease(save_doc_);

    if (save_doc_perm_)
        FRRibbonStyleStaticRelease(save_doc_perm_);

    if (edit_doc_)
        FRRibbonStyleStaticRelease(edit_doc_);

    if (edit_doc_perm_)
        FRRibbonStyleStaticRelease(edit_doc_perm_);

    if (doc_extraction_)
        FRRibbonStyleStaticRelease(doc_extraction_);

    if (doc_extraction_perm_)
        FRRibbonStyleStaticRelease(doc_extraction_perm_);

    if (printing_)
        FRRibbonStyleStaticRelease(printing_);

    if (printing_perm_)
        FRRibbonStyleStaticRelease(printing_perm_);

    if (print_screen_)
        FRRibbonStyleStaticRelease(print_screen_);

    if (print_screen_perm_)
        FRRibbonStyleStaticRelease(print_screen_perm_);

    if (transfer_)
        FRRibbonStyleStaticRelease(transfer_);

    if (transfer_perm_)
        FRRibbonStyleStaticRelease(transfer_perm_);

    if (secure_save_)
        FRRibbonStyleStaticRelease(secure_save_);

    if (secure_save_perm_)
        FRRibbonStyleStaticRelease(secure_save_perm_);

    if (secure_print_)
        FRRibbonStyleStaticRelease(secure_print_);

    if (secure_print_perm_)
        FRRibbonStyleStaticRelease(secure_print_perm_);
}

FR_RibbonStyleStatic SecurityPropertiesDlg::SetSmallFontStatic(UINT static_id) {
    FR_RibbonStyleStatic ribbonStatic = (FR_RibbonStyleStatic)FRRibbonBackStageViewItemModifiedToRibbonStyleButton(GetDlgItem(static_id), static_id, FR_RibbonStyle_Static, this);
    FRRibbonStyleStaticSetFontStyle(ribbonStatic, 12, FALSE, FALSE, FALSE, FALSE);
    FRRibbonStyleStaticSetTextColor(ribbonStatic, RGB(60, 60, 60));
    FRRibbonStyleStaticSetTextDrawFormat(ribbonStatic, DT_WORDBREAK);
    
    return ribbonStatic;
}

FR_RibbonStyleStatic SecurityPropertiesDlg::SetLargeFontStatic(UINT static_id) {
    FR_RibbonStyleStatic ribbonStatic = (FR_RibbonStyleStatic)FRRibbonBackStageViewItemModifiedToRibbonStyleButton(GetDlgItem(static_id), static_id, FR_RibbonStyle_Static, this);
    FRRibbonStyleStaticSetFontStyle(ribbonStatic, 14, FALSE, FALSE, FALSE, FALSE);
    FRRibbonStyleStaticSetTextColor(ribbonStatic, RGB(50, 50, 50));
 
    return ribbonStatic;
}

BEGIN_MESSAGE_MAP(SecurityPropertiesDlg, CDialogEx)
    
END_MESSAGE_MAP()
