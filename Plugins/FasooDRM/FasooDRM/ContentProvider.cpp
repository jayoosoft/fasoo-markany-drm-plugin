#include "stdafx.h"
#include "resource.h"
#include "FasooDRM.h"
#include "ContentProvider.h"
#include "FasooDocManager.h"
#include "FasooDocument.h"
#include "MultiLang.h"

extern CFasooDRMApp theApp;

// ctors
ContentProvider::ContentProvider() : cp_callbacks_(new FR_ContentProviderCallbacksRec) {
    std::memset(cp_callbacks_.get(), 0, sizeof(FR_ContentProviderCallbacksRec));

    cp_callbacks_->lStructSize = sizeof(FR_ContentProviderCallbacksRec);
    cp_callbacks_->clientData = nullptr;

    cp_callbacks_->FRConProviderOnFileOpen = OnFileOpen;
    cp_callbacks_->FRConProviderOnFileClose = OnFileClose;
    cp_callbacks_->FRConProviderOnGetContentSize = OnGetContentSize;
    cp_callbacks_->FRConProviderOnReadContent = OnReadContent;
    cp_callbacks_->FRConProviderOnWriteContent = OnWriteContent;
    cp_callbacks_->FRConProviderOnGetPermissions = OnGetPermissions;
    cp_callbacks_->FRConProviderCanBeSaved = OnCanBeSaved;
    cp_callbacks_->FRConProviderOnBackFillContent = OnBackFillContent;
}

// dtor
ContentProvider::~ContentProvider() {
    
}

// methods
FS_BOOL ContentProvider::OnFileOpen(FS_LPVOID clientData, FR_Document doc, FS_LPCWSTR lpszSource, FS_BOOL bIsAttachment) {
    const FasooDocument* fasoo_doc = theApp.doc_manager_->UpdateFasooDoc(lpszSource, doc);
    if (!fasoo_doc) {
        AppEventProvider::OnOpenDocument(clientData, lpszSource);
        fasoo_doc = theApp.doc_manager_->UpdateFasooDoc(lpszSource, doc);
        if (!fasoo_doc)
            return FALSE;
    }
    
    // enable/disable Snapshot tool
    if (!ADKIsLicenseValid(fasoo_doc->GetContent(), ADK_PURPOSE_PRINT_SCREEN)) {
        FRDocSetMenuEnableByName(doc, FR_MENU_ENABLE_SNAPSHOT, FALSE);        
    }

    return TRUE;
}

void ContentProvider::OnFileClose(FS_LPVOID clientData, FR_Document doc) {
    theApp.doc_manager_->RemoveFasooDoc(doc);
}

FS_BOOL ContentProvider::OnGetContentSize(FS_LPVOID clientData, /*IN*/ FR_Document doc,/*OUT*/ unsigned long * pTotalSize)
{
    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(doc);

    if (!fasoo_doc)
        return FALSE;

    *pTotalSize = fasoo_doc->GetContentSize();
    return TRUE;
}

FS_BOOL ContentProvider::OnReadContent(FS_LPVOID clientData, /*IN*/ FR_Document doc, /*IN*/ DWORD pos, /*OUT*/ unsigned char * pBuf, /*IN*/ unsigned long size)
{
    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(doc);

    if (!fasoo_doc) 
        return FALSE;
    

    const char* content = fasoo_doc->GetContentBuffer();

    if (content) {
        memcpy(pBuf, content + pos, size);
    }

    return TRUE;
}

FS_BOOL ContentProvider::OnWriteContent(FS_LPVOID clientData, /*IN*/ FR_Document doc, /*IN*/ unsigned char * pBuf, /*IN*/ unsigned long size, /*IN*/ FS_LPCWSTR lpSaveFilePath) {
    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(doc);

    if (!fasoo_doc) {
        return FALSE; // not secured doc
    }

    FS_BOOL ret = TRUE;
    if (!fasoo_doc->IsSecurityRemoved()) {
        std::wstring p{ lpSaveFilePath };

        auto pos = p.find(L"~~$");
        if (pos == std::wstring::npos)
            ret = SaveProtectedContentAs(fasoo_doc, lpSaveFilePath, pBuf, size);
        else {
            ret = SaveProtectedContent(fasoo_doc, lpSaveFilePath, pBuf, size);
        }

        fasoo_doc->UpdateData(reinterpret_cast<char*> (pBuf), size);
    }
    else {
        CFile file(lpSaveFilePath, CFile::modeReadWrite | CFile::typeBinary | CFile::modeCreate);
        try {
            file.Write(pBuf, size);
        }
        catch (...) {
            ret = FALSE;
        }

        file.Close();
    }

    return ret;
}

unsigned long ContentProvider::OnGetPermissions(FS_LPVOID clientData, /*IN*/ FR_Document doc, /*IN*/ unsigned long pdfselfPermissions) {
    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(doc);
    
    if (!fasoo_doc) {
        return -1;
    }

    auto perms = fasoo_doc->GetPermissions();
    if (perms != 0)
        return perms;

    HCONTENT content = fasoo_doc->GetContent();
    perms = -1;

    if (!ADKIsLicenseValid(content, ADK_PURPOSE_VIEW))
        perms &= (~0x1); // view

    if (!ADKIsLicenseValid(content, ADK_PURPOSE_SAVE))
        perms &= (~0x2); // change security

    if (!ADKIsLicenseValid(content, ADK_PURPOSE_EDIT))
        perms &= ~(FR_PERM_MODIFY_CONTENT | FR_PERM_ANNOTATE | FR_PERM_FILL_FORM);

    if (!ADKIsLicenseValid(content, ADK_PURPOSE_EXTRACT))
        perms &= (~FR_PERM_EXTRACT_COPY | FR_PERM_ASSEMBLE);

    if (!(ADKIsLicenseValid(content, ADK_PURPOSE_PRINT)))   // PLUGINRD-82: temporary disabled print if only watermark print is enabled || ADKIsLicenseValid(content, ADK_PURPOSE_SECURE_PRINT))) {
        perms &= ~(FR_PERM_PRINT | FR_PERM_PRINT_HIGN);

    fasoo_doc->SetPermissions(perms);

    return perms;
}

// TODO: clientData: use SAVE or SECURE_SAVE (determine from UI)
FS_BOOL ContentProvider::OnCanBeSaved(FS_LPVOID clientData, FR_Document frDoc) {
    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(frDoc);

    if (!fasoo_doc) {
        return true; // Not secured doc
    }

    if (!fasoo_doc->IsSecurityRemoved()) {
        auto can_save = fasoo_doc->CanBeSaved();

        if (!can_save.has_value()) {
            HCONTENT content = fasoo_doc->GetContent();

            bool s = ADKIsLicenseValid(content, ADK_PURPOSE_SECURE_SAVE);
            fasoo_doc->SetCanBeSaved(s);

            if (!s) {
                MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_SAVE_NOT_ALLOWED, MB_OK | MB_ICONERROR);                
            }

            return s;
        }

        return can_save.value();
    }

    return TRUE;
}

FS_BOOL ContentProvider::OnBackFillContent(FS_LPVOID clientData, FR_Document doc, unsigned char* pBuf, unsigned long size, FS_LPCWSTR lpSaveFilePath) {
    // in this callback we won't use ADKSetUsage(), becasue it was called by another callback (OnWriteContent);
    FasooDocument* fasoo_doc = theApp.doc_manager_->GetFasooDoc(doc);

    if (!fasoo_doc)
        return FALSE;

    HCONTENT content = ADKOpenContent(lpSaveFilePath, TRUE, TRUE);
    HCONTENT content_orig = ADKOpenContent(fasoo_doc->GetDocPath().c_str(), FALSE, FALSE);
    if (!content || !content_orig)
        return FALSE;
    
    if (!ADKPackContent(content, content_orig)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);

        ADKCloseContent(content);
        ADKCloseContent(content_orig);
        return FALSE;
    }

    DWORD written_bytes = 0;
    if (!ADKWriteContent(content, pBuf, size, &written_bytes)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);

        ADKCloseContent(content);
        ADKCloseContent(content_orig);
        return FALSE;
    }

    if (!ADKSetEndOfContent(content)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);

        ADKCloseContent(content);
        ADKCloseContent(content_orig);
        DeleteFile(lpSaveFilePath);

        return FALSE;
    }

    ADKCloseContent(content);
    ADKCloseContent(content_orig);
    
    // reopen file in read-only mode
    /*content = ADKOpenContent(fasoo_doc->GetDocPath().c_str(), FALSE, FALSE);
    if (!content) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);

        ADKCloseContent(content);
        DeleteFile(lpSaveFilePath);
        return FALSE;
    }

    fasoo_doc->SetContent(content);/*/
 
    return TRUE;
}

FS_BOOL ContentProvider::SaveProtectedContentAs(FasooDocument* fasoo_doc, FS_LPCWSTR file_path, unsigned char* buf, unsigned long size) {
    HCONTENT save_content = ADKOpenContent(file_path, TRUE, TRUE);

    if (!save_content) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
        return FALSE;
    }

    if (!ADKPackContent(save_content, fasoo_doc->GetContent())) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
        
        ADKCloseContent(save_content);
        return FALSE;
    }

    DWORD written_bytes = 0;
    if (!ADKWriteContent(save_content, buf, size, &written_bytes)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
        
        ADKCloseContent(save_content);
        DeleteFile(file_path);

        return FALSE;
    }

    if (!ADKSetEndOfContent(save_content)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
       
        ADKCloseContent(save_content);
        DeleteFile(file_path);

        return FALSE;
    }

    if (!ADKSetUsage(save_content, ADK_PURPOSE_SECURE_SAVE)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
        
        ADKCloseContent(save_content);
        DeleteFile(file_path);

        return FALSE;
    }

    ADKCloseContent(save_content);
    return TRUE;
}

FS_BOOL ContentProvider::SaveProtectedContent(FasooDocument* fasoo_doc, FS_LPCWSTR file_path, unsigned char* buf, unsigned long size) {
    HCONTENT content_backup = fasoo_doc->GetContent();

    HCONTENT content = ADKOpenContent(file_path, TRUE, TRUE);

    if (!content) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
        
        return FALSE;
    }

    if (!ADKPackContent(content, fasoo_doc->GetContent())) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
        
        ADKCloseContent(content);
        return FALSE;
    }

    DWORD written_bytes = 0;
    if (!ADKWriteContent(content, buf, size, &written_bytes)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
        
        ADKCloseContent(content);
        return FALSE;
    }

    if (!ADKSetEndOfContent(content)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
        
        ADKCloseContent(content);
        return FALSE;
    }

   if (!ADKSetUsage(content, ADK_PURPOSE_SECURE_SAVE)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
        
        ADKCloseContent(content);
        return FALSE;
    }
    
    ADKCloseContent(fasoo_doc->GetContent());
    fasoo_doc->SetContent(NULL);
    ADKCloseContent(content);
   
    return TRUE;
}
