﻿# FasooDRM plugin
## Compiling
1. Follow the instruction in [Fasoo
DRM插件相关信息说明](https://confluence.foxitsoftware.com/pages/viewpage.action?pageId=77904687)
and install Fasoo DRM client. Here you can find also couple of informations how
the system works.
2. Download the source codes:

    ```
    git clone git@source.foxitsoftware.cn:foxit/phantom-plugin-sdk.git
    cd phantom-plugin-sdk/
    git checkout feature/fasoo
    ```

3. Copy [f_extadk.dll](FasooDRM_SDK/x86/f_extadk.dll),
[f_extadk_0161.fac](FasooDRM_SDK/f_extadk_0161.fac), and
[f_extadk_0168.fac](FasooDRM_SDK/f_extadk_0161.fac) to the *c:\Windows\SysWOW64*
4. Open [FasooDRM.sln](FasooDRM.sln) or [AllPlugins.sln](../All/AllPlugins.sln) and
recompile project FasooDRM
5. Run *regedit.msc* and remove *HKEY_CURRENT_USER\Software\Foxit Software\Foxit
PhantomPDF 10.0\plugins\Installed\FasooDRM*
6. Install the plugin to the PhantomPDF (**Help** -> **Foxit Plug-Ins**) from
*FasooDRM\bin\FasooDRM\Win32\Debug\v141* folder

## devops
1. Push the code to the *feature/fasoo* branch
2. Create a merge request to *develop* branch and merge the code there
3. Run job
[http://10.103.3.133:8080/view/phantom-plugins/job/phantom-plugins-FasooDRM-vs2017]
on Jenkins server
4. Artifacts will automatically uploads to the [Conan
server](http://conan.foxitsoftware.com:8081/artifactory/webapp/#/artifacts/browse/tree/General/normex-repo-local/foxit/FasooDRM)