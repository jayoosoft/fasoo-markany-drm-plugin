#ifndef _F_EXTADK_H_
#define _F_EXTADK_H_

typedef DWORD_PTR HCONTENT, * PHCONTENT, * LPHCONTENT;

// for ADKGetContentSize
#define INVALID_CONTENT_SIZE		(DWORD)0xFFFFFFFF	// INVALID_FILE_SIZE

// for ADKSetContentPointer
#define INVALID_SET_CONTENT_POINTER (DWORD)-1			// INVALID_SET_FILE_POINTER
#define SCP_BEGIN					0					// FILE_BEGIN
#define SCP_CURRENT					1					// FILE_CURRENT
#define SCP_END						2					// FILE_END

typedef BOOL (__stdcall * pfnADKInitialize)			();
typedef BOOL (__stdcall * pfnADKInitializeAndCert)		();
typedef BOOL (__stdcall * pfnADKInitializeContentRights)(HCONTENT hContent);
typedef BOOL (__stdcall * pfnADKIsSecureContentByPath)(LPCWSTR pwszFilePath);
typedef DWORD (__stdcall * pfnADKGetContentTypeByPath)(LPCWSTR pwszFilePath);
typedef DWORD (__stdcall * pfnADKGetDomainIdByPath)(LPCWSTR pwszFilePath, LPWSTR pwszDomainId, UINT cchDomainId);
typedef BOOL (__stdcall * pfnADKIsLicenseValidByPath)(LPCWSTR pwszFilePath, WORD wPurpose);
typedef HCONTENT (__stdcall * pfnADKOpenContent)	(LPCWSTR pwszFilePath, BOOL bWritable, BOOL bTruncate);
typedef HCONTENT (__stdcall * pfnADKDuplicateContentHandle)(HCONTENT hContent);
typedef BOOL  (__stdcall * pfnADKIsSecureContent)	(HCONTENT hContent);
typedef DWORD (__stdcall * pfnADKGetContentType)	(HCONTENT hContent);
typedef DWORD (__stdcall * pfnADKGetDomainId)		(HCONTENT hContent, LPWSTR pwszDomainId, UINT cchDomainId);
typedef BOOL  (__stdcall * pfnADKPackContent)		(HCONTENT hContentTarget, HCONTENT hContentTemplate);
typedef BOOL  (__stdcall * pfnADKSetUsage)			(HCONTENT hContent, WORD wPurpose);
typedef BOOL  (__stdcall * pfnADKIsLicenseValid)	(HCONTENT hContent, WORD wPurpose);
typedef DWORD  (__stdcall * pfnADKGetContentSize)	(HCONTENT hContent);
typedef DWORD  (__stdcall * pfnADKSetContentPointer)(HCONTENT hContent, LONG lDistanceToMove, DWORD dwMoveMethod);
typedef BOOL  (__stdcall * pfnADKReadContent)		(HCONTENT hContent, LPVOID lpBuffer, DWORD dwNumberOfBytesToRead, LPDWORD lpdwNumberOfBytesRead);
typedef BOOL  (__stdcall * pfnADKBlockCapture)		(HCONTENT hContent, HWND hwnd, BOOL bSet);
typedef BOOL  (__stdcall * pfnADKWriteContent)		(HCONTENT hContent, LPCVOID lpBuffer, DWORD dwNumberOfBytesToWrite, LPDWORD lpdwNumberOfBytesWritten);
typedef BOOL  (__stdcall * pfnADKSetEndOfContent)	(HCONTENT hContent);
typedef BOOL  (__stdcall * pfnADKStartPrint)		(HCONTENT hContent);
typedef BOOL  (__stdcall * pfnADKEndPrint)			(HCONTENT hContent, LPBOOL pbSecurePrinted);
typedef BOOL  (__stdcall * pfnADKCloseContent)		(HCONTENT hContent);
typedef VOID  (__stdcall * pfnADKSetLastError)		(DWORD dwError);
typedef DWORD (__stdcall * pfnADKGetLastError)		();
typedef DWORD (__stdcall * pfnADKGetCID)			(HCONTENT hContent,LPSTR pszCID,LPDWORD lpcchCID);
typedef BOOL (__stdcall * pfnADKDeleteLicenseFile)	(LPCWSTR pwszFilePath);
typedef BOOL (__stdcall * pfnADKInitializeEx) (LPCWSTR pwszPC);
typedef DWORD (__stdcall * pfnADKIsVirtualPrinter) (HANDLE hPrinter, LPWSTR pwszPrinterName, LPWSTR pwszDriverName);
typedef BOOL (__stdcall * pfnADKIsLogonStatus) (LPCWSTR pwszDomainId);
typedef BOOL(__stdcall * pfnADKAutoPackContent) (LPCWSTR pwcsFilePath);
typedef BOOL(__stdcall * pfnADKIsAutoPackProcess) (LPCWSTR pwcsProcessPath);

BOOL __stdcall ADKInitialize();
BOOL __stdcall ADKInitializeAndCert();
BOOL __stdcall ADKInitializeContentRights(HCONTENT hContent);
BOOL __stdcall ADKIsSecureContentByPath(LPCWSTR pwszFilePath);
DWORD __stdcall ADKGetContentTypeByPath(LPCWSTR pwszFilePath);
DWORD __stdcall ADKGetDomainIdByPath(LPCWSTR pwszFilePath, LPWSTR pwszDomainId, UINT cchDomainId);
BOOL __stdcall ADKIsLicenseValidByPath(LPCWSTR pwszFilePath, WORD wPurpose);
HCONTENT __stdcall ADKOpenContent(LPCWSTR pwszFilePath, BOOL bWritable, BOOL bTruncate);
HCONTENT __stdcall ADKDuplicateContentHandle(HCONTENT hContent);
BOOL __stdcall ADKIsSecureContent(HCONTENT hContent);
DWORD __stdcall ADKGetContentType(HCONTENT hContent);
DWORD __stdcall ADKGetDomainId(HCONTENT hContent, LPWSTR pwszDomainId, UINT cchDomainId);
BOOL __stdcall ADKPackContent(HCONTENT hContentTarget, HCONTENT hContentTemplate);
BOOL __stdcall ADKSetUsage(HCONTENT hContent, WORD wPurpose);
BOOL __stdcall ADKIsLicenseValid(HCONTENT hContent, WORD wPurpose);
DWORD __stdcall ADKGetContentSize(HCONTENT hContent);
DWORD __stdcall ADKSetContentPointer(HCONTENT hContent, LONG lDistanceToMove, DWORD dwMoveMethod);
BOOL __stdcall ADKReadContent(HCONTENT hContent, LPVOID lpBuffer, DWORD dwNumberOfBytesToRead, LPDWORD lpdwNumberOfBytesRead);
BOOL __stdcall ADKBlockCapture(HCONTENT hContent, HWND hwnd, BOOL bSet);
BOOL __stdcall ADKWriteContent(HCONTENT hContent, LPCVOID lpBuffer, DWORD dwNumberOfBytesToWrite, LPDWORD lpdwNumberOfBytesWritten);
BOOL __stdcall ADKSetEndOfContent(HCONTENT hContent);
BOOL __stdcall ADKStartPrint(HCONTENT hContent);
BOOL __stdcall ADKEndPrint(HCONTENT hContent, LPBOOL pbSecurePrinted);
BOOL __stdcall ADKCloseContent(HCONTENT hContent);
VOID __stdcall ADKSetLastError(DWORD dwError);
DWORD __stdcall ADKGetLastError();
DWORD __stdcall ADKGetCID(HCONTENT hContent,LPSTR pszCID,LPDWORD lpcchCID);
BOOL __stdcall ADKDeleteLicenseFile(LPCWSTR pwszFilePath);
BOOL __stdcall ADKInitializeEx(LPCWSTR pwszPC);
DWORD __stdcall ADKIsVirtualPrinter(HANDLE hPrinter, LPWSTR pwszPrinterName, LPWSTR pwszDriverName);
BOOL __stdcall ADKIsLogonStatus(LPCWSTR pwszDomainId);
BOOL __stdcall ADKAutoPackContent(LPCWSTR pwcsFilePath);
BOOL __stdcall ADKIsAutoPackProcess(LPCWSTR pwcsProcessPath);

enum ADK_CONTENT_TYPE
{
	ADK_CONTENT_FSD		= 1,
	ADK_CONTENT_FSE		= 2,
	ADK_CONTENT_FSN		= 3,
	ADK_CONTENT_NX		= 4,
};

enum ADK_PURPOSE 
{
	ADK_PURPOSE_VIEW            = 2,
	ADK_PURPOSE_SAVE            = 3,
	ADK_PURPOSE_EDIT            = 4,
	ADK_PURPOSE_EXTRACT         = 5,
	ADK_PURPOSE_PRINT           = 6,
	ADK_PURPOSE_PRINT_SCREEN    = 8,
	ADK_PURPOSE_TRANSFER        = 10,
	ADK_PURPOSE_SECURE_SAVE     = 12,
	ADK_PURPOSE_SECURE_PRINT    = 13,
};

enum ADK_VIRTUAL_PRINT
{
	NOT_ALLOWED_VIRTUAL_PRINTER = 0,
	ALLOWED_VIRTUAL_PRINTER = 1,
	REAL_PRINTER = 2
};

enum ADK_ERROR 
{
	E_ADK_OK								= 0x0,
	E_ADK_LOAD_NXL_FAIL						= 0x1,
	E_ADK_NXL_INIT_FAIL						= 0x2,
	E_ADK_WIN32_CREATEFILE_FAIL				= 0x3,
	E_ADK_INVALID_ARGS						= 0x4,
	E_ADK_ALREADY_PRINTING					= 0x5,
	E_ADK_PRINTING_NOT_STARTED				= 0x6,
	E_ADK_MEMORY_ERROR						= 0x7,
	E_ADK_INSUFFICIENT_BUFFER				= 0x8,
	E_ADK_CONVERSION_FAIL					= 0x9,
	E_ADK_GETPROCESSNAME_FAIL				= 0xA,

	E_ADK_DRM_CLIENT_NOT_FOUND				= 0xD,
	
	E_ADK_NOT_SECURE_CONTENT				= 0xFF,

	E_ADK_CERT_START						= 0x100,
	E_ADK_CERT_NOT_ALLOWED_CONTENT			= 0x101,	// 사용하려는 파일이 fac 파일로부터 인증되지 않음.

	E_ADK_FAC_LOAD							= 0x200,
	E_ADK_FAC_LOAD_FAIL						= 0x201,	// 현재 모듈을 인증하는 fac 파일이 없다.

	E_ADK_CONTENT							= 0x300,
	E_ADK_CONTENT_OPEN_FAIL					= 0x301,
	E_ADK_CONTENT_READ_FAIL					= 0x302,
	E_ADK_CONTENT_WRITE_FAIL				= 0x303,
	E_ADK_CONTENT_INVALID_LICENSE			= 0x304,
	E_ADK_CONTENT_NOT_ENOUGH_INFO			= 0x305,	// 보안파일의 고유 정보를 얻을 수 없다.
	E_ADK_CONTENT_PACK_FAIL					= 0x306,
	E_ADK_CONTENT_SET_POINTER_FAIL			= 0x307,
	E_ADK_CONTENT_SET_END_FAIL				= 0x308,
	E_ADK_CONTENT_DIFFER_FROM_START			= 0x309,
	E_ADK_CONTENT_INIT_RIGHTS_FAIL			= 0x30A,
	E_ADK_CONTENT_LICENSE_DEL_FAIL			= 0x30B,
	E_ADK_CONTENT_NOT_AUTOPACK_PROCESS		= 0x30C,

	E_ADK_INTERNAL							= 0x400,
	E_ADK_INTERNAL_GET_CLSID_FAIL			= 0x401,
	E_ADK_INTERNAL_ENCRYPTION				= 0x402,
	E_ADK_INTERNAL_NXL_GET_DOMAINID_FAIL	= 0x403,
	E_ADK_INTERNAL_FNC_POINTER_FAIL			= 0x404,

	E_ADK_LIGHT_VERSION						= 0x500,

	E_ADK_GENERAL							= 0xABCD,
};

#endif