#include "stdafx.h"
#include "Resource.h"
#include "DocumentEventProvider.h"
#include "MarkanyDRM.h"
#include "MultiLang.h"
#include "Resource.h"

extern MarkanyDRMApp theApp;

// ctors
DocumentEventProvider::DocumentEventProvider() : de_callbacks_(new FR_DocEventCallbacksRec) {
    std::memset(de_callbacks_.get(), 0, sizeof(FR_DocEventCallbacksRec));

    de_callbacks_->clientData = nullptr;
    de_callbacks_->lStructSize = sizeof(FR_DocEventCallbacksRec);

    de_callbacks_->FRDocDidSave = OnFRDocDidSave;
    de_callbacks_->FRDocOnActivate = OnFRDocOnActivate;
    de_callbacks_->FRDocOnDeactivate = OnFRDocOnDeactivate;
    de_callbacks_->FRDocWillPrint = OnFRDocWillPrint;
    de_callbacks_->FRDocDidPrint = OnFRDocDidPrint;
    de_callbacks_->FRDocOnFrameCreate = OnFRDocOnFrameCreate;
}

// dtor

// methods
void DocumentEventProvider::OnFRDocDidSave(FS_LPVOID clientData, FR_Document doc, FS_BOOL bSaveAs) {
    MarkanyDocument* markany_doc = theApp.doc_manager_->GetMarkanyDoc(doc);
    if (!markany_doc)
        return;

    // reopen Fasoo document
    if (!bSaveAs) {
        HANDLE handle = CreateFile(markany_doc->GetDocPath().c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (handle != INVALID_HANDLE_VALUE)
            markany_doc->SetHandle(handle);        
    }
}

void DocumentEventProvider::OnFRDocOnActivate(FS_LPVOID clientData, FR_Document doc) {
    MarkanyDocument* markany_doc = theApp.doc_manager_->GetMarkanyDoc(doc);

    if (markany_doc) {
        HWND hwnd = FRDocGetDocFrameHandler(doc);

        if (hwnd) {
            theApp.drm_func_->mds_registerwindow(markany_doc->GetDocPath().c_str(), hwnd);
            theApp.drm_func_->mds_notifycurrenteditorwindow(markany_doc->GetDocPath().c_str(), hwnd);
        }
    }
}

void DocumentEventProvider::OnFRDocOnDeactivate(FS_LPVOID clientData, FR_Document doc) {
    MarkanyDocument* markany_doc = theApp.doc_manager_->GetMarkanyDoc(doc);

    if (markany_doc) {
        HWND hwnd = FRDocGetDocFrameHandler(doc);

        if (hwnd)
            theApp.drm_func_->mds_unregisterwindow(markany_doc->GetDocPath().c_str(), hwnd);
    }
}

void DocumentEventProvider::OnFRDocWillPrint(FS_LPVOID clientData, FR_Document doc) {
    MarkanyDocument* markany = theApp.doc_manager_->GetMarkanyDoc(doc);

    if (markany) {
        if (!theApp.drm_func_->mds_beginprint(markany->GetDocPath().c_str())) {
            MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_PRINT_FAILED, MB_OK | MB_ICONERROR);
        }
    }
}

void DocumentEventProvider::OnFRDocDidPrint(FS_LPVOID clientData, FR_Document doc) {
    MarkanyDocument* markany = theApp.doc_manager_->GetMarkanyDoc(doc);

    if (markany) {      
        if (!theApp.drm_func_->mds_endprint(markany->GetDocPath().c_str())) {
            MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_PRINT_FAILED, MB_OK | MB_ICONERROR);
        }

        // internally reopen document for update allowed number of prints
        if (!markany->Reopen())
            MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_PRINT_FAILED, MB_OK | MB_ICONERROR);
    }
}
void DocumentEventProvider::OnFRDocOnFrameCreate(FS_LPVOID clientData, FR_Document doc, HWND hFrameWnd) {
    MarkanyDocument* markany = theApp.doc_manager_->GetMarkanyDoc(doc);

    if (markany) {
        theApp.ShowInfoBulb(doc);
    }
}
