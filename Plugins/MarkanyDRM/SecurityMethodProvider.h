#pragma once
#include "StdAfx.h"

#include <memory>

class SecurityMethodProvider final
{
public:
    SecurityMethodProvider();
    ~SecurityMethodProvider();

    SecurityMethodProvider(const SecurityMethodProvider&) = delete;
    SecurityMethodProvider(SecurityMethodProvider&&) = delete;
    SecurityMethodProvider& operator=(const SecurityMethodProvider&) = delete;
    SecurityMethodProvider& operator=(SecurityMethodProvider&&) = delete;

    FR_SecurityMethodCallbacks GetSecurityMethodsCallbacsk() const noexcept { return sm_callbacks_.get(); }
    FS_LPWSTR GetMethodName() { return const_cast<FS_LPWSTR> (FSWideStringCastToLPCWSTR(method_name_.get())); }
    FS_LPWSTR GetMethodTitle() { return const_cast<FS_LPWSTR> (FSWideStringCastToLPCWSTR(method_title_.get())); }
    FS_LPWSTR GetMethodDescription() { return const_cast<FS_LPWSTR> (FSWideStringCastToLPCWSTR(method_description_.get())); }

    // callbacks
    static FS_LPWSTR OnSecurityMethodGetName(FS_LPVOID clientData);
    static FS_LPWSTR OnSecurityMethodGetTitle(FS_LPVOID clientData);
    static FS_BOOL OnSecurityMethodIsMyMethod(FS_LPVOID clientData, FR_Document doc);
    static FS_BOOL OnSecurityMethodCanBeModified(FS_LPVOID clientData);
    static FS_BOOL OnSecurityMethodCheckModuleLicense(FS_LPVOID clientData);
    static void OnSecurityMethodDoSetting(FS_LPVOID clientData, HWND hWnd, FS_BOOL* bSuc);
    static FS_LPWSTR OnSecurityMethodDescription(FS_LPVOID clientData);
    static FS_DIBitmap OnSecurityMethodGetShowIco(FS_LPVOID clientData, FS_FLOAT dbScale);
    static HWND OnSecurityMethodCreatePermSubDlg(FS_LPVOID clientData, HWND hParent);
    static void OnSecurityMethodDestroyPermSubDlg(FS_LPVOID clientData, HWND hWnd);
    
private:
    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> method_name_;
    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> method_title_;
    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> method_description_;
    
    std::unique_ptr<FR_SecurityMethodCallbacksRec> sm_callbacks_;
};
