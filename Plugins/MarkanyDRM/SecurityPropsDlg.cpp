// SecurityPropsDlg.cpp : implementation file
//

#include "StdAfx.h"
#include "MarkanyDRM.h"
#include "SecurityPropsDlg.h"
#include "MarkanyDocument.h"
#include "afxdialogex.h"

#include "MarkanyDRM.h"
#include "MultiLang.h"

#include <memory>

extern MarkanyDRMApp theApp;

// SecurityPropsDlg dialog

IMPLEMENT_DYNAMIC(SecurityPropsDlg, CDialogEx)

SecurityPropsDlg::SecurityPropsDlg(MarkanyDocument* doc, CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SECURITY_PROPS, pParent),
    doc_{doc}
{

}

SecurityPropsDlg::~SecurityPropsDlg()
{
    ReleaseRibbonStatics();
}

BOOL SecurityPropsDlg::OnInitDialog() {
    CDialogEx::OnInitDialog();

    HWND hwnd = GetSafeHwnd();

    FRLanguageTranslateDialog(theApp.fr_lang_.get(), hwnd, (FS_LPCWSTR)MAKEINTRESOURCE(IDD_SECURITY_PROPS));
    FRSysInstallDialogSkinTheme(hwnd);

    SetRibbonStatics();
    LoadStaticStrings();
    LoadPermissions();

    return TRUE;
}



void SecurityPropsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

void SecurityPropsDlg::SetRibbonStatics() {
    summary_ = SetLargeFontStatic(IDC_STATIC_DOC_SUMMARY);
    save_doc_ = SetSmallFontStatic(IDC_STATIC_SAVE_DOC);
    save_doc_perm_ = SetSmallFontStatic(IDC_SAVE_DOC);
    edit_doc_ = SetSmallFontStatic(IDC_STATIC_EDIT_DOC);
    edit_doc_perm_ = SetSmallFontStatic(IDC_EDIT_DOC);
    doc_extraction_ = SetSmallFontStatic(IDC_STATIC_DOC_EXTRACTION);
    doc_extraction_perm_ = SetSmallFontStatic(IDC_DOC_EXTRACTION);
    screen_capture_ = SetSmallFontStatic(IDC_STATIC_SCREEN_CAPTURE);
    screen_capture_perm_ = SetSmallFontStatic(IDC_SCREEN_CAPTURE);
    print_watermark_ = SetSmallFontStatic(IDC_STATIC_PRINT_WATERMARK);
    print_watermark_perm_ = SetSmallFontStatic(IDC_PRINT_WATERMARKING);
    content_exp_days_ = SetSmallFontStatic(IDC_STATIC_CONTENT_EXP_DAY);
    content_exp_days_perm_ = SetSmallFontStatic(IDC_CONTENT_EXPIRATION_DAYS);
    content_print_count_ = SetSmallFontStatic(IDC_STATIC_CONTENT_PRINT_COUNT);
    content_print_count_perm_ = SetSmallFontStatic(IDC_CONTENT_PRINT_COUNT);
    content_open_count_ = SetSmallFontStatic(IDC_STATIC_CONTENT_OPEN_COUNT);
    content_open_count_perm_ = SetSmallFontStatic(IDC_CONTENT_OPEN_COUNT);
}

void SecurityPropsDlg::LoadStaticStrings() {
    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> fs_str_ptr(FSWideStringNew(), FSWideStringDestroy);
    FS_WideString fs_str = fs_str_ptr.get();

    MultiLang::LoadTranslatedString(IDS_DOC_SUMMARY, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_DOC_SUMMARY)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));

    MultiLang::LoadTranslatedString(IDS_SAVE_DOCUMENT, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_SAVE_DOC)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));

    MultiLang::LoadTranslatedString(IDS_EDIT_DOCUMENT, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_EDIT_DOC)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));

    MultiLang::LoadTranslatedString(IDS_DOCUMENT_EXTRACTION, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_DOC_EXTRACTION)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));

    MultiLang::LoadTranslatedString(IDS_SCREEN_CAPTURE, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_SCREEN_CAPTURE)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));

    MultiLang::LoadTranslatedString(IDS_PRINT_WATERMARK, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_PRINT_WATERMARK)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));

    MultiLang::LoadTranslatedString(IDS_CONTENT_EXP_DAYS, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_CONTENT_EXP_DAY)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));

    MultiLang::LoadTranslatedString(IDS_CONTENT_PRINT_COUNT, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_CONTENT_PRINT_COUNT)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));

    MultiLang::LoadTranslatedString(IDS_CONTENT_OPEN_COUNT, fs_str);
    (static_cast<CStatic*> (GetDlgItem(IDC_STATIC_CONTENT_OPEN_COUNT)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(fs_str));
}

void SecurityPropsDlg::LoadPermissions() {
    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> allowed_str(FSWideStringNew(), FSWideStringDestroy);
    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> not_allowed_str(FSWideStringNew(), FSWideStringDestroy);
    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> unlimited_str(FSWideStringNew(), FSWideStringDestroy);

    FS_WideString allowed = allowed_str.get();
    FS_WideString not_allowed = not_allowed_str.get();
    FS_WideString unlimited = unlimited_str.get();

    MultiLang::LoadTranslatedString(IDS_ALLOWED, allowed);
    MultiLang::LoadTranslatedString(IDS_NOT_ALLOWED, not_allowed);
    MultiLang::LoadTranslatedString(IDS_UNLIMITED, unlimited);

    const auto& path = doc_->GetDocPath();
    auto props = doc_->GetProperties();

    if (theApp.drm_func_->mds_canesavable(path.c_str()))
        (static_cast<CStatic*> (GetDlgItem(IDC_SAVE_DOC)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(allowed));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_SAVE_DOC)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(not_allowed));

    if (theApp.drm_func_->mds_caneditable(path.c_str()))
        (static_cast<CStatic*> (GetDlgItem(IDC_EDIT_DOC)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(allowed));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_EDIT_DOC)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(not_allowed));

    if (props.bBlockCopy)
        (static_cast<CStatic*> (GetDlgItem(IDC_DOC_EXTRACTION)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(allowed));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_DOC_EXTRACTION)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(not_allowed));

    if (!props.bScreenCapture)
        (static_cast<CStatic*> (GetDlgItem(IDC_SCREEN_CAPTURE)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(allowed));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_SCREEN_CAPTURE)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(not_allowed));

    if (props.bWatermarking)
        (static_cast<CStatic*> (GetDlgItem(IDC_PRINT_WATERMARKING)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(allowed));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_PRINT_WATERMARKING)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(not_allowed));
    
    auto exp_days = _wtoi(props.ExpireDate);
    if (exp_days == -99)
        (static_cast<CStatic*> (GetDlgItem(IDC_CONTENT_EXPIRATION_DAYS)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(unlimited));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_CONTENT_EXPIRATION_DAYS)))->SetWindowText(props.ExpireDate);

    if (props.nPrint < 0)
        (static_cast<CStatic*> (GetDlgItem(IDC_CONTENT_PRINT_COUNT)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(unlimited));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_CONTENT_PRINT_COUNT)))->SetWindowText(std::to_wstring(props.nPrint).c_str());

    if (props.nOpen < 0)
        (static_cast<CStatic*> (GetDlgItem(IDC_CONTENT_OPEN_COUNT)))->SetWindowText((LPCWSTR)FSWideStringCastToLPCWSTR(unlimited));
    else
        (static_cast<CStatic*> (GetDlgItem(IDC_CONTENT_OPEN_COUNT)))->SetWindowText(std::to_wstring(props.nOpen).c_str());
}


void SecurityPropsDlg::ReleaseRibbonStatics() {
    if (summary_)
        FRRibbonStyleStaticRelease(summary_);

    if (save_doc_)
        FRRibbonStyleStaticRelease(save_doc_);

    if (save_doc_perm_)
        FRRibbonStyleStaticRelease(save_doc_perm_);

    if (edit_doc_)
        FRRibbonStyleStaticRelease(edit_doc_);

    if (edit_doc_perm_)
        FRRibbonStyleStaticRelease(edit_doc_perm_);

    if (doc_extraction_)
        FRRibbonStyleStaticRelease(doc_extraction_);

    if (doc_extraction_perm_)
        FRRibbonStyleStaticRelease(doc_extraction_perm_);

    if (screen_capture_)
        FRRibbonStyleStaticRelease(screen_capture_);

    if (screen_capture_perm_)
        FRRibbonStyleStaticRelease(screen_capture_perm_);

    if (print_watermark_)
        FRRibbonStyleStaticRelease(print_watermark_);

    if (print_watermark_perm_)
        FRRibbonStyleStaticRelease(print_watermark_perm_);

    if (content_exp_days_)
        FRRibbonStyleStaticRelease(content_exp_days_);

    if (content_exp_days_perm_)
        FRRibbonStyleStaticRelease(content_exp_days_perm_);

    if (content_print_count_)
        FRRibbonStyleStaticRelease(content_print_count_);

    if (content_print_count_perm_)
        FRRibbonStyleStaticRelease(content_print_count_perm_);

    if (content_open_count_)
        FRRibbonStyleStaticRelease(content_open_count_);

    if (content_open_count_perm_)
        FRRibbonStyleStaticRelease(content_open_count_perm_);
}

FR_RibbonStyleStatic SecurityPropsDlg::SetSmallFontStatic(UINT static_id) {
    FR_RibbonStyleStatic ribbonStatic = (FR_RibbonStyleStatic)FRRibbonBackStageViewItemModifiedToRibbonStyleButton(GetDlgItem(static_id), static_id, FR_RibbonStyle_Static, this);
    FRRibbonStyleStaticSetFontStyle(ribbonStatic, 12, FALSE, FALSE, FALSE, FALSE);
    FRRibbonStyleStaticSetTextColor(ribbonStatic, RGB(60, 60, 60));
    FRRibbonStyleStaticSetTextDrawFormat(ribbonStatic, DT_WORDBREAK);

    return ribbonStatic;
}

FR_RibbonStyleStatic SecurityPropsDlg::SetLargeFontStatic(UINT static_id) {
    FR_RibbonStyleStatic ribbonStatic = (FR_RibbonStyleStatic)FRRibbonBackStageViewItemModifiedToRibbonStyleButton(GetDlgItem(static_id), static_id, FR_RibbonStyle_Static, this);
    FRRibbonStyleStaticSetFontStyle(ribbonStatic, 14, FALSE, FALSE, FALSE, FALSE);
    FRRibbonStyleStaticSetTextColor(ribbonStatic, RGB(50, 50, 50));

    return ribbonStatic;
}

BEGIN_MESSAGE_MAP(SecurityPropsDlg, CDialogEx)
END_MESSAGE_MAP()


// SecurityPropsDlg message handlers
