#pragma once

#include "StdAfx.h"
#include "MarkanyDocument.h"
#include <memory>
#include <map>

using MarkanyDocs = std::map<FR_Document, std::unique_ptr<MarkanyDocument>>;

class MarkanyDocManager
{
public:
    MarkanyDocManager() {}
    ~MarkanyDocManager() {}

    void AddMarkanyDoc(const std::wstring& path, HANDLE handle, char* data, std::size_t data_size);
    void RemoveMarkanyDoc(FR_Document fr_doc);
    const MarkanyDocument* UpdateMarkanyDoc(const FS_LPCWSTR path, FR_Document new_fr_doc);

    const MarkanyDocument* GetMarkanyDoc(FR_Document fr_doc) const;
    MarkanyDocument* GetMarkanyDoc(FR_Document fr_doc);

private:
    MarkanyDocs docs_;

    //!< Document that pass via AppEventProvider::OnOpenDocument and waiting to
   //   process by ContentProvider::OnFileOpen
    std::unique_ptr<MarkanyDocument> unopened_doc_;
};
