from conans import ConanFile, MSBuild, tools
import os

class FasoodrmConan(ConanFile):
    name = "MarkanyDRM"
    version = os.environ.get('PACKAGE_VERSION', '1.0')
    author = "Foxit Software"
    url = "www.foxitsoftware.com"
    description = "MarkanyDRM plugin"
    settings = "os", "compiler", "build_type", "arch"
    generators = "visual_studio"

    def build(self):
        if self.settings.compiler == "Visual Studio":
            msbuild = MSBuild(self)
            msbuild.build("MarkanyDRM.sln", upgrade_project=False)
        else:
            raise Exception("Unsupported compiler. This project should be compiled by Visual Studio")

    def package(self):
        if self.settings.build_type == "Release":
            self.copy("bin\\MarkanyDRM\\Win32\\Release\\v141\\MarkanyDRM.fpi", dst="plugin_package", keep_path=False)
        elif self.settings.build_type == "Debug":
            self.copy("bin\\MarkanyDRM\\Win32\\Debug\\v141\\MarkanyDRM.fpi", dst="plugin_package", keep_path=False)
        else:
            raise Exception("Unknown build_type. Should be Release or Debug")
        self.copy("InstallMarkanyDRM.xml", dst="plugin_package", keep_path=False)
        self.copy("res\\Lang\\ko-KR\\MarkanyDRMlang_ko-KR.xml", dst="plugin_package\\Lang\\ko-KR", keep_path=False)
        self.copy("MarkanyDRM_DLL\\x86\\MADCLEM.dll", dst="plugin_package", keep_path=False)
        self.copy("*.gitlog", dst="plugin_package", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["plugin_package"]

