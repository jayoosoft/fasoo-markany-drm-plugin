#pragma once

#include "StdAfx.h"
#include "MarkanyDRMHelper.h"
#include "SecurityPropsDlg.h"

#include <string>
#include <memory>
#include <optional>

class MarkanyDocManager;

class MarkanyDocument final
{
    friend class MarkanyDocManager;

private:
    MarkanyDocument(const std::wstring& path, HANDLE handle, char* data, std::size_t data_size);

public:
    ~MarkanyDocument();

    std::size_t GetContentSize() const noexcept { return data_size_; }
    const char* GetContentBuffer() const noexcept { return data_.get(); }

    HANDLE GetHandle() const noexcept { return handle_; }
    void SetHandle(HANDLE handle) noexcept { handle_ = handle; }

    const std::wstring& GetDocPath() const noexcept { return doc_path_; }

    void SetPermissions(unsigned long p) noexcept { perms_ = p; }
    unsigned long GetPermissions() const noexcept { return perms_; }

    void UpdateData(char* data, std::size_t data_size);

    const std::optional<bool>& CanBeSaved() const noexcept { return can_save_; }
    void SetCanBeSaved(bool s) noexcept { can_save_ = s; }

    bool IsBlockCopyEnabled() { ReloadDocumentProperties(); return doc_props_.bBlockCopy; }

    const MA_ENCFILE_PROPERTY& GetProperties() { ReloadDocumentProperties();  return doc_props_; }

    void SetSecurityPropertiesDlg(SecurityPropsDlg* dlg) noexcept { sec_prop_dlg_ = dlg; }
    SecurityPropsDlg* GetSecurityPropertiesDlg() const noexcept { return sec_prop_dlg_; }

    bool Reopen();

private:
    MA_ENCFILE_PROPERTY doc_props_;
    std::wstring doc_path_;
    std::optional<bool> can_save_;
    unsigned long perms_{ 0 };
    HANDLE handle_;
    std::unique_ptr<char[]> data_;
    std::size_t data_size_;    
    SecurityPropsDlg* sec_prop_dlg_{ nullptr };

    void ReloadDocumentProperties();    
};
