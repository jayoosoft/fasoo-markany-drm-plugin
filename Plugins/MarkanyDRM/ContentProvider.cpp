#include "stdafx.h"
#include "ContentProvider.h"
#include "MultiLang.h"
#include "MarkanyDRM.h"
#include "AppEventProvider.h"

#include <vector>
#include <algorithm>
#include <iterator>

extern MarkanyDRMApp theApp;

// ctor
ContentProvider::ContentProvider() : cp_callbacks_(new FR_ContentProviderCallbacksRec) {
    std::memset(cp_callbacks_.get(), 0, sizeof(FR_ContentProviderCallbacksRec));

    cp_callbacks_->lStructSize = sizeof(FR_ContentProviderCallbacksRec);
    cp_callbacks_->clientData = nullptr;

    cp_callbacks_->FRConProviderOnFileOpen = OnFileOpen;
    cp_callbacks_->FRConProviderOnFileClose = OnFileClose;
    cp_callbacks_->FRConProviderOnReadContent = OnReadContent;
    cp_callbacks_->FRConProviderOnGetContentSize = OnGetContentSize;
    cp_callbacks_->FRConProviderOnGetPermissions = OnGetPermissions;
    cp_callbacks_->FRConProviderOnWriteContent = OnWriteContent;
    cp_callbacks_->FRConProviderCanBeSaved = OnCanBeSaved;
    cp_callbacks_->FRConProviderOnBackFillContent = OnBackFillContent;
}

// dtor
ContentProvider::~ContentProvider() {

}

// callbacks
FS_BOOL ContentProvider::OnFileOpen(FS_LPVOID clientData, FR_Document doc, FS_LPCWSTR lpszSource, FS_BOOL bIsAttachment) {
    const MarkanyDocument* markany_doc = theApp.doc_manager_->UpdateMarkanyDoc(lpszSource, doc);
    if (!markany_doc) {
        AppEventProvider::OnOpenDocument(clientData, lpszSource);

        markany_doc = theApp.doc_manager_->UpdateMarkanyDoc(lpszSource, doc);

        if (!markany_doc)
            return FALSE;   
    }

    FRDocSetMenuEnableByName(doc, FR_MENU_ENABLE_SNAPSHOT, FALSE);

    return TRUE;
}

void ContentProvider::OnFileClose(FS_LPVOID clientData, FR_Document doc) {
    MarkanyDocument* markany_doc = theApp.doc_manager_->GetMarkanyDoc(doc);

    if (markany_doc) {
        auto handle = markany_doc->GetHandle();
        if (handle)
            theApp.drm_func_->mds_CloseHandle(handle);

        theApp.drm_func_->mds_unregisteropenfile(markany_doc->GetDocPath().c_str());
        theApp.doc_manager_->RemoveMarkanyDoc(doc);
    }    
}

FS_BOOL ContentProvider::OnGetContentSize(FS_LPVOID clientData, /*IN*/ FR_Document doc,/*OUT*/ unsigned long * pTotalSize)
{
    MarkanyDocument* markany_doc = theApp.doc_manager_->GetMarkanyDoc(doc);

    if (!markany_doc)
        return FALSE;

    *pTotalSize = markany_doc->GetContentSize();
    return TRUE;
}

FS_BOOL ContentProvider::OnReadContent(FS_LPVOID clientData, /*IN*/ FR_Document doc, /*IN*/ DWORD pos, /*OUT*/ unsigned char * pBuf, /*IN*/ unsigned long size)
{
    MarkanyDocument* markany_doc = theApp.doc_manager_->GetMarkanyDoc(doc);

    if (!markany_doc)
        return FALSE;


    const char* content = markany_doc->GetContentBuffer();

    if (content) {
        memcpy(pBuf, content + pos, size);
    }

    return TRUE;
}

unsigned long ContentProvider::OnGetPermissions(FS_LPVOID clientData, /*IN*/ FR_Document doc, /*IN*/ unsigned long pdfselfPermissions) {
    MarkanyDocument* markany_doc = theApp.doc_manager_->GetMarkanyDoc(doc);

    if (!markany_doc)
        return -1;

    auto props = markany_doc->GetProperties();

    unsigned long perms = markany_doc->GetPermissions();
    if (perms != 0 && props.nPrint < 0)  // dyn. check number of enabled prints
        return perms;

    const wchar_t* file_path = markany_doc->GetDocPath().c_str();
    perms = -1;

    if (!theApp.drm_func_->mds_caneditable(file_path)) {
        perms &= ~(FR_PERM_MODIFY_CONTENT | FR_PERM_ANNOTATE | FR_PERM_ASSEMBLE | FR_PERM_FILL_FORM);    
    }

    if (!theApp.drm_func_->mds_canprintable(file_path) || props.nPrint == 0) {
        perms &= ~(FR_PERM_PRINT | FR_PERM_PRINT_HIGN);    
    }

    if (!theApp.drm_func_->mds_canesavable(file_path)) {
        FRDocSetMenuEnableByName(doc, FR_MENU_ENABLE_SAVEAS, FALSE);       
    }

    // disable extraction (copy text or images)
    if (!markany_doc->IsBlockCopyEnabled())
        perms &= ~FR_PERM_EXTRACT_COPY; 

    markany_doc->SetPermissions(perms);

    return perms;
}

FS_BOOL ContentProvider::OnWriteContent(FS_LPVOID clientData, /*IN*/ FR_Document doc, /*IN*/ unsigned char * pBuf, /*IN*/ unsigned long size, /*IN*/ FS_LPCWSTR lpSaveFilePath) {
    MarkanyDocument* markany_doc = theApp.doc_manager_->GetMarkanyDoc(doc);
    
    if (!markany_doc)
        return FALSE;

    std::wstring p{ lpSaveFilePath };
    bool save = (p.find(L"~~$") != std::wstring::npos);
    auto md_path = markany_doc->GetDocPath().c_str();

    HANDLE handle = CreateFile(lpSaveFilePath, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
    if (handle == INVALID_HANDLE_VALUE) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_CREATE_FILE, MB_OK | MB_ICONERROR);
        return FALSE;
    }

    if (theApp.drm_func_->mds_beginencryptfile(md_path, lpSaveFilePath, handle)) {
        DWORD written = 0;
        if (theApp.drm_func_->mds_WriteFile(handle, (PBYTE)pBuf, size, &written) == FALSE) {
            MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_PROTECTION_FAILED, MB_OK | MB_ICONERROR);
            CloseHandle(handle);
            return FALSE;
        }
            
        theApp.drm_func_->mds_CloseHandle(handle);

        if (!theApp.drm_func_->mds_endencryptfile(md_path, lpSaveFilePath, handle)) {
            MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_PROTECTION_FAILED, MB_OK | MB_ICONERROR);
            CloseHandle(handle);
            return FALSE;
        }

        markany_doc->UpdateData(reinterpret_cast<char*> (pBuf), size);
    }

    if (save) {
        CloseHandle(markany_doc->GetHandle());
        markany_doc->SetHandle(NULL);        
    }
        
    return TRUE;
}

FS_BOOL ContentProvider::OnBackFillContent(FS_LPVOID clientData, FR_Document doc, unsigned char* pBuf, unsigned long size, FS_LPCWSTR lpSaveFilePath) {
    MarkanyDocument* markany_doc = theApp.doc_manager_->GetMarkanyDoc(doc);

    if (!markany_doc)
        return FALSE;
    
    HANDLE handle = CreateFile(lpSaveFilePath, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ, NULL, TRUNCATE_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    auto md_path = markany_doc->GetDocPath().c_str();

    if (!handle)
        return FALSE;

    if (theApp.drm_func_->mds_beginencryptfile(md_path, lpSaveFilePath, handle)) {
        DWORD written = 0;
        if (theApp.drm_func_->mds_WriteFile(handle, (PBYTE)pBuf, size, &written) == FALSE) {
            MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_PROTECTION_FAILED, MB_OK | MB_ICONERROR);
            CloseHandle(handle);
            return FALSE;
        }

        theApp.drm_func_->mds_CloseHandle(handle);

        if (!theApp.drm_func_->mds_endencryptfile(md_path, lpSaveFilePath, handle)) {
            MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_PROTECTION_FAILED, MB_OK | MB_ICONERROR);
            CloseHandle(handle);
            return FALSE;
        }
    }

    markany_doc->SetHandle(handle);
    
    return TRUE;
}

FS_BOOL ContentProvider::OnCanBeSaved(FS_LPVOID clientData, FR_Document frDoc) {
    MarkanyDocument* markany = theApp.doc_manager_->GetMarkanyDoc(frDoc);

    if (!markany) {
        return TRUE; // Not secured doc
    }

    auto can_save = markany->CanBeSaved();
    bool s = true;

    if (!can_save.has_value()) {
        s = theApp.drm_func_->mds_canesavable(markany->GetDocPath().c_str());
        markany->SetCanBeSaved(s);
    }
    else
        s = can_save.value();

    if (!s)
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_SAVE_NOT_ALLOWED, MB_OK | MB_ICONERROR);

    return s;
}
