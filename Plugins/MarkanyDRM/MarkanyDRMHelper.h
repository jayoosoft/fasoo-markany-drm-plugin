#pragma once

#include "StdAfx.h"

typedef struct _MA_ENCFILE_PROPERTY
{
    WCHAR	strFileName[MAX_PATH * 2];
    WCHAR strCompanyName[256];
    WCHAR ExchangePolicy[256];
    BOOL bSave; //0:disable, 1:enable
    BOOL bEdit; //0:disable, 1:enable
    BOOL bWatermarking; //0:Not Print, 1:Print
    BOOL bScreenCapture; //0:NonBlock, 1:Block
    BOOL bBlockCopy; //0: disalbe, 1:enable
    WCHAR ExpireDate[256]; //Days left until expiration
    int nPrint;
    int nOpen;
}MA_ENCFILE_PROPERTY, *PMA_ENCFILE_PROPERTY;

class MarkanyDRMHelper
{
private:
    HMODULE m_hMod = NULL;

public:
    typedef BOOL(*pMds_initialize)(LPCWSTR);
    typedef BOOL(*pMds_finalize)();
    typedef BOOL(*pMds_isgoodtogo)(LPCWSTR, HANDLE);
    typedef BOOL(*pMds_ReadFile)(HANDLE, LPVOID, DWORD, LPDWORD);
    typedef BOOL(*pMds_WriteFile)(HANDLE, PBYTE, DWORD, LPDWORD);
    typedef DWORD(*pMds_SetFilePointer)(HANDLE, LONG, PLONG, DWORD);
    typedef BOOL(*pMds_CloseHandle)(HANDLE);
    typedef BOOL(*pMds_registeropenfile)(LPCWSTR);
    typedef BOOL(*pMds_unregisteropenfile)(LPCWSTR);
    typedef BOOL(*pMds_isdrmfile)(LPCWSTR);
    typedef BOOL(*pMds_beginencryptfile)(LPCWSTR, LPCWSTR, HANDLE);
    typedef BOOL(*pMds_endencryptfile)(LPCWSTR, LPCWSTR, HANDLE);
    typedef BOOL(*pMds_caneditable)(LPCWSTR);
    typedef BOOL(*pMds_canesavable)(LPCWSTR);
    typedef BOOL(*pMds_canprintable)(LPCWSTR);
    typedef BOOL(*pMds_cancombinefile)(LPCWSTR);
    typedef BOOL(*pMds_issecureprinter)(LPCWSTR, HANDLE, LPWSTR, LPWSTR);
    typedef BOOL(*pMds_beginprint)(LPCWSTR);
    typedef BOOL(*pMds_endprint)(LPCWSTR);
    typedef HANDLE(*pMds_SetClipboardData)(LPCWSTR, UINT, HANDLE);
    typedef HANDLE(*pMds_GetClipboardData)(LPCWSTR, UINT);
    typedef BOOL(*pMds_registerwindow)(LPCWSTR, HWND);
    typedef BOOL(*pMds_unregisterwindow)(LPCWSTR, HWND);
    typedef BOOL(*pMds_notifycurrenteditorwindow)(LPCWSTR, HWND);
    typedef MA_ENCFILE_PROPERTY(*pMds_GetEncFileInfo)(LPCWSTR);

    pMds_initialize mds_initialize;
    pMds_finalize mds_finalize;
    pMds_isgoodtogo mds_isgoodtogo;
    pMds_ReadFile mds_ReadFile;
    pMds_WriteFile mds_WriteFile;
    pMds_SetFilePointer mds_SetFilePointer;
    pMds_CloseHandle mds_CloseHandle;
    pMds_registeropenfile mds_registeropenfile;
    pMds_unregisteropenfile mds_unregisteropenfile;
    pMds_isdrmfile mds_isdrmfile;
    pMds_beginencryptfile mds_beginencryptfile;
    pMds_endencryptfile mds_endencryptfile;
    pMds_caneditable mds_caneditable;
    pMds_canesavable mds_canesavable;
    pMds_canprintable mds_canprintable;
    pMds_cancombinefile mds_cancombinefile;
    pMds_issecureprinter mds_issecureprinter;
    pMds_beginprint mds_beginprint;
    pMds_endprint mds_endprint;
    pMds_SetClipboardData mds_SetClipboardData;
    pMds_GetClipboardData mds_GetClipboardData;
    pMds_registerwindow mds_registerwindow;
    pMds_unregisterwindow mds_unregisterwindow;
    pMds_notifycurrenteditorwindow mds_notifycurrenteditorwindow;
    pMds_GetEncFileInfo mds_GetEncFileInfo;

    MarkanyDRMHelper() {}

    ~MarkanyDRMHelper() {
        if (m_hMod)
            FreeLibrary(m_hMod);
    }

    bool LoadDRMFunctions() {
        m_hMod = LoadLibrary(_T("MADCLEM.dll"));
        if (m_hMod == NULL)
            return false;

        mds_initialize = (pMds_initialize)GetProcAddress(m_hMod, "mds_initialize");
        if (mds_initialize == NULL)
            return false;

        mds_finalize = (pMds_finalize)GetProcAddress(m_hMod, "mds_finalize");
        if (mds_finalize == NULL)
            return false;

        mds_isgoodtogo = (pMds_isgoodtogo)GetProcAddress(m_hMod, "mds_isgoodtogo");
        if (mds_isgoodtogo == NULL)
            return false;

        mds_ReadFile = (pMds_ReadFile)GetProcAddress(m_hMod, "mds_ReadFile");
        if (mds_ReadFile == NULL)
            return false;

        mds_WriteFile = (pMds_WriteFile)GetProcAddress(m_hMod, "mds_WriteFile");
        if (mds_WriteFile == NULL)
            return false;

        mds_SetFilePointer = (pMds_SetFilePointer)GetProcAddress(m_hMod, "mds_SetFilePointer");
        if (mds_SetFilePointer == NULL)
            return false;

        mds_CloseHandle = (pMds_CloseHandle)GetProcAddress(m_hMod, "mds_CloseHandle");
        if (mds_CloseHandle == NULL)
            return false;

        mds_registeropenfile = (pMds_registeropenfile)GetProcAddress(m_hMod, "mds_registeropenfile");
        if (mds_registeropenfile == NULL)
            return false;

        mds_unregisteropenfile = (pMds_unregisteropenfile)GetProcAddress(m_hMod, "mds_unregisteropenfile");
        if (mds_unregisteropenfile == NULL)
            return false;

        mds_isdrmfile = (pMds_isdrmfile)GetProcAddress(m_hMod, "mds_isdrmfile");
        if (mds_isdrmfile == NULL)
            return false;

        mds_beginencryptfile = (pMds_beginencryptfile)GetProcAddress(m_hMod, "mds_beginencryptfile");
        if (mds_beginencryptfile == NULL)
            return false;

        mds_endencryptfile = (pMds_endencryptfile)GetProcAddress(m_hMod, "mds_endencryptfile");
        if (mds_endencryptfile == NULL)
            return false;

        mds_caneditable = (pMds_caneditable)GetProcAddress(m_hMod, "mds_caneditable");
        if (mds_caneditable == NULL)
            return false;

        mds_canesavable = (pMds_canesavable)GetProcAddress(m_hMod, "mds_canesavable");
        if (mds_canesavable == NULL)
            return false;

        mds_canprintable = (pMds_canprintable)GetProcAddress(m_hMod, "mds_canprintable");
        if (mds_canprintable == NULL)
            return false;

        mds_cancombinefile = (pMds_cancombinefile)GetProcAddress(m_hMod, "mds_cancombinefile");
        if (mds_cancombinefile == NULL)
            return false;

        mds_issecureprinter = (pMds_issecureprinter)GetProcAddress(m_hMod, "mds_issecureprinter");
        if (mds_issecureprinter == NULL)
            return false;

        mds_beginprint = (pMds_beginprint)GetProcAddress(m_hMod, "mds_beginprint");
        if (mds_beginprint == NULL)
            return false;

        mds_endprint = (pMds_endprint)GetProcAddress(m_hMod, "mds_endprint");
        if (mds_endprint == NULL)
            return false;

        mds_SetClipboardData = (pMds_SetClipboardData)GetProcAddress(m_hMod, "mds_SetClipboardData");
        if (mds_SetClipboardData == NULL)
            return false;

        mds_GetClipboardData = (pMds_GetClipboardData)GetProcAddress(m_hMod, "mds_GetClipboardData");
        if (mds_GetClipboardData == NULL)
            return false;

        mds_registerwindow = (pMds_registerwindow)GetProcAddress(m_hMod, "mds_registerwindow");
        if (mds_registerwindow == NULL)
            return false;

        mds_unregisterwindow = (pMds_unregisterwindow)GetProcAddress(m_hMod, "mds_unregisterwindow");
        if (mds_unregisterwindow == NULL)
            return false;

        mds_notifycurrenteditorwindow = (pMds_notifycurrenteditorwindow)GetProcAddress(m_hMod, "mds_notifycurrenteditorwindow");
        if (mds_notifycurrenteditorwindow == NULL)
            return false;

        mds_GetEncFileInfo = (pMds_GetEncFileInfo)GetProcAddress(m_hMod, "mds_GetEncFileInfo");
        if (mds_GetEncFileInfo == NULL) {
            return false;
        }

        return true;
    }    
};
