//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MarkanyDRM.rc
//
#define MARKANY_ICON_24                 2
#define IDD_SECURITY_PROPS              101
#define IDS_CANNOT_LOAD_DLL             1000
#define IDC_STATIC_DOC_SUMMARY          1000
#define IDS_PRINT_FAILED                1001
#define MARKANY_ICON_16                 1001
#define IDC_STATIC_SAVE_DOC             1001
#define IDS_CANNOT_OPEN_FILE            1002
#define IDC_SAVE_DOC                    1002
#define IDS_NO_RIGHT_TO_READ            1003
#define MARKANY_ICON_32                 1003
#define IDC_STATIC_EDIT_DOC             1003
#define IDS_CANNOT_REGISTER_FILE        1004
#define MARKANY_ICON_48                 1004
#define IDC_EDIT_DOC                    1004
#define IDS_PRINTER_DISABLED            1005
#define IDC_STATIC_DOC_EXTRACTION       1005
#define IDS_CANNOT_CREATE_FILE          1006
#define IDC_DOC_EXTRACTION              1006
#define IDS_PROTECTION_FAILED           1007
#define IDC_STATIC_SCREEN_CAPTURE       1007
#define IDS_SECURITY_METHOD_NAME        1008
#define IDC_SCREEN_CAPTURE              1008
#define IDS_SECURITY_METHOD_TITLE       1009
#define IDC_STATIC_PRINT_WATERMARK      1009
#define IDS_SECURITY_METHOD_DESCRIPTION 1010
#define IDC_PRINT_WATERMARKING          1010
#define IDS_MARKANYDRM_DO_SETTINGS      1011
#define IDC_STATIC_CONTENT_EXP_DAY      1011
#define IDS_BULB_TITLE                  1012
#define IDC_CONTENT_EXPIRATION_DAYS     1012
#define IDS_BULB_MESSAGE                1013
#define IDC_STATIC_CONTENT_PRINT_COUNT  1013
#define IDS_BULB_VIEW_PERMS             1014
#define IDC_CONTENT_PRINT_COUNT         1014
#define IDS_SAVE_NOT_ALLOWED            1015
#define IDC_STATIC_CONTENT_OPEN_COUNT   1015
#define IDC_CONTENT_OPEN_COUNT          1016
#define IDS_DOC_SUMMARY                 1016
#define IDS_SAVE_DOCUMENT               1017
#define IDS_EDIT_DOCUMENT               1018
#define IDS_DOCUMENT_EXTRACTION         1019
#define IDS_SCREEN_CAPTURE              1020
#define IDS_PRINT_WATERMARK             1021
#define IDS_CONTENT_EXP_DAYS            1022
#define IDS_CONTENT_PRINT_COUNT         1023
#define IDS_CONTENT_OPEN_COUNT          1024
#define IDS_ALLOWED                     1025
#define IDS_NOT_ALLOWED                 1026
#define IDS_UNLIMITED                   1027

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1025
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           1000
#endif
#endif
