/*********************************************************************

 Copyright (C) 2010 Foxit Corporation
 All rights reserved.

 NOTICE: Foxit permits you to use, modify, and distribute this file
 in accordance with the terms of the Foxit license agreement
 accompanying it. If you have received this file from a source other
 than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.

 ---------------------------------------------------------------------

APSDK.cpp

 - Defines the entry point for the DLL application, the entry point is PlugInMain.

 - Skeleton .cpp file for a plug-in. It implements the basic
   handshaking methods required for all plug-ins.

*********************************************************************/


#include "stdafx.h"
#include "Resource.h"
#include "MarkanyDRM.h"
#include "MultiLang.h"

#include <filesystem>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

/////////////////////////////////////////////////////////////////////////////
// MarkanyDRMApp

BEGIN_MESSAGE_MAP(MarkanyDRMApp, CWinApp)
	//{{AFX_MSG_MAP(MarkanyDRMApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// MarkanyDRMApp construction

MarkanyDRMApp::MarkanyDRMApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

// methods
bool MarkanyDRMApp::CheckMarkanyDll() const {
    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> app_dir(FSWideStringNew(), FSWideStringDestroy);

    FS_WideString s = app_dir.get();
    FRAppGetModuleFileName(&s);

    std::wstring dll_str{ FSWideStringCastToLPCWSTR(app_dir.get()) };
    dll_str = dll_str.substr(0, dll_str.find_last_of('\\'));

    dll_str += L"\\MADCLEM.dll";

    std::filesystem::path p{ dll_str };
   
    if (std::filesystem::exists(p)) {
        return true;
    }

    return false;
}

void MarkanyDRMApp::ShowInfoBulb(FR_Document doc) {
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    if (!doc)
        return;

    const char* bulb_btn_name = "MarkanyDRMBulbBtn";

    if (!FRBulbMsgCenterIsMessageExist(doc, (FS_LPCSTR)bulb_name_)) {
        std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> title(FSWideStringNew(), FSWideStringDestroy);
        std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> msg(FSWideStringNew(), FSWideStringDestroy);
        std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> view_perms(FSWideStringNew(), FSWideStringDestroy);

        if (!MultiLang::LoadTranslatedString(IDS_BULB_TITLE, title.get()))
            return;

        if (!MultiLang::LoadTranslatedString(IDS_BULB_MESSAGE, msg.get()))
            return;

        if (!MultiLang::LoadTranslatedString(IDS_BULB_VIEW_PERMS, view_perms.get()))
            return;

        std::unique_ptr<_t_FS_WideStringArray, decltype(FSWideStringArrayDestroy)> button_titles(FSWideStringArrayNew(), FSWideStringArrayDestroy);
        std::unique_ptr<_t_FS_ByteStringArray, decltype(FSByteStringArrayDestroy)> button_names(FSByteStringArrayNew(), FSByteStringArrayDestroy);

        FSWideStringArrayAdd(button_titles.get(), FSWideStringCastToLPCWSTR(view_perms.get()));
        FSByteStringArrayAdd(button_names.get(), (FS_LPSTR)bulb_btn_name, -1);

        FRBulbMsgOperationCallback show_perms_proc = static_cast<FRBulbMsgOperationCallback>([](void* client_data) {
            FRAppPopDocPropertyPage((FS_LPCWSTR)L"Security");
        });

        FRBULBMESSAGEINFO2 bulb_info;
        std::memset(&bulb_info, 0, sizeof(FRBULBMESSAGEINFO2));
        std::memset(&bulb_info.bulbMsgInfoV1, 0, sizeof(FRBULBMESSAGEINFO));

        bulb_info.lStructSize = sizeof(FRBULBMESSAGEINFO2);
        bulb_info.pClientData = doc;
        bulb_info.lpwsTitle = FSWideStringCastToLPCWSTR(title.get());

        bulb_info.bulbMsgInfoV1.pClientData = doc;
        bulb_info.bulbMsgInfoV1.lpsMsgName = (FS_LPCSTR)bulb_name_;
        bulb_info.bulbMsgInfoV1.lpwsMessage = FSWideStringCastToLPCWSTR(msg.get());
        bulb_info.bulbMsgInfoV1.opBtnCount = 1;
        bulb_info.bulbMsgInfoV1.opBtnTitles = button_titles.get();
        bulb_info.bulbMsgInfoV1.opBtnNames = button_names.get();
        bulb_info.bulbMsgInfoV1.opCallbackFuncList = &show_perms_proc;

        FRBulbMsgCenterAddMessage5(doc, bulb_info);
    }

    FRBulbMsgCenterShowMessage(doc, bulb_name_, TRUE);
}

void MarkanyDRMApp::HideInfoBulb(FR_Document doc) {
    if (FRBulbMsgCenterIsMessageExist(doc, (FS_LPCSTR)bulb_name_)) {
        FRBulbMsgCenterShowMessage(doc, bulb_name_, FALSE);
    }
}


/////////////////////////////////////////////////////////////////////////////
// The one and only MarkanyDRMApp object

MarkanyDRMApp theApp;

/** 
	APSDK is a plug-in that provides a proof of contencept how to
	implementat similar functionality as in Acrobat SDK.
*/



/* PIExportHFTs
** ------------------------------------------------------
**/
/** 
** Create and Add the extension HFT's.
**
** @return true to continue loading plug-in,
** false to cause plug-in loading to stop.
*/
FS_BOOL PIExportHFTs(void)
{
	return TRUE;
}

/** 
The application calls this function to allow it to
<ul>
<li> Import HFTs supplied by other plug-ins.
<li> Replace functions in the HFTs you're using (where allowed).
<li> Register to receive notification events.
</ul>
*/
FS_BOOL PIImportReplaceAndRegister(void)
{
	return TRUE;
}

////////////////////////////////////////////////////////////////////
/* Plug-ins can use their Initialization procedures to hook into Foxit Reader's 
	 * user interface by adding menu items, toolbar buttons, windows, and so on.
	 * It is also acceptable to modify Foxit Reader's user interface later when the plug-in is running.
	 */
void PILoadMenuBarUI(void* pParentWnd)
{

}

void PIReleaseMenuBarUI(void* pParentWnd)
{

}

//////////////////////////////////////////////////////////
void PILoadToolBarUI(void* pParentWnd)
{

}

void PIReleaseToolBarUI(void* pParentWnd)
{

}

void PILoadRibbonUI(void* pParentWnd)
{

}

void PILoadStatusBarUI(void* pParentWnd)
{

}

/* PIInit
** ------------------------------------------------------
**/
/** 
	The main initialization routine.
	
	@return true to continue loading the plug-in, 
	false to cause plug-in loading to stop.
*/
FS_BOOL PIInit(void)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    if (!theApp.CheckMarkanyDll())
        return FALSE;

    // MultiLang init
    theApp.fr_lang_ = std::unique_ptr<_t_FR_Language, decltype(FRLanguageRelease)>(FRLanguageCreate(AfxGetInstanceHandle()), FRLanguageRelease);
    FRLanguageChange(theApp.fr_lang_.get(), FRLanguageGetCurrentID());

    if (!theApp.drm_func_)
        theApp.drm_func_ = std::make_unique<MarkanyDRMHelper>();

    if (!theApp.drm_func_)
        return FALSE;

    if (!theApp.drm_func_->LoadDRMFunctions()) {
        return FALSE;
    }

    // Initialize Markany DRM system
    if (!theApp.drm_func_->mds_initialize(L"{0164E13B-F39B-4774-9D9F-6FEF4F4E5B11}")) 
        return FALSE;        

    // register callbacks
    if (!theApp.content_provider_)
        theApp.content_provider_ = std::make_unique<ContentProvider>();
    FRAppRegisterContentProvider(theApp.content_provider_->GetContentProviderCallbacks());

    if (!theApp.doc_manager_)
        theApp.doc_manager_ = std::make_unique<MarkanyDocManager>();

    if (!theApp.pei_provider_)
        theApp.pei_provider_ = std::make_unique<PrintExtraInfoProvider>();
    FRAppRegisterExtraPrintInfoProvider(theApp.pei_provider_->GetExtraPrintInfoCallbacks());

    if (!theApp.de_provider_)
        theApp.de_provider_ = std::make_unique<DocumentEventProvider>();
    FRAppRegisterDocHandlerOfPDDoc(theApp.de_provider_->GetDocEventCallbacks());

    if (!theApp.sm_provider_)
        theApp.sm_provider_ = std::make_unique<SecurityMethodProvider>();
    FRAppRegisterSecurityMethod(theApp.sm_provider_->GetSecurityMethodsCallbacsk());

    if (!theApp.ae_provider_)
        theApp.ae_provider_ = std::make_unique<AppEventProvider>();
    FRAppRegisterAppEventHandler(theApp.ae_provider_->GetAppEventCallbacks());
    
	return TRUE;
}

/* PIUnload
** ------------------------------------------------------
**/
/** 
	The unload routine.
	Called when your plug-in is being unloaded when the application quits.
	Use this routine to release any system resources you may have
	allocated.

	Returning false will cause an alert to display that unloading failed.
	@return true to indicate the plug-in unloaded.
*/
FS_BOOL PIUnload(void)
{
    // uninitialize Markany DRM system
    if (theApp.drm_func_)
        theApp.drm_func_->mds_finalize();

	return TRUE;
}

/** PIHandshake
	function provides the initial interface between your plug-in and the application.
	This function provides the callback functions to the application that allow it to 
	register the plug-in with the application environment.

	Required Plug-in handshaking routine:
	
	@param handshakeVersion the version this plug-in works with. 
	@param handshakeData OUT the data structure used to provide the primary entry points for the plug-in. These
	entry points are used in registering the plug-in with the application and allowing the plug-in to register for 
	other plug-in services and offer its own.
	@return true to indicate success, false otherwise (the plug-in will not load).
*/
FS_BOOL PIHandshake(FS_INT32 handshakeVersion, void *handshakeData)
{
	if(handshakeVersion != HANDSHAKE_V0100)
		return FALSE;
	
	/* Cast handshakeData to the appropriate type */
	PIHandshakeData_V0100* pData = (PIHandshakeData_V0100*)handshakeData;
	
	/* Set the name we want to go by */
	pData->PIHDRegisterPlugin(pData, "MarkanyDRM", (FS_LPCWSTR)L"MarkanyDRM");

	/* If you export your own HFT, do so in here */
	pData->PIHDSetExportHFTsCallback(pData, &PIExportHFTs);
		
	/*
	** If you import plug-in HFTs, replace functionality, and/or want to register for notifications before
	** the user has a chance to do anything, do so in here.
	*/
	pData->PIHDSetImportReplaceAndRegisterCallback(pData, &PIImportReplaceAndRegister);

	/* Perform your plug-in's initialization in here */
	pData->PIHDSetInitDataCallback(pData, &PIInit);

	PIInitUIProcs initUIProcs;
	INIT_CALLBACK_STRUCT(&initUIProcs, sizeof(PIInitUIProcs));
	initUIProcs.lStructSize = sizeof(PIInitUIProcs);
	initUIProcs.PILoadMenuBarUI = PILoadMenuBarUI;
	initUIProcs.PIReleaseMenuBarUI = PIReleaseMenuBarUI;
	initUIProcs.PILoadToolBarUI = PILoadToolBarUI;
	initUIProcs.PIReleaseToolBarUI = PIReleaseToolBarUI;
	initUIProcs.PILoadRibbonUI = PILoadRibbonUI;
	initUIProcs.PILoadStatusBarUI = PILoadStatusBarUI;
	pData->PIHDSetInitUICallbacks(pData, &initUIProcs);

	/* Perform any memory freeing or state saving on "quit" in here */
	pData->PIHDSetUnloadCallback(pData, &PIUnload);

	return TRUE;
}

