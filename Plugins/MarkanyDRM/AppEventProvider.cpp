#include "stdafx.h"
#include "MarkanyDRM.h"
#include "AppEventProvider.h"
#include "MultiLang.h"

#include <vector>

extern MarkanyDRMApp theApp;

// ctor
AppEventProvider::AppEventProvider() : app_callbacks_(new FR_AppEventCallbacksRec) {
    std::memset(app_callbacks_.get(), 0, sizeof(FR_AppEventCallbacksRec));

    app_callbacks_->lStructSize = sizeof(FR_AppEventCallbacksRec);
    app_callbacks_->clientData = nullptr;

    app_callbacks_->FRAppOnOpenDocument = OnOpenDocument;
    app_callbacks_->FRAppOnLangUIChange = OnLangUIChange;
}

// callbacks
FS_BOOL AppEventProvider::OnOpenDocument(FS_LPVOID clientData, FS_LPCWSTR lpszFilePath) {
     if (!theApp.drm_func_->mds_isdrmfile(lpszFilePath))
        return FALSE;

    HANDLE handle = CreateFile(lpszFilePath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

    if (handle == INVALID_HANDLE_VALUE) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_OPEN_FILE, MB_OK | MB_ICONERROR);
        return TRUE;
    }

    if (!theApp.drm_func_->mds_isgoodtogo(lpszFilePath, handle)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_NO_RIGHT_TO_READ, MB_OK | MB_ICONERROR);
        CloseHandle(handle);
        return TRUE;
    }

    DWORD bytes = 0;
    DWORD total_bytes = 0;
    char buffer[4096] = { 0 };
    std::vector<char> data;
    data.reserve(16384);

    while (theApp.drm_func_->mds_ReadFile(handle, buffer, 4096, &bytes)) {
        if (bytes == 0)
            break;

        std::copy(buffer, buffer + bytes, std::back_inserter(data));
        total_bytes += bytes;
        std::memset(buffer, 0, 4096);
    }

    if (!theApp.drm_func_->mds_registeropenfile(lpszFilePath)) {
        MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_CANNOT_REGISTER_FILE, MB_OK | MB_ICONERROR);
        CloseHandle(handle);
        return TRUE;
    }

    theApp.doc_manager_->AddMarkanyDoc(lpszFilePath, handle, data.data(), static_cast<std::size_t> (total_bytes));

    return FALSE;
}

void AppEventProvider::OnLangUIChange(FS_LPVOID clientData) {
    if (theApp.fr_lang_.get())
        FRLanguageChange(theApp.fr_lang_.get(), FRLanguageGetCurrentID());
}
