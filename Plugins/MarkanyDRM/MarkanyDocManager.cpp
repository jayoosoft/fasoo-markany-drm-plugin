#include "stdafx.h"
#include "MarkanyDocManager.h"

#include <sstream>
// ctors

// dtor

// methods
void MarkanyDocManager::AddMarkanyDoc(const std::wstring&path, HANDLE handle, char* data, std::size_t data_size) {
    this->unopened_doc_.reset(new MarkanyDocument(path, handle, data, data_size));
}

void MarkanyDocManager::RemoveMarkanyDoc(FR_Document fr_doc) {
    docs_.erase(fr_doc);
}

const MarkanyDocument* MarkanyDocManager::UpdateMarkanyDoc(const FS_LPCWSTR path, FR_Document fr_doc)
{
    if (!this->unopened_doc_)
        return nullptr;

    assert(this->unopened_doc_->GetDocPath() == path);
    assert(docs_.find(fr_doc) == docs_.end());

    const auto& ins_pair = docs_.emplace(fr_doc, std::move(this->unopened_doc_)).first;
    return ins_pair->second.get();
}

const MarkanyDocument* MarkanyDocManager::GetMarkanyDoc(FR_Document fr_doc) const {
    const auto& doc = docs_.find(fr_doc);
    return doc != docs_.end() ? doc->second.get() : nullptr;
}

MarkanyDocument* MarkanyDocManager::GetMarkanyDoc(FR_Document fr_doc) {
    auto& doc = docs_.find(fr_doc);

    if (doc != docs_.end())
        return doc->second.get();

    return nullptr;
}
