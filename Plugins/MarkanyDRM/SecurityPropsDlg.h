#pragma once

//#include "MarkanyDocument.h"

// SecurityPropsDlg dialog
class MarkanyDocument;

class SecurityPropsDlg : public CDialogEx
{
	DECLARE_DYNAMIC(SecurityPropsDlg)

public:
	SecurityPropsDlg(MarkanyDocument* doc, CWnd* pParent = nullptr);   // standard constructor
	virtual ~SecurityPropsDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SECURITY_PROPS };
#endif

protected:
    virtual BOOL OnInitDialog();

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
    FR_RibbonStyleStatic summary_ { nullptr };
    FR_RibbonStyleStatic save_doc_ { nullptr };
    FR_RibbonStyleStatic save_doc_perm_ { nullptr };
    FR_RibbonStyleStatic edit_doc_ { nullptr };
    FR_RibbonStyleStatic edit_doc_perm_ { nullptr };
    FR_RibbonStyleStatic doc_extraction_ { nullptr };
    FR_RibbonStyleStatic doc_extraction_perm_ { nullptr };
    FR_RibbonStyleStatic screen_capture_ { nullptr };
    FR_RibbonStyleStatic screen_capture_perm_ { nullptr };
    FR_RibbonStyleStatic print_watermark_ { nullptr };
    FR_RibbonStyleStatic print_watermark_perm_ { nullptr };
    FR_RibbonStyleStatic content_exp_days_ { nullptr };
    FR_RibbonStyleStatic content_exp_days_perm_ { nullptr };
    FR_RibbonStyleStatic content_print_count_ { nullptr };
    FR_RibbonStyleStatic content_print_count_perm_ { nullptr };
    FR_RibbonStyleStatic content_open_count_ { nullptr };
    FR_RibbonStyleStatic content_open_count_perm_ { nullptr };

    void LoadStaticStrings();
    void SetRibbonStatics();
    void LoadPermissions();
    void ReleaseRibbonStatics();
    FR_RibbonStyleStatic SetSmallFontStatic(UINT static_id);
    FR_RibbonStyleStatic SetLargeFontStatic(UINT static_id);

    MarkanyDocument* doc_;
};
