#include "stdafx.h"
#include "MarkanyDRM.h"
#include "MarkanyDocument.h"

extern MarkanyDRMApp theApp;

// ctor
MarkanyDocument::MarkanyDocument(const std::wstring& path, HANDLE handle, char* data, std::size_t data_size) 
    : doc_path_{ path },
    handle_{ handle },
    data_{new char[data_size]},
    data_size_{ data_size }
{
    std::memcpy(data_.get(), data, data_size_);

    doc_props_ = theApp.drm_func_->mds_GetEncFileInfo(path.c_str());
}

// dtor
MarkanyDocument::~MarkanyDocument() {
    
}

// methods
void MarkanyDocument::UpdateData(char* data, std::size_t data_size) {
    data_.reset(new char[data_size]);
    data_size_ = data_size;

    std::memcpy(data_.get(), data, data_size_);
}

void MarkanyDocument::ReloadDocumentProperties() {
    doc_props_ = theApp.drm_func_->mds_GetEncFileInfo(doc_path_.c_str());
}

bool MarkanyDocument::Reopen() {
    if (handle_) {
        theApp.drm_func_->mds_CloseHandle(handle_);
        theApp.drm_func_->mds_unregisteropenfile(doc_path_.c_str());

        handle_ = CreateFile(doc_path_.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (handle_ == INVALID_HANDLE_VALUE) {
            handle_ = NULL;
            return false;
        }

        theApp.drm_func_->mds_registeropenfile(doc_path_.c_str());

        return true;
    }

    return false;
}
