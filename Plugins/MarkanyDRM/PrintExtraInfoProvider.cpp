#include "stdafx.h"
#include "PrintExtraInfoProvider.h"
#include "MarkanyDRM.h"
#include "MultiLang.h"
#include "Resource.h"

#include <winspool.h>

extern MarkanyDRMApp theApp;

// ctor
PrintExtraInfoProvider::PrintExtraInfoProvider() : pei_callbacks_(new FR_ExtraPrintInfoProviderCallbackRec) {
    std::memset(pei_callbacks_.get(), 0, sizeof(FR_ExtraPrintInfoProviderCallbackRec));

    pei_callbacks_->clientData = nullptr;
    pei_callbacks_->lStructSize = sizeof(FR_ExtraPrintInfoProviderCallbackRec);
    pei_callbacks_->FRToMast = OnFRToMast;
    pei_callbacks_->FRCanBePrinted2 = OnFRCanBePrinted2;
}

// dtor

// methods
FS_BOOL PrintExtraInfoProvider::OnFRToMast(FS_LPVOID clientData, FR_Document frDoc) {
    return theApp.doc_manager_->GetMarkanyDoc(frDoc) != nullptr;
}

FS_BOOL PrintExtraInfoProvider::OnFRCanBePrinted2(FS_LPVOID clientData,
    FR_Document frDoc,
    FS_INT32 copies,
    FS_BOOL bTotalPages,
    FS_DWordArray arrPages,
    FS_BOOL bPostScript,
    FS_BOOL bLocal,
    FS_BOOL bNetWork,
    FS_BOOL bShared,
    FS_WideString Printer,
    FS_ByteString PrinterModel,
    FS_ByteString bsSubset,
    FS_ByteString PrinterDriver,
    FS_ByteString PrinterData,
    FS_ByteString PrinterProt) 
{
    MarkanyDocument* doc = theApp.doc_manager_->GetMarkanyDoc(frDoc);

    if (!doc)
        return TRUE;

    HANDLE printer_handle = NULL;
    FS_LPCWSTR printer_name = FSWideStringCastToLPCWSTR(Printer);

    std::unique_ptr<_t_FS_WideString, decltype(FSWideStringDestroy)> printer_driver_str(FSWideStringNew(), FSWideStringDestroy);
    FSWideStringConvertFrom(printer_driver_str.get(), PrinterDriver, NULL);
    
    if (OpenPrinter((LPTSTR) printer_name, &printer_handle, NULL)) {
        if (printer_handle) {
            if (theApp.drm_func_->mds_issecureprinter(doc->GetDocPath().c_str(), printer_handle, (LPWSTR)printer_name, (LPWSTR)printer_driver_str.get())) {
                return TRUE;
            }
            else {
                MultiLang::ShowMessageBox(FRAppGetMainFrameWnd(), IDS_PRINTER_DISABLED, MB_OK | MB_ICONERROR);
                return FALSE;
            }

            CloseHandle(printer_handle);
        }
    }

    return FALSE;
}
