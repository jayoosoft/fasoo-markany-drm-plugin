#pragma once
#include "StdAfx.h"
#include <memory>

class AppEventProvider final
{
public:
    AppEventProvider();
    ~AppEventProvider() {}

    FR_AppEventCallbacks GetAppEventCallbacks() const noexcept { return app_callbacks_.get(); }

    static FS_BOOL OnOpenDocument(FS_LPVOID clientData, FS_LPCWSTR lpszFilePath);
    static void OnLangUIChange(FS_LPVOID clientData);

private:
    std::unique_ptr<FR_AppEventCallbacksRec> app_callbacks_;
};
