#pragma once

#include "StdAfx.h"

#include <memory>

class DocumentEventProvider final
{
public:
    DocumentEventProvider();
    ~DocumentEventProvider() {}

    FR_DocEventCallbacks GetDocEventCallbacks() const noexcept { return de_callbacks_.get(); }

    static void OnFRDocDidSave(FS_LPVOID clientData, FR_Document doc, FS_BOOL bSaveAs);
    static void OnFRDocOnActivate(FS_LPVOID clientData, FR_Document doc);
    static void OnFRDocOnDeactivate(FS_LPVOID clientData, FR_Document doc);
    static void OnFRDocWillPrint(FS_LPVOID clientData, FR_Document doc);
    static void OnFRDocDidPrint(FS_LPVOID clientData, FR_Document doc);
    static void OnFRDocOnFrameCreate(FS_LPVOID clientData, FR_Document doc, HWND hFrameWnd);

private:
    std::unique_ptr<FR_DocEventCallbacksRec> de_callbacks_;
};
