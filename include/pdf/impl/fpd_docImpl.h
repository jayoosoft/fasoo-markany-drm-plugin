﻿

#ifndef FPD_DOCIMPL_H
#define FPD_DOCIMPL_H

#ifndef FS_INTERNALINC_H
#include "../../basic/fs_internalInc.h"
#endif

#ifndef FPD_PARSEREXPT_H
#include "../fpd_parserExpT.h"
#endif

#ifndef FPD_DOCEXPT_H
#include "../fpd_docExpT.h"
#endif

#ifndef FPD_OBJSEXPT_H
#include "../fpd_objsExpT.h"
#endif

#ifndef FPD_RESOURCEIMPL_H
#include "../fpd_resourceExpT.h"
#endif

#ifndef FPD_RESOURCEIMPL_H
#include "../fpd_renderExpT.h"
#endif

/*@}*/ 


#ifdef __cplusplus
extern "C"{
#endif

/** @brief Page enumeration handler interface. */
class CFPD_CustomerEnumPageHandler : public IPDF_EnumPageHandler, public CFX_Object
{
public:
	CFPD_CustomerEnumPageHandler(FPD_EnumPage enumPage);
	/**
	 * Enumerates page function for call back. Return TRUE if you want the enumeration to continue.
	 *
	 * @param[in] pPageDict		The page dictionary. 
	 * @return Non-zero means you want the enumeration to continue, otherwise want not.
	 */
	virtual FX_BOOL EnumPage(CPDF_Dictionary* pPageDict);

	void* GetClientData(){return m_enumPage.clientData;}
public:
	FPD_EnumPage m_enumPage;
};


class CFPD_Doc_V1
{
public:
	//************************************
	// Function: New
	// Param[in]:    void
	// Return:   The new empty document.
	// Remarks:	 Creates a new empty document. </Brief> The only <a>FPD_Object</a> in the document will be a
	// Catalog dictionary. See <Italic>Section 3.6 in the PDF Reference</Italic>. 
	// Notes:
	//************************************      
	static FPD_Document	New(void);

	//************************************
	// Function:  Destroy 
	// Param[in]: doc	The document to destroy.	
	// Return:	  void
	// Remarks:   Destroys the document created by <a>FPDDocNew</a>. When Foxit Reader opens the doc, it is taken over and 
	// don't destroy it.
	// Notes:	
	//************************************
	static void	Destroy(FPD_Document doc);

	//************************************
	// Function:  Open
	// Param[in]: wszFilePath	The input file full path name.
	// Param[in]: szPassword	The input password string.
	// Return:	 The newly createddocument from an existing PDF file.
	// Remarks:	  Creates a new document from an existing PDF file. You must call <a>FPDDocClose</a>() once for every
	// successful open. 	
	// Notes:
	//************************************
	static FPD_Document	Open(FS_LPCWSTR wszFilePath, FS_LPCSTR szPassword);

	//************************************
	// Function:  OpenMemDocument
	// Param[in]: dataBuf	The input memory block.
	// Param[in]: size		The size in bytes of the memory block.
	// Param[in]: szPassword	The input password string.
	// Return:	  The newly created document from memory block.
	// Remarks:   Creates a  document from memory block. You must call <a>FPDDocClose</a>() once for every
	// successful open. 
	// Notes:
	//************************************
	static FPD_Document	OpenMemDocument(void* dataBuf, FS_INT32 size, FS_LPCSTR szPassword);

	//************************************
	// Function:  StartProgressiveLoad
	// Param[in]: fileRead		The file access handler. Plug-ins should free the object after the document is parsed completely.
	// Param[in]: szPassword	The input password string.
	// Param[in]: pause			The pause handler. This can be <a>NULL</a> if you don't want to pause.
	// Param[out]: outPDDoc		Receiving the document.
	// Return:	  The result code. See <a>FPDLoadErrCodes</a>.
	// Remarks:   Document loading is a progressive process. It might take a long time to
	// load a document, especially when a file is corrupted, FPDFEMB will try to recover the document contents by scanning the whole 
	// file. If "pause" parameter is provided, this function may return FPDFERR_TOBECONTINUED any time during the document loading.<br>
	// When <a>FPD_LOADERR_TOBECONTINUED</a> is returned, the "outPDDoc" parameter will still receive a valid document handle, however, 
	// no further operations can be performed on the document, except the <a>FPDDocContinueLoad</a>() function call, which resume the 
	// document loading. 
	// Notes:
	//************************************
	static FS_RESULT StartProgressiveLoad(FS_FileReadHandler fileRead, FS_LPCSTR szPassword, FS_PauseHandler pause, FPD_Document* outPDDoc);
	
	//************************************
	// Function:  ContinueLoad
	// Param[in]: doc	The PDF document.
	// Param[in]: pause		The pause handler. This can be <a>NULL</a> if you don't want to pause.
	// Return:	  The result code. See <a>FPDLoadErrCodes</a>.
	// Remarks:   Continue loading a PDF document. You must call <a>FPDDocClose</a>() once for every
	// successful open. 
	// Notes:
	//************************************
	static FS_RESULT ContinueLoad(FPD_Document doc, FS_PauseHandler pause);
	

	//************************************
	// Function:  Close 
	// Param[in]: doc	The document returned by <a>FPDDocOpen</a>(), <a>FPDDocStartProgressiveLoad</a>() or <a>FPDDocOpenMemDocument</a>().
	// Return:    Error code. FPD_LOADERR_SUCCESS for success.
	// Remarks:   Closes the document returned by <a>FPDDocOpen</a>(), <a>FPDDocStartProgressiveLoad</a>() or <a>FPDDocOpenMemDocument</a>(). 	
	// Notes: Call <a>FPDDocDestroy</a>() to release a document which created by <a>FPDDocNew</a>().
	//************************************
	static FS_RESULT	Close(FPD_Document doc);

	//************************************
	// Function:  GetRoot
	// Param[in]: doc	A PDF document.
	// Return:    The root dictionary (document catalog dictionary).
	// Remarks:   Gets root dictionary (document catalog dictionary) from a PDF document.
	// Notes:
	//************************************
	static FPD_Object GetRoot(FPD_Document doc);

	//************************************
	// Function:  GetInfo
	// Param[in]: doc	A PDF document.
	// Return:    The document info dictionary.
	// Remarks:   Gets document info dictionary from a PDF document. Could be NULL.
	// Notes:
	//************************************
	static FPD_Object GetInfo(FPD_Document doc);

	//************************************
	// Function:  GetID
	// Param[in]: doc		A PDF document.
	// Param[out]:outID1	It receives the first ID string of the document.
	// Param[out]:outID2	It receives the second ID string of the document.
	// Return:    void
	// Remarks:   Gets ID strings of the document. Could be empty.
	// Notes:
	//************************************
	static void	GetID(FPD_Document doc,FS_ByteString* outID1, FS_ByteString* outID2);

	//************************************
	// Function:  GetPageCount
	// Param[in]: doc
	// Return: The page count.
	// Remarks:   Gets number of pages in this document.
	// Notes:
	//************************************
	static FS_INT32	GetPageCount(FPD_Document doc);

	//************************************
	// Function:  GetPage
	// Param[in]: doc			A PDF document.
	// Param[in]: page_index	Specifies the zero-based index of the page in the document.
	// Return:    The specified page dictionary of the document.
	// Remarks:   Gets page dictionary of a specified page of the document. Page index starts from zero for the first page.
	// Notes:
	//************************************
	static FPD_Object GetPage(FPD_Document doc, FS_INT32 page_index);	
	
	//************************************
	// Function:  GetPageIndex
	// Param[in]: doc		A PDF document.
	// Param[in]: objNum	The input indirect object number int the document.
	// Return:    The zero-based index of page in the document.
	// Remarks:   Gets the page index from a indirect object number in the document.
	// Notes:
	//************************************
	static FS_INT32	GetPageIndex(FPD_Document doc, FS_DWORD objNum);

	//************************************
	// Function:  GetUserPermissions
	// Param[in]: doc The input PDF document.
	// Return:    The user permission of the document.
	// Remarks:   Gets the user permission of the document.
	// Notes:
	//************************************
	static FS_DWORD	GetUserPermissions(FPD_Document doc);

	//************************************
	// Function:  IsOwner
	// Param[in]: doc The input PDF document.
	// Return:    <a>TRUE</a> for being owner.
	// Remarks:   Whether the user has the owner permission of the document.
    // Notes:
	//************************************
	static FS_BOOL IsOwner(FPD_Document doc);

	//************************************
	// Function:  IsFormStream	
	// Param[in]: doc		A PDF document.
	// Param[in]: objnum	The input indirect object number.
	// Param[out]:outIsFormStream	It receive whether it's a form stream or not.
	// Return:    Non-zero means determined, otherwise unknown.
	// Remarks:   Checks if an indirect object is a form stream or not, without actually loading the object.
	// Notes:
	//************************************
	static FS_BOOL IsFormStream(FPD_Document doc,FS_DWORD objnum, FS_BOOL* outIsFormStream);


	//************************************
	// Function:  LoadFont
	// Param[in]: doc		A PDF document.
	// Param[in]: fontDict	The input font dictionary.
	// Return:    A PDF font.
	// Remarks:   Loads a font from font dictionary in the document.
	// Notes:
	//************************************
	static FPD_Font	LoadFont(FPD_Document doc, FPD_Object fontDict);

	//************************************
	// Function:  LoadColorSpace
	// Param[in]: doc		A PDF document.
	// Param[in]: csObj		The input PDF object.
	// Return:    A PDF color space.
	// Remarks:   Loads a color space from a PDF object in the document.
	// Notes:
	//************************************
	static FPD_ColorSpace LoadColorSpace(FPD_Document doc, FPD_Object csObj);

	//************************************
	// Function:  LoadPattern
	// Param[in]: doc		A PDF document.
	// Param[in]: csObj		The input PDF object.
	// Param[in]: bShading	Whether the pattern is a shading pattern or not.
	// Return:    A PDF pattern.
	// Remarks:   Loads a pattern from a PDF object in the document.
	// Notes:
	//************************************
	static FPD_Pattern LoadPattern(FPD_Document doc, FPD_Object csObj, FS_BOOL bShading);

	//************************************
	// Function:  LoadImageF
	// Param[in]: doc		A PDF document.
	// Param[in]: obj		The input PDF object.
	// Param[in]: bCache	Whether we will cache the image or not.
	// Return:    A PDF image.
	// Remarks:   Loads an image from a PDF object in the document.
	// Notes:
	//************************************
	static FPD_Image LoadImageF(FPD_Document doc, FPD_Object obj, FS_BOOL bCache);

	//************************************
	// Function:  LoadFontFile
	// Param[in]: doc	A PDF document.
	// Param[in]: stm	The input PDF stream.
	// Return:    A PDF stream accessor.
	// Remarks:   Loads a PDF stream accessor from a PDF stream in the document.
	// Notes:
	//************************************
	static FPD_StreamAcc LoadFontFile(FPD_Document doc, FPD_Object stm);

	//************************************
	// Function:  GetInfoObjNum
	// Param[in]: doc The input PDF document.
	// Return:    The number of info object.
	// Remarks:   Gets the number of info object.
	// Notes:
	//************************************
	static FS_DWORD	GetInfoObjNum(FPD_Document doc);

	//************************************
	// Function:  GetRootObjNum
	// Param[in]: doc The input PDF document.
	// Return:    The number of root object.
	// Remarks:   Gets the number of root object.
	// Notes:
	//************************************
	static FS_DWORD	GetRootObjNum(FPD_Document doc);

	//************************************
	// Function:  EnumPages
	// Param[in]: doc				A PDF document.
	// Param[in]: pEnumPageHandler	The user-supplied page enumeration handler.
	// Return:    void
	// Remarks:   Enumerates pages with user-supplied page enumeration handler.
	// Notes:
	//************************************
	static void	EnumPages(FPD_Document doc, FPD_EnumPageHandler pEnumPageHandler);

	//************************************
	// Function:  NewEnumPageHandler 
	// Param[in]: enumPage	The user-supplied page enumeration struct.
	// Return:    The user-supplied page enumeration handler.
	// Remarks:   Creates user-supplied page enumeration handler.
	// Notes:
	//************************************
	static FPD_EnumPageHandler NewEnumPageHandler(FPD_EnumPage enumPage);	

	//************************************
	// Function:  DeleteEnumPageHandler
	// Param[in]: pEnumPageHandler	The user-supplied page enumeration handler.
	// Return:    void
	// Remarks:   Deletes the user-supplied page enumeration handler.
	// Notes:
	//************************************
	static void	DeleteEnumPageHandler(FPD_EnumPageHandler pEnumPageHandler);


	//************************************
	// Function:  SetRootObjNum	
	// Param[in]: doc			A empty PDF document.
	// Param[in]: RootObjNum	The input root object number.
	// Return:    void
	// Remarks:   Sets the root object number in the PDF document. Must be called for an empty document.
	// Notes:
	//************************************
	static void	SetRootObjNum(FPD_Document doc, FS_INT32 RootObjNum);

	//************************************
	// Function:  SetInfoObjNum	
	// Param[in]: doc			A empty PDF document.
	// Param[in]: InfoObjNum	The input info object number.
	// Return:    void
	// Remarks:   Sets the info object number in the PDF document. Must be called for an empty document.
	// Notes:
	//************************************
	static void	SetInfoObjNum(FPD_Document doc, FS_INT32 InfoObjNum);

	//************************************
	// Function:  SetID
	// Param[in]: doc		A PDF document.
	// Param[in]: szID1		The first file ID.
	// Param[in]: nLen1		The length of the first ID byte string.
	// Param[in]: szID2		The second file ID.
	// Param[in]: nLen2		The length of the second ID byte string.
	// Return:    void
	// Remarks:   Sets the PDF file ID.
	// Notes:
	//************************************
	static void	SetID(FPD_Document doc, FS_LPCBYTE szID1, FS_INT32 nLen1, FS_LPCBYTE szID2, FS_INT32 nLen2);

	//************************************
	// Function:  AddWindowsFont
	// Param[in]: doc				A PDF document.
	// Param[in]: pLogFont			Points to a LOGFONT(WIN32) structure that defines the characteristics of the logical font.
	// Param[in]: bVert				Whether the font is a vertical font or not.
	// Param[in]: bTranslateName	Whether we will do font name translating or not.
	// Return:    A PDF font.
	// Remarks:   Imports a Windows font into the PDF document.
	// Notes:
	//************************************
	static FPD_Font	AddWindowsFont(FPD_Document doc, void* pLogFont, FS_BOOL bVert, FS_BOOL bTranslateName);

	//************************************
	// Function:  AddWindowsFontW
	// Param[in]: doc				A PDF document.
	// Param[in]: pLogFont			Points to a LOGFONTW(WIN32) structure that defines the characteristics of the logical font.
	// Param[in]: bVert				Whether the font is a vertical font or not.
	// Param[in]: bTranslateName	Whether we will do font name translating or not.
	// Return:    A PDF font.
	// Remarks:	  Imports a Windows font into the PDF document.
	// Notes:
	//************************************
	static FPD_Font	AddWindowsFontW(FPD_Document doc, void* pLogFont, FS_BOOL bVert, FS_BOOL bTranslateName);
	
	//************************************
	// Function:  AddStandardFont
	// Param[in]: doc		A PDF document.
	// Param[in]: font		The font name.
	// Param[in]: encoding	The font encoding.
	// Return:    
    // Remarks:   Adds a Type1 base-14 font to the PDF document.
	// Notes:
	//************************************
	static FPD_Font	AddStandardFont(FPD_Document doc, const FS_CHAR* font, FPD_FontEncoding encoding);

	//************************************
	// Function:  BuildResourceList
	// Param[in]: doc	A PDF document.
	// Param[in]: type	The name of specified type.
	// Param[out]:arr	It will receive all resource objects of specified type in the document.
	// Return:    void
	// Remarks:   Build a list of all resources of specified type in this document. On return,
	//            the array contains pointers to FPD_Object objects.
	// Notes:
	//************************************
	static void	BuildResourceList(FPD_Document doc, const FS_CHAR* type, FS_PtrArray arr);

	//************************************
	// Function:  CreateNewPage
	// Param[in]: doc		A PDF document.
	// Param[in]: iPage		Specifies the zero-based index of page to be created.
	// Return:    The created page dictionary.
	// Remarks:   Creates a new page in the PDF document. Return the created page.
	// Notes:
	//************************************
	static FPD_Object CreateNewPage(FPD_Document doc, FS_INT32 iPage);

	//************************************
	// Function:  DeletePage
	// Param[in]: doc		A PDF document.
	// Param[in]: iPage		Specifies the zero-based index of page to be deleted.
	// Return:    void
	// Remarks:   Deletes specified page in the PDF document.
	// Notes:
	//************************************
	static void	DeletePage(FPD_Document doc, FS_INT32 iPage);

	//************************************
	// Function:  ConvertIndirectObjects
	// Param[in]: doc					A PDF document.
	// Param[in]: obj					The input PDF object.
	// Param[in]: bConvertStream		Whether we will convert stream to indirect object or not.
	// Param[in]: bConvertDictionary	Whether we will convert dictionary to indirect object or not.
	// Return:    void
	// Remarks:   Converts stream of dictionary members of an object to be indirect objects.
	// Notes:
	//************************************
	static void	ConvertIndirectObjects(FPD_Document doc, FPD_Object obj, FS_BOOL bConvertStream, FS_BOOL bConvertDictionary);
	
	//************************************
	// Function:  GetPageContentModify
	// Param[in]: doc			A PDF document.
	// Param[in]: page_dict		The input page dictionary.
	// Return:    An modifiable content stream for the page.
	// Remarks:   Gets a modifiable content stream for a page in the PDF document.
	// Notes:
	//************************************
	static FPD_Object GetPageContentModify(FPD_Document doc, FPD_Object page_dict);

	//************************************
	// Function:  QuickSearch
	// Param[in]: doc				The input PDF document.
	// Param[in]: page_index		The zero-based page index to be searched.
	// Param[in]: pattern			The pattern to search.
	// Param[in]: bCaseSensitive	Whether the pattern matching is case sensitive.
	// Return:    Non-zero means searched one successfully.
	// Remarks:   Quick search an pattern for specified page in the PDF document.
	// Notes:
	//************************************
	static FS_BOOL QuickSearch(FPD_Document doc, FS_INT32 page_index, FS_LPCWSTR pattern, FS_BOOL bCaseSensitive);
	
	//************************************
	// Function:  ReloadPages
	// Param[in]: doc	A PDF document.
	// Return:    void
	// Remarks:   Reload page list. This can be used when document is progressively downloaded.
	// Notes:
	//************************************
	static void	ReloadPages(FPD_Document doc);

	//************************************
	// Function:  LoadDoc
	// Param[in]: doc The input PDF document.
	// Return:    void
	// Remarks:   Internally used
	// Notes:
	//************************************
	static void	LoadDoc(FPD_Document doc);

	//************************************
	// Function:  GetIndirectObject
	// Param[in]: doc		The input document.
	// Param[in]: objNum	The input object number.
	// Return:    An indirect object.
	// Remarks:   Loads an indirect object by an object number.
	// Notes:
	//************************************
	static FPD_Object GetIndirectObject(FPD_Document doc, FS_DWORD objNum);

	//************************************
	// Function:  GetIndirectType
	// Param[in]: doc		The input document.
	// Param[in]: objNum	The input object number.
	// Return:    The type of the document.
	// Remarks:   Gets type of an document, without loading the object content.
	// Notes:
	//************************************
	static FS_INT32	GetIndirectType(FPD_Document doc, FS_DWORD objNum);

	//************************************
	// Function:  AddIndirectObject
	// Param[in]: doc		The input document.
	// Param[in]: obj		The input object.
	// Return:    The new object number.
	// Remarks:   Adds an object to indirect object list. The new object number is returned.
	// Notes:
	//************************************
	static FS_DWORD	AddIndirectObject(FPD_Document doc, FPD_Object obj);

	//************************************
	// Function:  ReleaseIndirectObject
	// Param[in]: doc		The input document.
	// Param[in]: objNum	The object number to release.
	// Return:    void
	// Remarks:   Sometimes, for saving memory space, we can release a loaded indirect object.
	//            However, use this with caution because the object pointer will become invalid.
	// Notes:
	//************************************
	static void	ReleaseIndirectObject(FPD_Document doc, FS_DWORD objNum);

	//************************************
	// Function:  DeleteIndirectObject
	// Param[in]: doc		The input document.
	// Param[in]: objNum	The object number to delete.
	// Return:    void
	// Remarks:   Deletes an indirect object. Use this function with caution!
	// Notes:
	//************************************
	static void	DeleteIndirectObject(FPD_Document doc, FS_DWORD objNum);

	//************************************
	// Function:  ImportIndirectObject
	// Param[in]: doc		The input document.
	// Param[in]: pBuffer	The binary data for the document. It must be not encrypted.
	// Param[in]: size		The size in bytes of the binary data.
	// Return:    An object.
	// Remarks:   Imports an object from its binary format.
	//            This is used when the PDF is fetched in "Progressive Downloading" fashion.
	//            After this function call, the data buffer can be destroyed.
	//            This object must not be encrypted.
	// Notes:
	//************************************
	static FPD_Object ImportIndirectObject(FPD_Document doc, FS_LPBYTE pBuffer, FS_DWORD size);

	//************************************
	// Function:  ImportExternalObject
	// Param[in]: doc		The input document.
	// Param[in]: obj		The object in external object collection.
	// Param[out]:mapping   It updates the mapping from old object number to new object number.	
	// Return:    A new indirect object.
	// Remarks:   Imports an object from external object collection as a new document.
	//            If the external object refers to other external indirect objects, those indirect objects
	//            are also imported.
	//            The mapping from old object number to new object number is updated during the import process.
	// Notes:
	//************************************
	static FPD_Object ImportExternalObject(FPD_Document doc, FPD_Object obj, FS_MapPtrToPtr mapping);

	//************************************
	// Function:  InsertIndirectObject
	// Param[in]: doc		The input document.
	// Param[in]: objNum	The new object number of the indirect object to insert.
	// Param[in]: obj		The indirect object to insert.
	// Return:    void
	// Remarks:   Inserts an indirect object with specified object number.
	//			  This is used when the PDF is fetched in "Progressive Downloading" fashion, or parsing FDF.
	//            If an indirect object with the same object number exists, the previous one will be destroyed.
	// Notes:
	//************************************
	static void	InsertIndirectObject(FPD_Document doc, FS_DWORD objNum, FPD_Object obj);

	//************************************
	// Function:  GetLastobjNum
	// Param[in]: doc		The input document.
	// Return: The number of the last object.
	// Remarks:   Gets last assigned indirect object number.
	// Notes:
	//************************************
	static FS_DWORD	GetLastobjNum(FPD_Document doc);

	//************************************
	// Function:  ReloadFileStreams
	// Param[in]: doc		The input document.
	// Return:    void
	// Remarks:   Reload all file based stream when we do reparsing.
	// Notes:
	//************************************
	static void	ReloadFileStreams(FPD_Document doc);

	//************************************
	// Function:  GetStartPosition
	// Param[in]: doc		The input document.
	// Return: The start position of the indirect objects.
	// Remarks:   Gets the start position of the indirect objects.
	// Notes:
	//************************************
	static FS_POSITION GetStartPosition(FPD_Document doc);

	//************************************
	// Function:  GetNextAssoc
	// Param[in]: doc			The input document.
	// Param[in,out]:outPos		Input current position and receive the next position.
	// Param[out]:outObjNum		It receives the current object number.
	// Param[out]:outObject		It receives the current indirect object pointer.
	// Return:    void
	// Remarks:   Accesses the indirect object of current position, and move the position to next.
	// Notes:
	//************************************
	static void	GetNextAssoc(FPD_Document doc, FS_POSITION* outPos, FS_DWORD* outObjNum, FPD_Object* outObject);

	//************************************
	// Function:  IsModified
	// Param[in]: doc		The input document.
	// Return: <a>TRUE</a> if the document is modified, otherwise not. 
	// Remarks:   Checks if any of the indirect objects are modified, since loading from parser, or last ClearModified.
	// Notes:
	//************************************
	static FS_BOOL IsModified(FPD_Document doc);

	//************************************
	// Function:  ClearModified
	// Param[in]: doc		The input document.
	// Return:    void
	// Remarks:   Clears the modified flag.
	// Notes:
	//************************************
	static void	ClearModified(FPD_Document doc);

	//************************************
	// Function:  Save
	// Param[in]: doc		The input document.
	// Param[in]: filename	The output file name.
	// Param[in]: flags		The saving flags. You can set FPD_SAVE_DEFAULT by default.
	// Param[in]: bEnable	Whether to do data compressing.
	// Return:    Non-zero means success, otherwise failed.
	// Remarks:   Writes the whole document into a new file.
	// Notes:
	//************************************
	static FS_BOOL	Save(FPD_Document doc, FS_LPCSTR filename, FS_DWORD flags, FS_BOOL bEnable);

	//************************************
	// Function:  GetParser
	// Param[in]: doc		The input document.
	// Return:    The PDF parser.
	// Remarks:   Gets the PDF file parser associated with a <a>FPD_Document</a> object.
	// Notes:
	//************************************
	static FPD_Parser GetParser(FPD_Document doc);

	//************************************
	// Function:  CheckSignature
	// Param[in]: doc The input document.
	// Return: <a>TRUE</a> if the document is signed, otherwise not.
	// Remarks: Checks whether the document is signed or not.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1
	//************************************
	static FS_BOOL CheckSignature(FPD_Document doc);

	//************************************
	// Function:  GenerateFileID
	// Param[in]: dwSeed1	A seed value to initialize MT generator.
	// Param[in]: dwSeed2	A seed value to initialize MT generator.
	// Param[out]: pBuffer	It receives the file ID.
	// Return: <a>TRUE</a> for success, otherwise failure.
	// Remarks: Generates the ID of the PDF document.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static FS_BOOL GenerateFileID(FS_DWORD dwSeed1, FS_DWORD dwSeed2, FS_LPDWORD pBuffer);

	//************************************
	// Function:  GetReviewType
	// Param[in]: doc				The document.
	// Return: The review type. 0:normal, 1:share, 2:email.
	// Remarks: Gets the review type. 0:normal, 1:share, 2:email.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static FS_INT32 GetReviewType(FPD_Document doc);

	//************************************
	// Function:  IsPortfolio
	// Param[in]: doc	The input PDF document.
	// Return: TRUE if the document is a portfolio document, otherwise not.
	// Remarks: Checks whether the document is a portfolio document or not.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static FS_BOOL IsPortfolio(FPD_Document doc);

	//************************************
	// Function:  RemoveUR3
	// Param[in]: doc	The input PDF document.
	// Return: TRUE for success, otherwise failure.
	// Remarks: Removes the UR3 signature.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1.0.0
	//************************************
	static FS_BOOL RemoveUR3(FPD_Document doc);

	//************************************
	// Function:  AddFXFont
	// Param[in]: doc		The input PDF document.
	// Param[in]: pFXFont	The input Foxit GE font object.
	// Param[in]: charset	The input Charset ID.
	// Param[in]: bVert		Whether it's a vertical writing font. For CJK charsets only.
	// Return: The PDF font object.
	// Remarks: Adds the Foxit GE font to the PDF document. It will return a PDF font object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.0.0
	//************************************
	static FPD_Font AddFXFont(FPD_Document doc, FPD_FXFont pFXFont, FS_INT32 charset, FS_BOOL bVert);

	//************************************
	// Function:  GetIndirectObjsCount
	// Param[in]: doc	The input PDF document.
	// Return: The indirect object count.
	// Remarks: Gets the indirect object count.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1.0
	//************************************
	static FS_INT32 GetIndirectObjsCount(FPD_Document doc);

	//************************************
	// Function:  Save2
	// Param[in]: doc		The input document.
	// Param[in]: wStrfilename	The output file name,wide string type.
	// Param[in]: flags		The saving flags. You can set FPD_SAVE_DEFAULT by default.
	// Param[in]: bEnable	Whether to do data compressing.
	// Return:    Non-zero means success, otherwise failed.
	// Remarks:   Writes the whole document into a new file.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3.1
	//************************************
	static FS_BOOL Save2(FPD_Document doc, FS_LPCWSTR wStrfilename, FS_DWORD flags, FS_BOOL bEnable);

	//************************************
	// Function:  InsertDocumentAtPos
	// Param[in]: doc				The input document.
	// Param[in]: index				The page index where the document will be inserted.
	// Param[in]: insertedDoc		The inserted document.
	// Param[in]: pBookmarkTitle	The input bookmark title. Set NULL as default.
	// Return:    TRUE means success, otherwise failed.
	// Remarks:   Inserts the document into the original document at the specified position.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.4.0
	//************************************
	static FS_BOOL InsertDocumentAtPos(FPD_Document doc, FS_INT32 index, FPD_Document insertedDoc, const FS_WCHAR* pBookmarkTitle);

	//************************************
	// Function:  MetadataGetAllCustomKeys
	// Param[in]: doc				The input document.
	// Param[out]: keys				All the custom keys in the info dictionary.
	// Return:  The number of all the custom keys.
	// Remarks: Get all custom keys in metadata.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7
	//************************************
	static FS_INT32 MetadataGetAllCustomKeys(FPD_Document doc, FS_WideStringArray keys);

	//************************************
	// Function:  MetadataDeleteCustomKey
	// Param[in]: doc				The input document.
	// Param[in]: wsItem			The custom keys to be deleted.
	// Return:  whether delete the custom key success.
	//			if the key is a standard key, return false.
	// Remarks: Delete special custom key in metadata.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7
	//************************************
	static FS_BOOL MetadataDeleteCustomKey(FPD_Document doc, const FS_WideString wsItem);

	//************************************
	// Function:  MetadataGetString
	// Param[in]: doc				The input document.
	// Param[in]: wsItem			The specified item to get.
	// Param[out]: wsStr			Receive content string of the item.
	// Param[out]: pbUseInfoOrXML	Whether to get the metadata string is using the info dictionary or the metadata stream.
	// Return: Whether get the string success. 0 for success.
	// Remarks: Get specific item string corresponding to document info or root entries.
	// Notes:	Metadata provides some application level information about document, such as author, title, creation time and others. <br>
	//			There are two ways to store these information in PDF file:
	//			<ul>
	//			<li> in "Info" dictionary in trail dictionary.</li>
	//			<li> in metadata stream(with XML format) in the "Catalog" dictionary. </li>
	//			</ul>
	//			Which information would be got depends on the information of modified date, which is defined in key "ModDate" in the "Info" dictionary and "MetadataDate" in XML.<br>	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7
	//************************************
	static FS_INT32 MetadataGetString(FPD_Document doc,
									  const FS_WideString wsItem,
									  FS_WideString wsStr,
									  FS_INT32 *pbUseInfoOrXML);

	//************************************
	// Function:  MetadataSetString
	// Param[in]: doc				The input document.
	// Param[in]: wsItem			The specified item to set.
	// Param[in]: wsStr				The content string of the item.
	// Param[in]: bUTF8				Whether the content string is saved as UTF-8 encoding or not in PDF.
	// Return: Whether set the string success.
	//		   For custom keys, if "wsStr" is NULL, return false.
	// Remarks: Set specific item string corresponding to specific item.
	// Notes:	Only the PDF2.0 supports for the UTF-8 Unicode character encoding scheme.
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7
	//************************************
	static FS_BOOL MetadataSetString(FPD_Document doc,
									const FS_WideString wsItem,
									const FS_WideString wsStr,
									FS_BOOL bUTF8);

	//************************************
	// Function:  MetadataSyncUpdate
	// Param[in]: doc				The input document.
	// Return:  whether update the metadata success.
	// Remarks: Preserve the consistency of the metadata string that is saved by the information dictionary or the metadata stream.
	//			Be called only after the edit operation : MetadataSetString.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7
	//************************************
	static FS_BOOL MetadataSyncUpdate(FPD_Document doc);
};


//////////////////////////////////////////////////////////////////////////////////////////////////////
/*								 CFPD_NameTree_V1				                    */
//////////////////////////////////////////////////////////////////////////////////////////////////////
class CFPD_NameTree_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: rootDict	The root dictionary for the name tree (See Ref Table 3.28).
	// Return:    A name tree object from a root dictionary.
	// Remarks:   Creates a name tree object from a root dictionary.
	// Notes:
	//************************************
	static FPD_NameTree	New(FPD_Object rootDict);

	//************************************
	// Function:  New2	
	// Param[in]: doc			The PDF document.
	// Param[in]: szCategory	The category key name for name tree, like "Dests", "AP", etc. (See Ref Table 3.28).
	// Return:    A name tree object from a root dictionary.
	// Remarks:   Creates a name tree object from a root dictionary.
	// Notes:
	//************************************
	static FPD_NameTree	New2(FPD_Document doc, FS_LPCSTR szCategory);

	//************************************
	// Function:  Destroy	
	// Param[in]: nameTree	A name tree object created by FPDNameTreeNew or FPDNameTreeNew2.
	// Return:    void
	// Remarks:   Destroys a name tree object created by FPDNameTreeNew or FPDNameTreeNew2.
	// Notes:
	//************************************
	static void	Destroy(FPD_NameTree nameTree);
	
	//************************************
	// Function:  LookupValue	
	// Param[in]: nameTree		A name tree object.
	// Param[in]: index			The zero-based index of entry value.
	// Param[out]:outName		The name to be searched.
	// Return:    The value of specified entry of the found PDF name tree node.
	// Remarks:   Lookup a PDF name tree node by index that contains the name, and return specified entry value.
	// Notes:
	//************************************
	static FPD_Object LookupValue(FPD_NameTree nameTree, FS_INT32 index, FS_WideString* outName);

	//************************************
	// Function:  LookupValueByName	
	// Param[in]: nameTree	A name tree object.
	// Param[in]: wszName	The name to be searched.
	// Return:    The value of specified entry of the found PDF name tree node.
	// Remarks:   Lookup a PDF name tree node that contains the name, and return specified entry value.
	// Notes:
	//************************************
	static FPD_Object LookupValueByName(FPD_NameTree nameTree, FS_LPCWSTR wszName);

	//************************************
	// Function:  LookupNamedDest	
	// Param[in]: nameTree	A name tree object.
	// Param[in]: doc		The document.
	// Param[in]: szName	The input name.
	// Return:    The corresponding destination.
	// Remarks:   Lookup PDF name destination.
	// Notes:
	//************************************
	static FPD_Object LookupNamedDest(FPD_NameTree nameTree, FPD_Document doc, FS_LPSTR szName);

	//************************************
	// Function:  SetValue	
	// Param[in]: nameTree	A name tree object.
	// Param[in]: doc		The document.
	// Param[in]: szKey		The key is entry in the names dictionary.
	// Param[in]: wszName	The name to be searched.
	// Param[in]: value		The input entry value.
	// Return:    The index of the set value
	// Remarks:   Sets the entry value of specified PDF name tree node that contains specified name.
	// Notes:
	//************************************
	static FS_INT32	SetValue(FPD_NameTree nameTree, FPD_Document doc, FS_LPCSTR szKey, FS_LPCWSTR wszName, FPD_Object value);

	//************************************
	// Function:  GetIndex	
	// Param[in]: nameTree	A name tree object.
	// Param[in]: wszName	The name to be searched.
	// Return:    The index of the name If found csName. otherwise return -1. 
	// Remarks:   Gets index of the name in name tree.
	// Notes:
	//************************************
	static FS_INT32	GetIndex(FPD_NameTree nameTree, FS_LPCWSTR wszName);

	//************************************
	// Function:  Remove	
	// Param[in]: nameTree	A name tree object.
	// Param[in]: index		The zero-based index of entry value.
	// Param[in]: wszName	The name to be searched.
	// Return:    The value nonzero if successful, otherwise 0.
	// Remarks:   Removes the name and entry value by nIndex in name tree, if csName is empty. otherwise ignore nIndex.
	// Notes:
	//************************************
	static FS_BOOL Remove(FPD_NameTree nameTree, FS_INT32 index, FS_LPCWSTR wszName);

	//************************************
	// Function:  GetCount	
	// Param[in]: nameTree	A name tree object.
	// Return:    The number of key-value pairs.
	// Remarks:   Gets the number of key-value pairs in name tree.
	// Notes:
	//************************************
	static FS_INT32	GetCount(FPD_NameTree nameTree);

	//************************************
	// Function:  GetRoot	
	// Param[in]: nameTree	A name tree object.
	// Return:    The root dictionary of the tree.
	// Remarks:   Gets the root dictionary of the tree.
	// Notes:
	//************************************
	static FPD_Object GetRoot(FPD_NameTree nameTree);
};


//////////////////////////////////////////////////////////////////////////////////////////////////////
/*										 CFPD_Bookmark_V1		                    */
//////////////////////////////////////////////////////////////////////////////////////////////////////
class CFPD_Bookmark_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: outlineDict	The input outline item dictionary.
	// Return:    A PDF Bookmark object
	// Remarks:   Creates a PDF Bookmark object from a outline item dictionary.
	// Notes:
	//************************************
	static FPD_Bookmark	New(FPD_Object outlineDict);

	//************************************
	// Function:  Destroy
	// Param[in]: bookmark	A PDF Bookmark object created by FPDBookmarkNew.
	// Return:    void
	// Remarks:   Destroys a PDF Bookmark object created by FPDBookmarkNew.
	// Notes:
	//************************************
	static void	Destroy(FPD_Bookmark bookmark);

	//************************************
	// Function:  IsVaild
	// Param[in]: bookmark The input bookmark.
	// Return:    <a>TRUE</a> for being valid.
	// Remarks:   Tests wWhether the bookmark is valid.
	// Notes:
	//************************************
	static FS_BOOL IsVaild(FPD_Bookmark bookmark);


	//************************************
	// Function:  GetColorRef
	// Param[in]: bookmark The input bookmark.
	// Return:    The color of a bookmark.
	// Remarks:   Gets the color of a bookmark. In Windows COLORREF format: 0x00ggbbrr.
	// Notes:
	//************************************
	static FS_DWORD	GetColorRef(FPD_Bookmark bookmark);


	//************************************
	// Function:  GetFontStyle
	// Param[in]: bookmark The input bookmark.
	// Return:    The font style of a bookmark.
	// Remarks:   Gets the font style of a bookmark. Italic and/or bold.
	// Notes:
	//************************************
	static FS_DWORD	GetFontStyle(FPD_Bookmark bookmark);

	//************************************
	// Function:  GetTitle
	// Param[in]: bookmark The input bookmark.
	// Param[out]:outTitle It receives the title of a bookmark.
	// Return:    void
	// Remarks:   Gets the title of a bookmark. A unicode encoded string is returned.
	// Notes:
	//************************************
	static void	GetTitle(FPD_Bookmark bookmark, FS_WideString* outTitle);


	//************************************
	// Function:  GetDest
	// Param[in]: bookmark  The input bookmark.
	// Param[in]: doc		The input PDF document.
	// Param[out]:outDest	A PDF destination object.
	// Return:    void
	// Remarks:   Gets the destination of a bookmark.
	// Notes:
	//************************************
	static void GetDest(FPD_Bookmark bookmark, FPD_Document doc, FPD_Dest* outDest);

	//************************************
	// Function:  GetAction
	// Param[in]: bookmark		The input bookmark.
	// Param[out]:outAction		It receives the PDF action of a bookmark.
	// Return:    void
	// Remarks:   Gets the PDF action of a bookmark.
	// Notes:
	//************************************
	static void GetAction(FPD_Bookmark bookmark, FPD_Action* outAction);

	//************************************
	// Function:  GetDictionary
	// Param[in]: bookmark The input bookmark.
	// Return:    The outline item dictionary.
	// Remarks:	  Gets the outline item dictionary.
	// Notes:
	//************************************
	static FPD_Object GetDictionary(FPD_Bookmark bookmark);


	//************************************
	// Function:  GetFirstChild
	// Param[in]: doc				The input PDF document.
	// Param[in]: parent			The input parent bookmark.
	// Param[out]:outFirstChild		The first child bookmark.
	// Return:    <a>TRUE</a> if the child bookmark exist,otherwise <a>FALSE</a>.
	// Remarks:   Gets the first child bookmark of specified parent bookmark.
	//            If <param>pParent</param> is <a>NULL</a>, gets top level items.
	// Notes:
	//************************************
	static FS_BOOL	 GetFirstChild(FPD_Document doc, FPD_Bookmark parent, FPD_Bookmark* outFirstChild);

	//************************************
	// Function:  GetNextSibling
	// Param[in]: doc				The input PDF document.
	// Param[in]: bookmark			The input bookmark.
	// Param[out]:outNextSibling	The next sibling bookmark.
	// Return:    <a>TRUE</a> if the next sibling bookmark exist, otherwise <a>FALSE</a>.
	// Remarks:   Gets the next sibling bookmark of specified bookmark in the same level.
	// Notes:
	//************************************
	static FS_BOOL	 GetNextSibling(FPD_Document doc, FPD_Bookmark bookmark, FPD_Bookmark* outNextSibling);
};

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*										 CFPD_Dest_V1								*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
class CFPD_Dest_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: obj	The input PDF object.
	// Return:    A PDF destination object
	// Remarks:   Creates a PDF destination object from a PDF object.
	// Notes:
	//************************************
	static FPD_Dest	New(FPD_Object obj);

	//************************************
	// Function:  Destroy
	// Param[in]: dest The input PDF destination object.
	// Return:    void
	// Remarks:   Destroys a PDF destination object.
	// Notes:
	//************************************
	static void	Destroy(FPD_Dest dest);
	
	//************************************
	// Function:  GetRemoteName
	// Param[in]: dest			A PDF destination object.
	// Param[out]:pStrResult	It receives the remote name of named destination.
	// Return:    void
	// Remarks:   Gets the remote name of named destination.
	// Notes:
	//************************************
	static void	GetRemoteName(FPD_Dest dest, FS_ByteString* pStrResult);

	//************************************
	// Function:  GetPageIndex
	// Param[in]: doc	The PDF document.
	// Param[in]: dest	A PDF destination object.
	// Return:    The zero-based index of the page referred to.
	// Remarks:   Gets zero-based index of the page in the document.
	// Notes:
	//************************************
	static FS_INT32	GetPageIndex(FPD_Document doc, FPD_Dest dest);

	//************************************
	// Function:  GetPageObjNum
	// Param[in]: dest	A PDF destination object.
	// Return:    The object number of the page.
	// Remarks:   Gets the object number of the page.
	// Notes:
	//************************************
	static FS_DWORD	GetPageObjNum(FPD_Dest dest);

	//************************************
	// Function:  GetZoomMode
	// Param[in]: dest	A PDF destination object.
	// Return:    The zoom mode of the destination.
	// Remarks:   Gets the zoom mode of the destination.
	// Notes:
	//************************************
	static FS_INT32	GetZoomMode(FPD_Dest dest);

	//************************************
	// Function:  GetParam
	// Param[in]: dest		A PDF destination object.
	// Param[in]: index		The zero-based index of the param.
	// Return:    The param by index.
	// Remarks:   Gets a param.
	// Notes:
	//************************************
	static double GetParam(FPD_Dest dest, FS_INT32 index);

	//************************************
	// Function:  GetPDFObject
	// Param[in]: dest		A PDF destination object.
	// Param[out]: pObject	It receives the PDF object of the destination.
	// Return:    void
	// Remarks:   Gets the PDF object of the destination.
	// Notes:
	//************************************
	static void GetPDFObject(FPD_Dest dest, FPD_Object* pObject);
	
};

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*										 CFPD_OCContext_V1											*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
class CFPD_OCContext_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: doc		The input PDF document.
	// Param[in]: UsageType	The intended usage type of optional content.
	// Return:    An optional content context.
	// Remarks:	  Creates optional content context from a PDF document.
	// Notes:
	//************************************
	static FPD_OCContext New(FPD_Document doc, FPD_OCC_UsageType UsageType);

	//************************************
	// Function:  Destroy
	// Param[in]: occ	Optional content context.
	// Return:    void
	// Remarks:   Destroys optional content context.
	// Notes:
	//************************************
	static void	Destroy(FPD_OCContext occ);
	
	//************************************
	// Function:  GetDocument
	// Param[in]: occ	The input optional content context.
	// Return:    The PDF document in the optional content context.
	// Remarks:   Gets the PDF document in the optional content context.
	// Notes:
	//************************************
	static FPD_Document	GetDocument(FPD_OCContext occ);

	//************************************
	// Function:  GetUsageType
	// Param[in]: occ The input optional content context.
	// Return:    The usage type of optional content.
	// Remarks:   Gets the usage type of optional content.
	// Notes:
	//************************************
	static FS_INT32	GetUsageType(FPD_OCContext occ);

	//************************************
	// Function:  CheckOCGVisible
	// Param[in]: occ		The input optional content context.
	// Param[in]: OCGDict	The optional content group dictionary.
	// Return:    <a>TRUE</a> for being visible.
	// Remarks:   Checks whether the optional content group is visible or not.
	// Notes:
	//************************************
	static FS_BOOL CheckOCGVisible(FPD_OCContext occ, const FPD_Object OCGDict);

	//************************************
	// Function:  ResetOCContext
	// Param[in]: occ The input optional content context.
	// Return:    void
	// Remarks:   Resets the optional content context.
	// Notes:
	//************************************
	static void	ResetOCContext(FPD_OCContext occ);	


	//************************************
	// Function:  SetOCGState
	// Param[in]: occ		The input optional content context.
	// Param[in]: ocgDict	The input OCG dictionary.
	// Param[in]: bState	The new state of the OCG.
	// Param[in]: bNotify	Whether to notify or not.
	// Return:    void
	// Remarks:   Resets the optional content context.
	// Notes:
	//************************************
	static void	SetOCGState(FPD_OCContext occ, FPD_Object ocgDict, FS_BOOL bState, FS_BOOL bNotify);
};


//////////////////////////////////////////////////////////////////////////////////////////////////////
/*										 CFPD_OCGroup_V1								*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
class CFPD_OCGroup_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: dict	The input PDF dictionary.
	// Return:    Optional content group from a PDF dictionary.
	// Remarks:   Creates optional content group from a PDF dictionary.
	// Notes:
	//************************************
	static FPD_OCGroup New(FPD_Object dict);

	//************************************
	// Function:  Destroy
	// Param[in]: ocg	Optional content group.
	// Return:    void
	// Remarks:   Destroys optional content group;
	// Notes:
	//************************************
	static void	Destroy(FPD_OCGroup ocg);


	//************************************
	// Function:  GetName
	// Param[in]: ocg		Optional content group.
	// Param[out]:outName	It will receive the name of the OCG.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Gets the name of the OCG.
	// Notes:
	//************************************
	static FS_BOOL GetName(FPD_OCGroup ocg, FS_WideString* outName);

	//************************************
	// Function:  SetName
	// Param[in]: ocg		Optional content group.
	// Param[in]: wszName	The new OCG name.
	// Return:    void
	// Remarks:   Sets the name of the OCG.
	// Notes:
	//************************************
	static void	SetName(FPD_OCGroup ocg, FS_LPCWSTR wszName);


	//************************************
	// Function:  HasIntent
	// Param[in]: ocg		Optional content group.
	// Param[in]: szIntent	The input intent.
	// Return:    Whether the OCG has the specified intent or not.
	// Remarks:   Does the OCG have the specified intent?
	// Notes:
	//************************************
	static FS_BOOL HasIntent(FPD_OCGroup ocg, FS_LPCSTR szIntent);


	//************************************
	// Function:  GetCreatorInfo
	// Param[in]: ocg				Optional content group.
	// Param[out]:outWstrCreator	It receives that specifies the application that created the group.
	// Param[out]:outStrType		It receives that specifies the type of content controlled by the group.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   CreatorInfo entry of optional content usage dictionary.
	// Notes:
	//************************************
	static FS_BOOL GetCreatorInfo(FPD_OCGroup ocg, FS_WideString* outWstrCreator, FS_ByteString* outStrType);

	//************************************
	// Function:  GetLanguageInfo
	// Param[in]: ocg			Optional content group.
	// Param[out]:outInfo		It receives that specifies a language and possibly a locale.
	// Param[out]:outPrefered	It receives whether the language is a preferred language.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Language entry of optional content usage dictionary.
	// Notes:
	//************************************
	static FS_BOOL GetLanguageInfo(FPD_OCGroup ocg, FS_ByteString* outInfo, FS_BOOL* outPrefered);

	//************************************
	// Function:  GetExportState
	// Param[in]: ocg	Optional content group.
	// Return:    The export state of the OCG.
	// Remarks:   Gets the export state of the OCG. Exports entry of optional content usage dictionary.
	// Notes:
	//************************************
	static FS_BOOL GetExportState(FPD_OCGroup ocg);

	//************************************
	// Function:  GetZoomRange
	// Param[in]: ocg		Optional content group.
	// Param[out]:outMin	It receives the minimum recommended magnification factor. 
	// Param[out]:outMax	It receives the maximum recommended magnification factor
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Gets zoom entry of optional content usage dictionary.
	// Notes:
	//************************************
	static FS_BOOL GetZoomRange(FPD_OCGroup ocg, FS_FLOAT* outMin, FS_FLOAT* outMax);

	//************************************
	// Function:  GetPrintInfo
	// Param[in]: ocg		Optional content group.
	// Param[out]:outType	It receives that specifies the kind of content controlled by the group.
	// Param[out]:outState	It receives the printing state of OCG.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Gets print entry of optional content usage dictionary.
	// Notes:
	//************************************
	static FS_BOOL GetPrintInfo(FPD_OCGroup ocg, FS_ByteString* outType, FS_BOOL* outState);

	//************************************
	// Function:  GetViewState
	// Param[in]: ocg	Optional content group.
	// Return:    The view state of the OCG.
	// Remarks:   Gets the view state of the OCG. View entry of optional content usage dictionary.
	// Notes:
	//************************************
	static FS_BOOL GetViewState(FPD_OCGroup ocg);

	//************************************
	// Function:  GetUserType
	// Param[in]: ocg			Optional content group.
	// Param[out]:outType		It receives the user type of whom this OCG is primarily intended.
	// Param[out]:outUserArr	It receives a string that represents the name(s) of the individual, position or organization.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   User entry of optional content usage dictionary.
	// Notes:
	//************************************
	static FS_BOOL GetUserType(FPD_OCGroup ocg, FS_ByteString* outType, FS_WideStringArray* outUserArr);

	//************************************
	// Function:  GetPageElementType
	// Param[in]: ocg		Optional content group.
	// Param[out]:outName	It receives the page element type.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Gets PageElement entry of optional content usage dictionary.
	// Notes:
	//************************************
	static FS_BOOL GetPageElementType(FPD_OCGroup ocg, FS_ByteString* outName);	

	//************************************
	// Function:  GetDictionary
	// Param[in]: ocg		Optional content group.
	// Return:    The OCG dictionary.
	// Remarks: Get the OCG dictionary.
	// Notes:
	//************************************
	static FPD_Object GetDictionary(FPD_OCGroup ocg);	
};


//////////////////////////////////////////////////////////////////////////////////////////////////////
/*										 CFPD_OCGroupSet_V1							*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
class CFPD_OCGroupSet_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: obj	A PDF object.
	// Return:    An optional content group set.
	// Remarks:   Creates optional content group set from a PDF object.
	// Notes:
	//************************************
	static FPD_OCGroupSet New(FPD_Object obj);

	//************************************
	// Function:  Destroy
	// Param[in]: ocgs	Optional content group set.
	// Return:    void
	// Remarks:   Destroys optional content group set.
	// Notes:
	//************************************
	static void	Destroy(FPD_OCGroupSet ocgs);
	
	//************************************
	// Function:  CountElements
	// Param[in]: ocgs	Optional content group set.
	// Return:    The count of elements in the OCG set.
	// Remarks:   Gets the count of elements in the OCG set.
	// Notes:
	//************************************
	static FS_INT32	CountElements(FPD_OCGroupSet ocgs);

	//************************************
	// Function:  IsSubGroupSet
	// Param[in]: ocgs		Optional content group set.
	// Param[in]: index		The input zero-based element in the array.
	// Return:    Non-zero means a subgroup, otherwise not.
	// Remarks:   Checks whether the specified element is a subgroup or not.
	// Notes:
	//************************************
	static FS_BOOL IsSubGroupSet(FPD_OCGroupSet ocgs, FS_INT32 index);

	//************************************
	// Function:  GetGroup
	// Param[in]: ocgs		Optional content group set.
	// Param[in]: index		The input zero-based element in the array.
	// Param[out]:pOCGroup	It receives the optional content group.
	// Return:    void
	// Remarks:   Gets a OCG from specified position.
	// Notes:
	//************************************
	static void GetGroup(FPD_OCGroupSet ocgs, FS_INT32 index, FPD_OCGroup* pOCGroup);

	//************************************
	// Function:  GetSubGroupSet
	// Param[in]: ocgs			Optional content group set.
	// Param[in]: index			The input zero-based element in the array.
	// Param[out]:pOCGroupSet	It receives the optional content group set.
	// Return:    void
	// Remarks:   Gets a OCG set from specified position.
	// Notes:
	//************************************
	static void	GetSubGroupSet(FPD_OCGroupSet ocgs, FS_INT32 index, FPD_OCGroupSet* pOCGroupSet);

	//************************************
	// Function:  FindGroup
	// Param[in]: ocgs			Optional content group set.
	// Param[in]: groupDict		The input OCG dictionary.
	// Return:    The zero-based index in the array.
	// Remarks:   Finds a OCG in the array.
	// Notes:
	//************************************
	static FS_INT32	FindGroup(FPD_OCGroupSet ocgs, const FPD_Object groupDict);

	//************************************
	// Function:  GetSubGroupSetName
	// Param[in]: ocgs		Optional content group set.
	// Param[out]:outName	It receives the name of the OCG set.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Gets the group set name.
	// Notes:
	//************************************
	static FS_BOOL GetSubGroupSetName(FPD_OCGroupSet ocgs, FS_WideString* outName);

};

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*										 CFPD_OCProperties_V1						*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _FPDFAPI_MINI_
class IPDF_OCNotify{};
class CPDF_OCGroup{};
#endif

class CFPD_CustomerOCNotifyHandler : public IPDF_OCNotify, public CFX_Object
{
public:
	CFPD_CustomerOCNotifyHandler(FPD_OCGStateChangedNotify lpProc);
	void	OnOCGStateChanged(CPDF_Document* pDoc, CPDF_OCGroup ocg, FX_BOOL bVisible);

	FPD_OCGStateChangedNotify m_lpProc;

};

class CFPD_OCNotify_V1
{
public:
	//************************************
	// Function:  FPD_OCNotifyNew
	// Param[in]: proc A callback for Optional Content Notification FPD_OCNotify object.
	// Return:    The OC notify interface.
	// Remarks:   Creates a new OC notify interface.
	// Notes:
	//************************************
	static FPD_OCNotify	FPD_OCNotifyNew(FPD_OCGStateChangedNotify proc);

	//************************************
	// Function:  FPD_OCNotifyDestroy
	// Param[in]: notify The OC notify interface.
	// Return:    void
	// Remarks:   Destroys the OC notify interface.
	// Notes:
	//************************************
	static void	FPD_OCNotifyDestroy(FPD_OCNotify notify);
};

class CFPD_OCProperties_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: doc	The input PDF document.
	// Return:    Optional content properties.
	// Remarks:   Creates optional content properties from a PDF document.
	// Notes:
	//************************************
	static FPD_OCProperties	New(FPD_Document doc);

	//************************************
	// Function:  Destroy
	// Param[in]: ocprops	Optional content properties.
	// Return:    void
	// Remarks:   Destroys optional content properties.
	// Notes:
	//************************************
	static void	Destroy(FPD_OCProperties ocprops);
	
	//************************************
	// Function:  GetDocument
	// Param[in]: ocprops	Optional content properties.
	// Return:    The PDF document.
	// Remarks:	  Gets the PDF document.
	// Notes:
	//************************************
	static FPD_Document	GetDocument(FPD_OCProperties ocprops);

	//************************************
	// Function:  GetOCGroups
	// Param[in]: ocprops		Optional content properties.
	// Param[out]:arrOCGs		It receives all OCG objects in specified page.
	// Param[in]: page_index	The input zero-based page index.
	// Return:    The number of OCG retrieved.
	// Remarks:   Retrieves all OCG objects for a document or a page.
	//            If iPageIndex equals to -1, all document level OCG objects are returned; 
	//            or all page level OCG objects are returned.
	// Notes:
	//************************************
	static FS_INT32	GetOCGroups(FPD_OCProperties ocprops, FS_PtrArray* arrOCGs, FS_INT32 page_index);

	//************************************
	// Function:  IsOCGroup
	// Param[in]: ocprops	Optional content properties.
	// Param[in]: dict		The input PDF dictionary.
	// Return:    Non-zero means valid, otherwise invalid.
	// Remarks:   Determines whether a dictionary object is a valid OCG object.
	// Notes:
	//************************************
	static FS_BOOL IsOCGroup(FPD_OCProperties ocprops, const FPD_Object dict);

	//************************************
	// Function:  RetrieveOCGPages
	// Param[in]: ocprops		Optional content properties.
	// Param[in]: dict			The input OCG dictionary.
	// Param[out]:arrPages		It receives all page dictionaries in which the specified OCG is referenced.
	// Return:    The number of pages referenced.
	// Remarks:   Retrieves all pages objects in which the specified pOCGDict is referenced.
	//            One OCG can be shared by several pages. pages is an array of page dictionary objects.
	//            The returned value is the count of the elements in pages array.
	// Notes:
	//************************************
	static FS_INT32	RetrieveOCGPages(FPD_OCProperties ocprops, const FPD_Object dict, FS_PtrArray* arrPages);
	
	//************************************
	// Function:  IsOCGInPage
	// Param[in]: ocprops		Optional content properties.
	// Param[in]: page_dict		The input page dictionary.
	// Param[in]: ocg_dict		The input OCG dictionary.
	// Return:    Non-zero means in the page, otherwise not.
	// Remarks:   Determines whether a OCG object is in a page or not.
	// Notes:
	//************************************
	static FS_BOOL IsOCGInPage(FPD_OCProperties ocprops, const FPD_Object page_dict, FPD_Object ocg_dict);

	//************************************
	// Function:  GetOCGroupOrder
	// Param[in]: ocprops			Optional content properties.
	// Param[out]:pOCGroupSet		It receives the OCG set.
	// Return:    void
	// Remarks:   Orders entry in optional content configuration dictionary.
	//            All document level OCG objects can be stored in an ordered tree object, this will be showed in UI.
	// Notes:
	//************************************
	static void GetOCGroupOrder(FPD_OCProperties ocprops, FPD_OCGroupSet* pOCGroupSet);




	//************************************
	// Function:  CountConfigs
	// Param[in]: ocprops	Optional content properties.
	// Return:    The count of configuration dictionaries in the OCP.
	// Remarks:   Gets the count of configuration dictionaries in the OCP.
	// Notes:
	//************************************
	static FS_INT32	CountConfigs(FPD_OCProperties ocprops);

	//************************************
	// Function:  GetConfig
	// Param[in]: ocprops	Optional content properties.
	// Param[in]: index		The input zero-based configuration dictionary index.
	// Return:    A configuration dictionary.
	// Remarks:   Gets a configuration dictionary in the OCP.
	// Notes:
	//************************************
	static FPD_Object GetConfig(FPD_OCProperties ocprops, FS_INT32 index);


	//************************************
	// Function:  AddOCNotify
	// Param[in]: ocprops				Optional content properties.
	// Param[in]: ocgNotifyCallback		The input user supplied notify interface to add.
	// Return:    void
	// Remarks:   Adds an user-supplied optional content notify interface to the OCP.
	// Notes:
	//************************************
	static void	AddOCNotify(FPD_OCProperties ocprops, FPD_OCNotify ocgNotifyCallback);

	//************************************
	// Function:  RemoveOCNotify
	// Param[in]: ocprops				Optional content properties.
	// Param[in]: ocgNotifyCallback		The input user supplied notify interface to remove.
	// Return:    void
	// Remarks:   Removes an user-supplied optional content notify interface from OCP.
	// Notes:
	//************************************
	static void	RemoveOCNotify(FPD_OCProperties ocprops, FPD_OCNotify ocgNotifyCallback);
};


/////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Actions
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*									CFPD_LWinParam_V1												*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
class CFPD_LWinParam_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: dict	The input PDF dictionary.
	// Return:    Windows launch Param.
	// Remarks:   Creates windows launch param from a PDF dictionary.
	// Notes:
	//************************************
	static FPD_LWinParam New(FPD_Object dict);

	//************************************
	// Function:  Destroy
	// Param[in]: param		Windows launch param.
	// Return:    void
	// Remarks:	  Destroys windows launch param.
	// Notes:
	//************************************
	static void	Destroy(FPD_LWinParam param);

	//************************************
	// Function:  GetFileName
	// Param[in]: param			Windows launch param.
	// Param[out]:outFileName	It receives the file name
	// Return:    void
	// Remarks:   Gets the file name of the the application to be launched or the document to be opened or printed.
	// Notes:
	//************************************
	static void	GetFileName(FPD_LWinParam param, FS_ByteString* outFileName);

	//************************************
	// Function:  SetFileName
	// Param[in]: param		Windows launch param.
	// Param[in]: szFile	The new file name.
	// Return:    void
	// Remarks:   Sets the file name in the windows launch param.
	// Notes:
	//************************************
	static void	SetFileName(FPD_LWinParam param, FS_LPSTR szFile);

	//************************************
	// Function:  GetDefaultDirectory
	// Param[in]: param				Windows launch param.
	// Param[out]:outDirectory		The default directory in standard DOS syntax.
	// Return:    void
	// Remarks:   Gets the default directory in standard DOS syntax.
	// Notes:
	//************************************
	static void	GetDefaultDirectory(FPD_LWinParam param, FS_ByteString* outDirectory);

	//************************************
	// Function:  SetDefaultDirectory
	// Param[in]: param			Windows launch param.
	// Param[in]: szDirectory	The new default directory.
	// Return:    void
	// Remarks:   Sets the default directory.
	// Notes:
	//************************************
	static void	SetDefaultDirectory(FPD_LWinParam param, FS_LPSTR szDirectory);

	//************************************
	// Function:  GetOperation
	// Param[in]: param				Windows launch param.
	// Param[out]:outOperation		It receives the operation to perform.
	// Return:    void
	// Remarks:   Gets the operation to perform.
	// Notes:
	//************************************
	static void	GetOperation(FPD_LWinParam param, FS_ByteString* outOperation);

	//************************************
	// Function:  SetOperation
	// Param[in]: param				Windows launch param.
	// Param[in]: szOperation		The new operation string.
	// Return:    void
	// Remarks:   Sets the operation ASCII string.
	// Notes:
	//************************************
	static void	SetOperation(FPD_LWinParam param, FS_LPSTR szOperation);

	//************************************
	// Function:  GetParam
	// Param[in]: param			Windows launch param.
	// Param[out]:outParam		It receives the param to be passed to the application designated by the F entry. 
	// Return:    void
	// Remarks:   Gets the param to be passed to the application designated by the F entry. 
	// Notes:
	//************************************
	static void	GetParam(FPD_LWinParam param, FS_ByteString* outParam);

	//************************************
	// Function:  SetParam
	// Param[in]: param			Windows launch param.
	// Param[in]: szParam		The new application params.
	// Return:    void
	// Remarks:   Sets the application Params.
	// Notes:
	//************************************
	static void	SetParam(FPD_LWinParam param, FS_LPSTR szParam);

	//************************************
	// Function:  GetDict
	// Param[in]: param			Windows launch param.
	// Return:    The windows launch param dictionary.
	// Remarks:   Gets the windows launch Param dictionary.
	// Notes:
	//************************************
	static FPD_Object GetDict(FPD_LWinParam param);
};

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*								 CFPD_ActionFields_V1												*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
class CFPD_ActionFields_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: action	The input PDF action.
	// Return:    PDF action fields.
	// Remarks:   Creates PDF action fields from a PDF action.
	// Notes:
	//************************************
	static FPD_ActionFields	New(FPD_Action action);

	//************************************
	// Function:  Destroy
	// Param[in]: actFields		The input PDF action fields.
	// Return:    void
	// Remarks:   Destroys PDF action fields.
	// Notes:
	//************************************
	static void	Destroy(FPD_ActionFields actFields);
	
	//************************************
	// Function:  GetFieldsCount
	// Param[in]: actFields		The input PDF action fields.
	// Return:    The count of action fields.
	// Remarks:   Gets the count of action fields.
	// Notes:
	//************************************
	static FS_DWORD	GetFieldsCount(FPD_ActionFields actFields);

	//************************************
	// Function:  GetAllFields
	// Param[in]: actFields			The input PDF action fields.
	// Param[out]:outFieldObjects	It receives all fields in the action dictionary.
	// Return:    void
	// Remarks:   Gets all fields in the action dictionary.
	//            outFieldObjects is an array of FPD_Object.
	// Notes:
	//************************************
	static void	GetAllFields(FPD_ActionFields actFields, FS_PtrArray* outFieldObjects);

	//************************************
	// Function:  GetField
	// Param[in]: actFields		The input PDF action fields.
	// Param[in]: index			The zero-based field index.
	// Return:    A PDF object.
	// Remarks:   Gets a field value.
	//			  returned object may be FPD_Object or FPD_String.
	// Notes:
	//************************************
	static FPD_Object GetField(FPD_ActionFields actFields, FS_DWORD index);

	//************************************
	// Function:  InsertField
	// Param[in]: actFields		The input PDF action fields.
	// Param[in]: iInsertAt		The zero-based field index to insert at.
	// Param[in]: field			The field value.
	// Return:    void
	// Remarks:	  Inserts a field.
	//            field: it can be FPD_Object or FPD_String object,
	//            if need insert field as name(string), construct name as FPD_String object
	// Notes:
	//************************************
	static void	InsertField(FPD_ActionFields actFields, FS_DWORD iInsertAt, const FPD_Object field);
	
	//************************************
	// Function:  RemoveField
	// Param[in]: actFields		The input PDF action fields.
	// Param[in]: index			The zero-based field index to be removed.
	// Return:    void
	// Remarks:   Removes a field from PDF action fields. 
	// Notes:
	//************************************
	static void	RemoveField(FPD_ActionFields actFields, FS_DWORD index);

	//************************************
	// Function:  GetAction
	// Param[in]: actFields		The input PDF action fields.
	// Return:    The PDF action dictionary.
	// Remarks:	  Gets the PDF action dictionary.
	// Notes:
	//************************************
	static FPD_Action GetAction(FPD_ActionFields actFields);
};


//////////////////////////////////////////////////////////////////////////////////////////////////////
/*										 CFPD_Action_V1												*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
class CFPD_Action_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: dict	The input PDF dictionary.
	// Return:    A PDF action.
	// Remarks:   Creates a PDF action from a PDF dictionary.
	// Notes:
	//************************************
	static FPD_Action New(FPD_Object dict);

	//************************************
	// Function:  New2
	// Param[in]: doc			The input PDF document.
	// Param[in]: actionType	The input action type.
	// Return:    A PDF action.
	// Remarks:   Creates a PDF action with specified action type.
	// Notes:
	//************************************
	static FPD_Action New2(FPD_Document doc, FPD_ActionType actionType);

	//************************************
	// Function:  New3
	// Param[in]: doc		The input PDF document.
	// Param[in]: szType	The input action type name.
	// Return:	  A PDF action.
	// Remarks:   Creates a PDF action with specified action type name.
	// Notes:
	//************************************
	static FPD_Action New3(FPD_Document doc, FS_LPSTR szType);

	//************************************
	// Function:  Destroy
	// Param[in]: action	A PDF action.
	// Return:    void
	// Remarks:   Destroys a PDF action.
	// Notes:
	//************************************
	static void	Destroy(FPD_Action action);

	//************************************
	// Function:  GetTypeName
	// Param[in]: action		A PDF action.
	// Param[out]:outTypeName	It receives the type name of the action.
	// Return:    void
	// Remarks:   Gets the type name of the action.
	// Notes:
	//************************************
	static void	GetTypeName(FPD_Action action, FS_ByteString* outTypeName);

	//************************************
	// Function:  GetType
	// Param[in]: action	A PDF action.
	// Return:    The type of the action.
	// Remarks:   Gets the type of the action.
	// Notes:
	//************************************
	static FS_INT32	GetType(FPD_Action action);

	//************************************
	// Function:  GetDest
	// Param[in]: action	A PDF action.
	// Param[in]: doc		The input PDF document.
	// Param[out]:outDest	A PDF destination.
	// Return:    void
	// Remarks:   Gets the destination.
	// Notes:
	//************************************
	static void GetDest(FPD_Action action, FPD_Document doc, FPD_Dest* outDest);

	//************************************
	// Function:  SetDest
	// Param[in]: action	A PDF action.
	// Param[in]: dest		Ref to a PDF destination.
	// Return:    void
	// Remarks:   Sets the destination.
	// Notes:
	//************************************
	static void	SetDest(FPD_Action action, const FPD_Dest dest);


	//************************************
	// Function:  GetFilePath
	// Param[in]: action	A PDF action.
	// Param[out]:outName	It receives the file full path.
	// Return:    void
	// Remarks:   Gets the file full path.
	// Notes:
	//************************************
	static void	GetFilePath(FPD_Action action, FS_WideString* outName);

	//************************************
	// Function:  SetFilePath
	// Param[in]: action		A PDF action.
	// Param[in]: wszFilePath	The input file full path.
	// Param[in]: bIsURL		Whether the file path is a URL or not.
	// Return:    void
	// Remarks:   Sets the file full path.
	// Notes:
	//************************************
	static void	SetFilePath(FPD_Action action, FS_LPCWSTR wszFilePath, FS_BOOL bIsURL);


	//************************************
	// Function:  GetNewWindow
	// Param[in]: action	A PDF action.
	// Return:    <a>TRUE</a> for opening the destination document in a new window.
	// Remarks:   Checks whether to open the destination document in a new window or not.
	// Notes:
	//************************************
	static FS_BOOL GetNewWindow(FPD_Action action);

	//************************************
	// Function:  SetNewWindow
	// Param[in]: action		A PDF action.
	// Param[in]: bNewWindow	The flag which identifies whether to open the destination document in a new window or not.
	// Return:    void
	// Remarks:   Sets the new window flag.
	// Notes:
	//************************************
	static void	SetNewWindow(FPD_Action action, FS_BOOL bNewWindow);


	//************************************
	// Function:  GetWinParam
	// Param[in]: action		A PDF action.
	// Param[out]:outWinParam	The windows launch params.
	// Return:    void
	// Remarks:   Gets the windows launch param.
	// Notes:
	//************************************
	static void GetWinParam(FPD_Action action, FPD_LWinParam* outWinParam);

	//************************************
	// Function:  SetWinParam
	// Param[in]: action	A PDF action.
	// Param[in]: param		Ref to a windows launch param object.
	// Return:    void
	// Remarks:   Sets the windows launch params.
	// Notes:
	//************************************
	static void	SetWinParam(FPD_Action action, const FPD_LWinParam param);


	//************************************
	// Function:  GetURI
	// Param[in]: action	A PDF action.
	// Param[in]: doc		The input PDF document.
	// Param[out]:outURL	It receives the URI.
	// Return:    void
	// Remarks:   Gets the URI(uniform resource identifier) of the PDF action.
	// Notes:
	//************************************
	static void	GetURI(FPD_Action action, FPD_Document doc, FS_ByteString* outURL);

	//************************************
	// Function:  SetURI	
	// Param[in]: action	A PDF action.
	// Param[in]: szURI		The input URI.
	// Return:    void
	// Remarks:   Sets the URI of the PDF action.
	// Notes:
	//************************************
	static void	SetURI(FPD_Action action, FS_LPSTR szURI);


	//************************************
	// Function:  GetMouseMap
	// Param[in]: action	A PDF action.
	// Return:    <a>TRUE</a> for tracking the mouse position when the URI is resolved.
	// Remarks:   Gets the flag which indicates whether to track the mouse position when the URI is resolved.
	// Notes:
	//************************************
	static FS_BOOL GetMouseMap(FPD_Action action);

	//************************************
	// Function:  SetMouseMap
	// Param[in]: action	A PDF action.
	// Param[in]: bMap		The new mouse-position-tracking flag.
	// Return:    void
	// Remarks:   Sets the mouse-position-tracking flag. 
	// Notes:
	//************************************
	static void	SetMouseMap(FPD_Action action, FS_BOOL bMap);


	//************************************
	// Function:  GetWidgets
	// Param[in]: action		A PDF action.
	// Param[out]:outWidgets	It receives the action fields.
	// Return:    void
	// Remarks:   Gets the fields array.
	// Notes:
	//************************************
	static void GetWidgets(FPD_Action action, FPD_ActionFields* outWidgets);


	//************************************
	// Function:  GetHideStatus
	// Param[in]: action	A PDF action.
	// Return:    The hide status of a PDF action.
	// Remarks:   Gets the hide status of a PDF action.
	// Notes:
	//************************************
	static FS_BOOL GetHideStatus(FPD_Action action);

	//************************************
	// Function:  SetHideStatus
	// Param[in]: action	A PDF action.
	// Param[in]: bHide		The input hide status.
	// Return:    void
	// Remarks:   Sets the hide status of a PDF action.
	// Notes:
	//************************************
	static void SetHideStatus(FPD_Action action, FS_BOOL bHide);


	//************************************
	// Function:  GetNameAction
	// Param[in]: action			A PDF action.
	// Param[out]:outNamedAction	It receives the name of named action.
	// Return:    void
	// Remarks:   Gets the name of named action.
	// Notes:
	//************************************
	static void GetNameAction(FPD_Action action, FS_ByteString* outNamedAction);

	//************************************
	// Function:  SetNameAction
	// Param[in]: action	A PDF action.
	// Param[in]: szName	The  input name of named action.
	// Return:    void
	// Remarks:   Sets the name of named action.
	// Notes:
	//************************************
	static void	SetNameAction(FPD_Action action, FS_LPCSTR szName);


	//************************************
	// Function:  GetFlags
	// Param[in]: action	A PDF action.
	// Return:    The flags for action type SubmitForm, ResetForm.
	// Remarks:   Gets the flags for action type SubmitForm, ResetForm.
	// Notes:
	//************************************
	static FS_DWORD	GetFlags(FPD_Action action);

	//************************************
	// Function:  SetFlags
	// Param[in]: action	A PDF action.
	// Param[in]: dwFlags	The input flags.
	// Return:    void
	// Remarks:   Sets the flags for action type SubmitForm, ResetForm.
	// Notes:
	//************************************
	static void	SetFlags(FPD_Action action, FS_DWORD dwFlags);


	//************************************
	// Function:  GetJavaScript
	// Param[in]: action			A PDF action.
	// Param[out]:outJavaScript		It receives the javascript script to be executed.
	// Return:    void
	// Remarks:   Gets the javascript script to be executed.
	// Notes:
	//************************************
	static void	GetJavaScript(FPD_Action action, FS_WideString* outJavaScript);

	//************************************
	// Function:  SetJavaScript
	// Param[in]: action			A PDF action.
	// Param[in]: doc				The input PDF document.
	// Param[in]: szJavaScript		The javascript in byte string.
	// Return:    void
	// Remarks:   Sets the javascript with a byte string. 
	// Notes:
	//************************************
	static void	SetJavaScript(FPD_Action action, FPD_Document doc, FS_LPCSTR szJavaScript);

	//************************************
	// Function:  SetJavaScriptW
	// Param[in]: action			A PDF action.
	// Param[in]: doc				The input PDF document.
	// Param[in]: wszJavaScript		The javascript in wide string.
	// Return:    void
	// Remarks:	  Sets the javascript with a wide string. 
	// Notes:
	//************************************
	static void	SetJavaScriptW(FPD_Action action, FPD_Document doc, FS_LPCWSTR wszJavaScript);


	//************************************
	// Function:  CountRenditions
	// Param[in]: action	A PDF action.
	// Return:    The count of renditions.
	// Remarks:   Gets the count of renditions.
	// Notes:
	//************************************
	static FS_INT32	CountRenditions(FPD_Action action);

	//************************************
	// Function:  GetRendition
	// Param[in]: action			A PDF action.
	// Param[in]: index				The zero-based rendition index.
	// Param[out]:outRendition		It receives the rendition.
	// Return:    void
	// Remarks:   Gets a rendition. 
	// Notes:
	//************************************
	static void GetRendition(FPD_Action action, FS_INT32 index, FPD_Rendition* outRendition);

	//************************************
	// Function:  InsertRendition
	// Param[in]: action			A PDF action.
	// Param[in]: doc				The input PDF document.
	// Param[in]: renditionDict		The input rendition dictionary.
	// Param[in]: index				The zero-based rendition index.
	// Return:    The inserted index in the rendition array.
	// Remarks:   Inserts a rendition.
	// Notes:
	//************************************
	static FS_INT32	InsertRendition(FPD_Action action, FPD_Document doc, FPD_Object renditionDict, FS_INT32 index);

	//************************************
	// Function:  RemoveRendition
	// Param[in]: action			A PDF action.
	// Param[in]: renditionDict		The input rendition dictionary.
	// Return:    void
	// Remarks:   Removes a rendition.
	// Notes:
	//************************************
	static void	RemoveRendition(FPD_Action action, FPD_Object renditionDict);


	//************************************
	// Function:  GetAnnot
	// Param[in]: action	A PDF action.
	// Return:	  The annotation dictionary.
	// Remarks:   Gets the annotation dictionary.
	// Notes:
	//************************************
	static FPD_Object GetAnnot(FPD_Action action);

	//************************************
	// Function:  SetAnnot
	// Param[in]: action		A PDF action.
	// Param[in]: doc			The input PDF document.
	// Param[in]: annotDict		The input annotation dictionary.
	// Return:    void
	// Remarks:   Sets the annotation dictionary.
	// Notes:
	//************************************
	static void	SetAnnot(FPD_Action action, FPD_Document doc, FPD_Object annotDict);	

	
	//************************************
	// Function:  GetOperationType
	// Param[in]: action	A PDF action.
	// Return:    The operation type.
	// Remarks:   Gets the operation type.
	// Notes:
	//************************************
	static FS_INT32	GetOperationType(FPD_Action action);

	//************************************
	// Function:  SetOperationType
	// Param[in]: action	A PDF action.
	// Param[in]: iType		The input operation type.
	// Return:    void
	// Remarks:   Sets the operation type.
	// Notes:
	//************************************
	static void	SetOperationType(FPD_Action action, FS_INT32 iType);


	//************************************
	// Function:  GetSoundStream
	// Param[in]: action	A PDF action.
	// Return:    The sound stream object.
	// Remarks:   Gets the sound stream object.
	// Notes:
	//************************************
	static FPD_Object GetSoundStream(FPD_Action action);

	//************************************
	// Function:  GetVolume
	// Param[in]: action	A PDF action.
	// Return:    The volume of the sound.
	// Remarks:   Gets the volume of the sound.  The volume at which to play the sound, in the range -1.0 to 1.0.
	// Notes:
	//************************************
	static FS_FLOAT	GetVolume(FPD_Action action);

	//************************************
	// Function:  IsSynchronous
	// Param[in]: action	A PDF action.
	// Return:    <a>TRUE</a> for playing the sound synchronously.
	// Remarks:   Checks whether to play the sound synchronously or asynchronously.
	// Notes:
	//************************************
	static FS_BOOL IsSynchronous(FPD_Action action);

	//************************************
	// Function:  IsRepeat
	// Param[in]: action	A PDF action.
	// Return:    <a>TRUE</a> for repeating the sound indefinitely.
	// Remarks:   Checks whether to repeat the sound indefinitely.
	// Notes:
	//************************************
	static FS_BOOL IsRepeat(FPD_Action action);

	//************************************
	// Function:  IsMixPlay
	// Param[in]: action	A PDF action.
	// Return:    <a>TRUE</a> for mixing this sound with any other sound already playing.
	// Remarks:   Checks whether to mix this sound with any other sound already playing.
	// Notes:
	//************************************
	static FS_BOOL IsMixPlay(FPD_Action action);


	//************************************
	// Function:  CountOCGStates
	// Param[in]: action	A PDF action.
	// Return:    The count of OCG states.
	// Remarks:   Gets the count of OCG states.
	// Notes:
	//************************************
	static FS_INT32 CountOCGStates(FPD_Action action);

	//************************************
	// Function:  GetOCGStates
	// Param[in]: action		A PDF action.
	// Param[in]: iIndex		The zero-based OCG state index.
	// Param[out]:outState		It receives the OCG state.
	// Param[out]:outArrOcgs	It receives the OCG state.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Gets OCG array with specified state.
	// Notes: 
	//************************************
	static FS_BOOL GetOCGStates(FPD_Action action, FS_INT32 iIndex, FS_INT32* outState, FS_PtrArray* outArrOcgs);

	//************************************
	// Function:  InsertOCGStates
	// Param[in]: action		A PDF action.
	// Param[in]: doc			The input PDF document.
	// Param[in]: iIndex		The OCG state index to insert at.
	// Param[in]: OCGState		The OCG state to insert.
	// Param[in]: arrOcgs		The OCG array.
	// Return:    The number of states inserted.
	// Remarks:   Inserts a OCG state.
	// Notes:
	//************************************
	static FS_INT32	InsertOCGStates(FPD_Action action, FPD_Document doc, FS_INT32 iIndex, FPD_OCGState OCGState, const FS_PtrArray arrOcgs);

	//************************************
	// Function:  ReplaceOCGStates
	// Param[in]: action	A PDF action.
	// Param[in]: doc		The input PDF document.
	// Param[in]: iIndex	The start OCG state index to replace.
	// Param[in]: arrOcgs	The OCG array.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Replaces a OCG state.
	// Notes: 
	//************************************
	static FS_BOOL ReplaceOCGStates(FPD_Action action, FPD_Document doc, FS_INT32 iIndex, const FS_PtrArray arrOcgs);

	//************************************
	// Function:  RemoveOCGStates
	// Param[in]: action	A PDF action.
	// Param[in]: index		The OCG state index.
	// Return:    void
	// Remarks:   Removes a OCG state.
	// Notes:
	//************************************
	static void	RemoveOCGStates(FPD_Action action, FS_INT32 index);

	//************************************
	// Function:  IsStatePreserved
	// Param[in]: action	A PDF action.
	// Return:    <a>TRUE</a> for the radio-button state relationships between optional content groups applying. Otherwise not.
	// Remarks:   Checks whether the radio-button state relationships between optional content groups apply or not.
	// Notes:
	//************************************
	static FS_BOOL IsStatePreserved(FPD_Action action);

	//************************************
	// Function:  SetStatePreserved
	// Param[in]: action		A PDF action.
	// Param[in]: bPreserved	The new flag.
	// Return:    void
	// Remarks:   Sets the radio-button state relationships flag.
	// Notes:
	//************************************
	static void	SetStatePreserved(FPD_Action action, FS_BOOL bPreserved);


	//************************************
	// Function:  GetSubActionsCount
	// Param[in]: action	A PDF action.
	// Return:    The sub-action count.
	// Remarks:   Gets the sub-action count.
	// Notes:
	//************************************
	static FS_DWORD	GetSubActionsCount(FPD_Action action);

	//************************************
	// Function:  GetSubAction
	// Param[in]: action	A PDF action.
	// Param[in]: index		The zero-based sub-action index.
	// Param[out]  outAction It receives the sub-action.
	// Return:    void
	// Remarks:   Gets a sub-action.
	// Notes:
	//************************************
	static void GetSubAction(FPD_Action action, FS_DWORD index, FPD_Action* outAction);

	//************************************
	// Function:  InsertSubAction
	// Param[in]: action		A PDF action.
	// Param[in]: index			The zero-based sub-action index to insert at.
	// Param[in]: document		The PDF document.
	// Param[in]: subAction		The input sub-action.
	// Return:    void
	// Remarks:   Inserts a sub-action at specified position.
	// Notes:
	//************************************
	static void	InsertSubAction(FPD_Action action, FS_DWORD index, FPD_Document document, const FPD_Action subAction);

	//************************************
	// Function:  RemoveSubAction
	// Param[in]: action	A PDF action.
	// Param[in]: index		The zero-based sub-action index.
	// Return:    void
	// Remarks:   Removes a sub-action.
	// Notes:
	//************************************
	static void	RemoveSubAction(FPD_Action action, FS_DWORD index);

	//************************************
	// Function:  RemoveAllSubActions
	// Param[in]: action	A PDF action.
	// Return:    void
	// Remarks:   Removes all sub-actions.
	// Notes:
	//************************************
	static void	RemoveAllSubActions(FPD_Action action);

	//************************************
	// Function:  GetDict
	// Param[in]: action	A PDF action.
	// Return:    The PDF action dictionary.
	// Remarks:   Gets the PDF action dictionary.
	// Notes:
	//************************************
	static FPD_Object GetDict(FPD_Action action);	
};


//////////////////////////////////////////////////////////////////////////////////////////////////////
/*										 CFPD_AAction_V1								*/
//////////////////////////////////////////////////////////////////////////////////////////////////////

class CFPD_AAction_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: dict	The input PDF dictionary.
	// Return:    PDF additional-action.
	// Remarks:   Creates PDF additional-action from a PDF dictionary.
	// Notes:
	//************************************
	static FPD_AAction New(FPD_Object dict);

	//************************************
	// Function:  Destroy
	// Param[in]: aaction	PDF additional-action
	// Return:    void
	// Remarks:   Destroys PDF additional-action.
	// Notes:
	//************************************
	static void	Destroy(FPD_AAction aaction);
	
	//************************************
	// Function:  ActionExist
	// Param[in]: aaction	PDF additional-action
	// Param[in]: eType		The input additional-action type.
	// Return:    Non-zero means exist, otherwise not exist.
	// Remarks:   Checks whether the specified additional-action type exists or not.
	// Notes:
	//************************************
	static FS_BOOL ActionExist(FPD_AAction aaction, FPD_AActionType eType);

	//************************************
	// Function:  GetAction
	// Param[in]: aaction		PDF additional-action
	// Param[in]: eType			The input additional-action type.
	// Param[out]:outAction		An action.
	// Return:    void
	// Remarks:   Gets an action from the additional-action with specified additional-action type. 
	// Notes:
	//************************************
	static void GetAction(FPD_AAction aaction, FPD_AActionType eType, FPD_Action* outAction);

	//************************************
	// Function:  SetAction
	// Param[in]: aaction	PDF additional-action
	// Param[in]: doc		The PDF document.
	// Param[in]: eType		The additional-action type.
	// Param[in]: action	The input action.
	// Return:    void
	// Remarks:   Sets a additional-action of specified type.
	// Notes:
	//************************************
	static void SetAction(FPD_AAction aaction, FPD_Document doc, FPD_AActionType eType, const FPD_Action action);

	//************************************
	// Function:  RemoveAction
	// Param[in]: aaction	PDF additional-action
	// Param[in]: eType		The additional-action type to be removed.
	// Return:    void
	// Remarks:   Removes a additional-action.
	// Notes:
	//************************************
	static void	RemoveAction(FPD_AAction aaction, FPD_AActionType eType);

	//************************************
	// Function:  GetStartPos
	// Param[in]: aaction	PDF additional-action
	// Return:	  The start position of action list.
	// Remarks:   Gets the start position of action list.
	// Notes:
	//************************************
	static FS_POSITION GetStartPos(FPD_AAction aaction);

	//************************************
	// Function:  GetNextAction
	// Param[in]: aaction		PDF additional-action
	// Param[in,out]:outPos		Input the current position and receive the next position.
	// Param[out]:outType		Receive the additional-action type.
	// Param[out]:outAction		Receive the current action.
	// Return:    void
	// Remarks:   Gets the current action and move the position to next position.
	// Notes:
	//************************************
	static void GetNextAction(FPD_AAction aaction, FS_POSITION* outPos, FPD_AActionType* outType, FPD_Action* outAction);

	//************************************
	// Function:  GetDictionary
	// Param[in]: aaction		PDF additional-action
	// Return:    The additional-action dictionary.
	// Remarks:   Gets the additional-action dictionary.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.2.2
	//************************************
	static FPD_Object GetDictionary(FPD_AAction aaction);
};

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*								 CFPD_DocJSActions_V1								*/
//////////////////////////////////////////////////////////////////////////////////////////////////////

class CFPD_DocJSActions_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: doc	The input PDF document.
	// Return:    Document-level JavaScript actions.
	// Remarks:   Creates document-level JavaScript actions from a PDF document.
	// Notes:
	//************************************
	static FPD_DocJSActions New(FPD_Document doc);

	//************************************
	// Function:  Destroy
	// Param[in]: jsActions		The input document-level JavaScript actions.
	// Return:    void
	// Remarks:   Destroys document-level JavaScript actions.
	// Notes:
	//************************************
	static void	Destroy(FPD_DocJSActions jsActions);
	
	//************************************
	// Function:  CountJSActions
	// Param[in]: jsActions		The input document-level JavaScript actions.
	// Return:    The count of JavaScript actions.
	// Remarks:   Gets the count of JavaScript actions.
	// Notes:
	//************************************
	static FS_INT32	CountJSActions(FPD_DocJSActions jsActions);

	//************************************
	// Function:  GetJSAction
	// Param[in]: jsActions		The input document-level JavaScript actions.
	// Param[in]: index			The zero-based JavaScipt action index.
	// Param[in]: wszName		The name of the JavaScript action.
	// Param[out]:outAction		A JavaScript action.
	// Return:    void
	// Remarks:   Gets a JavaScript action with the position and the name.
	// Notes:
	//************************************
	static void GetJSAction(FPD_DocJSActions jsActions, FS_INT32 index, FS_LPCWSTR wszName, FPD_Action* outAction);

	//************************************
	// Function:  GetJSAction2
	// Param[in]: jsActions		The input document-level JavaScript actions.
	// Param[in]: wszName		The name of the JavaScript action.
	// Param[out]:outAction		A JavaScript action.
	// Return:    void
	// Remarks:   Gets a JavaScript action with the name.
	// Notes:
	//************************************
	static void GetJSAction2(FPD_DocJSActions jsActions, FS_LPCWSTR wszName, FPD_Action* outAction);

	//************************************
	// Function:  SetJSAction
	// Param[in]: jsActions		The input document-level JavaScript actions.
	// Param[in]: wszName		The name of the JavaScript action.
	// Param[in]: action		The JavaScript action.
	// Return:    The zero-based index of the JavaScript action.
	// Remarks:   Sets a JavaScript action.
	// Notes:
	//************************************
	static FS_INT32	SetJSAction(FPD_DocJSActions jsActions, FS_LPCWSTR wszName, FPD_Action action);

	//************************************
	// Function:  FindJSAction
	// Param[in]: jsActions		The input document-level JavaScript actions.
	// Param[in]: wszName		The name of the JavaScript action.
	// Return:    The zero-based index of the JavaScript action.
	// Remarks:   Finds a JavaScript action.
	//            return the position (zero based index), -1 means not found.
	// Notes:
	//************************************
	static FS_INT32	FindJSAction(FPD_DocJSActions jsActions, FS_LPCWSTR wszName);

	//************************************
	// Function:  RemoveJSAction
	// Param[in]: jsActions		The input document-level JavaScript actions.
	// Param[in]: index			The zero-based index of JavaScript action to be removed.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Removes a JavaScript action.
	// Notes:
	//************************************
	static FS_BOOL RemoveJSAction(FPD_DocJSActions jsActions, FS_INT32 index);

	//************************************
	// Function:  GetDocument
	// Param[in]: jsActions		The input document-level JavaScript actions.
	// Return:    The PDF document.
	// Remarks:   Gets the PDF document.
	// Notes:
	//************************************
	static FPD_Document	GetDocument(FPD_DocJSActions jsActions);
};

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*									 CFPD_FileSpec_V1								*/
//////////////////////////////////////////////////////////////////////////////////////////////////////


class CFPD_FileSpec_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: void
	// Return:    A new file specification object.
	// Remarks:   Creates a new file specification object - dictionary object.
	// Notes:
	//************************************
	static FPD_FileSpec	New(void);

	//************************************
	// Function:  NewFromObj
	// Param[in]: obj	The input PDF object.
	// Return:    A new file specification object.
	// Remarks:   Creates a new file specification object from a PDF object.
	// Notes:
	//************************************
	static FPD_FileSpec	NewFromObj(FPD_Object obj);

	//************************************
	// Function:  Destroy
	// Param[in]: fileSpec	A file specification object.
	// Return:    void
	// Remarks:   Destroys a file specification object.
	// Notes:
	//************************************
	static void	Destroy(FPD_FileSpec fileSpec);
	
	//************************************
	// Function:  IsURL
	// Param[in]: fileSpec	A file specification object.
	// Return:    <a>TRUE</a> for being a URL, otherwise not.
	// Remarks:   Checks whether it's a URL or not.
	//            If is an URL, FPDFileSpecGetFileName gets the URL address.
	// Notes:
	//************************************
	static FS_BOOL IsURL(FPD_FileSpec fileSpec);

	//************************************
	// Function:  GetFileName
	// Param[in]: fileSpec	A file specification object.
	// Param[out]:outFile	It receives the file name.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Gets the file name.
	// Notes:
	//************************************
	static FS_BOOL GetFileName(FPD_FileSpec fileSpec, FS_WideString* outFile);

	//************************************
	// Function:  GetFileStream
	// Param[in]: fileSpec	A file specification object.
	// Return:    The PDF stream object from the file specification.
	// Remarks:   Gets a PDF stream from the file specification.
	// Notes:
	//************************************
	static FPD_Object GetFileStream(FPD_FileSpec fileSpec);

	//************************************
	// Function:  SetFileName
	// Param[in]: fileSpec	A file specification object.
	// Param[in]: wszFile	Input a file name.
	// Param[in]: bURL		Whether the input file name is an URL.
	// Return:    void
	// Remarks:   Sets the file name.
	// Notes:
	//************************************
	static void	SetFileName(FPD_FileSpec fileSpec, FS_LPCWSTR wszFile, FS_BOOL bURL);

	//************************************
	// Function:  SetEmbeddedFile
	// Param[in]: fileSpec				A file specification object.
	// Param[in]: doc					The PDF document.
	// Param[in]: pFileReadHandler		The file access interface.
	// Param[in]: szFilePath			The file path.
	// Return:    void
	// Remarks:   Sets an embedded file.
	// Notes:
	//************************************
	static void	SetEmbeddedFile(FPD_FileSpec fileSpec, FPD_Document doc, FS_FileReadHandler pFileReadHandler, FS_LPCWSTR szFilePath);
	
	
};

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*										 CFPD_Media_V1								*/
//////////////////////////////////////////////////////////////////////////////////////////////////////

class CFPD_MediaPlayer_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: void
	// Return:    An empty media player information object.
	// Remarks:   Creates an empty media player information object.
	// Notes:
	//************************************
	static FPD_MediaPlayer New(void);

	//************************************
	// Function:  NewFromDict
	// Param[in]: dict	The input PDF dictionary.
	// Return:    An media player information object.
	// Remarks:   Creates an media player information object from a PDF dictionary.
	// Notes:
	//************************************
	static FPD_MediaPlayer NewFromDict(FPD_Object dict);

	//************************************
	// Function:  Destroy
	// Param[in]: player	The media player information object.
	// Return:    void
	// Remarks:   Destroys the media player information object.
	// Notes:
	//************************************
	static void	Destroy(FPD_MediaPlayer player);

	
	//************************************
	// Function:  GetSoftwareURI
	// Param[in]: player	The media player information object.
	// Param[out]:outURL	The software URI.
	// Return:    void
	// Remarks:   Gets the software URI.
	// Notes:
	//************************************
	static void	GetSoftwareURI(FPD_MediaPlayer player, FS_ByteString* outURL);

	//************************************
	// Function:  SetSoftwareURI
	// Param[in]: player	The media player information object.
	// Param[in]: szURI		The input software URI.
	// Return:    void
	// Remarks:   Sets the software URI.
	// Notes:
	//************************************
	static void	SetSoftwareURI(FPD_MediaPlayer player, FS_LPCSTR szURI);


	//************************************
	// Function:  GetOSArray
	// Param[in]: player		The media player information object.
	// Param[out]:outOSArray	It receives the OS array.
	// Return:    The number of OS in the array.
	// Remarks:   Gets the OS array in the software identifier dictionary.
	// Notes:
	//************************************
	static FS_INT32	GetOSArray(FPD_MediaPlayer player, FS_ByteStringArray* outOSArray);

	//************************************
	// Function:  SetOSArray
	// Param[in]: player	The media player information object.
	// Param[in]: osArray	The input OS array.
	// Return:    void
	// Remarks:   Sets the OS array.
	// Notes:
	//************************************
	static void	SetOSArray(FPD_MediaPlayer player, const FS_ByteStringArray osArray);
};

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*									 CFPD_Rendition_V1								*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
class CFPD_Rendition_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: void
	// Return:    An empty rendition object.
	// Remarks:   Creates an empty rendition object.
	// Notes:
	//************************************
	static FPD_Rendition New(void);

	//************************************
	// Function:  NewFromDict
	// Param[in]: dict	The input PDF dictionary.
	// Return:    A rendition object.
	// Remarks:   Creates a rendition object from a PDF dictionary.
	// Notes:
	//************************************
	static FPD_Rendition NewFromDict(FPD_Object dict);

	//************************************
	// Function:  Destroy
	// Param[in]: rendition		The input rendition object.
	// Return:    void
	// Remarks:   Destroys a rendition object.
	// Notes:
	//************************************
	static void	Destroy(FPD_Rendition rendition);	

	
	//************************************
	// Function:  HasMediaClip
	// Param[in]: rendition		The input rendition object.
	// Return:    <a>TRUE</a> for the rendition dictionary having the C entry, otherwise not.
	// Remarks:   Checks whether the rendition dictionary has the C entry.
	// Notes:
	//************************************
	static FS_BOOL HasMediaClip(FPD_Rendition rendition);


	//************************************
	// Function:  GetRenditionName
	// Param[in]: rendition		The input rendition object.
	// Param[out]:outName		The rendition name.
	// Return:    void
	// Remarks:   Gets the rendition name.
	// Notes:
	//************************************
	static void	GetRenditionName(FPD_Rendition rendition, FS_WideString* outName);

	//************************************
	// Function:  SetRenditionName
	// Param[in]: rendition		The input rendition object.
	// Param[in]: wszName		The input rendition name.
	// Return:    void
	// Remarks:   Sets the rendition name.
	// Notes:
	//************************************
	static void	SetRenditionName(FPD_Rendition rendition, FS_LPCWSTR wszName);


	//************************************
	// Function:  GetMediaClipName
	// Param[in]: rendition		The input rendition object.
	// Param[out]:outName		The media clip name.
	// Return:    void
	// Remarks:   Gets the media clip name.
	// Notes:
	//************************************
	static void	GetMediaClipName(FPD_Rendition rendition, FS_WideString* outName);

	//************************************
	// Function:  SetMediaClipName
	// Param[in]: rendition		The input rendition object.
	// Param[in]: wszName		The input media clip name.
	// Return:    void
	// Remarks:   Sets the media clip name.
	// Notes:
	//************************************
	static void	SetMediaClipName(FPD_Rendition rendition, FS_LPCWSTR wszName);


	//************************************
	// Function:  GetMediaClipFile
	// Param[in]: rendition		The input rendition object.
	// Param[out]:outFileSpec	The file specification of the actual media data.
	// Return:    void
	// Remarks:   Gets the file specification of the actual media data.
	// Notes:
	//************************************
	static void GetMediaClipFile(FPD_Rendition rendition, FPD_FileSpec* outFileSpec);

	//************************************
	// Function:  SetMediaClipFile
	// Param[in]: rendition		The input rendition object.
	// Param[in]: doc			The input PDF document.
	// Param[in]: file			The input file specification object.
	// Return:    void
	// Remarks:   Sets the file specification of the actual media data.
	// Notes:
	//************************************
	static void	SetMediaClipFile(FPD_Rendition rendition, FPD_Document doc, FPD_FileSpec file);


	//************************************
	// Function:  GetMediaClipContentType
	// Param[in]: rendition			The input rendition object.
	// Param[out]:outContentType	The content type (MIME type) of the media data.
	// Return:    void
	// Remarks:   Gets the content type (MIME type) of the media data.
	// Notes:
	//************************************
	static void	GetMediaClipContentType(FPD_Rendition rendition, FS_ByteString* outContentType);

	//************************************
	// Function:  SetMediaClipContentType
	// Param[in]: rendition			The input rendition object.
	// Param[in]: szContentType		The input content type.
	// Return:    void
	// Remarks:   Sets the content type (MIME type) of the media data.
	// Notes:
	//************************************
	static void	SetMediaClipContentType(FPD_Rendition rendition, FS_LPCSTR szContentType );


	//************************************
	// Function:  GetPermission
	// Param[in]: rendition		The input rendition object.
	// Return:    The media permission
	// Remarks:   Gets the media permission that indicates the circumstances under which it is acceptable 
	//			  to write a temporary file in order to play a media clip.
	// Notes:
	//************************************
	static FPD_MediaPermission	GetPermission(FPD_Rendition rendition);

	//************************************
	// Function:  SetPermission
	// Param[in]: rendition			The input rendition object.
	// Param[in]: ePermission		The input media permission.
	// Return:    void
	// Remarks:   Sets the media permission.
	// Notes:
	//************************************
	static void	SetPermission(FPD_Rendition rendition, FPD_MediaPermission ePermission);


	//************************************
	// Function:  GetMediaDescriptions	
	// Param[in]: rendition			The input rendition object.
	// Param[out]:outDescArray		It receives the text descriptions array.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Gets the text descriptions array that provides alternate text descriptions for 
	//            the media clip data in case it cannot be played.
	// Notes:
	//************************************
	static FS_BOOL GetMediaDescriptions(FPD_Rendition rendition, FS_WideStringArray* outDescArray);

	//************************************
	// Function:  SetMediaDescriptions
	// Param[in]: rendition		The input rendition object.
	// Param[in]: descArray		The input text descriptions array.
	// Return:    void
	// Remarks:   Sets the text descriptions array.
	// Notes:
	//************************************
	static void	SetMediaDescriptions(FPD_Rendition rendition, const FS_WideStringArray descArray);


	//************************************
	// Function:  GetMediaBaseURL
	// Param[in]: rendition		The input rendition object.
	// Param[out]:outURL		It receives the absolute URL.
	// Return:    void
	// Remarks:   Gets an absolute URL to be used as the base URL in resolving any relative URLs found within the media data.
	// Notes:
	//************************************
	static void	GetMediaBaseURL(FPD_Rendition rendition, FS_ByteString* outURL);

	//************************************
	// Function:  SetMediaBaseURL
	// Param[in]: rendition		The input rendition object.
	// Param[in]: szBaseURL		The input base URL.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the base URL.
	// Notes:
	//************************************
	static void	SetMediaBaseURL(FPD_Rendition rendition, FS_LPCSTR szBaseURL, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  CountMediaPlayers
	// Param[in]: rendition		The input rendition object.
	// Param[in]: ePlayerType	The input media player type.
	// Return:    The count of media player of specified type.
	// Remarks:   Gets the count of media players of specified type.
	// Notes:
	//************************************
	static FS_INT32	CountMediaPlayers(FPD_Rendition rendition, FPD_MediaPlayerType ePlayerType);

	//************************************
	// Function:  GetMediaPlayer
	// Param[in]: rendition			The input rendition object.
	// Param[in]: ePlayerType		The input media player type.
	// Param[in]: index				The zero-based index in the specified media player type.
	// Param[out]:outMediaPlayer	A media player.
	// Return:    void
	// Remarks:   Gets a media player.
	// Notes:
	//************************************
	static void GetMediaPlayer(FPD_Rendition rendition, FPD_MediaPlayerType ePlayerType, FS_INT32 index, FPD_MediaPlayer* outMediaPlayer);

	//************************************
	// Function:  AddMediaPlayer
	// Param[in]: rendition		The input rendition object.
	// Param[in]: ePlayerType	The input media player type.
	// Param[in]: player		The input media player.
	// Return:    The current count of specified media player type.
	// Remarks:   Adds a media player.
	// Notes:
	//************************************
	static FS_INT32 AddMediaPlayer(FPD_Rendition rendition, FPD_MediaPlayerType ePlayerType, FPD_MediaPlayer player);

	//************************************
	// Function:  RemoveMediaPlayer
	// Param[in]: rendition		The input rendition object.
	// Param[in]: ePlayerType	The input media player type.
	// Param[in]: player		The media player to be removed.
	// Return:    void
	// Remarks:   Removes a media player.
	// Notes:
	//************************************
	static void	RemoveMediaPlayer(FPD_Rendition rendition, FPD_MediaPlayerType ePlayerType, FPD_MediaPlayer player);


	//************************************
	// Function:  GetVolumn	
	// Param[in]: rendition		The input rendition object.
	// Return:    The volume
	// Remarks:   Gets the volume that specifies the desired volume level as a percentage of recorded volume level.
	// Notes:
	//************************************
	static FS_INT32 GetVolumn(FPD_Rendition rendition);
	//************************************
	// Function:  SetVolumn
	// Param[in]: rendition		The input rendition object.
	// Param[in]: iVolumn		The input volume.
	// Param[in]: eParamType	The media play param type.
	// Return:    void
	// Remarks:   Sets the volume.
	// Notes:
	//************************************
	static void	SetVolumn(FPD_Rendition rendition, FS_INT32 iVolumn, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  IsControlBarVisible
	// Param[in]: rendition		The input rendition object.
	// Return:    <a>TRUE</a> for being visible, otherwise not.
	// Remarks:   Checks to display a player-specific controller user interface (for example, play/pause/stop controls) when playing.
	// Notes:
	//************************************
	static FS_BOOL IsControlBarVisible(FPD_Rendition rendition);


	//************************************
	// Function:  EnableControlBarVisible
	// Param[in]: rendition		The input rendition object.
	// Param[in]: bVisible		The input visibility flag of the control bar.
	// Param[in]: eParamType		The input media play param type.
	// Return:    void	
	// Remarks:   Sets the control bar visibility flag.
	// Notes:
	//************************************
	static void	EnableControlBarVisible(FPD_Rendition rendition, FS_BOOL bVisible, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  GetFitStyle
	// Param[in]: rendition		The input rendition object.
	// Return:    The fit style (manner).
	// Remarks:   Gets the fit style (manner) in which the player should treat a visual media 
	//            type that does not exactly fit the rectangle in which it plays.
	// Notes:
	//************************************
	static FS_INT32	GetFitStyle(FPD_Rendition rendition);

	//************************************
	// Function:  SetFitStyle
	// Param[in]: rendition		The input rendition object.
	// Param[in]: iFitStyle		The input fit style.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the fit style.
	// Notes:
	//************************************
	static void	SetFitStyle(FPD_Rendition rendition, FS_INT32 iFitStyle, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  GetDuration	
	// Param[in]: rendition		The input rendition object.
	// Return:    The intrinsic duration.
	// Remarks:   Gets the intrinsic duration.
	// Notes:
	//************************************
	static FS_INT32	GetDuration(FPD_Rendition rendition);

	//************************************
	// Function:  SetDuration
	// Param[in]: rendition		The input rendition object.
	// Param[in]: iDuration		The input intrinsic duration.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the intrinsic duration.
	// Notes:
	//************************************
	static void	SetDuration(FPD_Rendition rendition, FS_INT32 iDuration, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  AutoPlay
	// Param[in]: rendition		The input rendition object.
	// Return:    <a>TRUE</a> for the media automatically playing when activated, otherwise not.
	// Remarks:   Checks whether the media should automatically play when activated.
	// Notes:
	//************************************
	static FS_BOOL AutoPlay(FPD_Rendition rendition);

	//************************************
	// Function:  EnableAutoPlay
	// Param[in]: rendition		The input rendition object.
	// Param[in]: bAuto			The input auto-play flag.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the auto-play flag.
	// Notes:
	//************************************
	static void	EnableAutoPlay(FPD_Rendition rendition, FS_BOOL bAuto, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  RepeatCount
	// Param[in]: rendition		The input rendition object.
	// Return:    The repeat count.
	// Remarks:	  Gets the repeat count.
	// Notes:
	//************************************
	static FS_INT32	RepeatCount(FPD_Rendition rendition);

	//************************************
	// Function:  SetRepeatCount
	// Param[in]: rendition		The input rendition object.
	// Param[in]: iCount		The input repeat count.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the repeat count.
	// Notes:
	//************************************
	static void	SetRepeatCount(FPD_Rendition rendition, FS_INT32 iCount, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  GetWindowStatus	
	// Param[in]: rendition		The input rendition object.
	// Return:    0: floating window; 1: full screen; 2: hidden window; 3: child window.
	// Remarks:   Gets the window status(type) that the media object should play in.
	// Notes:
	//************************************
	static FS_INT32	GetWindowStatus(FPD_Rendition rendition);

	//************************************
	// Function:  SetWindowStatus	
	// Param[in]: rendition		The input rendition object.
	// Param[in]: iStatus		The input window status.0: floating window; 1: full screen; 2: hidden window; 3: child window.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the window status.
	// Notes:
	//************************************
	static void	SetWindowStatus(FPD_Rendition rendition, FS_INT32 iStatus, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  GetBackgroundColor	
	// Param[in]: rendition		The input rendition object.
	// Return:    The background color for the rectangle in which the media is being played. 
	// Remarks:   Gets the background color for the rectangle in which the media is being played. 
	// Notes:
	//************************************
	static FS_COLORREF GetBackgroundColor(FPD_Rendition rendition);

	//************************************
	// Function:  SetBackgroundColor
	// Param[in]: rendition		The input rendition object.
	// Param[in]: color			The input background color.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the background color.
	// Notes:
	//************************************
	static void	SetBackgroundColor(FPD_Rendition rendition, FS_COLORREF color, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  GetBackgroundOpacity	
	// Param[in]: rendition		The input rendition object.
	// Return:    The background opacity.
	// Remarks:   Gets the background opacity.
	// Notes:
	//************************************
	static FS_FLOAT	GetBackgroundOpacity(FPD_Rendition rendition);

	//************************************
	// Function:  SetBackgroundOpacity
	// Param[in]: rendition		The input rendition object.
	// Param[in]: iOpacity		The input background opacity.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the background opacity.
	// Notes:
	//************************************
	static void	SetBackgroundOpacity(FPD_Rendition rendition, FS_FLOAT iOpacity, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  GetMonitor
	// Param[in]: rendition		The input rendition object.
	// Return:    The monitor specifier
	// Remarks:   Gets the monitor specifier that specifies which monitor in a multi-monitor 
	//            system a floating or full-screen window should appear on.
	// Notes:
	//************************************
	static FS_INT32	GetMonitor(FPD_Rendition rendition);

	//************************************
	// Function:  SetMonitor	
	// Param[in]: rendition		The input rendition object.
	// Param[in]: iMonitor		The input monitor specifier.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the monitor specifier.
	// Notes:
	//************************************
	static void	SetMonitor(FPD_Rendition rendition, FS_INT32 iMonitor, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  GetFloatingWindowSize
	// Param[in]: rendition		The input rendition object.
	// Param[out]:iWidth		It receives the floating window's width.
	// Param[out]:iHeight		It receives the floating window's height.
	// Return:    <a>TRUE</a>: returned size is available; <a>FALSE</a>: not available.
	// Remarks:   Gets the floating window's width and height.
	// Notes:
	//************************************
	static FS_BOOL GetFloatingWindowSize(FPD_Rendition rendition, FS_INT32* iWidth, FS_INT32* iHeight);

	//************************************
	// Function:  SetFloatingWindowSize
	// Param[in]: rendition		The input rendition object.
	// Param[in]: iWidth		The width of the floating window.
	// Param[in]: iHeight		The height of the floating window.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the floating window's width and height.
	// Notes:
	//************************************
	static void	SetFloatingWindowSize(
		FPD_Rendition rendition, 
		FS_INT32 iWidth, 
		FS_INT32 iHeight, 
		FPD_MediaPlayParamType eParamType
		);


	//************************************
	// Function:  GetFloatingWindowRelativeType
	// Param[in]: rendition		The input rendition object.
	// Return:    0: document window; 1: application window; 2: full virtual desktop; 3: monitor screen.
	// Remarks:   Gets the window type relative to which the floating window should be positioned. 
	// Notes:
	//************************************
	static FS_INT32	GetFloatingWindowRelativeType(FPD_Rendition rendition);

	//************************************
	// Function:  SetFloatingWindowRelativeType
	// Param[in]: rendition		The input rendition object.
	// Param[in]: iType			The input window relative type. 0: document window; 1: application window; 2: full virtual desktop; 3: monitor screen.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the window relative type.
	// Notes:
	//************************************
	static void	SetFloatingWindowRelativeType(FPD_Rendition rendition, FS_INT32 iType, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  GetFloatingWindowPosition	
	// Param[in]: rendition		The input rendition object.
	// Return:    0: upper left; 1: upper center; 2: upper right; 3: center left; 4: center; 5: center right; 6: lower left; 7: lower center; 8: lower right.
	// Remarks:   Gets the floating window position.
	// Notes:
	//************************************
	static FS_INT32 GetFloatingWindowPosition(FPD_Rendition rendition);

	//************************************
	// Function:  SetFloatingWindowPosition
	// Param[in]: rendition		The input rendition object.
	// Param[in]: iPosition		The input floating window position. 0: upper left; 1: upper center; 2: upper right; 3: center left; 4: center; 5: center right; 6: lower left; 7: lower center; 8: lower right.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the floating window position.
	// Notes:
	//************************************
	static void	SetFloatingWindowPosition(FPD_Rendition rendition, FS_INT32 iPosition, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  GetFloatingWindowOffscreen
	// Param[in]: rendition		The input rendition object.
	// Return:    0: nothing; 1: move or resize; 2: consider the object non-viable.
	// Remarks:   Gets what should occur if the floating window is positioned totally or partially off screen.
	// Notes:
	//************************************
	static FS_INT32	GetFloatingWindowOffscreen(FPD_Rendition rendition);

	//************************************
	// Function:  SetFloatingWindowOffscreen
	// Param[in]: rendition		The input rendition object.
	// Param[in]: iOffscreen	The input action Function.0: nothing; 1: move or resize; 2: consider the object non-viable.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the action Function.
	// Notes:
	//************************************
	static void	SetFloatingWindowOffscreen(FPD_Rendition rendition, FS_INT32 iOffscreen, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  HasFloatingWindowTitleBar
	// Param[in]: rendition		The input rendition object.
	// Return:    <a>TRUE</a> for the floating window having a title bar.
	// Remarks:   Checks whether the floating window should have a title bar.
	// Notes:
	//************************************
	static FS_BOOL HasFloatingWindowTitleBar(FPD_Rendition rendition);

	//************************************
	// Function:  EnableFloatingWindowTitleBar
	// Param[in]: rendition		The input rendition object.
	// Param[in]: bVisible		The input visibility flag.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the title bar visibility flag.
	// Notes:
	//************************************
	static void	EnableFloatingWindowTitleBar(FPD_Rendition rendition, FS_BOOL bVisible, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  HasFloatingWindowCloseButton	
	// Param[in]: rendition		The input rendition object.
	// Return:    <a>TRUE</a> for the floating window including user interface elements, otherwise not.
	// Remarks:   Checks whether the floating window should include user interface elements that allow a user to close a floating window.
	// Notes:
	//************************************
	static FS_BOOL HasFloatingWindowCloseButton(FPD_Rendition rendition);

	//************************************
	// Function:  EnableFloatingWindowCloseButton
	// Param[in]: rendition		The input rendition object.
	// Param[in]: bVisible		The input visibility flag.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the visibility flag of close-button.
	// Notes:
	//************************************
	static void	EnableFloatingWindowCloseButton(FPD_Rendition rendition, FS_BOOL bVisible, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  GetFloatingWindowResizeType	
	// Param[in]: rendition		The input rendition object.
	// Return:    0: may not be resized; 1: resize with aspect ratio; 2: resize anyway.
	// Remarks:   Checks whether the floating window may be resized by a user.
	// Notes:
	//************************************
	static FS_INT32	GetFloatingWindowResizeType(FPD_Rendition rendition);

	//************************************
	// Function:  SetFloatingWindowResizeType
	// Param[in]: rendition		The input rendition object.
	// Param[in]: iType			The input floating window resizing type. 0: may not be resized; 1: resize with aspect ratio; 2: resize anyway.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the floating window resizing type.
	// Notes:
	//************************************
	static void	SetFloatingWindowResizeType(FPD_Rendition rendition, FS_INT32 iType, FPD_MediaPlayParamType eParamType);


	//************************************
	// Function:  GetFloatingWindowTitle
	// Param[in]: rendition			The input rendition object.
	// Param[out]:outTitleArray		It receives the multi-language text array.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Gets the multi-language text array providing text to display on the floating window's title bar.
	// Notes:
	//************************************
	static FS_BOOL GetFloatingWindowTitle(FPD_Rendition rendition, FS_WideStringArray* outTitleArray);

	//************************************
	// Function:  SetFloatingWindowTitle
	// Param[in]: rendition		The input rendition object.
	// Param[in]: titleArray	The input multi-language text array.
	// Param[in]: eParamType	The input media play param type.
	// Return:    void
	// Remarks:   Sets the multi-language text array to provide text for title bar.
	// Notes:
	//************************************
	static void	SetFloatingWindowTitle(FPD_Rendition rendition, FS_WideStringArray titleArray, FPD_MediaPlayParamType eParamType);
};



//////////////////////////////////////////////////////////////////////////////////////////////////////
/*										 CFPD_Link_V1								*/
//////////////////////////////////////////////////////////////////////////////////////////////////////


class CFPD_Link_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: dict		The input PDF dictionary.
	// Return:    A PDF link.
	// Remarks:   Creates a PDF link from a PDF dictionary.
	// Notes:
	//************************************
	static FPD_Link	New(FPD_Object dict);

	//************************************
	// Function:  Destroy
	// Param[in]: link		The input PDF link.
	// Return:    void
	// Remarks:   Destroys a PDF link.
	// Notes:
	//************************************
	static void	Destroy(FPD_Link link);
	
	//************************************
	// Function:  GetLinkAtPoint
	// Param[in]: doc		The input PDF document.
	// Param[in]: page		The input PDF page.
	// Param[in]: x			The input x-coordinate of the point.
	// Param[in]: y			The input y-coordinate of the point.
	// Param[out]: outLink It receives the link at specified point.
	// Return:	  void
	// Remarks:   Gets the link at specified point. The point is specified in user space. 
	//            <a>NULL</a> for no link at that point.
	// Notes:
	//************************************
	static void	GetLinkAtPoint(FPD_Document doc, FPD_Page page, FS_FLOAT x, FS_FLOAT y, FPD_Link* outLink);

	//************************************
	// Function:  CountLinks
	// Param[in]: doc		The input PDF document.
	// Param[in]: page		The input PDF page.
	// Return:    The count of links in the page.
	// Remarks:   Gets the count of links in specified page.
	// Notes:
	//************************************
	static FS_INT32	CountLinks(FPD_Document doc, FPD_Page page);

	//************************************
	// Function:  GetLink
	// Param[in]: doc		The input PDF document.
	// Param[in]: page		The input PDF page.
	// Param[in]: index		The zero-based link index int the page.
	// Param[out]: outLink It receives the link by index.
	// Return:    void
	// Remarks:   Gets a PDF link.
	// Notes:
	//************************************
	static void	GetLink(FPD_Document doc, FPD_Page page, FS_INT32 index, FPD_Link* outLink);
	
	//************************************
	// Function:  GetRect
	// Param[in]: link The input link.
	// Return:    The rectangle in which the link should be activated.
	// Remarks:   Gets the rectangle in which the link should be activated.
	// Notes:
	//************************************
	static FS_FloatRect	GetRect(FPD_Link link);


	//************************************
	// Function:  GetDest
	// Param[in]: link		The input PDF link.
	// Param[in]: doc		The input PDF document.
	// Param[out]:outDest	The destination of the link.
	// Return:    void
	// Remarks:   Gets the destination of the link.
	// Notes:
	//************************************
	static void GetDest(FPD_Link link, FPD_Document doc, FPD_Dest* outDest);

	//************************************
	// Function:  GetAction
	// Param[in]: link			The input PDF link.
	// Param[out]:outAction		The action of the link.
	// Return:    void
	// Remarks:   Gets the action of the link.
	// Notes:
	//************************************
	static void GetAction(FPD_Link link, FPD_Action* outAction);
};


//////////////////////////////////////////////////////////////////////////////////////////////////////
/*										 CFPD_Annot_V1								*/
//////////////////////////////////////////////////////////////////////////////////////////////////////

class CFPD_Annot_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: dict		The input PDF dictionary.
	// Return:    An annotation.
	// Remarks:   Creates an annotation from a PDF dictionary.
	// Notes:
	//************************************
	static FPD_Annot New(FPD_Object dict);

	//************************************
	// Function:  Destroy
	// Param[in]: annot		The input annotation.
	// Return:    void
	// Remarks:   Destroys an annotation.
	// Notes:
	//************************************
	static void	Destroy(FPD_Annot annot);
	
	//************************************
	// Function:  GetAnnotDict
	// Param[in]: annot		The input annotation.
	// Return:    The annotation dictionary.
	// Remarks:   Gets the annotation dictionary. We store all information within the original annotation dictionary.
	// Notes:
	//************************************
	static FPD_Object GetAnnotDict(FPD_Annot annot);

	//************************************
	// Function:  GetSubType
	// Param[in]: annot			The input annotation.
	// Param[out]:outSubType	It receives the annotation type name.
	// Return:    void
	// Remarks:   Gets the annotation type name.
	// Notes:
	//************************************
	static void	GetSubType(FPD_Annot annot, FS_ByteString* outSubType);

	//************************************
	// Function:  GetFlags
	// Param[in]: annot		The input annotation.
	// Return:    The annotation flags.
	// Remarks:   Gets the annotation flags.
	// Notes:
	//************************************
	static FS_DWORD	GetFlags(FPD_Annot annot);

	//************************************
	// Function:  GetRect
	// Param[in]: annot		The input annotation.
	// Return:    The bounding box of the annotation. 
	// Remarks:   Gets annotation bounding box in user space.
	// Notes:
	//************************************
	static FS_FloatRect	GetRect(FPD_Annot annot);

	//************************************
	// Function:  DrawAppearance
	// Param[in]: annot		The input annotation.
	// Param[in]: page		The page it belongs to.
	// Param[in]: dc		The device to draw on.
	// Param[in]: matrix	The transformation matrix from user space to device space.
	// Param[in]: eMode		The input appearance mode.
	// Param[in]: opts		The render options.
	// Return:    <a>TRUE</a> if the appearance successfully found and drawn.
	// Remarks:   Draws annotation's appearance, using default appearance rules.
	// Notes:
	//************************************
	static FS_BOOL DrawAppearance(FPD_Annot annot, 
		const FPD_Page page, 
		FPD_RenderDevice dc, 
		FS_AffineMatrix matrix, 
		FPD_AnnotAppearanceMode eMode, 
		const FPD_RenderOptions opts);

	//************************************
	// Function:  DrawInContext
	// Param[in]: annot		The input annotation.
	// Param[in]: page		The page it belongs to.
	// Param[in]: context	The context to draw in.
	// Param[in]: matrix	The transformation matrix from user space to device space.
	// Param[in]: eMode		The input appearance mode.
	// Return:    <a>TRUE</a> if the appearance successfully found and drawn.
	// Remarks:   Draws annotation's appearance in rendering context.
	// Notes:
	//************************************
	static FS_BOOL DrawInContext(
		FPD_Annot annot, 
		const FPD_Page page, 
		const FPD_RenderContext context,
		FS_AffineMatrix matrix, 
		FPD_AnnotAppearanceMode eMode
		);

	//************************************
	// Function:  ClearCachedAP
	// Param[in]: annot		The input annotation.
	// Return:    void
	// Remarks:   Clears all cached appearance, when the application changed any appearance settings.
	// Notes:
	//************************************
	static void	ClearCachedAP(FPD_Annot annot);

	//************************************
	// Function:  DrawBorder
	// Param[in]: annot		The input annotation.
	// Param[in]: dc		The device to draw on.
	// Param[in]: matrix	The transformation matrix from user space to device space.
	// Param[in]: opts		The render options.
	// Return:    void
	// Remarks:   Draws border of the annotation, using border settings within the dictionary.
	// Notes:
	//************************************
	static void	DrawBorder(
		FPD_Annot annot, 
		FPD_RenderDevice dc, 
		FS_AffineMatrix matrix,
		const FPD_RenderOptions opts
		);


	//************************************
	// Function:  CountIRTNotes
	// Param[in]: annot		The input annotation.
	// Return:    The count of annotations in "in reply to" annotation list.
	// Remarks:   Gets the count of annotations in "in reply to" annotation list.
	// Notes:
	//************************************
	static FS_INT32	CountIRTNotes(FPD_Annot annot);

	//************************************
	// Function:  GetIRTNote
	// Param[in]: annot		The input annotation.
	// Param[in]: index		The zero-based index in the "in reply to" annotation list.
	// Return:    An "in reply to" annotation.
	// Remarks:   Gets an "in reply to" annotation. 
	// Notes:
	//************************************
	static FPD_Annot GetIRTNote(FPD_Annot annot, FS_INT32 index);

	//************************************
	// Function:  GetAPForm
	// Param[in]: annot		The input annotation.
	// Param[in]: page		The page it belongs to.
	// Param[in]: eMode		The input appearance mode.
	// Return:    A PDF form.
	// Remarks:   Gets an appearance stream.
	// Notes:
	//************************************
	static FPD_Form	GetAPForm(FPD_Annot annot, const FPD_Page page, FPD_AnnotAppearanceMode eMode);

	static void    SetPrivateData(FPD_Annot annot, FS_LPVOID module_id, FS_LPVOID pData, FRPD_CALLBACK_FREEDATA callback);

	static FS_LPVOID       GetPrivateData(FPD_Annot annot, FS_LPVOID module_id);
};



//////////////////////////////////////////////////////////////////////////////////////////////////////
/*									 CFPD_AnnotList_V1								*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
class CFPD_AnnotList_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: page		The input PDF page.
	// Return:    An annotation list.
	// Remarks:   Creates an annotation list from a PDF page.
	// Notes:
	//************************************
	static FPD_AnnotList New(FPD_Page page);

	//************************************
	// Function:  Destroy
	// Param[in]: annotList		The input annotation list.
	// Return:    void
	// Remarks:   Destroys an annotation list.
	// Notes:
	//************************************
	static void	Destroy(FPD_AnnotList annotList);
	
	//************************************
	// Function:  DisplayAnnots
	// Param[in]: annotList		The input annotation list.
	// Param[in]: page			The page it belongs to.
	// Param[in]: dc			The device to draw on.
	// Param[in]: matrix		The transformation matrix from objects space to device space.
	// Param[in]: bShowWidget	Whether to show widget or not.
	// Param[in]: opts			The render options.
	// Return:    void
	// Remarks:   Display all annotations with specified object to DC transformation.
	// Notes:
	//************************************
	static void	DisplayAnnots(
		FPD_AnnotList annotList, 
		const FPD_Page page, 
		FPD_RenderDevice dc, 
		FS_AffineMatrix matrix, 
		FS_BOOL bShowWidget, 
		FPD_RenderOptions opts
		);
	
	//************************************
	// Function:  DisplayAnnotsEx
	// Param[in]: annotList		The input annotation list.
	// Param[in]: pPage			The page it belongs to.
	// Param[in]: context		The context to draw in.
	// Param[in]: bPrinting		Whether to print or not.
	// Param[in]: matrix		The transformation matrix from objects space to device space.
	// Param[in]: bShowWidget	Whether to show widget or not.
	// Param[in]: opts			The render options.
	// Return:    void
	// Remarks:   Adds annotations to render job list, used in printing for transparency support.
	// Notes:
	//************************************
	static void	DisplayAnnotsEx(
		FPD_AnnotList annotList, 
		const FPD_Page pPage, 
		FPD_RenderContext context, 
		FS_BOOL bPrinting, 
		FS_AffineMatrix matrix, 
		FS_BOOL bShowWidget, 
		FPD_RenderOptions opts
		);
	


	//************************************
	// Function:  GetAt
	// Param[in]: annotList		The input annotation list.
	// Param[in]: index			The zero-based index in the annotation list.
	// Return:    An annotation.
	// Remarks:   Gets an annotation at specified position.
	// Notes:
	//************************************
	static FPD_Annot GetAt(FPD_AnnotList annotList, FS_INT32 index);

	//************************************
	// Function:  Count
	// Param[in]: annotList		The input annotation list.
	// Return:    The count of annotations in the annotation list.
	// Remarks:   Gets the count of annotations in the annotation list.
	// Notes:
	//************************************
	static FS_INT32	Count(FPD_AnnotList annotList);

	//************************************
	// Function:  GetIndex
	// Param[in]: annotList		The input annotation list.
	// Param[in]: annot			The input annotation.
	// Return:    The zero-based index in the annotation list.
	// Remarks:   Gets the annotation's index.
	// Notes:
	//************************************
	static FS_INT32	GetIndex(FPD_AnnotList annotList, FPD_Annot annot);


	//************************************
	// Function:  RemoveAll
	// Param[in]: annotList		The input annotation list.
	// Return:    void
	// Remarks:   Removes all the annotations.
	// Notes:     Operations to the annotation list will be reflected in the PDF document structure.
	//            For example, the "Annots" array of page dictionary will get changed.
	//************************************
	static void	RemoveAll(FPD_AnnotList annotList);

	//************************************
	// Function:  Replace
	// Param[in]: annotList		The input annotation list.
	// Param[in]: index			The zero-based index in the annotation list.
	// Param[in]: annot			The new annotation.
	// Return:    void
	// Remarks:   Replaces the specified position with a new annotation.
	// Notes:
	//************************************
	static void	Replace(FPD_AnnotList annotList, FS_INT32 index, FPD_Annot annot);

	//************************************
	// Function:  Insert
	// Param[in]: annotList		The input annotation list.
	// Param[in]: index			The zero-based index to insert before.
	// Param[in]: annot			The annotation to insert.
	// Return:    void
	// Remarks:   Inserts BEFORE the index, if index equals to current count, then append.
	// Notes:
	//************************************
	static void	Insert(FPD_AnnotList annotList, FS_INT32 index, FPD_Annot annot);

	//************************************
	// Function:  Remove
	// Param[in]: annotList		The input annotation list.
	// Param[in]: index			The zero-based index of the annotation to be removed.
	// Return:    void
	// Remarks:   Removes an annotation.
	// Notes:
	//************************************
	static void	Remove(FPD_AnnotList annotList, FS_INT32 index);

	//************************************
	// Function:  RemoveTemp
	// Param[in]: annotList		The input annotation list.
	// Param[in]: index			The zero-based index of the annotation to be removed.
	// Return:    void
	// Remarks:   Removes from list only, not from page.
	// Notes:
	//************************************
	static void	RemoveTemp(FPD_AnnotList annotList, FS_INT32 index);

	//************************************
	// Function:  MoveToFirst
	// Param[in]: annotList		The input annotation list.
	// Param[in]: index			The zero-based index of the annotation to be moved.
	// Return:    void
	// Remarks:   Moves an annotation to the first of the annotation list.
	// Notes:
	//************************************
	static void	MoveToFirst(FPD_AnnotList annotList, FS_INT32 index);

	//************************************
	// Function:  MoveToLast
	// Param[in]: annotList		The input annotation list.
	// Param[in]: index			The zero-based index of the annotation to be moved.
	// Return:    void
	// Remarks:   Moves an annotation to the last of the annotation list.
	// Notes:
	//************************************
	static void	MoveToLast(FPD_AnnotList annotList, FS_INT32 index);

	//************************************
	// Function:  MoveTo
	// Param[in]: annotList		The input annotation list.
	// Param[in]: annot			The annotation to be moved.
	// Param[in]: iNewIndex		The new position to move to.
	// Return:    void
	// Remarks:   Moves an annotation to specified position.
	// Notes:
	//************************************
	static void	MoveTo(FPD_AnnotList annotList, FPD_Annot annot, FS_INT32 iNewIndex);

	//************************************
	// Function:  GetDocument
	// Param[in]: annotList		The input annotation list.
	// Return:    The PDF document.
	// Remarks:   Gets the PDF document.
	// Notes:
	//************************************
	static FPD_Document	GetDocument(FPD_AnnotList annotList);

	//************************************
	// Function:  SetFixedIconParams
	// Param[in]: annotList			The input annotation list.
	// Param[in]: nFixedIconType	Indicates how to display fixed annotations.
	// 0 is to show as normal annotations, ignore NoZoom or NoRotate flag,
	// and 1 is to show as fixed scaling size, <param>sx</param> and <param>sy</param> specifies scaling fractors in x and y axes,
	// and 2 is to show as fixed device size which is specified by <param>sx</param> and <param>sy</param> in device units.
	// Param[in]: sx				Fixed scaling size or fixed device size according to the value of <param>nFixedIconType</param> in x axis.
	// Param[in]: sy				Fixed scaling size or fixed device size according to the value of <param>nFixedIconType</param> in y axis.
	// Param[in]: nRotate			Page rotation, valid values are 0, 1, 2, 3, same as <a>FPDPageGetDisplayMatrix</a>.
	// Return:    The PDF document.
	// Remarks:   Enable to show icon annotations as fixed size (NoZoom or NoRotate flag).
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1.0.0
	//************************************
	static void SetFixedIconParams(FPD_AnnotList annotList, FS_INT32 nFixedIconType, FS_FLOAT sx, FS_FLOAT sy, FS_INT32 nRotate);
	
	//************************************
	// Function:  GetAnnotMatrix
	// Param[in]: annotList			The input annotation list.
	// Param[in]: pAnnotDict		Annotation's dictionary object.
	// Param[in]: pUser2Device		Current displaying transformation matrix.
	// Param[out]: outMatrix		Annotation matrix to display.
	// Return:    void.
	// Remarks:   Gets annotation transformation matrix from page space to device space.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1.0.0
	//************************************
	static void GetAnnotMatrix(FPD_AnnotList annotList, FPD_Object pAnnotDict, FS_AffineMatrix pUser2Device, FS_AffineMatrix* outMatrix);
	
	//************************************
	// Function:  GetAnnotRect
	// Param[in]: annotList			The input annotation list.
	// Param[in]: pAnnotDict		Annotation's dictionary object.
	// Param[in]: pUser2Device		Transformation matrix from user space to device space.
	// Param[out]: outAnnotRC		Annotation rectangle in device coordinates.
	// Return:    void.
	// Remarks:   Gets annotation rectangle in device coordinates.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1.0.0
	//************************************
	static void GetAnnotRect(FPD_AnnotList annotList, FPD_Object pAnnotDict, FS_AffineMatrix pUser2Device, FS_FloatRect* outAnnotRC);
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Interactive forms
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////
/*							 CFPD_DefaultAppearance_V1								*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
class CFPD_DefaultAppearance_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: szAppearance		The input default appearance string.
	// Return:    Default appearance interpreter, serves for DA entry in form field dictionary.
	// Remarks:   Creates from a default appearance string that containing a sequence of valid 
    //            page-content graphics or text state operators that define such properties as the field's 
	//            text size and color.
	// Notes:
	//************************************
	static FPD_DefaultAppearance New(FS_LPCSTR szAppearance);

	//************************************
	// Function:  Destroy
	// Param[in]: defAppearance		The input default appearance interpreter.
	// Return:    void
	// Remarks:   Destroys default appearance interpreter.
	// Notes:
	//************************************
	static void	Destroy(FPD_DefaultAppearance defAppearance);

	
	//************************************
	// Function:  HasFont
	// Param[in]: defAppearance		The input default appearance interpreter.
	// Return:    <a>TRUE</a> for a font having been used in the appearance, otherwise not.
	// Remarks:   Checks whether a font has been used in the appearance. 
	// Notes:
	//************************************
	static FS_BOOL HasFont(FPD_DefaultAppearance defAppearance);

	//************************************
	// Function:  GetFontString
	// Param[in]: defAppearance		The input default appearance interpreter.
	// Param[out]:outFontString		It receives the font instruction in the appearance string.
	// Return:    void
	// Remarks:   Gets the font instruction in the appearance string.
	// Notes:
	//************************************
	static void	GetFontString(FPD_DefaultAppearance defAppearance, FS_ByteString* outFontString);

	//************************************
	// Function:  GetFont
	// Param[in]: defAppearance		The input default appearance interpreter.
	// Param[out]:outFontNameTag	It receives font name tag of font.
	// Param[out]:outFontSize		It receives the font size of the font.
	// Return:    void
	// Remarks:   Gets the font. 
	// Notes:
	//************************************
	static void	GetFont(FPD_DefaultAppearance defAppearance, FS_ByteString* outFontNameTag, FS_FLOAT* outFontSize);

	//************************************
	// Function:  SetFont
	// Param[in]: defAppearance		The input default appearance interpreter.
	// Param[in]: szFontNameTag		The font name tag of the font.
	// Param[in]: fFontSize			The size of the font.
	// Return:    void
	// Remarks:   Sets the font name tag and size of the font.
	// Notes:
	//************************************
	static void	SetFont(FPD_DefaultAppearance defAppearance, FS_LPSTR szFontNameTag, FS_FLOAT fFontSize);


	//************************************
	// Function:  HasColor
	// Param[in]: defAppearance			The input default appearance interpreter.
	// Param[in]: bStrokingOperation	Whether the stroking color to be checked.
	// Return:    Non-zero means having one, otherwise having none.
	// Remarks:   Checks whether a color has been used in the appearance.
	// Notes:
	//************************************
	static FS_BOOL HasColor(FPD_DefaultAppearance defAppearance, FS_BOOL bStrokingOperation);

	//************************************
	// Function:  GetColorString
	// Param[in]: defAppearance			The input default appearance interpreter.
	// Param[in]: bStrokingOperation	Whether the stroking color instruction to be fetched.
	// Param[out]:outColorString		The color instruction in the appearance string.
	// Return:    void
	// Remarks:   Gets the color instruction in the appearance string.
	// Notes:
	//************************************
	static void	GetColorString(
		FPD_DefaultAppearance defAppearance, 
		FS_BOOL bStrokingOperation, 
		FS_ByteString* outColorString
		);

	//************************************
	// Function:  GetColorInclueCMYK
	// Param[in]: defAppearance			The input default appearance interpreter.
	// Param[out]:pIclrType				It receives the color type.
	// Param[out]:fc[4]					It receives the color values of components.
	// Param[in]: bStrokingOperation	Whether the stroking color to be fetched.
	// Return:    void
	// Remarks:   Gets the color (including CMYK).
	// Notes:
	//************************************
	static void	GetColorInclueCMYK(
		FPD_DefaultAppearance defAppearance, 
		FS_INT32* pIclrType, 
		FS_FLOAT fc[4], 
		FS_BOOL bStrokingOperation
		);

	//************************************
	// Function:  GetColor
	// Param[in]: defAppearance			The input default appearance interpreter.
	// Param[out]:pClr					It receives the color.
	// Param[out]:pIclrType				It receives the color type.
	// Param[in]: bStrokingOperation	Whether the stroking color to be fetched.
	// Return:    void
	// Remarks:   Gets the color (not including CMYK).
	// Notes:
	//************************************
	static void	GetColor(
		FPD_DefaultAppearance defAppearance, 
		FS_ARGB* pClr, 
		FS_INT32* pIclrType, 
		FS_BOOL bStrokingOperation
		);

	//************************************
	// Function:  SetColor
	// Param[in]: defAppearance			The input default appearance interpreter.
	// Param[in]: clr					The input color.
	// Param[in]: iClrType				The input color type.
	// Param[in]: bStrokingOperation	Whether the stroking color to be set.
	// Return:    void
	// Remarks:   Sets the color.
	// Notes:
	//************************************
	static void	SetColor(
		FPD_DefaultAppearance defAppearance, 
		FS_ARGB clr, 
		FS_INT32 iClrType, 
		FS_BOOL bStrokingOperation
		);



	//************************************
	// Function:  HasTextMatrix
	// Param[in]: defAppearance		The input default appearance interpreter.
	// Return:    <a>TRUE</a> for a text matrix having been used in the appearance, otherwise not.
	// Remarks:   Checks whether a text matrix has been used in the appearance.
	// Notes:
	//************************************
	static FS_BOOL HasTextMatrix(FPD_DefaultAppearance defAppearance);

	//************************************
	// Function:  GetTextMatrixString
	// Param[in]: defAppearance			The input default appearance interpreter.
	// Param[out]:outMatrixString		It receives the text matrix instruction in the appearance string.
	// Return:    void
	// Remarks:   Gets the text matrix instruction in the appearance string.
	// Notes:
	//************************************
	static void	GetTextMatrixString(FPD_DefaultAppearance defAppearance, FS_ByteString* outMatrixString);

	//************************************
	// Function:  GetTextMatrix
	// Param[in]: defAppearance		The input default appearance interpreter.
	// Return:    The text matrix.
	// Remarks:   Gets the text matrix.
	// Notes:
	//************************************
	static FS_AffineMatrix GetTextMatrix(FPD_DefaultAppearance defAppearance);

	//************************************
	// Function:  SetTextMatrix
	// Param[in]: defAppearance		The input default appearance interpreter.
	// Param[in]: matrix			The input text matrix.
	// Return:    void
	// Remarks:   Sets the text matrix.
	// Notes:
	//************************************
	static void	SetTextMatrix(FPD_DefaultAppearance defAppearance, FS_AffineMatrix matrix);
};

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*									 CFPD_InterForm_V1								*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
class CFPD_CustomerFormNotifyHandler : public CPDF_FormNotify
{
public:
	CFPD_CustomerFormNotifyHandler(const FPD_FormNotifyCallbacksRec* pNotify);
	virtual FS_INT32		BeforeValueChange(CPDF_FormField* pField, CFX_WideString& csValue);
	virtual FS_INT32		AfterValueChange(CPDF_FormField* pField);
	virtual FS_INT32		BeforeSelectionChange( CPDF_FormField* pField, CFX_WideString& csValue);
	virtual FS_INT32		AfterSelectionChange( CPDF_FormField* pField);
	virtual FS_INT32		AfterCheckedStatusChange( CPDF_FormField* pField, CFX_ByteArray& statusArray);
	virtual FS_INT32		BeforeFormReset( CPDF_InterForm* pForm);
	virtual FS_INT32		AfterFormReset( CPDF_InterForm* pForm);
	virtual FS_INT32		BeforeFormImportData( CPDF_InterForm* pForm);
	virtual FS_INT32		AfterFormImportData( CPDF_InterForm* pForm);

//protected:
	FPD_FormNotifyCallbacksRec m_Notify;
	
};

class CFPD_FormNotify_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: callbacks		The callback set.
	// Return:    A newly form notify object.
	// Remarks:   Creates a form notify object.
	// See: FPDInterFormGetFormNotify
	// See: FPDInterFormSetFormNotify
	// See: FPDFormNotifyDestroy
	// Notes:
	//************************************
	static FPD_FormNotify New(FPD_FormNotifyCallbacks callbacks);

	//************************************
	// Function:  Destroy
	// Param[in]: formNotify	The <a>FPD_FormNotify</a> object to be destroied.
	// Return:    void
	// Remarks:   Destroys a form notify object.
	// Notes:
	//************************************
	static void Destroy(FPD_FormNotify formNotify);
};

class CFPD_InterForm_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: doc			The PDF document.
	// Param[in]: bUpdateAP		Whether we need to regenerate appearance streams for fields.
	// Return:    A PDF interactive form.
	// Remarks:   Creates a form from a document. The bUpdateAP param specifies whether we need to 
	//            regenerate appearance streams for the fields.
	// Notes:
	//************************************
	static FPD_InterForm New(FPD_Document doc, FS_BOOL bUpdateAP);

	//************************************
	// Function:  Destroy
	// Param[in]: form		The input PDF interactive form.
	// Return:    void
	// Remarks:   Destroys a PDF interactive form.
	// Notes:
	//************************************
	static void	Destroy(FPD_InterForm form);


	//************************************
	// Function:  EnableUpdateAP
	// Param[in]: bUpdateAP		the input auto-update-appearance flag.
	// Return:    void
	// Remarks:   Change the auto-update-appearance flag.
	// Notes:
	//************************************
	static void	EnableUpdateAP(FS_BOOL bUpdateAP);

	//************************************
	// Function:  UpdatingAPEnabled
	// Param[in]: void
	// Return:    <a>TRUE</a> for needing to update appearance automatically.
	// Remarks:   Checks whether need to update appearance automatically.
	// Notes:
	//************************************
	static FS_BOOL UpdatingAPEnabled(void);

	//************************************
	// Function:  GenerateNewResourceName
	// Param[in]: resDict		An existing resource dictionary.
	// Param[in]: csType		Specifies resource type to generate a new name string, currently,
	//							it can be ExtGState, ColorSpace, Font, any other is no sense.
	// Param[in]: iMinLen		The minimum length of the resource name.
	// Param[in]: csPrefix		A prefix string to add in the head of he new resource name.
	// Param[out]:outName		A resource tag name.
	// Return:    void
	// Remarks:   Generate a new resource tag name, especially for ExtGState, ColorSpace, Font, etc.
	// Notes:
	//************************************
	static void	GenerateNewResourceName(const FPD_Object resDict, FS_LPCSTR csType, FS_INT32 iMinLen, FS_LPSTR csPrefix, FS_ByteString* outName);


	//************************************
	// Function:  AddSystemDefaultFont
	// Param[in]: doc		The PDF document.
	// Return:    A PDF font.
	// Remarks:   Adds the system default font.
	// Notes:
	//************************************
	static FPD_Font	AddSystemDefaultFont(const FPD_Document doc);

	//************************************
	// Function:  AddSystemFont
	// Param[in]: doc			The PDF document.
	// Param[in]: szFontName	The input font name.
	// Param[in]: iCharSet		The input character set.
	// Return:    A PDF font.
	// Remarks:   Adds a system font with font name and character set. ANSI version.
	// Notes:
	//************************************
	static FPD_Font	AddSystemFont(const FPD_Document doc, FS_LPSTR szFontName, FS_BYTE iCharSet);

	//************************************
	// Function:  AddSystemFontW
	// Param[in]: doc			The PDF document.
	// Param[in]: wszFontName	he input font name.
	// Param[in]: iCharSet		The input character set.
	// Return:    A PDF font.
	// Remarks:   Adds a system font with font name and character set. Unicode version.
	// Notes:
	//************************************
	static FPD_Font	AddSystemFontW(const FPD_Document doc, FS_LPWSTR wszFontName, FS_BYTE iCharSet);

	//************************************
	// Function:  AddStandardFont
	// Param[in]: doc			The PDF document.
	// Param[in]: szFontName	The input font name.
	// Return:    A PDF font.
	// Remarks:   Adds a standard font with font name.
	// Notes:
	//************************************
	static FPD_Font	AddStandardFont(const FPD_Document doc, FS_LPSTR szFontName);

	//************************************
	// Function:  GetNativeFont
	// Param[in]: iCharSet		The input character set.
	// Param[out]:pLogFont		It receives the font information. Points to LOGFONTA structure.
	// Param[out]:outFont		The font name.
	// Return:    void
	// Remarks:   Gets the native font name with a character set and a logical font structure.
	// Notes:
	//************************************
	static void	GetNativeFont(FS_BYTE iCharSet, FS_LPVOID pLogFont, FS_ByteString* outFont);

	//************************************
	// Function:  GetNativeFont2
	// Param[out]:pLogFont		It receives the font information. Points to LOGFONTA structure.
	// Param[out]:outFont		The font name.
	// Return:    void
	// Remarks:   Gets the native font name with a logical font structure.
	// Notes:
	//************************************
	static void	GetNativeFont2(FS_LPVOID pLogFont, FS_ByteString* outFont);

	//************************************
	// Function:  AddNativeFont
	// Param[in]: iCharSet		The input character set.
	// Param[in]: doc			The PDF document.
	// Return:    A PDF font.
	// Remarks:   Adds a native font with a character set.
	// Notes:
	//************************************
	static FPD_Font	AddNativeFont(FS_BYTE iCharSet, const FPD_Document doc);

	//************************************
	// Function:  AddNativeFont2
	// Param[in]: doc	The PDF document.
	// Return:    A PDF font.
	// Remarks:   Adds the native font of the PDF document.
	// Notes:
	//************************************
	static FPD_Font	AddNativeFont2(const FPD_Document doc);


	//************************************
	// Function:  ValidateFieldName
	// Param[in]: form				The input PDF interactive form.
	// Param[in,out]:inOutNewFieldName	Input a field name and receives the valid field name.
	// Param[in]: iType				The field type. uses FPD_FORM_FIELDTYPE_X macros.
	// Return:    Non-zero means the input field name is valid, otherwise invalid.
	// Remarks:   Checks the field name of specified type is valid or not, receives the valid field name when it's invalid.
	// Notes:
	//************************************
	static FS_BOOL ValidateFieldName(FPD_InterForm form, FS_WideString* inOutNewFieldName, FS_INT32 iType);

	//************************************
	// Function:  ValidateFieldName2
	// Param[in]: form				The input PDF interactive form.
	// Param[in]: field				The input field.
	// Param[in,out]:inOutNewFieldName	Input a field name and receives the valid field name.
	// Return:    Non-zero means the input field name is valid, otherwise invalid.
	// Remarks:   Checks the field name for the input field is valid or not, receives the valid field name when it's invalid.
	// Notes:
	//************************************
	static FS_BOOL ValidateFieldName2(FPD_InterForm form, const FPD_FormField field, FS_WideString* inOutNewFieldName);

	//************************************
	// Function:  ValidateFieldName3
	// Param[in]: form				The input PDF interactive form.
	// Param[in]: control			The input control.
	// Param[in,out]:inOutNewFieldName	Input a field name and receives the valid field name.
	// Return:    Non-zero means the input field name is valid, otherwise invalid.
	// Remarks:   Checks the field name for the input control is valid or not, receives the valid field name when it's invalid.
	// Notes:
	//************************************
	static FS_BOOL ValidateFieldName3(FPD_InterForm form, const FPD_FormControl control, FS_WideString* inOutNewFieldName);

	//************************************
	// Function:  NewField
	// Param[in]: form					The input PDF interactive form.
	// Param[in,out]:inOutFieldName		Input a field name and receives the valid field full name.
	// Param[in]: iType					The input field type.
	// Return:    A form field.
	// Remarks:   Creates and add a field in form, param iType uses FPD_FORM_FIELDTYPE_X macros.
	//            inOutFieldName is in/out param. If succeed, csFieldName is the validated field full name.
	// Notes:
	//************************************
	static FPD_FormField NewField(FPD_InterForm form, FS_WideString* inOutFieldName, FS_INT32 iType);

	//************************************
	// Function:  NewControl
	// Param[in]: form					The input PDF interactive form.
	// Param[in,out]:inOutFieldName		Input a field name and receives the valid field full name.
	// Param[in]: iType					The input field type.
	// Return:    A form control.
	// Remarks:   Creates and add a control in form, param iType uses FPD_FORM_FIELDTYPE_X macros.
	//            inOutFieldName is in/out param. If succeed, inOutFieldName is the validated field full name.
	// Notes:     This Function will call NewField if necessary.
	//            Use this Function to create a control and use returned object to initialize Widget.
	//************************************
	static FPD_FormControl NewControl(FPD_InterForm form, FS_WideString* inOutFieldName, FS_INT32 iType);

	
	//************************************
	// Function:  CountFields
	// Param[in]: form				The input PDF interactive form.
	// Param[in]: wszFieldName		The input field name to be matched.
	//  -empty means for all fields; or system will match the field name.
	//  -as shortest string, for example, text1 will match text1.0, text1.2.0, etc. but text1 will not match test10 or test11.1..
	// Return:    The count of fields with specified field name to be matched. 
	// Remarks:   Gets the count of specified fields.
	// Notes:
	//************************************
	static FS_DWORD	CountFields(FPD_InterForm form, FS_LPWSTR wszFieldName);

	//************************************
	// Function:  GetField
	// Param[in]: form				The input PDF interactive form.
	// Param[in]: index				The zero-based field index in the matched field array.
	// Param[in]: wszFieldName		The field name to be matched.
	//  -empty means for all fields; or system will match the field name.
	//  -as shortest string, for example, text1 will match text1.0, text1.2.0, etc. but text1 will not match test10 or test11.1..
	// Return:    A form field.
	// Remarks:   Gets a form field.
	// Notes:
	//************************************
	static FPD_FormField GetField(FPD_InterForm form, FS_DWORD index, FS_LPWSTR wszFieldName);

	//************************************
	// Function:  GetAllFieldNames
	// Param[in]: form					The input PDF interactive form.
	// Param[out]:outAllFieldNames		It receives all field names in the form.
	// Return:    void
	// Remarks:   Gets all field names in the form.
	// Notes:
	//************************************
	static void	GetAllFieldNames(FPD_InterForm form, FS_WideStringArray* outAllFieldNames);

	//************************************
	// Function:  IsValidFormField
	// Param[in]: form		The input PDF interactive form.
	// Param[in]: pField	The input pointer.
	// Return:    Non-zero means it's a form field pointer, otherwise is not.
	// Remarks:   Checks whether a pointer is a form field pointer.
	// Notes:
	//************************************
	static FS_BOOL IsValidFormField(FPD_InterForm form, const void* pField);

	//************************************
	// Function:  GetFieldByDict
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: fieldDict		The input form field dictionary.
	// Return:    A form field.
	// Remarks:   Retrieves the field by field dictionary.
	// Notes:
	//************************************
	static FPD_FormField GetFieldByDict(FPD_InterForm form, FPD_Object fieldDict);


	//************************************
	// Function:  CountControls
	// Param[in]: form				The input PDF interactive form.
	// Param[in]: wszFieldName		The input field name to be matched.
	//  -empty means for all fields; or system will match the field name.
	//  -as shortest string, for example, text1 will match text1.0, text1.2.0, etc. but text1 will not match test10 or test11.1..
	// Return:    The count of controls with specified field name to be matched. 
	// Remarks:   Gets the count of specified controls.
	// Notes:
	//************************************
	static FS_DWORD	CountControls(FPD_InterForm form, FS_LPWSTR wszFieldName);

	//************************************
	// Function:  GetControl
	// Param[in]: form				The input PDF interactive form.
	// Param[in]: index				The zero-based control index in the matched control array.
	// Param[in]: wszFieldName		The field name to be matched.
	//  -empty means for all fields; or system will match the field name.
	//  -as shortest string, for example, text1 will match text1.0, text1.2.0, etc. but text1 will not match test10 or test11.1..
	// Return:    A form control.
	// Remarks:   Gets a form control.
	// Notes:
	//************************************
	static FPD_FormControl GetControl(FPD_InterForm form, FS_DWORD index, FS_LPWSTR wszFieldName);

	//************************************
	// Function:  IsValidFormControl
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: pControl		The input pointer.
	// Return:    Non-zero means it's a form control pointer, otherwise is not.
	// Remarks:   Checks whether a pointer is a form control pointer.
	// Notes:
	//************************************
	static FS_BOOL IsValidFormControl(FPD_InterForm form, const void* pControl);

	//************************************
	// Function:  CountPageControls
	// Param[in]: form		The input PDF interactive form.
	// Param[in]: page		The input PDF page.
	// Return:    The count of controls in specified page. 
	// Remarks:   Gets the count of controls in specified page.
	// Notes:
	//************************************
	static FS_INT32	CountPageControls(FPD_InterForm form, FPD_Page page);

	//************************************
	// Function:  GetPageControl
	// Param[in]: form		The input PDF interactive form.
	// Param[in]: page		The input PDF page.
	// Param[in]: index		The zero-based control index in the page.
	// Return:    A control int the page.
	// Remarks:   Gets a control in specified page.
	// Notes:
	//************************************
	static FPD_FormControl GetPageControl(FPD_InterForm form, FPD_Page page, FS_INT32 index);

	//************************************
	// Function:  GetControlAtPoint
	// Param[in]: form		The input PDF interactive form.
	// Param[in]: page		The input PDF page.
	// Param[in]: pdfX		The x-coordinate of mouse position in user space.
	// Param[in]: pdfY		The y-coordinate of mouse position in user space.
	// Return:    A control.
	// Remarks:   Retrieves the control by mouse position.
	// Notes:
	//************************************
	static FPD_FormControl GetControlAtPoint(FPD_InterForm form, FPD_Page page, FS_FLOAT pdfX, FS_FLOAT pdfY);

	//************************************
	// Function:  GetControlByDict
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: widgetDict	The input widget dictionary.
	// Return:    A control.
	// Remarks:   Retrieves the control by widget dictionary.
	// Notes:
	//************************************
	static FPD_FormControl GetControlByDict(FPD_InterForm form, FPD_Object widgetDict);


	//************************************
	// Function:  RenameField
	// Param[in]: form					The input PDF interactive form.
	// Param[in]: wszOldName			The field's old name.
	// Param[in,out]:inOutNewName		Input the field's new name and receive the validated field full name, if success.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Rename a field's full name. The field identified by it's old full name.
	// Notes:
	//************************************
	static FS_BOOL RenameField(FPD_InterForm form, FS_LPWSTR wszOldName, FS_WideString* inOutNewName);

	//************************************
	// Function:  RenameField2
	// Param[in]: form					The input PDF interactive form.
	// Param[in]: field					The input field.
	// Param[in,out]:inOutNewName		Input the field's new name and receive the validated field full name.if success.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Rename a field's full name. 
	// Notes:
	//************************************
	static FS_BOOL RenameField2(FPD_InterForm form, FPD_FormField field, FS_WideString* inOutNewName);

	//************************************
	// Function:  RenameControl
	// Param[in]: form					The input PDF interactive form.
	// Param[in]: pControl				The input control.
	// Param[in,out]:inOutNewName		Input the control's new name and receive the validated control full name.if success.
	// Return:    <a>TRUE</a> for success, otherwise not.
	// Remarks:   Rename a control's full name. 
	// Notes:
	//************************************
	static FS_BOOL RenameControl(FPD_InterForm form, FPD_FormControl pControl, FS_WideString* inOutNewName);

	//************************************
	// Function:  DeleteField
	// Param[in]: form				The input PDF interactive form.
	// Param[in]: wszFieldName		The input field name.
	// Return:    void
	// Remarks:   Deletes a field with specified field name.
	//            If csName is empty, means for all fields.
	// Notes:     When delete fields, all controls assigned to them will be deleted too.
	//************************************
	static void	DeleteField(FPD_InterForm form, FS_LPWSTR wszFieldName);

	//************************************
	// Function:  DeleteField2
	// Param[in]: form		The input PDF interactive form.
	// Param[in]: field		The input field.
	// Return:    void
	// Remarks:   Deletes a field.
	//            Field object will be deleted if success.
	// Notes:     When delete fields, all controls assigned to them will be deleted too.
	//************************************
	static void	DeleteField2(FPD_InterForm form, FPD_FormField field);

	//************************************
	// Function:  DeleteControl
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: control		The input control.
	// Return:    void
	// Remarks:   Deletes a control.
	//            control object will be deleted if success.
	// Notes:     When delete controls, corresponding fields will be deleted only when they have no any control associated
	//  with them. If csFieldName is empty, it means to all fields or controls.
	//************************************
	static void	DeleteControl(FPD_InterForm form, FPD_FormControl control);


	//************************************
	// Function:  CountInternalFields
	// Param[in]: form				The input PDF interactive form.
	// Param[in]: wszFieldName		The input field name to be matched.
	//  -If empty, this will retrieve all internal field dictionaries from the root list of the whole form.
	//  -If has value, all internal fields which names match the csfieldName will return.
	// Return:    The count of internal fields with specified field name to be matched. 
	// Remarks:   Gets the count of specified internal fields.
	// Notes:
	//************************************
	static FS_DWORD	CountInternalFields(FPD_InterForm form, FS_LPWSTR wszFieldName);

	//************************************
	// Function:  GetInternalField
	// Param[in]: form				The input PDF interactive form.
	// Param[in]: index				The zero-based internal field index in the matched internal field array.
	// Param[in]: wszFieldName		The field name to be matched.
	//  -If empty, this will retrieve all internal field dictionaries from the root list of the whole form.
	//  -If has value, all internal fields which names match the csfieldName will return.
	// Return:	  The internal field dictionary.
	// Remarks:   Gets an internal field.
	// Notes:
	//************************************
	static FPD_Object GetInternalField(FPD_InterForm form, FS_DWORD index, FS_LPWSTR wszFieldName);


	//************************************
	// Function:  GetDocument
	// Param[in]: form		The input PDF interactive form.
	// Return:    The PDF document.
	// Remarks:   Gets the PDF document.
	// Notes:
	//************************************
	static FPD_Document	GetDocument(FPD_InterForm form);

	//************************************
	// Function:  GetFormDict
	// Param[in]: form		The input PDF interactive form.
	// Return:    The AcroForm dictionary.
	// Remarks:   Gets the AcroForm dictionary.
	// Notes:
	//************************************
	static FPD_Object GetFormDict(FPD_InterForm form);


	//************************************
	// Function:  NeedConstructAP
	// Param[in]: form		The input PDF interactive form.
	// Return:    <a>TRUE</a> for constructing appearance streams and appearance dictionaries, otherwise not.
	// Remarks:   Checks whether to construct appearance streams and appearance dictionaries for all widget annotations in the document.
	// Notes:
	//************************************
	static FS_BOOL NeedConstructAP(FPD_InterForm form);

	//************************************
	// Function:  NeedConstructAP2
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: bNeedAP		The input construct flag.
	// Return:    void
	// Remarks:   Sets the construct flag.
	// Notes:
	//************************************
	static void	NeedConstructAP2(FPD_InterForm form, FS_BOOL bNeedAP);


	//************************************
	// Function:  CountFieldsInCalculationOrder
	// Param[in]: form		The input PDF interactive form.
	// Return:    The count of all fields in CO array.
	// Remarks:   The count of all fields in CO array.
	// Notes:
	//************************************
	static FS_INT32	CountFieldsInCalculationOrder(FPD_InterForm form);

	//************************************
	// Function:  GetFieldInCalculationOrder
	// Param[in]: form		The input PDF interactive form.
	// Param[in]: index		The zero-based field index in the CO array.
	// Return:    A form field.
	// Remarks:   Gets a form field from the calculation array.
	// Notes:
	//************************************
	static FPD_FormField GetFieldInCalculationOrder(FPD_InterForm form, FS_INT32 index);

	//************************************
	// Function:  FindFieldInCalculationOrder
	// Param[in]: form		The input PDF interactive form.
	// Param[in]: field		The input form field.
	// Return:    The zero-based index of the specified field in the CO array. -1 means not found.
	// Remarks:   Finds a form field in the CO array.
	// Notes:
	//************************************
	static FS_INT32	FindFieldInCalculationOrder(FPD_InterForm form, const FPD_FormField field);

	//************************************
	// Function:  InsertFieldInCalculationOrder
	// Param[in]: form		The input PDF interactive form.
	// Param[in]: field		The input form field.
	// Param[in]: index		The zero-based field index in the CO array to insert at.
	// Return:    The zero-based index of the inserted field. -1 means failed.
	// Remarks:   Inserts a form field to the CO array.
	// Notes:
	//************************************
	static FS_INT32	InsertFieldInCalculationOrder(FPD_InterForm form, const FPD_FormField field, FS_INT32 index );

	//************************************
	// Function:  MoveFieldInCalculationOrder
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: field			The input form field.
	// Param[in]: iNewIndex		The new zero-based field index in the CO array.
	// Return:    The current index of specified field. -1 means failed.
	// Remarks:   Moves a form field in the CO array to another position.
	// Notes:
	//************************************
	static FS_INT32	MoveFieldInCalculationOrder(FPD_InterForm form, const FPD_FormField field, FS_INT32 iNewIndex);

	//************************************
	// Function:  RemoveFieldInCalculationOrder
	// Param[in]: form		The input PDF interactive form.
	// Param[in]: field		The input form field to be removed.
	// Return:    void
	// Remarks:   Removes a field in the CO array.
	// Notes:
	//************************************
	static void	RemoveFieldInCalculationOrder(FPD_InterForm form, const FPD_FormField field);


	//************************************
	// Function:  CountFormFonts
	// Param[in]: form		The input PDF interactive form.
	// Return:    The count of font resources.
	// Remarks:   Gets the count of font resources.
	// Notes:
	//************************************
	static FS_DWORD	CountFormFonts(FPD_InterForm form);

	//************************************
	// Function:  GetFormFont
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: index			The zero-based font index in the font resources.
	// Param[out]:outNameTag	It receives the name tag of the font, such as /Helv (get rid of slash).
	// Return:    A PDF font.
	// Remarks:   Gets a PDF font from the font resources.
	// Notes:
	//************************************
	static FPD_Font	GetFormFont(FPD_InterForm form, FS_DWORD index, FS_ByteString* outNameTag);

	//************************************
	// Function:  GetFormFont2
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: szNameTag		the name tag of the font, such as /Helv (get rid of slash).
	// Return:    A PDF font.
	// Remarks:   Gets a PDF font with specified name tag.
	// Notes:
	//************************************
	static FPD_Font	GetFormFont2(FPD_InterForm form, FS_LPSTR szNameTag);

	//************************************
	// Function:  GetFormFont3
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: szFontName	The input font name.
	// Param[out]:outNameTag	It receives the name tag of the font, such as /Helv (get rid of slash).
	// Return:    A PDF font.
	// Remarks:   Finds a PDF font with specified font name.
	// Notes:
	//************************************
	static FPD_Font	GetFormFont3(FPD_InterForm form, FS_LPSTR szFontName, FS_ByteString* outNameTag);

	//************************************
	// Function:  GetNativeFormFont
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: iCharSet		The input character set.
	// Param[out]:outNameTag	It receives the name tag of the font, such as /Helv (get rid of slash).
	// Return:    A PDF font.
	// Remarks:   Gets a native-form font with a character set.
	// Notes:
	//************************************
	static FPD_Font	GetNativeFormFont(FPD_InterForm form, FS_BYTE iCharSet, FS_ByteString* outNameTag);

	//************************************
	// Function:  GetNativeFormFont2
	// Param[in]: form			The input PDF interactive form.
	// Param[out]:outNameTag	It receives the name tag of the font, such as /Helv (get rid of slash).
	// Return:    A PDF font.
	// Remarks:   Gets the native-form font.
	// Notes:
	//************************************
	static FPD_Font	GetNativeFormFont2(FPD_InterForm form, FS_ByteString* outNameTag);

	//************************************
	// Function:  FindFormFont
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: font			The input PDF font.
	// Param[out]:outNameTag	It receives the name tag of the font, such as /Helv (get rid of slash).
	// Return:    Non-zero means found, otherwise not found.
	// Remarks:   Finds a font with font pointer.
	// Notes:
	//************************************
	static FS_BOOL FindFormFont(FPD_InterForm form, const FPD_Font font, FS_ByteString* outNameTag);

	//************************************
	// Function:  FindFormFont2
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: szFontName	The input font name.
	// Param[out]:outFont		It receives the found PDF font.
	// Param[out]:outNameTag	It receives the name tag of the font, such as /Helv (get rid of slash).
	// Return:    Non-zero means found, otherwise not found.
	// Remarks:   Finds a font with font name. ANSI version.
	// Notes:
	//************************************
	static FS_BOOL FindFormFont2(FPD_InterForm form, FS_LPSTR szFontName, FPD_Font* outFont, FS_ByteString* outNameTag);

	//************************************
	// Function:  FindFormFont3
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: wszFontName	The input font name.
	// Param[out]:outFont		It receives the found PDF font.
	// Param[out]:outNameTag	It receives the name tag of the font, such as /Helv (get rid of slash).
	// Return:    Non-zero means found, otherwise not found.
	// Remarks:   Finds a font with font name. Unicode version.
	// Notes:
	//************************************
	static FS_BOOL FindFormFont3(FPD_InterForm form, FS_LPWSTR wszFontName, FPD_Font* outFont, FS_ByteString* outNameTag);

	//************************************
	// Function:  AddFormFont
	// Param[in]: form				The input PDF interactive form.
	// Param[in]: font				The input PDF font.
	// Param[in,out]:inOutNameTag	Input a name tag and receive a valid name tag of the font, such as /Helv (get rid of slash).
	// Return:    void
	// Remarks:   Adds a form font with specified name tag.
	// Notes:
	//************************************
	static void	AddFormFont(FPD_InterForm form, const FPD_Font font, FS_ByteString* inOutNameTag);

	//************************************
	// Function:  AddNativeFormFont
	// Param[in]: form				The input PDF interactive form.
	// Param[in]: iCharSet			The input character set.
	// Param[in,out]:inOutNameTag	Input a name tag and receive a valid name tag of the font, such as /Helv (get rid of slash).
	// Return:    A PDF font.
	// Remarks:   Adds a native form font with specified name tag.
	// Notes:
	//************************************
	static FPD_Font	AddNativeFormFont(FPD_InterForm form, FS_BYTE iCharSet, FS_ByteString* inOutNameTag);

	//************************************
	// Function:  AddNativeFormFont2
	// Param[in]: form				The input PDF interactive form.
	// Param[in,out]:inOutNameTag	Input a name tag and receive a valid name tag of the font, such as /Helv (get rid of slash).
	// Return:    A PDF font.
	// Remarks:   Adds the native form font.
	// Notes:
	//************************************
	static FPD_Font	AddNativeFormFont2(FPD_InterForm form, FS_ByteString* inOutNameTag);

	//************************************
	// Function:  RemoveFormFont
	// Param[in]: form		The input PDF interactive form.
	// Param[in]: font		The input PDF font pointer.
	// Return:    void
	// Remarks:   Removes a form font with font pointer.
	// Notes:
	//************************************
	static void	RemoveFormFont(FPD_InterForm form, const FPD_Font font);

	//************************************
	// Function:  RemoveFormFont2
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: szNameTag		The input name tag of the font, such as /Helv (get rid of slash).
	// Return:    void
	// Remarks:   Removes a form font with specified name tag.
	// Notes:
	//************************************
	static void	RemoveFormFont2(FPD_InterForm form, FS_LPCSTR szNameTag);
	

	//************************************
	// Function:  GetDefaultAppearance
	// Param[in]: form			The input PDF interactive form.
	// Param[out]:outDefAP		The default appearance interpreter.
	// Return:    void
	// Remarks:   Gets the default appearance interpreter.
	// Notes:     Do not forget to save default appearance after you change any setting in it when you
	//   retrieve an object of FPD_DefaultAppearance by GetDefaultAppearance method.
	//************************************
	static void	GetDefaultAppearance(FPD_InterForm form, FPD_DefaultAppearance* outDefAP);

	//************************************
	// Function:  SetDefaultAppearance
	// Param[in]: form		The input PDF interactive form.
	// Param[in]: cDA		The input default appearance interpreter.
	// Return:    void
	// Remarks:   Sets the default appearance interpreter.
	// Notes:
	//************************************
	static void	SetDefaultAppearance(FPD_InterForm form, const FPD_DefaultAppearance cDA);

	//************************************
	// Function:  GetDefaultFormFont
	// Param[in]: form		The input PDF interactive form.
	// Return:    The default form font. 
	// Remarks:   Gets the default form font. 
	// Notes:
	//************************************
	static FPD_Font GetDefaultFormFont(FPD_InterForm form);

	//************************************
	// Function:  SetDefaultFormFont
	// Param[in]: form		The input PDF interactive form.
	// Param[in]: font		The input font.
	// Return:    void
	// Remarks:   Sets the default form font.
	// Notes:
	//************************************
	static void	SetDefaultFormFont(FPD_InterForm form, const FPD_Font font);


	//************************************
	// Function:  GetFormAlignment
	// Param[in]: form		The input PDF interactive form.
	// Return:    alignment value: 
	//   <ul>
	//   <li>0 - left alignment, the default setting.</li>
	//   <li>1 - centered.</li>
	//   <li>2 - right alignment.</li>
	//   </ul>
	// Remarks:   Gets the form alignment.
	// Notes:
	//************************************
	static FS_INT32	GetFormAlignment(FPD_InterForm form);

	//************************************
	// Function:  SetFormAlignment
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: iAlignment	The input form alignment type.
	//   <ul>
	//   <li>0 - left alignment, the default setting.</li>
	//   <li>1 - centered,</li>
	//   <li>2 - right alignment.</li>
	//   </ul>
	// Return:    void
	// Remarks:   Sets the form alignment.
	// Notes:
	//************************************
	static void	SetFormAlignment(FPD_InterForm form, FS_INT32 iAlignment);


	//************************************
	// Function:  CheckRequiredFields
	// Param[in]: form					The input PDF interactive form.
	// Param[in]: fields				The input form fields array. fields is the array of FPD_FormField.
	// Param[in]: bIncludeOrExclude		Whether to include or exclude the required fields.
	// Return:    The checked form field.
	// Remarks:   Checks the required fields' checked flag.
	// Notes:
	//************************************
	static FPD_FormField CheckRequiredFields(FPD_InterForm form, const FS_PtrArray fields, FS_BOOL bIncludeOrExclude);

	//************************************
	// Function:  ExportToFDF
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: szPDFPath		The input FDF file full path.
	// Return:    A FDF document.
	// Remarks:   Exports the interactive form to a FDF format file.
	// Notes:
	//************************************
	static FDF_Document ExportToFDF(FPD_InterForm form, FS_LPCWSTR szPDFPath);

	//************************************
	// Function:  ExportToFDF2
	// Param[in]: form					The input PDF interactive form.
	// Param[in]: szPDFPath				The input FDF file full path.
	// Param[in]: fields				The input form fields array. fields is the array of FPD_FormField.
	// Param[in]: bIncludeOrExclude		Whether to include or exclude the form fields.
	// Return:    A FDF document.
	// Remarks:   Exports the interactive form to a FDF format file, include or exclude some fields. 
	// Notes:
	//************************************
	static FDF_Document	ExportToFDF2(FPD_InterForm form, FS_LPCWSTR szPDFPath, FS_PtrArray fields, FS_BOOL bIncludeOrExclude);

	//************************************
	// Function:  ImportFromFDF
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: FDFDoc		The input FDF document.
	// Param[in]: bNotify		Whether do notifying.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Imports a FDF document.
	// Notes:
	//************************************
	static FS_BOOL ImportFromFDF(FPD_InterForm form, const FDF_Document FDFDoc, FS_BOOL bNotify);


	//************************************
	// Function:  ResetForm
	// Param[in]: form					The input PDF interactive form.
	// Param[in]: fields				The input form fields array. Fields is the array of FPD_FormField.
	// Param[in]: bIncludeOrExclude		Whether to include or exclude the form fields.
	// Param[in]: bNotify				Whether to do notifying.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Resets form fields.
	// Notes:
	//************************************
	static FS_BOOL ResetForm(FPD_InterForm form, const FS_PtrArray fields, FS_BOOL bIncludeOrExclude, FS_BOOL bNotify);

	//************************************
	// Function:  ResetForm2
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: bNotify		Whether to do notifying.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Resets all form fields.
	// Notes:
	//************************************
	static FS_BOOL ResetForm2(FPD_InterForm form, FS_BOOL bNotify);

	//************************************
	// Function:  ReloadForm
	// Param[in]: form		The input PDF interactive form.
	// Return:    void
	// Remarks:   Reload the interactive form.
	// Notes:
	//************************************
	static void	ReloadForm(FPD_InterForm form);

	//************************************
	// Function:  GetFormNotify
	// Param[in]: form			The input PDF interactive form.
	// Return:	  A form notify handler.
	// Remarks:   Gets the form notify handler.
	// Notes:
	//************************************
	static FPD_FormNotify GetFormNotify(FPD_InterForm form);

	//************************************
	// Function:  SetFormNotify
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: pNotify		The notify callbacks.
	// Return:	  A previous form notify handler.
	// Remarks:   Sets the form notify handler.
	// Notes:
	//************************************
	static void SetFormNotify(FPD_InterForm form, FPD_FormNotify formNotify);

	//************************************
	// Function:  GetPageWithWidget
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: iCurPage		The current page index.
	// Param[in]: bNext			Whether to retrieve the next or previous page.
	// Return:	  The next or previous page index.
	// Remarks:   Retrieves the next or previous page for the page specified by iCurPage.
	// Notes:
	//************************************
	static FS_INT32	GetPageWithWidget(FPD_InterForm form, FS_INT32 iCurPage, FS_BOOL bNext);


	//************************************
	// Function:  IsUpdated
	// Param[in]: form		The input PDF interactive form.
	// Return:    <a>TRUE</a> for the form being updated.
	// Remarks:   Checks whether the form is updated.
	// Notes:
	//************************************
	static FS_BOOL IsUpdated(FPD_InterForm form);

	//************************************
	// Function:  ClearUpdatedFlag
	// Param[in]: form		The input PDF interactive form.
	// Return:    void
	// Remarks:   Clears the updated flag.
	// Notes:
	//************************************
	static void	ClearUpdatedFlag(FPD_InterForm form);

	//************************************
	// Function:  FixPageFields
	// Param[in]: form		The input PDF interactive form.
	// Param[in]: page		The input page dictionary.
	// Return:    void
	// Remarks:   Supplement some form field from a page
	// Notes:
	//************************************
	static void	FixPageFields(FPD_InterForm form, FPD_Page page);

	//************************************
	// Function:  AddControl
	// Param[in]: form			The input PDF interactive form.
	// Param[in]: fieldDict		The input field dictionary.
	// Param[in]: pWidgetDict	The input widget dictionary.
	// Return:    A form control.
	// Remarks:   Adds a control to the PDF interactive form with a widget.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.2.2
	//************************************
	static FPD_FormControl AddControl(FPD_InterForm form, const FPD_Object fieldDict, const FPD_Object pWidgetDict);
};


//////////////////////////////////////////////////////////////////////////////////////////////////////
/*									 CFPD_FormField_V1;								*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
class CFPD_FormField_V1
{
public:
	
	//************************************
	// Function:  GetFullName
	// Param[in]: formField		The input PDF interactive form field.
	// Param[out]:outName		The full name of the field.
	// Return:    void
	// Remarks:   Gets the full name of the field.
	// Notes:
	//************************************
	static void	GetFullName(FPD_FormField formField, FS_WideString* outName);

	//************************************
	// Function:  GetType
	// Param[in]: formField		The input PDF interactive form field.
	// Return:    The type of the field.
	// Remarks:   Gets the type of the field.
	// Notes:
	//************************************
	static FPD_FormFieldType	GetType(FPD_FormField formField);

	//************************************
	// Function:  GetFlags
	// Param[in]: formField		The input PDF interactive form field.
	// Return:    The field flags.
	// Remarks:   Gets the field flags.
	// Notes:
	//************************************
	static FS_DWORD	GetFlags(FPD_FormField formField);

	//************************************
	// Function:  GetInterForm
	// Param[in]: formField		The input PDF interactive form field.
	// Return:    The interactive form which it belongs to.
	// Remarks:   Gets the interactive form which it belongs to.
	// Notes:
	//************************************
	static FPD_InterForm GetInterForm(FPD_FormField formField);

	//************************************
	// Function:  GetFieldDict
	// Param[in]: formField		The input PDF interactive form field.
	// Return:    The field dictionary.
	// Remarks:   Gets the field dictionary.
	// Notes:
	//************************************
	static FPD_Object GetFieldDict(FPD_FormField formField);

	//************************************
	// Function:  ResetField
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: bNotify		Whether do notifying.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Resets the form field.
	// Notes:
	//************************************
	static FS_BOOL ResetField(FPD_FormField formField, FS_BOOL bNotify);


	//************************************
	// Function:  CountControls
	// Param[in]: formField		The input PDF interactive form field.
	// Return:    The count of controls belongs to this field.
	// Remarks:   Gets the count of controls belongs to this field.
	// Notes:
	//************************************
	static FS_INT32	CountControls(FPD_FormField formField);

	//************************************
	// Function:  GetControl
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: index			The input zero-based control index in the field.
	// Return:    A form control.
	// Remarks:   Gets a control.
	// Notes:
	//************************************
	static FPD_FormControl GetControl(FPD_FormField formField, FS_INT32 index);

	//************************************
	// Function:  GetControlIndex
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: control		The input form control.
	// Return:    The index of the control in the field.
	// Remarks:   Gets the index of a control.
	// Notes:
	//************************************
	static FS_INT32	GetControlIndex(FPD_FormField formField, FPD_FormControl control);


	//************************************
	// Function:  GetFieldType
	// Param[in]: formField		The input PDF interactive form field.
	// Return:    Types like FPD_FORM_FIELDTYPE_xxxx
	// Remarks:   Gets the field type.
	// Notes:
	//************************************
	static FS_INT32	GetFieldType(FPD_FormField formField);


	//************************************
	// Function:  GetAdditionalAction
	// Param[in]: formField		The input PDF interactive form field.
	// Param[out]:outAAction	The additional action of the field.
	// Return:    void
	// Remarks:   Gets the additional action of the field.
	// Notes:
	//************************************
	static void GetAdditionalAction(FPD_FormField formField, FPD_AAction* outAAction);

	//************************************
	// Function:  SetAdditionalAction
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: aa			The input additional action.
	// Return:    void
	// Remarks:   Sets the additional action of the field.
	// Notes:
	//************************************
	static void	SetAdditionalAction(FPD_FormField formField, FPD_AAction aa);


	//************************************
	// Function:  GetAlternateName
	// Param[in]: formField		The input PDF interactive form field.
	// Param[out]:outName		The alternate field name
	// Return:    void
	// Remarks:   Gets the alternate field name to be used in place of the actual
	//            field name wherever the field must be identified in the user interface.
	// Notes:
	//************************************
	static void	GetAlternateName(FPD_FormField formField, FS_WideString* outName);

	//************************************
	// Function:  SetAlternateName
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: szName		The input alternate field name.
	// Return:    void
	// Remarks:   Sets the alternate field name. ANSI version.
	// Notes:
	//************************************
	static void	SetAlternateName(FPD_FormField formField, FS_LPCSTR szName);

	//************************************
	// Function:  SetAlternateNameW
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: wszName		The input alternate field name.
	// Return:    void
	// Remarks:   Sets the alternate field name. Unicode version.
	// Notes:
	//************************************
	static void	SetAlternateNameW(FPD_FormField formField, FS_LPCWSTR wszName);


	//************************************
	// Function:  GetMappingName
	// Param[in]: formField		The input PDF interactive form field.
	// Param[out]:outName		The mapping name
	// Return:    void
	// Remarks:   Gets the mapping name to be used when exporting interactive form field data from the document.
	// Notes:
	//************************************
	static void	GetMappingName(FPD_FormField formField, FS_WideString* outName);

	//************************************
	// Function:  SetMappingName
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: szName		The input mapping name. 
	// Return:    void
	// Remarks:   Sets the mapping name. ANSI version.
	// Notes:
	//************************************
	static void	SetMappingName(FPD_FormField formField, FS_LPCSTR szName);

	//************************************
	// Function:  SetMappingNameW
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: wszName		The input mapping name. 
	// Return:    void
	// Remarks:   Sets the mapping name. Unicode version.
	// Notes:
	//************************************
	static void	SetMappingNameW(FPD_FormField formField, FS_LPCWSTR wszName);


	//************************************
	// Function:  GetFieldFlags
	// Param[in]: formField		The input PDF interactive form field.
	// Return:    The field flags.
	// Remarks:   Gets the field flags.
	// Notes:
	//************************************
	static FS_DWORD	GetFieldFlags(FPD_FormField formField);

	//************************************
	// Function:  SetFieldFlags
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: dwFlags		The input field flags.
	// Return:    void
	// Remarks:   Sets the field flags.
	// Notes:
	//************************************
	static void	SetFieldFlags(FPD_FormField formField, FS_DWORD dwFlags);


	//************************************
	// Function:  GetDefaultStyle
	// Param[in]: formField		The input PDF interactive form field.
	// Param[out]:outStyle		The default style string.
	// Return:    void
	// Remarks:   Gets the default style string.
	// Notes:
	//************************************
	static void	GetDefaultStyle(FPD_FormField formField, FS_ByteString* outStyle);

	//************************************
	// Function:  SetDefaultStyle
	// Param[in]: formField			The input PDF interactive form field.
	// Param[in]: szDefaultStyle	The input default style string.
	// Return:    void
	// Remarks:   Sets the default style string.
	// Notes:
	//************************************
	static void	SetDefaultStyle(FPD_FormField formField, FS_LPSTR szDefaultStyle);


	//************************************
	// Function:  GetRichTextString
	// Param[in]: formField		The input PDF interactive form field.
	// Param[out]:outText		The rich text string.
	// Return:    void
	// Remarks:   Gets the rich text string.
	// Notes:
	//************************************
	static void	GetRichTextString(FPD_FormField formField, FS_WideString* outText);

	//************************************
	// Function:  SetRichTextString
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: szRichText	The input rich text string.
	// Return:    void
	// Remarks:   Sets the rich text string. ANSI version.
	// Notes:
	//************************************
	static void	SetRichTextString(FPD_FormField formField, FS_LPCSTR szRichText);

	//************************************
	// Function:  SetRichTextStringW
	// Param[in]: formField			The input PDF interactive form field.
	// Param[in]: wszRichText		The input rich text string.
	// Return:    void
	// Remarks:   Sets the rich text string. Unicode version.
	// Notes:
	//************************************
	static void	SetRichTextStringW(FPD_FormField formField, FS_LPCWSTR wszRichText);


	//************************************
	// Function:  GetValue
	// Param[in]: formField		The input PDF interactive form field.
	// Param[out]:outValue		The value of the field.
	// Return:    void
	// Remarks:   Gets the value of the field.
	// Notes:
	//  <ul>
	//  <li>For a text field, the value is the text string;</li>
	//  <li>For a rich text field, the value is the rich text XML element;</li>
	//  <li>For a file field, the value is the path name of the file;</li>
	//  <li>For a radio button or check box field, the value is value string of the selected button;</li>
	//  <li>For a list box field, the value is the value of first selected item, if any;</li>
	//  <li>For a comb box field, the value is the text string.</li>
	//  </ul>
	//************************************
	static void	GetValue(FPD_FormField formField, FS_WideString* outValue);

	//************************************
	// Function:  GetDefaultValue
	// Param[in]: formField		The input PDF interactive form field.
	// Param[out]:outValue		The default value of the field.
	// Return:    void
	// Remarks:   Gets the default value of the field.
	// Notes:
	//  <ul>
	//  <li>For a text field, the value is the text string;</li>
	//  <li>For a rich text field, the value is the rich text XML element;</li>
	//  <li>For a file field, the value is the path name of the file;</li>
	//  <li>For a radio button or check box field, the value is value string of the selected button;</li>
	//  <li>For a list box field, the value is the value of first selected item, if any;</li>
	//  <li>For a comb box field, the value is the text string.</li>
	//  </ul>
	//************************************
	static void	GetDefaultValue(FPD_FormField formField, FS_WideString* outValue);

	//************************************
	// Function:  SetValue
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: wszValue		The input field value.
	// Param[in]: bNotify		Whether do notifying.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Sets the value of the field. not applicable to non-unison radio box.
	// Notes:
	//  <ul>
	//  <li>For a text field, the value is the text string;</li>
	//  <li>For a rich text field, the value is the rich text XML element;</li>
	//  <li>For a file field, the value is the path name of the file;</li>
	//  <li>For a radio button or check box field, the value is value string of the selected button;</li>
	//  <li>For a list box field, the value is the value of first selected item, if any;</li>
	//  <li>For a comb box field, the value is the text string.</li>
	//  </ul>
	//************************************
	static FS_BOOL SetValue(FPD_FormField formField, FS_LPCWSTR wszValue, FS_BOOL bNotify);

	//************************************
	// Function:  SetDefaultValue
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: wszValue		The input default field value.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Sets the default value of the field.
	// Notes:
	//  <ul>
	//  <li>For a text field, the value is the text string;</li>
	//  <li>For a rich text field, the value is the rich text XML element;</li>
	//  <li>For a file field, the value is the path name of the file;</li>
	//  <li>For a radio button or check box field, the value is value string of the selected button;</li>
	//  <li>For a list box field, the value is the value of first selected item, if any;</li>
	//  <li>For a comb box field, the value is the text string.</li>
	//  </ul>
	//************************************
	static FS_BOOL SetDefaultValue(FPD_FormField formField, FS_LPCWSTR wszValue);


	//************************************
	// Function:  GetMaxLen
	// Param[in]: formField		The input PDF interactive form field.
	// Return:	  The maximum length of the field's text, in characters.
	// Remarks:   Gets the maximum length of the field's text, in characters.
	// Notes:
	//************************************
	static FS_INT32	GetMaxLen(FPD_FormField formField);

	//************************************
	// Function:  SetMaxLen
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: iMaxLen		The input maximum length.
	// Return:    void
	// Remarks:   Sets the maximum length of the field's text, in characters.
	// Notes:
	//************************************
	static void	SetMaxLen(FPD_FormField formField, FS_INT32 iMaxLen);


	//************************************
	// Function:  CountSelectedItems
	// Param[in]: formField		The input PDF interactive form field.
	// Return:    The count of selected items.
	// Remarks:   Gets the count of selected items.
	// Notes:
	//************************************
	static FS_INT32	CountSelectedItems(FPD_FormField formField);

	//************************************
	// Function:  GetSelectedIndex
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: index			The item index in the selected item array.
	// Return:    The item index in all item array(include selected and unselected).
	// Remarks:   Gets the selected item index in the item array(include selected and unselected).
	// Notes:
	//************************************
	static FS_INT32	GetSelectedIndex(FPD_FormField formField, FS_INT32 index);

	//************************************
	// Function:  ClearSelection
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: bNotify		Whether do notifying.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Clears the selection.
	// Notes:
	//************************************
	static FS_BOOL ClearSelection(FPD_FormField formField, FS_BOOL bNotify);

	//************************************
	// Function:  IsItemSelected
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: index			The zero-based item index in the item array.
	// Return:    Non-zero means selected, otherwise unselected.
	// Remarks:   Checks whether the specified item has been selected.
	// Notes:
	//************************************
	static FS_BOOL IsItemSelected(FPD_FormField formField, FS_INT32 index);

	//************************************
	// Function:  SetItemSelection
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: index			The zero-based item index in the item array.
	// Param[in]: bSelected		The input selection flag.
	// Param[in]: bNotify		Whether do notifying.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Sets the selection flag of specified item.
	// Notes:
	//************************************
	static FS_BOOL SetItemSelection(FPD_FormField formField, FS_INT32 index, FS_BOOL bSelected, FS_BOOL bNotify);

	//************************************
	// Function:  IsItemDefaultSelected
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: index			The zero-based item index in the item array.
	// Return:    Non-zero means default selected, otherwise default unselected.
	// Remarks:   Checks whether the specified item's default selection flag is set.
	// Notes:
	//************************************
	static FS_BOOL IsItemDefaultSelected(FPD_FormField formField, FS_INT32 index);

	//************************************
	// Function:  SetItemDefaultSelection
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: index			The zero-based item index in the item array.
	// Param[in]: bSelected		The input default selection flag.
	// Return:    void
	// Remarks:   Sets the default selection flag of specified item.
	// Notes:
	//************************************
	static void	SetItemDefaultSelection(FPD_FormField formField, FS_INT32 index, FS_BOOL bSelected);

	//************************************
	// Function:  GetDefaultSelectedItem
	// Param[in]: formField		The input PDF interactive form field.
	// Return:    The default selected item index.
	// Remarks:   Gets the default selected item index.
	// Notes:
	//************************************
	static FS_INT32	GetDefaultSelectedItem(FPD_FormField formField);


	//************************************
	// Function:  CountOptions
	// Param[in]: formField		The input PDF interactive form field.
	// Return:    The count of options.
	// Remarks:   Gets the count of options.
	// Notes:
	//************************************
	static FS_INT32	CountOptions(FPD_FormField formField);

	//************************************
	// Function:  GetOptionLabel
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: index			The zero-based option index.
	// Param[out]:outLabel		The label of the option.
	// Return:    void
	// Remarks:   Gets the label of specified option.
	// Notes:
	//************************************
	static void	GetOptionLabel(FPD_FormField formField, FS_INT32 index, FS_WideString* outLabel);

	//************************************
	// Function:  GetOptionValue
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: index			The zero-based option index.
	// Param[out]:outValue		The value of the option.
	// Return:    void
	// Remarks:   Gets the value of specified option.
	// Notes:
	//************************************
	static void	GetOptionValue(FPD_FormField formField, FS_INT32 index, FS_WideString* outValue);

	//************************************
	// Function:  InsertOption
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: wszOptLabel	The label of the option.
	// Param[in]: index			The option index to insert at.
	// Param[in]: bNotify		Whether do notifying.
	// Return:    The inserted option index.
	// Remarks:   Inserts an option at specified position.
	// Notes:
	//************************************
	static FS_INT32	InsertOption(FPD_FormField formField, FS_LPWSTR wszOptLabel, FS_INT32 index, FS_BOOL bNotify);

	//************************************
	// Function:  FindOption
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: wszOptLabel	The label of the option to be found.
	// Return:    The index of the found option.
	// Remarks:   Finds an option by the label.
	// Notes:
	//************************************
	static FS_INT32	FindOption(FPD_FormField formField, FS_LPWSTR wszOptLabel);

	//************************************
	// Function:  FindOptionValue
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: csOptValue	The value of the option to be found.
	// Param[in]: iStartIndex	The start option index to find.
	// Return:    The index of the found option.
	// Remarks:   Finds an option by it's value.
	// Notes:
	//************************************
	static FS_INT32	FindOptionValue(FPD_FormField formField, FS_LPCWSTR csOptValue, FS_INT32 iStartIndex);

	//************************************
	// Function:  SetOptionLabel
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: index			The zero-based index of the option.
	// Param[in]: wszOptLabel	The new option label.
	// Param[in]: bNotify		Whether do notifying.
	// Return:	  Non-zero means success, otherwise failure.
	// Remarks:   Sets the option label.
	// Notes:
	//************************************
	static FS_BOOL SetOptionLabel(FPD_FormField formField, FS_INT32 index, FS_LPCWSTR wszOptLabel, FS_BOOL bNotify);

	//************************************
	// Function:  SetOptionValue
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: index			The zero-based index of the option.
	// Param[in]: wszOptValue	The new option label.
	// Param[in]: bNotify		Whether do notifying.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Sets the option value.
	// Notes:
	//************************************
	static FS_BOOL SetOptionValue(FPD_FormField formField, FS_INT32 index, FS_LPCWSTR wszOptValue, FS_BOOL bNotify);

	//************************************
	// Function:  DeleteOption
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: index			The zero-based index of the option to be deleted.
	// Param[in]: bNotify		Whether do notifying.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Deletes an option.
	// Notes:
	//************************************
	static FS_BOOL DeleteOption(FPD_FormField formField, FS_INT32 index, FS_BOOL bNotify);

	//************************************
	// Function:  ClearOptions
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: bNotify		Whether do notifying.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Clears all options.
	// Notes:
	//************************************
	static FS_BOOL ClearOptions(FPD_FormField formField, FS_BOOL bNotify);


	//************************************
	// Function:  CheckControl	
	// Param[in]: formField			The input PDF interactive form field.
	// Param[in]: iControlIndex		The index of the control.
	// Param[in]: bChecked			The input check state.
	// Param[in]: bNotify			Whether do notifying.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Checks a control.
	// Notes:
	//************************************
	static FS_BOOL CheckControl(FPD_FormField formField, FS_INT32 iControlIndex, FS_BOOL bChecked, FS_BOOL bNotify);

	//************************************
	// Function:  DefaultCheckControl
	// Param[in]: formField			The input PDF interactive form field.
	// Param[in]: iControlIndex		The index of the control.
	// Param[in]: bChecked			the input check state.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Checks a control's default check state.
	// Notes:
	//************************************
	static FS_BOOL DefaultCheckControl(FPD_FormField formField, FS_INT32 iControlIndex, FS_BOOL bChecked);

	//************************************
	// Function:  UpdateUnisonStatus
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: bNotify		Whether do notifying.
	// Return:    void
	// Remarks:   Resets or recreate Opt array list and AP stream, for RadioButton only.
	//            This is used whenever unison status is changed.
	// Notes:
	//************************************
	static void	UpdateUnisonStatus(FPD_FormField formField, FS_BOOL bNotify);


	//************************************
	// Function:  GetTopVisibleIndex	
	// Param[in]: formField		The input PDF interactive form field.
	// Return:    The top visible option index.
	// Remarks:   Gets the top visible option index.
	// Notes:
	//************************************
	static FS_INT32	GetTopVisibleIndex(FPD_FormField formField);

	//************************************
	// Function:  SetTopVisibleIndex
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: index			The input option index.
	// Return:    void
	// Remarks:   Sets the top visible option index.
	// Notes:
	//************************************
	static void	SetTopVisibleIndex(FPD_FormField formField, FS_INT32 index);


	//************************************
	// Function:  CountSelectedOptions
	// Param[in]: formField		The input PDF interactive form field.
	// Return:    The count of option indices in the "I" entry list.
	// Remarks:   Gets the count of option indices in the "I" entry list.
	// Notes:
	//************************************
	static FS_INT32	CountSelectedOptions(FPD_FormField formField);

	//************************************
	// Function:  GetSelectedOptionIndex
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: index			zero-based index in "I" entry list.
	// Return:    The index in all option array.
	// Remarks:   Gets the index of a selection option in all option array.
	// Notes:
	//************************************
	static FS_INT32	GetSelectedOptionIndex(FPD_FormField formField, FS_INT32 index);

	//************************************
	// Function:  IsOptionSelected
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: iOptIndex		zero-based index of option in Opt list.
	// Return:    Non-zero means selected, otherwise unselected.
	// Remarks:   Checks whether specified option has been selected.
	// Notes:
	//************************************
	static FS_BOOL IsOptionSelected(FPD_FormField formField, FS_INT32 iOptIndex);

	//************************************
	// Function:  SelectOption
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: iOptIndex		zero-based index of option in Opt list.
	// Param[in]: bSelected		The input selection flag.
	// Param[in]: bNotify		Whether do notifying.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Sets specified option to be selected.
	// Notes:
	//************************************
	static FS_BOOL SelectOption(FPD_FormField formField, FS_INT32 iOptIndex, FS_BOOL bSelected, FS_BOOL bNotify);

	//************************************
	// Function:  ClearSelectedOptions
	// Param[in]: formField		The input PDF interactive form field.
	// Param[in]: bNotify		Whether do notifying.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Clears selected options to be unselected.
	// Notes:
	//************************************
	static FS_BOOL ClearSelectedOptions(FPD_FormField formField, FS_BOOL bNotify);
	

	//************************************
	// Function:  GetFontSize	
	// Param[in]: formField		The input PDF interactive form field.
	// Return:    The font size.
	// Remarks:   Gets the font size.
	// Notes:
	//************************************
	static FS_FLOAT GetFontSize(FPD_FormField formField);

	//************************************
	// Function:  GetFont
	// Param[in]: formField		The input PDF interactive form field.
	// Return:    The PDF font.
	// Remarks:   Gets the font.
	// Notes:
	//************************************
	static FPD_Font	GetFont(FPD_FormField formField);
};

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*										 CFPD_IconFit_V1								*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
class CFPD_IconFit_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: dict		The input PDF dictionary.
	// Return:    A PDF icon fit.
	// Remarks:   Creates a PDF icon fit from a PDF dictionary.
	// Notes:
	//************************************
	static FPD_IconFit New(FPD_Object dict);

	//************************************
	// Function:  Destroy
	// Param[in]: iconFit		The input PDF icon fit.
	// Return:    void
	// Remarks:   Destroys a PDF icon fit.
	// Notes:
	//************************************
	static void	Destroy(FPD_IconFit iconFit);


	//************************************
	// Function:  GetScaleMethod
	// Param[in]: iconFit		The input PDF icon fit.
	// Return:    The scale method.
	// Remarks:   Gets the scale method.
	// Notes:
	//************************************
	static FS_INT32	GetScaleMethod(FPD_IconFit iconFit);

	//************************************
	// Function:  SetScaleMethod
	// Param[in]: iconFit			The input PDF icon fit.
	// Param[in]: eScaleFunction	The new scale method.
	// Return:    void
	// Remarks:   Sets the scale method.
	// Notes:
	//************************************
	static void	SetScaleMethod(FPD_IconFit iconFit, FPD_IconScaleMethod eScaleFunction);


	//************************************
	// Function:  IsProportionalScale
	// Param[in]: iconFit		The input PDF icon fit.
	// Return:    <a>TRUE</a> for the scaling being proportional.
	// Remarks:   Checks whether the scaling is proportional.
	// Notes:
	//************************************
	static FS_BOOL IsProportionalScale(FPD_IconFit iconFit);

	//************************************
	// Function:  ProportionalScale
	// Param[in]: iconFit		The input PDF icon fit.
	// Param[in]: bScale		Whether the proportional scaling flag to be set.
	// Return:    void
	// Remarks:   Change the proportional scaling flag.
	// Notes:
	//************************************
	static void	ProportionalScale(FPD_IconFit iconFit, FS_BOOL bScale);


	//************************************
	// Function:  GetIconPosition
	// Param[in]: iconFit		The input PDF icon fit.
	// Param[out]:outLeft		It receives the x-direction fraction.
	// Param[out]:outBottom		It receives the y-direction fraction.
	// Return:    void
	// Remarks:   Gets the the fraction of leftover space to allocate at the left and bottom of the icon.
	// Notes:
	//************************************
	static void	GetIconPosition(FPD_IconFit iconFit, FS_FLOAT* outLeft, FS_FLOAT* outBottom);

	//************************************
	// Function:  SetIconPosition
	// Param[in]: iconFit		The input PDF icon fit.
	// Param[in]: fLeft			The input x-direction fraction.
	// Param[in]: fBottom		The input y-direction fraction.
	// Return:    void
	// Remarks:   Sets the the fraction of leftover space to allocate at the left and bottom of the icon.
	// Notes:
	//************************************
	static void	SetIconPosition(FPD_IconFit iconFit, FS_FLOAT fLeft, FS_FLOAT fBottom);


	//************************************
	// Function:  GetFittingBounds
	// Param[in]: iconFit		The input PDF icon fit.
	// Return:    The fiting-bound flag
	// Remarks:   Gets the fiting-bound flag which indicates that the button appearance should be scaled to fit 
    //            fully within the bounds of the annotation without taking into consideration the line 
	//            width of the border.
	// Notes:
	//************************************
	static FS_BOOL GetFittingBounds(FPD_IconFit iconFit);

	//************************************
	// Function:  SetFittingBounds
	// Param[in]: iconFit		The input PDF icon fit.
	// Param[in]: bFitBounds	The input fitting bound flag.
	// Return:    void
	// Remarks:   Sets the fitting-bound flag.
	// Notes:
	//************************************
	static void	SetFittingBounds(FPD_IconFit iconFit, FS_BOOL bFitBounds);

	//************************************
	// Function:  GetDict
	// Param[in]: iconFit		The input PDF icon fit.
	// Return:    The icon fit dictionary.
	// Remarks:   Gets the icon fit dictionary.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.2.2
	//************************************
	static FPD_Object GetDict(FPD_IconFit iconFit);
};

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*									 CFPD_FormControl_V1								*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
class CFPD_FormControl_V1
{
public:

	//************************************
	// Function:  GetType
	// Param[in]: formContrl		The input PDF interactive form control.
	// Return:    A PDF interactive form control.
	// Remarks:   Gets the field type.
	// Notes:
	//************************************
	static FPD_FormFieldType	GetType(FPD_FormControl formContrl);

	//************************************
	// Function:  GetInterForm
	// Param[in]: formContrl		The input PDF interactive form control.
	// Return:    The interactive form it belongs to.
	// Remarks:   Gets the interactive form it belongs to.
	// Notes:
	//************************************
	static FPD_InterForm GetInterForm(FPD_FormControl formContrl);

	//************************************
	// Function:  GetField
	// Param[in]: formContrl		The input PDF interactive form control.
	// Return:    The field it belongs to.
	// Remarks:   Gets the field it belongs to.
	// Notes:
	//************************************
	static FPD_FormField GetField(FPD_FormControl formContrl);

	//************************************
	// Function:  GetWidget
	// Param[in]: formContrl		The input PDF interactive form control.
	// Return:    The widget annotation dictionary.
	// Remarks:   Gets the widget annotation dictionary.
	// Notes:
	//************************************
	static FPD_Object GetWidget(FPD_FormControl formContrl);

	//************************************
	// Function:  GetRect
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[out]:outRect			The rectangle of the widget.
	// Return:    void
	// Remarks:   Gets the rectangle of the widget.
	// Notes:
	//************************************
	static void	GetRect(FPD_FormControl formContrl, FS_FloatRect* outRect);

	//************************************
	// Function:  DrawControl
	// Param[in]: formContrl			The input PDF interactive form control.
	// Param[in]: device				The device to draw on.
	// Param[in]: matrix				The transformation matrix from form control space to device space.
	// Param[in]: page					The PDF page it belongs to.
	// Param[in]: eMode					The input appearance mode.
	// Return:    void
	// Remarks:   Draws the form control.
	// Notes:
	//************************************
	static void	DrawControl(
		FPD_FormControl formContrl, 
		FPD_RenderDevice device, 
		FS_AffineMatrix matrix,
		FPD_Page page, 
		FPD_AnnotAppearanceMode eMode
		);


	//************************************
	// Function:  GetCheckedAPState
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[out]:outState			The checked state appearance string.
	// Return:    void
	// Remarks:   Gets the checked state appearance string.
	// Notes:
	//************************************
	static void	GetCheckedAPState(FPD_FormControl formContrl, FS_ByteString* outState);

	//************************************
	// Function:  GetExportValue
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[out]:outValue			The export mapping name.
	// Return:    void
	// Remarks:   Gets the export mapping name.
	// Notes:
	//************************************
	static void	GetExportValue(FPD_FormControl formContrl, FS_WideString* outValue);

	//************************************
	// Function:  SetExportValue
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: wszValue			The input export name.
	// Param[in]: bNotify			Whether do notifying.
	// Return:    void
	// Remarks:   Sets the export mapping name.
	// Notes:
	//************************************
	static void	SetExportValue(FPD_FormControl formContrl, FS_LPCWSTR wszValue, FS_BOOL bNotify);

	//************************************
	// Function:  IsChecked
	// Param[in]: formContrl		The input PDF interactive form control.
	// Return:    <a>TRUE</a> for the control being checked.
	// Remarks:   Checks whether the control is checked.
	// Notes:
	//************************************
	static FS_BOOL IsChecked(FPD_FormControl formContrl);

	//************************************
	// Function:  IsDefaultChecked
	// Param[in]: formContrl		The input PDF interactive form control.
	// Return:    <a>TRUE</a> for the contro being default checked.
	// Remarks:   Checks whether the control is default checked.
	// Notes:
	//************************************
	static FS_BOOL IsDefaultChecked(FPD_FormControl formContrl);


	//************************************
	// Function:  GetHighlightingMode
	// Param[in]: formContrl		The input PDF interactive form control.
	// Return:    The highlighting mode.
	// Remarks:   Gets the highlighting mode.
	// Notes:
	//************************************
	static FPD_FormCtrlHighlightingMode	GetHighlightingMode(FPD_FormControl formContrl);

	//************************************
	// Function:  SetHighlightingMode
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: eHLMode			The input highlighting mode.
	// Return:    void
	// Remarks:   Sets the highlighting mode.
	// Notes:
	//************************************
	static void	SetHighlightingMode(FPD_FormControl formContrl, FPD_FormCtrlHighlightingMode eHLMode);


	//************************************
	// Function:  HasMKEntry
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: szEntry			The input entry name.
	// Return:    Non-zero means exist, otherwise not exist.
	// Remarks:   Checks whether the specified entry exist in the appearance characteristics dictionary.
	// Notes:
	//************************************
	static FS_BOOL HasMKEntry(FPD_FormControl formContrl, FS_LPSTR szEntry);

	//************************************
	// Function:  RemoveMKEntry
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: szEntry			The input entry name to be removed.
	// Return:    void
	// Remarks:   Removes a entry in the appearance characteristics dictionary.
	// Notes:
	//************************************
	static void	RemoveMKEntry(FPD_FormControl formContrl, FS_LPSTR szEntry);


	//************************************
	// Function:  GetRotation
	// Param[in]: formContrl		The input PDF interactive form control.
	// Return:    The number of degrees.
	// Remarks:   Gets the number of degrees by which the widget annotation is rotated 
	//            counterclockwise relative to the page. The value must be a multiple of 90.
	// Notes:
	//************************************
	static FS_INT32	GetRotation(FPD_FormControl formContrl);

	//************************************
	// Function:  SetRotation
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: iDegree			The input number of degrees to rotated counterclockwise.
	// Return:    void
	// Remarks:   Sets the rotation.
	// Notes:
	//************************************
	static void	SetRotation(FPD_FormControl formContrl, FS_INT32 iDegree);


	//************************************
	// Function:  GetBorderColor
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[out]:iColorType		It receives the color type.
	// Return:    The FX_ARGB color.
	// Remarks:   Gets the border color.
	// Notes:
	//************************************
	static FS_ARGB GetBorderColor(FPD_FormControl formContrl, FS_INT32* iColorType);

	//************************************
	// Function:  GetOriginalBorderColor
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: index				The component index of the original color.
	// Return:    The value of the component.
	// Remarks:   Gets an original color component of the border color.
	// Notes:
	//************************************
	static FS_FLOAT GetOriginalBorderColor(FPD_FormControl formContrl, FS_INT32 index);

	//************************************
	// Function:  GetOriginalBorderColor2
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[out]:outColorType		It receives the color type.
	// Param[out]:outFc[4]			It receives the values of the components.
	// Return:    void
	// Remarks:   Gets the original color of the border.
	// Notes:
	//************************************
	static void	GetOriginalBorderColor2(FPD_FormControl formContrl, FS_INT32* outColorType, FS_FLOAT outFc[4]);

	//************************************
	// Function:  SetBorderColor
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: iColorType		The input color type.
	// Param[in]: color				The FS_ARGB value of the input color.
	// Return:    void
	// Remarks:   Sets the border color.
	// Notes:
	//************************************
	static void	SetBorderColor(FPD_FormControl formContrl, FS_INT32 iColorType, FS_ARGB color);


	//************************************
	// Function:  GetBackgroundColor
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[out]:outColorType		It receives the color type.
	// Return:    The FS_ARGB color.c
	// Remarks:   Gets the background color.
	// Notes:
	//************************************
	static FS_ARGB GetBackgroundColor(FPD_FormControl formContrl, FS_INT32* outColorType);

	//************************************
	// Function:  GetOriginalBackgroundColor
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: index				The component index of the original color.
	// Return:    The value of the component.
	// Remarks:   Gets an original color component of the background color.
	// Notes:
	//************************************
	static FS_FLOAT	GetOriginalBackgroundColor(FPD_FormControl formContrl, FS_INT32 index);

	//************************************
	// Function:  GetOriginalBackgroundColor2
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[out]:outColorType		It receives the color type.
	// Param[out]:fc[4]				It receives the values of the components.
	// Return:    void
	// Remarks:   Gets the original color of the background.
	// Notes:
	//************************************
	static void	GetOriginalBackgroundColor2(FPD_FormControl formContrl, FS_INT32* outColorType, FS_FLOAT fc[4]);

	//************************************
	// Function:  SetBackgroundColor
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: iColorType		The input color type.
	// Param[in]: color				The FS_ARGB value of the input color.
	// Return:    void
	// Remarks:   Sets the background color.
	// Notes: 
	//************************************
	static void	SetBackgroundColor(FPD_FormControl formContrl, FS_INT32 iColorType, FS_ARGB color);


	//************************************
	// Function:  GetNormalCaption
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[out]:outCaption		The widget annotation's normal caption.
	// Return:    void
	// Remarks:   Gets the widget annotation's normal caption.
	// Notes:
	//************************************
	static void	GetNormalCaption(FPD_FormControl formContrl, FS_WideString* outCaption);

	//************************************
	// Function:  SetNormalCaption
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: szCaption			The input normal caption.
	// Return:    void
	// Remarks:   Sets the widget annotation's normal caption. ANSI version.
	// Notes:
	//************************************
	static void	SetNormalCaption(FPD_FormControl formContrl, FS_LPCSTR szCaption);

	//************************************
	// Function:  SetNormalCaptionW
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: wszCaption		The input normal caption.
	// Return:    void
	// Remarks:   Sets the widget annotation's normal caption. Unicode version.
	// Notes:
	//************************************
	static void	SetNormalCaptionW(FPD_FormControl formContrl, FS_LPCWSTR wszCaption);


	//************************************
	// Function:  GetRolloverCaption
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[out]:outCaption		The widget annotation's rollover caption.
	// Return:    void
	// Remarks:   Gets the widget annotation's rollover caption.
	// Notes:
	//************************************
	static void	GetRolloverCaption(FPD_FormControl formContrl, FS_WideString* outCaption);

	//************************************
	// Function:  SetRolloverCaption
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: szCaption			The input rollover caption.
	// Return:    void
	// Remarks:   Sets the widget annotation's rollover caption. ANSI version.
	// Notes:
	//************************************
	static void	SetRolloverCaption(FPD_FormControl formContrl, FS_LPCSTR szCaption);

	//************************************
	// Function:  SetRolloverCaptionW
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: wszCaption		The input rollover caption.
	// Return:    void
	// Remarks:   Sets the widget annotation's rollover caption. Unicode version.
	// Notes:
	//************************************
	static void	SetRolloverCaptionW(FPD_FormControl formContrl, FS_LPCWSTR wszCaption);


	//************************************
	// Function:  GetDownCaption
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[out]:outCaption		The widget annotation's alternate (down) caption.
	// Return:    void
	// Remarks:   Gets the widget annotation's alternate (down) caption.
	// Notes:
	//************************************
	static void	GetDownCaption(FPD_FormControl formContrl, FS_WideString* outCaption);

	//************************************
	// Function:  SetDownCaption
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: szCaption			The input down caption.
	// Return:    void	
	// Remarks:   Sets the widget annotation's down caption. ANSI version.
	// Notes:
	//************************************
	static void	SetDownCaption(FPD_FormControl formContrl,  FS_LPCSTR szCaption);

	//************************************
	// Function:  SetDownCaptionW
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: wszCaption		The input down caption.
	// Return:    void
	// Remarks:   Sets the widget annotation's down caption. Unicode version.
	// Notes:
	//************************************
	static void	SetDownCaptionW(FPD_FormControl formContrl, FS_LPCWSTR wszCaption);


	//************************************
	// Function:  GetNormalIcon
	// Param[in]: formContrl		The input PDF interactive form control.
	// Return:    The widget annotation's normal icon. 
	// Remarks:   Gets the widget annotation's normal icon. 
	// Notes:
	//************************************
	static FPD_Object GetNormalIcon(FPD_FormControl formContrl);

	//************************************
	// Function:  SetNormalIcon
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: iconStm			The input PDF stream of normal icon.
	// Return:    void
	// Remarks:   Sets the widget annotation's normal icon.
	// Notes:
	//************************************
	static void	SetNormalIcon(FPD_FormControl formContrl, const FPD_Object iconStm);


	//************************************
	// Function:  GetRolloverIcon
	// Param[in]: formContrl		The input PDF interactive form control.
	// Return:    The widget annotation's rollover icon.
	// Remarks:   Gets the widget annotation's rollover icon.
	// Notes:
	//************************************
	static FPD_Object GetRolloverIcon(FPD_FormControl formContrl);

	//************************************
	// Function:  SetRolloverIcon
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: iconStream		The input PDF stream of rollover icon.
	// Return:    void
	// Remarks:   Sets the widget annotation's rollover icon.
	// Notes:
	//************************************
	static void	SetRolloverIcon(FPD_FormControl formContrl, const FPD_Object iconStream);


	//************************************
	// Function:  GetDownIcon
	// Param[in]: formContrl		The input PDF interactive form control.
	// Return:    The widget annotation's down icon.
	// Remarks:   Gets the widget annotation's down icon.
	// Notes:
	//************************************
	static FPD_Object GetDownIcon(FPD_FormControl formContrl);

	//************************************
	// Function:  SetDownIcon
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: iconStream		The input PDF stream of down icon.
	// Return:    void
	// Remarks:   Sets the widget annotation's down icon.
	// Notes:
	//************************************
	static void	SetDownIcon(FPD_FormControl formContrl, const FPD_Object iconStream);


	//************************************
	// Function:  GetIconFit
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[out]:outIconFit		The icon fit of the widget. 
	// Return:    void
	// Remarks:   Gets the icon fit of the widget. 
	// Notes:
	//************************************
	static void GetIconFit(FPD_FormControl formContrl, FPD_IconFit* outIconFit);

	//************************************
	// Function:  SetIconFit
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: cIF				The input icon fit.
	// Param[in]: pObjs				The indirect object collection.
	// Return:    void
	// Remarks:   Sets the icon fit of the widget.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void	SetIconFit(FPD_FormControl formContrl, const FPD_IconFit cIF, void* pObjs);

	//************************************
	// Function:  GetTextPosition
	// Param[in]: formContrl		The input PDF interactive form control.
	// Return:    The text position.
	// Remarks:   Gets where to position the text of the widget annotation's caption relative to its icon.
	// Notes:
	//************************************
	static FS_INT32	GetTextPosition(FPD_FormControl formContrl);

	//************************************
	// Function:  SetTextPosition
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: iPos				The input text position.
	// Return:    void
	// Remarks:   Sets the text position.
	// Notes:
	//************************************
	static void	SetTextPosition(FPD_FormControl formContrl, FS_INT32 iPos);


	//************************************
	// Function:  GetAction
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[out]:outAction			The action to be performed when the annotation is activated.
	// Return:    void
	// Remarks:   Gets the action to be performed when the annotation is activated.
	// Notes:
	//************************************
	static void GetAction(FPD_FormControl formContrl, FPD_Action* outAction);

	//************************************
	// Function:  SetAction
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: action			The input action.
	// Return:    void
	// Remarks:   Sets the action to be performed when the annotation is activated.
	// Notes:
	//************************************
	static void	SetAction(FPD_FormControl formContrl, const FPD_Action action);


	//************************************
	// Function:  GetAdditionalAction
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[out]:pAAction			It receives the additional-actions.
	// Return:    void
	// Remarks:   Gets the additional-actions defining the annotation's behavior in response to various trigger events. 
	// Notes:
	//************************************
	static void GetAdditionalAction(FPD_FormControl formContrl, FPD_AAction* pAAction);

	//************************************
	// Function:  SetAdditionalAction
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: aaction			The input additional-actions.
	// Return:    void
	// Remarks:   Sets the additional-actions.
	// Notes:
	//************************************
	static void	SetAdditionalAction(FPD_FormControl formContrl, const FPD_AAction aaction);


	//************************************
	// Function:  GetDefaultAppearance
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[out]:outDefAP			The default appearance.
	// Return:    void
	// Remarks:   Gets the default appearance.
	// Notes: Do not forget to save default appearance after you change any setting in it when you
	//		retrieve an object of FPD_DefaultAppearance by GetDefaultAppearance method.
	//************************************
	static void	GetDefaultAppearance(FPD_FormControl formContrl, FPD_DefaultAppearance* outDefAP);

	//************************************
	// Function:  SetDefaultAppearance
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: cDA				The input default appearance.
	// Return:    void
	// Remarks:   Sets the default appearance.
	// Notes:
	//************************************
	static void	SetDefaultAppearance(FPD_FormControl formContrl, const FPD_DefaultAppearance cDA);

	//************************************
	// Function:  GetDefaultControlFont
	// Param[in]: formContrl		The input PDF interactive form control.
	// Return:    The default font of the control.
	// Remarks:   Gets the default font of the control.
	// Notes:
	//************************************
	static FPD_Font	GetDefaultControlFont(FPD_FormControl formContrl);

	//************************************
	// Function:  SetDefaultControlFont
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: font				The input PDF font.
	// Return:    void
	// Remarks:   Sets the default font of the control. 
	// Notes:
	//************************************
	static void	SetDefaultControlFont(FPD_FormControl formContrl, const FPD_Font font);


	//************************************
	// Function:  GetControlAlignment
	// Param[in]: formContrl		The input PDF interactive form control.
	// Return:    The alignment of the control.
	//  <ul><li>0 - left alignment, the default setting.</li>
	//  <li>1 - centered, </li>
	//  <li>2 - right alignment. </li></ul>
	// Remarks:   Gets the alignment of the control.
	// Notes:
	//************************************
	static FS_INT32	GetControlAlignment(FPD_FormControl formContrl);

	//************************************
	// Function:  SetControlAlignment
	// Param[in]: formContrl		The input PDF interactive form control.
	// Param[in]: iAlignment		The input alignment.
	//  0 - left alignment, the default setting.
	//  1 - centered, 
	//  2 - right alignment. 
	// Return:    void.
	// Remarks:   Sets the alignment of the control. 
	// Notes:
	//************************************
	static void	SetControlAlignment(FPD_FormControl formContrl, FS_INT32 iAlignment);
};

class CFPD_FDFDoc_V1
{
public:

	//************************************
	// Function: New
	// Return:   A new empty FDF document.
	// Remarks:  Creates a new empty FDF document.
	// Notes:
	//************************************
	static FDF_Document New();

	//************************************
	// Function:  OpenFromFile
	// Param[in]: strFilePath		The input file full path.
	// Return:    The FDF document.
	// Remarks:   Loads a FDF document from a file.
	// Notes:
	//************************************
	static FDF_Document OpenFromFile(FS_LPCSTR strFilePath);

	//************************************
	// Function:  PareMemory
	// Param[in]: memoryBlock		The input memory block.
	// Param[in]: size				The size in bytes of the block.
	// Return:    The FDF document.
	// Remarks:   Loads a FDF document from a memory block.
	// Notes:
	//************************************
	static FDF_Document PareMemory(FS_LPCBYTE memoryBlock, FS_DWORD size);

	//************************************
	// Function:  Destroy
	// Param[in]: doc	The input FDF document.	
	// Return:    void
	// Remarks:   Destroys The FDF document.
	// Notes:
	//************************************
	static void Destroy(FDF_Document doc);

	//************************************
	// Function:  WriteFile
	// Param[in]: doc				The input FDF document.	
	// Param[in]: strFilePath		The file full path to write to.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Writes the content of this FDF document to a file.
	// Notes:
	//************************************
	static FS_BOOL WriteFile(FDF_Document doc, FS_LPCSTR strFilePath);

	//************************************
	// Function:  GetRoot
	// Param[in]: doc	The input FDF document.	
	// Return:    Root dictionary of the FDF document.
	// Remarks:   Gets root dictionary of the FDF document. All other data can be fetched from this root.
	// Notes:
	//************************************
	static FPD_Object GetRoot(FDF_Document doc);

	//************************************
	// Function:  GetWin32Path
	// Param[in]: doc			The input FDF document.	
	// Param[out]:outPath		The file path of attached PDF document, if any.
	// Return:    Root dictionary of the FDF document.
	// Remarks:   Gets file path of attached PDF document, if any. Different platform has different format.
	// Notes:
	//************************************
	static void GetWin32Path(FDF_Document doc, FS_WideString* outPath);

	//************************************
	// Function:  GetIndirectObject
	// Param[in]: doc		The input memory document.
	// Param[in]: objNum	The input object number.
	// Return:    An memory document.
	// Remarks:   Loads an indirect object by an object number.
	// Notes:
	//************************************
	static FPD_Object GetIndirectObject(FDF_Document doc, FS_DWORD objNum);

	//************************************
	// Function:  GetIndirectType
	// Param[in]: doc		The input memory document.
	// Param[in]: objNum	The input object number.
	// Return:    The type of the memory document.
	// Remarks:   Gets type of an memory document, without loading the object content.
	// Notes:
	//************************************
	static FS_INT32	GetIndirectType(FDF_Document doc, FS_DWORD objNum);

	//************************************
	// Function:  AddIndirectObject
	// Param[in]: doc		The input memory document.
	// Param[in]: obj		The input object.
	// Return:    The new object number.
	// Remarks:   Adds an object to indirect object list. The new object number is returned.
	// Notes:
	//************************************
	static FS_DWORD	AddIndirectObject(FDF_Document doc, FPD_Object obj);

	//************************************
	// Function:  ReleaseIndirectObject
	// Param[in]: doc		The input memory document.
	// Param[in]: objNum	The object number to release.
	// Return:    void
	// Remarks:   Sometimes, for saving memory space, we can release a loaded memory document.
	//            However, use this with caution because the object pointer will become invalid.
	// Notes:
	//************************************
	static void	ReleaseIndirectObject(FDF_Document doc, FS_DWORD objNum);

	//************************************
	// Function:  DeleteIndirectObject
	// Param[in]: doc		The input memory document.
	// Param[in]: objNum	The object number to delete.
	// Return:    void
	// Remarks:   Deletes an memory document. Use this function with caution!
	// Notes:
	//************************************
	static void	DeleteIndirectObject(FDF_Document doc, FS_DWORD objNum);

	//************************************
	// Function:  ImportIndirectObject
	// Param[in]: doc		The input memory document.
	// Param[in]: pBuffer	The binary data for the memory document. It must be not encrypted.
	// Param[in]: size		The size in bytes of the binary data.
	// Return:    An object.
	// Remarks:   Imports an object from its binary format.
	//            This is used when the PDF is fetched in "Progressive Downloading" fashion.
	//            After this function call, the data buffer can be destroyed.
	//            This object must not be encrypted.
	// Notes:
	//************************************
	static FPD_Object ImportIndirectObject(FDF_Document doc, FS_LPCBYTE pBuffer, FS_DWORD size);

	//************************************
	// Function:  ImportExternalObject
	// Param[in]: doc		The input memory document.
	// Param[in]: obj		The object in external object collection.
	// Param[out]:mapping   It updates the mapping from old object number to new object number.	
	// Return:    A new memory document.
	// Remarks:   Imports an object from external object collection as a new memory document.
	//            If the external object refers to other external indirect objects, those indirect objects
	//            are also imported.
	//            The mapping from old object number to new object number is updated during the import process.
	// Notes:
	//************************************
	static FPD_Object ImportExternalObject(FDF_Document doc, FPD_Object obj, FS_MapPtrToPtr mapping);

	//************************************
	// Function:  InsertIndirectObject
	// Param[in]: doc		The input memory document.
	// Param[in]: objNum	The new object number of the indirect object to insert.
	// Param[in]: obj		The indirect object to insert.
	// Return:    void
	// Remarks:   Inserts an indirect object with specified object number.
	//			  This is used when the PDF is fetched in "Progressive Downloading" fashion, or parsing FDF.
	//            If an indirect object with the same object number exists, the previous one will be destroyed.
	// Notes:
	//************************************
	static void	InsertIndirectObject(FDF_Document doc, FS_DWORD objNum, FPD_Object obj);

	//************************************
	// Function:  GetLastobjNum
	// Param[in]: doc		The input memory document.
	// Return:    Last assigned indirect object number.
	// Remarks:   Gets last assigned indirect object number.
	// Notes:
	//************************************
	static FS_DWORD	GetLastobjNum(FDF_Document doc);

	//************************************
	// Function:  ReloadFileStreams
	// Param[in]: doc		The input memory document.
	// Return:    void
	// Remarks:   Reload all file based stream when we do reparsing.
	// Notes:
	//************************************
	static void	ReloadFileStreams(FDF_Document doc);

	//************************************
	// Function:  GetStartPosition
	// Param[in]: doc		The input memory document.
	// Return:    The start position of the indirect objects.
	// Remarks:   Gets the start position of the indirect objects.
	// Notes:
	//************************************
	static FS_POSITION GetStartPosition(FDF_Document doc);

	//************************************
	// Function:  GetNextAssoc
	// Param[in]: doc			The input memory document.
	// Param[in,out]:outPos		Input current position and receive the next position.
	// Param[out]:outObjNum		It receives the current object number.
	// Param[out]:outObject		It receives the current indirect object pointer.
	// Return:    void
	// Remarks:   Accesses the indirect object of current position, and move the position to next.
	// Notes:
	//************************************
	static void	GetNextAssoc(FDF_Document doc, FS_POSITION* outPos, FS_DWORD* outObjNum, FPD_Object* outObject);

	//************************************
	// Function:  IsModified
	// Param[in]: doc		The input memory document.
	// Return:    <a>TRUE</a> for the indirect objects are modified, otherwise not.
	// Remarks:   Checks if any of the indirect objects are modified, since loading from parser, or last ClearModified.
	// Notes:
	//************************************
	static FS_BOOL IsModified(FDF_Document doc);

	//************************************
	// Function:  ClearModified
	// Param[in]: doc		The input memory document.
	// Return:    void
	// Remarks:   Clears the modified flag.
	// Notes:
	//************************************
	static void	ClearModified(FDF_Document doc);

	//************************************
	// Function:  WriteBuf
	// Param[in]: doc		The input document.
	// Param[out]:outBuf	It will receive the content of this PDF document.
	// Return:    <a>TRUE</a> means success, otherwise failure.
	// Remarks:   Writes the content of this FDF document to a memory block.
	// Notes:
	//************************************
	static FS_BOOL WriteBuf(FDF_Document doc, FS_ByteString* outBuf);

	//************************************
	// Function:  GetAnnotCount
	// Param[in]: doc		The input document.
	// Param[in]: szFilter	Pointer to a filter string. 
	//						It can be NULL, otherwise it must be like "Text" or "Text, Link, Circle".
	// Return:    The count of annotations.
	// Remarks:   Count annotations in a FDF document by specific filter.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static FS_INT32 GetAnnotCount(FDF_Document doc, FS_LPCSTR szFilter);

	//************************************
	// Function:  GetAnnotDict
	// Param[in]: doc		The input document.
	// Param[in]: szFilter	Pointer to a filter string. 
	//						It can be NULL, otherwise it must be like "Text" or "Text, Link, Circle".
	// Param[in]: nIndex	A zero-based annotation index based on the annotations which are specified by filter.
	// Return:    The FDF annotation dictionary.
	// Remarks:   Get an annotation dictionary from FDF document by specific filter and index.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static FPD_Object GetAnnotDict(FDF_Document doc, FS_LPCSTR szFilter, FS_INT32 nIndex);

	//************************************
	// Function:  GetAnnotPageIndex
	// Param[in]: fdfAnnotDict		The FDF annotation dictionary.
	// Return:    The zero-based index of PDF page.
	// Remarks:   Get the zero-based index of PDF page on which the original PDF annotation locates.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static FS_INT32 GetAnnotPageIndex(FPD_Object fdfAnnotDict);

	//************************************
	// Function:  ExportAnnotToPDFPage
	// Param[in]: doc				The input document.
	// Param[in]: fdfAnnotDict		The FDF annotation dictionary.
	// Param[in]: pdfDoc			The PDF document to which the FDF annotation is exported.
	// Param[in]: pdfPage			The PDF page to which the FDF annotation is exported.
	// Param[out]: arrAnnotDict		The pointer array to receive generated PDF annotation dictionaries.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Export an annotation object loaded from a FDF document into a PDF page.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static FS_BOOL ExportAnnotToPDFPage(FDF_Document doc, FPD_Object fdfAnnotDict, FPD_Document pdfDoc, FPD_Page pdfPage, FS_PtrArray* arrAnnotDict);

	//************************************
	// Function:  ImportPDFAnnot
	// Param[in]: doc			The input document.
	// Param[in]: pdfAnnotDict	The PDF annotation dictionary to be imported. 
	// Param[in]: nPageIndex	The zero-based index of PDF page on which the annotation locates.
	// Return:    Non-zero means success, otherwise failure.
	// Remarks:   Import an annotation from PDF document.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static FS_BOOL ImportPDFAnnot(FDF_Document doc, FPD_Object pdfAnnotDict, FS_INT32 nPageIndex);

	//************************************
	// Function:  SetPDFPath
	// Param[in]: doc			The input document.
	// Param[in]: wszPDFPath	The PDF path. 
	// Return:    
	// Remarks:   Set the path of PDF document to FDF document.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static void SetPDFPath(FDF_Document doc, FS_LPCWSTR wszPDFPath);
};

class CFPD_ConnectedInfo_V7
{
public:
	//************************************
	// Function:  New
	// Param[in]: fpdDoc	The input document.
	// Return: The connected PDF info object.
	// Remarks: Creates the connected PDF info object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1
	//************************************
	static FPD_ConnectedInfo New(FPD_Document fpdDoc);

	//************************************
	// Function:  Destroy
	// Param[in]: connectedInfo	The input connected PDF info object.
	// Return: void.
	// Remarks: Destroys the connected PDF info object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1
	//************************************
	static void Destroy(FPD_ConnectedInfo connectedInfo);

	//************************************
	// Function:  SetId
	// Param[in]: connectedInfo	The input connected PDF info object.
	// Param[in]: nIdType		The input Id type.
	// Param[in]: bsURI			The input URI.
	// Param[in]: bsCert		The input certificate. This param is reserved now.
	// Return: void.
	// Remarks: Sets the Id of the connected PDF info object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1
	//************************************
	static void SetId(FPD_ConnectedInfo connectedInfo, FS_INT32 nIdType, FS_ByteString bsURI, FS_ByteString bsCert);

	//************************************
	// Function:  GetId
	// Param[in]: connectedInfo	The input connected PDF info object.
	// Param[in]: nIdType		The input Id type.
	// Param[out]: outURI		It receives the URI.
	// Param[out]: outCert		It receives the certificate. This param is reserved now.
	// Return: TRUE for success, otherwise failure.
	// Remarks: Gets the Id of the connected PDF info object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1
	//************************************
	static FS_BOOL GetId(FPD_ConnectedInfo connectedInfo, FS_INT32 nIdType, FS_ByteString* outURI, FS_ByteString* outCert);

	//************************************
	// Function:  IsConnectedPDF
	// Param[in]: connectedInfo	The input connected PDF info object.
	// Return: TRUE if the PDF document is a connected PDF, otherwise not.
	// Remarks: Checks whether the PDF document is a connected PDF or not.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1
	//************************************
	static FS_BOOL IsConnectedPDF(FPD_ConnectedInfo connectedInfo);

	//************************************
	// Function:  IsConnectedPDF2
	// Param[in]: fpdDoc	The input PDF document.
	// Return: TRUE if the PDF document is a connected PDF, otherwise not.
	// Remarks: Checks whether the PDF document is a connected PDF or not.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1
	//************************************
	static FS_BOOL IsConnectedPDF2(FPD_Document fpdDoc);

	//************************************
	// Function:  SetTracking
	// Param[in]: connectedInfo	The input connected PDF info object.
	// Param[in]: track			The input tracking type.
	// Return: void.
	// Remarks: Sets the tracking type. This interface is reserved now.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1
	//************************************
	static void SetTracking(FPD_ConnectedInfo connectedInfo, FS_INT32 track);

	//************************************
	// Function:  GetTracking
	// Param[in]: connectedInfo	The input connected PDF info object.
	// Param[out]: outTrack		It receives the tracking type.
	// Return: TRUE for success, otherwise failure.
	// Remarks: Gets the tracking type. This interface is reserved now.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1
	//************************************
	static FS_BOOL GetTracking(FPD_ConnectedInfo connectedInfo, FS_INT32* outTrack);

	//************************************
	// Function:  Update
	// Param[in]: connectedInfo	The input connected PDF info object.
	// Param[in]: bOpenAction	Whether to add open action to ConnectedPDF document or not. Sets it FALSE as default.
	// Return: void.
	// Remarks: Updates the connected PDF info.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1
	//************************************
	static void Update(FPD_ConnectedInfo connectedInfo, FS_BOOL bOpenAction);

	//************************************
	// Function:  SetEncryptOffline
	// Param[in]: connectedInfo	The input connected PDF info object.
	// Param[in]: bOffline		The input offline flag parameter.
	// Return: void.
	// Remarks: Sets whether the encrypted connected PDF is offline or not.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static void SetEncryptOffline(FPD_ConnectedInfo connectedInfo, FS_BOOL bOffline);

	//************************************
	// Function:  SetEncryptEnvelope
	// Param[in]: connectedInfo	The input connected PDF info object.
	// Param[in]: bsEnvelope	The input offline envelope.
	// Return: void.
	// Remarks: Sets envelope string to encrypt dictionary.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static void SetEncryptEnvelope(FPD_ConnectedInfo connectedInfo, FS_ByteString bsEnvelope);

	//************************************
	// Function:  GetEncryptOffline
	// Param[in]: connectedInfo	The input connected PDF info object.
	// Param[out]: outIsOffline	It receives the result whether the encrypted connected PDF is offline or not.
	// Return: TRUE for success, otherwise failure.
	// Remarks: Sets envelope string to encrypt dictionary.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static FS_BOOL GetEncryptOffline(FPD_ConnectedInfo connectedInfo, FS_BOOL* outIsOffline);

	//************************************
	// Function:  GetEncryptEnvelope
	// Param[in]: connectedInfo	The input connected PDF info object.
	// Param[out]: outEnvelope	It receives the offline envelope.
	// Return: TRUE for success, otherwise failure.
	// Remarks: Gets the offline envelope.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static FS_BOOL GetEncryptEnvelope(FPD_ConnectedInfo connectedInfo, FS_ByteString* outEnvelope);
	
	//************************************
	// Function:  SetOpenActionURL
	// Param[in]: connectedInfo	The input connected PDF info object.
	// Param[in]: lpsURL		The input sever URL.
	// Return: TRUE for success, otherwise failure.
	// Remarks: Sets OpenAction javascript server URL.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static FS_BOOL SetOpenActionURL(FPD_ConnectedInfo connectedInfo, FS_LPCSTR lpsURL);

	//************************************
	// Function:  SetOpenActionURL2
	// Param[in]: connectedInfo	The input connected PDF info object.
	 // Param[in]: nType		Sees <a>FPDConnectedOpenActionURLTypes</a> definitions.
	// Param[in]: lpsURL		The input sever URL.
	// Return: TRUE for success, otherwise failure.
	// Remarks: Sets OpenAction javascript server URL.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.0.2
	//************************************
	static FS_BOOL SetOpenActionURL2(FPD_ConnectedInfo connectedInfo, FS_INT32 nType, FS_LPCSTR lpsURL);

	//************************************
	// Function:  CheckSettingOpenAction
	// Param[in]: connectedInfo	The input connected PDF info object.
	// Param[in]: nType		Sees <a>FPDConnectedOpenActionURLTypes</a> definitions.
	// Param[in]: lpsURL		The input sever URL.
	// Return: TRUE if the OpenAction javascript server URL is set.
	// Remarks: Checks whether the OpenAction javascript server URL is set or not.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.0.2
	//************************************
	static FS_BOOL CheckSettingOpenAction(FPD_ConnectedInfo connectedInfo, FS_INT32 nType, FS_LPCSTR lpsURL);

	//************************************
	// Function:  DeleteOpenAction
	// Param[in]: connectedInfo	The input connected PDF info object.
	// Return: void.
	// Remarks: Deletes ConnectedPDF info in OpenAction.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.2.1
	//************************************
	static void DeleteOpenAction(FPD_ConnectedInfo connectedInfo);

	//************************************
	// Function:  SetEndpoint
	// Param[in]: connectedInfo	The input connected PDF info object.
	// Param[in]: lpsEndPoint	The input URL of connected PDF web services.
	// Return: void.
	// Remarks: Sets the URL of connected PDF web services.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3
	//************************************
	static void SetEndpoint(FPD_ConnectedInfo connectedInfo, FS_LPCSTR lpsEndPoint);
	
	//************************************
	// Function:  GetEndpoint
	// Param[in]: connectedInfo	The input connected PDF info object.
	// Param[out]: outEndPoint	It receives the URL of connected PDF web services.
	// Return: TRUE if success, FALSE if failed.
	// Remarks: Gets the URL of connected PDF web services.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3
	//************************************
	static FS_BOOL GetEndpoint(FPD_ConnectedInfo connectedInfo, FS_ByteString* outEndPoint);
};

class CFPD_WrapperDoc_V10
{
public:
	//************************************
	// Function:  New
	// Param[in]: fpdDoc	The input document.
	// Return: The wrapper document object.
	// Remarks: Create the wrapper document object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3.1
	//************************************
	static FPD_WrapperDoc New(FPD_Document fpdDoc);

	//************************************
	// Function:  Destroy
	// Param[in]: wrapperDoc	The input wrapper document object.
	// Return: void.
	// Remarks: Destroys the wrapper document object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3.1
	//************************************
	static void Destroy(FPD_WrapperDoc wrapperDoc);

	//************************************
	// Function:  GetWrapperType
	// Param[in]: wrapperDoc	The input wrapper document object.
	// Return:	-1 for error;
	//			<a>PDF_WRAPPERTYPE_NO</a> for no wrapper document;
	//			<a>PDF_WRAPPERTYPE_FOXIT</a> for Foxit wrapper document;
	//			<a>PDF_WRAPPERTYPE_PDF2</a> for PDF2.0 wrapper document.
	// Remarks: Whether the document is a wrapper document or normal.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3.1
	//************************************
	static FS_INT32	GetWrapperType(FPD_WrapperDoc wrapperDoc);

	//************************************
	// Function:  GetCryptographicFilter
	// Param[in]: wrapperDoc	The input wrapper document object.
	// Param[out]: wsGraphicFilter	The name of the cryptographic filter.
	// Param[out]: fVersion			The version number of the cryptographic filter, if present; Otherwise, 0.
	// Return:	TRUE if success, otherwise return FALSE.
	// Remarks:	Get the details of the cryptographic filter needed to decrypt the encrypted payload.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3.1
	//************************************
	static FS_BOOL GetCryptographicFilter(FPD_WrapperDoc wrapperDoc, FS_WideString *wsGraphicFilter, FS_FLOAT *fVersion);

	//************************************
	// Function:  GetPayLoadSize
	// Param[in]: wrapperDoc	The input wrapper document object.
	// Return:	The size of payload;
	// Remarks: Get the file size of the uncompressed encrypted payload document.
	// Notes:	if no "size" key, return -1;
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3.1
	//************************************
	static FS_INT64 GetPayLoadSize(FPD_WrapperDoc wrapperDoc);

	//************************************
	// Function:  GetPayloadFileName
	// Param[in]: wrapperDoc	The input wrapper document object.
	// Param[out]: wsFileName	The file name for encrypted payload documents.
	// Return:	TRUE if success, otherwise return FALSE.
	// Remarks:	Get the file name for encrypted payload documents which shall include the name of the
	//			cryptographic filter needed to decrypt the document.
	// Notes:	The name must not contain or be derived from the encrypted payload's actual file name.
	//			This is to avoid potential disclosure of sensitive information in the original filename.
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3.1
	//************************************
	static FS_BOOL GetPayloadFileName(FPD_WrapperDoc wrapperDoc, FS_WideString *wsFileName);

	//************************************
	// Function:   StartGetPayload
	// Param[in]: wrapperDoc	The input wrapper document object.
	// Param[out]: pPayload		The embedded encrypted payload document's file stream.
	// Param[in]:  pPause		The user-supplied pause object.
	// Return:	   TRUE, to be continued, otherwise, finished.
	// Remarks:	   Get the embedded encrypted payload document's file stream.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3.1
	//************************************
	static FS_BOOL StartGetPayload(FPD_WrapperDoc wrapperDoc, FS_FileWriteHandler *pPayload, FS_PauseHandler pPause);

	//************************************
	// Function:   Continue
	// Param[in]: wrapperDoc	The input wrapper document object.
	// Param[in]:  pPause		The user-supplied pause object.
	// Return:	   Negative value if failure, 0 if finishes, and positive value if need to be continued.
	// Remarks:	   Continue getting the embedded encrypted payload document's file stream.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3.1
	//************************************
	static FS_INT32 Continue(FPD_WrapperDoc wrapperDoc, FS_PauseHandler pPause);
};

//////////////////////////////////////////////////////////////////////////////////////////////////////
/*									CFPD_3dContext_V12												*/
//////////////////////////////////////////////////////////////////////////////////////////////////////

class CFPD_3dContext_V12
{
public:
	static FPD_3dContext New(FPD_Document fpdDoc);
	static void Destroy(FPD_3dContext context);

	static FS_BOOL InitDefaultEngine(FPD_3dContext contex);
	static FS_BOOL InitializeWithEngine(FPD_3dContext contex, FPD_3deRuntime pEngine);
	static void RegisterFormatLoader(FPD_3dContext contex, FS3DE_3DArtworkFormat format, FPD_Vendor_ModelLoaderCallback loaderCallback);
	static FPD_Document GetDocument(FPD_3dContext contex);
	static FPD_3deRuntime GetEngine(FPD_3dContext contex);
	static FS_BOOL IsPDF20(FPD_3dContext contex);
	static FPD_3dAnnotData LoadAnnotData(FPD_Object pPageDict, FPD_Object pAnnotDict, FPD_3dContext contex);
	static void	RegisterScriptProvider(FPD_3dContext contex, FPD_3DScriptProvider pHostProvider);
	static void RegisterCompositionProvider(FPD_3dContext contex, FPD_3DCompositionProvider pCompositionProvider);
};

class CFPD_3dAnnotData_V12
{
public:
	static FPD_3dAnnotData_3dArtwork As3DArtwork(FPD_3dAnnotData annotData);
	static FPD_3dAnnotData_RichMedia AsRichMedia(FPD_3dAnnotData annotData);
	static void Release(FPD_3dAnnotData annotData);
};

class CFPD_3dAnnotData3dArtwork_V12
{
public:
	static void Deactivate(FPD_3dAnnotData_3dArtwork artwork, FS3DE_ActivationState state);
	static FPD_3dScene GetScene(FPD_3dAnnotData_3dArtwork artwork);
	static FPD_3deCanvas ActivateAndAllocCanvasEmbedded(FPD_3dAnnotData_3dArtwork artwork, FS3DE_ActivationState param, void* pParentNativeWindow);
	static FPD_3deCanvas AllocCanvasEmbedded(FPD_3dAnnotData_3dArtwork artwork, void* pParentNativeWindow);
	static void GetActivationOptions(FPD_3dAnnotData_3dArtwork artwork, PFPDF3D_ActivationOptions poutOptions);
	static void SetActivationOptions(FPD_3dAnnotData_3dArtwork artwork, PFPDF3D_ActivationOptions options);
	static FS_BOOL LoadActivationOptions(FPD_3dAnnotData_3dArtwork artwork, FPD_Object pAnnotDict, PFPDF3D_ActivationOptions poutOptions);
	static void ReleaseCanvas(FPD_3dAnnotData_3dArtwork artwork, FPD_3deCanvas canvas);
	static FPD_3deAsset	  GetAsset(FPD_3dAnnotData_3dArtwork artwork);
};

class CFPD_3dScene_V12
{
public:	
	static FPD_3deAsset	GetAssetForAnnotData(FPD_3dScene scene,const FPD_3dAnnotData annotData);
	static FPD_3DArtworkFormat GetFormat(FPD_3dScene scene);	
	static FS_BOOL RetrievePresetViewList(FPD_3dScene scene, FS_PtrArray p3deViewArray);
	static FPD_3deView GetDefaultPresetView(FPD_3dAnnotData annotData);
	static void UpdatePresetViewList(FPD_3dScene scene, FS_PtrArray p3deViewArray);
	static FPD_3deView CreatePresetView(FPD_3dScene scene, FS_LPCWSTR lpwsDisplayName);	
};


class CFPD_3deAsset_V12
{
public:	
	static FPD_H3DE_AssetNode GetParentNode(FPD_3deAsset asset, FPD_H3DE_AssetNode *nodeCurrent);
	static FPD_H3DE_AssetNode GetNextSiblingNode(FPD_3deAsset asset, FPD_H3DE_AssetNode *nodeCurrent);
	static FPD_H3DE_AssetNode GetFirstChildNode(FPD_3deAsset asset, FPD_H3DE_AssetNode *nodeCurrent);
	static void GetNodeName(FPD_3deAsset asset, FPD_H3DE_AssetNode *node, FS_WideString *wsName);
	static void NarrowScope(FPD_3deAsset asset, FPD_H3DE_AssetNode *node, FS_BOOL bOnlyItsChildren);
	static FPD_H3DE_AssetNode GetRootNode(FPD_3deAsset asset);
	static FS3DE_USIZE	GetNodeMetaDataCount(FPD_3deAsset asset, FPD_H3DE_AssetNode *node);
	static void	GetNodeMetaDataItem(FPD_3deAsset asset, FPD_H3DE_AssetNode *node, FS3DE_USIZE index, FS_WideString *wsKey, FS_WideString *wsValue);
};

class CFPD_3deRuntime_V12
{
public:
	static FPD_3deRuntime CreateRuntime();
	static FPD_3deRuntime CreateRuntimeWithBackend(FS_DWORD dwBackendId);
	static FPD_3deAsset CreateAsset(FPD_3deRuntime runtime);
	static void Tick(FPD_3deRuntime runtime);
	static FS3DE_USIZE CountInstantiatedCanvases(FPD_3deRuntime runtime);
	static void QueryAvailableBackendList(FS_DWordArray rgBackends);
	static void GetBackendFriendlyName(FS_DWORD dwBackendId, FS_WideString wsoutName);
	static void UpdateTextProvider(FPD_3deRuntime p3dEngine, FPD_3deTextProvider pTextProvider);
	static void UpdatePreference_AngleUnitDisplay(FPD_3deRuntime p3dEngine, FS_WideString wsDegreeUnitText, FS_WideString wsRadianUnitText);
	static void UpdatePreference_NodeMetadataStoragePath(FPD_3deRuntime p3dEngine, FS_LPCSTR lpszNodeMetadataStoragePath);
	static void UpdateProvider_I18n(FPD_3deRuntime p3dEngine, FPD_3DEI18nProvider pProvider);
	static void UpdatePreference_AntiAliasing(FPD_3deRuntime p3dEngine, const FS_WordArray rgPreferenceSequence);
	static void QueryPreference_ForceDoubleSidedRendering(FPD_3deRuntime p3dEngine, FS_BOOL* bForce);
};

class CFPD_3deCanvas_V12
{
public:
	static void* GetNativeWindow(FPD_3deCanvas canvas);
	static void	Instantiate(FPD_3deCanvas canvas);
	static void	InstantiateAndLive(FPD_3deCanvas canvas);
	static void	Uninstantiate(FPD_3deCanvas canvas);
	static void GetState(FPD_3deCanvas canvas);
	static FPD_3deView AcquireCurrentView(FPD_3deCanvas canvas);
	static void ReleaseCurrentView(FPD_3deCanvas canvas, FPD_3deView pView);
	static FPD_3deAsset GetPrimaryAsset(FPD_3deCanvas canvas);
	static void SetControllerTool(FPD_3deCanvas canvas, FPD_3deCanvas_ControllerTool pTool);
	static void RelocateInPlace(FPD_3deCanvas canvas, FS_Rect rect);

	static void SetNotificationCallback_ContextMenu(FPD_3deCanvas canvas,
		FPD_NotificationCallback_ContextMenu pCallback, FS_LPVOID pUserData);	
	static void SetNotificationCallback_PointerFocus(FPD_3deCanvas canvas,
		FPD_NotificationCallback_PointerFocusEnter pEnterCallback,
		FPD_NotificationCallback_PointerFocusLost pLostCallback, FS_LPVOID pUserData);

	static FS_BOOL HaveAnimation(FPD_3deCanvas canvas);
	static FS_BOOL IsAnimationPlaying(FPD_3deCanvas canvas);
	static void ToggleAnimation(FPD_3deCanvas canvas, FS_BOOL bPlaying);
	static void SeekAnimationProgress(FPD_3deCanvas canvas, FS_FLOAT fPercentage);
	static FS_FLOAT GetAnimationProgress(FPD_3deCanvas canvas);

	static FPD_H3DE_AssetNode GetChosenNode(FPD_3deCanvas canvas);
	static void SetChosenNode(FPD_3deCanvas canvas, FPD_H3DE_AssetNode hChosenNode);
	static void SetNotificationCallback_ClaimMeasure(FPD_3deCanvas canvas,
		FPD_NotificationCallback_ClaimMeasure pCallback, FS_LPVOID pUserData);
	static FPD_3deMeasure GetChosenMeasure(FPD_3deCanvas canvas);
	static void SetChosenMeasure(FPD_3deCanvas canvas, FPD_3deMeasure measurechosen);
	static FS_BOOL Snapshot(FPD_3deCanvas canvas, FS_DIBitmap* bmpDIBitmap);
};

class CFPD_3deView_V12
{
public:	
	static void GetName(FPD_3deView view, FS_WideString *wsName);
	static void GetDisplayName(FPD_3deView view, FS_WideString *wsDisplayName);
	static FS3DE_Coordinate* GetCenterOfOrbit(FPD_3deView view);
	static FPD_3deViewProjection GetProjection(FPD_3deView view);
	static FPD_3deViewBackground GetBackground(FPD_3deView view);
	static FPD_3deViewRenderMode GetRenderMode(FPD_3deView view);
	static FPD_3deViewLightingScheme GetLightingScheme(FPD_3deView view);

	static void SetCameraToWorldMatrixWithNode(FPD_3deView view, FS_LPVOID pNodePathData);
	static void SetCenterOfOrbit(FPD_3deView view, FS3DE_Coordinate fDist);
	static void SetProjection(FPD_3deView view, FPD_3deViewProjection pProjection);
	static void SetBackground(FPD_3deView view, FPD_3deViewBackground bgBackground);
	static void SetRenderMode(FPD_3deView view, FPD_3deViewRenderMode rmRenderMode);
	static void SetLightingScheme(FPD_3deView view, FPD_3deViewLightingScheme lsLightingScheme);

	static void ImportViewConfig(FPD_3deView view, FPD_3deView source, FS3DE_ViewConfigSet options);
	
	static void SetNameAndDisplayName(FPD_3deView view, FS_LPCWSTR wsName, FS_LPCWSTR wsDisplayName);
	static void SetCrossSections(FPD_3deView view,	FS_PtrArray fsViewCrossSectionArray);

	static void ClearViewConfig(FPD_3deView view, FS3DE_ViewConfigSet cfgConfigSet);

	static FS_BOOL GetCrossSections(FPD_3deView view, FS_PtrArray *fsViewCrossSectionArray);

	static void ClearAndImportViewConfig(FPD_3deView view, FPD_3deView pSrcView, FS3DE_ViewConfigSet cfgConfigSet);

	static FS_BOOL GetMeasurements(FPD_3deView view, FS_PtrArray *fsViewMeasurementsArray);
	static void SetMeasurements(FPD_3deView view, FS_PtrArray vecMeasurements);
	static void SetTrackingView(FPD_3deView view, FPD_3deView pTrackingView);
	static FPD_Object F3DViewGetDict(FPD_3deView view);
};

class CFPD_3deViewRenderMode_V12
{
public:
	static void SwitchKind(FPD_3deViewRenderMode viewRender, FS3DE_RenderModeType eType);
	static FPD_3deViewRenderMode Create(FS3DE_RenderModeType eType,
		FS_COLORREF fActiveColor, FS_BOOL bFaceColorUseBG, FS_COLORREF fFaceColor,
		FS_FLOAT fOpacity, FS_FLOAT fCreaseValueAngle);
	static void Release(FPD_3deViewRenderMode renderMode);
	static FS3DE_RenderModeType QueryModeType(FPD_3deViewRenderMode renderMode);
};

class CFPD_3deViewLightingScheme_V12
{
public:

	static FPD_3deViewLightingScheme Create(FS_BOOL bConfigured, FS3DE_LightingSchemeType eTypeTag);
	static void Destroy(FPD_3deViewLightingScheme scheme);

	static FS3DE_LightingSchemeType QuerySchemeType(FPD_3deCanvas pCanvas);
	static void SwitchSchemeType(FPD_3deCanvas pCanvas, FS3DE_LightingSchemeType eType);
	static FS_BOOL QueryArtworkSchemeExist(FPD_3deCanvas pCanvas);
};

class CFPD_3deCanvasControllerTool_V12
{
public:
	static FPD_3deCanvas_ControllerTool GetRotateTool();
	static FPD_3deCanvas_ControllerTool GetSpinTool();
	static FPD_3deCanvas_ControllerTool GetPanTool();
	static FPD_3deCanvas_ControllerTool GetZoomTool();
	static FPD_3deCanvas_ControllerTool GetWalkTool();
	static FPD_3deCanvas_ControllerTool GetFlyTool();
	static FPD_3deCanvas_ControllerTool GetSimpleControllerTool(FS3DE_BuiltinSimpleControllerTool eToolType);
	static FPD_3deCanvas_ControllerTool CreateCommentControllerTool();
	static void	ReleaseCommentControllerTool(FPD_3deCanvas_ControllerTool tool);
	static FPD_3deCanvas_ControllerTool CreateMeasureControllerTool(FS3DE_MeasureType eMeasureType);
	static void	ReleaseMeasureControllerTool(FPD_3deCanvas_ControllerTool tool);
	static void SetMeasureControllerTooMeasureType(FPD_3deCanvas_ControllerTool tool, FS3DE_MeasureType eMeasureType);
	static void SetMeasureScale(FPD_3deCanvas_ControllerTool tool, FS_FLOAT fM, FS_FLOAT fN, const FS_WideString wsUnit);	
};

class CFPD_3dVendor_V12
{
public:
	static FPD_3dVendorHandler CreateHandler(FPD_3dVendor_TempFileCallback pTempFileCallback);
	static void ReleaseHandler(FPD_3dVendorHandler pHandler);
	static void SetPDFModuleMgrIfNecessary(void *pModuleMgr);
	static void SetTempFileCallback(FPD_3dVendorHandler pHandler);
	static FPD_3deAsset ModelLoader(FPD_Object pStream, FPD_3DArtworkFormat eFormat);
	static FPD_3deAsset PRCLoader(FPD_Object pStream, FPD_3DArtworkFormat eFormat, FPD_3deRuntime p3DEngine);
	static void GetNodeMetadataStorageKey(FS_ByteString *bsStorageKey);
};

class CFPD_3deViewBackground_V12
{
public:
	static FPD_3deViewBackground Create(FS_COLORREF fBkgColor, FS_BOOL bEntireAnnotation);
	static void Release(FPD_3deViewBackground viewBkg);
	static void SetBackgroundColor(FPD_3deViewBackground viewBkg, FS_COLORREF fBackgroundColor);
	static FS_COLORREF QueryBackgroundColor(FPD_3deViewBackground viewBkg);
	static FS_COLORREF DefaultBackgroundColor();
	static FS_BOOL QueryEntireAnnotation(FPD_3deViewBackground viewBkg);
};

class CFPD_3deViewCameraParam_V12
{
public:
	static FS3DE_ViewProjectionMode QueryProjectionMode(FPD_3deCanvas canvas);
	static void SwitchProjectionMode(FPD_3deCanvas canvas, FS3DE_ViewProjectionMode eProjectionMode);
	static void ImportCameraConfig1(FPD_3deCanvas canvas, FS3DE_CameraConfig1 cfgCameraConfig);
	static void ImportCameraConfig2(FPD_3deCanvas canvas, FS3DE_CameraConfig2 cfgCameraConfig);
	static void ExportCameraConfig(FPD_3deCanvas canvas, FS3DE_CameraConfig1* pConfig1, FS3DE_CameraConfig2* pConfig2);
	static void FitToVisible(FPD_3deCanvas canvas);
	static void ZoomToPart(FPD_3deCanvas canvas, FPD_H3DE_AssetNode hAssetNode);
	static void IsolateNode(FPD_3deCanvas canvas, FPD_H3DE_AssetNode hAssetNode);
	static void IsolateParentNode(FPD_3deCanvas canvas, FPD_H3DE_AssetNode hAssetNode);
};

class CFPD_3deViewCrossSection_V12
{
public:
	static FS_BOOL ToggleCrossSection(FPD_3deCanvas canvas);
	static void	ImportCrossSectionConfig(FPD_3deCanvas canvas, PFS3DE_CrossSectionConfig sectionConfig);
	static void ExportCrossSectionConfig(FPD_3deCanvas canvas, FS3DE_CrossSectionConfig *outSectionConfig);
	static void GetCenterPos(FPD_3deViewCrossSection section, FS3DE_Vector *pCenterPos);
	static void SetCenterPos(FPD_3deViewCrossSection section, FS3DE_Vector CenterPos);
	static FS_BOOL GetOrientation(FPD_3deViewCrossSection section, FS3DE_Vector *pvOrientationAngles, FSInitialAxis *eInitialAxis);
	static void SetOrientation(FPD_3deViewCrossSection section, FS3DE_Vector vOrientationAngles, FSInitialAxis eInitialAxis);
	static void GetPlane(FPD_3deViewCrossSection section, FS_BOOL *pbVisible, FS_COLORREF *pcColor, FS_FLOAT *pfOpacity);
	static void SetPlane(FPD_3deViewCrossSection section, FS_BOOL bVisible, FS_COLORREF cColor, FS_FLOAT fOpacity);
	static void GetIntersection(FPD_3deViewCrossSection section, FS_BOOL* pbVisible, FS_COLORREF *pcColor);
	static void SetIntersection(FPD_3deViewCrossSection section, FS_BOOL bVisible, FS_COLORREF cColor);
	static FS_BOOL GetShowTransparent(FPD_3deViewCrossSection section);
	static void	SetShowTransparent(FPD_3deViewCrossSection section, FS_BOOL ShowTransparent);
	static FS_BOOL GetSectionCapping(FPD_3deViewCrossSection section);
	static void	SetSectionCapping(FPD_3deViewCrossSection section, FS_BOOL SectionCapping);
	static FS_BOOL GetIgnoreChosenNode(FPD_3deViewCrossSection section);
	static void	SetIgnoreChosenNode(FPD_3deViewCrossSection section, FS_BOOL IgnoreChosenNode);
};

class CFPD_3deViewNode_V12
{
public:
	static FPD_3deViewNode StartModifyNode(FPD_3deCanvas pCanvas, FPD_H3DE_AssetNode hNode);
	static void FinishModifyNode(FPD_3deCanvas pCanvas, FPD_3deViewNode pViewNode);
	static FPD_3deViewNode StartQueryNode(FPD_3deCanvas pCanvas, FPD_H3DE_AssetNode hNode);
	static void FinishQueryNode(FPD_3deCanvas pCanvas, FPD_3deViewNode pViewNode);

	static void GetName(FPD_3deViewNode pViewNode, FS_WideString *wsName);
	static void	SetName(FPD_3deViewNode pViewNode, FS_WideString wsName);
	static FS_BOOL*	GetVisibility(FPD_3deViewNode pViewNode);
	static void SetVisibility(FPD_3deViewNode pViewNode, FS_BOOL bVisible);
	static FS_FLOAT* GetOpacity(FPD_3deViewNode pViewNode);
	static void	SetOpacity(FPD_3deViewNode pViewNode, FS_FLOAT fOpacity);
	static FS3DE_3DMatrix GetMatrix(FPD_3deViewNode pViewNode);
	static void	SetMatrix(FPD_3deViewNode pViewNode, FS3DE_3DMatrix mMatrix);
	static FPD_3deViewRenderMode GetRenderMode(FPD_3deViewNode pViewNode);
	static void	SetRenderMode(FPD_3deViewNode pViewNode, FPD_3deViewRenderMode rmRenderMode);
};


class CFPD_3deMeasure_V12
{
public:
	static  FS3DE_MeasureType GetType(FPD_3deMeasure measure);
	static  FPD_3deView	GetView(FPD_3deMeasure measure);
	static  FS_BOOL GetName(FPD_3deMeasure measure, FS_WideString* outName) ;
	static  FS_BOOL	GetPlane(FPD_3deMeasure measure, FS3DE_Vector* vec3d) ;
	static  FS_BOOL GetAnchorPoints(FPD_3deMeasure measure, FS_PtrArray vecAnchorPoints);
	static  void	ReleaseFS3DeVectorArray(FS_PtrArray vecArray);
	static FS_BOOL GetAnchorNames(FPD_3deMeasure measure, FS_WideStringArray vecAnchorNames);	
	static FS_BOOL GetExtraAnchorPoints(FPD_3deMeasure measure,FS_PtrArray vecAnchorPoints);
	static FS_BOOL GetDirVectors(FPD_3deMeasure measure, FS_PtrArray vecDirVectors) ;
	static FS_BOOL		GetTextPos(FPD_3deMeasure measure, PFS3DE_Vector p3devec);
	static FS_BOOL		GetTextXDir(FPD_3deMeasure measure, PFS3DE_Vector p3devec);
	static FS_BOOL		GetTextYDir(FPD_3deMeasure measure, PFS3DE_Vector p3devec);
	static FS_BOOL		GetTextBoxSize(FPD_3deMeasure measure, FS_FLOAT& x, FS_FLOAT& y);
	static FS_BOOL			   GetExtensionLineLength(FPD_3deMeasure measure,FS_FLOAT& length);
	static FS_BOOL					   GetTextSize(FPD_3deMeasure measure,FS_FLOAT& size);
	static FS_BOOL		GetColor(FPD_3deMeasure measure,FS3DE_RGBColor* color);
	static FS_FLOAT				   GetValue(FPD_3deMeasure measure);
	static FS_WideString		   GetUnits(FPD_3deMeasure measure);
	static FS_UINT		   GetPrecision(FPD_3deMeasure measure);
	static FS_WideString		   GetUserText(FPD_3deMeasure measure);
	static FS_BOOL   GetAngleType(FPD_3deMeasure measure,FS3DE_MeasureAngleType& angleType);
	static FS_BOOL				   GetShowCircle(FPD_3deMeasure measure);
	static FS_BOOL  GetRadialType(FPD_3deMeasure measure,FS3DE_MeasureRadialType& radicalType);
	static FS_BOOL	SetName(FPD_3deMeasure measure, FS_LPCWSTR lpwsStr);
	static void  SetTextPos(FPD_3deMeasure measure, FS3DE_Vector vNewPos);
	static void  SetAnchorPointsAndNames(FPD_3deMeasure measure, const FS_PtrArray vecAnchorPoints, const FS_WideStringArray vecAnchorNames);
	static void  SetPlane(FPD_3deMeasure measure, const FS3DE_Vector vPlaneNoraml);
	static void SetTextXDir(FPD_3deMeasure measure, const FS3DE_Vector vTXDir);
	static void SetTextYDir(FPD_3deMeasure measure, const FS3DE_Vector vTYDir);
	static void SetValue(FPD_3deMeasure measure, FS_FLOAT fValue);
	static void SetColor(FPD_3deMeasure measure, const FS3DE_RGBColor cColor);
	static void SetAngularShowArc(FPD_3deMeasure measure, FS_BOOL bShow);
	static void SetDirVectors(FPD_3deMeasure measure, const FS_PtrArray vecDirVectors);
	static void SetUserText(FPD_3deMeasure measure, const FS_WideString wsUT);
	static void SetShow(FPD_3deMeasure measure, FS_BOOL bShow);
	static void SetTextBoxSize(FPD_3deMeasure measure, FS_FLOAT x, FS_FLOAT y);
	static void SetUnits(FPD_3deMeasure measure, const FS_WideString wsUnits);
	static FPD_3deMeasure ControllerGetCurrentMeasure(FPD_3deCanvas_ControllerTool pTool);
	static FS3DE_USIZE GetCurrentCollectedPointCount(FPD_3deCanvas_ControllerTool pTool);
	static void SetMeasureValueChangeCallback(FPD_MeasureValueChangeCallback pCallback, FS_LPVOID pUserData);
	static FPD_Object Measure_GetDict(FPD_3deMeasure measure);
};

class CFPD_3deViewMiscOptions_V12
{
public:
	static void SetHighlightColor(FPD_3deViewMiscOptions options, FPD_3deCanvas pCanvas, FS3DE_RGBColor cColor);
	static void SetBoundingBoxVisible(FPD_3deViewMiscOptions options, FPD_3deCanvas pCanvas, FS_BOOL bVisible);
	static void SetBoundingBoxColor(FPD_3deViewMiscOptions options, FPD_3deCanvas pCanvas,  FS3DE_RGBColor cColor);
	static void SetShowGrid(FPD_3deViewMiscOptions options, FPD_3deCanvas pCanvas, FS_BOOL bShow);
	static FS_BOOL		  GetShowGrid(FPD_3deViewMiscOptions options, FPD_3deCanvas pCanvas);
	static void			  SetGridMode(FPD_3deViewMiscOptions options, FPD_3deCanvas pCanvas, FS3DE_GridMode eGridMode);
	static FS_BOOL			GetGridMode(FPD_3deViewMiscOptions options, FPD_3deCanvas pCanvas, FS3DE_GridMode& eGridMode);
	static void			  SetGridSize(FPD_3deViewMiscOptions options, FPD_3deCanvas pCanvas, FS_INT32 nGridSize);
	static FS_BOOL			  GetGridSize(FPD_3deViewMiscOptions options, FPD_3deCanvas pCanvas,FS_INT32& nGridSize);
};


class CFPD_CertStore_V13
{
public:
	static FPD_CertStore New(void);
	static void Destroy(FPD_CertStore certStore);
	static FS_LPVOID  GetHCertStore(FPD_CertStore certStore);
	static FS_INT32  LoadMYSystemCerts(FPD_CertStore certStore, const FS_BOOL& bOnlyUsedForSign);
	static FS_LPVOID GetCertFromCertSNId(FPD_CertStore certStore, FS_ByteString csCertID);
	static void GetCertSNID(const FS_LPVOID pCert, FS_ByteString* bsSNID);
	static FS_DWORD GetCertCount(FPD_CertStore certStore);
	static void GetCertsArray(FPD_CertStore certStore, FS_PtrArray* pArray);
	static FS_BOOL IsPrivateKeyCert(FPD_CertStore certStore, FS_LPVOID pCertContext);
};

class CFPD_3deTextProvider_V12
{
public:
	static FPD_3deTextProvider Create();
	static void Release(FPD_3deTextProvider pProvider);
	static void	 SetTextScalingRatio(FPD_3deTextProvider pProvider,FS_FLOAT fTextScalingRatio);
};

class CFPD_Script3DHostProviderHandler : public IPDFScript3D_HostProvider
{
public:
	CFPD_Script3DHostProviderHandler(FPD_ScriptHostProviderCallbacksRec* pProvider);
	virtual FX_BOOL Host(CPDF_Document* pDoc, FXJSE_HVALUE hValue);
	virtual FX_BOOL ConsolePrint(FX_BSTR bsText);
	virtual FX_BOOL ConsolePrintLn(FX_BSTR bsText);
	virtual void NotifyScriptExecutionStart(CPDF_Document* pDoc);
	virtual void NotifyScriptExecutionFinish(CPDF_Document* pDoc);

	FPD_ScriptHostProviderCallbacksRec m_Provider;
};

class CFPD_ScriptHostHostProvider_V12
{
public:
	static FPD_Script3DHostProvider New(FPD_ScriptHostProviderCallbacks callbacks);
	static void Destroy(FPD_Script3DHostProvider provider);
};


class CFPD_Script3DEngine_V12
{
public:
	static FPD_Script3D_Engine New(FSJSE_HRUNTIME hRuntime);
	static void Destroy(FPD_Script3D_Engine engine);
	static FSJSE_HCONTEXT	Get3DScriptContext(FPD_Script3D_Engine engine, FPD_3dAnnotData_3dArtwork pAnnotData);
	static void RegisterHostProvider(FPD_Script3D_Engine engine, FPD_Script3DHostProvider pHostProvider);
	static FPD_3DScriptProvider Get3DScriptProvider(FPD_Script3D_Engine engine);
};

class CFPD_I18NProviderHandler : public I3DE_I18nProvider
{
public:
	CFPD_I18NProviderHandler(FPD_I18nProviderCallbacksRec* pProvider);
	virtual void FormatNumberWithPrecision(CFX_WideString& wsResult, FX_FLOAT fNumber, FX3DE_USIZE nPrecision);

	FPD_I18nProviderCallbacksRec m_Provider;
};

class CFPD_3DI18NProviderHandler_V12
{
public:
	static FPD_3DEI18nProvider New(FPD_I18nProviderCallbacks callbacks);
	static void Destroy(FPD_3DEI18nProvider provider);
};


class CFPD_3DCompositionProvider: public IPDF3D_CompositionProvider
{
public:
	CFPD_3DCompositionProvider(FPD_3DCompositionProviderCallbacksRec* pProvider);
	virtual void GenerateBackground(CFX_DIBitmap& bmpBitmap, CPDF3D_AnnotData* pAnnotData, I3DE_Canvas* pCanvas);
	virtual FX_BOOL GenerateForeground(CFX_DIBitmap& bmpBitmap,	CPDF3D_AnnotData* pAnnotData, I3DE_Canvas* pCanvas,	I3DE_ViewOverlay* pViewOverlay);

	FPD_3DCompositionProviderCallbacksRec m_Provider;
};

class CFPD_3DCompositionProvider_V13
{
public:
	static FPD_3DCompositionProvider New(FPD_3DCompositionProviderCallbacks callbacks);
	static void Destroy(FPD_3DCompositionProvider provider);
};


class CFPD_PageLabel_V13
{
public:	
	 //************************************
	 // Function:  New
	 // Param[in]: pDocument			The input PDF document.
	 // Return:    FPD_PageLabel.
	 // Remarks:   Construct from a PDF document.
	 // Notes:
	 // Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	 //************************************
	static FPD_PageLabel New(FPD_Document pDocument);

	//************************************
	// Function:  Destroy 
	// Param[in]: pagelabel			The input object CPDF_PageLabel.
	// Return:	  void
	// Remarks:   Destroys the pagelabel created by <a>FPDDocNew</a>.
	// Notes:	
	 // Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static void	Destroy(FPD_PageLabel pagelabel);
	
	//************************************
	// Function:  GetLabel
	// Param[in]: pagelabel			The input object CPDF_PageLabel.
	// Param[in]: nPage				Specifies the zero-based index of the page.
	// Param[out]: wsLabel			 FS_WideString type of page label.
	// Return:    FS_WideString type of page label.
	// Remarks:   Get page label full string.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static void					GetLabel(FPD_PageLabel pagelabel, FS_INT32 nPage,FS_WideString* wsLabel);

	//************************************
	// Function:  GetPageByLabel
	// Param[in]: pagelabel			The input object CPDF_PageLabel.
	// Param[in]: bsLabel			Specifies the page label.
	// Return:    Get the page index of specified page label.
	// Remarks:  
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static FS_INT32						GetPageByLabel(FPD_PageLabel pagelabel, FS_LPCBYTE bsLabel);
	
	//************************************
	// Function:  GetPageByLabel2
	// Param[in]: pagelabel			The input object CPDF_PageLabel.
	// Param[in]: wsLabel			Specifies the page label.
	// Return:     Get the page index of specified page label.
	// Remarks:  
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static FS_INT32						GetPageByLabel2(FPD_PageLabel pagelabel, FS_LPCWSTR wsLabel);
};

#ifdef __cplusplus
};
#endif

#endif//FPD_DOCIMPL_H
