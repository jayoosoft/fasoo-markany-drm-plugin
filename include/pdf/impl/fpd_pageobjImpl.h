﻿/** @file fpd_pageobj.h.
 * 
 *  @brief defined  page objects.
 */


#ifndef FPD_PAGEOBJIMPL_H
#define FPD_PAGEOBJIMPL_H

#ifndef FS_INTERNALINC_H
#include "../../basic/fs_internalInc.h"
#endif

#ifndef FPD_PAGEOBJEXPT_H
#include "../fpd_pageobjExpT.h"
#endif

#ifndef	FPD_RESOURCEEXPT_H
#include "../fpd_resourceExpT.h"
#endif

#ifndef	FPD_PAGEEXPT_H
#include "../fpd_pageExpT.h"
#endif

#include "../../basic/fs_basicExpT.h"
#include "../../basic/fs_basicCalls.h"

#ifdef __cplusplus
extern "C" {
#endif//__cplusplus
	
class CFPD_Path_V1
{	
public:
	//************************************
	// Function: New
	// Return:   A new empty PDF path data object.
	// Remarks:  Creates a new empty PDF path data object.
	// Notes:	
	//************************************
	static FPD_Path	New(void);

	//************************************
	// Function:  Destroy
	// Param[in]: path The input PDF path data object.
	// Return:    void
	// Remarks:   Destroys the PDF path data object.
	// Notes:
	//************************************
	static void	Destroy(FPD_Path path);


	//************************************
	// Function:  GetPointCount
	// Param[in]: path	The input PDF path data object.
	// Return:    The point count int the path.
	// Remarks:   Gets the point count int the path.
	// Notes:
	//************************************
	static FS_INT32	GetPointCount(FPD_Path path);

	//************************************
	// Function:  GetFlag
	// Param[in]: path		The input PDF path data object.
	// Param[in]: index		Specifies the zero-based index of path point in the path.
	// Return:    The flag of specified path point. 
	// Remarks:   Gets the flag of specified path point.
	// Notes:
	//************************************
	static FS_INT32	GetFlag(FPD_Path path, FS_INT32 index);

	//************************************
	// Function:  GetPointX
	// Param[in]: path		The input PDF path data object.
	// Param[in]: index		Specifies the zero-based index of path point in the path.
	// Return:	  The x-coordinate of specified path point.
	// Remarks:   Gets the x-coordinate of specified path point.
	// Notes:
	//************************************
	static FS_FLOAT	GetPointX(FPD_Path path, FS_INT32 index);

	//************************************
	// Function:  GetPointY
	// Param[in]: path		The input PDF path data object.
	// Param[in]: index		Specifies the zero-based index of path point in the path.
	// Return:    The y-coordinate of specified path point.
	// Remarks:   Gets the y-coordinate of specified path point.
	// Notes:
	//************************************
	static FS_FLOAT	GetPointY(FPD_Path path, FS_INT32 index);

	//************************************
	// Function:  GetPoint
	// Param[in]: path		The input PDF path data object.
	// Param[in]: index		The point index in the points array.
	// Return:    The path point in the path points array by the index.
	// Remarks:   Gets the path point in the path points array by the index.
	// Notes:
	//************************************
	static FS_PATHPOINT	GetPoint(FPD_Path path, FS_INT32 index);

	//************************************
	// Function:  GetBoundingBox
	// Param[in]: path	The input PDF path data object.
	// Return:    The bounding box of all control points.
	// Remarks:   Gets bounding box of all control points.
	// Notes:     The result can be used as the bounding box of the whole filled path.
	//            However, when path is stroked using geometry pen, the actual bounding box can be much larger.
	//************************************
	static FS_FloatRect	GetBoundingBox(FPD_Path path);

	//************************************
	// Function:  GetBoundingBox2
	// Param[in]: path			The input PDF path data object.
	// Param[in]: lineWidth		The line width used in stroking.
	// Param[in]: miterLimit	The miter limit value for line joint in stroking.
	// Return:    The bounding box for stroked path.
	// Remarks:   Calculates bounding box (guaranteed to contain all path, may be larger) for stroked path.
	// Notes:
	//************************************
	static FS_FloatRect	GetBoundingBox2(FPD_Path path, FS_FLOAT lineWidth, FS_FLOAT miterLimit);

	//************************************
	// Function:  Transform
	// Param[in]: path			The input PDF path data object.
	// Param[in]: matrix		The input matrix used to transform.
	// Return:    void
	// Remarks:   Transforms this path.
	// Notes:
	//************************************
	static void	Transform(FPD_Path path, FS_AffineMatrix matrix);

	//************************************
	// Function:  Append
	// Param[in]: path		The input PDF path data object.
	// Param[in]: src		The source path.
	// Param[in]: pMatrix	The specified matrix. <a>NULL</a> means no transformation.
	// Return:    void
	// Remarks:   Appends a path. Optionally a matrix can be specified to transform the source path before appending.
	// Notes:
	//************************************
	static void	Append(FPD_Path path, FPD_Path src, FS_AffineMatrix* pMatrix);

	//************************************
	// Function:  AppendRect
	// Param[in]: path			The input PDF path data object.
	// Param[in]: left			The x-coordinate of the left-bottom corner.
	// Param[in]: bottom		The y-coordinate of the left-bottom corner.
	// Param[in]: right			The x-coordinate of the right-top corner.
	// Param[in]: top			The y-coordinate of the right-top corner.
	// Return:    void
	// Remarks:   Appends a rectangle.
	// Notes:
	//************************************
	static void	AppendRect(FPD_Path path, FS_FLOAT left, FS_FLOAT bottom, FS_FLOAT right, FS_FLOAT top);

	//************************************
	// Function:  IsRect
	// Param[in]: path	The input PDF path data object.
	// Return:    <a>TRUE</a> if the path is actually a rectangle, otherwise not.
	// Remarks:   Tests whether the path is a actually rectangle.
	// Notes:
	//************************************
	static FS_BOOL IsRect(FPD_Path path);

	//************************************
	// Function:  SetPointCount
	// Param[in]: path		The input PDF path data object.
	// Param[in]: nPoints	The new count of path point to change.
	// Return:    void
	// Remarks:   Changes the path point count and prepares adequate allocated buffer.
	// Notes:
	//************************************
	static void SetPointCount(FPD_Path path, FS_INT32 nPoints);

	//************************************
	// Function:  SetPoint
	// Param[in]: path		The input PDF path data object.
	// Param[in]: index		Specifies the zero-based index of path point in the path.
	// Param[in]: x			The x-coordinate of the point to set.
	// Param[in]: y			The y-coordinate of the point to set.
	// Param[in]: flag		The flag of the point to set.
	// Return:    void
	// Remarks:   Sets the point data for specified path point.
	// Notes:
	//************************************
	static void SetPoint(FPD_Path path, FS_INT32 index, FS_FLOAT x, FS_FLOAT y, FS_INT32 flag);

	//************************************
	// Function:  GetModify
	// Param[in]: path		The input PDF path data object.
	// Return:    void.
	// Remarks:	  The interface helps init the object if the object is NULL.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static void GetModify(FPD_Path path);
	
	static void	TrimPoints(FPD_Path path, FS_INT32 nPoints);

};


class CFPD_ClipPath_V1
{	
public:
	//************************************
	// Function: New
	// Return:   A new empty clip path data object.
	// Remarks:  Creates a new empty clip path data object.
	// Notes:	
	//************************************
	static FPD_ClipPath	New(void);

	//************************************
	// Function:  Destroy
	// Param[in]: path The input clip path data object.
	// Return:    void
	// Remarks:   Destroys the clip path data object.
	// Notes:
	//************************************
	static void	Destroy(FPD_ClipPath path);

	//************************************
	// Function:  AppendPath
	// Param[in]: path				The input clip path data object.
	// Param[in]: pathAppendTo		The input clipping path.
	// Param[in]: type				The clip type of the input clipping path.
	// Param[in]: bAutoMerge		Whether to merge the clipping path automatically.
	// Return:    void
	// Remarks:   Appends a clipping path.
	// Notes:
	//************************************
	static void	AppendPath(FPD_ClipPath path, FPD_Path pathAppendTo, FS_INT32 type, FS_BOOL bAutoMerge);

	//************************************
	// Function:  DeletePath
	// Param[in]: path			The input clip path data object.
	// Param[in]: layerIndex	The path index to remove.
	// Return:    void
	// Remarks:   Removes a path from path list.
	// Notes:
	//************************************
	static void DeletePath(FPD_ClipPath path, FS_INT32 layerIndex);

	//************************************
	// Function:  GetPathCount
	// Param[in]: path	The input clip path data object.
	// Return:   The count of paths in the clip path.
	// Remarks:   Gets the count of paths in the clip path.
	// Notes:
	//************************************
	static FS_DWORD GetPathCount(FPD_ClipPath path);

	//************************************
	// Function:  Transform
	// Param[in]: path			The input clip path data object.
	// Param[in]: matrix		The input matrix used to transform.
	// Return:    void
	// Remarks:   Transforms this path.
	// Notes:
	//************************************
	static void	Transform(FPD_ClipPath path, const FS_AffineMatrix matrix);

	//************************************
	// Function:  GetPath
	// Param[in]: path		The input clip path data object.
	// Param[in]: index		Specifies the zero-based path index in the clip path.
	// Param[out]: outPath	It receives the path.
	// Return:    void
	// Remarks:   Gets a path.
	// Notes:
	//************************************
	static void	GetPath(FPD_ClipPath path, FS_INT32 index, FPD_Path* outPath);

	//************************************
	// Function:  GetClipType
	// Param[in]: path		The input clip path data object.
	// Param[in]: index		Specifies the zero-based path index in the clip path
	// Return:    The clip type of specified path
	// Remarks:   Gets the clip type of specified path.
	// Notes:
	//************************************
	static FS_INT32	GetClipType(FPD_ClipPath path, FS_INT32 index);

	//************************************
	// Function:  GetTextCount
	// Param[in]: path	The input clip path data object.
	// Return:    The count of text objects in the clip path.
	// Remarks:   Gets the count of text objects in the clip path.
	// Notes:
	//************************************
	static FS_DWORD	GetTextCount(FPD_ClipPath path);

	//************************************
	// Function:  GetClipBox
	// Param[in]: path	The input clip path data object.
	// Return:    The clip box of the clip path.
	// Remarks:   Gets the clip box of the clip path.
	// Notes:
	//************************************
	static FS_FloatRect	GetClipBox(FPD_ClipPath path);

	//************************************
	// Function:  GetText
	// Param[in]: path	 The input clip path data object.
	// Param[in]: i		 Specifies the zero-based text object index in the clip path.
	// Return:    A text object.
	// Remarks:   Gets a text object.
	// Notes:
	//************************************
	static FPD_PageObject GetText(FPD_ClipPath path, FS_INT32 i);

	//************************************
	// Function:  AppendTexts
	// Param[in]: path			The input clip path data object.
	// Param[in]: pTextsBuf		Pointer to clipping text objects to append.
	// Param[in]: count			The count of clipping text objects to append.
	// Return:    void
	// Remarks:   Appends clipping text objects.
	// Notes:
	//************************************
	static void AppendTexts(FPD_ClipPath path, FPD_PageObject* pTextsBuf, FS_INT32 count);

	//************************************
	// Function:  SetCount
	// Param[in]: path			The input clip path data object.
	// Param[in]: path_count	The estimated count of path in the clip path data.
	// Param[in]: text_count	The estimated count of text object in the clip path data.
	// Return:    void
	// Remarks:   Estimates the count of path and text in the clip path data, and allocate the memory.
	// Notes:
	//************************************
	static void SetCount(FPD_ClipPath path, FS_INT32 path_count, FS_INT32 text_count);

	//************************************
	// Function:  IsNull
	// Param[in]: path	The input clip path data object.
	// Return:    Non-zero means <a>NULL</a>, otherwise not <a>NULL</a>.
	// Remarks:   Tests whether the path data object is <a>NULL</a> or not.
	// Notes:
	//************************************
	static FS_BOOL IsNull(FPD_ClipPath path);

	//************************************
	// Function:  GetModify
	// Param[in]: path	The input clip path data object.
	// Return:    void.
	// Remarks:	  The interface helps init the object if the object is NULL.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static void GetModify(FPD_ClipPath path);

	//************************************
	// Function:  GetPathPointer
	// Param[in]: path		The input clip path data object.
	// Param[in]: index		Specifies the zero-based path index in the clip path.
	// Return:    The pointer to the specified path.
	// Remarks:   Gets the pointer to the specified path.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static FPD_Path	GetPathPointer(FPD_ClipPath path, FS_INT32 index);
};

class CFPD_ColorState_V1
{	
public:
	//************************************
	// Function:  New
	// Param[in]: void
	// Return:    A new empty PDF color state object.
	// Remarks:	  Creates a new empty PDF color state object.
	// Notes:
	//************************************
	static FPD_ColorState New(void);

	//************************************
	// Function:  Destroy 
	// Param[in]: clrState The input PDF color state object.
	// Return:    void
	// Remarks:   Destroys the PDF color state object.
	// Notes:
	//************************************
	static void	Destroy(FPD_ColorState clrState);

	
	//************************************
	// Function:  GetFillColor
	// Param[in]: clrState	The input PDF color state object.
	// Return:    The filling color.
	// Remarks:	  Gets the filling color.
	// Notes:
	//************************************
	static FPD_Color GetFillColor(FPD_ColorState clrState);


	//************************************
	// Function:  GetStrokeColor
	// Param[in]: clrState	The input PDF color state object.
	// Return:    The stroking color.
	// Remarks:	  Gets the stroking color.
	// Notes:
	//************************************
	static FPD_Color GetStrokeColor(FPD_ColorState clrState);


	//************************************
	// Function:  SetFillColor
	// Param[in]: clrState		The input PDF color state object.
	// Param[in]: clrSpace		The color space of the filling color.
	// Param[in]: pValue		The color component values in the specified color space.
	// Param[in]: nValues		The count of the input parameters.
	// Return:    void
	// Remarks:	  Sets the filling normal color.
	// Notes:
	//************************************
	static void SetFillColor(FPD_ColorState clrState, FPD_ColorSpace clrSpace, FS_FLOAT* pValue, FS_INT32 nValues);


	//************************************
	// Function:  SetStrokeColor
	// Param[in]: clrState		The input PDF color state object.
	// Param[in]: clrSpace		The color space of the stroking color.
	// Param[in]: pValue		The color component values in the specified color space.
	// Param[in]: nValues		The count of the input parameters.
	// Return:    void
	// Remarks:	  Sets the stroking normal color.
	// Notes:
	//************************************
	static void SetStrokeColor(FPD_ColorState clrState, FPD_ColorSpace clrSpace, FS_FLOAT* pValue, FS_INT32 nValues);


	//************************************
	// Function:  SetFillPatternColor
	// Param[in]: clrState		The input PDF color state object.
	// Param[in]: pattern		The input pattern.
	// Param[in]: pValue		The input parameters for the pattern color.
	// Param[in]: nValues		The count of the input parameters.
	// Return:    void
	// Remarks:	  Sets the filling pattern color.
	// Notes:
	//************************************
	static void SetFillPatternColor(
		FPD_ColorState clrState, 
		FPD_Pattern pattern, 
		FS_FLOAT* pValue, 
		int nValues
		);


	//************************************
	// Function:  SetStrokePatternColor
	// Param[in]: clrState		The input PDF color state object.
	// Param[in]: pattern		The input pattern.
	// Param[in]: pValue		The input parameters for the pattern color.
	// Param[in]: nValues		The count of the input parameters.
	// Return:    void
	// Remarks:	  Sets the stroking pattern color.
	// Notes:
	//************************************
	static void SetStrokePatternColor(
		FPD_ColorState clrState, 
		FPD_Pattern pattern, 
		FS_FLOAT* pValue, 
		int nValues
		);

	//************************************
	// Function:  IsNull
	// Param[in]: clrState		The input PDF color state object.
	// Return:    Non-zero means <a>NULL</a>, otherwise not <a>NULL</a>.
	// Remarks:	  Tests whether the color state object is <a>NULL</a> or not.
	// Notes:
	//************************************
	static FS_BOOL IsNull(FPD_ColorState clrState);

	//************************************
	// Function:  GetModify
	// Param[in]: clrState The input PDF color state object.
	// Return:    void.
	// Remarks:	  The interface helps init the object if the object is NULL.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void GetModify(FPD_ColorState clrState);

	//************************************
	// Function:  NotUseFillColor
	// Param[in]: clrState		The input PDF color state object.
	// Return:    void
	// Remarks:	  Do not use the filling mode.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void NotUseFillColor(FPD_ColorState clrState);
};

class CFPD_TextState_V1
{
public:

	//************************************
	// Function:  New	
	// Param[in]: void
	// Return:    A new empty PDF text state object.
	// Remarks:	  Creates a new empty PDF text state object.
	// Notes:
	//************************************
	static FPD_TextState New(void);


	//************************************
	// Function:  Destroy
	// Param[in]: textState The input PDF text state object.
	// Return:    void
	// Remarks:	  Destroys the PDF text state object.
	// Notes:
	//************************************
	static void Destroy(FPD_TextState textState);


	//************************************
	// Function:  GetFont	
	// Param[in]: textState	The input PDF text state object.
	// Return:    The font.
	// Remarks:	  Gets the font.
	// Notes:
	//************************************
	static FPD_Font	GetFont(FPD_TextState textState);


	//************************************
	// Function:  SetFont
	// Param[in]: textState		The input PDF text state object.
	// Param[in]: font			The font to set.
	// Return:    void
	// Remarks:	  Sets the font. 
	// Notes:
	//************************************
	static void		SetFont(FPD_TextState textState, FPD_Font font);


	//************************************
	// Function:  GetFontSize
	// Param[in]: textState	The input PDF text state object.
	// Return:    The font size.
	// Remarks:	  Gets the font size.
	// Notes:
	//************************************
	static FS_FLOAT	GetFontSize(FPD_TextState textState);


	//************************************
	// Function:  GetMatrix	
	// Param[in]: textState	The input PDF text state object.
	// Return:    The text transformation matrix. It has 4 items.(matrix[4]).
	// Remarks:	  Gets The text transformation matrix. Tt has 4 items.(matrix[4]).
	// Notes:
	//************************************
	static FS_FLOAT* GetMatrix(FPD_TextState textState);


	//************************************
	// Function:  GetFontSizeV
	// Param[in]: textState	The input PDF text state object.
	// Return:    The vertical size in device units.
	// Remarks:	  Gets the vertical size in device units.
	// Notes:
	//************************************
	static FS_FLOAT	GetFontSizeV(FPD_TextState textState);


	//************************************
	// Function:  GetFontSizeH 
	// Param[in]: textState	The input PDF text state object.
	// Return:	  The horizontal size in device units
	// Remarks:	  Gets the horizontal size in device units
	// Notes:
	//************************************
	static FS_FLOAT	GetFontSizeH(FPD_TextState textState);


	//************************************
	// Function:  GetBaselineAngle
	// Param[in]: textState	The input PDF text state object.
	// Return:    The angle between device space X-axis and text baseline. In radians.
	// Remarks:	  Gets the angle between device space X-axis and text baseline. In radians.
	// Notes:
	//************************************
	static FS_FLOAT	GetBaselineAngle(FPD_TextState textState);


	//************************************
	// Function:  GetShearAngle
	// Param[in]: textState	The input PDF text state object.
	// Return:    The angle that text space Y-axis shears in device space. In radians.
	// Remarks:	  Gets the angle that text space Y-axis shears in device space. In radians.
	// Notes:
	//************************************
	static FS_FLOAT	GetShearAngle(FPD_TextState textState);

	//************************************
	// Function:  SetFontSize
	// Param[in]: textState	The input PDF text state object.
	// Param[in]: fontSize	The input font size.
	// Return:    void
	// Remarks:	  Sets the font size.
	// Notes:
	//************************************
	static void	SetFontSize(FPD_TextState textState, FS_FLOAT fontSize);

	//************************************
	// Function:  SetCharSpace
	// Param[in]: textState	The input PDF text state object.
	// Param[in]: fCharSpace	The input character space.
	// Return:    void
	// Remarks:	  Sets the character space.
	// Notes:
	//************************************
	static void	SetCharSpace(FPD_TextState textState, FS_FLOAT fCharSpace);

	//************************************
	// Function:  SetWordSpace
	// Param[in]: textState		The input PDF text state object.
	// Param[in]: fWordSpace	The input word space.
	// Return:    void
	// Remarks:	  Sets the word space.
	// Notes:
	//************************************
	static void	SetWordSpace(FPD_TextState textState, FS_FLOAT fWordSpace);

	//************************************
	// Function:  SetMatrix
	// Param[in]: textState		The input PDF text state object.
	// Param[in]: matrix		The input text transformation matrix.
	// Return:    void
	// Remarks:	  Sets the text transformation matrix.
	// Notes:
	//************************************
	static void	SetMatrix(FPD_TextState textState, FS_FLOAT	matrix[4]);

	//************************************
	// Function:  SetTextMode
	// Param[in]: textState		The input PDF text state object.
	// Param[in]: iTextMode		The input text mode. Only 0 is valid in this version.
	// Return:    void
	// Remarks:	  Sets the text mode.
	// Notes:
	//************************************
	static void	SetTextMode(FPD_TextState textState, FS_INT32 iTextMode);

	//************************************
	// Function:  SetTextCTM
	// Param[in]: textState		The input PDF text state object.
	// Param[in]: CTM			The input CTM.
	// Return:    void
	// Remarks:	  Sets the CTM for stroking purpose.
	// Notes:
	//************************************
	static void	SetTextCTM(FPD_TextState textState, FS_FLOAT CTM[4]);

	//************************************
	// Function:  GetTextMode
	// Param[in]: textState		The input PDF text state object.
	// Return:    The text mode.
	// Remarks:	  Gets the text mode.
	// Notes:
	//************************************
	static FS_INT32	GetTextMode(FPD_TextState textState);

	//************************************
	// Function:  GetTextCTM
	// Param[in]: textState		The input PDF text state object.
	// Param[out]: outCTM		It receives the text CTM for stroking purpose.
	// Return:    void
	// Remarks:	  Gets the CTM for stroking purpose. The CTM array must be allocated and freed by caller. 
	// It must contain 4 elements.
	// Notes:
	//************************************
	static void	GetTextCTM(FPD_TextState textState, FS_FLOAT* outCTM);

	//************************************
	// Function:  GetCharSpace
	// Param[in]: textState	The input PDF text state object.
	// Return:    The character space.
	// Remarks:	  Gets the character space.
	// Notes:
	//************************************
	static FS_FLOAT	GetCharSpace(FPD_TextState textState);

	//************************************
	// Function:  GetWordSpace
	// Param[in]: textState		The input PDF text state object.
	// Return:    The word space.
	// Remarks:	  Gets the word space.
	// Notes:
	//************************************
	static FS_FLOAT	GetWordSpace(FPD_TextState textState);

	//************************************
	// Function:  IsNull
	// Param[in]: textState		The input PDF text state object.
	// Return:    Non-zero means <a>NULL</a>, otherwise not <a>NULL</a>.
	// Remarks:	  Tests whether the text state object is <a>NULL</a> or not.
	// Notes:
	//************************************
	static FS_BOOL	IsNull(FPD_TextState textState);

	//************************************
	// Function:  GetModify
	// Param[in]: textState The input PDF text state object.
	// Return:    void.
	// Remarks:	  The interface helps init the object if the object is NULL.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void GetModify(FPD_TextState textState);

};


//PDF general state class
class CFPD_GeneralState_V1
{
public:
	//************************************
	// Function:  New	
	// Param[in]: void
	// Return:    A new empty PDF general state object.
	// Remarks:	  Creates a new empty PDF general state object.
	// Notes:
	//************************************
	static FPD_GeneralState New(void);

	//************************************
	// Function:  Destroy
	// Param[in]: genState The input PDF general state data object.
	// Return:    void
	// Remarks:	  Destroys the PDF general state data object.
	// Notes:
	//************************************
	static void	Destroy(FPD_GeneralState genState);

	//************************************
	// Function:  SetRenderIntent
	// Param[in]: genState	The input PDF general state data object.
	// Param[in]: ri		The input rendering intent.
	// Return:    void
	// Remarks:	  Sets the rendering intent.
	// Notes:
	//************************************
	static void	SetRenderIntent(FPD_GeneralState genState, FS_LPCSTR ri);

	//************************************
	// Function:  GetBlendType
	// Param[in]: genState	The input PDF general state data object.
	// Return:    The blend mode.
	// Remarks:	  Gets the blend mode.
	// Notes:
	//************************************
	static FS_INT32 GetBlendType(FPD_GeneralState genState);

	//************************************
	// Function:  GetAlpha
	// Param[in]: genState		The input PDF general state data object.
	// Param[in]: bStroke		Whether to get the current stroking alpha constant.
	// Return:    The current filling or stroking alpha constant.
	// Remarks:	  Gets the current filling or stroking alpha constant.
	// Notes:
	//************************************
	static FS_INT32 GetAlpha(FPD_GeneralState genState, FS_BOOL bStroke);

	//************************************
	// Function:  SetBlendMode
	// Param[in]: genState		The input PDF general state data object.
	// Param[in]: BlendMode		The input current blend mode name.
	// Return:    void
	// Remarks:	  Sets the current blend mode name.
	// Notes:
	//************************************
	static void SetBlendMode(FPD_GeneralState genState, char BlendMode[16]);

	//************************************
	// Function:  SetBlendType
	// Param[in]: genState		The input PDF general state data object.
	// Param[in]: iBlendType	The input blend type.
	// Return:    void
	// Remarks:	  Sets the current blend mode to be used in the transparent imaging model.
	// Notes:
	//************************************
	static void SetBlendType(FPD_GeneralState genState, FS_INT32 iBlendType);

	//************************************
	// Function:  SetSoftMask
	// Param[in]: genState		The input PDF general state data object.
	// Param[in]: softMask		The input current soft mask.
	// Return:    void
	// Remarks:	  Sets the current soft mask, specifying the mask shape or mask opacity values to be used in the transparent imaging model.
	// Notes:
	//************************************
	static void SetSoftMask(FPD_GeneralState genState, FPD_Object softMask);

	//************************************
	// Function:  SetSoftMaskMatrix
	// Param[in]: genState			The input PDF general state data object.
	// Param[in]: SoftMaskMatrix	The input matrix of the current soft mask.
	// Return:    void
	// Remarks:	  Sets the matrix of the current soft mask.
	// Notes:
	//************************************
	static void SetSoftMaskMatrix(FPD_GeneralState genState, FS_FLOAT SoftMaskMatrix[6]);

	//************************************
	// Function:  SetStrokeAlpha
	// Param[in]: genState			The input PDF general state data object.
	// Param[in]: fStrokeAlpha		The input current stroking alpha constant.
	// Return:    void
	// Remarks:	  Sets The current stroking alpha constant, specifying the constant shape or constant 
	//            opacity value to be used for stroking operations in the transparent imaging model.
	// Notes:
	//************************************
	static void SetStrokeAlpha(FPD_GeneralState genState, FS_FLOAT fStrokeAlpha);

	//************************************
	// Function:  SetFillAlpha
	// Param[in]: genState			The input PDF general state data object.
	// Param[in]: fFillAlpha		The input current filling alpha constant.
	// Return:    void
	// Remarks:	  Same as stroking alpha, but for non-stroking operations.
	// Notes:
	//************************************
	static void SetFillAlpha(FPD_GeneralState genState, FS_FLOAT fFillAlpha);

	//************************************
	// Function:  IsNull
	// Param[in]: genState			The input PDF general state data object.
	// Return:    Non-zero means <a>NULL</a>, otherwise not <a>NULL</a>.
	// Remarks:	  Tests whether the general state object is <a>NULL</a> or not.
	// Notes:
	//************************************
	static FS_BOOL IsNull(FPD_GeneralState genState);

	//************************************
	// Function:  GetModify
	// Param[in]: genState The input PDF general state object.
	// Return:    void.
	// Remarks:	  The interface helps init the object if the object is NULL.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void GetModify(FPD_GeneralState genState);
};

class CFPD_GraphState_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: void
	// Return:    A new empty PDF graphic state object.
	// Remarks:	  Creates a new empty PDF graphic state object.
	// Notes:
	//************************************
	static FPD_GraphState New(void);


	//************************************
	// Function:  Destroy
	// Param[in]: graphState	The graphic state object to be destroyed.
	// Return:    void
	// Remarks:	  Destroys a graphic state object.
	// Notes:
	//************************************
	static void Destroy(FPD_GraphState graphState);

	//************************************
	// Function:  SetDashCount
	// Param[in]: graphState	The input graphic state object.
	// Param[in]: count		The new count of dash points.
	// Return:    void
	// Remarks:	  In order to keep heap integrity, the function is used to allocate enough buffer for dash array.
	// Notes:
	//************************************
	static void	SetDashCount(FPD_GraphState graphState, FS_INT32 count);

	//************************************
	// Function:  GetDashCount
	// Param[in]: graphState	The input graphic state object.
	// Return:    The total size of dash array.
	// Remarks:	  Gets the total size of dash array.
	// Notes:
	//************************************
	static FS_INT32	GetDashCount(FPD_GraphState graphState);

	//************************************
	// Function:  GetDashArray
	// Param[in]: graphState	The input graphic state object.
	// Return:    The dash array used by graphic state object.
	// Remarks:	  Gets the dash array.
	// Notes:
	//************************************
	static FS_FLOAT* GetDashArray(FPD_GraphState graphState);


	//************************************
	// Function:  GetDashPhase
	// Param[in]: graphState	The input graphic state object.
	// Return:    The dash phase for line dash pattern.
	// Remarks:	  Gets the dash phase for line dash pattern.
	// Notes:
	//************************************
	static FS_FLOAT GetDashPhase(FPD_GraphState graphState);

	//************************************
	// Function:  SetDashPhase
	// Param[in]: graphState	The input graphic state object.
	// Param[in]: dashPhase		The new dash phase.
	// Return:    void
	// Remarks:	  Sets the new dash phase for line dash pattern.
	// Notes:
	//************************************
	static void SetDashPhase(FPD_GraphState graphState, FS_FLOAT dashPhase);

	//************************************
	// Function:  GetLineCap
	// Param[in]: graphState	The input graphic state object.
	// Return:    The line cap style.
	// Remarks:	  Gets the line cap style.
	// Notes:
	//************************************
	static FPD_LineCap GetLineCap(FPD_GraphState graphState);

	//************************************
	// Function:  SetLineCap
	// Param[in]: graphState	The input graphic state object.
	// Param[in]: cap	The new style of line cap.
	// Return:    void
	// Remarks:	  Sets the new line cap style.
	// Notes:
	//************************************
	static void SetLineCap(FPD_GraphState graphState, FPD_LineCap cap);

	//************************************
	// Function:  GetLineJoin
	// Param[in]: graphState	The input graphic state object.
	// Return:    The line join style.
	// Remarks:	  Gets the line join style.
	// Notes:
	//************************************
	static FPD_LineJoin GetLineJoin(FPD_GraphState graphState);

	//************************************
	// Function:  SetLineJoin
	// Param[in]: graphState	The input graphic state object.
	// Param[in]: join	The new style of line join.
	// Return:    void
	// Remarks:	  Sets the new style of line join.
	// Notes:
	//************************************
	static void SetLineJoin(FPD_GraphState graphState, FPD_LineJoin join);

	//************************************
	// Function:  GetMiterLimit
	// Param[in]: graphState	The input graphic state object.
	// Return:    The miter limit for line join.
	// Remarks:	  Gets the miter limit for line join.
	// Notes:
	//************************************
	static FS_FLOAT GetMiterLimit(FPD_GraphState graphState);

	//************************************
	// Function:  SetMiterLimit
	// Param[in]: graphState	The input graphic state object.
	// Param[in]: limit		The new miter limit for line join.
	// Return:    void
	// Remarks:	  Sets the new miter limit for line join.
	// Notes:
	//************************************
	static void SetMiterLimit(FPD_GraphState graphState, FS_FLOAT limit);

	//************************************
	// Function:  GetLineWidth
	// Param[in]: graphState	The input graphic state object.
	// Return:    The line width.
	// Remarks:	  Gets the line width.
	// Notes:
	//************************************
	static FS_FLOAT GetLineWidth(FPD_GraphState graphState);

	//************************************
	// Function:  SetLineWidth
	// Param[in]: graphState	The input graphic state object.
	// Param[in]: width		The new width of lines.
	// Return:    void
	// Remarks:	  Sets new width of lines.
	// Notes:
	//************************************
	static void SetLineWidth(FPD_GraphState graphState, FS_FLOAT width);

	//************************************
	// Function:  IsNull
	// Param[in]: graphState	The input graphic state object.
	// Return:    Non-zero means <a>NULL</a>, otherwise not <a>NULL</a>.
	// Remarks:	  Tests whether the graphic state object is <a>NULL</a> or not.
	// Notes:
	//************************************
	static FS_BOOL IsNull(FPD_GraphState graphState);
	
	//************************************
	// Function:  GetModify
	// Param[in]: graphState The input PDF graph state object.
	// Return:    void.
	// Remarks:	  The interface helps init the object if the object is NULL.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void GetModify(FPD_GraphState graphState);

	//************************************
	// Function:  SetDashArray
	// Param[in]: graphState	The input graphic state object.
	// Param[in]: dashArray		The input dash array.
	// Return:    void
	// Remarks:	  Sets the dash phase for line dash pattern.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void SetDashArray(FPD_GraphState graphState, FS_FLOAT* dashArray);
};


//class page object
class CFPD_PageObject_V1
{
public:

	//************************************
	// Function:  New
	// Param[in]: type		The input page object type.
	// Return:    A page object.
	// Remarks:   Creates a new empty PDF page object.
	// Notes:
	//************************************
	static FPD_PageObject New(FS_INT32 type);


	//************************************
	// Function:  Destroy
	// Param[in]: pageObj The input PDF page object.
	// Return:    void
	// Remarks:   Destroys the PDF page object.
	// Notes:
	//************************************
	static void	Destroy(FPD_PageObject pageObj);


	//************************************
	// Function:  Clone
	// Param[in]: pageObj		The source object.
	// Return:    The cloned object.
	// Remarks:   Clones a page object.
	// Notes:
	//************************************
	static FPD_PageObject	Clone(FPD_PageObject pageObj);


	//************************************
	// Function:  Copy
	// Param[in]: pageObj		The input PDF page object.
	// Param[in]: srcObject		The source page object.
	// Return:    void
	// Remarks:   Copies from another page object.
	// Notes:
	//************************************
	static void	Copy(FPD_PageObject pageObj, const FPD_PageObject srcObject);

	//************************************
	// Function:  RemoveClipPath
	// Param[in]: pageObj	The input PDF page object.
	// Return:    void
	// Remarks:   Removes clipping path of the object.
	// Notes:
	//************************************
	static void	RemoveClipPath(FPD_PageObject pageObj);


	//************************************
	// Function:  AppendClipPath
	// Param[in]: pageObj		The input PDF page object.
	// Param[in]: path			The input clipping path.
	// Param[in]: type			The clip type of the input clipping path.
	// Param[in]: bAutoMerge	Whether to merge the clipping path automatically.
	// Return:    void
	// Remarks:   Appends a clipping path.
	// Notes:
	//************************************
	static void	AppendClipPath(FPD_PageObject pageObj, FPD_Path path, FS_INT32 type, FS_BOOL bAutoMerge);


	//************************************
	// Function:  CopyClipPath
	// Param[in]: pageObj		The input PDF page object.
	// Param[in]: srcObj		The source page object.
	// Return:    void
	// Remarks:   Copies clipping path from another object.
	// Notes:
	//************************************
	static void	CopyClipPath(FPD_PageObject pageObj, FPD_PageObject srcObj);


	//************************************
	// Function:  TransformClipPath
	// Param[in]: pageObj		The input PDF page object.
	// Param[in]: matrix		The matrix used to transform.
	// Return:    void
	// Remarks:   Transforms the clip path. Rotate, shear, or move clip path.
	// Notes:
	//************************************
	static void	TransformClipPath(FPD_PageObject pageObj, FS_AffineMatrix matrix);


	//************************************
	// Function:  SetColorState
	// Param[in]: pageObj	The input PDF page object.
	// Param[in]: state		The new color state.
	// Return:    void
	// Remarks:   Sets the color state.
	// Notes:
	//************************************
	static void	SetColorState(FPD_PageObject pageObj, FPD_ColorState state);


	//************************************
	// Function:  GetBBox
	// Param[in]: pageObj	The input PDF page object.
	// Param[in]: pMatrix	The input transformation matrix.
	// Return:    The bounding box of the page object.
	// Remarks:   Gets the bounding box of the page object, optionally with a transformation matrix.
	// Notes:
	//************************************
	static FS_Rect GetBBox(FPD_PageObject pageObj, FS_AffineMatrix* pMatrix);

	//************************************
	// Function:  GetOriginalBBox
	// Param[in]: pageObj	The input PDF page object.
	// Return:    The original bounding box of the page object
	// Remarks:   Gets the original bounding box of the page object.
	// Notes:
	//************************************
	static FS_FloatRect GetOriginalBBox(FPD_PageObject pageObj);


	//************************************
	// Function:  GetType
	// Param[in]: pageObj	The input PDF page object.
	// Return:    Gets the page object type.
	// Remarks:   The page object type.
	// Notes:
	//************************************
	static FS_INT32	GetType(FPD_PageObject pageObj);

	//************************************
	// Function:  GetClipPath
	// Param[in]: pageObj	The input PDF page object.
	// Return:    The clip path state.
	// Remarks:   Gets the clip path state.
	// Notes:
	//************************************
	static FPD_ClipPath	GetClipPath(FPD_PageObject pageObj);


	//************************************
	// Function:  GetGraphState
	// Param[in]: pageObj	The input PDF page object.
	// Return:    The graph state for graphs and type3 font or stroke texts.
	// Remarks:   Gets the graph state. For graphs and type3 font or stroke texts.
	// Notes:
	//************************************
	static FPD_GraphState GetGraphState(FPD_PageObject pageObj);


	//************************************
	// Function:  GetColorState
	// Param[in]: pageObj	The input PDF page object.
	// Return:    The color state for texts, graphs and uncolored images.
	// Remarks:   Gets the color state. For texts, graphs and uncolored images.
	// Notes:
	//************************************
	static FPD_ColorState GetColorState(FPD_PageObject pageObj);


	//************************************
	// Function:  GetTextState
	// Param[in]: pageObj	The input PDF page object.
	// Return:    The text state for texts only.
	// Remarks:   Gets the text state. For texts only.
	// Notes:
	//************************************
	static FPD_TextState GetTextState(FPD_PageObject pageObj);


	//************************************
	// Function:  GetGeneralState
	// Param[in]: pageObj	The input PDF page object.
	// Return:    The general state for all objects.
	// Remarks:   Gets the general state. For all objects.
	// Notes:
	//************************************
	static FPD_GeneralState	GetGeneralState(FPD_PageObject pageObj);

	//************************************
	// Function:  GetContentMark
	// Param[in]: pageObj	The input PDF page object.
	// Return:    The content mark. For all objects.
	// Remarks:   Gets the content mark.
	// Notes:
	//************************************
	static FPD_ContentMark GetContentMark(FPD_PageObject pageObj);

	//************************************
	// Function:  DefaultStates
	// Param[in]: pageObj	The input PDF page object.
	// Return:    void
	// Remarks:   Sets all graphic states to default.
	// Notes:
	//************************************
	static void DefaultStates(FPD_PageObject pageObj);

	//************************************
	// Function:  CopyStates
	// Param[in]: pageObj	The input PDF page object.
	// Param[in]: src		The input graphic states.
	// Return:    void
	// Remarks:   Copies from another graphic states.
	// Notes:
	//************************************
	static void CopyStates(FPD_PageObject pageObj, const FPD_PageObject src);


	//************************************
	// Function:  SetGraphState
	// Param[in]: pageObj		The input PDF page object.
	// Param[in]: grahpState	The graphic state to be set.
	// Return:    void
	// Remarks:   Sets the graph state.
	// Notes:
	//************************************
	static void SetGraphState(FPD_PageObject pageObj, FPD_GraphState grahpState);

	//************************************
	// Function:  SetTextState
	// Param[in]: pageObj	The input PDF page object.
	// Param[in]: textState	The text state to be set.
	// Return:    void
	// Remarks:   Sets the text state. 
	// Notes:
	//************************************
	static void SetTextState(FPD_PageObject pageObj, FPD_TextState textState);

	//************************************
	// Function:  SetGeneralState
	// Param[in]: pageObj	The input PDF page object.
	// Param[in]: genState	The general state to be set.
	// Return:    void
	// Remarks:   Sets the general state. 
	// Notes:
	//************************************
	static void SetGeneralState(FPD_PageObject pageObj, FPD_GeneralState genState);

	//************************************
	// Function:  HasClipPath
	// Param[in]: pageObj	The input PDF page object.
	// Return: <a>TRUE</a> if the page object has the clip path, otherwise not.
	// Remarks: Checks whether the page object has the clip path or not.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	//************************************
	static FS_BOOL	HasClipPath(FPD_PageObject pageObj);

	//************************************
	// Function:  GetContentMark2
	// Param[in]: pageObj				The input PDF page object.
	// Param[in]: bGetModifiableCopy	Get a modifiable copy of the object. If the reference was refer to null, 
	// then a new object will be created. The returned pointer can be used to alter the object content.
	// Return:    The content mark. For all objects.
	// Remarks:   Gets the content mark.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3.1
	//************************************
	static FPD_ContentMark GetContentMark2(FPD_PageObject pageObj, FS_BOOL bGetModifiableCopy);

};


//class page text object
class CFPD_TextObject_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: void
	// Return:    The PDF text object
	// Remarks:   Creates a new empty PDF text object.
	// Notes:
	//************************************
	static FPD_PageObject New(void);

	//************************************
	// Function:  Destroy
	// Param[in]: objText The input PDF text object.
	// Return:    void
	// Remarks:   Destroys the input PDF text object. If it is added to the page, it is taken over and don't destroy it.
	// Notes:
	//************************************
	static void	Destroy(FPD_PageObject objText);

	//************************************
	// Function:  CountItems
	// Param[in]: objText	The input PDF text object.
	// Return:    The count of text object items.
	// Remarks:   Gets the count of text object items.
	// Notes:
	//************************************
	static FS_INT32 CountItems(FPD_PageObject objText);

	//************************************
	// Function:  GetItemInfo
	// Param[in]: objText	The input PDF text object.
	// Param[in]: index		Specifies zero-based item index in the text object.
	// Return:    The specified text object item information.
	// Remarks:   Gets specified text object item information.
	// Notes:
	//************************************
	static FPD_TextObjectItem GetItemInfo(FPD_PageObject objText, FS_INT32 index);

	//************************************
	// Function:  CountChars
	// Param[in]: objText	The input PDF text object.
	// Return:    The count of characters in the text object. 
	// Remarks:   Gets the count of characters in the text object. 
	// Notes:
	//************************************
	static FS_INT32 CountChars(FPD_PageObject objText);

	//************************************
	// Function:   GetCharInfo
	// Param[in]:  objText			The input PDF text object.
	// Param[in]:  index			Specifies zero-based character index in the text object.
	// Param[out]: outCharcode		It receives the character code.
	// Param[out]: outKerning		It receives the kerning(x-direction only).
	// Return:     void
	// Remarks:    Gets the information of specified character.
	// Notes:
	//************************************
	static void GetCharInfo(FPD_PageObject objText, FS_INT32 index, FS_DWORD* outCharcode, FS_FLOAT* outKerning);

	//************************************
	// Function:  GetPosX
	// Param[in]: objText	The input PDF text object.
	// Return:    The x-coordinate of the origin in the device space
	// Remarks:   Gets the x-coordinate of the origin in the device space
	// Notes:
	//************************************
	static FS_FLOAT	GetPosX(FPD_PageObject objText);

	//************************************
	// Function:  GetPosY
	// Param[in]: objText	The input PDF text object.
	// Return:    The y-coordinate of the origin in the device space.
	// Remarks:   Gets the y-coordinate of the origin in the device space.
	// Notes:
	//************************************
	static FS_FLOAT	GetPosY(FPD_PageObject objText);

	//************************************
	// Function:   GetTextMatrix
	// Param[in]:  objText			The input PDF text object.
	// Param[out]: outMatrix		It receives the matrix from text space to object space.
	// Return:     void
	// Remarks:    Gets matrix from text space to object space.
	// Notes:
	//************************************
	static void	GetTextMatrix(FPD_PageObject objText, FS_AffineMatrix* outMatrix);

	//************************************
	// Function:  GetFont
	// Param[in]: objText	The input PDF text object.
	// Return:    The font.
	// Remarks:   Gets the font.
	// Notes:
	//************************************
	static FPD_Font	GetFont(FPD_PageObject objText);

	//************************************
	// Function:  GetFontSize
	// Param[in]: objText	The input PDF text object.
	// Return:    The font size.
	// Remarks:   Gets the font size.
	// Notes:
	//************************************
	static FS_FLOAT	GetFontSize(FPD_PageObject objText);

	//************************************
	// Function:  SetEmpty
	// Param[in]: objText	The input PDF text object.
	// Return:    void
	// Remarks:   Sets the text object to be empty.
	// Notes:
	//************************************
	static void	SetEmpty(FPD_PageObject objText);

	//************************************
	// Function:  SetText
	// Param[in]: objText		The input PDF text object.
	// Param[in]: strText		The input text segment.
	// Return:    void
	// Remarks:   Sets a single text segment without any kerning inside.
	// Notes:
	//************************************
	static void SetText(FPD_PageObject objText, const FS_ByteString strText);


	//************************************
	// Function:  SetText2
	// Param[in]: objText		The input PDF text object.
	// Param[in]: strTextArr	The input text segments.
	// Param[in]: pKerning		The kerning array.
	// Return:    void
	// Remarks:   Sets text using segmented fashion.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static void	SetText2(FPD_PageObject objText, FS_ByteStringArray strTextArr, FS_FLOAT* pKerning);


	//************************************
	// Function:  SetText3
	// Param[in]: objText		The input PDF text object.
	// Param[in]: nChars		The count of input character codes.
	// Param[in]: pCharCodes	The input character codes.
	// Param[in]: pKernings		The input kerning array.
	// Return:    void
	// Remarks:   Sets text using char-kerning pair fashion.
	// Notes:
	//************************************
	static void	SetText3(FPD_PageObject objText, FS_INT32 nChars, FS_DWORD* pCharCodes, FS_FLOAT* pKernings);


	//************************************
	// Function:  SetPosition
	// Param[in]: objText	The input PDF text object.
	// Param[in]: x			The x-coordinate in device space.
	// Param[in]: y			The y-coordinate in device space. 
	// Return:    void
	// Remarks:   Sets the origin position in device space.
	// Notes:
	//************************************
	static void	SetPosition(FPD_PageObject objText, FS_FLOAT x, FS_FLOAT y);


	//************************************
	// Function:  SetTextState
	// Param[in]: objText			The input PDF text object.
	// Param[in]: textState			The new text state.
	// Return:    void
	// Remarks:   Sets the text state.
	// Notes:
	//************************************
	static void	SetTextState(FPD_PageObject objText, FPD_TextState textState);

	//************************************
	// Function:  Transform
	// Param[in]: objText		The input PDF text object.
	// Param[in]: matrix		The transformation matrix.
	// Return:    void
	// Remarks:   Transforms the text object.
	// Notes:
	//************************************
	static void	Transform(FPD_PageObject objText, FS_AffineMatrix matrix);

	//************************************
	// Function:   CalcCharPos
	// Param[in]:  objText			The input PDF text object.
	// Param[out]: outPosArray		It receives the character position array.
	// Return:     void
	// Remarks:    Calculates origin positions in text space, for each character.
	// Notes:      The position array must be allocated and freed by caller. It must contain at least nChars*2 elements.
	//   For each character, the origin position (along the text baseline) and next origin position will be calculated.
	//************************************
	static void	CalcCharPos(FPD_PageObject objText, FS_FLOAT* outPosArray);

	//************************************
	// Function:  SetData
	// Param[in]: objText		The input PDF text object.
	// Param[in]: nChars		The count of characters to set.
	// Param[in]: pCharCodes	The input character codes array.
	// Param[in]: pCharPos		The input character positions array in text space.
	// Param[in]: x				The x-coordinate of the origin position, in device space.
	// Param[in]: y				The y-coordinate of the origin position, in device space.
	// Return:    void
	// Remarks:	  Sets text data.
	// Notes:
	//************************************
	static void	SetData(
		FPD_PageObject objText,
		FS_INT32 nChars, 
		FS_DWORD* pCharCodes, 
		FS_FLOAT* pCharPos, 
		FS_FLOAT x, 
		FS_FLOAT y
		);


	//************************************
	// Function:   GetData
	// Param[in]:  objText			The input PDF text object.
	// Param[out]: outCharCount		It receives the count of characters.
	// Param[out]: outCharCodes		It receives the character codes array.
	// Param[out]: outCharPos		It receives the character positions array in text space.
	// Return:     void
	// Remarks:	   Gets text data.
	// Notes:
	//************************************
	static void	GetData(FPD_PageObject objText, FS_INT32* outCharCount, FS_DWORD** outCharCodes, FS_FLOAT** outCharPos);


	//************************************
	// Function:  RecalcPositionData
	// Param[in]: objText	The input PDF text object.
	// Return:    void
	// Remarks:   Recalculates the position data.
	// Notes:
	//************************************
	static void	RecalcPositionData(FPD_PageObject objText);
};

//FPD_PathObject_V1
class CFPD_PathObject_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: void
	// Return:    A new empty PDF path object. 
	// Remarks:   Creates a new empty PDF path object.
	// Notes:
	//************************************
	static FPD_PageObject New(void);


	//************************************
	// Function:  Destroy
	// Param[in]: objPath	The input PDF path object.
	// Return:    void
	// Remarks:   Destroys the PDF path object.
	// Notes:
	//************************************
	static void	Destroy(FPD_PageObject objPath);


	//************************************
	// Function:  Transform
	// Param[in]: objPath		The input PDF path object.
	// Param[in]: matrix		The transformation matrix.
	// Return:    void
	// Remarks:   Transforms the path object.
	// Notes:
	//************************************
	static void	Transform(FPD_PageObject objPath, FS_AffineMatrix matrix);


	//************************************
	// Function:  SetGraphState
	// Param[in]: objPath			The input PDF path object.
	// Param[in]: graphState		The input new graph state.
	// Return:    void
	// Remarks:   Sets the graph state.
	// Notes:
	//************************************
	static void	SetGraphState(FPD_PageObject objPath, FPD_GraphState graphState);


	//************************************
	// Function:  CalcBoundingBox
	// Param[in]: objPath	The input PDF path object.
	// Return:    void
	// Remarks:   Calculates the bounding box.
	// Notes:
	//************************************
	static void	CalcBoundingBox(FPD_PageObject objPath);


	//************************************
	// Function:   GetTransformMatrix
	// Param[in]:  objPath				The input PDF path object.
	// Param[out]: outmatrix			It receives the transformation matrix.
	// Return:     void
	// Remarks:	   Gets the transformation matrix.
	// Notes:      Transformation matrix used to transform the path coordinates. Also used to determine line geometry.
	//************************************
	static void GetTransformMatrix(FPD_PageObject objPath, FS_AffineMatrix* outmatrix);


	//************************************
	// Function:  SetTransformMatrix
	// Param[in]: objPath		The input PDF path object.
	// Param[in]: matrix		The transformation matrix.
	// Return:    void
	// Remarks:   Sets the transformation matrix.
	// Notes:     Transformation matrix used to transform the path coordinates. Also used to determine line geometry.
	//************************************
	static void SetTransformMatrix(FPD_PageObject objPath, const FS_AffineMatrix* matrix);


	//************************************
	// Function:  GetPath
	// Param[in]: objPath	The input PDF path object.
	// Return:    The path data object.
	// Remarks:   Gets the path data object. Reference to path data.
	// Notes:
	//************************************
	static FPD_Path	GetPath(FPD_PageObject objPath);


	//************************************
	// Function:  IsStrokeMode
	// Param[in]: objPath	The input PDF path object.
	// Return:    <a>TRUE</a> to stroke the path, otherwise not.
	// Remarks:   Tests whether the paint mode for the path object is stroking mode.
	// Notes:
	//************************************
	static FS_BOOL IsStrokeMode(FPD_PageObject objPath);


	//************************************
	// Function:  SetStrokeMode
	// Param[in]: objPath		The input PDF path object.
	// Param[in]: bStroke		True to stroke the path, otherwise not.
	// Return:    void
	// Remarks:   Sets whether to stroke the path.
	// Notes:
	//************************************
	static void	SetStrokeMode(FPD_PageObject objPath, FS_BOOL bStroke);


	//************************************
	// Function:  GetFillMode
	// Param[in]: objPath	The input PDF path object.
	// Return:    The filling mode code. See <a>FSFillingModeFlags</a>.
	// Remarks:   Gets the filling mode of the page object.
	// Notes:
	//************************************
	static FS_INT32 GetFillMode(FPD_PageObject objPath);

	//************************************
	// Function:  SetFillMode
	// Param[in]: objPath	The input PDF path object.
	// Param[in]: mode	The new filling mode. See <a>FSFillingModeFlags</a>.
	// Return:    void
	// Remarks:   Sets the new filling mode.
	// Notes:
	//************************************
	static void SetFillMode(FPD_PageObject objPath, FS_INT32 mode);
};

//CFPD_ImageObject_V1
class CFPD_ImageObject_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: void
	// Return:    A new empty PDF image object.
	// Remarks:   Creates a new empty PDF image object.
	// Notes:
	//************************************
	static FPD_PageObject New(void);


	//************************************
	// Function:  Destroy
	// Param[in]: objImage The input PDF image object.
	// Return:    void
	// Remarks:   Destroys the PDF image object.
	// Notes:
	//************************************
	static void	Destroy(FPD_PageObject objImage);

	//************************************
	// Function:  Transform
	// Param[in]: objImage		The input PDF image object.
	// Param[in]: matrix		The transformation matrix.
	// Return:    void
	// Remarks:   Transforms the image object.
	// Notes:
	//************************************
	static void	Transform(FPD_PageObject objImage, FS_AffineMatrix matrix);


	//************************************
	// Function:  CalcBoundingBox
	// Param[in]: objImage	The input PDF image object.
	// Return:    void
	// Remarks:   Calculates the bounding box.
	// Notes:
	//************************************
	static void	CalcBoundingBox(FPD_PageObject objImage);


	//************************************
	// Function:   GetTransformMatrix
	// Param[in]:  objImage		The input PDF image object.
	// Param[out]: outmatrix	It receives the transformation matrix.
	// Return:     void
	// Remarks:    Gets the transformation matrix.
	// Notes:
	//************************************
	static void GetTransformMatrix(FPD_PageObject objImage, FS_AffineMatrix* outmatrix);


	//************************************
	// Function:  SetTransformMatrix
	// Param[in]: objImage		The input PDF image object.
	// Param[in]: matrix		The transformation matrix.
	// Return:    void
	// Remarks:   Sets the transformation matrix.
	// Notes:
	//************************************
	static void SetTransformMatrix(FPD_PageObject objImage, const FS_AffineMatrix* matrix);


	//************************************
	// Function:  GetImage
	// Param[in]: objImage	The input PDF image object.
	// Return:    The image data object.
	// Remarks:   Gets the image data object.
	// Notes:
	//************************************
	static FPD_Image GetImage(FPD_PageObject objImage);


	//************************************
	// Function:  SetImage
	// Param[in]: objImage		The input PDF image object.
	// Param[in]: image			The input image data object.
	// Return:    void
	// Remarks:   Sets the image data object.
	// Notes:
	//************************************
	static void	SetImage(FPD_PageObject objImage, FPD_Image image);

	
};

//CFPD_ShadingObject_V1
class CFPD_ShadingObject_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: void
	// Return:    A new empty PDF shading object
	// Remarks:   Creates a new empty PDF shading object.
	// Notes:
	//************************************
	static FPD_PageObject New(void);


	//************************************
	// Function:  Destroy
	// Param[in]: objShading The input PDF shading object.
	// Return:    void
	// Remarks:   Destroys the PDF shading object.
	// Notes:
	//************************************
	static void	Destroy(FPD_PageObject objShading);
	
	//************************************
	// Function:  Transform
	// Param[in]: objShading	The input PDF shading object.
	// Param[in]: matrix		The transformation matrix.
	// Return:    void
	// Remarks:   Transforms the path object.
	// Notes:
	//************************************
	static void	Transform(FPD_PageObject objShading, FS_AffineMatrix matrix);


	//************************************
	// Function:  CalcBoundingBox
	// Param[in]: objShading	The input PDF shading object.
	// Return:    void
	// Remarks:   Calculates the bounding box.
	// Notes:
	//************************************
	static void	CalcBoundingBox(FPD_PageObject objShading);


	//************************************
	// Function:   GetTransformMatrix
	// Param[in]:  objShading		The input PDF shading object.
	// Param[out]: outmatrix		It receives the transformation matrix.
	// Return:     void
	// Remarks:    Gets the transformation matrix.
	// Notes:
	//************************************
	static void GetTransformMatrix(FPD_PageObject objShading, FS_AffineMatrix* outmatrix);


	//************************************
	// Function:  SetTransformMatrix
	// Param[in]: objShading	The input PDF shading object.
	// Param[in]: matrix		The transformation matrix.
	// Return:    void
	// Remarks:   Sets the transformation matrix.
	// Notes:
	//************************************
	static void SetTransformMatrix(FPD_PageObject objShading, const FS_AffineMatrix* matrix);


	//************************************
	// Function:  GetPage
	// Param[in]: objShading	The input PDF shading object.
	// Return:    The page.
	// Remarks:   Gets the page.
	// Notes:
	//************************************
	static FPD_Page	GetPage(FPD_PageObject objShading);


	//************************************
	// Function:  SetPage
	// Param[in]: objShading	The input PDF shading object.
	// Param[in]: page			The input page.
	// Return:    void
	// Remarks:   Sets the page.
	// Notes:
	//************************************
	static void	SetPage(FPD_PageObject objShading, FPD_Page page);


	//************************************
	// Function:  GetShadingPattern
	// Param[in]: objShading	The input PDF shading object.
	// Return:    void
	// Remarks:   Gets the shading pattern.
	// Notes:
	//************************************
	static FPD_ShadingPattern GetShadingPattern(FPD_PageObject objShading);


	//************************************
	// Function:  SetShadingPattern
	// Param[in]: objShading	The input PDF shading object.
	// Param[in]: pattern		The input shading pattern.
	// Return:    void
	// Remarks:   Sets the shading pattern.
	// Notes:
	//************************************
	static void	SetShadingPattern(FPD_PageObject objShading, FPD_ShadingPattern pattern);
};

//CFPD_FormObject_V1
class CFPD_FormObject_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: void
	// Return:    A new empty PDF form object.
	// Remarks:   Creates a new empty PDF form object.
	// Notes:
	//************************************
	static FPD_PageObject New(void);


	//************************************
	// Function:  Destroy
	// Param[in]: objForm The input PDF form object.
	// Return:    void
	// Remarks:   Destroys the PDF form object.
	// Notes:
	//************************************
	static void	Destroy(FPD_PageObject objForm);

	//************************************
	// Function:  Transform
	// Param[in]: objForm		The input PDF form object.
	// Param[in]: matrix		The transformation matrix.
	// Return:    void
	// Remarks:   Transforms the path object.
	// Notes:
	//************************************
	static void	Transform(FPD_PageObject objForm, FS_AffineMatrix matrix);


	//************************************
	// Function:  CalcBoundingBox
	// Param[in]: objForm	The input PDF form object.
	// Return:    void
	// Remarks:   Calculates the bounding box.
	// Notes:
	//************************************
	static void	CalcBoundingBox(FPD_PageObject objForm);


	//************************************
	// Function:   GetTransformMatrix
	// Param[in]:  objForm			The input PDF form object.
	// Param[out]: outmatrix		It receives the transformation matrix.
	// Return:     void
	// Remarks:    Gets the transformation matrix.
	// Notes:
	//************************************
	static void GetTransformMatrix(FPD_PageObject objForm, FS_AffineMatrix* outmatrix);


	//************************************
	// Function:  SetTransformMatrix
	// Param[in]: objForm		The input PDF form object.
	// Param[in]: matrix		The transformation matrix.
	// Return:    void
	// Remarks:   Sets the transformation matrix.
	// Notes:
	//************************************
	static void SetTransformMatrix(FPD_PageObject objForm, const FS_AffineMatrix* matrix);


	//************************************
	// Function:  GetForm
	// Param[in]: objForm	The input PDF form object.
	// Return:    The form.
	// Remarks:   Gets the form.
	// Notes:
	//************************************
	static FPD_Form	GetForm(FPD_PageObject objForm);


	//************************************
	// Function:  SetForm
	// Param[in]: objForm		The input PDF form object.
	// Param[in]: form			The input form.
	// Return:    void
	// Remarks:   Sets the form.
	// Notes:
	//************************************
	static void	SetForm(FPD_PageObject objForm, FPD_Form form);
};

//CFPD_InlineImages_V1
class CFPD_InlineImages_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: void
	// Return:    A new empty PDF inline images object.
	// Remarks:   Creates a new empty PDF inline images object.
	// Notes:
	//************************************
	static FPD_PageObject New(void);


	//************************************
	// Function:  Destroy
	// Param[in]: objInlineImgs The input PDF inline images object.
	// Return:    void
	// Remarks:   Destroys the PDF inline images object.
	// Notes:
	//************************************
	static void	Destroy(FPD_PageObject objInlineImgs);

	//************************************
	// Function:  AddMatrix
	// Param[in]: objInlineImgs			The input PDF inline images object.
	// Param[in]: matrix				The input transform matrix.
	// Return:    void
	// Remarks:   Adds a transform matrix.
	// Notes:
	//************************************
	static void	AddMatrix(FPD_PageObject objInlineImgs, const FS_AffineMatrix* matrix);


	//************************************
	// Function:  CountMatrix
	// Param[in]: objInlineImgs	The input PDF inline images object.
	// Return:    The number of transform matrix in this object.
	// Remarks:   Counts the number of transform matrix in this object.
	// Notes:
	//************************************
	static int	CountMatrix(FPD_PageObject objInlineImgs);


	//************************************
	// Function:  GetMatrix
	// Param[in]: objInlineImgs		The input PDF inline images object.
	// Param[in]: index				The index of transform matrix.
	// Return:    The transform matrix from object by index.
	// Remarks:   Gets a transform matrix from object by index.
	// Notes:
	//************************************
	static FS_AffineMatrix	GetMatrix(FPD_PageObject objInlineImgs, FS_INT32 index);


	//************************************
	// Function:  GetStream
	// Param[in]: objInlineImgs	The input PDF inline images object.
	// Return:    The stream.
	// Remarks:   Gets the stream.
	// Notes:
	//************************************
	static FPD_Object GetStream(FPD_PageObject objInlineImgs);


	//************************************
	// Function:  SetStream
	// Param[in]: objInlineImgs			The input PDF inline images object.
	// Param[in]: stream				The input stream
	// Return:    void
	// Remarks:   Sets the stream
	// Notes:
	//************************************
	static void	SetStream(FPD_PageObject objInlineImgs, FPD_Object stream);

};

//content mark

/** @brief PDF content mark item class. */
class CFPD_ContentMarkItem_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: void
	// Return:    A new empty PDF content mark item object.
	// Remarks:   Creates a new empty PDF content mark item object.
	// Notes:
	//************************************
	static FPD_ContentMarkItem New(void);


	//************************************
	// Function:  Destroy
	// Param[in]: item The input PDF content mark item object.
	// Return:    void
	// Remarks:   Destroys the PDF content mark item object.
	// Notes:
	//************************************
	static void	Destroy(FPD_ContentMarkItem item);

	//************************************
	// Function:  GetName
	// Param[in]: item The input PDF content mark item object.
	// Return:    The name(tag) of the content mark item.
	// Remarks:   Gets the name(tag) of the content mark item.
	// Notes:
	//************************************
	static char* GetName(FPD_ContentMarkItem item);


	//************************************
	// Function:  GetParamType
	// Param[in]: item	The input PDF content mark item object.
	// Return:    The parameter type of the content mark item.
	// Remarks:   Gets the parameter type of the content mark item.
	// Notes:
	//************************************
	static FPD_MarkItemParamType GetParamType(FPD_ContentMarkItem item);


	//************************************
	// Function:  GetParam
	// Param[in]: item	The input PDF content mark item object.
	// Return:    The parameter of the content mark item.
	// Remarks:   Gets the parameter of the content mark item.
	// Notes:
	//************************************
	static void* GetParam(FPD_ContentMarkItem item);


	//************************************
	// Function:  SetName
	// Param[in]: item			The input PDF content mark item object.
	// Param[in]: csName		The input new name(tag).
	// Return:    void
	// Remarks:   Sets the name(tag) of the content mark item.
	// Notes:
	//************************************
	static void	SetName(FPD_ContentMarkItem item, FS_LPCSTR csName);


	//************************************
	// Function:  SetParam
	// Param[in]: item			The input PDF content mark item object.
	// Param[in]: type			The input new parameter type.
	// Param[in]: param			The input new parameter.
	// Return:    void
	// Remarks:   Sets the parameter of the content mark item.
	// Notes:
	//************************************
	static void	SetParam(FPD_ContentMarkItem item, FPD_MarkItemParamType type, void* param);
};

/** @brief PDF content mark class. */
class CFPD_ContentMark_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: void
	// Return:    A new empty PDF content mark object.
	// Remarks:   Creates a new empty PDF content mark object.
	// Notes:
	//************************************
	static FPD_ContentMark	New(void);


	//************************************
	// Function:  Destroy
	// Param[in]: mark The input PDF content mark object.
	// Return:    void
	// Remarks:   Destroys the PDF content mark object.
	// Notes:
	//************************************
	static void	Destroy(FPD_ContentMark mark);

	
	//************************************
	// Function:  GetMCID
	// Param[in]: mark The input PDF content mark object.
	// Return:    The marked-content identifier.
	// Remarks:   Gets the marked-content identifier.
	// Notes:
	//************************************
	static FS_INT32 GetMCID(FPD_ContentMark mark);


	//************************************
	// Function:  HasMark
	// Param[in]: mark		The input PDF content mark object.	
	// Param[in]: tag		The name(tag) of the content mark item.
	// Return:    Non-zero means it has, otherwise it has not.
	// Remarks:   Checks whether the content mark has a specified content mark item.
	// Notes:
	//************************************
	static FS_BOOL HasMark(FPD_ContentMark mark, FS_LPCSTR tag);


	//************************************
	// Function:   LookupMark
	// Param[in]:  mark		The input PDF content mark object.
	// Param[in]:  tag		The name(tag) of the content mark item.
	// Param[out]: outDict  It receives the parameter(attributes) dictionary.
	// Return:    Non-zero means found one, otherwise found none.
	// Remarks:   Lookups a content mark item.
	// Notes:
	//************************************
	static FS_BOOL LookupMark(FPD_ContentMark mark, FS_LPCSTR tag, FPD_Object* outDict);

	//************************************
	// Function:  CountItems
	// Param[in]: mark	The input PDF content mark object.
	// Return:    The number of content mark data in this object.
	// Remarks:   Counts the number of content mark data in this object.
	// Notes:
	//************************************
	static FS_INT32 CountItems(FPD_ContentMark mark);


	//************************************
	// Function:  GetItem
	// Param[in]: mark		The input PDF content mark object.
	// Param[in]: index		The zero-based content mark item index in the content mark data.
	// Return:    A content mark item.
	// Remarks:   Gets the content mark item by index.
	// Notes:
	//************************************
	static FPD_ContentMarkItem GetItem(FPD_ContentMark mark, FS_INT32 index);

	//************************************
	// Function:  AddMark
	// Param[in]: mark		The input PDF content mark object.
	// Param[in]: tag		The input name(tag) of the content mark item.
	// Param[in]: dict		The parameter(attributes) dictionary of the content mark item.
	// Param[in]: bDictNeedClone	Whether the input dictionary must be copied or not.
	// Return:    void
	// Remarks:   Adds a content mark item.
	// Notes:
	//************************************
	static void AddMark(FPD_ContentMark mark, FS_LPCSTR tag, FPD_Object dict, FS_BOOL bDictNeedClone);

	//************************************
	// Function:  DeleteMark
	// Param[in]: mark		The input PDF content mark object.
	// Param[in]: tag		The name(tag) of the content mark item.
	// Return:    void
	// Remarks:   Deletes a content mark item.
	// Notes:
	//************************************
	static void DeleteMark(FPD_ContentMark mark, FS_LPCSTR tag);

	//************************************
	// Function:  DeleteLastMark
	// Param[in]: mark		The input PDF content mark object.
	// Return:    void
	// Remarks:   Deletes the last content mark item.
	// Notes:
	//************************************
	static void DeleteLastMark(FPD_ContentMark mark);

	//************************************
	// Function:  IsNull
	// Param[in]: mark		The input PDF content mark object.
	// Return:    Non-zero means <a>NULL</a>, otherwise not <a>NULL</a>.
	// Remarks:   Tests whether the content mark object is <a>NULL</a> or not.
	// Notes:
	//************************************
	static FS_BOOL IsNull(FPD_ContentMark mark);

	//************************************
	// Function:  Copy
	// Param[in]: mark				The input PDF content mark object.
	// Param[in]: pSrcContentMark	The input PDF content mark object to be copied.
	// Return:    void.
	// Remarks:   Copies the source content mark to the specified one.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3.1
	//************************************
	static void Copy(FPD_ContentMark mark, FPD_ContentMark pSrcContentMark);
};


/** @brief BaseGraphicObjcet class. */
class CFPD_PathObjectUtils_V13
{
public:

	//************************************
	// Function:  GetLineWidth
	// Param[in]: pathObjectUtils The input pathObjectUtils
	// Param[in]: fLineWidth		The input line width.
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   GetLineWidth.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL         GetLineWidth(FPD_PathObjectUtils pathObjectUtils, FS_FLOAT& fLineWidth);

	//************************************
	// Function:  SetLineWidth
	// Param[in]: pathObjectUtils The input pathObjectUtils
	// Param[in]: fLineWidth		The input line width.
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetLineWidth.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL         SetLineWidth(FPD_PathObjectUtils pathObjectUtils, FS_FLOAT fLineWidth);

	//************************************
	// Function:  SetMiterLimit
	// Param[in]: pathObjectUtils The input pathObjectUtils
	// Param[in]: fMiterLimit		The input line width.
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetMiterLimit.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL         SetMiterLimit(FPD_PathObjectUtils pathObjectUtils, FS_FLOAT fMiterLimit);

	//************************************
	// Function:  GetMiterLimit
	// Param[in]: pathObjectUtils The input pathObjectUtils
	// Param[in]: fMiterLimit		The input line width.
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   GetMiterLimit.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL         GetMiterLimit(FPD_PathObjectUtils pathObjectUtils, FS_FLOAT& fMiterLimit);

	//************************************
	// Function:  SetLineCap
	// Param[in]: pathObjectUtils	The input pathObjectUtils
	// Param[in]: cap				The input line width.
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetLineCap.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL         SetLineCap(FPD_PathObjectUtils pathObjectUtils, FPD_LineCap cap);

	//************************************
	// Function:  GetLineCap
	// Param[in]: pathObjectUtils	The input pathObjectUtils
	// Param[in]: nLinecap			The input line nlinecap.
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   GetLineCap.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL         GetLineCap(FPD_PathObjectUtils pathObjectUtils, FPD_LineCap& nLinecap);

	//************************************
	// Function:  GetLineJoin
	// Param[in]: pathObjectUtils	The input pathObjectUtils
	// Param[in]: nLineJoin			The input line nLineJoin.
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   GetLineJoin.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL         GetLineJoin(FPD_PathObjectUtils pathObjectUtils, FPD_LineJoin& nLineJoin);

	//************************************
	// Function:  SetLineJoin
	// Param[in]: pathObjectUtils	The input pathObjectUtils
	// Param[in]: nLineJoin			The input line join.
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetLineJoin.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL         SetLineJoin(FPD_PathObjectUtils pathObjectUtils, FPD_LineJoin join);

	//************************************
	// Function:  GetLineStyle
	// Param[in]: pathObjectUtils	The input pathObjectUtils
	// Param[out]: pDash				The input aDash.
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   GetLineStyle.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL         GetLineStyle(FPD_PathObjectUtils pathObjectUtils, FS_FloatArray &pDash);

	//************************************
	// Function:  SetLineStyle
	// Param[in]: pathObjectUtils	The input pathObjectUtils
	// Param[in]: aDash				The input aDash.
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetLineStyle.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL         SetLineStyle(FPD_PathObjectUtils pathObjectUtils, FS_FloatArray pDash);

	//************************************
	// Function:  GetStrokeInfo
	// Param[in]: pathObjectUtils	The input pathObjectUtils
	// Param[in]: pColor			The input color.
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   GetStrokeInfo.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL         GetStrokeInfo(FPD_PathObjectUtils pathObjectUtils, FS_BOOL & bStroke, FPD_Color& pColor);

	//************************************
	// Function:  SetStrokeInfo
	// Param[in]: pathObjectUtils	The input pathObjectUtils
	// Param[in]: bStroke			The input bStroke.
	// Param[in]: pColor			The input color.
	// Param[in]: pAddUndo			The input AddUndo.
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetStrokeInfo.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL         SetStrokeInfo(FPD_PathObjectUtils pathObjectUtils, FS_BOOL bStroke, const FPD_Color pColor, FS_BOOL bAddUndo);

	//************************************
	// Function:  GetFillInfo
	// Param[in]: pathObjectUtils	The input pathObjectUtils
	// Param[in]: pColor			The input color.
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   GetFillInfo.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL         GetFillInfo(FPD_PathObjectUtils pathObjectUtils, FS_BOOL& bFill, FPD_Color& pColor);

	//************************************
	// Function:  SetFillInfo
	// Param[in]: pathObjectUtils	The input pathObjectUtils
	// Param[in]: pColor			The input color.
	// Param[in]: pAddUndo			The input AddUndo.
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetFillInfo.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL         SetFillInfo(FPD_PathObjectUtils pathObjectUtils, FS_BOOL bFill, const FPD_Color pColor, FS_BOOL bAddUndo);

	//************************************
	// Function:  SetColorSpace
	// Param[in]: pathObjectUtils	The input pathObjectUtils
	// Param[in]: pColor			The input bFillOrStroke.
	// Param[in]: pColor			The input nColorSpace.
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetColorSpace.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void            SetColorSpace(FPD_PathObjectUtils pathObjectUtils, FS_BOOL bFillOrStroke, FS_INT32 nColorSpace);
};

class CFPD_ShadingObjectUtils_V13
{
public:
	static void                        ChangeShadingColor(FPD_ShadingObjectUtils shadingObjectUtils, FS_BOOL bUndo);
	static void                        ChangeColorUndoStart(FPD_ShadingObjectUtils shadingObjectUtils);
	static void                        ChangeColorUndoEnd(FPD_ShadingObjectUtils shadingObjectUtils);
	static void                        ChangeColorUndoCancle(FPD_ShadingObjectUtils shadingObjectUtils);
	static void                        EditReferenceLine(FPD_ShadingObjectUtils shadingObjectUtils);

	//Gradient功能专用
	static void                        SetGridientCursorColor(FPD_ShadingObjectUtils shadingObjectUtils, FS_COLORREF clr);
	static FS_COLORREF                 GetGridientCursorColor(FPD_ShadingObjectUtils shadingObjectUtils);
	static FS_COLORREF                 GetGridientColorAt(FPD_ShadingObjectUtils shadingObjectUtils, FS_INT32 nIndex);
	static FS_FLOAT                    GetGridientCursorLocation(FPD_ShadingObjectUtils shadingObjectUtils);
	static FS_INT32                    GetGridientCursorIndex(FPD_ShadingObjectUtils shadingObjectUtils);
	static void                        SetGradientColorShowRect(FPD_ShadingObjectUtils shadingObjectUtils, FS_Rect rect);
	static void                        AddGradientCursorInfoBefore(FPD_ShadingObjectUtils shadingObjectUtils, FS_DevicePoint point);
	static FS_BOOL                     AddGradientCursorInfoExcute(FPD_ShadingObjectUtils shadingObjectUtils, FS_DevicePoint point);
	static FS_BOOL                     AddGradientCursorInfoEnd(FPD_ShadingObjectUtils shadingObjectUtils);
	static FS_BOOL                     AddGradientCursorInfoDblClk(FPD_ShadingObjectUtils shadingObjectUtils, FS_DevicePoint point);
	static FS_BOOL                     DelGradientCursorInfo(FPD_ShadingObjectUtils shadingObjectUtils);
	static void                        DefaultGradientDataInit(FPD_ShadingObjectUtils shadingObjectUtils);
	static void                        SetGradientCurrentInfo(FPD_ShadingObjectUtils shadingObjectUtils, const FS_PtrArray &colorArray);
	static void                        PaintGridient(FPD_ShadingObjectUtils shadingObjectUtils, FPD_RenderDevice& device);
};

class CFPD_PathEditor_V13
{
public:
	static FS_BOOL         HasStartEdit(FPD_PathEditor edit);
	static FS_BOOL         IsClipPathMode(FPD_PathEditor edit);
	static void            AppendSubPath(FPD_PathEditor edit, FPD_GeoDrawType type);
};

class CFPD_ShadingEditor_V13
{
public:
	static FS_BOOL         IsReferenceLineWorking(FPD_ShadingEditor edit);

};

class CFPD_TextEditor_V13
{
public:
	//************************************
	// Function:  OnFontNameChanged
	// Param[in]: edit						The input FPD_TextEditor
	// Param[in]: wsFontName				The input wsFontName
	// Param[in]: wsScriptName				The input wsScriptName
	// Return:    <a>NULL</a>.
	// Remarks:   OnFontNameChanged.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void            OnFontNameChanged(FPD_TextEditor edit, FS_WideString  wsFontName, FS_BOOL bBold, FS_BOOL& bChangeItalic);

	//************************************
	// Function:  OnFontSizeChanged
	// Param[in]: edit						The input FPD_TextEditor
	// Param[in]: flFontSize				The input fontSize
	// Return:    <a>NULL</a>.
	// Remarks:   OnFontSizeChanged.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void            OnFontSizeChanged(FPD_TextEditor edit, FS_FLOAT flFontSize);

	//************************************
	// Function:  OnTextColorChanged
	// Param[in]: edit						The input FPD_TextEditor
	// Param[in]: textColor				The input textColor
	// Return:    <a>NULL</a>.
	// Remarks:   OnTextColorChanged.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void            OnTextColorChanged(FPD_TextEditor edit, FS_COLORREF textColor);

	//************************************
	// Function:  OnBoldChanged
	// Param[in]: edit						The input FPD_TextEditor
	// Param[in]: bBold						The input bBold
	// Return:    <a>NULL</a>.
	// Remarks:   OnBoldChanged.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void            OnBoldChanged(FPD_TextEditor edit, FS_BOOL bBold);

	//************************************
	// Function:  OnItalicChanged
	// Param[in]: edit						The input FPD_TextEditor
	// Param[in]: bItalic					The input bItalic
	// Param[in]: bEnabled					The input bEnabled
	// Return:    <a>NULL</a>.
	// Remarks:   OnItalicChanged.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void            OnItalicChanged(FPD_TextEditor edit, FS_BOOL bItalic, FS_BOOL& bEnabled);

	//************************************
	// Function:  OnAlignChanged
	// Param[in]: edit						The input FPD_TextEditor
	// Param[in]: dwAlign					The input dwAlign
	// Return:    <a>NULL</a>.
	// Remarks:   OnAlignChanged.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void            OnAlignChanged(FPD_TextEditor edit, FS_INT32 dwAlign);

	//************************************
	// Function:  OnCharSpaceChanged
	// Param[in]: edit						The input FPD_TextEditor
	// Param[in]: flSpace					The input flSpace
	// Return:    <a>NULL</a>.
	// Remarks:   OnCharSpaceChanged.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void            OnCharSpaceChanged(FPD_TextEditor edit, FS_FLOAT flSpace);

	//************************************
	// Function:  OnWordSpaceChanged
	// Param[in]: edit						The input FPD_TextEditor
	// Param[in]: fWordSpace					The input fWordSpace
	// Return:    <a>NULL</a>.
	// Remarks:   OnWordSpaceChanged.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void            OnWordSpaceChanged(FPD_TextEditor edit, FS_FLOAT fWordSpace);

	//************************************
	// Function:  OnCharHorzScaleChanged
	// Param[in]: edit						The input FPD_TextEditor
	// Param[in]: nScale					The input nScale
	// Return:    <a>NULL</a>.
	// Remarks:   OnCharHorzScaleChanged.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void            OnCharHorzScaleChanged(FPD_TextEditor edit, FS_INT32 nScale);

	//************************************
	// Function:  OnLineLeadingChanged
	// Param[in]: edit						The input FPD_TextEditor
	// Param[in]: flLineLeading				The input flLineLeading
	// Return:    <a>NULL</a>.
	// Remarks:   OnLineLeadingChanged.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void            OnLineLeadingChanged(FPD_TextEditor edit, FS_FLOAT flLineLeading);

	//************************************
	// Function:  OnUnderlineChanged
	// Param[in]: edit						The input FPD_TextEditor
	// Param[in]: bUnderline				The input bUnderline
	// Return:    <a>NULL</a>.
	// Remarks:   OnUnderlineChanged.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void            OnUnderlineChanged(FPD_TextEditor edit, FS_BOOL bUnderline);

	//************************************
	// Function:  OnCrossChanged
	// Param[in]: edit						The input FPD_TextEditor
	// Param[in]: bCross					The input bCross
	// Return:    <a>NULL</a>.
	// Remarks:   OnCrossChanged.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void            OnCrossChanged(FPD_TextEditor edit, FS_BOOL bCross);

	//************************************
	// Function:  OnSuperScriptChanged
	// Param[in]: edit						The input FPD_TextEditor
	// Param[in]: bSuperSet					The input bSuperSet
	// Return:    <a>NULL</a>.
	// Remarks:   OnSuperScriptChanged.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void            OnSuperScriptChanged(FPD_TextEditor edit, FS_BOOL bSuperSet);

	//************************************
	// Function:  OnSubScriptChanged
	// Param[in]: edit						The input FPD_TextEditor
	// Param[in]: bSubSet					The input bSubSet
	// Return:    <a>NULL</a>.
	// Remarks:   OnSubScriptChanged.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void            OnSubScriptChanged(FPD_TextEditor edit, FS_BOOL bSubSet);

	//************************************
	// Function:  OnWritingDirctionChanged
	// Param[in]: edit						The input FPD_TextEditor
	// Param[in]: eDir						The input eDir
	// Return:    <a>NULL</a>.
	// Remarks:   OnWritingDirctionChanged.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void            OnWritingDirctionChanged(FPD_TextEditor edit, FPD_WritingDirection eDir);

	static FS_BOOL                     IsSelect(FPD_TextEditor edit);
	static void                        Copy(FPD_TextEditor edit);
	static void                        Paste(FPD_TextEditor edit);
	static void                        Cut(FPD_TextEditor edit);
	static void                        Delete(FPD_TextEditor edit);
	static void                        SelectAll(FPD_TextEditor edit);
	static FS_BOOL                     IsAdd(FPD_TextEditor edit);
    static void                        GetICaretFormData(FPD_TextEditor edit, FS_FloatPoint& head, FS_FloatPoint& foot);
    static void                        GetTextObjectMatrix(FPD_TextEditor edit, FS_AffineMatrix & mt);
};


class CFPD_TextObjectUtils_V13
{
public:
	//************************************
	// Function:  GetFontSize
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: fFontSize					The input fFontSize
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   GetFontSize.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          GetFontSize(FPD_TextObjectUtils objectUtils, FS_FLOAT& fFontSize);

	//************************************
	// Function:  SetFontSize
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: fFontSize					The input fFontSize
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetFontSize.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          SetFontSize(FPD_TextObjectUtils objectUtils, FS_FLOAT fFontSize);

	//************************************
	// Function:  GetHorizontalScale
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: nHorzScale					The input nHorzScale
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   GetHorizontalScale.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          GetHorizontalScale(FPD_TextObjectUtils objectUtils, FS_INT32& nHorzScale);

	//************************************
	// Function:  SetHorizontalScale
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: nHorzScale				The input nHorzScale
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetHorizontalScale.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          SetHorizontalScale(FPD_TextObjectUtils objectUtils, FS_INT32 nHorzScale);

	//************************************
	// Function:  SetCharSpace
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: fCharSpace				The input fCharSpace
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetCharSpace.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          SetCharSpace(FPD_TextObjectUtils objectUtils, FS_FLOAT fCharSpace);

	//************************************
	// Function:  GetCharSpace
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: fCharSpace				The input fCharSpace
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   GetCharSpace.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          GetCharSpace(FPD_TextObjectUtils objectUtils, FS_FLOAT& fCharSpace);

	//************************************
	// Function:  GetWordSpace
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: fWordSpace				The input fWordSpace
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   GetWordSpace.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          GetWordSpace(FPD_TextObjectUtils objectUtils, FS_FLOAT& fWordSpace);

	//************************************
	// Function:  SetWordSpace
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: fWordSpace				The input fWordSpace
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetWordSpace.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          SetWordSpace(FPD_TextObjectUtils objectUtils, FS_FLOAT fWordSpace);

	//************************************
	// Function:  SetFont
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: wsFontName				The input wsFontName
	// Param[in]: bBold						The input bBold
	// Param[in]: bItalic					The input bItalic
	// Param[in]: nOPType					The input nOPType
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetFont.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          SetFont(FPD_TextObjectUtils objectUtils, FS_WideString wsFontName, FS_BOOL bBold, FS_BOOL bItalic, FPD_FONTOPERATION nOPType);

	//************************************
	// Function:  GetFont
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: wsFontName				The input wsFontName
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   GetFont.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          GetFont(FPD_TextObjectUtils objectUtils, FS_WideString wsFontName, FS_BOOL bFullName);

	//************************************
	// Function:  GetTextMode
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: mode						The input mode
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   GetTextMode.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          GetTextMode(FPD_TextObjectUtils objectUtils, FPD_TextMode& mode);

	//************************************
	// Function:  SetTextMode
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: mode						The input mode
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetTextMode.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          SetTextMode(FPD_TextObjectUtils objectUtils, FPD_TextMode mode);

	//************************************
	// Function:  CanRemoveTextKerning
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   CanRemoveTextKerning.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          CanRemoveTextKerning(FPD_TextObjectUtils objectUtils);

	//************************************
	// Function:  RemoveTextKerning
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Return:    <a>NULL</a>.
	// Remarks:   RemoveTextKerning.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void             RemoveTextKerning(FPD_TextObjectUtils objectUtils);

	//************************************
	// Function:  CanMergeText
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   CanMergeText.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          CanMergeText(FPD_TextObjectUtils objectUtils);

	//************************************
	// Function:  MergeText
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   MergeText.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          MergeText(FPD_TextObjectUtils objectUtils);

	//************************************
	// Function:  CanSplitText
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   CanSplitText.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          CanSplitText(FPD_TextObjectUtils objectUtils);

	//************************************
	// Function:  SplitText
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: aSplitSubArray			The input aSplitSubArray
	// Param[in]: nCount					The input nCount
	// Return:    <a>NULL</a>.
	// Remarks:   SplitText.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void             SplitText(FPD_TextObjectUtils objectUtils, FS_INT64* aSplitSubArray, FS_INT32 nCount);

	//************************************
	// Function:  CanText2Path
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   CanText2Path.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          CanText2Path(FPD_TextObjectUtils objectUtils);

	//************************************
	// Function:  GetStrokeInfo
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: bStroke					The input bStroke
	// Param[in]: color						The input color
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   GetStrokeInfo.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          GetStrokeInfo(FPD_TextObjectUtils objectUtils, FS_BOOL& bStroke, FPD_Color& pColor);

	//************************************
	// Function:  SetStrokeInfo
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: bStroke					The input bStroke
	// Param[in]: pColor					The input pColor
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetStrokeInfo.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          SetStrokeInfo(FPD_TextObjectUtils objectUtils, FS_BOOL bStroke, const FPD_Color pColor, FS_BOOL bAddUndo);

	//************************************
	// Function:  GetFillInfo
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: bFill						The input bFill
	// Param[in]: color						The input color
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   GetFillInfo.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          GetFillInfo(FPD_TextObjectUtils objectUtils, FS_BOOL& bFill, FPD_Color& pColor);

	//************************************
	// Function:  SetFillInfo
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: bFill						The input bFill
	// Param[in]: color						The input color
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetFillInfo.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL          SetFillInfo(FPD_TextObjectUtils objectUtils, FS_BOOL bFill, const FPD_Color pColor, FS_BOOL bAddUndo);

	//************************************
	// Function:  SetColorSpace
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// Param[in]: bFillOrStroke			    The input bFillOrStroke.
	// Param[in]: nColorSpace			    The input nColorSpace.
	// Return:    <a>TRUE/FALSE</a>.
	// Remarks:   SetColorSpace.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void             SetColorSpace(FPD_TextObjectUtils objectUtils, FS_BOOL bFillOrStroke, FS_INT32 nColorSpace);

	//************************************
	// Function:  GetTextColorInfo
	// Param[in]: objectUtils				The input FPD_TextObjectUtils
	// param[out]  bOutHasColor             It receives the exist flag of color.
	// param[out]  pOutColor                It receives color struct.
	// Return:    <a>NULL</a>.
	// Remarks:   GetTextColorInfo.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static void             GetTextColorInfo(FPD_TextObjectUtils objectUtils, FS_BOOL& bOutHasColor, FPD_Color& pOutColor);

	//************************************
	// Function:  IsWordSpaceValid
	// Return:    <a>FPD_ValidFlag</a>.
	// Remarks:   IsWordSpaceValid.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FPD_ValidFlag        IsWordSpaceValid(FPD_TextObjectUtils objectUtils);

	//************************************
	// Function:  GetFontBoldInfo
	// Return:    <a>TRUE/FLASE</a>.
	// Remarks:   GetFontBoldInfo.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL				GetFontBoldInfo(FPD_TextObjectUtils objectUtils, FS_BOOL& bIsEnable);

	//************************************
	// Function:  GetFontItalic
	// Return:    <a>TRUE/FLASE</a>.
	// Remarks:   GetFontItalic.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 10.0.0
	//************************************
	static FS_BOOL				GetFontItalic(FPD_TextObjectUtils objectUtils, FS_BOOL& bIsEnable);
};


class CFPD_GraphicObject_V13
{
public:

	static FPD_GraphicObject  Create(FPD_PageObject pPageObject,
		FS_WORD nIndex,
		FS_POSITION pos,
		FPD_Page page,
		FS_BOOL bIsInForm,
		FS_PtrArray aFormObj,
		const FS_AffineMatrix MtForm,
		const FS_DWordArray aFormIdx);

	static void Destroy(FPD_GraphicObject obj);

	static FPD_Page  GetPage(FPD_GraphicObject graphicObj);

	static FPD_PageObject GetGraphicObject(FPD_GraphicObject graphicObj);

	static FS_DWORD  GetIndex(FPD_GraphicObject graphicObj);

	static FS_POSITION  GetPosition(FPD_GraphicObject graphicObj);

	static FS_BOOL  IsInForm(FPD_GraphicObject graphicObj);

	static void  GetFormIndex(FPD_GraphicObject graphicObj, FS_DWordArray& pdwArr);

	static void  GetFormObjects(FPD_GraphicObject graphicObj, FS_PtrArray& pdwArr);

	static FS_AffineMatrix  GetFormMatrix(FPD_GraphicObject graphicObj);

};


/** @brief CharProcess class. */
class CFPD_GraphicEditor_V13
{
public:
	static FPD_ObjectFilter GetType(FPD_GraphicEditor pEditor);
	static FS_BOOL          EndEdit(FPD_GraphicEditor pEditor, FS_INT32 mode);
};

/** @brief CGraphicObjectUtils class. */
class CFPD_GraphicObjectUtils_V13
{
public:

	//attribute
	static FS_BOOL         GetHeight(FPD_GraphicObjectUtils objUtils, FS_FLOAT& fHeight);
	static FS_BOOL         SetHeight(FPD_GraphicObjectUtils objUtils, FS_FLOAT fHeight);
	static FS_BOOL         SetWidth(FPD_GraphicObjectUtils objUtils, FS_FLOAT fWidth);
	static FS_BOOL         GetWidth(FPD_GraphicObjectUtils objUtils, FS_FLOAT& fWidth);
	static FS_BOOL         GetXPosition(FPD_GraphicObjectUtils objUtils, FS_FLOAT& fPosX);
	static FS_BOOL         SetXPosition(FPD_GraphicObjectUtils objUtils, FS_FLOAT fPosX);
	static FS_BOOL         GetYPosition(FPD_GraphicObjectUtils objUtils, FS_FLOAT& fPosY);
	static FS_BOOL         SetYPosition(FPD_GraphicObjectUtils objUtils, FS_FLOAT fPosY);
	static FS_BOOL         GetOpacity(FPD_GraphicObjectUtils objUtils, FS_INT32& nOpacity);
	static FS_BOOL         SetOpacity(FPD_GraphicObjectUtils objUtils, FS_INT32 nOpacity, FS_BOOL bUndo);

	//transform
	static void            Rotate(FPD_GraphicObjectUtils objUtils, FS_FLOAT fRotate);
	static void            Flip(FPD_GraphicObjectUtils objUtils, FS_BOOL bVertical);
	static void            Shear(FPD_GraphicObjectUtils objUtils, FS_FLOAT fAngle);
	static void            Move(FPD_GraphicObjectUtils objUtils, FPD_EditorPage destPage, const FS_DevicePoint& ptOffset);
	static void            Align(FPD_GraphicObjectUtils objUtils, FPD_AlignMode mode);
	static void            Center(FPD_GraphicObjectUtils objUtils, FPD_CenterMode mode);
	static void            Distribute(FPD_GraphicObjectUtils objUtils, FS_BOOL bVertical);
	static void            Size(FPD_GraphicObjectUtils objUtils, FPD_SizeMode mode);
	static void            Scale(FPD_GraphicObjectUtils objUtils, FS_Rect rcNew);
	static void            ScaleFloat(FPD_GraphicObjectUtils objUtils, FS_FLOAT fDelta, FS_BOOL bHorizontal);
	//rendering order
	static void            ChangeRenderingOrder(FPD_GraphicObjectUtils objUtils, FPD_RendingOrder operation);
	static FS_BOOL         CanChangeRenderingOrder(FPD_GraphicObjectUtils objUtils, FPD_RendingOrder operation);
	//clip
	static void            ClearClipPath(FPD_GraphicObjectUtils objUtils);
	static FS_BOOL         EditClipPath(FPD_GraphicObjectUtils objUtil);
	static FS_BOOL         AddClipPath(FPD_GraphicObjectUtils objUtil, FPD_GeoDrawType type);

	static FPD_GraphicObjectUtilsType	GetType(FPD_GraphicObjectUtils objUtils);
	static void            Cut(FPD_GraphicObjectUtils objUtils);
	static FS_BOOL         Copy(FPD_GraphicObjectUtils objUtils);
	static void            Delete(FPD_GraphicObjectUtils objUtils);
	static FS_BOOL         SavePageArchive(FPD_IPageEditor pPageEditor, FS_LPCBYTE &pBuffer, FS_DWORD &size);
	static FS_BOOL         ReadPageArchive(FPD_IPageEditor pPageEditor, const FS_LPCBYTE &pBuffer, const FS_DWORD &size,
		FS_DevicePoint &ptPaste, FS_PtrArray& vecObject);
};

/** @brief ImageEditor class. */
class CFPD_ImageEditor_V13
{
public:
	static void            SaveAsImage(FPD_ImageEditor imageEditor);
	static void            PreviewFilter(FPD_ImageEditor imageEditor, FPD_Filter eFilter);
	static void            CancelPreviewFilter(FPD_ImageEditor imageEditor, FS_BOOL bUpdateView);
	static void            AdjustFilter(FPD_ImageEditor imageEditor, FPD_Filter eFilter);

	static void                        RotateVertically(FPD_ImageEditor imageEditor, FPD_EditorPage editorPage);
	static void                        RotateHorizontally(FPD_ImageEditor imageEditor, FPD_EditorPage editorPage);
	static FS_BOOL                     Replace(FPD_ImageEditor imageEditor, FPD_EditorPage editorPage);
	static void                        Copy(FPD_ImageEditor imageEditor, FPD_EditorPage editorPage);
	static void                        Paste(FPD_ImageEditor imageEditor, FPD_EditorPage editorPage);
	static void                        DeleteRegion(FPD_ImageEditor imageEditor, FPD_EditorPage editorPage, FS_BOOL bCopy, FS_BOOL bBackgroundColor, FS_BOOL bIgnoreSelection);
	static void                        ZoomIn(FPD_ImageEditor imageEditor);
	static void                        ZoomTo(FPD_ImageEditor imageEditor, FS_FLOAT fScale, FS_BOOL bUpdate);
	static void                        ZoomOut(FPD_ImageEditor imageEditor);
	static void                        AdjustColor(FPD_ImageEditor imageEditor, FPD_EditorPage editorPage, FPD_AdjustRgb nColorRGB, FS_INT32 nPercentage);
	static FS_BOOL                     LoadFromClipboard(FPD_ImageEditor imageEditor, FS_LPSTR pDIB);

	static void                        OnClientRectChanged(FPD_ImageEditor imageEditor, const FS_Rect rect);
	static void                        SetActionType(FPD_ImageEditor imageEditor, FPD_ImageEditorToolActionType nType);
	static FPD_ImageEditorToolActionType	GetActionType(FPD_ImageEditor imageEditor);
	static FS_BOOL						OnSetCursor(FPD_ImageEditor imageEditor);
	static void						OnSize(FPD_ImageEditor imageEditor);
	static FPD_IOptionData				GetCurrentOptionData(FPD_ImageEditor imageEditor);
	static FPD_IPublicOptionData			GetPublicOptionData(FPD_ImageEditor imageEditor);
	static FPD_ImageHistogramData			GetImageHistogramData(FPD_ImageEditor imageEditor);
	static void CompositeImageObject(FPD_ImageEditor imageEditor, FS_INT32 nOpacity, FS_INT32 nFill, FS_INT32 nBlendMode);
	static void CancelEdit(FPD_ImageEditor imageEditor, FPD_EditorPage editorPage);
};

/** @brief Page enumeration handler interface. */
class CFPD_EditorContextHandler : public pageeditor::IEditorContext
{
public:
	CFPD_EditorContextHandler(FPD_EditorContextCallbacks callbacks);

	virtual CPDF_Matrix									 GetRenderMatrix(pageeditor::EditorPage editorPage);
	virtual window::IPWL_FontMap*						 GetFontMap(CPDF_Document* pDoc);
	virtual pageeditor::IOperationNotify*                GetOperationNotify();
	virtual pageeditor::IClipboardHandler*				 GetClipboardtHandler();
	virtual pageeditor::IPopupMenuHandler*               GetPopupMenuHandler();
	virtual pageeditor::ITip*                            GetTip();
	virtual void                             GetGridMainLine(pageeditor::EditorPage editorPage, std::vector<FX_INT32>& XArray, std::vector<FX_INT32>& YArray);
	virtual void                             GetGridLine(pageeditor::EditorPage editorPage, std::vector<FX_INT32>& XArray, std::vector<FX_INT32>& YArray);
	virtual FX_BOOL                          IsSnap2GridEnabled();
	virtual void                             GetSystemDPI(FX_FLOAT& fHorzDpi, FX_FLOAT& fVertDpi);
	virtual CPDF_Page*                       GetLatestPDFPage(pageeditor::EditorPage editorPage, std::size_t nPageIndex);
	// 	//IFormatTools
	virtual std::wstring                     GetToolbarFontName();
	virtual void                             UpdateTextFormat(const edit::CFVT_WordProps* pt, const edit::CFVT_SecProps *sp, pageeditor::TextProperty textProperty);
	virtual void                             ActiveTextFormat(const edit::CFVT_WordProps* pt, const edit::CFVT_SecProps *sp, std::wstring wsBarName, FX_BOOL bIsNewObject);
	virtual std::wstring                     GetFontFaceName(const std::wstring& lpwsScriptName);

	virtual CFX_FloatRect                    GetPageVisibleRect(pageeditor::EditorPage editorPage);
	virtual FX_FLOAT                         GetPageViewScale(pageeditor::EditorPage editorPage);
	virtual CFX_DIBitmap*                    GetRotateCtrlIcon();
	virtual std::wstring                     GetString(pageeditor::UIStringResourceID strID);
	virtual void                             LocalizeDot(std::wstring& str);
	virtual FX_BOOL                          IsProEditingEnabled();
	virtual CFX_DIBitmap*                    LoadPNGFromID(pageeditor::UIImageResourceID png);

	virtual void                             GetScrollPos(pageeditor::EditorPage editorPage, FX_INT32& nHorz, FX_INT32& nVert);
	virtual pageeditor::EditorPage           GetDefaultEditorPage();


	virtual FX_RECT                          GetPageRect(pageeditor::EditorPage editorPage);
	virtual void                             GetTemplate(pageeditor::EditorPage editorPage, CPDF_GraphicsObject * pGraphicsObject);
	virtual FX_BOOL                          IsDirectionEnabled();
	virtual FX_BOOL                          GetMainParagraphDir();
	virtual FX_BOOL                          IsUseSystemSelectionColor();
	virtual FX_DWORD                         GetSystemHightColor();
	virtual FX_BOOL                          GetDefaultTextColor(FX_COLORREF& color);
	virtual FX_BOOL                          GetIsHindDigit();
	virtual FX_BOOL                          IsRepalceDocColor();
	virtual FX_COLORREF                      GetDocumentClr();
	virtual FX_BOOL                          IsForceBWOnly();
	virtual window::IPWL_SpellCheck*         SetNewSpellCheck();
	virtual CPDF_RenderOptions*              GetRenderOptions(pageeditor::EditorPage editpage);
	virtual void                             TextChanged(pageeditor::EditorPage editorPage);

	virtual FX_BOOL                          IsShiftKeyDown();
	virtual FX_BOOL                          IsControlKeyDown();
	virtual FX_BOOL                          IsInsertKeyDown();

	//image editor
	virtual void                             GetImageViewScrollPos(FX_INT32& nHorz, FX_INT32& nVert);
	virtual FX_RECT                          GetImageViewClientRect();
	virtual CFX_Point                        GetImageViewCursorPos();
	virtual std::wstring                     GetTempPathPE();
	virtual FX_LPSTR                         GlobalAllocMemory(FX_DWORD nSize, void* &pOutHandle);
	virtual void                             GlobalFreeMemory(void* pOutHandle);

	virtual FX_BOOL                          GetWIPPluginProviderAllowedPaste();
	virtual CFX_ByteString                   GetBitmapInfo(const CFX_DIBitmap* pBitmap);
    virtual FX_RECT                          GetClientRect(pageeditor::EditorPage editpage);

	void* GetClientData() { return m_callbacks.clientData; }
public:
	FPD_EditorContextCallbacksRec m_callbacks;
};


class CFPD_IPageEditor_V13
{
public:
	static FS_BOOL			StartEdit(FPD_IPageEditor pageEditor);
	static void               RestartEdit(FPD_IPageEditor pageEditor);
	static FS_BOOL			EndEdit(FPD_IPageEditor pageEditor);


	static FS_BOOL			HasSelection(FPD_IPageEditor pageEditor);
	static FS_INT32			CountSelection(FPD_IPageEditor pageEditor);
	static void				GetSelection(FPD_IPageEditor pageEditor, FS_PtrArray arr);
	static FPD_ExistType      HaveFillColorInSelection(FPD_IPageEditor pageEditor);
	static FPD_EditorPage     GetActivePage(FPD_IPageEditor pageEditor);
	static void				SetObjectFilter(FPD_IPageEditor pageEditor, FPD_ObjectFilter type);

	static FS_BOOL            OnVKUP(FPD_IPageEditor pageEditor, FS_INT32 nKeyCode, FS_BOOL bCtrl, FS_BOOL bShift);
	static FS_BOOL            OnVKDOWN(FPD_IPageEditor pageEditor, FS_INT32 nKeyCode, FS_BOOL bCtrl, FS_BOOL bShift);
	static FS_BOOL            OnLeftButtonDown(FPD_IPageEditor pageEditor, FPD_EditorPage editorPage, FS_DevicePoint pt, FS_BOOL bCtrl, FS_BOOL bShift);
	static FS_BOOL            OnLeftButtonUp(FPD_IPageEditor pageEditor, FPD_EditorPage editorPage, FS_DevicePoint pt, FS_BOOL bCtrl, FS_BOOL bShift);
	static FS_BOOL            OnLeftButtonDblClk(FPD_IPageEditor pageEditor, FPD_EditorPage editorPage, FS_DevicePoint pt, FS_BOOL bCtrl);
	static FS_BOOL            OnMouseMove(FPD_IPageEditor pageEditor, FPD_EditorPage editorPage, FS_DevicePoint pt, FS_BOOL bShift, FS_BOOL bCtrl);
	static FS_BOOL            OnMouseWheel(FPD_IPageEditor pageEditor, FPD_EditorPage editorPage, FS_DevicePoint pt, FS_SHORT zDelta);
	static FS_BOOL            OnRightButtonDown(FPD_IPageEditor pageEditor, FPD_EditorPage editorPage, FS_DevicePoint pt, FS_BOOL bCtrl, FS_BOOL bShift);
	static FS_BOOL            OnRightButtonUp(FPD_IPageEditor pageEditor, FPD_EditorPage editorPage, FS_DevicePoint pt, FS_BOOL bCtrl, FS_BOOL bShift);
	static void               OnReleaseCapture(FPD_IPageEditor pageEditor, FS_DevicePoint pt, FS_BOOL bCtrl, FS_BOOL bShift);

	static FS_BOOL            OnPaint(FPD_IPageEditor pageEditor, FPD_RenderDevice& device);
	static void*              GetObjectAtPoint(FPD_IPageEditor pageEditor, FPD_EditorPage editorPage, FS_DevicePoint pt);

	static FS_BOOL            CanPaste(FPD_IPageEditor pageEditor);
	static FS_BOOL            Paste(FPD_IPageEditor pageEditor);
	static void               SelectAll(FPD_IPageEditor pageEditor);

	static FPD_GraphicEditor         GetGraphicEditor(FPD_IPageEditor pageEditor);
	static FPD_GraphicObjectUtils    GetGraphicObjectUtils(FPD_IPageEditor pageEditor);


	static FS_BOOL			CreatePath(FPD_IPageEditor PageEditor, FPD_GeoDrawType type);
	static FS_BOOL			CreateShading(FPD_IPageEditor PageEditor, FPD_GeoDrawType type);
	static FS_BOOL			CreateImage(FPD_IPageEditor PageEditor, const FS_LPCWSTR sPathFile, FS_FileReadHandler pRetFileRead);
	static FS_BOOL			CreateText(FPD_IPageEditor PageEditor, FPD_PageObject pTextObjectTemplate);

	static FPD_IPageEditor	CreatePageEditor(FPD_EditorContextCallbacks pContext);
	static void				DestroyPageEditor(FPD_IPageEditor pPageEditor);
	static void				ClearSelection(FPD_IPageEditor PageEditor, FS_BOOL bRefresh);
	static FS_BOOL            Text2Path(FPD_IPageEditor PageEditor);
	static void               OnChar(FPD_IPageEditor PageEditor, FS_WCHAR Key);
	static FS_BOOL            EditGraphicObject(FPD_IPageEditor PageEditor, FPD_EditorPage eidtorPage, FS_PtrArray graphicObj, FPD_Operation type);
	static FS_BOOL            CreateImage2(FPD_IPageEditor PageEditor, FS_INT32 nWidth, FS_INT32 nHeight, FS_COLORREF clr, FS_INT32 nImageDPIX, FS_INT32 nImageDPIY, FS_BOOL bCompress);
};



/** @brief IUndoItem class. */
class CFPD_IUndoItem_V13
{
public:
	static void                 OnUndo(FPD_IUndoItem item);
	static void                 OnRedo(FPD_IUndoItem item);
	static void                 OnRelease(FPD_IUndoItem item);
};




class CFPD_IClipboardHandler : public pageeditor::IClipboardHandler
{
public:
	CFPD_IClipboardHandler(FPD_ClipBoardHandlerCallbacksRec* pcallbacks);
	virtual void            CopyGraphicObject();
	virtual FX_BOOL         CanPasteGraphicObject();
	virtual std::vector<CPDF_GraphicsObject*>    PasteGraphicObject();
	virtual void                                 GetClipboardText(pageeditor::EditorPage editorPage, std::wstring& wsText);
	virtual void                                 SetClipboardText(pageeditor::EditorPage editorPage, std::wstring sSelText);
	virtual FX_BOOL                              IsFormatAvailable(pageeditor::ObjectFilter type);
	//image editor
	virtual FX_BOOL PasteBitmap();
	virtual void    CopyBitmap(void* pHandle);

	void* GetClientData() { return m_callbacks.clientData; }

public:
	FPD_ClipBoardHandlerCallbacksRec m_callbacks;
};

class CFPD_IClipboard_V13
{
public:
	static FPD_IClipboardHandler New(FPD_ClipBoardHandlerCallbacks callbacks);

	static void Destroy(FPD_IClipboardHandler handler);
};


class CFPD_IPopupMenuHandler : public pageeditor::IPopupMenuHandler
{
public:
	CFPD_IPopupMenuHandler(FPD_PopupMenuHandlerCallbacksRec* pcallbacks);
	virtual pageeditor::PE_HMENU        CreatePopupMenu();
	virtual pageeditor::PE_HMENU        CreatePopupSubMenu();
	virtual void            AppendMenuItem(pageeditor::PE_HMENU hMenu, FX_DWORD nIDItem);
	virtual void            AppendSubMenu(pageeditor::PE_HMENU hMenu, FX_DWORD nIDItem, pageeditor::PE_HMENU hSubMenu);
	virtual void            RemoveMenuItem(pageeditor::PE_HMENU hMenu, FX_DWORD nIdentify, pageeditor::IdentifyMode mode);
	virtual void            AppendSeparator(pageeditor::PE_HMENU hMenu);
	virtual void            EnableMenuItem(pageeditor::PE_HMENU hMenu, FX_DWORD nIdentify, FX_BOOL bEnabled, pageeditor::IdentifyMode mode);
	virtual FX_DWORD        TrackPopupMenu(pageeditor::PE_HMENU hMenu, FX_INT32 x, FX_INT32 y, pageeditor::EditorPage editorPage);
	virtual void            DestroyMenu(pageeditor::PE_HMENU hMenu);
	virtual FX_BOOL         OnCreateDlg(pageeditor::PopupMenuItem nIDItem, FX_INT32 &value);
	virtual FX_BOOL         IsMenuEnabled(pageeditor::PE_HMENU hMenu);
	virtual void            CheckMenuItem(pageeditor::PE_HMENU hMenu, FX_DWORD nIDItem, FX_BOOL bCheck);
	virtual void            InsertMenuItem(pageeditor::PE_HMENU hMenu, FX_INT32 nPosition, FX_INT32 nNewPos, CFX_WideString lpNewItem);
	virtual void            InsertSeparator(pageeditor::PE_HMENU hMenu, FX_INT32 nPosition);

	void* GetClientData() { return m_callbacks.clientData; }

public:
	FPD_PopupMenuHandlerCallbacksRec m_callbacks;
};

class CFPD_IPopupMenu_V13
{
public:
	static FPD_IPopupMenuHandler New(FPD_PopupMenuHandlerCallbacks callbacks);

	static void Destroy(FPD_IPopupMenuHandler handler);
};

class CFPD_ITipHandler : public pageeditor::ITip
{
public:
	CFPD_ITipHandler(FPD_TipCallbacks callbacks) {
		memset(&m_callbacks, 0, sizeof(FPD_TipCallbacksRec));
		this->m_callbacks = *callbacks;
		if (callbacks->lStructSize < sizeof(FPD_TipCallbacksRec))
		{
			FS_BYTE * pAttress = (FS_BYTE *)&m_callbacks;
			memset(pAttress + callbacks->lStructSize, 0, sizeof(FPD_TipCallbacksRec) - callbacks->lStructSize); //set new create method to 0.
		}
	}
	virtual void                    Show(FX_BOOL bVisible, FX_BOOL bDelay);
	virtual void                    MoveTo(const CFX_Point& point);
	virtual void                    SetTip(const std::wstring& str, pageeditor::EditorPage editorPage);

public:
	FPD_TipCallbacksRec m_callbacks;
	void* GetClientData() { return m_callbacks.clientData; }
};

class CFPD_ITip_V13
{
public:
	static FPD_ITipHandler New(FPD_TipCallbacks callbacks);

	static void Destroy(FPD_ITipHandler handler);
};

class CFPD_IOperationNotifyHandler : public pageeditor::IOperationNotify
{
public:
	typedef CFX_ArrayTemplate<const pageeditor::CGraphicObject*> CFX_GraphicArray;

	CFPD_IOperationNotifyHandler(FPD_IOperationNotifyCallbacks callbacks) {
		memset(&m_callbacks, 0, sizeof(FPD_IOperationNotifyCallbacksRec));
		this->m_callbacks = *callbacks;
		if (callbacks->lStructSize < sizeof(FPD_IOperationNotifyCallbacksRec))
		{
			FS_BYTE * pAttress = (FS_BYTE *)&m_callbacks;
			memset(pAttress + callbacks->lStructSize, 0, sizeof(FPD_IOperationNotifyCallbacksRec) - callbacks->lStructSize); //set new create method to 0.
		}
	}

	virtual void OnAddUndo(pageeditor::IUndoItem* pUndoItem, FX_BOOL bEdit);
	virtual void OnUndoRedoBeginEdit(void(*func)(void* pClientData), void* pClientData, FX_BOOL bOwnUndo);
	virtual void OnUndoRedoEndEdit(FX_BOOL bOwnUndo);
	virtual void OnContentChange(pageeditor::Operation type, pageeditor::EditorPage editorPage, const std::vector<pageeditor::CGraphicObject*>::const_iterator itBegin, const std::vector<pageeditor::CGraphicObject*>::const_iterator itEnd);
	virtual void OnContentChangeStart(pageeditor::Operation type, pageeditor::EditorPage editorPage, const std::vector<pageeditor::CGraphicObject*>::const_iterator itBegin, const std::vector<pageeditor::CGraphicObject*>::const_iterator itEnd);
	virtual void OnContentChangeEnd(pageeditor::Operation type, pageeditor::EditorPage editorPage, const std::vector<pageeditor::CGraphicObject*>::const_iterator itBegin, const std::vector<pageeditor::CGraphicObject*>::const_iterator itEnd, FX_BOOL bChangeMark = true);
	virtual void OnContentChangeCancle(pageeditor::EditorPage editorPage);
	virtual void OnEnsurePageVisible(pageeditor::EditorPage editorPage, CFX_FloatPoint rcLeftTop);
	virtual void OnInvalid(pageeditor::EditorPage editorPage, std::vector<FX_RECT>& invalidRects);
	virtual void OnPageInvalid(pageeditor::EditorPage editorPage);
	virtual void OnSelectionChange();
	virtual void OnCursorChange(pageeditor::EditorPage editorPage, pageeditor::UICursorResourceType type);
	virtual void OnSetPropsAsDefault(pageeditor::GraphicObjectUtilsType type, const std::vector<pageeditor::CGraphicObject*>::const_iterator itBegin, const std::vector<pageeditor::CGraphicObject*>::const_iterator itEnd);
	//virtual void OnGoToPageViewByRect(pageeditor::EditorPage editorPage, CFX_FloatRect rc);
	virtual FX_BOOL OnShowMessageBox(pageeditor::EditorPage editorPage, pageeditor::UIDialogResourceID id, FX_DWORD flag);
	virtual void OnSavePageState(pageeditor::EditorPage editorPage);
	virtual void OnRestorePageState();
	virtual void OnKillTimer(FX_DWORD nTimerID);
	virtual FX_INT32 OnSetTimer(FX_DWORD nIDEvent, FX_DWORD nElapse, pageeditor::TIMER_CALLBACK callback);
	virtual void OnSetCurrentCapture(pageeditor::EditorPage editorPage, void* pCapData);
	virtual void OnReleaseCurrentCapture(pageeditor::EditorPage editorPage);

	virtual FX_BOOL OnEnterEdit(pageeditor::EditorPage editorPage, pageeditor::ObjectFilter nObjectType, void* pParam);
	virtual FX_BOOL OnExistEdit(pageeditor::EditorPage editorPage, pageeditor::ObjectFilter nObjectType);
	virtual FX_BOOL OnShowSysColorDlg(FX_COLORREF& color);
	virtual void OnImageViewInvalid(const FX_RECT* pInvalidRects, FX_BOOL bErase);
	virtual void OnImageViewSetCapture();
	virtual void OnImageViewReleaseCapture();
	virtual void OnImageViewSetFocus();
	virtual void OnImageViewSetScrollSizes(CFX_Point sizeTotal, CFX_Point sizePage, CFX_Point sizeLine);
	virtual void OnImageViewChangeZoomScaleInfo(FX_FLOAT dbScale, FX_BOOL bErase);
	virtual void OnImageViewCreateDIBSection(pageeditor::BitMapInfo* bi);
	virtual void OnImageHistogramDataChanged(const pageeditor::ImageHistogramData& data);
	virtual FX_BOOL OnImageViewGetOpenFileName(FX_WCHAR* pSzFilter, FX_WCHAR* pSzOutFile);
	virtual FX_BOOL OnImageViewGetSaveFileName(FX_WCHAR* pSzFilter, FX_WCHAR* pSzOutFile);
	virtual void OnUpdateColor(FX_COLORREF color);
	virtual void OnCaretChange(const edit::CFVT_SecProps& secProps, const edit::CFVT_WordProps& wordProps, const std::wstring& sFontName);
    virtual void OnRegisterKoreanInputHandler(pageeditor::EditorPage editorPage);
public:
	FPD_IOperationNotifyCallbacksRec m_callbacks;
	void* GetClientData() { return m_callbacks.clientData; }
};

class CFPD_IOperationNotify_V13
{
public:
	static FPD_IOperationNotifyHandler New(FPD_IOperationNotifyCallbacks callbacks);

	static void Destroy(FPD_IOperationNotifyHandler handler);
};


class CFPD_IPublicOptionData_V13
{
public:
	static void SetForegroundColor(FPD_IPublicOptionData optionData, FS_COLORREF color);
	static FS_COLORREF GetForegroundColor(FPD_IPublicOptionData optionData);
	static void SetBackgroundColor(FPD_IPublicOptionData optionData, FS_COLORREF color);
	static FS_COLORREF GetBackgroundColor(FPD_IPublicOptionData optionData);
	static void SetLayerOpacity(FPD_IPublicOptionData optionData, FS_INT32 value);
	static FS_INT32 GetLayerOpacity(FPD_IPublicOptionData optionData);
	static void SetLayerFill(FPD_IPublicOptionData optionData, FS_INT32 value);
	static FS_INT32 GetLayerFill(FPD_IPublicOptionData optionData);
	static void SetLayerBlendMode(FPD_IPublicOptionData optionData, FPD_ImageEditoBlendMode value);
	static FPD_ImageEditoBlendMode GetLayerBlendMode(FPD_IPublicOptionData optionData);
};

class CFPD_IBaseBrushOptionData_V13
{
public:
	static void SetBrushDiameter(FPD_IBaseBrushOptionData optionData, FS_INT32 value);
	static FS_INT32 GetBrushDiameter(FPD_IBaseBrushOptionData optionData);
	static void SetBrushHardness(FPD_IBaseBrushOptionData optionData, FS_INT32 value);
	static FS_INT32 GetBrushHardness(FPD_IBaseBrushOptionData optionData);
};


class CFPD_IBrushOptionData_V13
{
public:
	static void SetBlendMode(FPD_IBrushOptionData optionData, FPD_ImageEditoBlendMode value);
	static FPD_ImageEditoBlendMode GetBlendMode(FPD_IBrushOptionData optionData);
	static void SetFlow(FPD_IBrushOptionData optionData, FS_BYTE value);
	static FS_BYTE GetFlow(FPD_IBrushOptionData optionData);
	static void SetOpacity(FPD_IBrushOptionData optionData, FS_BYTE value);
	static FS_BYTE GetOpacity(FPD_IBrushOptionData optionData);
	static void SetBaseBrushDiameter(FPD_IBrushOptionData optionData, FS_INT32 value);
	static FS_INT32 GetBaseBrushDiameter(FPD_IBrushOptionData optionData);
	static void SetBaseBrushHardness(FPD_IBrushOptionData optionData, FS_INT32 value);
	static FS_INT32 GetBaseBrushHardness(FPD_IBrushOptionData optionData);
};


class CFPD_IEraserOptionData_V13
{
public:
	static void SetEraserMode(FPD_IEraserOptionData optionData, FPD_EraserMode value);
	static FPD_EraserMode GetEraserMode(FPD_IEraserOptionData optionData);
	static void SetFlow(FPD_IEraserOptionData optionData, FS_BYTE value);
	static FS_BYTE GetFlow(FPD_IEraserOptionData optionData);
	static void SetOpacity(FPD_IEraserOptionData optionData, FS_BYTE value);
	static FS_BYTE GetOpacity(FPD_IEraserOptionData optionData);
	static void SetEraserHistory(FPD_IEraserOptionData optionData, FS_BOOL bSet);
	static FS_BOOL GetEraserHistory(FPD_IEraserOptionData optionData);
	static void SetBaseBrushDiameter(FPD_IEraserOptionData optionData, FS_INT32 value);
	static FS_INT32 GetBaseBrushDiameter(FPD_IEraserOptionData optionData);
	static void SetBaseBrushHardness(FPD_IEraserOptionData optionData, FS_INT32 value);
	static FS_INT32 GetBaseBrushHardness(FPD_IEraserOptionData optionData);
};

class CFPD_IMagicWandOptionData_V13
{
public:
	static void SetTolerance(FPD_IMagicWandOptionData optionData, FS_INT32 value);
	static FS_INT32 GetTolerance(FPD_IMagicWandOptionData optionData);
	static void SetIsContinuous(FPD_IMagicWandOptionData optionData, FS_BOOL value);
	static FS_BOOL GetIsContinuous(FPD_IMagicWandOptionData optionData);
};

class CFPD_IDodgeOptionData_V13
{
public:
	static void SetDodgeMode(FPD_IDodgeOptionData optionData, FPD_TonesMode value);
	static FPD_TonesMode GetDodgeMode(FPD_IDodgeOptionData optionData);
	static void SetFlow(FPD_IDodgeOptionData optionData, FS_BYTE value);
	static FS_BYTE GetFlow(FPD_IDodgeOptionData optionData);
	static void SetOpacity(FPD_IDodgeOptionData optionData, FS_BYTE value);
	static FS_BYTE GetOpacity(FPD_IDodgeOptionData optionData);
	static void SetBaseBrushDiameter(FPD_IDodgeOptionData optionData, FS_INT32 value);
	static FS_INT32 GetBaseBrushDiameter(FPD_IDodgeOptionData optionData);
	static void SetBaseBrushHardness(FPD_IDodgeOptionData optionData, FS_INT32 value);
	static FS_INT32 GetBaseBrushHardness(FPD_IDodgeOptionData optionData);
};

class CFPD_IBurnOptionData_V13
{
public:
	static void SetFlow(FPD_IBurnOptionData optionData, FS_BYTE value);
	static FS_BYTE GetFlow(FPD_IBurnOptionData optionData);
	static void SetOpacity(FPD_IBurnOptionData optionData, FS_BYTE value);
	static FS_BYTE GetOpacity(FPD_IBurnOptionData optionData);
	static void SetBurnMode(FPD_IBurnOptionData optionData, FPD_TonesMode value);
	static FPD_TonesMode GetBurnMode(FPD_IBurnOptionData optionData);
	static void SetBaseBrushDiameter(FPD_IBurnOptionData optionData, FS_INT32 value);
	static FS_INT32 GetBaseBrushDiameter(FPD_IBurnOptionData optionData);
	static void SetBaseBrushHardness(FPD_IBurnOptionData optionData, FS_INT32 value);
	static FS_INT32 GetBaseBrushHardness(FPD_IBurnOptionData optionData);
};

class CFPD_IEyedropperData_V13
{
public:
	static FPD_EyedropperSampleType GetSampleType(FPD_IEyedropperData optionData);
	static void SetSampleType(FPD_IEyedropperData optionData, FPD_EyedropperSampleType sampleType);
};

class CFPD_ICloneStampData_V13
{
public:
	static void SetBlendMode(FPD_ICloneStampData stampData, FPD_ImageEditoBlendMode value);
	static FPD_ImageEditoBlendMode GetBlendMode(FPD_ICloneStampData stampData);
	static void SetFlow(FPD_ICloneStampData stampData, FS_BYTE value);
	static FS_BYTE GetFlow(FPD_ICloneStampData stampData);
	static void SetOpacity(FPD_ICloneStampData stampData, FS_BYTE value);
	static FS_BYTE GetOpacity(FPD_ICloneStampData stampData);
	static void SetIsAlineCheck(FPD_ICloneStampData stampData, FS_BOOL value);
	static FS_BOOL GetIsAlineCheck(FPD_ICloneStampData stampData);
	static void SetBaseBrushDiameter(FPD_ICloneStampData stampData, FS_INT32 value);
	static FS_INT32 GetBaseBrushDiameter(FPD_ICloneStampData stampData);
	static void SetBaseBrushHardness(FPD_ICloneStampData stampData, FS_INT32 value);
	static FS_INT32 GetBaseBrushHardness(FPD_ICloneStampData stampData);
};

class CFPD_IPaintBucketOptionData_V13
{
public:
	static void SetTolerance(FPD_IPaintBucketOptionData optionData, FS_INT32 value);
	static FS_INT32 GetTolerance(FPD_IPaintBucketOptionData optionData);
	static void SetOpacity(FPD_IPaintBucketOptionData optionData, FS_INT32 value);
	static FS_INT32 GetOpacity(FPD_IPaintBucketOptionData optionData);
	static void SetColorBlendMode(FPD_IPaintBucketOptionData optionData, FPD_ImageEditoBlendMode value);
	static FPD_ImageEditoBlendMode GetColorBlendMode(FPD_IPaintBucketOptionData optionData);
	static void SetContinuous(FPD_IPaintBucketOptionData optionData, FS_BOOL bContinuous);
	static FS_BOOL GetContinuous(FPD_IPaintBucketOptionData optionData);
};

class CFPD_ISpotHealingBrushData_V13
{
public:
	static FS_INT32 GetDiameter(FPD_ISpotHealingBrushData brushData);
	static void SetDiameter(FPD_ISpotHealingBrushData brushData, FS_INT32 value);
	static FS_INT32 GetRoundness(FPD_ISpotHealingBrushData brushData);
	static void SetRoundness(FPD_ISpotHealingBrushData brushData, FS_INT32 value);
	static FS_BOOL GetAline(FPD_ISpotHealingBrushData brushData);
	static void SetAline(FPD_ISpotHealingBrushData brushData, FS_BOOL value);
	static FPD_ImageMode GetMode(FPD_ISpotHealingBrushData brushData);
	static void SetMode(FPD_ISpotHealingBrushData brushData, FPD_ImageMode value);
};

class CFPD_CEditObject_V13
{
public:
	static FPD_EditObject New(FPD_PageObject pPageObject, FS_INT32 nIndex, FS_POSITION pos, FS_PtrArray vecTextRange, FS_BOOL bIsInForm);
	static FPD_EditObject New2(const FPD_EditObject& src);
	static void Delete(FPD_EditObject editObject);
	static FPD_EditObject CombineObjct(FPD_EditObject editObject,const FPD_EditObject& src);
	static FS_BOOL		  IsEqual(FPD_EditObject editObject,const FPD_EditObject& src);
	static void			  CopyBaseInfo(FPD_EditObject editObject,const FPD_EditObject& src, FS_BOOL bClone);
	static void			  UpdateFormInfo(FPD_EditObject editObject);
	static FS_BOOL		  UpdateFormInfo2(FPD_EditObject editObject,FPD_Page page);
	static FPD_PageObject  GetLastFormObj(FPD_EditObject editObject);
	static FPD_PageObject  GetFirstFormObj(FPD_EditObject editObject);
	static FPD_Page		  GetContainer(FPD_EditObject editObject, FPD_Page page);
	static FS_FloatRect	  GetObjBBox(FPD_EditObject editObject, FS_BOOL bTypeSet);
	static void			  Reset(FPD_EditObject editObject);
	static void			  UndoRedoState(FS_PtrArray& vecObj, FS_BOOL bSort);
	static void			  MergeTextObjRange(FPD_EditObject& obj);
	static void			  MergeTextObjRange2(FS_PtrArray& vecObjs);
};

class CFPD_IJoinSplit_V13
{
public:
	static void                ExitJoinEditing(FPD_ITouchJoinSplit joinSplit);
	static FS_BOOL             OnActivate(FPD_ITouchJoinSplit joinSplit);
	static FS_BOOL             OnDeactivate(FPD_ITouchJoinSplit joinSplit);
	static FS_BOOL             OnPaint(FPD_ITouchJoinSplit joinSplit, const FS_PtrArray& visibleEditorPage, FPD_RenderDevice& device);
	static FS_BOOL             OnLeftButtonDown(FPD_ITouchJoinSplit joinSplit, FPD_TouchEditorPage editorPage, FS_DevicePoint pt);
	static FS_BOOL             OnLeftButtonUp(FPD_ITouchJoinSplit joinSplit, FPD_TouchEditorPage editorPage, FS_DevicePoint pt);
	static FS_BOOL             OnMouseMove(FPD_ITouchJoinSplit joinSplit, FPD_TouchEditorPage editorPage, FS_DevicePoint pt);
	static FS_BOOL             OnRightButtonUp(FPD_ITouchJoinSplit joinSplit, FPD_TouchEditorPage editorPage, FS_DevicePoint pt);
	static FS_BOOL             IsProcessing(FPD_ITouchJoinSplit joinSplit);
	static FS_BOOL             BtnEnableStatus(FPD_ITouchJoinSplit joinSplit,FPD_TouchJoinSpiltOperationType opType);
	static void                JoinBoxes(FPD_ITouchJoinSplit joinSplit);
	static void                SplitBoxes(FPD_ITouchJoinSplit joinSplit);
	static void                LinkBoxes(FPD_ITouchJoinSplit joinSplit);
	static void                UnlinkBoxes(FPD_ITouchJoinSplit joinSplit);
	static void                SelectNone(FPD_ITouchJoinSplit joinSplit);
	static void                OnAfterInsertPages(FPD_ITouchJoinSplit joinSplit, FS_INT32 nInsertAt, FS_INT32 nCount);
	static void                OnAfterDeletePages(FPD_ITouchJoinSplit joinSplit, const FS_WordArray &arrDelPages);
	static void                OnAfterReplacePages(FPD_ITouchJoinSplit joinSplit, FS_INT32 nStart, const FS_WordArray& arrSrcPages);
	static void                OnAfterResizePage(FPD_ITouchJoinSplit joinSplit, FS_INT32 iPage);
	static void                OnAfterRotatePage(FPD_ITouchJoinSplit joinSplit, FS_INT32 iPage, FS_INT32 nRotate);
	static void                OnAfterMovePages(FPD_ITouchJoinSplit joinSplit, FS_INT32 nMoveTo, const FS_WordArray arrToMove);
};

class CFPD_ITouchup_V13
{
public:
	static FS_BOOL             OnActivate(FPD_ITouchup touchup);
	static FS_BOOL             OnDeactivate(FPD_ITouchup touchup);
	static void                ExistEditing(FPD_ITouchup touchup,FS_BOOL bEndDirectly);
	static FS_BOOL             OnPaint(FPD_ITouchup touchup, const FS_PtrArray& visibleEditorPage, FPD_RenderDevice& device);
	static FS_BOOL             OnChar(FPD_ITouchup touchup, FS_WCHAR Key);
	static FS_BOOL             OnVKUP(FPD_ITouchup touchup, FS_UINT nKeyCode);
	static FS_BOOL             OnVKDOWN(FPD_ITouchup touchup, FS_UINT nKeyCode);
	static FS_BOOL             OnLeftButtonDown(FPD_ITouchup touchup, FPD_TouchEditorPage editorPage, FS_DevicePoint pt);
	static FS_BOOL             OnLeftButtonUp(FPD_ITouchup touchup, FPD_TouchEditorPage editorPage, FS_DevicePoint pt);
	static FS_BOOL             OnLeftButtonDblClk(FPD_ITouchup touchup, FPD_TouchEditorPage editorPage, FS_DevicePoint pt);
	static FS_BOOL             OnMouseMove(FPD_ITouchup touchup, FPD_TouchEditorPage editorPage, FS_DevicePoint pt);
	static FS_BOOL             OnMouseWheel(FPD_ITouchup touchup, FPD_TouchEditorPage editorPage, FS_DevicePoint pt, FS_SHORT zDelta);
	static FS_BOOL             OnRightButtonDown(FPD_ITouchup touchup, FPD_TouchEditorPage editorPage, FS_DevicePoint pt);
	static FS_BOOL             OnRightButtonUp(FPD_ITouchup touchup, FPD_TouchEditorPage editorPage, FS_DevicePoint pt);
	static FS_BOOL             CanSelectAll(FPD_ITouchup touchup);
	static FS_BOOL             DoSelectAll(FPD_ITouchup touchup);
	static FS_BOOL             CanDelete(FPD_ITouchup touchup);
	static FS_BOOL             DoDelete(FPD_ITouchup touchup);
	static FS_BOOL             CanCopy(FPD_ITouchup touchup);
	static FS_BOOL             DoCopy(FPD_ITouchup touchup);
	static FS_BOOL             CanCut(FPD_ITouchup touchup);
	static FS_BOOL             DoCut(FPD_ITouchup touchup);
	static FS_BOOL             CanPaste(FPD_ITouchup touchup);
	static FS_BOOL             DoPaste(FPD_ITouchup touchup);
	static FS_BOOL             CanDeselectAll(FPD_ITouchup touchup);
	static FS_BOOL             DoDeselectAll(FPD_ITouchup touchup);
	static void                OnFontNameChanged(FPD_ITouchup touchup, const FS_WideString& wsFontName);
	static void                OnFontSizeChanged(FPD_ITouchup touchup, FS_FLOAT fFontSize);
	static void                OnTextColorChanged(FPD_ITouchup touchup, FS_COLORREF textColor);
	static void                OnBoldItlicChanged(FPD_ITouchup touchup, const FS_WideString& cwFontName, FS_BOOL bBold, FS_BOOL bItlic, FS_BOOL bIsBoldChanged);
	static void                OnAlignChanged(FPD_ITouchup touchup, FS_INT32 dwAlign);
	static void                OnCharSpaceChanged(FPD_ITouchup touchup, FS_FLOAT flSpace);
	static void                OnCharHorzScaleChanged(FPD_ITouchup touchup, FS_INT32 nScale);
	static void                OnLineLeadingChanged(FPD_ITouchup touchup, FS_FLOAT flLineLeading);
	static void                OnUnderlineChanged(FPD_ITouchup touchup, FS_BOOL bUnderline);
	static void                OnCrossChanged(FPD_ITouchup touchup, FS_BOOL bCross);
	static void                OnSuperScriptChanged(FPD_ITouchup touchup, FS_BOOL bSuperSet);
	static void                OnSubScriptChanged(FPD_ITouchup touchup, FS_BOOL bSubScript);
	static void                OnParagraphSpacingChanged(FPD_ITouchup touchup, FS_FLOAT fParaSpacing);
	static void                OnInDentChanged(FPD_ITouchup touchup);
	static void                OnDeDentChanged(FPD_ITouchup touchup);
	static void                OnAfterInsertPages(FPD_ITouchup touchup, FS_INT32 nInsertAt, FS_INT32 nCount);
	static void                OnAfterDeletePages(FPD_ITouchup touchup, const FS_WordArray &arrDelPages);
	static void                OnAfterReplacePages(FPD_ITouchup touchup, FS_INT32 nStart, const FS_WordArray& arrSrcPagess);
	static void                OnAfterResizePage(FPD_ITouchup touchup, FS_INT32 nPageIndex);
	static void                OnAfterRotatePage(FPD_ITouchup touchup, FS_INT32 iPage, FS_INT32 nRotate);
	static void                OnAfterMovePages(FPD_ITouchup touchup, FS_INT32 nMoveTo, const FS_WordArray arrToMove);
	static FS_BOOL             OnPageContentChange(FPD_ITouchup touchup, FS_PtrArray editorPages);
	static FS_LPVOID           GetFXEditor(FPD_ITouchup touchup);
};


class CFPD_ITouchupManager_V13
{
public:
	static FPD_ITouchupManager                 CreateManager(FPD_ITouchupProvider pProvider, FPD_Document pPDFDoc);
	static FPD_ITouchup                        GetTouchup(FPD_ITouchupManager touchMgr);
	static FPD_ITouchJoinSplit                  GetJoinSpilt(FPD_ITouchupManager touchMgr);
};


class CFPD_ITouchUndoItem_V13
{
public:
	static void                 OnUndo(FPD_ITouchUndoItem item);
	static void                 OnRedo(FPD_ITouchUndoItem item);
	static void                 OnRelease(FPD_ITouchUndoItem item);
};

class CFPD_ITouchClipboardHandler : public touchup::IClipboardHandler
{
public:
	CFPD_ITouchClipboardHandler(FPD_TouchClipboardHandlerCallbacksRec* pcallbacks);
	virtual CFX_WideString        GetClipboardText();
	virtual void                  SetClipboardText(CFX_WideString sSelText);

	void* GetClientData() { return m_callbacks.clientData; }

public:
	FPD_TouchClipboardHandlerCallbacksRec m_callbacks;
};

class CFPD_ITouchClipboard_V13
{
public:
	static FPD_ITouchClipboardHandler New(FPD_TouchClipBoardHandlerCallbacks callbacks);
	static void Destroy(FPD_ITouchClipboardHandler handler);
};

class CFPD_ITouchPopupMenuHandler : public touchup::IPopupMenuHandler
{
public:
	CFPD_ITouchPopupMenuHandler(FPD_TouchPopupMenuHandlerCallbacksRec* pcallbacks);
	virtual touchup::IPopupMenuHandler::TC_HMENU       CreatePopupMenu();
	virtual touchup::IPopupMenuHandler::TC_HMENU        CreatePopupSubMenu();
	virtual void            AppendMenuItem(touchup::IPopupMenuHandler::TC_HMENU hMenu, FX_DWORD nIDItem);
	virtual void            AppendSubMenu(touchup::IPopupMenuHandler::TC_HMENU hMenu, FX_DWORD nIDItem, touchup::IPopupMenuHandler::TC_HMENU hSubMenu);
	virtual void            RemoveMenuItem(touchup::IPopupMenuHandler::TC_HMENU hMenu, FX_DWORD nIdentify, touchup::IPopupMenuHandler::IdentifyMode mode);
	virtual void            AppendSeparator(touchup::IPopupMenuHandler::TC_HMENU hMenu);
	virtual void            EnableMenuItem(touchup::IPopupMenuHandler::TC_HMENU hMenu, FX_DWORD nIdentify, FX_BOOL bEnabled, touchup::IPopupMenuHandler::IdentifyMode mode);
	virtual FX_DWORD        TrackPopupMenu(touchup::IPopupMenuHandler::TC_HMENU hMenu, FX_INT32 x, FX_INT32 y, touchup::EditorPage editorPage);
	virtual void            DestroyMenu(touchup::IPopupMenuHandler::TC_HMENU hMenu);
	virtual FX_BOOL         IsMenuEnabled(touchup::IPopupMenuHandler::TC_HMENU hMenu);
	virtual void            CheckMenuItem(touchup::IPopupMenuHandler::TC_HMENU hMenu, FX_DWORD nIDItem, FX_BOOL bCheck);
	virtual void            InsertMenuItem(touchup::IPopupMenuHandler::TC_HMENU hMenu, FX_INT32 nPosition, FX_INT32 nNewPos, CFX_WideString lpNewItem);
	virtual void            InsertSeparator(touchup::IPopupMenuHandler::TC_HMENU hMenu, FX_INT32 nPosition);

	void* GetClientData() { return m_callbacks.clientData; }

public:
	FPD_TouchPopupMenuHandlerCallbacksRec m_callbacks;
};

class CFPD_ITouchPopupMenu_V13
{
public:
	static FPD_ITouchPopupMenuHandler New(FPD_TouchPopupMenuHandlerCallbacks callbacks);
	static void Destroy(FPD_ITouchPopupMenuHandler handler);
};

class CFPD_IProgressBarHandler : public touchup::IProgressBarHandler
{
public:
	CFPD_IProgressBarHandler(FPD_TouchProgressBarHandlerCallbacksRec* pcallbacks);
	virtual touchup::IProgressBarHandler::ProgressBar        Create(FX_BOOL bShowCancelButton, FX_BOOL bCenter);
	virtual void               SetText(touchup::IProgressBarHandler::ProgressBar bar, CFX_WideString& text);
	virtual void               SetRange(touchup::IProgressBarHandler::ProgressBar bar, FX_INT32 nLower, FX_INT32 nUpper);
	virtual void               SetCurrentValue(touchup::IProgressBarHandler::ProgressBar bar, FX_INT32 nPos);
	virtual FX_BOOL            IsCancelled(touchup::IProgressBarHandler::ProgressBar bar);
	virtual void               Destroy(touchup::IProgressBarHandler::ProgressBar bar);

	void* GetClientData() { return m_callbacks.clientData; }

public:
	FPD_TouchProgressBarHandlerCallbacksRec m_callbacks;
};

class CFPD_ITouchProgressBar_V13
{
public:
	static FPD_ITouchProgressBarHandler New(FPD_TouchProgressBarHandlerCallbacks callbacks);
	static void Destroy(FPD_ITouchProgressBarHandler handler);
};

class CFPD_ITouchOperationNotify : public touchup::IOperationNotify
{
public:
	CFPD_ITouchOperationNotify(FPD_ITouchOperationNotifyCallbacksRec* pcallbacks);	
	virtual FX_BOOL             OnEnterEditing(touchup::EditorPage editorPage, void* pParam);
	virtual FX_BOOL             OnExistEditing(CFX_WideStringArray& array);
	virtual void                OnAddUndo(touchup::IUndoItem* pUndoItem);
	virtual void                OnInvalid(touchup::EditorPage editorPage, std::vector<FX_RECT>* pInvalidRects, FX_BOOL bRenderDataChange);
	virtual void                OnContentChange(touchup::IOperationNotify::Operation type, touchup::EditorPage editorPage, const std::vector<touchup::CEditObject>::const_iterator itBegin, const std::vector<touchup::CEditObject>::const_iterator itEnd);
	virtual void                OnChangeDoc(CPDF_Document* pPDFDoc);
	virtual void                OnLROnPageWithoutText();
	void* GetClientData() { return m_callbacks.clientData; }

public:
	FPD_ITouchOperationNotifyCallbacksRec m_callbacks;
};

class CFPD_ITouchOperationNotify_V13
{
public:
	static FPD_ITouchOperationNotifyHandler New(FPD_ITouchOperationNotifyCallbacks callbacks);
	static void Destroy(FPD_ITouchOperationNotifyHandler handler);
};

class CFPD_ITouchUndoHandler : public touchup::IUndoHandler
{
public:
	CFPD_ITouchUndoHandler(FPD_ITouchUndoHandlerCallbacksRec* pcallbacks);
	void                BeginEdit(const std::function<void(void* pClientData)>& undoExecuteProc, void* pClientData, FX_BOOL bOwnUndo);
	void                EndEdit();
	FX_BOOL             IsUndoRedoing();
	void                BeginUndoGroup();
	void                EndUndoGroup(FX_BOOL bEdit);

	void* GetClientData() { return m_callbacks.clientData; }

public:
	FPD_ITouchUndoHandlerCallbacksRec m_callbacks;
};

class CFPD_ITouchUndoHandler_V13
{
public:
	static FPD_ITouchUndoHandler New(FPD_ITouchUndoHandlerCallbacks callbacks);
	static void Destroy(FPD_ITouchUndoHandler handler);
};

class CFPD_ITouchTextFormatHandler : public touchup::ITextFormatHandler
{
public:
	CFPD_ITouchTextFormatHandler(FPD_ITouchTextFormatHandlerCallbacksRec* pcallbacks);
	FX_BOOL             FindFontInFormatBar(CFX_WideString& fontName);
	void                EnableFormatToolBar(FX_BOOL bEnable);
	void                SetOwnerFontNameArr(const CFX_WideStringArray& fontName);
	void                UpdateTextFormat(const edit::CFVT_SecProps& secProps, const edit::CFVT_WordProps & wordProps);

	void* GetClientData() { return m_callbacks.clientData; }

public:
	FPD_ITouchTextFormatHandlerCallbacksRec m_callbacks;
};

class CFPD_ITouchTextFormatHandler_V13
{
public:
	static FPD_ITouchTextFormatHandler New(FPD_ITouchTextFormatHandlerCallbacks callbacks);
	static void Destroy(FPD_ITouchTextFormatHandler handler);
};


class CFPD_ITouchProviderHandler : public touchup::ITouchupProvider
{
public:
	CFPD_ITouchProviderHandler(FPD_ITouchProviderCallbacksRec* pcallbacks);
	virtual touchup::IOperationNotify*                GetOperationNotify();
	virtual touchup::IClipboardHandler*               GetClipboardtHandler();
	virtual touchup::IPopupMenuHandler*               GetPopupMenuHandler();
	virtual touchup::IProgressBarHandler*             GetProgressBarHandler();
	virtual window::IFX_SystemHandler*                GetSystemHandler();
	virtual touchup::IUndoHandler*                    GetUndoHandler();
	virtual touchup::ITextFormatHandler*              GetTextFormatHandler();
	virtual CPDF_Matrix                               GetRenderMatrix(touchup::EditorPage editorPage);
	virtual CFX_WideString                            GetString(touchup::ITouchupProvider::UIStringResourceID strID);
	virtual FX_BOOL                                   IsSupportBullets(FX_WCHAR word);
	virtual std::unique_ptr<window::IPWL_SpellCheck>  GetSpellCheckEngine();
	virtual window::IPWL_FontMap*                     GetFontMap(CPDF_Document* pDoc);
	virtual CPDF_Rect                                 GetPageVisibleRect(CPDF_Page *pPage);
	virtual FX_BOOL                                   GetVisiblePage(CPDF_Document* pPDFDoc, std::vector<touchup::EditorPage>&  visiblePage);
	virtual FX_RECT                                   GetPageRect(touchup::EditorPage editorPage);
	virtual CPDF_Page*                                GetPage(CPDF_Document* pPDFDoc, FX_INT32 nPageIndex);
	virtual FX_FLOAT                                  GetPageViewScale(touchup::EditorPage editorPage);
	virtual void                                      GetGridMainLine(touchup::EditorPage editorPage, std::vector<FX_INT32>& XArray, std::vector<FX_INT32>& YArray);
	virtual void                                      GetGridLine(touchup::EditorPage editorPage, std::vector<FX_INT32>& XArray, std::vector<FX_INT32>& YArray);
	virtual FX_BOOL                                   IsSnap2GridEnabled();
	virtual FX_BOOL                                   IsEastenArabicNumeralMode();
	virtual touchup::EditorPage                       GetDefaultEditorPage(CPDF_Page* pPDFPage);
	virtual touchup::EditorPage                       GetCurrentEditorPage();
	virtual FX_BOOL                                   GotoPageView(FX_INT32 index, FX_FLOAT left, FX_FLOAT top);
	virtual void                                      ScrollToCenterPoint(FX_INT32 index, CPDF_Point pdfPoint);
	virtual FX_INT32                                  GetRotation();
	virtual window::FX_HWND                           GetPageWindow(touchup::EditorPage editorPage);
	virtual FX_DWORD                                  GetTickCountTC();
	virtual FX_DWORD                                  GetDoubleClickTimeTC();
	virtual FX_BOOL                                   IsCurDocShowInBrowser();
	virtual FX_BOOL                                   GetRunOCR();
	virtual FX_BOOL                                   IsEditingUseHindiDigits();
	virtual FX_BOOL                                   IsUseSystemSelectionColor();
	virtual FX_DWORD                                  GetSysColorTC(int nIndex);
	virtual void                                      GetPopupMenuString(FX_INT32 nIndex, CFX_WideString& cwMenuString);
	virtual CPDF_RenderOptions*                       GetRenderOptions(touchup::EditorPage editorPage);
	virtual FX_BOOL                                   GetDocForceColor(FX_COLORREF& color);
	virtual FX_BOOL                                   IsReplaceColor(FX_BOOL& bReplace);
	virtual FX_BOOL                                   IsBlackAndWhiteOnly(FX_BOOL& bAll);
	virtual FX_RECT                                   GetPageWindowClientRect(touchup::EditorPage editorPage);
	virtual CFX_WideString                            GetToolbarFontName();
	virtual FX_BOOL                                   GetTextColor(FX_COLORREF& color);
	virtual FX_BOOL                                   IsWipPluginAllowedPaste();
	virtual CPDF_Page*                                GetLatestPDFPage(touchup::EditorPage editorPage, std::size_t nPageIndex);
	virtual FX_BOOL                                   IsTouchDevice();
	virtual void                                      SetOwnUndoMode(CPDF_Document* pDoc, FX_BOOL bOwnUnoMode);
	virtual FX_BOOL                                   IsJoinSpiltProcessing();
	virtual FX_FLOAT                                  GetCustomIndent(touchup::EditorPage editorPage);
	virtual void                                      ChangeCursor(UICursorResourceType type);
	virtual void                                      EnableJoinSplitFormat(FX_BOOL bEnable);
	virtual void                                      ShowTip(const CFX_WideString& str, touchup::EditorPage editorPage);
	virtual void                                      SetFocusTC(touchup::EditorPage editroPage);
	virtual FX_BOOL                                   ShowMessageBox(ITouchupProvider::UIDialogResourceID id, FX_DWORD flag, void* pData);
	virtual void                                      LoopWindowMessage();
	virtual FX_BOOL                                   ProcessByMultithreading(std::size_t jobCount, const std::function<void()>& job, const std::function<void()>& waitting);

	void* GetClientData() { return m_callbacks.clientData; }

public:
	FPD_ITouchProviderCallbacksRec m_callbacks;
};

class CFPD_ITouchProvider_V13
{
public:
	static FPD_ITouchupProvider New(FPD_ITouchProviderCallbacks callbacks);
	static void Destroy(FPD_ITouchupProvider handler);
};

#ifdef __cplusplus
};
#endif//__cplusplus

#endif//FPD_PAGEOBJIMPL_H
