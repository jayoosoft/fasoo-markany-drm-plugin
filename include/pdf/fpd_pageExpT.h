﻿/*********************************************************************

 Copyright (C) 2010 Foxit Corporation
 All rights reserved.
  
 NOTICE: Foxit permits you to use, modify, and distribute this file
 in accordance with the terms of the Foxit license agreement
 accompanying it. If you have received this file from a source other
 than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
	
 ---------------------------------------------------------------------
	  
fpd_pageExpT.h
		
 - Types, macros, structures, etc. required to use the FPDPage HFT.

*********************************************************************/

/**
 * @defgroup FPDLayer Foxit Portable Document Layer
 */

/*@{*/

/**
 * @addtogroup FPDPAGE
 * @{
 */

/**
 * @file
 * @brief FPD page and form definitions. 
 */

#ifndef FPD_PAGEEXPT_H
#define FPD_PAGEEXPT_H

#ifndef FS_COMMON_H
#include "../basic/fs_common.h"
#endif

#ifndef FS_BASICEXPT_H
#include "../basic/fs_basicExpT.h"
#endif

#ifndef FS_STRINGEXPT_H
#include "../basic/fs_stringExpT.h"
#endif

#ifndef FPD_OBJSEXPT_H
#include "fpd_objsExpT.h"
#endif

#ifndef FPD_RESOURCEEXPT_H
#include "fpd_resourceExpT.h"
#endif

#ifndef FPD_PAGEOBJEXPT_H
#include "fpd_pageobjExpT.h"
#endif

#ifdef __cplusplus
extern "C"{
#endif

/* @OBJSTART FPD_Page */
#ifndef FPD_PAGE
#define	FPD_PAGE
/** 
 *@brief  A <a>FPD_Page</a> is a page in a document, corresponding to the PDF Page object
 * (see Page Objects in Section 3.6.2, Page Tree, in the PDF Reference).
 *
 * Among other associated objects, a page contains:
 * <ul>
 * <li>A series of objects representing the objects drawn on the page.</li>
 * <li>A list of resources used in drawing the page.</li>
 * <li>Annotations.</li>
 * <li>An optional thumbnail image of the page.</li>
 * <li>Beads used in any articles that occur on the page.</li>
 * </ul>
 * See <a>FPDPageNew</a>, <a>FPDPageDestroy</a>.
*/
typedef struct _t_FPD_Page* FPD_Page;
#endif
/* @OBJEND */

/* @OBJSTART FPD_Form */
#ifndef FPD_FORM
#define	FPD_FORM
/** @brief A <a>FPD_Form</a> is a self-contained set of graphics operators that is used when a particular graphic 
* is drawn more than once in a document. It corresponds to to a PDF XObject <Italic>(see Section 4.9, Form XObjects, 
* in the PDF Reference)</Italic>. See <a>FPDFormNew</a>, <a>FPDFormDestroy</a>.
*/
typedef struct _t_FPD_Form* FPD_Form;
#endif
/* @OBJEND */

/* @OBJSTART FPD_ParseOptions */
#ifndef FPD_PARSEOPTIONS
#define	FPD_PARSEOPTIONS
/** @brief The page parsing options. */
typedef struct _t_FPD_ParseOptions* FPD_ParseOptions;
#endif
/* @OBJEND */

/* @OBJSTART FPD_AllStates */	
#ifndef FPD_ALLSTATES 
#define FPD_ALLSTATES
/** @brief No document exits. */
	typedef struct _t_FPD_AllStates* FPD_AllStates;
#endif
/* @OBJEND */

/* @OBJSTART FPD_ContentGenerator */	
#ifndef FPD_CONTENTGENERATOR 
#define FPD_CONTENTGENERATOR
/** @brief PDF Page or Form content generator. */
	typedef struct _t_FPD_ContentGenerator* FPD_ContentGenerator;
#endif

/* @ENUMSTART */
/** 
  * @brief The status of generating PDF page or Form content progressively.
  */
enum FPD_ProgressiveStatus
{
	FPD_Ready,              /**< Ready. */
	FPD_ToBeContinued,		/**< To be continued. */
	FPD_Found,				/**< Found. */
	FPD_NotFound,           /**< Not found. */
	FPD_Failed,				/**< Failed. */
	FPD_Done                /**< Done. */
};
/* @ENUMEND */

/* @OBJEND */

/* @OBJSTART FPD_ColorSeparator */
#ifndef FPD_COLORSEPARATOR
#define	FPD_COLORSEPARATOR
/**
 *@brief  A <a>FPD_ColorSeparator</a> is used to export PS or EPS format.
 * See <a>FPDColorSeparatorNew</a>, <a>FPDColorSeparatorDestroy</a>.
*/
typedef struct _t_FPD_ColorSeparator* FPD_ColorSeparator;
#endif
/* @OBJEND */


/* @COMMONSTART */
/* @DEFGROUPSTART FPDTransparencyAttrFlags */

/**
 * @name Transparency attribute flags for a page object group (form or page).
 */
/*@{*/

/** @brief Whether a transparency group is present. */
#define FPD_TRANS_GROUP			0x0100
/** @brief Whether a transparency group is isolated. For isolated groups, a separate level device is required. */
#define FPD_TRANS_ISOLATED		0x0200
/** @brief Whether a transparency group is knockout. For non-knockout groups, composition is required for all objects inside. */
#define FPD_TRANS_KNOCKOUT		0x0400

/*@}*/

/* @DEFGROUPEND */

/* @DEFGROUPSTART FPDContentParsingState */

/**
 * @name The flags for PDF content-parsing state.
 */
/*@{*/

/** @brief The page is not parsed. */
#define FPD_CONTENT_NOT_PARSED	0
/** @brief The page is parsing. */
#define FPD_CONTENT_PARSING		1
/** @brief The page is parsed. */
#define FPD_CONTENT_PARSED		2

/*@}*/

/* @DEFGROUPEND */

/* @DEFGROUPSTART FPDOSTextExtractingFlags */

/**
 * @name The flags for text extracting.
 */
/*@{*/
	
/** @brief Auto rotating. */
#define FPD_2TXT_AUTO_ROTATE		1
/** @brief Auto width. */
#define FPD_2TXT_AUTO_WIDTH			2
/** @brief Keep column. */
#define FPD_2TXT_KEEP_COLUMN		4
/** @brief Whether to use OCR. */
#define FPD_2TXT_USE_OCR			8
/** @brief Whether to include invisible texts. */
#define FPD_2TXT_INCLUDE_INVISIBLE	16

/*@}*/

/* @DEFGROUPEND */

/* @COMMONEND */


/* @STRUCTSTART FPD_EditorPage*/
typedef struct  __FPD_EditorPage__
{
	FPD_Page pPDFPage;
	/**
	*@brief The view corresponding to CPDF_Page.
	*
	*@details Get current page rendering information, such as matrix.
	*/
	void* pDocView;

	FS_BOOL isValid() {
		return (pPDFPage && pDocView);
	}

	FS_BOOL isEqual(const __FPD_EditorPage__& other) {
		return(other.pDocView == pDocView && other.pPDFPage == pPDFPage);
	}
}FPD_EditorPage;
/* @STRUCTEND */

/* @STRUCTSTART FPD_EditorPage*/
typedef struct __FPD_ShadingColor
{
	FS_FLOAT from;
	FS_FLOAT to;
	FS_COLORREF from_color;
	FS_COLORREF to_color;
}FPD_ShadingColor;
/* @STRUCTEND */

/* @OBJSTART FR_Edit_FontMap */
#ifndef FR_EDIT_FONTMAP
#define FR_EDIT_FONTMAP
/**
 * @brief
 */
typedef struct _t_FR_Edit_FontMap* FR_Edit_FontMap;
#endif


/* @OBJSTART FR_VTSecProps */
#ifndef FR_VTSECPROPS
#define FR_VTSECPROPS
/**
 * @brief
 */
typedef struct _t_FR_VTSecProps* FR_VTSecProps;
#endif
/* @OBJEND */


/* @OBJSTART FR_VTWordProps */
#ifndef FR_VTWORDPROPS
#define FR_VTWORDPROPS
/**
 * @brief
 */
typedef struct _t_FR_VTWordProps* FR_VTWordProps;
#endif
/* @OBJEND */

#ifndef FPD_RENDEROPTIONS
#define FPD_RENDEROPTIONS
typedef struct _t_FPD_RenderOptions* FPD_RenderOptions;
#endif

/* @OBJSTART FR_SpellCheck */
#ifndef FR_SPELLCHECK
#define FR_SPELLCHECK
/**
 * @brief The <a>FR_SpellCheck</a> object can be used to check the spelling. See <a>FRSpellCheckNew</a>.
 */
typedef struct _t_FR_SpellCheck* FR_SpellCheck;
#endif
/* @OBJEND */

#ifndef FPD_EDITCONTEXT
#define FPD_EDITCONTEXT
/** @brief A callback for <a>FPD_EnumPageHandler</a>. See <a>FPDDocEnumPages</a>.
*/
typedef struct
{
	/* The size of data structure. It must be set to <Italic>sizeof</Italic>(<a>FPD_EDITCONTEXT</a>).  */
	unsigned long lStructSize;

	/** The user-supplied data. */
	FS_LPVOID		clientData;

	FR_Edit_FontMap(*GetFontMap)(FS_LPVOID clientData, FPD_Document pDoc);
	FPD_IOperationNotifyHandler(*GetOperationNotify)(FS_LPVOID clientData);
	FPD_IClipboardHandler(*GetClipboardtHandler)(FS_LPVOID clientData);
	FPD_IPopupMenuHandler(*GetPopupMenuHandler)(FS_LPVOID clientData);
	FPD_ITipHandler(*GetTip)(FS_LPVOID clientData);
	void(*GetGridMainLine)(FS_LPVOID clientData, FPD_EditorPage editorPage, FS_DWordArray& XArray, FS_DWordArray& YArray);
	void(*GetGridLine)(FS_LPVOID clientData, FPD_EditorPage editorPage, FS_DWordArray& XArray, FS_DWordArray& YArray);
	FS_BOOL(*IsSnap2GridEnabled)(FS_LPVOID clientData);
	void(*GetSystemDPI)(FS_LPVOID clientData, FS_FLOAT& fHorzDpi, FS_FLOAT& fVertDpi);
	FPD_Page(*GetLatestPDFPage)(FS_LPVOID clientData, FPD_EditorPage editorPage, FS_INT32 nPageIndex);
	void(*GetToolbarFontName)(FS_LPVOID clientData, FS_WideString& pFontNameOut);
	void(*UpdateTextFormat)(FS_LPVOID clientData, const FR_VTWordProps pt, const FR_VTSecProps sp, FPD_TextProperty textProperty);
	void(*ActiveTextFormat)(FS_LPVOID clientData, const FR_VTWordProps pt, const FR_VTSecProps sp, FS_WideString wsBarName, FS_BOOL bIsNewObject);
	void(*GetFontFaceName)(FS_LPVOID clientData, const FS_LPCWSTR lpwsScriptName, FS_WideString& pFontNameOut);
	FS_FloatRect(*GetPageVisibleRect)(FS_LPVOID clientData, FPD_EditorPage editorPage);
	FS_FLOAT(*GetPageViewScale)(FS_LPVOID clientData, FPD_EditorPage editorPage);
	FS_DIBitmap(*GetRotateCtrlIcon)(FS_LPVOID clientData);
	void(*GetString)(FS_LPVOID clientData, FPD_UIStringResourceID strID, FS_WideString& pStringOut);
	void(*LocalizeDot)(FS_LPVOID clientData, FS_WideString& str);
	FS_BOOL(*IsProEditingEnabled)(FS_LPVOID clientData);
	FS_DIBitmap(*LoadPNGFromID)(FS_LPVOID clientData, FPD_UIImageResourceID png);

	FS_AffineMatrix(*GetRenderMatrix)(FS_LPVOID clientData, FPD_EditorPage editorPage);
	void(*GetScrollPos)(FS_LPVOID clientData, FPD_EditorPage editorPage, FS_INT32 &nHorz, FS_INT32 & nVert);
	FPD_EditorPage(*GetDefaultEditorPage)();

	FS_Rect(*GetPageRect)(FS_LPVOID clientData, FPD_EditorPage editorPage);
	void(*GetTemplate)(FS_LPVOID clientData, FPD_EditorPage editorPage, FPD_PageObject pGraphicsObject);
	FS_BOOL(*IsDirectionEnabled)(FS_LPVOID clientData);
	FS_BOOL(*GetMainParagraphDir)(FS_LPVOID clientData);
	FS_BOOL(*IsUseSystemSelectionColor)(FS_LPVOID clientData);
	FS_DWORD(*GetSystemHightColor)(FS_LPVOID clientData);
	FS_BOOL(*GetDefaultTextColor)(FS_LPVOID clientData, FS_COLORREF* color);
	FS_BOOL(*GetIsHindDigit)(FS_LPVOID clientData);
	FS_BOOL(*IsRepalceDocColor)(FS_LPVOID clientData);
	FS_COLORREF(*GetDocumentClr)(FS_LPVOID clientData);
	FS_BOOL(*IsForceBWOnly)(FS_LPVOID clientData);

	FR_SpellCheck(*SetNewSpellCheck)(FS_LPVOID clientData);
	FPD_RenderOptions(*GetRenderOptions)(FS_LPVOID clientData, FPD_EditorPage editpage);
	FS_BOOL(*IsShiftKeyDown)(FS_LPVOID clientData);
	FS_BOOL(*IsControlKeyDown)(FS_LPVOID clientData);
	FS_BOOL(*IsInsertKeyDown)(FS_LPVOID clientData);

	//image editor
	void(*GetImageViewScrollPos)(FS_LPVOID clientData, FS_INT32& nHorz, FS_INT32& nVert);
	FS_Rect(*GetImageViewClientRect)(FS_LPVOID clientData);
	FS_DevicePoint(*GetImageViewCursorPos)(FS_LPVOID clientData);
	void(*GetTempPathPE)(FS_LPVOID clientData, FS_WideString& pStrPath);
	FS_LPSTR(*GlobalAllocMemory)(FS_LPVOID clientData, FS_DWORD nSize, FS_LPVOID &pOutHandle);
	void(*GlobalFreeMemory)(FS_LPVOID clientData, FS_LPVOID pOutHandle);
	FS_BOOL(*GetWIPPluginProviderAllowedPaste)(FS_LPVOID clientData);
	void(*GetBitmapInfo)(FS_LPVOID clientData, const FS_DIBitmap pBitmap, FS_ByteString& pOutStr);
	void(*TextChanged)(FS_LPVOID clientData, FPD_EditorPage editpage);
    FS_Rect(*GetClientRect)(FS_LPVOID clientData, FPD_EditorPage editpage);

}FPD_EditorContextCallbacksRec, *FPD_EditorContextCallbacks;
#endif


#ifndef FPD_CLIPBOARD
#define FPD_CLIPBOARD
/** @brief A callback for <a>FPD_EnumPageHandler</a>. See <a>FPDDocEnumPages</a>.
*/
typedef struct
{
	/* The size of data structure. It must be set to <Italic>sizeof</Italic>(<a>FPD_EDITCONTEXT</a>).  */
	unsigned long lStructSize;

	/** The user-supplied data. */
	FS_LPVOID		clientData;

	void(*CopyGraphicObject)(FS_LPVOID clientData);
	FS_BOOL(*CanPasteGraphicObject)(FS_LPVOID clientData);
	FS_PtrArray(*PasteGraphicObject)(FS_LPVOID clientData);
	void(*GetClipboardText)(FS_LPVOID clientData, FPD_EditorPage editorPage, FS_WideString sSelText);
	void(*SetClipboardText)(FS_LPVOID clientData, FPD_EditorPage editorPage, FS_LPCWSTR sSelText);
	FS_BOOL(*PasteBitmap)(FS_LPVOID clientData);
	void(*CopyBitmap)(FS_LPVOID clientData, FS_LPVOID pHandle);
	FS_BOOL(*IsFormatAvailable)(FS_LPVOID clientData, FPD_ObjectFilter nObjectType);
}FPD_ClipBoardHandlerCallbacksRec, *FPD_ClipBoardHandlerCallbacks;

#endif

/* @COMMONEND */



#ifndef FPD_POPUPMENU
#define FPD_POPUPMENU
/** @brief A callback for <a>FPD_EnumPageHandler</a>. See <a>FPDDocEnumPages</a>.
*/
typedef struct
{
	/* The size of data structure. It must be set to <Italic>sizeof</Italic>(<a>FPD_EDITCONTEXT</a>).  */
	unsigned long lStructSize;

	/** The user-supplied data. */
	FS_LPVOID		clientData;
	FS_LPVOID(*CreatePopupMenu)(FS_LPVOID clientData);
	FS_LPVOID(*CreatePopupSubMenu)(FS_LPVOID clientData);
	void(*AppendMenuItem)(FS_LPVOID clientData, FS_LPVOID hMenu, FS_DWORD nIDItem);
	void(*AppendSubMenu)(FS_LPVOID clientData, FS_LPVOID hMenu, FS_DWORD nIDItem, FS_LPVOID hSubMenu);

	void(*RemoveMenuItem)(FS_LPVOID clientData, FS_LPVOID hMenu, FS_DWORD nIdentify, FPD_IdentifyMode mode);
	void(*AppendSeparator)(FS_LPVOID clientData, FS_LPVOID hMenu);
	void(*EnableMenuItem)(FS_LPVOID clientData, FS_LPVOID hMenu, FS_DWORD nIdentify, FS_BOOL bEnabled, FPD_IdentifyMode mode);
	FS_DWORD(*TrackPopupMenu)(FS_LPVOID clientData, FS_LPVOID hMenu, FS_INT32 x, FS_INT32 y, FPD_EditorPage editorPage);
	void(*DestroyMenu)(FS_LPVOID clientData, FS_LPVOID hMenu);
	FS_BOOL(*OnCreateDlg)(FS_LPVOID clientData, FPD_PopupMenuItem nIDItem, FS_INT32 &value);
	FS_BOOL(*IsMenuEnabled)(FS_LPVOID clientData, FS_LPVOID hMenu);
	void(*InsertSeparator)(FS_LPVOID clientData, FS_LPVOID hMenu, FS_INT32 nPosition);
	void(*CheckMenuItem)(FS_LPVOID clientData, FS_LPVOID hMenu, FS_DWORD nIDItem, FS_BOOL bCheck);
	void(*InsertMenuItem)(FS_LPVOID clientData, FS_LPVOID hMenu, FS_INT32 nPosition, FS_INT32 nNewPos, FS_WideString lpNewItem);
}FPD_PopupMenuHandlerCallbacksRec, *FPD_PopupMenuHandlerCallbacks;

#endif

#ifndef FPD_ITIPCALLACKREC
#define FPD_ITIPCALLACKREC
typedef struct
{
	/* The size of data structure. It must be set to <Italic>sizeof</Italic>(<a>FPD_EDITCONTEXT</a>).  */
	unsigned long lStructSize;

	/** The user-supplied data. */
	FS_LPVOID		clientData;

	void(*Show)(FS_LPVOID clientData, FS_BOOL bVisible, FS_BOOL bDelay);
	void(*MoveTo)(FS_LPVOID clientData, const FS_DevicePoint& point);
	void(*SetTip)(FS_LPVOID clientData, const FS_WideString & str, FPD_EditorPage editorPage);
}FPD_TipCallbacksRec, *FPD_TipCallbacks;
#endif

#ifndef FPD_IOPERATIONNOTIFYCALLBACKREC
#define FPD_IOPERATIONNOTIFYCALLBACKREC
typedef void(*PAGEEDITOR_TIMER_CALLBACK)(FS_DWORD idEvent);
typedef struct
{
	/* The size of data structure. It must be set to <Italic>sizeof</Italic>(<a>FPD_EDITCONTEXT</a>).  */
	unsigned long lStructSize;

	/** The user-supplied data. */
	FS_LPVOID		clientData;


	void(*OnAddUndo)(FS_LPVOID clientData, FPD_IUndoItem pUndoItem, FS_BOOL bEdit);

	void(*OnUndoRedoBeginEdit)(FS_LPVOID clientData, void(*func)(void* pClientData), void* pClientData, FS_BOOL bOwnUndo);
	void(*OnUndoRedoEndEdit)(FS_LPVOID clientData, FS_BOOL bOwnUndo);
	void(*OnContentChangeCancle)(FS_LPVOID clientData, FPD_EditorPage editorPage);
	void(*OnEnsurePageVisible)(FS_LPVOID clientData, FPD_EditorPage editorPage, FS_FloatPoint rcLeftTop);
	void(*OnInvalid)(FS_LPVOID clientData, FPD_EditorPage editorPage, FS_FloatRectArray& invalidRects);
	void(*OnPageInvalid)(FS_LPVOID clientData, FPD_EditorPage editorPage);
	void(*OnSelectionChange)(FS_LPVOID clientData);
	void(*OnCursorChange)(FS_LPVOID clientData, FPD_EditorPage editorPage, FPD_UICursorResourceType type);
	//void(*OnGoToPageViewByRect)(FS_LPVOID clientData, FPD_EditorPage editorPage, FS_FloatRect rc);
	FS_BOOL(*OnShowMessageBox)(FS_LPVOID clientData, FPD_EditorPage editorPage, FPD_UIDialogResourceID id, FS_DWORD flag);
	void(*OnSavePageState)(FS_LPVOID clientData, FPD_EditorPage editorPage);
	void(*OnRestorePageState)(FS_LPVOID clientData);
	void(*OnKillTimer)(FS_LPVOID clientData, FS_DWORD nTimerID);
	//FX_INT32(*OnSetTimer)(FX_DWORD nIDEvent, FX_DWORD nElapse, pageeditor::TIMER_CALLBACK callback);
	void(*OnSetCurrentCapture)(FS_LPVOID clientData, FPD_EditorPage editorPage, FS_LPVOID pCapData);
	void(*OnReleaseCurrentCapture)(FS_LPVOID clientData, FPD_EditorPage editorPage);

	void(*OnContentChangeStart)(FS_LPVOID clientData, FPD_Operation type, FPD_EditorPage editroPage, FS_PtrArray& arr);
	void(*OnContentChangeEnd)(FS_LPVOID clientData, FPD_Operation type, FPD_EditorPage editroPage, FS_PtrArray& arr, FS_BOOL bChangeMark);
	void(*OnSetPropsAsDefault)(FS_LPVOID clientData, FPD_Operation type, FS_PtrArray& arr);
	void(*OnContentChange)(FS_LPVOID clientData, FPD_Operation type, FPD_EditorPage editroPage, FS_PtrArray& arr);
	FS_INT32(*OnSetTimer)(FS_LPVOID clientData, FS_DWORD nIDEvent, FS_DWORD nElapse, PAGEEDITOR_TIMER_CALLBACK callback);

	FS_BOOL(*OnEnterEdit)(FS_LPVOID clientData, FPD_EditorPage editorPage, FPD_ObjectFilter nObjectType, FS_LPVOID pParam);
	FS_BOOL(*OnExistEdit)(FS_LPVOID clientData, FPD_EditorPage editorPage, FPD_ObjectFilter nObjectType);
	FS_BOOL(*OnShowSysColorDlg)(FS_LPVOID clientData, FS_COLORREF& color);
	void(*OnImageViewInvalid)(FS_LPVOID clientData, const FS_Rect* pInvalidRects, FS_BOOL bErase);
	void(*OnImageViewSetCapture)(FS_LPVOID clientData);
	void(*OnImageViewReleaseCapture)(FS_LPVOID clientData);
	void(*OnImageViewSetFocus)(FS_LPVOID clientData);
	void(*OnImageViewSetScrollSizes)(FS_LPVOID clientData, FS_DevicePoint sizeTotal, FS_DevicePoint sizePage, FS_DevicePoint sizeLine);
	void(*OnImageViewChangeZoomScaleInfo)(FS_LPVOID clientData, FS_FLOAT dbScale, FS_BOOL bErase);
	void(*OnImageViewCreateDIBSection)(FS_LPVOID clientData, PFPD_BitMapInfo bi);
	void(*OnImageHistogramDataChanged)(FS_LPVOID clientData, const FPD_ImageHistogramData& data);
	FS_BOOL(*OnImageViewGetOpenFileName)(FS_LPVOID clientData, FS_WCHAR* pSzFilter, FS_WCHAR* pSzOutFile);
	FS_BOOL(*OnImageViewGetSaveFileName)(FS_LPVOID clientData, FS_WCHAR* pSzFilter, FS_WCHAR* pSzOutFile);
	void(*OnUpdateColor)(FS_LPVOID clientData, FS_COLORREF color);
	void(*OnCaretChange)(FS_LPVOID clientData, const FR_VTSecProps secProps, const FR_VTWordProps wordProps, const FS_WideString sFontName);
    void(*OnRegisterKoreanInputHandler)(FS_LPVOID clientData, FPD_EditorPage editorPage);

}FPD_IOperationNotifyCallbacksRec, *FPD_IOperationNotifyCallbacks;
#endif


/* @STRUCTSTART FPD_EditorPage*/
typedef struct  __FPD_TouchEditorPage__
{
	FPD_Page pPDFPage;
	/**
	*@brief The view corresponding to CPDF_Page.
	*
	*@details Get current page rendering information, such as matrix.
	*/
	void* pClientData;

	__FPD_TouchEditorPage__() {
		pPDFPage = nullptr;
		pClientData = nullptr;
	}

	__FPD_TouchEditorPage__(FPD_Page pPage, FS_LPVOID pView) {

		pPDFPage = pPage;
		pClientData = pView;
	}

	FS_BOOL isValid() {
		return (pPDFPage && pClientData);
	}

	FS_BOOL isEqual(const __FPD_TouchEditorPage__& other) {
		return(other.pClientData == pClientData && other.pPDFPage == pPDFPage);
	}

	FS_BOOL isLess (const __FPD_TouchEditorPage__& right) const {		
		if (pPDFPage == right.pPDFPage) {
			return pClientData < right.pClientData;
		}
		else {
			return pPDFPage < right.pPDFPage;
		}
	}
}FPD_TouchEditorPage;
/* @STRUCTEND */


enum FPD_TouchIdentifyMode
{
	FPDTkPosition,
	FPDTkCommand	
};

enum FPD_TouchPopupMenuHandlerItem
{
	FPDTkJoinTextBoxes=0,
	FPDTkSpiltTextBoxes,
	FPDTkLinkTextBoxes,
	FPDTkUnlinkTextBoxes,
	FPDTkSelectNone,
	FPDTkMaxValue
};

enum  FPD_TouchUIDialogResourceID
{
	FPDTkJoinSpiltTips = 0,
	FPDTkNotJoin,
	FPDTkRotatedText,
	FPDTkParagraSpace,
};

enum FPD_TouchUIStringResourceID
{
	FPDTkWaitProcess,
	FPDTkLayoutRecognition,
};

enum FPD_TouchUICursorResourceType
{
	FPDTkSizeNs,
	FPDTkSizeWe,
	FPDTkSizeNwse,
	FPDTkSizNesw,
	FPDTkSizeAll
};



#ifndef FPD_TOUCHPOPUPMENU
#define FPD_TOUCHPOPUPMENU
/** @brief A callback for <a>FPD_EnumPageHandler</a>. See <a>FPDDocEnumPages</a>.
*/
typedef struct
{
	/* The size of data structure. It must be set to <Italic>sizeof</Italic>(<a>FPD_EDITCONTEXT</a>).  */
	unsigned long lStructSize;

	/** The user-supplied data. */
	FS_LPVOID		clientData;
	FS_LPVOID(*FPDTCreatePopupMenu)(FS_LPVOID clientData);
	FS_LPVOID(*FPDTCreatePopupSubMenu)(FS_LPVOID clientData);
	void(*FPDTAppendMenuItem)(FS_LPVOID clientData, FS_LPVOID hMenu, FS_DWORD nIDItem);
	void(*FPDTAppendSubMenu)(FS_LPVOID clientData, FS_LPVOID hMenu, FS_DWORD nIDItem, FS_LPVOID hSubMenu);
	void(*FPDTRemoveMenuItem)(FS_LPVOID clientData, FS_LPVOID hMenu, FS_DWORD nIdentify, FPD_TouchIdentifyMode mode);
	void(*FPDTAppendSeparator)(FS_LPVOID clientData, FS_LPVOID hMenu);
	void(*FPDTEnableMenuItem)(FS_LPVOID clientData, FS_LPVOID hMenu, FS_DWORD nIdentify, FS_BOOL bEnabled, FPD_TouchIdentifyMode mode);
	FS_DWORD(*FPDTTrackPopupMenu)(FS_LPVOID clientData, FS_LPVOID hMenu, FS_INT32 x, FS_INT32 y, FPD_TouchEditorPage editorPage);
	void(*FPDTDestroyMenu)(FS_LPVOID clientData, FS_LPVOID hMenu);
	FS_BOOL(*FPDTIsMenuEnabled)(FS_LPVOID clientData, FS_LPVOID hMenu);
	void(*FPDTInsertSeparator)(FS_LPVOID clientData, FS_LPVOID hMenu, FS_INT32 nPosition);
	void(*FPDTCheckMenuItem)(FS_LPVOID clientData, FS_LPVOID hMenu, FS_DWORD nIDItem, FS_BOOL bCheck);
	void(*FPDTInsertMenuItem)(FS_LPVOID clientData, FS_LPVOID hMenu, FS_INT32 nPosition, FS_INT32 nNewPos, FS_WideString lpNewItem);
}FPD_TouchPopupMenuHandlerCallbacksRec, *FPD_TouchPopupMenuHandlerCallbacks;
#endif

#ifndef FPD_TOUCHPROGRESSBAR
#define FPD_TOUCHPROGRESSBAR
/** @brief A callback for <a>FPD_EnumPageHandler</a>. See <a>FPDDocEnumPages</a>.
*/
typedef struct
{
	/* The size of data structure. It must be set to <Italic>sizeof</Italic>(<a>FPD_EDITCONTEXT</a>).  */
	unsigned long lStructSize;

	/** The user-supplied data. */
	FS_LPVOID		clientData;

	FS_LPVOID(*FPDTCreate)(FS_LPVOID clientData,FS_BOOL bShowCancelButton, FS_BOOL bCenter);
	void(*FPDTSetText)(FS_LPVOID clientData, FS_LPVOID bar, FS_WideString& text);
	void(*FPDTSetRange)(FS_LPVOID clientData, FS_LPVOID bar, FS_INT32 nLower, FS_INT32 nUpper);
	void(*FPDTSetCurrentValue)(FS_LPVOID clientData, FS_LPVOID bar, FS_INT32 nPos);
	FS_BOOL(*FPDTIsCancelled)(FS_LPVOID clientData, FS_LPVOID bar);
	void(*FPDTDestroy)(FS_LPVOID clientData, FS_LPVOID bar);
}FPD_TouchProgressBarHandlerCallbacksRec, *FPD_TouchProgressBarHandlerCallbacks;

#endif


#ifndef FPD_ITOUCHOPERATIONNOTIFYCALLBACKREC
#define FPD_ITOUCHOPERATIONNOTIFYCALLBACKREC
typedef struct
{
	/* The size of data structure. It must be set to <Italic>sizeof</Italic>(<a>FPD_EDITCONTEXT</a>).  */
	unsigned long lStructSize;

	/** The user-supplied data. */
	FS_LPVOID		clientData;

	FS_BOOL(*FPDTOnEnterEditing)(FS_LPVOID clientData, FPD_TouchEditorPage editorPage, FS_LPVOID pParam);
	FS_BOOL(*FPDTOnExistEditing)(FS_LPVOID clientData, FS_WideStringArray& arrayStr);
	void(*FPDTOnAddUndo)(FS_LPVOID clientData, FPD_ITouchUndoItem pUndoItem);
	void(*FPDTOnInvalid)(FS_LPVOID clientData, FPD_TouchEditorPage editorPage, FS_FloatRectArray pInvalidRects, FS_BOOL bRenderDataChange);
	void(*FPDTOnContentChange)(FS_LPVOID clientData, FPD_TouchOperation type, FPD_TouchEditorPage editorPage, FS_PtrArray& arr);
	void(*FPDTOnChangeDoc)(FS_LPVOID clientData, FPD_Document pdfDoc);
	void(*FPDTOnLROnPageWithoutText)(FS_LPVOID clientData);
}FPD_ITouchOperationNotifyCallbacksRec, *FPD_ITouchOperationNotifyCallbacks;
#endif

typedef void(*FPDTouchundoExecuteProc)(FS_LPVOID pVariable);
typedef void(*FPDTouchjobExecuteProc)();
typedef void(*FPDTouchwaitExecuteProc)();

#ifndef FPD_ITOUCHUNDOHANDLERCALLBACKREC
#define FPD_ITOUCHUNDOHANDLERCALLBACKREC
typedef struct
{
	/* The size of data structure. It must be set to <Italic>sizeof</Italic>(<a>FPD_EDITCONTEXT</a>).  */
	unsigned long lStructSize;

	/** The user-supplied data. */
	FS_LPVOID		clientData;

	void(*FPDTBeginEdit)(FS_LPVOID clientData, const FPDTouchundoExecuteProc proc, FS_LPVOID pClientData, FS_BOOL bOwnUndo);
	void(*FPDTEndEdit)(FS_LPVOID clientData);
	FS_BOOL(*FPDTIsUndoRedoing)(FS_LPVOID clientData);
	void(*FPDTBeginUndoGroup)(FS_LPVOID clientData);
	void(*FPDTEndUndoGroup)(FS_LPVOID clientData, FS_BOOL bEdit);
}FPD_ITouchUndoHandlerCallbacksRec, *FPD_ITouchUndoHandlerCallbacks;
#endif

#ifndef FPD_ITOUCHTEXTFORMATHANDLERCALLBACKREC
#define FPD_ITOUCHTEXTFORMATHANDLERCALLBACKREC
typedef struct
{
	/* The size of data structure. It must be set to <Italic>sizeof</Italic>(<a>FPD_EDITCONTEXT</a>).  */
	unsigned long lStructSize;

	/** The user-supplied data. */
	FS_LPVOID		clientData;

	FS_BOOL(*FPDTFindFontInFormatBar)(FS_LPVOID clientData, FS_WideString& fontName);
	void(*FPDTEnableFormatToolBar)(FS_LPVOID clientData, FS_BOOL bEnable);
	void(*FPDTSetOwnerFontNameArr)(FS_LPVOID clientData, const FS_WideStringArray& fontName);
	void(*FPDTUpdateTextFormat)(FS_LPVOID clientData, const FR_VTSecProps secProps, const FR_VTWordProps wordProps);
}FPD_ITouchTextFormatHandlerCallbacksRec, *FPD_ITouchTextFormatHandlerCallbacks;
#endif


#ifndef FPD_ITOUCHPROVIDERCALLBACKREC
#define FPD_ITOUCHPROVIDERCALLBACKREC
typedef struct
{
	/* The size of data structure. It must be set to <Italic>sizeof</Italic>(<a>FPD_EDITCONTEXT</a>).  */
	unsigned long lStructSize;

	/** The user-supplied data. */
	FS_LPVOID		clientData;

	FPD_ITouchOperationNotifyHandler(*FPDTGetOperationNotify)(FS_LPVOID clientData);
	FPD_ITouchClipboardHandler(*FPDTGetClipboardtHandler)(FS_LPVOID clientData);
	FPD_ITouchPopupMenuHandler(*FPDTGetPopupMenuHandler)(FS_LPVOID clientData);
	FPD_ITouchProgressBarHandler(*FPDTGetProgressBarHandler)(FS_LPVOID clientData);
	FS_LPVOID(*FPDTGetSystemHandler)(FS_LPVOID clientData);
	FPD_ITouchUndoHandler(*FPDTGetUndoHandler)(FS_LPVOID clientData);
	FPD_ITouchTextFormatHandler(*FPDTGetTextFormatHandler)(FS_LPVOID clientData);
	FS_AffineMatrix(*FPDTGetRenderMatrix)(FS_LPVOID clientData, FPD_TouchEditorPage editorPage);
	FS_WideString(*FPDTGetString)(FS_LPVOID clientData, FPD_TouchUIStringResourceID strID);
	FS_BOOL(*FPDTIsSupportBullets)(FS_LPVOID clientData, FS_WCHAR word);
	FR_SpellCheck(*FPDTGetSpellCheckEngine)(FS_LPVOID clientData);
	FR_Edit_FontMap(*FPDTGetFontMap)(FS_LPVOID clientData, FPD_Document pDoc);
	FS_FloatRect(*FPDTGetPageVisibleRect)(FS_LPVOID clientData, FPD_Page pPage);
	FS_BOOL(*FPDTGetVisiblePage)(FS_LPVOID clientData, FPD_Document pPDFDoc, FS_PtrArray visiblePage);
	FS_Rect(*FPDTGetPageRect)(FS_LPVOID clientData, FPD_TouchEditorPage editorPage);	
	FPD_Page(*FPDTGetPage)(FS_LPVOID clientData, FPD_Document pPDFDoc, FS_INT32 nPageIndex);
	FS_FLOAT(*FPDTGetPageViewScale)(FS_LPVOID clientData, FPD_TouchEditorPage editorPage);
	void(*FPDTGetGridMainLine)(FS_LPVOID clientData, FPD_TouchEditorPage editorPage, FS_DWordArray& xArray, FS_DWordArray& yArray);
	void(*FPDTGetGridLine)(FS_LPVOID clientData, FPD_TouchEditorPage editorPage, FS_DWordArray& xArray, FS_DWordArray& yArray);
	FS_BOOL(*FPDTIsSnap2GridEnabled)(FS_LPVOID clientData);
	FS_BOOL(*FPDTIsEastenArabicNumeralMode)(FS_LPVOID clientData);
	FPD_TouchEditorPage(*FPDTGetDefaultEditorPage)(FS_LPVOID clientData, FPD_Page pPDFPage);
	FPD_TouchEditorPage(*FPDTGetCurrentEditorPage)(FS_LPVOID clientData);
	FS_BOOL(*FPDTGotoPageView)(FS_LPVOID clientData, FS_INT32 index, FS_FLOAT left, FS_FLOAT top);
	void(*FPDTScrollToCenterPoint)(FS_LPVOID clientData, FS_INT32 index, FS_FloatPoint pdfPoint);
	FS_INT32(*FPDTGetRotation)(FS_LPVOID clientData);
	FS_LPVOID(*FPDTGetPageWindow)(FS_LPVOID clientData, FPD_TouchEditorPage editorPage);
	FS_DWORD(*FPDTGetTickCountTC)(FS_LPVOID clientData);
	FS_DWORD(*FPDTGetDoubleClickTimeTC)(FS_LPVOID clientData);
	FS_BOOL(*FPDTIsCurDocShowInBrowser)(FS_LPVOID clientData);
	FS_BOOL(*FPDTGetRunOCR)(FS_LPVOID clientData);
	FS_BOOL(*FPDTIsEditingUseHindiDigits)(FS_LPVOID clientData);
	FS_BOOL(*FPDTIsUseSystemSelectionColor)(FS_LPVOID clientData);
	FS_DWORD(*FPDTGetSysColorTC)(FS_LPVOID clientData,FS_INT32 nIndex);
	void(*FPDTGetPopupMenuString)(FS_LPVOID clientData, FS_INT32 nIndex, FS_WideString& cwMenuString);
	FPD_RenderOptions(*FPDTGetRenderOptions)(FS_LPVOID clientData, FPD_TouchEditorPage editorPage);
	FS_BOOL(*FPDTGetDocForceColor)(FS_LPVOID clientData, FS_COLORREF* color);
	FS_BOOL(*FPDTIsReplaceColor)(FS_LPVOID clientData, FS_BOOL& bReplace);
	FS_BOOL(*FPDTIsBlackAndWhiteOnly)(FS_LPVOID clientData, FS_BOOL& bAll);
	FS_Rect(*FPDTGetPageWindowClientRect)(FS_LPVOID clientData, FPD_TouchEditorPage editorPage);
	void(*FPDTGetToolbarFontName)(FS_LPVOID clientData,FS_WideString& wsFontName);
	FS_BOOL(*FPDTGetTextColor)(FS_LPVOID clientData, FS_COLORREF& color);
	FS_BOOL(*FPDTIsWipPluginAllowedPaste)(FS_LPVOID clientData);
	FPD_Page(*FPDTGetLatestPDFPage)(FS_LPVOID clientData, FPD_TouchEditorPage editorPage, FS_UINT nPageIndex);
	FS_BOOL(*FPDTIsTouchDevice)(FS_LPVOID clientData);
	void(*FPDTSetOwnUndoMode)(FS_LPVOID clientData, FPD_Document pDoc, FS_BOOL bOwnUnoMode);
	FS_BOOL(*FPDTIsJoinSpiltProcessing)(FS_LPVOID clientData);
	FS_FLOAT(*FPDTGetCustomIndent)(FS_LPVOID clientData, FPD_TouchEditorPage editorPage);	
	void(*FPDTChangeCursor)(FS_LPVOID clientData, FPD_TouchUICursorResourceType type);
	void(*FPDTEnableJoinSplitFormat)(FS_LPVOID clientData, FS_BOOL bEnable);
	void(*FPDTShowTip)(FS_LPVOID clientData, const FS_WideString& str, FPD_TouchEditorPage editorPage);
	void(*FPDTSetFocusTC)(FS_LPVOID clientData, FPD_TouchEditorPage editroPage);
	FS_BOOL(*FPDTShowMessageBox)(FS_LPVOID clientData, FPD_TouchUIDialogResourceID id, FS_DWORD flag, FS_LPVOID pData);
	void(*FPDTLoopWindowMessage)(FS_LPVOID clientData);
	FS_BOOL(*FPDTProcessByMultithreading)(FS_LPVOID clientData, FS_UINT jobCount, const FPDTouchjobExecuteProc job, const FPDTouchwaitExecuteProc waitting);
}FPD_ITouchProviderCallbacksRec, *FPD_ITouchProviderCallbacks;
#endif

#ifdef __cplusplus
};
#endif

#endif

/** @} */


/*@}*/