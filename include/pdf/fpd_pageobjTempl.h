﻿/*****************************************************************************

Copyright (C) 2010 Foxit Corporation
All rights reserved.

NOTICE: Foxit permits you to use, modify, and distribute this file
in accordance with the terms of the Foxit license agreement
accompanying it. If you have received this file from a source other
than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
 
*****************************************************************************/

//----------_V1----------
//*****************************
/* Path HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDPathNew
 * @details Creates a new empty PDF path data object.
 * @return   A new empty PDF path data object.
 * @note 
 */
INTERFACE(FPD_Path, FPDPathNew, (void))

/**
 * @brief FPDPathDestroy
 * @details Destroys the PDF path data object.
 * @param[in]  path The input PDF path data object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPathDestroy, (FPD_Path path))

/**
 * @brief FPDPathGetPointCount
 * @details Gets the point count int the path.
 * @param[in]  path The input PDF path data object.
 * @return   The point count int the path.
 * @note 
 */
INTERFACE(FS_INT32, FPDPathGetPointCount, (FPD_Path path))

/**
 * @brief FPDPathGetFlag
 * @details Gets the flag of specified path point.
 * @param[in]  path The input PDF path data object.
 * @param[in]  index Specifies the zero-based index of path point in the path.
 * @return   The flag of specified path point.
 * @note 
 */
INTERFACE(FS_INT32, FPDPathGetFlag, (FPD_Path path,  FS_INT32 index))

/**
 * @brief FPDPathGetPointX
 * @details Gets the x-coordinate of specified path point.
 * @param[in]  path The input PDF path data object.
 * @param[in]  index Specifies the zero-based index of path point in the path.
 * @return   The x-coordinate of specified path point.
 * @note 
 */
INTERFACE(FS_FLOAT, FPDPathGetPointX, (FPD_Path path,  FS_INT32 index))

/**
 * @brief FPDPathGetPointY
 * @details Gets the y-coordinate of specified path point.
 * @param[in]  path The input PDF path data object.
 * @param[in]  index Specifies the zero-based index of path point in the path.
 * @return   The y-coordinate of specified path point.
 * @note 
 */
INTERFACE(FS_FLOAT, FPDPathGetPointY, (FPD_Path path,  FS_INT32 index))

/**
 * @brief FPDPathGetPoint
 * @details Gets the path point in the path points array by the index.
 * @param[in]  path The input PDF path data object.
 * @param[in]  index The point index in the points array.
 * @return   The path point in the path points array by the index.
 * @note 
 */
INTERFACE(FS_PATHPOINT, FPDPathGetPoint, (FPD_Path path,  FS_INT32 index))

/**
 * @brief FPDPathGetBoundingBox
 * @details Gets bounding box of all control points.
 * @param[in]  path The input PDF path data object.
 * @return   The bounding box of all control points.
 * @note The result can be used as the bounding box of the whole filled path.
	*            However, when path is stroked using geometry pen, the actual bounding box can be much larger.
 */
INTERFACE(FS_FloatRect, FPDPathGetBoundingBox, (FPD_Path path))

/**
 * @brief FPDPathGetBoundingBox2
 * @details Calculates bounding box (guaranteed to contain all path, may be larger) for stroked path.
 * @param[in]  path The input PDF path data object.
 * @param[in]  lineWidth The line width used in stroking.
 * @param[in]  miterLimit The miter limit value for line joint in stroking.
 * @return   The bounding box for stroked path.
 * @note 
 */
INTERFACE(FS_FloatRect, FPDPathGetBoundingBox2, (FPD_Path path,  FS_FLOAT lineWidth,  FS_FLOAT miterLimit))

/**
 * @brief FPDPathTransform
 * @details Transforms this path.
 * @param[in]  path The input PDF path data object.
 * @param[in]  matrix The input matrix used to transform.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPathTransform, (FPD_Path path,  FS_AffineMatrix matrix))

/**
 * @brief FPDPathAppend
 * @details Appends a path. Optionally a matrix can be specified to transform the source path before appending.
 * @param[in]  path The input PDF path data object.
 * @param[in]  src The source path.
 * @param[in]  pMatrix The specified matrix. <a>NULL</a> means no transformation.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPathAppend, (FPD_Path path,  FPD_Path src,  FS_AffineMatrix* pMatrix))

/**
 * @brief FPDPathAppendRect
 * @details Appends a rectangle.
 * @param[in]  path The input PDF path data object.
 * @param[in]  left The x-coordinate of the left-bottom corner.
 * @param[in]  bottom The y-coordinate of the left-bottom corner.
 * @param[in]  right The x-coordinate of the right-top corner.
 * @param[in]  top The y-coordinate of the right-top corner.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPathAppendRect, (FPD_Path path,  FS_FLOAT left,  FS_FLOAT bottom,  FS_FLOAT right,  FS_FLOAT top))

/**
 * @brief FPDPathIsRect
 * @details Tests whether the path is a actually rectangle.
 * @param[in]  path The input PDF path data object.
 * @return   <a>TRUE</a> if the path is actually a rectangle, otherwise not.
 * @note 
 */
INTERFACE(FS_BOOL, FPDPathIsRect, (FPD_Path path))

/**
 * @brief FPDPathSetPointCount
 * @details Changes the path point count and prepares adequate allocated buffer.
 * @param[in]  path The input PDF path data object.
 * @param[in]  nPoints The new count of path point to change.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPathSetPointCount, (FPD_Path path,  FS_INT32 nPoints))

/**
 * @brief FPDPathSetPoint
 * @details Sets the point data for specified path point.
 * @param[in]  path The input PDF path data object.
 * @param[in]  index Specifies the zero-based index of path point in the path.
 * @param[in]  x The x-coordinate of the point to set.
 * @param[in]  y The y-coordinate of the point to set.
 * @param[in]  flag The flag of the point to set.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPathSetPoint, (FPD_Path path,  FS_INT32 index,  FS_FLOAT x,  FS_FLOAT y,  FS_INT32 flag))

/**
 * @brief FPDPathGetModify
 * @details The interface helps init the object if the object is NULL.
 * @param[in]  path The input PDF path data object.
 * @return   void.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
 */
INTERFACE(void, FPDPathGetModify, (FPD_Path path))

INTERFACE(void, FPDPathTrimPoints, (FPD_Path path,  FS_INT32 nPoints))

NumOfSelector(FPDPath)
ENDENUM

//*****************************
/* ClipPath HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDClipPathNew
 * @details Creates a new empty clip path data object.
 * @return   A new empty clip path data object.
 * @note 
 */
INTERFACE(FPD_ClipPath, FPDClipPathNew, (void))

/**
 * @brief FPDClipPathDestroy
 * @details Destroys the clip path data object.
 * @param[in]  path The input clip path data object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDClipPathDestroy, (FPD_ClipPath path))

/**
 * @brief FPDClipPathAppendPath
 * @details Appends a clipping path.
 * @param[in]  path The input clip path data object.
 * @param[in]  pathAppendTo The input clipping path.
 * @param[in]  type The clip type of the input clipping path.
 * @param[in]  bAutoMerge Whether to merge the clipping path automatically.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDClipPathAppendPath, (FPD_ClipPath path,  FPD_Path pathAppendTo,  FS_INT32 type,  FS_BOOL bAutoMerge))

/**
 * @brief FPDClipPathDeletePath
 * @details Removes a path from path list.
 * @param[in]  path The input clip path data object.
 * @param[in]  layerIndex The path index to remove.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDClipPathDeletePath, (FPD_ClipPath path,  FS_INT32 layerIndex))

/**
 * @brief FPDClipPathGetPathCount
 * @details Gets the count of paths in the clip path.
 * @param[in]  path The input clip path data object.
 * @return   The count of paths in the clip path.
 * @note 
 */
INTERFACE(FS_DWORD, FPDClipPathGetPathCount, (FPD_ClipPath path))

/**
 * @brief FPDClipPathTransform
 * @details Transforms this path.
 * @param[in]  path The input clip path data object.
 * @param[in]  matrix The input matrix used to transform.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDClipPathTransform, (FPD_ClipPath path,  const FS_AffineMatrix matrix))

/**
 * @brief FPDClipPathGetPath
 * @details Gets a path.
 * @param[in]  path The input clip path data object.
 * @param[in]  index Specifies the zero-based path index in the clip path.
 * @param[out]  outPath It receives the path.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDClipPathGetPath, (FPD_ClipPath path,  FS_INT32 index,  FPD_Path* outPath))

/**
 * @brief FPDClipPathGetClipType
 * @details Gets the clip type of specified path.
 * @param[in]  path The input clip path data object.
 * @param[in]  index Specifies the zero-based path index in the clip path
 * @return   The clip type of specified path
 * @note 
 */
INTERFACE(FS_INT32, FPDClipPathGetClipType, (FPD_ClipPath path,  FS_INT32 index))

/**
 * @brief FPDClipPathGetTextCount
 * @details Gets the count of text objects in the clip path.
 * @param[in]  path The input clip path data object.
 * @return   The count of text objects in the clip path.
 * @note 
 */
INTERFACE(FS_DWORD, FPDClipPathGetTextCount, (FPD_ClipPath path))

/**
 * @brief FPDClipPathGetClipBox
 * @details Gets the clip box of the clip path.
 * @param[in]  path The input clip path data object.
 * @return   The clip box of the clip path.
 * @note 
 */
INTERFACE(FS_FloatRect, FPDClipPathGetClipBox, (FPD_ClipPath path))

/**
 * @brief FPDClipPathGetText
 * @details Gets a text object.
 * @param[in]  path The input clip path data object.
 * @param[in]  i Specifies the zero-based text object index in the clip path.
 * @return   A text object.
 * @note 
 */
INTERFACE(FPD_PageObject, FPDClipPathGetText, (FPD_ClipPath path,  FS_INT32 i))

/**
 * @brief FPDClipPathAppendTexts
 * @details Appends clipping text objects.
 * @param[in]  path The input clip path data object.
 * @param[in]  pTextsBuf Pointer to clipping text objects to append.
 * @param[in]  count The count of clipping text objects to append.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDClipPathAppendTexts, (FPD_ClipPath path,  FPD_PageObject* pTextsBuf,  FS_INT32 count))

/**
 * @brief FPDClipPathSetCount
 * @details Estimates the count of path and text in the clip path data, and allocate the memory.
 * @param[in]  path The input clip path data object.
 * @param[in]  path_count The estimated count of path in the clip path data.
 * @param[in]  text_count The estimated count of text object in the clip path data.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDClipPathSetCount, (FPD_ClipPath path,  FS_INT32 path_count,  FS_INT32 text_count))

/**
 * @brief FPDClipPathIsNull
 * @details Tests whether the path data object is <a>NULL</a> or not.
 * @param[in]  path The input clip path data object.
 * @return   Non-zero means <a>NULL</a>, otherwise not <a>NULL</a>.
 * @note 
 */
INTERFACE(FS_BOOL, FPDClipPathIsNull, (FPD_ClipPath path))

/**
 * @brief FPDClipPathGetModify
 * @details The interface helps init the object if the object is NULL.
 * @param[in]  path The input clip path data object.
 * @return   void.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
 */
INTERFACE(void, FPDClipPathGetModify, (FPD_ClipPath path))

/**
 * @brief FPDClipPathGetPathPointer
 * @details Gets the pointer to the specified path.
 * @param[in]  path The input clip path data object.
 * @param[in]  index Specifies the zero-based path index in the clip path.
 * @return   The pointer to the specified path.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
 */
INTERFACE(FPD_Path, FPDClipPathGetPathPointer, (FPD_ClipPath path,  FS_INT32 index))

NumOfSelector(FPDClipPath)
ENDENUM

//*****************************
/* ColorState HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDColorStateNew
 * @details Creates a new empty PDF color state object.
 * @param[in]  void 
 * @return   A new empty PDF color state object.
 * @note 
 */
INTERFACE(FPD_ColorState, FPDColorStateNew, (void))

/**
 * @brief FPDColorStateDestroy
 * @details Destroys the PDF color state object.
 * @param[in]  clrState The input PDF color state object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDColorStateDestroy, (FPD_ColorState clrState))

/**
 * @brief FPDColorStateGetFillColor
 * @details Gets the filling color.
 * @param[in]  clrState The input PDF color state object.
 * @return   The filling color.
 * @note 
 */
INTERFACE(FPD_Color, FPDColorStateGetFillColor, (FPD_ColorState clrState))

/**
 * @brief FPDColorStateGetStrokeColor
 * @details Gets the stroking color.
 * @param[in]  clrState The input PDF color state object.
 * @return   The stroking color.
 * @note 
 */
INTERFACE(FPD_Color, FPDColorStateGetStrokeColor, (FPD_ColorState clrState))

/**
 * @brief FPDColorStateSetFillColor
 * @details Sets the filling normal color.
 * @param[in]  clrState The input PDF color state object.
 * @param[in]  clrSpace The color space of the filling color.
 * @param[in]  pValue The color component values in the specified color space.
 * @param[in]  nValues The count of the input parameters.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDColorStateSetFillColor, (FPD_ColorState clrState,  FPD_ColorSpace clrSpace,  FS_FLOAT* pValue,  FS_INT32 nValues))

/**
 * @brief FPDColorStateSetStrokeColor
 * @details Sets the stroking normal color.
 * @param[in]  clrState The input PDF color state object.
 * @param[in]  clrSpace The color space of the stroking color.
 * @param[in]  pValue The color component values in the specified color space.
 * @param[in]  nValues The count of the input parameters.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDColorStateSetStrokeColor, (FPD_ColorState clrState,  FPD_ColorSpace clrSpace,  FS_FLOAT* pValue,  FS_INT32 nValues))

/**
 * @brief FPDColorStateSetFillPatternColor
 * @details Sets the filling pattern color.
 * @param[in]  clrState The input PDF color state object.
 * @param[in]  pattern The input pattern.
 * @param[in]  pValue The input parameters for the pattern color.
 * @param[in]  nValues The count of the input parameters.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDColorStateSetFillPatternColor, (FPD_ColorState clrState,  FPD_Pattern pattern,  FS_FLOAT* pValue,  int nValues))

/**
 * @brief FPDColorStateSetStrokePatternColor
 * @details Sets the stroking pattern color.
 * @param[in]  clrState The input PDF color state object.
 * @param[in]  pattern The input pattern.
 * @param[in]  pValue The input parameters for the pattern color.
 * @param[in]  nValues The count of the input parameters.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDColorStateSetStrokePatternColor, (FPD_ColorState clrState,  FPD_Pattern pattern,  FS_FLOAT* pValue,  int nValues))

/**
 * @brief FPDColorStateIsNull
 * @details Tests whether the color state object is <a>NULL</a> or not.
 * @param[in]  clrState The input PDF color state object.
 * @return   Non-zero means <a>NULL</a>, otherwise not <a>NULL</a>.
 * @note 
 */
INTERFACE(FS_BOOL, FPDColorStateIsNull, (FPD_ColorState clrState))

/**
 * @brief FPDColorStateGetModify
 * @details The interface helps init the object if the object is NULL.
 * @param[in]  clrState The input PDF color state object.
 * @return   void.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 1.0
 */
INTERFACE(void, FPDColorStateGetModify, (FPD_ColorState clrState))

/**
 * @brief FPDColorStateNotUseFillColor
 * @details Do not use the filling mode.
 * @param[in]  clrState The input PDF color state object.
 * @return   void
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 1.0
 */
INTERFACE(void, FPDColorStateNotUseFillColor, (FPD_ColorState clrState))

NumOfSelector(FPDColorState)
ENDENUM

//*****************************
/* TextState HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDTextStateNew
 * @details Creates a new empty PDF text state object.
 * @param[in]  void 
 * @return   A new empty PDF text state object.
 * @note 
 */
INTERFACE(FPD_TextState, FPDTextStateNew, (void))

/**
 * @brief FPDTextStateDestroy
 * @details Destroys the PDF text state object.
 * @param[in]  textState The input PDF text state object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextStateDestroy, (FPD_TextState textState))

/**
 * @brief FPDTextStateGetFont
 * @details Gets the font.
 * @param[in]  textState The input PDF text state object.
 * @return   The font.
 * @note 
 */
INTERFACE(FPD_Font, FPDTextStateGetFont, (FPD_TextState textState))

/**
 * @brief FPDTextStateSetFont
 * @details Sets the font.
 * @param[in]  textState The input PDF text state object.
 * @param[in]  font The font to set.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextStateSetFont, (FPD_TextState textState,  FPD_Font font))

/**
 * @brief FPDTextStateGetFontSize
 * @details Gets the font size.
 * @param[in]  textState The input PDF text state object.
 * @return   The font size.
 * @note 
 */
INTERFACE(FS_FLOAT, FPDTextStateGetFontSize, (FPD_TextState textState))

/**
 * @brief FPDTextStateGetMatrix
 * @details Gets The text transformation matrix. Tt has 4 items.(matrix[4]).
 * @param[in]  textState The input PDF text state object.
 * @return   The text transformation matrix. It has 4 items.(matrix[4]).
 * @note 
 */
INTERFACE(FS_FLOAT*, FPDTextStateGetMatrix, (FPD_TextState textState))

/**
 * @brief FPDTextStateGetFontSizeV
 * @details Gets the vertical size in device units.
 * @param[in]  textState The input PDF text state object.
 * @return   The vertical size in device units.
 * @note 
 */
INTERFACE(FS_FLOAT, FPDTextStateGetFontSizeV, (FPD_TextState textState))

/**
 * @brief FPDTextStateGetFontSizeH
 * @details Gets the horizontal size in device units
 * @param[in]  textState The input PDF text state object.
 * @return   The horizontal size in device units
 * @note 
 */
INTERFACE(FS_FLOAT, FPDTextStateGetFontSizeH, (FPD_TextState textState))

/**
 * @brief FPDTextStateGetBaselineAngle
 * @details Gets the angle between device space X-axis and text baseline. In radians.
 * @param[in]  textState The input PDF text state object.
 * @return   The angle between device space X-axis and text baseline. In radians.
 * @note 
 */
INTERFACE(FS_FLOAT, FPDTextStateGetBaselineAngle, (FPD_TextState textState))

/**
 * @brief FPDTextStateGetShearAngle
 * @details Gets the angle that text space Y-axis shears in device space. In radians.
 * @param[in]  textState The input PDF text state object.
 * @return   The angle that text space Y-axis shears in device space. In radians.
 * @note 
 */
INTERFACE(FS_FLOAT, FPDTextStateGetShearAngle, (FPD_TextState textState))

/**
 * @brief FPDTextStateSetFontSize
 * @details Sets the font size.
 * @param[in]  textState The input PDF text state object.
 * @param[in]  fontSize The input font size.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextStateSetFontSize, (FPD_TextState textState,  FS_FLOAT fontSize))

/**
 * @brief FPDTextStateSetCharSpace
 * @details Sets the character space.
 * @param[in]  textState The input PDF text state object.
 * @param[in]  fCharSpace The input character space.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextStateSetCharSpace, (FPD_TextState textState,  FS_FLOAT fCharSpace))

/**
 * @brief FPDTextStateSetWordSpace
 * @details Sets the word space.
 * @param[in]  textState The input PDF text state object.
 * @param[in]  fWordSpace The input word space.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextStateSetWordSpace, (FPD_TextState textState,  FS_FLOAT fWordSpace))

/**
 * @brief FPDTextStateSetMatrix
 * @details Sets the text transformation matrix.
 * @param[in]  textState The input PDF text state object.
 * @param[in]  matrix The input text transformation matrix.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextStateSetMatrix, (FPD_TextState textState,  FS_FLOAT	matrix[4]))

/**
 * @brief FPDTextStateSetTextMode
 * @details Sets the text mode.
 * @param[in]  textState The input PDF text state object.
 * @param[in]  iTextMode The input text mode. Only 0 is valid in this version.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextStateSetTextMode, (FPD_TextState textState,  FS_INT32 iTextMode))

/**
 * @brief FPDTextStateSetTextCTM
 * @details Sets the CTM for stroking purpose.
 * @param[in]  textState The input PDF text state object.
 * @param[in]  CTM The input CTM.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextStateSetTextCTM, (FPD_TextState textState,  FS_FLOAT CTM[4]))

/**
 * @brief FPDTextStateGetTextMode
 * @details Gets the text mode.
 * @param[in]  textState The input PDF text state object.
 * @return   The text mode.
 * @note 
 */
INTERFACE(FS_INT32, FPDTextStateGetTextMode, (FPD_TextState textState))

/**
 * @brief FPDTextStateGetTextCTM
 * @details Gets the CTM for stroking purpose. The CTM array must be allocated and freed by caller. 
	* It must contain 4 elements.
 * @param[in]  textState The input PDF text state object.
 * @param[out]  outCTM It receives the text CTM for stroking purpose.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextStateGetTextCTM, (FPD_TextState textState,  FS_FLOAT* outCTM))

/**
 * @brief FPDTextStateGetCharSpace
 * @details Gets the character space.
 * @param[in]  textState The input PDF text state object.
 * @return   The character space.
 * @note 
 */
INTERFACE(FS_FLOAT, FPDTextStateGetCharSpace, (FPD_TextState textState))

/**
 * @brief FPDTextStateGetWordSpace
 * @details Gets the word space.
 * @param[in]  textState The input PDF text state object.
 * @return   The word space.
 * @note 
 */
INTERFACE(FS_FLOAT, FPDTextStateGetWordSpace, (FPD_TextState textState))

/**
 * @brief FPDTextStateIsNull
 * @details Tests whether the text state object is <a>NULL</a> or not.
 * @param[in]  textState The input PDF text state object.
 * @return   Non-zero means <a>NULL</a>, otherwise not <a>NULL</a>.
 * @note 
 */
INTERFACE(FS_BOOL, FPDTextStateIsNull, (FPD_TextState textState))

/**
 * @brief FPDTextStateGetModify
 * @details The interface helps init the object if the object is NULL.
 * @param[in]  textState The input PDF text state object.
 * @return   void.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 1.0
 */
INTERFACE(void, FPDTextStateGetModify, (FPD_TextState textState))

NumOfSelector(FPDTextState)
ENDENUM

//*****************************
/* GeneralState HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDGeneralStateNew
 * @details Creates a new empty PDF general state object.
 * @param[in]  void 
 * @return   A new empty PDF general state object.
 * @note 
 */
INTERFACE(FPD_GeneralState, FPDGeneralStateNew, (void))

/**
 * @brief FPDGeneralStateDestroy
 * @details Destroys the PDF general state data object.
 * @param[in]  genState The input PDF general state data object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDGeneralStateDestroy, (FPD_GeneralState genState))

/**
 * @brief FPDGeneralStateSetRenderIntent
 * @details Sets the rendering intent.
 * @param[in]  genState The input PDF general state data object.
 * @param[in]  ri The input rendering intent.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDGeneralStateSetRenderIntent, (FPD_GeneralState genState,  FS_LPCSTR ri))

/**
 * @brief FPDGeneralStateGetBlendType
 * @details Gets the blend mode.
 * @param[in]  genState The input PDF general state data object.
 * @return   The blend mode.
 * @note 
 */
INTERFACE(FS_INT32, FPDGeneralStateGetBlendType, (FPD_GeneralState genState))

/**
 * @brief FPDGeneralStateGetAlpha
 * @details Gets the current filling or stroking alpha constant.
 * @param[in]  genState The input PDF general state data object.
 * @param[in]  bStroke Whether to get the current stroking alpha constant.
 * @return   The current filling or stroking alpha constant.
 * @note 
 */
INTERFACE(FS_INT32, FPDGeneralStateGetAlpha, (FPD_GeneralState genState,  FS_BOOL bStroke))

/**
 * @brief FPDGeneralStateSetBlendMode
 * @details Sets the current blend mode name.
 * @param[in]  genState The input PDF general state data object.
 * @param[in]  BlendMode The input current blend mode name.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDGeneralStateSetBlendMode, (FPD_GeneralState genState,  char BlendMode[16]))

/**
 * @brief FPDGeneralStateSetBlendType
 * @details Sets the current blend mode to be used in the transparent imaging model.
 * @param[in]  genState The input PDF general state data object.
 * @param[in]  iBlendType The input blend type.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDGeneralStateSetBlendType, (FPD_GeneralState genState,  FS_INT32 iBlendType))

/**
 * @brief FPDGeneralStateSetSoftMask
 * @details Sets the current soft mask, specifying the mask shape or mask opacity values to be used in the transparent imaging model.
 * @param[in]  genState The input PDF general state data object.
 * @param[in]  softMask The input current soft mask.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDGeneralStateSetSoftMask, (FPD_GeneralState genState,  FPD_Object softMask))

/**
 * @brief FPDGeneralStateSetSoftMaskMatrix
 * @details Sets the matrix of the current soft mask.
 * @param[in]  genState The input PDF general state data object.
 * @param[in]  SoftMaskMatrix The input matrix of the current soft mask.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDGeneralStateSetSoftMaskMatrix, (FPD_GeneralState genState,  FS_FLOAT SoftMaskMatrix[6]))

/**
 * @brief FPDGeneralStateSetStrokeAlpha
 * @details Sets The current stroking alpha constant, specifying the constant shape or constant 
	*            opacity value to be used for stroking operations in the transparent imaging model.
 * @param[in]  genState The input PDF general state data object.
 * @param[in]  fStrokeAlpha The input current stroking alpha constant.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDGeneralStateSetStrokeAlpha, (FPD_GeneralState genState,  FS_FLOAT fStrokeAlpha))

/**
 * @brief FPDGeneralStateSetFillAlpha
 * @details Same as stroking alpha, but for non-stroking operations.
 * @param[in]  genState The input PDF general state data object.
 * @param[in]  fFillAlpha The input current filling alpha constant.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDGeneralStateSetFillAlpha, (FPD_GeneralState genState,  FS_FLOAT fFillAlpha))

/**
 * @brief FPDGeneralStateIsNull
 * @details Tests whether the general state object is <a>NULL</a> or not.
 * @param[in]  genState The input PDF general state data object.
 * @return   Non-zero means <a>NULL</a>, otherwise not <a>NULL</a>.
 * @note 
 */
INTERFACE(FS_BOOL, FPDGeneralStateIsNull, (FPD_GeneralState genState))

/**
 * @brief FPDGeneralStateGetModify
 * @details The interface helps init the object if the object is NULL.
 * @param[in]  genState The input PDF general state object.
 * @return   void.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 1.0
 */
INTERFACE(void, FPDGeneralStateGetModify, (FPD_GeneralState genState))

NumOfSelector(FPDGeneralState)
ENDENUM

//*****************************
/* GraphState HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDGraphStateNew
 * @details Creates a new empty PDF graphic state object.
 * @param[in]  void 
 * @return   A new empty PDF graphic state object.
 * @note 
 */
INTERFACE(FPD_GraphState, FPDGraphStateNew, (void))

/**
 * @brief FPDGraphStateDestroy
 * @details Destroys a graphic state object.
 * @param[in]  graphState The graphic state object to be destroyed.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDGraphStateDestroy, (FPD_GraphState graphState))

/**
 * @brief FPDGraphStateSetDashCount
 * @details In order to keep heap integrity, the function is used to allocate enough buffer for dash array.
 * @param[in]  graphState The input graphic state object.
 * @param[in]  count The new count of dash points.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDGraphStateSetDashCount, (FPD_GraphState graphState,  FS_INT32 count))

/**
 * @brief FPDGraphStateGetDashCount
 * @details Gets the total size of dash array.
 * @param[in]  graphState The input graphic state object.
 * @return   The total size of dash array.
 * @note 
 */
INTERFACE(FS_INT32, FPDGraphStateGetDashCount, (FPD_GraphState graphState))

/**
 * @brief FPDGraphStateGetDashArray
 * @details Gets the dash array.
 * @param[in]  graphState The input graphic state object.
 * @return   The dash array used by graphic state object.
 * @note 
 */
INTERFACE(FS_FLOAT*, FPDGraphStateGetDashArray, (FPD_GraphState graphState))

/**
 * @brief FPDGraphStateGetDashPhase
 * @details Gets the dash phase for line dash pattern.
 * @param[in]  graphState The input graphic state object.
 * @return   The dash phase for line dash pattern.
 * @note 
 */
INTERFACE(FS_FLOAT, FPDGraphStateGetDashPhase, (FPD_GraphState graphState))

/**
 * @brief FPDGraphStateSetDashPhase
 * @details Sets the new dash phase for line dash pattern.
 * @param[in]  graphState The input graphic state object.
 * @param[in]  dashPhase The new dash phase.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDGraphStateSetDashPhase, (FPD_GraphState graphState,  FS_FLOAT dashPhase))

/**
 * @brief FPDGraphStateGetLineCap
 * @details Gets the line cap style.
 * @param[in]  graphState The input graphic state object.
 * @return   The line cap style.
 * @note 
 */
INTERFACE(FPD_LineCap, FPDGraphStateGetLineCap, (FPD_GraphState graphState))

/**
 * @brief FPDGraphStateSetLineCap
 * @details Sets the new line cap style.
 * @param[in]  graphState The input graphic state object.
 * @param[in]  cap The new style of line cap.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDGraphStateSetLineCap, (FPD_GraphState graphState,  FPD_LineCap cap))

/**
 * @brief FPDGraphStateGetLineJoin
 * @details Gets the line join style.
 * @param[in]  graphState The input graphic state object.
 * @return   The line join style.
 * @note 
 */
INTERFACE(FPD_LineJoin, FPDGraphStateGetLineJoin, (FPD_GraphState graphState))

/**
 * @brief FPDGraphStateSetLineJoin
 * @details Sets the new style of line join.
 * @param[in]  graphState The input graphic state object.
 * @param[in]  join The new style of line join.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDGraphStateSetLineJoin, (FPD_GraphState graphState,  FPD_LineJoin join))

/**
 * @brief FPDGraphStateGetMiterLimit
 * @details Gets the miter limit for line join.
 * @param[in]  graphState The input graphic state object.
 * @return   The miter limit for line join.
 * @note 
 */
INTERFACE(FS_FLOAT, FPDGraphStateGetMiterLimit, (FPD_GraphState graphState))

/**
 * @brief FPDGraphStateSetMiterLimit
 * @details Sets the new miter limit for line join.
 * @param[in]  graphState The input graphic state object.
 * @param[in]  limit The new miter limit for line join.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDGraphStateSetMiterLimit, (FPD_GraphState graphState,  FS_FLOAT limit))

/**
 * @brief FPDGraphStateGetLineWidth
 * @details Gets the line width.
 * @param[in]  graphState The input graphic state object.
 * @return   The line width.
 * @note 
 */
INTERFACE(FS_FLOAT, FPDGraphStateGetLineWidth, (FPD_GraphState graphState))

/**
 * @brief FPDGraphStateSetLineWidth
 * @details Sets new width of lines.
 * @param[in]  graphState The input graphic state object.
 * @param[in]  width The new width of lines.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDGraphStateSetLineWidth, (FPD_GraphState graphState,  FS_FLOAT width))

/**
 * @brief FPDGraphStateIsNull
 * @details Tests whether the graphic state object is <a>NULL</a> or not.
 * @param[in]  graphState The input graphic state object.
 * @return   Non-zero means <a>NULL</a>, otherwise not <a>NULL</a>.
 * @note 
 */
INTERFACE(FS_BOOL, FPDGraphStateIsNull, (FPD_GraphState graphState))

/**
 * @brief FPDGraphStateGetModify
 * @details The interface helps init the object if the object is NULL.
 * @param[in]  graphState The input PDF graph state object.
 * @return   void.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 1.0
 */
INTERFACE(void, FPDGraphStateGetModify, (FPD_GraphState graphState))

/**
 * @brief FPDGraphStateSetDashArray
 * @details Sets the dash phase for line dash pattern.
 * @param[in]  graphState The input graphic state object.
 * @param[in]  dashArray The input dash array.
 * @return   void
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 1.0
 */
INTERFACE(void, FPDGraphStateSetDashArray, (FPD_GraphState graphState,  FS_FLOAT* dashArray))

NumOfSelector(FPDGraphState)
ENDENUM

//*****************************
/* PageObject HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDPageObjectNew
 * @details Creates a new empty PDF page object.
 * @param[in]  type The input page object type.
 * @return   A page object.
 * @note 
 */
INTERFACE(FPD_PageObject, FPDPageObjectNew, (FS_INT32 type))

/**
 * @brief FPDPageObjectDestroy
 * @details Destroys the PDF page object.
 * @param[in]  pageObj The input PDF page object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPageObjectDestroy, (FPD_PageObject pageObj))

/**
 * @brief FPDPageObjectClone
 * @details Clones a page object.
 * @param[in]  pageObj The source object.
 * @return   The cloned object.
 * @note 
 */
INTERFACE(FPD_PageObject, FPDPageObjectClone, (FPD_PageObject pageObj))

/**
 * @brief FPDPageObjectCopy
 * @details Copies from another page object.
 * @param[in]  pageObj The input PDF page object.
 * @param[in]  srcObject The source page object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPageObjectCopy, (FPD_PageObject pageObj,  const FPD_PageObject srcObject))

/**
 * @brief FPDPageObjectRemoveClipPath
 * @details Removes clipping path of the object.
 * @param[in]  pageObj The input PDF page object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPageObjectRemoveClipPath, (FPD_PageObject pageObj))

/**
 * @brief FPDPageObjectAppendClipPath
 * @details Appends a clipping path.
 * @param[in]  pageObj The input PDF page object.
 * @param[in]  path The input clipping path.
 * @param[in]  type The clip type of the input clipping path.
 * @param[in]  bAutoMerge Whether to merge the clipping path automatically.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPageObjectAppendClipPath, (FPD_PageObject pageObj,  FPD_Path path,  FS_INT32 type,  FS_BOOL bAutoMerge))

/**
 * @brief FPDPageObjectCopyClipPath
 * @details Copies clipping path from another object.
 * @param[in]  pageObj The input PDF page object.
 * @param[in]  srcObj The source page object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPageObjectCopyClipPath, (FPD_PageObject pageObj,  FPD_PageObject srcObj))

/**
 * @brief FPDPageObjectTransformClipPath
 * @details Transforms the clip path. Rotate, shear, or move clip path.
 * @param[in]  pageObj The input PDF page object.
 * @param[in]  matrix The matrix used to transform.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPageObjectTransformClipPath, (FPD_PageObject pageObj,  FS_AffineMatrix matrix))

/**
 * @brief FPDPageObjectSetColorState
 * @details Sets the color state.
 * @param[in]  pageObj The input PDF page object.
 * @param[in]  state The new color state.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPageObjectSetColorState, (FPD_PageObject pageObj,  FPD_ColorState state))

/**
 * @brief FPDPageObjectGetBBox
 * @details Gets the bounding box of the page object, optionally with a transformation matrix.
 * @param[in]  pageObj The input PDF page object.
 * @param[in]  pMatrix The input transformation matrix.
 * @return   The bounding box of the page object.
 * @note 
 */
INTERFACE(FS_Rect, FPDPageObjectGetBBox, (FPD_PageObject pageObj,  FS_AffineMatrix* pMatrix))

/**
 * @brief FPDPageObjectGetOriginalBBox
 * @details Gets the original bounding box of the page object.
 * @param[in]  pageObj The input PDF page object.
 * @return   The original bounding box of the page object
 * @note 
 */
INTERFACE(FS_FloatRect, FPDPageObjectGetOriginalBBox, (FPD_PageObject pageObj))

/**
 * @brief FPDPageObjectGetType
 * @details The page object type.
 * @param[in]  pageObj The input PDF page object.
 * @return   Gets the page object type.
 * @note 
 */
INTERFACE(FS_INT32, FPDPageObjectGetType, (FPD_PageObject pageObj))

/**
 * @brief FPDPageObjectGetClipPath
 * @details Gets the clip path state.
 * @param[in]  pageObj The input PDF page object.
 * @return   The clip path state.
 * @note 
 */
INTERFACE(FPD_ClipPath, FPDPageObjectGetClipPath, (FPD_PageObject pageObj))

/**
 * @brief FPDPageObjectGetGraphState
 * @details Gets the graph state. For graphs and type3 font or stroke texts.
 * @param[in]  pageObj The input PDF page object.
 * @return   The graph state for graphs and type3 font or stroke texts.
 * @note 
 */
INTERFACE(FPD_GraphState, FPDPageObjectGetGraphState, (FPD_PageObject pageObj))

/**
 * @brief FPDPageObjectGetColorState
 * @details Gets the color state. For texts, graphs and uncolored images.
 * @param[in]  pageObj The input PDF page object.
 * @return   The color state for texts, graphs and uncolored images.
 * @note 
 */
INTERFACE(FPD_ColorState, FPDPageObjectGetColorState, (FPD_PageObject pageObj))

/**
 * @brief FPDPageObjectGetTextState
 * @details Gets the text state. For texts only.
 * @param[in]  pageObj The input PDF page object.
 * @return   The text state for texts only.
 * @note 
 */
INTERFACE(FPD_TextState, FPDPageObjectGetTextState, (FPD_PageObject pageObj))

/**
 * @brief FPDPageObjectGetGeneralState
 * @details Gets the general state. For all objects.
 * @param[in]  pageObj The input PDF page object.
 * @return   The general state for all objects.
 * @note 
 */
INTERFACE(FPD_GeneralState, FPDPageObjectGetGeneralState, (FPD_PageObject pageObj))

/**
 * @brief FPDPageObjectGetContentMark
 * @details Gets the content mark.
 * @param[in]  pageObj The input PDF page object.
 * @return   The content mark. For all objects.
 * @note 
 */
INTERFACE(FPD_ContentMark, FPDPageObjectGetContentMark, (FPD_PageObject pageObj))

/**
 * @brief FPDPageObjectDefaultStates
 * @details Sets all graphic states to default.
 * @param[in]  pageObj The input PDF page object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPageObjectDefaultStates, (FPD_PageObject pageObj))

/**
 * @brief FPDPageObjectCopyStates
 * @details Copies from another graphic states.
 * @param[in]  pageObj The input PDF page object.
 * @param[in]  src The input graphic states.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPageObjectCopyStates, (FPD_PageObject pageObj,  const FPD_PageObject src))

/**
 * @brief FPDPageObjectSetGraphState
 * @details Sets the graph state.
 * @param[in]  pageObj The input PDF page object.
 * @param[in]  grahpState The graphic state to be set.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPageObjectSetGraphState, (FPD_PageObject pageObj,  FPD_GraphState grahpState))

/**
 * @brief FPDPageObjectSetTextState
 * @details Sets the text state.
 * @param[in]  pageObj The input PDF page object.
 * @param[in]  textState The text state to be set.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPageObjectSetTextState, (FPD_PageObject pageObj,  FPD_TextState textState))

/**
 * @brief FPDPageObjectSetGeneralState
 * @details Sets the general state.
 * @param[in]  pageObj The input PDF page object.
 * @param[in]  genState The general state to be set.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPageObjectSetGeneralState, (FPD_PageObject pageObj,  FPD_GeneralState genState))

/**
 * @brief FPDPageObjectHasClipPath
 * @details Checks whether the page object has the clip path or not.
 * @param[in]  pageObj The input PDF page object.
 * @return   <a>TRUE</a> if the page object has the clip path, otherwise not.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 2.0
 */
INTERFACE(FS_BOOL, FPDPageObjectHasClipPath, (FPD_PageObject pageObj))

/**
 * @brief FPDPageObjectGetContentMark2
 * @details Gets the content mark.
 * @param[in]  pageObj The input PDF page object.
 * @param[in]  bGetModifiableCopy Get a modifiable copy of the object. If the reference was refer to null, 
	* then a new object will be created. The returned pointer can be used to alter the object content.
 * @return   The content mark. For all objects.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 8.3.1
 */
INTERFACE(FPD_ContentMark, FPDPageObjectGetContentMark2, (FPD_PageObject pageObj,  FS_BOOL bGetModifiableCopy))

NumOfSelector(FPDPageObject)
ENDENUM

//*****************************
/* TextObject HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDTextObjectNew
 * @details Creates a new empty PDF text object.
 * @param[in]  void 
 * @return   The PDF text object
 * @note 
 */
INTERFACE(FPD_PageObject, FPDTextObjectNew, (void))

/**
 * @brief FPDTextObjectDestroy
 * @details Destroys the input PDF text object. If it is added to the page, it is taken over and don't destroy it.
 * @param[in]  objText The input PDF text object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextObjectDestroy, (FPD_PageObject objText))

/**
 * @brief FPDTextObjectCountItems
 * @details Gets the count of text object items.
 * @param[in]  objText The input PDF text object.
 * @return   The count of text object items.
 * @note 
 */
INTERFACE(FS_INT32, FPDTextObjectCountItems, (FPD_PageObject objText))

/**
 * @brief FPDTextObjectGetItemInfo
 * @details Gets specified text object item information.
 * @param[in]  objText The input PDF text object.
 * @param[in]  index Specifies zero-based item index in the text object.
 * @return   The specified text object item information.
 * @note 
 */
INTERFACE(FPD_TextObjectItem, FPDTextObjectGetItemInfo, (FPD_PageObject objText,  FS_INT32 index))

/**
 * @brief FPDTextObjectCountChars
 * @details Gets the count of characters in the text object.
 * @param[in]  objText The input PDF text object.
 * @return   The count of characters in the text object.
 * @note 
 */
INTERFACE(FS_INT32, FPDTextObjectCountChars, (FPD_PageObject objText))

/**
 * @brief FPDTextObjectGetCharInfo
 * @details Gets the information of specified character.
 * @param[in]  objText The input PDF text object.
 * @param[in]  index Specifies zero-based character index in the text object.
 * @param[out]  outCharcode It receives the character code.
 * @param[out]  outKerning It receives the kerning(x-direction only).
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextObjectGetCharInfo, (FPD_PageObject objText,  FS_INT32 index,  FS_DWORD* outCharcode,  FS_FLOAT* outKerning))

/**
 * @brief FPDTextObjectGetPosX
 * @details Gets the x-coordinate of the origin in the device space
 * @param[in]  objText The input PDF text object.
 * @return   The x-coordinate of the origin in the device space
 * @note 
 */
INTERFACE(FS_FLOAT, FPDTextObjectGetPosX, (FPD_PageObject objText))

/**
 * @brief FPDTextObjectGetPosY
 * @details Gets the y-coordinate of the origin in the device space.
 * @param[in]  objText The input PDF text object.
 * @return   The y-coordinate of the origin in the device space.
 * @note 
 */
INTERFACE(FS_FLOAT, FPDTextObjectGetPosY, (FPD_PageObject objText))

/**
 * @brief FPDTextObjectGetTextMatrix
 * @details Gets matrix from text space to object space.
 * @param[in]  objText The input PDF text object.
 * @param[out]  outMatrix It receives the matrix from text space to object space.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextObjectGetTextMatrix, (FPD_PageObject objText,  FS_AffineMatrix* outMatrix))

/**
 * @brief FPDTextObjectGetFont
 * @details Gets the font.
 * @param[in]  objText The input PDF text object.
 * @return   The font.
 * @note 
 */
INTERFACE(FPD_Font, FPDTextObjectGetFont, (FPD_PageObject objText))

/**
 * @brief FPDTextObjectGetFontSize
 * @details Gets the font size.
 * @param[in]  objText The input PDF text object.
 * @return   The font size.
 * @note 
 */
INTERFACE(FS_FLOAT, FPDTextObjectGetFontSize, (FPD_PageObject objText))

/**
 * @brief FPDTextObjectSetEmpty
 * @details Sets the text object to be empty.
 * @param[in]  objText The input PDF text object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextObjectSetEmpty, (FPD_PageObject objText))

/**
 * @brief FPDTextObjectSetText
 * @details Sets a single text segment without any kerning inside.
 * @param[in]  objText The input PDF text object.
 * @param[in]  strText The input text segment.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextObjectSetText, (FPD_PageObject objText,  const FS_ByteString strText))

/**
 * @brief FPDTextObjectSetText2
 * @details Sets text using segmented fashion.
 * @param[in]  objText The input PDF text object.
 * @param[in]  strTextArr The input text segments.
 * @param[in]  pKerning The kerning array.
 * @return   void
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
 */
INTERFACE(void, FPDTextObjectSetText2, (FPD_PageObject objText,  FS_ByteStringArray strTextArr,  FS_FLOAT* pKerning))

/**
 * @brief FPDTextObjectSetText3
 * @details Sets text using char-kerning pair fashion.
 * @param[in]  objText The input PDF text object.
 * @param[in]  nChars The count of input character codes.
 * @param[in]  pCharCodes The input character codes.
 * @param[in]  pKernings The input kerning array.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextObjectSetText3, (FPD_PageObject objText,  FS_INT32 nChars,  FS_DWORD* pCharCodes,  FS_FLOAT* pKernings))

/**
 * @brief FPDTextObjectSetPosition
 * @details Sets the origin position in device space.
 * @param[in]  objText The input PDF text object.
 * @param[in]  x The x-coordinate in device space.
 * @param[in]  y The y-coordinate in device space.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextObjectSetPosition, (FPD_PageObject objText,  FS_FLOAT x,  FS_FLOAT y))

/**
 * @brief FPDTextObjectSetTextState
 * @details Sets the text state.
 * @param[in]  objText The input PDF text object.
 * @param[in]  textState The new text state.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextObjectSetTextState, (FPD_PageObject objText,  FPD_TextState textState))

/**
 * @brief FPDTextObjectTransform
 * @details Transforms the text object.
 * @param[in]  objText The input PDF text object.
 * @param[in]  matrix The transformation matrix.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextObjectTransform, (FPD_PageObject objText,  FS_AffineMatrix matrix))

/**
 * @brief FPDTextObjectCalcCharPos
 * @details Calculates origin positions in text space, for each character.
 * @param[in]  objText The input PDF text object.
 * @param[out]  outPosArray It receives the character position array.
 * @return   void
 * @note The position array must be allocated and freed by caller. It must contain at least nChars*2 elements.
	*   For each character, the origin position (along the text baseline) and next origin position will be calculated.
 */
INTERFACE(void, FPDTextObjectCalcCharPos, (FPD_PageObject objText,  FS_FLOAT* outPosArray))

/**
 * @brief FPDTextObjectSetData
 * @details Sets text data.
 * @param[in]  objText The input PDF text object.
 * @param[in]  nChars The count of characters to set.
 * @param[in]  pCharCodes The input character codes array.
 * @param[in]  pCharPos The input character positions array in text space.
 * @param[in]  x The x-coordinate of the origin position, in device space.
 * @param[in]  y The y-coordinate of the origin position, in device space.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextObjectSetData, (FPD_PageObject objText,  FS_INT32 nChars,  FS_DWORD* pCharCodes,  FS_FLOAT* pCharPos,  FS_FLOAT x,  FS_FLOAT y))

/**
 * @brief FPDTextObjectGetData
 * @details Gets text data.
 * @param[in]  objText The input PDF text object.
 * @param[out]  outCharCount It receives the count of characters.
 * @param[out]  outCharCodes It receives the character codes array.
 * @param[out]  outCharPos It receives the character positions array in text space.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextObjectGetData, (FPD_PageObject objText,  FS_INT32* outCharCount,  FS_DWORD** outCharCodes,  FS_FLOAT** outCharPos))

/**
 * @brief FPDTextObjectRecalcPositionData
 * @details Recalculates the position data.
 * @param[in]  objText The input PDF text object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDTextObjectRecalcPositionData, (FPD_PageObject objText))

NumOfSelector(FPDTextObject)
ENDENUM

//*****************************
/* PathObject HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDPathObjectNew
 * @details Creates a new empty PDF path object.
 * @param[in]  void 
 * @return   A new empty PDF path object.
 * @note 
 */
INTERFACE(FPD_PageObject, FPDPathObjectNew, (void))

/**
 * @brief FPDPathObjectDestroy
 * @details Destroys the PDF path object.
 * @param[in]  objPath The input PDF path object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPathObjectDestroy, (FPD_PageObject objPath))

/**
 * @brief FPDPathObjectTransform
 * @details Transforms the path object.
 * @param[in]  objPath The input PDF path object.
 * @param[in]  matrix The transformation matrix.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPathObjectTransform, (FPD_PageObject objPath,  FS_AffineMatrix matrix))

/**
 * @brief FPDPathObjectSetGraphState
 * @details Sets the graph state.
 * @param[in]  objPath The input PDF path object.
 * @param[in]  graphState The input new graph state.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPathObjectSetGraphState, (FPD_PageObject objPath,  FPD_GraphState graphState))

/**
 * @brief FPDPathObjectCalcBoundingBox
 * @details Calculates the bounding box.
 * @param[in]  objPath The input PDF path object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPathObjectCalcBoundingBox, (FPD_PageObject objPath))

/**
 * @brief FPDPathObjectGetTransformMatrix
 * @details Gets the transformation matrix.
 * @param[in]  objPath The input PDF path object.
 * @param[out]  outmatrix It receives the transformation matrix.
 * @return   void
 * @note Transformation matrix used to transform the path coordinates. Also used to determine line geometry.
 */
INTERFACE(void, FPDPathObjectGetTransformMatrix, (FPD_PageObject objPath,  FS_AffineMatrix* outmatrix))

/**
 * @brief FPDPathObjectSetTransformMatrix
 * @details Sets the transformation matrix.
 * @param[in]  objPath The input PDF path object.
 * @param[in]  matrix The transformation matrix.
 * @return   void
 * @note Transformation matrix used to transform the path coordinates. Also used to determine line geometry.
 */
INTERFACE(void, FPDPathObjectSetTransformMatrix, (FPD_PageObject objPath,  const FS_AffineMatrix* matrix))

/**
 * @brief FPDPathObjectGetPath
 * @details Gets the path data object. Reference to path data.
 * @param[in]  objPath The input PDF path object.
 * @return   The path data object.
 * @note 
 */
INTERFACE(FPD_Path, FPDPathObjectGetPath, (FPD_PageObject objPath))

/**
 * @brief FPDPathObjectIsStrokeMode
 * @details Tests whether the paint mode for the path object is stroking mode.
 * @param[in]  objPath The input PDF path object.
 * @return   <a>TRUE</a> to stroke the path, otherwise not.
 * @note 
 */
INTERFACE(FS_BOOL, FPDPathObjectIsStrokeMode, (FPD_PageObject objPath))

/**
 * @brief FPDPathObjectSetStrokeMode
 * @details Sets whether to stroke the path.
 * @param[in]  objPath The input PDF path object.
 * @param[in]  bStroke True to stroke the path, otherwise not.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPathObjectSetStrokeMode, (FPD_PageObject objPath,  FS_BOOL bStroke))

/**
 * @brief FPDPathObjectGetFillMode
 * @details Gets the filling mode of the page object.
 * @param[in]  objPath The input PDF path object.
 * @return   The filling mode code. See <a>FSFillingModeFlags</a>.
 * @note 
 */
INTERFACE(FS_INT32, FPDPathObjectGetFillMode, (FPD_PageObject objPath))

/**
 * @brief FPDPathObjectSetFillMode
 * @details Sets the new filling mode.
 * @param[in]  objPath The input PDF path object.
 * @param[in]  mode The new filling mode. See <a>FSFillingModeFlags</a>.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDPathObjectSetFillMode, (FPD_PageObject objPath,  FS_INT32 mode))

NumOfSelector(FPDPathObject)
ENDENUM

//*****************************
/* ImageObject HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDImageObjectNew
 * @details Creates a new empty PDF image object.
 * @param[in]  void 
 * @return   A new empty PDF image object.
 * @note 
 */
INTERFACE(FPD_PageObject, FPDImageObjectNew, (void))

/**
 * @brief FPDImageObjectDestroy
 * @details Destroys the PDF image object.
 * @param[in]  objImage The input PDF image object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDImageObjectDestroy, (FPD_PageObject objImage))

/**
 * @brief FPDImageObjectTransform
 * @details Transforms the image object.
 * @param[in]  objImage The input PDF image object.
 * @param[in]  matrix The transformation matrix.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDImageObjectTransform, (FPD_PageObject objImage,  FS_AffineMatrix matrix))

/**
 * @brief FPDImageObjectCalcBoundingBox
 * @details Calculates the bounding box.
 * @param[in]  objImage The input PDF image object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDImageObjectCalcBoundingBox, (FPD_PageObject objImage))

/**
 * @brief FPDImageObjectGetTransformMatrix
 * @details Gets the transformation matrix.
 * @param[in]  objImage The input PDF image object.
 * @param[out]  outmatrix It receives the transformation matrix.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDImageObjectGetTransformMatrix, (FPD_PageObject objImage,  FS_AffineMatrix* outmatrix))

/**
 * @brief FPDImageObjectSetTransformMatrix
 * @details Sets the transformation matrix.
 * @param[in]  objImage The input PDF image object.
 * @param[in]  matrix The transformation matrix.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDImageObjectSetTransformMatrix, (FPD_PageObject objImage,  const FS_AffineMatrix* matrix))

/**
 * @brief FPDImageObjectGetImage
 * @details Gets the image data object.
 * @param[in]  objImage The input PDF image object.
 * @return   The image data object.
 * @note 
 */
INTERFACE(FPD_Image, FPDImageObjectGetImage, (FPD_PageObject objImage))

/**
 * @brief FPDImageObjectSetImage
 * @details Sets the image data object.
 * @param[in]  objImage The input PDF image object.
 * @param[in]  image The input image data object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDImageObjectSetImage, (FPD_PageObject objImage,  FPD_Image image))

NumOfSelector(FPDImageObject)
ENDENUM

//*****************************
/* ShadingObject HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDShadingObjectNew
 * @details Creates a new empty PDF shading object.
 * @param[in]  void 
 * @return   A new empty PDF shading object
 * @note 
 */
INTERFACE(FPD_PageObject, FPDShadingObjectNew, (void))

/**
 * @brief FPDShadingObjectDestroy
 * @details Destroys the PDF shading object.
 * @param[in]  objShading The input PDF shading object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDShadingObjectDestroy, (FPD_PageObject objShading))

/**
 * @brief FPDShadingObjectTransform
 * @details Transforms the path object.
 * @param[in]  objShading The input PDF shading object.
 * @param[in]  matrix The transformation matrix.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDShadingObjectTransform, (FPD_PageObject objShading,  FS_AffineMatrix matrix))

/**
 * @brief FPDShadingObjectCalcBoundingBox
 * @details Calculates the bounding box.
 * @param[in]  objShading The input PDF shading object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDShadingObjectCalcBoundingBox, (FPD_PageObject objShading))

/**
 * @brief FPDShadingObjectGetTransformMatrix
 * @details Gets the transformation matrix.
 * @param[in]  objShading The input PDF shading object.
 * @param[out]  outmatrix It receives the transformation matrix.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDShadingObjectGetTransformMatrix, (FPD_PageObject objShading,  FS_AffineMatrix* outmatrix))

/**
 * @brief FPDShadingObjectSetTransformMatrix
 * @details Sets the transformation matrix.
 * @param[in]  objShading The input PDF shading object.
 * @param[in]  matrix The transformation matrix.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDShadingObjectSetTransformMatrix, (FPD_PageObject objShading,  const FS_AffineMatrix* matrix))

/**
 * @brief FPDShadingObjectGetPage
 * @details Gets the page.
 * @param[in]  objShading The input PDF shading object.
 * @return   The page.
 * @note 
 */
INTERFACE(FPD_Page, FPDShadingObjectGetPage, (FPD_PageObject objShading))

/**
 * @brief FPDShadingObjectSetPage
 * @details Sets the page.
 * @param[in]  objShading The input PDF shading object.
 * @param[in]  page The input page.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDShadingObjectSetPage, (FPD_PageObject objShading,  FPD_Page page))

/**
 * @brief FPDShadingObjectGetShadingPattern
 * @details Gets the shading pattern.
 * @param[in]  objShading The input PDF shading object.
 * @return   void
 * @note 
 */
INTERFACE(FPD_ShadingPattern, FPDShadingObjectGetShadingPattern, (FPD_PageObject objShading))

/**
 * @brief FPDShadingObjectSetShadingPattern
 * @details Sets the shading pattern.
 * @param[in]  objShading The input PDF shading object.
 * @param[in]  pattern The input shading pattern.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDShadingObjectSetShadingPattern, (FPD_PageObject objShading,  FPD_ShadingPattern pattern))

NumOfSelector(FPDShadingObject)
ENDENUM

//*****************************
/* FormObject HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDFormObjectNew
 * @details Creates a new empty PDF form object.
 * @param[in]  void 
 * @return   A new empty PDF form object.
 * @note 
 */
INTERFACE(FPD_PageObject, FPDFormObjectNew, (void))

/**
 * @brief FPDFormObjectDestroy
 * @details Destroys the PDF form object.
 * @param[in]  objForm The input PDF form object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDFormObjectDestroy, (FPD_PageObject objForm))

/**
 * @brief FPDFormObjectTransform
 * @details Transforms the path object.
 * @param[in]  objForm The input PDF form object.
 * @param[in]  matrix The transformation matrix.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDFormObjectTransform, (FPD_PageObject objForm,  FS_AffineMatrix matrix))

/**
 * @brief FPDFormObjectCalcBoundingBox
 * @details Calculates the bounding box.
 * @param[in]  objForm The input PDF form object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDFormObjectCalcBoundingBox, (FPD_PageObject objForm))

/**
 * @brief FPDFormObjectGetTransformMatrix
 * @details Gets the transformation matrix.
 * @param[in]  objForm The input PDF form object.
 * @param[out]  outmatrix It receives the transformation matrix.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDFormObjectGetTransformMatrix, (FPD_PageObject objForm,  FS_AffineMatrix* outmatrix))

/**
 * @brief FPDFormObjectSetTransformMatrix
 * @details Sets the transformation matrix.
 * @param[in]  objForm The input PDF form object.
 * @param[in]  matrix The transformation matrix.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDFormObjectSetTransformMatrix, (FPD_PageObject objForm,  const FS_AffineMatrix* matrix))

/**
 * @brief FPDFormObjectGetForm
 * @details Gets the form.
 * @param[in]  objForm The input PDF form object.
 * @return   The form.
 * @note 
 */
INTERFACE(FPD_Form, FPDFormObjectGetForm, (FPD_PageObject objForm))

/**
 * @brief FPDFormObjectSetForm
 * @details Sets the form.
 * @param[in]  objForm The input PDF form object.
 * @param[in]  form The input form.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDFormObjectSetForm, (FPD_PageObject objForm,  FPD_Form form))

NumOfSelector(FPDFormObject)
ENDENUM

//*****************************
/* InlineImages HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDInlineImagesNew
 * @details Creates a new empty PDF inline images object.
 * @param[in]  void 
 * @return   A new empty PDF inline images object.
 * @note 
 */
INTERFACE(FPD_PageObject, FPDInlineImagesNew, (void))

/**
 * @brief FPDInlineImagesDestroy
 * @details Destroys the PDF inline images object.
 * @param[in]  objInlineImgs The input PDF inline images object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDInlineImagesDestroy, (FPD_PageObject objInlineImgs))

/**
 * @brief FPDInlineImagesAddMatrix
 * @details Adds a transform matrix.
 * @param[in]  objInlineImgs The input PDF inline images object.
 * @param[in]  matrix The input transform matrix.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDInlineImagesAddMatrix, (FPD_PageObject objInlineImgs,  const FS_AffineMatrix* matrix))

/**
 * @brief FPDInlineImagesCountMatrix
 * @details Counts the number of transform matrix in this object.
 * @param[in]  objInlineImgs The input PDF inline images object.
 * @return   The number of transform matrix in this object.
 * @note 
 */
INTERFACE(int, FPDInlineImagesCountMatrix, (FPD_PageObject objInlineImgs))

/**
 * @brief FPDInlineImagesGetMatrix
 * @details Gets a transform matrix from object by index.
 * @param[in]  objInlineImgs The input PDF inline images object.
 * @param[in]  index The index of transform matrix.
 * @return   The transform matrix from object by index.
 * @note 
 */
INTERFACE(FS_AffineMatrix, FPDInlineImagesGetMatrix, (FPD_PageObject objInlineImgs,  FS_INT32 index))

/**
 * @brief FPDInlineImagesGetStream
 * @details Gets the stream.
 * @param[in]  objInlineImgs The input PDF inline images object.
 * @return   The stream.
 * @note 
 */
INTERFACE(FPD_Object, FPDInlineImagesGetStream, (FPD_PageObject objInlineImgs))

/**
 * @brief FPDInlineImagesSetStream
 * @details Sets the stream
 * @param[in]  objInlineImgs The input PDF inline images object.
 * @param[in]  stream The input stream
 * @return   void
 * @note 
 */
INTERFACE(void, FPDInlineImagesSetStream, (FPD_PageObject objInlineImgs,  FPD_Object stream))

NumOfSelector(FPDInlineImages)
ENDENUM

//*****************************
/* ContentMarkItem HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDContentMarkItemNew
 * @details Creates a new empty PDF content mark item object.
 * @param[in]  void 
 * @return   A new empty PDF content mark item object.
 * @note 
 */
INTERFACE(FPD_ContentMarkItem, FPDContentMarkItemNew, (void))

/**
 * @brief FPDContentMarkItemDestroy
 * @details Destroys the PDF content mark item object.
 * @param[in]  item The input PDF content mark item object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDContentMarkItemDestroy, (FPD_ContentMarkItem item))

/**
 * @brief FPDContentMarkItemGetName
 * @details Gets the name(tag) of the content mark item.
 * @param[in]  item The input PDF content mark item object.
 * @return   The name(tag) of the content mark item.
 * @note 
 */
INTERFACE(char*, FPDContentMarkItemGetName, (FPD_ContentMarkItem item))

/**
 * @brief FPDContentMarkItemGetParamType
 * @details Gets the parameter type of the content mark item.
 * @param[in]  item The input PDF content mark item object.
 * @return   The parameter type of the content mark item.
 * @note 
 */
INTERFACE(FPD_MarkItemParamType, FPDContentMarkItemGetParamType, (FPD_ContentMarkItem item))

/**
 * @brief FPDContentMarkItemGetParam
 * @details Gets the parameter of the content mark item.
 * @param[in]  item The input PDF content mark item object.
 * @return   The parameter of the content mark item.
 * @note 
 */
INTERFACE(void*, FPDContentMarkItemGetParam, (FPD_ContentMarkItem item))

/**
 * @brief FPDContentMarkItemSetName
 * @details Sets the name(tag) of the content mark item.
 * @param[in]  item The input PDF content mark item object.
 * @param[in]  csName The input new name(tag).
 * @return   void
 * @note 
 */
INTERFACE(void, FPDContentMarkItemSetName, (FPD_ContentMarkItem item,  FS_LPCSTR csName))

/**
 * @brief FPDContentMarkItemSetParam
 * @details Sets the parameter of the content mark item.
 * @param[in]  item The input PDF content mark item object.
 * @param[in]  type The input new parameter type.
 * @param[in]  param The input new parameter.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDContentMarkItemSetParam, (FPD_ContentMarkItem item,  FPD_MarkItemParamType type,  void* param))

NumOfSelector(FPDContentMarkItem)
ENDENUM

//*****************************
/* ContentMark HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDContentMarkNew
 * @details Creates a new empty PDF content mark object.
 * @param[in]  void 
 * @return   A new empty PDF content mark object.
 * @note 
 */
INTERFACE(FPD_ContentMark, FPDContentMarkNew, (void))

/**
 * @brief FPDContentMarkDestroy
 * @details Destroys the PDF content mark object.
 * @param[in]  mark The input PDF content mark object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDContentMarkDestroy, (FPD_ContentMark mark))

/**
 * @brief FPDContentMarkGetMCID
 * @details Gets the marked-content identifier.
 * @param[in]  mark The input PDF content mark object.
 * @return   The marked-content identifier.
 * @note 
 */
INTERFACE(FS_INT32, FPDContentMarkGetMCID, (FPD_ContentMark mark))

/**
 * @brief FPDContentMarkHasMark
 * @details Checks whether the content mark has a specified content mark item.
 * @param[in]  mark The input PDF content mark object.
 * @param[in]  tag The name(tag) of the content mark item.
 * @return   Non-zero means it has, otherwise it has not.
 * @note 
 */
INTERFACE(FS_BOOL, FPDContentMarkHasMark, (FPD_ContentMark mark,  FS_LPCSTR tag))

/**
 * @brief FPDContentMarkLookupMark
 * @details Lookups a content mark item.
 * @param[in]  mark The input PDF content mark object.
 * @param[in]  tag The name(tag) of the content mark item.
 * @param[out]  outDict It receives the parameter(attributes) dictionary.
 * @return   Non-zero means found one, otherwise found none.
 * @note 
 */
INTERFACE(FS_BOOL, FPDContentMarkLookupMark, (FPD_ContentMark mark,  FS_LPCSTR tag,  FPD_Object* outDict))

/**
 * @brief FPDContentMarkCountItems
 * @details Counts the number of content mark data in this object.
 * @param[in]  mark The input PDF content mark object.
 * @return   The number of content mark data in this object.
 * @note 
 */
INTERFACE(FS_INT32, FPDContentMarkCountItems, (FPD_ContentMark mark))

/**
 * @brief FPDContentMarkGetItem
 * @details Gets the content mark item by index.
 * @param[in]  mark The input PDF content mark object.
 * @param[in]  index The zero-based content mark item index in the content mark data.
 * @return   A content mark item.
 * @note 
 */
INTERFACE(FPD_ContentMarkItem, FPDContentMarkGetItem, (FPD_ContentMark mark,  FS_INT32 index))

/**
 * @brief FPDContentMarkAddMark
 * @details Adds a content mark item.
 * @param[in]  mark The input PDF content mark object.
 * @param[in]  tag The input name(tag) of the content mark item.
 * @param[in]  dict The parameter(attributes) dictionary of the content mark item.
 * @param[in]  bDictNeedClone Whether the input dictionary must be copied or not.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDContentMarkAddMark, (FPD_ContentMark mark,  FS_LPCSTR tag,  FPD_Object dict,  FS_BOOL bDictNeedClone))

/**
 * @brief FPDContentMarkDeleteMark
 * @details Deletes a content mark item.
 * @param[in]  mark The input PDF content mark object.
 * @param[in]  tag The name(tag) of the content mark item.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDContentMarkDeleteMark, (FPD_ContentMark mark,  FS_LPCSTR tag))

/**
 * @brief FPDContentMarkDeleteLastMark
 * @details Deletes the last content mark item.
 * @param[in]  mark The input PDF content mark object.
 * @return   void
 * @note 
 */
INTERFACE(void, FPDContentMarkDeleteLastMark, (FPD_ContentMark mark))

/**
 * @brief FPDContentMarkIsNull
 * @details Tests whether the content mark object is <a>NULL</a> or not.
 * @param[in]  mark The input PDF content mark object.
 * @return   Non-zero means <a>NULL</a>, otherwise not <a>NULL</a>.
 * @note 
 */
INTERFACE(FS_BOOL, FPDContentMarkIsNull, (FPD_ContentMark mark))

/**
 * @brief FPDContentMarkCopy
 * @details Copies the source content mark to the specified one.
 * @param[in]  mark The input PDF content mark object.
 * @param[in]  pSrcContentMark The input PDF content mark object to be copied.
 * @return   void.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 8.3.1
 */
INTERFACE(void, FPDContentMarkCopy, (FPD_ContentMark mark,  FPD_ContentMark pSrcContentMark))

NumOfSelector(FPDContentMark)
ENDENUM

//----------_V2----------
//----------_V3----------
//----------_V4----------
//----------_V5----------
//----------_V6----------
//----------_V7----------
//----------_V8----------
//----------_V9----------
//----------_V10----------
//----------_V11----------
//----------_V12----------
//----------_V13----------
//*****************************
/* PathObjectUtils HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDPathObjectUtilsGetLineWidth
 * @details GetLineWidth.
 * @param[in]  pathObjectUtils The input pathObjectUtils
 * @param[in]  fLineWidth The input line width.
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDPathObjectUtilsGetLineWidth, (FPD_PathObjectUtils pathObjectUtils,  FS_FLOAT& fLineWidth))

/**
 * @brief FPDPathObjectUtilsSetLineWidth
 * @details SetLineWidth.
 * @param[in]  pathObjectUtils The input pathObjectUtils
 * @param[in]  fLineWidth The input line width.
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDPathObjectUtilsSetLineWidth, (FPD_PathObjectUtils pathObjectUtils,  FS_FLOAT fLineWidth))

/**
 * @brief FPDPathObjectUtilsSetMiterLimit
 * @details SetMiterLimit.
 * @param[in]  pathObjectUtils The input pathObjectUtils
 * @param[in]  fMiterLimit The input line width.
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDPathObjectUtilsSetMiterLimit, (FPD_PathObjectUtils pathObjectUtils,  FS_FLOAT fMiterLimit))

/**
 * @brief FPDPathObjectUtilsGetMiterLimit
 * @details GetMiterLimit.
 * @param[in]  pathObjectUtils The input pathObjectUtils
 * @param[in]  fMiterLimit The input line width.
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDPathObjectUtilsGetMiterLimit, (FPD_PathObjectUtils pathObjectUtils,  FS_FLOAT& fMiterLimit))

/**
 * @brief FPDPathObjectUtilsSetLineCap
 * @details SetLineCap.
 * @param[in]  pathObjectUtils The input pathObjectUtils
 * @param[in]  cap The input line width.
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDPathObjectUtilsSetLineCap, (FPD_PathObjectUtils pathObjectUtils,  FPD_LineCap cap))

/**
 * @brief FPDPathObjectUtilsGetLineCap
 * @details GetLineCap.
 * @param[in]  pathObjectUtils The input pathObjectUtils
 * @param[in]  nLinecap The input line nlinecap.
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDPathObjectUtilsGetLineCap, (FPD_PathObjectUtils pathObjectUtils,  FPD_LineCap& nLinecap))

/**
 * @brief FPDPathObjectUtilsGetLineJoin
 * @details GetLineJoin.
 * @param[in]  pathObjectUtils The input pathObjectUtils
 * @param[in]  nLineJoin The input line nLineJoin.
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDPathObjectUtilsGetLineJoin, (FPD_PathObjectUtils pathObjectUtils,  FPD_LineJoin& nLineJoin))

/**
 * @brief FPDPathObjectUtilsSetLineJoin
 * @details SetLineJoin.
 * @param[in]  pathObjectUtils The input pathObjectUtils
 * @param[in]  nLineJoin The input line join.
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDPathObjectUtilsSetLineJoin, (FPD_PathObjectUtils pathObjectUtils,  FPD_LineJoin join))

/**
 * @brief FPDPathObjectUtilsGetLineStyle
 * @details GetLineStyle.
 * @param[in]  pathObjectUtils The input pathObjectUtils
 * @param[out]  pDash The input aDash.
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDPathObjectUtilsGetLineStyle, (FPD_PathObjectUtils pathObjectUtils,  FS_FloatArray &pDash))

/**
 * @brief FPDPathObjectUtilsSetLineStyle
 * @details SetLineStyle.
 * @param[in]  pathObjectUtils The input pathObjectUtils
 * @param[in]  aDash The input aDash.
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDPathObjectUtilsSetLineStyle, (FPD_PathObjectUtils pathObjectUtils,  FS_FloatArray pDash))

/**
 * @brief FPDPathObjectUtilsGetStrokeInfo
 * @details GetStrokeInfo.
 * @param[in]  pathObjectUtils The input pathObjectUtils
 * @param[in]  pColor The input color.
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDPathObjectUtilsGetStrokeInfo, (FPD_PathObjectUtils pathObjectUtils,  FS_BOOL & bStroke,  FPD_Color& pColor))

/**
 * @brief FPDPathObjectUtilsSetStrokeInfo
 * @details SetStrokeInfo.
 * @param[in]  pathObjectUtils The input pathObjectUtils
 * @param[in]  bStroke The input bStroke.
 * @param[in]  pColor The input color.
 * @param[in]  pAddUndo The input AddUndo.
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDPathObjectUtilsSetStrokeInfo, (FPD_PathObjectUtils pathObjectUtils,  FS_BOOL bStroke,  const FPD_Color pColor,  FS_BOOL bAddUndo))

/**
 * @brief FPDPathObjectUtilsGetFillInfo
 * @details GetFillInfo.
 * @param[in]  pathObjectUtils The input pathObjectUtils
 * @param[in]  pColor The input color.
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDPathObjectUtilsGetFillInfo, (FPD_PathObjectUtils pathObjectUtils,  FS_BOOL& bFill,  FPD_Color& pColor))

/**
 * @brief FPDPathObjectUtilsSetFillInfo
 * @details SetFillInfo.
 * @param[in]  pathObjectUtils The input pathObjectUtils
 * @param[in]  pColor The input color.
 * @param[in]  pAddUndo The input AddUndo.
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDPathObjectUtilsSetFillInfo, (FPD_PathObjectUtils pathObjectUtils,  FS_BOOL bFill,  const FPD_Color pColor,  FS_BOOL bAddUndo))

/**
 * @brief FPDPathObjectUtilsSetColorSpace
 * @details SetColorSpace.
 * @param[in]  pathObjectUtils The input pathObjectUtils
 * @param[in]  pColor The input bFillOrStroke.
 * @param[in]  pColor The input nColorSpace.
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDPathObjectUtilsSetColorSpace, (FPD_PathObjectUtils pathObjectUtils,  FS_BOOL bFillOrStroke,  FS_INT32 nColorSpace))

NumOfSelector(FPDPathObjectUtils)
ENDENUM

//*****************************
/* ShadingObjectUtils HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FPDShadingObjectUtilsChangeShadingColor, (FPD_ShadingObjectUtils shadingObjectUtils,  FS_BOOL bUndo))

INTERFACE(void, FPDShadingObjectUtilsChangeColorUndoStart, (FPD_ShadingObjectUtils shadingObjectUtils))

INTERFACE(void, FPDShadingObjectUtilsChangeColorUndoEnd, (FPD_ShadingObjectUtils shadingObjectUtils))

INTERFACE(void, FPDShadingObjectUtilsChangeColorUndoCancle, (FPD_ShadingObjectUtils shadingObjectUtils))

INTERFACE(void, FPDShadingObjectUtilsEditReferenceLine, (FPD_ShadingObjectUtils shadingObjectUtils))

INTERFACE(void, FPDShadingObjectUtilsSetGridientCursorColor, (FPD_ShadingObjectUtils shadingObjectUtils,  FS_COLORREF clr))

INTERFACE(FS_COLORREF, FPDShadingObjectUtilsGetGridientCursorColor, (FPD_ShadingObjectUtils shadingObjectUtils))

INTERFACE(FS_COLORREF, FPDShadingObjectUtilsGetGridientColorAt, (FPD_ShadingObjectUtils shadingObjectUtils,  FS_INT32 nIndex))

INTERFACE(FS_FLOAT, FPDShadingObjectUtilsGetGridientCursorLocation, (FPD_ShadingObjectUtils shadingObjectUtils))

INTERFACE(FS_INT32, FPDShadingObjectUtilsGetGridientCursorIndex, (FPD_ShadingObjectUtils shadingObjectUtils))

INTERFACE(void, FPDShadingObjectUtilsSetGradientColorShowRect, (FPD_ShadingObjectUtils shadingObjectUtils,  FS_Rect rect))

INTERFACE(void, FPDShadingObjectUtilsAddGradientCursorInfoBefore, (FPD_ShadingObjectUtils shadingObjectUtils,  FS_DevicePoint point))

INTERFACE(FS_BOOL, FPDShadingObjectUtilsAddGradientCursorInfoExcute, (FPD_ShadingObjectUtils shadingObjectUtils,  FS_DevicePoint point))

INTERFACE(FS_BOOL, FPDShadingObjectUtilsAddGradientCursorInfoEnd, (FPD_ShadingObjectUtils shadingObjectUtils))

INTERFACE(FS_BOOL, FPDShadingObjectUtilsAddGradientCursorInfoDblClk, (FPD_ShadingObjectUtils shadingObjectUtils,  FS_DevicePoint point))

INTERFACE(FS_BOOL, FPDShadingObjectUtilsDelGradientCursorInfo, (FPD_ShadingObjectUtils shadingObjectUtils))

INTERFACE(void, FPDShadingObjectUtilsDefaultGradientDataInit, (FPD_ShadingObjectUtils shadingObjectUtils))

INTERFACE(void, FPDShadingObjectUtilsSetGradientCurrentInfo, (FPD_ShadingObjectUtils shadingObjectUtils,  const FS_PtrArray &colorArray))

INTERFACE(void, FPDShadingObjectUtilsPaintGridient, (FPD_ShadingObjectUtils shadingObjectUtils,  FPD_RenderDevice& device))

NumOfSelector(FPDShadingObjectUtils)
ENDENUM

//*****************************
/* PathEditor HFT functions */
//*****************************

BEGINENUM
INTERFACE(FS_BOOL, FPDPathEditorHasStartEdit, (FPD_PathEditor edit))

INTERFACE(FS_BOOL, FPDPathEditorIsClipPathMode, (FPD_PathEditor edit))

INTERFACE(void, FPDPathEditorAppendSubPath, (FPD_PathEditor edit,  FPD_GeoDrawType type))

NumOfSelector(FPDPathEditor)
ENDENUM

//*****************************
/* ShadingEditor HFT functions */
//*****************************

BEGINENUM
INTERFACE(FS_BOOL, FPDShadingEditorIsReferenceLineWorking, (FPD_ShadingEditor edit))

NumOfSelector(FPDShadingEditor)
ENDENUM

//*****************************
/* TextEditor HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDTextEditorOnFontNameChanged
 * @details OnFontNameChanged.
 * @param[in]  edit The input FPD_TextEditor
 * @param[in]  wsFontName The input wsFontName
 * @param[in]  wsScriptName The input wsScriptName
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextEditorOnFontNameChanged, (FPD_TextEditor edit,  FS_WideString  wsFontName,  FS_BOOL bBold,  FS_BOOL& bChangeItalic))

/**
 * @brief FPDTextEditorOnFontSizeChanged
 * @details OnFontSizeChanged.
 * @param[in]  edit The input FPD_TextEditor
 * @param[in]  flFontSize The input fontSize
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextEditorOnFontSizeChanged, (FPD_TextEditor edit,  FS_FLOAT flFontSize))

/**
 * @brief FPDTextEditorOnTextColorChanged
 * @details OnTextColorChanged.
 * @param[in]  edit The input FPD_TextEditor
 * @param[in]  textColor The input textColor
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextEditorOnTextColorChanged, (FPD_TextEditor edit,  FS_COLORREF textColor))

/**
 * @brief FPDTextEditorOnBoldChanged
 * @details OnBoldChanged.
 * @param[in]  edit The input FPD_TextEditor
 * @param[in]  bBold The input bBold
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextEditorOnBoldChanged, (FPD_TextEditor edit,  FS_BOOL bBold))

/**
 * @brief FPDTextEditorOnItalicChanged
 * @details OnItalicChanged.
 * @param[in]  edit The input FPD_TextEditor
 * @param[in]  bItalic The input bItalic
 * @param[in]  bEnabled The input bEnabled
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextEditorOnItalicChanged, (FPD_TextEditor edit,  FS_BOOL bItalic,  FS_BOOL& bEnabled))

/**
 * @brief FPDTextEditorOnAlignChanged
 * @details OnAlignChanged.
 * @param[in]  edit The input FPD_TextEditor
 * @param[in]  dwAlign The input dwAlign
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextEditorOnAlignChanged, (FPD_TextEditor edit,  FS_INT32 dwAlign))

/**
 * @brief FPDTextEditorOnCharSpaceChanged
 * @details OnCharSpaceChanged.
 * @param[in]  edit The input FPD_TextEditor
 * @param[in]  flSpace The input flSpace
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextEditorOnCharSpaceChanged, (FPD_TextEditor edit,  FS_FLOAT flSpace))

/**
 * @brief FPDTextEditorOnWordSpaceChanged
 * @details OnWordSpaceChanged.
 * @param[in]  edit The input FPD_TextEditor
 * @param[in]  fWordSpace The input fWordSpace
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextEditorOnWordSpaceChanged, (FPD_TextEditor edit,  FS_FLOAT fWordSpace))

/**
 * @brief FPDTextEditorOnCharHorzScaleChanged
 * @details OnCharHorzScaleChanged.
 * @param[in]  edit The input FPD_TextEditor
 * @param[in]  nScale The input nScale
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextEditorOnCharHorzScaleChanged, (FPD_TextEditor edit,  FS_INT32 nScale))

/**
 * @brief FPDTextEditorOnLineLeadingChanged
 * @details OnLineLeadingChanged.
 * @param[in]  edit The input FPD_TextEditor
 * @param[in]  flLineLeading The input flLineLeading
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextEditorOnLineLeadingChanged, (FPD_TextEditor edit,  FS_FLOAT flLineLeading))

/**
 * @brief FPDTextEditorOnUnderlineChanged
 * @details OnUnderlineChanged.
 * @param[in]  edit The input FPD_TextEditor
 * @param[in]  bUnderline The input bUnderline
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextEditorOnUnderlineChanged, (FPD_TextEditor edit,  FS_BOOL bUnderline))

/**
 * @brief FPDTextEditorOnCrossChanged
 * @details OnCrossChanged.
 * @param[in]  edit The input FPD_TextEditor
 * @param[in]  bCross The input bCross
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextEditorOnCrossChanged, (FPD_TextEditor edit,  FS_BOOL bCross))

/**
 * @brief FPDTextEditorOnSuperScriptChanged
 * @details OnSuperScriptChanged.
 * @param[in]  edit The input FPD_TextEditor
 * @param[in]  bSuperSet The input bSuperSet
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextEditorOnSuperScriptChanged, (FPD_TextEditor edit,  FS_BOOL bSuperSet))

/**
 * @brief FPDTextEditorOnSubScriptChanged
 * @details OnSubScriptChanged.
 * @param[in]  edit The input FPD_TextEditor
 * @param[in]  bSubSet The input bSubSet
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextEditorOnSubScriptChanged, (FPD_TextEditor edit,  FS_BOOL bSubSet))

/**
 * @brief FPDTextEditorOnWritingDirctionChanged
 * @details OnWritingDirctionChanged.
 * @param[in]  edit The input FPD_TextEditor
 * @param[in]  eDir The input eDir
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextEditorOnWritingDirctionChanged, (FPD_TextEditor edit,  FPD_WritingDirection eDir))

INTERFACE(FS_BOOL, FPDTextEditorIsSelect, (FPD_TextEditor edit))

INTERFACE(void, FPDTextEditorCopy, (FPD_TextEditor edit))

INTERFACE(void, FPDTextEditorPaste, (FPD_TextEditor edit))

INTERFACE(void, FPDTextEditorCut, (FPD_TextEditor edit))

INTERFACE(void, FPDTextEditorDelete, (FPD_TextEditor edit))

INTERFACE(void, FPDTextEditorSelectAll, (FPD_TextEditor edit))

INTERFACE(FS_BOOL, FPDTextEditorIsAdd, (FPD_TextEditor edit))

INTERFACE(void, FPDTextEditorGetICaretFormData, (FPD_TextEditor edit,  FS_FloatPoint& head,  FS_FloatPoint& foot))

INTERFACE(void, FPDTextEditorGetTextObjectMatrix, (FPD_TextEditor edit,  FS_AffineMatrix & mt))

NumOfSelector(FPDTextEditor)
ENDENUM

//*****************************
/* TextObjectUtils HFT functions */
//*****************************

BEGINENUM
/**
 * @brief FPDTextObjectUtilsGetFontSize
 * @details GetFontSize.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  fFontSize The input fFontSize
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsGetFontSize, (FPD_TextObjectUtils objectUtils,  FS_FLOAT& fFontSize))

/**
 * @brief FPDTextObjectUtilsSetFontSize
 * @details SetFontSize.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  fFontSize The input fFontSize
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsSetFontSize, (FPD_TextObjectUtils objectUtils,  FS_FLOAT fFontSize))

/**
 * @brief FPDTextObjectUtilsGetHorizontalScale
 * @details GetHorizontalScale.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  nHorzScale The input nHorzScale
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsGetHorizontalScale, (FPD_TextObjectUtils objectUtils,  FS_INT32& nHorzScale))

/**
 * @brief FPDTextObjectUtilsSetHorizontalScale
 * @details SetHorizontalScale.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  nHorzScale The input nHorzScale
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsSetHorizontalScale, (FPD_TextObjectUtils objectUtils,  FS_INT32 nHorzScale))

/**
 * @brief FPDTextObjectUtilsSetCharSpace
 * @details SetCharSpace.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  fCharSpace The input fCharSpace
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsSetCharSpace, (FPD_TextObjectUtils objectUtils,  FS_FLOAT fCharSpace))

/**
 * @brief FPDTextObjectUtilsGetCharSpace
 * @details GetCharSpace.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  fCharSpace The input fCharSpace
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsGetCharSpace, (FPD_TextObjectUtils objectUtils,  FS_FLOAT& fCharSpace))

/**
 * @brief FPDTextObjectUtilsGetWordSpace
 * @details GetWordSpace.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  fWordSpace The input fWordSpace
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsGetWordSpace, (FPD_TextObjectUtils objectUtils,  FS_FLOAT& fWordSpace))

/**
 * @brief FPDTextObjectUtilsSetWordSpace
 * @details SetWordSpace.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  fWordSpace The input fWordSpace
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsSetWordSpace, (FPD_TextObjectUtils objectUtils,  FS_FLOAT fWordSpace))

/**
 * @brief FPDTextObjectUtilsSetFont
 * @details SetFont.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  wsFontName The input wsFontName
 * @param[in]  bBold The input bBold
 * @param[in]  bItalic The input bItalic
 * @param[in]  nOPType The input nOPType
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsSetFont, (FPD_TextObjectUtils objectUtils,  FS_WideString wsFontName,  FS_BOOL bBold,  FS_BOOL bItalic,  FPD_FONTOPERATION nOPType))

/**
 * @brief FPDTextObjectUtilsGetFont
 * @details GetFont.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  wsFontName The input wsFontName
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsGetFont, (FPD_TextObjectUtils objectUtils,  FS_WideString wsFontName,  FS_BOOL bFullName))

/**
 * @brief FPDTextObjectUtilsGetTextMode
 * @details GetTextMode.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  mode The input mode
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsGetTextMode, (FPD_TextObjectUtils objectUtils,  FPD_TextMode& mode))

/**
 * @brief FPDTextObjectUtilsSetTextMode
 * @details SetTextMode.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  mode The input mode
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsSetTextMode, (FPD_TextObjectUtils objectUtils,  FPD_TextMode mode))

/**
 * @brief FPDTextObjectUtilsCanRemoveTextKerning
 * @details CanRemoveTextKerning.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsCanRemoveTextKerning, (FPD_TextObjectUtils objectUtils))

/**
 * @brief FPDTextObjectUtilsRemoveTextKerning
 * @details RemoveTextKerning.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextObjectUtilsRemoveTextKerning, (FPD_TextObjectUtils objectUtils))

/**
 * @brief FPDTextObjectUtilsCanMergeText
 * @details CanMergeText.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsCanMergeText, (FPD_TextObjectUtils objectUtils))

/**
 * @brief FPDTextObjectUtilsMergeText
 * @details MergeText.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsMergeText, (FPD_TextObjectUtils objectUtils))

/**
 * @brief FPDTextObjectUtilsCanSplitText
 * @details CanSplitText.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsCanSplitText, (FPD_TextObjectUtils objectUtils))

/**
 * @brief FPDTextObjectUtilsSplitText
 * @details SplitText.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  aSplitSubArray The input aSplitSubArray
 * @param[in]  nCount The input nCount
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextObjectUtilsSplitText, (FPD_TextObjectUtils objectUtils,  FS_INT64* aSplitSubArray,  FS_INT32 nCount))

/**
 * @brief FPDTextObjectUtilsCanText2Path
 * @details CanText2Path.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsCanText2Path, (FPD_TextObjectUtils objectUtils))

/**
 * @brief FPDTextObjectUtilsGetStrokeInfo
 * @details GetStrokeInfo.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  bStroke The input bStroke
 * @param[in]  color The input color
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsGetStrokeInfo, (FPD_TextObjectUtils objectUtils,  FS_BOOL& bStroke,  FPD_Color& pColor))

/**
 * @brief FPDTextObjectUtilsSetStrokeInfo
 * @details SetStrokeInfo.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  bStroke The input bStroke
 * @param[in]  pColor The input pColor
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsSetStrokeInfo, (FPD_TextObjectUtils objectUtils,  FS_BOOL bStroke,  const FPD_Color pColor,  FS_BOOL bAddUndo))

/**
 * @brief FPDTextObjectUtilsGetFillInfo
 * @details GetFillInfo.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  bFill The input bFill
 * @param[in]  color The input color
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsGetFillInfo, (FPD_TextObjectUtils objectUtils,  FS_BOOL& bFill,  FPD_Color& pColor))

/**
 * @brief FPDTextObjectUtilsSetFillInfo
 * @details SetFillInfo.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  bFill The input bFill
 * @param[in]  color The input color
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsSetFillInfo, (FPD_TextObjectUtils objectUtils,  FS_BOOL bFill,  const FPD_Color pColor,  FS_BOOL bAddUndo))

/**
 * @brief FPDTextObjectUtilsSetColorSpace
 * @details SetColorSpace.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
 * @param[in]  bFillOrStroke The input bFillOrStroke.
 * @param[in]  nColorSpace The input nColorSpace.
 * @return   <a>TRUE/FALSE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextObjectUtilsSetColorSpace, (FPD_TextObjectUtils objectUtils,  FS_BOOL bFillOrStroke,  FS_INT32 nColorSpace))

/**
 * @brief FPDTextObjectUtilsGetTextColorInfo
 * @details GetTextColorInfo.
 * @param[in]  objectUtils The input FPD_TextObjectUtils
	* param[out]  bOutHasColor             It receives the exist flag of color.
	* param[out]  pOutColor                It receives color struct.
 * @return   <a>NULL</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(void, FPDTextObjectUtilsGetTextColorInfo, (FPD_TextObjectUtils objectUtils,  FS_BOOL& bOutHasColor,  FPD_Color& pOutColor))

/**
 * @brief FPDTextObjectUtilsIsWordSpaceValid
 * @details IsWordSpaceValid.
 * @return   <a>FPD_ValidFlag</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FPD_ValidFlag, FPDTextObjectUtilsIsWordSpaceValid, (FPD_TextObjectUtils objectUtils))

/**
 * @brief FPDTextObjectUtilsGetFontBoldInfo
 * @details GetFontBoldInfo.
 * @return   <a>TRUE/FLASE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsGetFontBoldInfo, (FPD_TextObjectUtils objectUtils,  FS_BOOL& bIsEnable))

/**
 * @brief FPDTextObjectUtilsGetFontItalic
 * @details GetFontItalic.
 * @return   <a>TRUE/FLASE</a>.
 * @note 
 * @since   <a>SDK_LATEEST_VERSION</a> > 10.0.0
 */
INTERFACE(FS_BOOL, FPDTextObjectUtilsGetFontItalic, (FPD_TextObjectUtils objectUtils,  FS_BOOL& bIsEnable))

NumOfSelector(FPDTextObjectUtils)
ENDENUM

//*****************************
/* GraphicObject HFT functions */
//*****************************

BEGINENUM
INTERFACE(FPD_GraphicObject, FPDGraphicObjectCreate, (FPD_PageObject pPageObject,  FS_WORD nIndex,  FS_POSITION pos,  FPD_Page page,  FS_BOOL bIsInForm,  FS_PtrArray aFormObj,  const FS_AffineMatrix MtForm,  const FS_DWordArray aFormIdx))

INTERFACE(void, FPDGraphicObjectDestroy, (FPD_GraphicObject obj))

INTERFACE(FPD_Page, FPDGraphicObjectGetPage, (FPD_GraphicObject graphicObj))

INTERFACE(FPD_PageObject, FPDGraphicObjectGetGraphicObject, (FPD_GraphicObject graphicObj))

INTERFACE(FS_DWORD, FPDGraphicObjectGetIndex, (FPD_GraphicObject graphicObj))

INTERFACE(FS_POSITION, FPDGraphicObjectGetPosition, (FPD_GraphicObject graphicObj))

INTERFACE(FS_BOOL, FPDGraphicObjectIsInForm, (FPD_GraphicObject graphicObj))

INTERFACE(void, FPDGraphicObjectGetFormIndex, (FPD_GraphicObject graphicObj,  FS_DWordArray& pdwArr))

INTERFACE(void, FPDGraphicObjectGetFormObjects, (FPD_GraphicObject graphicObj,  FS_PtrArray& pdwArr))

INTERFACE(FS_AffineMatrix, FPDGraphicObjectGetFormMatrix, (FPD_GraphicObject graphicObj))

NumOfSelector(FPDGraphicObject)
ENDENUM

//*****************************
/* GraphicEditor HFT functions */
//*****************************

BEGINENUM
INTERFACE(FPD_ObjectFilter, FPDGraphicEditorGetType, (FPD_GraphicEditor pEditor))

INTERFACE(FS_BOOL, FPDGraphicEditorEndEdit, (FPD_GraphicEditor pEditor,  FS_INT32 mode))

NumOfSelector(FPDGraphicEditor)
ENDENUM

//*****************************
/* GraphicObjectUtils HFT functions */
//*****************************

BEGINENUM
INTERFACE(FS_BOOL, FPDGraphicObjectUtilsGetHeight, (FPD_GraphicObjectUtils objUtils,  FS_FLOAT& fHeight))

INTERFACE(FS_BOOL, FPDGraphicObjectUtilsSetHeight, (FPD_GraphicObjectUtils objUtils,  FS_FLOAT fHeight))

INTERFACE(FS_BOOL, FPDGraphicObjectUtilsSetWidth, (FPD_GraphicObjectUtils objUtils,  FS_FLOAT fWidth))

INTERFACE(FS_BOOL, FPDGraphicObjectUtilsGetWidth, (FPD_GraphicObjectUtils objUtils,  FS_FLOAT& fWidth))

INTERFACE(FS_BOOL, FPDGraphicObjectUtilsGetXPosition, (FPD_GraphicObjectUtils objUtils,  FS_FLOAT& fPosX))

INTERFACE(FS_BOOL, FPDGraphicObjectUtilsSetXPosition, (FPD_GraphicObjectUtils objUtils,  FS_FLOAT fPosX))

INTERFACE(FS_BOOL, FPDGraphicObjectUtilsGetYPosition, (FPD_GraphicObjectUtils objUtils,  FS_FLOAT& fPosY))

INTERFACE(FS_BOOL, FPDGraphicObjectUtilsSetYPosition, (FPD_GraphicObjectUtils objUtils,  FS_FLOAT fPosY))

INTERFACE(FS_BOOL, FPDGraphicObjectUtilsGetOpacity, (FPD_GraphicObjectUtils objUtils,  FS_INT32& nOpacity))

INTERFACE(FS_BOOL, FPDGraphicObjectUtilsSetOpacity, (FPD_GraphicObjectUtils objUtils,  FS_INT32 nOpacity,  FS_BOOL bUndo))

INTERFACE(void, FPDGraphicObjectUtilsRotate, (FPD_GraphicObjectUtils objUtils,  FS_FLOAT fRotate))

INTERFACE(void, FPDGraphicObjectUtilsFlip, (FPD_GraphicObjectUtils objUtils,  FS_BOOL bVertical))

INTERFACE(void, FPDGraphicObjectUtilsShear, (FPD_GraphicObjectUtils objUtils,  FS_FLOAT fAngle))

INTERFACE(void, FPDGraphicObjectUtilsMove, (FPD_GraphicObjectUtils objUtils,  FPD_EditorPage destPage,  const FS_DevicePoint& ptOffset))

INTERFACE(void, FPDGraphicObjectUtilsAlign, (FPD_GraphicObjectUtils objUtils,  FPD_AlignMode mode))

INTERFACE(void, FPDGraphicObjectUtilsCenter, (FPD_GraphicObjectUtils objUtils,  FPD_CenterMode mode))

INTERFACE(void, FPDGraphicObjectUtilsDistribute, (FPD_GraphicObjectUtils objUtils,  FS_BOOL bVertical))

INTERFACE(void, FPDGraphicObjectUtilsSize, (FPD_GraphicObjectUtils objUtils,  FPD_SizeMode mode))

INTERFACE(void, FPDGraphicObjectUtilsScale, (FPD_GraphicObjectUtils objUtils,  FS_Rect rcNew))

INTERFACE(void, FPDGraphicObjectUtilsScaleFloat, (FPD_GraphicObjectUtils objUtils,  FS_FLOAT fDelta,  FS_BOOL bHorizontal))

INTERFACE(void, FPDGraphicObjectUtilsChangeRenderingOrder, (FPD_GraphicObjectUtils objUtils,  FPD_RendingOrder operation))

INTERFACE(FS_BOOL, FPDGraphicObjectUtilsCanChangeRenderingOrder, (FPD_GraphicObjectUtils objUtils,  FPD_RendingOrder operation))

INTERFACE(void, FPDGraphicObjectUtilsClearClipPath, (FPD_GraphicObjectUtils objUtils))

INTERFACE(FS_BOOL, FPDGraphicObjectUtilsEditClipPath, (FPD_GraphicObjectUtils objUtil))

INTERFACE(FS_BOOL, FPDGraphicObjectUtilsAddClipPath, (FPD_GraphicObjectUtils objUtil,  FPD_GeoDrawType type))

INTERFACE(FPD_GraphicObjectUtilsType, FPDGraphicObjectUtilsGetType, (FPD_GraphicObjectUtils objUtils))

INTERFACE(void, FPDGraphicObjectUtilsCut, (FPD_GraphicObjectUtils objUtils))

INTERFACE(FS_BOOL, FPDGraphicObjectUtilsCopy, (FPD_GraphicObjectUtils objUtils))

INTERFACE(void, FPDGraphicObjectUtilsDelete, (FPD_GraphicObjectUtils objUtils))

INTERFACE(FS_BOOL, FPDGraphicObjectUtilsSavePageArchive, (FPD_IPageEditor pPageEditor,  FS_LPCBYTE &pBuffer,  FS_DWORD &size))

INTERFACE(FS_BOOL, FPDGraphicObjectUtilsReadPageArchive, (FPD_IPageEditor pPageEditor,  const FS_LPCBYTE &pBuffer,  const FS_DWORD &size,  FS_DevicePoint &ptPaste,  FS_PtrArray& vecObject))

NumOfSelector(FPDGraphicObjectUtils)
ENDENUM

//*****************************
/* ImageEditor HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FPDImageEditorSaveAsImage, (FPD_ImageEditor imageEditor))

INTERFACE(void, FPDImageEditorPreviewFilter, (FPD_ImageEditor imageEditor,  FPD_Filter eFilter))

INTERFACE(void, FPDImageEditorCancelPreviewFilter, (FPD_ImageEditor imageEditor,  FS_BOOL bUpdateView))

INTERFACE(void, FPDImageEditorAdjustFilter, (FPD_ImageEditor imageEditor,  FPD_Filter eFilter))

INTERFACE(void, FPDImageEditorRotateVertically, (FPD_ImageEditor imageEditor,  FPD_EditorPage editorPage))

INTERFACE(void, FPDImageEditorRotateHorizontally, (FPD_ImageEditor imageEditor,  FPD_EditorPage editorPage))

INTERFACE(FS_BOOL, FPDImageEditorReplace, (FPD_ImageEditor imageEditor,  FPD_EditorPage editorPage))

INTERFACE(void, FPDImageEditorCopy, (FPD_ImageEditor imageEditor,  FPD_EditorPage editorPage))

INTERFACE(void, FPDImageEditorPaste, (FPD_ImageEditor imageEditor,  FPD_EditorPage editorPage))

INTERFACE(void, FPDImageEditorDeleteRegion, (FPD_ImageEditor imageEditor,  FPD_EditorPage editorPage,  FS_BOOL bCopy,  FS_BOOL bBackgroundColor,  FS_BOOL bIgnoreSelection))

INTERFACE(void, FPDImageEditorZoomIn, (FPD_ImageEditor imageEditor))

INTERFACE(void, FPDImageEditorZoomTo, (FPD_ImageEditor imageEditor,  FS_FLOAT fScale,  FS_BOOL bUpdate))

INTERFACE(void, FPDImageEditorZoomOut, (FPD_ImageEditor imageEditor))

INTERFACE(void, FPDImageEditorAdjustColor, (FPD_ImageEditor imageEditor,  FPD_EditorPage editorPage,  FPD_AdjustRgb nColorRGB,  FS_INT32 nPercentage))

INTERFACE(FS_BOOL, FPDImageEditorLoadFromClipboard, (FPD_ImageEditor imageEditor,  FS_LPSTR pDIB))

INTERFACE(void, FPDImageEditorOnClientRectChanged, (FPD_ImageEditor imageEditor,  const FS_Rect rect))

INTERFACE(void, FPDImageEditorSetActionType, (FPD_ImageEditor imageEditor,  FPD_ImageEditorToolActionType nType))

INTERFACE(FPD_ImageEditorToolActionType, FPDImageEditorGetActionType, (FPD_ImageEditor imageEditor))

INTERFACE(FS_BOOL, FPDImageEditorOnSetCursor, (FPD_ImageEditor imageEditor))

INTERFACE(void, FPDImageEditorOnSize, (FPD_ImageEditor imageEditor))

INTERFACE(FPD_IOptionData, FPDImageEditorGetCurrentOptionData, (FPD_ImageEditor imageEditor))

INTERFACE(FPD_IPublicOptionData, FPDImageEditorGetPublicOptionData, (FPD_ImageEditor imageEditor))

INTERFACE(FPD_ImageHistogramData, FPDImageEditorGetImageHistogramData, (FPD_ImageEditor imageEditor))

INTERFACE(void, FPDImageEditorCompositeImageObject, (FPD_ImageEditor imageEditor,  FS_INT32 nOpacity,  FS_INT32 nFill,  FS_INT32 nBlendMode))

INTERFACE(void, FPDImageEditorCancelEdit, (FPD_ImageEditor imageEditor,  FPD_EditorPage editorPage))

NumOfSelector(FPDImageEditor)
ENDENUM

//*****************************
/* IPageEditor HFT functions */
//*****************************

BEGINENUM
INTERFACE(FS_BOOL, FPDIPageEditorStartEdit, (FPD_IPageEditor pageEditor))

INTERFACE(void, FPDIPageEditorRestartEdit, (FPD_IPageEditor pageEditor))

INTERFACE(FS_BOOL, FPDIPageEditorEndEdit, (FPD_IPageEditor pageEditor))

INTERFACE(FS_BOOL, FPDIPageEditorHasSelection, (FPD_IPageEditor pageEditor))

INTERFACE(FS_INT32, FPDIPageEditorCountSelection, (FPD_IPageEditor pageEditor))

INTERFACE(void, FPDIPageEditorGetSelection, (FPD_IPageEditor pageEditor,  FS_PtrArray arr))

INTERFACE(FPD_ExistType, FPDIPageEditorHaveFillColorInSelection, (FPD_IPageEditor pageEditor))

INTERFACE(FPD_EditorPage, FPDIPageEditorGetActivePage, (FPD_IPageEditor pageEditor))

INTERFACE(void, FPDIPageEditorSetObjectFilter, (FPD_IPageEditor pageEditor,  FPD_ObjectFilter type))

INTERFACE(FS_BOOL, FPDIPageEditorOnVKUP, (FPD_IPageEditor pageEditor,  FS_INT32 nKeyCode,  FS_BOOL bCtrl,  FS_BOOL bShift))

INTERFACE(FS_BOOL, FPDIPageEditorOnVKDOWN, (FPD_IPageEditor pageEditor,  FS_INT32 nKeyCode,  FS_BOOL bCtrl,  FS_BOOL bShift))

INTERFACE(FS_BOOL, FPDIPageEditorOnLeftButtonDown, (FPD_IPageEditor pageEditor,  FPD_EditorPage editorPage,  FS_DevicePoint pt,  FS_BOOL bCtrl,  FS_BOOL bShift))

INTERFACE(FS_BOOL, FPDIPageEditorOnLeftButtonUp, (FPD_IPageEditor pageEditor,  FPD_EditorPage editorPage,  FS_DevicePoint pt,  FS_BOOL bCtrl,  FS_BOOL bShift))

INTERFACE(FS_BOOL, FPDIPageEditorOnLeftButtonDblClk, (FPD_IPageEditor pageEditor,  FPD_EditorPage editorPage,  FS_DevicePoint pt,  FS_BOOL bCtrl))

INTERFACE(FS_BOOL, FPDIPageEditorOnMouseMove, (FPD_IPageEditor pageEditor,  FPD_EditorPage editorPage,  FS_DevicePoint pt,  FS_BOOL bShift,  FS_BOOL bCtrl))

INTERFACE(FS_BOOL, FPDIPageEditorOnMouseWheel, (FPD_IPageEditor pageEditor,  FPD_EditorPage editorPage,  FS_DevicePoint pt,  FS_SHORT zDelta))

INTERFACE(FS_BOOL, FPDIPageEditorOnRightButtonDown, (FPD_IPageEditor pageEditor,  FPD_EditorPage editorPage,  FS_DevicePoint pt,  FS_BOOL bCtrl,  FS_BOOL bShift))

INTERFACE(FS_BOOL, FPDIPageEditorOnRightButtonUp, (FPD_IPageEditor pageEditor,  FPD_EditorPage editorPage,  FS_DevicePoint pt,  FS_BOOL bCtrl,  FS_BOOL bShift))

INTERFACE(void, FPDIPageEditorOnReleaseCapture, (FPD_IPageEditor pageEditor,  FS_DevicePoint pt,  FS_BOOL bCtrl,  FS_BOOL bShift))

INTERFACE(FS_BOOL, FPDIPageEditorOnPaint, (FPD_IPageEditor pageEditor,  FPD_RenderDevice& device))

INTERFACE(void*, FPDIPageEditorGetObjectAtPoint, (FPD_IPageEditor pageEditor,  FPD_EditorPage editorPage,  FS_DevicePoint pt))

INTERFACE(FS_BOOL, FPDIPageEditorCanPaste, (FPD_IPageEditor pageEditor))

INTERFACE(FS_BOOL, FPDIPageEditorPaste, (FPD_IPageEditor pageEditor))

INTERFACE(void, FPDIPageEditorSelectAll, (FPD_IPageEditor pageEditor))

INTERFACE(FPD_GraphicEditor, FPDIPageEditorGetGraphicEditor, (FPD_IPageEditor pageEditor))

INTERFACE(FPD_GraphicObjectUtils, FPDIPageEditorGetGraphicObjectUtils, (FPD_IPageEditor pageEditor))

INTERFACE(FS_BOOL, FPDIPageEditorCreatePath, (FPD_IPageEditor PageEditor,  FPD_GeoDrawType type))

INTERFACE(FS_BOOL, FPDIPageEditorCreateShading, (FPD_IPageEditor PageEditor,  FPD_GeoDrawType type))

INTERFACE(FS_BOOL, FPDIPageEditorCreateImage, (FPD_IPageEditor PageEditor,  const FS_LPCWSTR sPathFile,  FS_FileReadHandler pRetFileRead))

INTERFACE(FS_BOOL, FPDIPageEditorCreateText, (FPD_IPageEditor PageEditor,  FPD_PageObject pTextObjectTemplate))

INTERFACE(FPD_IPageEditor, FPDIPageEditorCreatePageEditor, (FPD_EditorContextCallbacks pContext))

INTERFACE(void, FPDIPageEditorDestroyPageEditor, (FPD_IPageEditor pPageEditor))

INTERFACE(void, FPDIPageEditorClearSelection, (FPD_IPageEditor PageEditor,  FS_BOOL bRefresh))

INTERFACE(FS_BOOL, FPDIPageEditorText2Path, (FPD_IPageEditor PageEditor))

INTERFACE(void, FPDIPageEditorOnChar, (FPD_IPageEditor PageEditor,  FS_WCHAR Key))

INTERFACE(FS_BOOL, FPDIPageEditorEditGraphicObject, (FPD_IPageEditor PageEditor,  FPD_EditorPage eidtorPage,  FS_PtrArray graphicObj,  FPD_Operation type))

INTERFACE(FS_BOOL, FPDIPageEditorCreateImage2, (FPD_IPageEditor PageEditor,  FS_INT32 nWidth,  FS_INT32 nHeight,  FS_COLORREF clr,  FS_INT32 nImageDPIX,  FS_INT32 nImageDPIY,  FS_BOOL bCompress))

NumOfSelector(FPDIPageEditor)
ENDENUM

//*****************************
/* IUndoItem HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FPDIUndoItemOnUndo, (FPD_IUndoItem item))

INTERFACE(void, FPDIUndoItemOnRedo, (FPD_IUndoItem item))

INTERFACE(void, FPDIUndoItemOnRelease, (FPD_IUndoItem item))

NumOfSelector(FPDIUndoItem)
ENDENUM

//*****************************
/* IClipboard HFT functions */
//*****************************

BEGINENUM
INTERFACE(FPD_IClipboardHandler, FPDIClipboardNew, (FPD_ClipBoardHandlerCallbacks callbacks))

INTERFACE(void, FPDIClipboardDestroy, (FPD_IClipboardHandler handler))

NumOfSelector(FPDIClipboard)
ENDENUM

//*****************************
/* IPopupMenu HFT functions */
//*****************************

BEGINENUM
INTERFACE(FPD_IPopupMenuHandler, FPDIPopupMenuNew, (FPD_PopupMenuHandlerCallbacks callbacks))

INTERFACE(void, FPDIPopupMenuDestroy, (FPD_IPopupMenuHandler handler))

NumOfSelector(FPDIPopupMenu)
ENDENUM

//*****************************
/* ITip HFT functions */
//*****************************

BEGINENUM
INTERFACE(FPD_ITipHandler, FPDITipNew, (FPD_TipCallbacks callbacks))

INTERFACE(void, FPDITipDestroy, (FPD_ITipHandler handler))

NumOfSelector(FPDITip)
ENDENUM

//*****************************
/* IOperationNotify HFT functions */
//*****************************

BEGINENUM
INTERFACE(FPD_IOperationNotifyHandler, FPDIOperationNotifyNew, (FPD_IOperationNotifyCallbacks callbacks))

INTERFACE(void, FPDIOperationNotifyDestroy, (FPD_IOperationNotifyHandler handler))

NumOfSelector(FPDIOperationNotify)
ENDENUM

//*****************************
/* IPublicOptionData HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FPDIPublicOptionDataSetForegroundColor, (FPD_IPublicOptionData optionData,  FS_COLORREF color))

INTERFACE(FS_COLORREF, FPDIPublicOptionDataGetForegroundColor, (FPD_IPublicOptionData optionData))

INTERFACE(void, FPDIPublicOptionDataSetBackgroundColor, (FPD_IPublicOptionData optionData,  FS_COLORREF color))

INTERFACE(FS_COLORREF, FPDIPublicOptionDataGetBackgroundColor, (FPD_IPublicOptionData optionData))

INTERFACE(void, FPDIPublicOptionDataSetLayerOpacity, (FPD_IPublicOptionData optionData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDIPublicOptionDataGetLayerOpacity, (FPD_IPublicOptionData optionData))

INTERFACE(void, FPDIPublicOptionDataSetLayerFill, (FPD_IPublicOptionData optionData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDIPublicOptionDataGetLayerFill, (FPD_IPublicOptionData optionData))

INTERFACE(void, FPDIPublicOptionDataSetLayerBlendMode, (FPD_IPublicOptionData optionData,  FPD_ImageEditoBlendMode value))

INTERFACE(FPD_ImageEditoBlendMode, FPDIPublicOptionDataGetLayerBlendMode, (FPD_IPublicOptionData optionData))

NumOfSelector(FPDIPublicOptionData)
ENDENUM

//*****************************
/* IBaseBrushOptionData HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FPDIBaseBrushOptionDataSetBrushDiameter, (FPD_IBaseBrushOptionData optionData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDIBaseBrushOptionDataGetBrushDiameter, (FPD_IBaseBrushOptionData optionData))

INTERFACE(void, FPDIBaseBrushOptionDataSetBrushHardness, (FPD_IBaseBrushOptionData optionData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDIBaseBrushOptionDataGetBrushHardness, (FPD_IBaseBrushOptionData optionData))

NumOfSelector(FPDIBaseBrushOptionData)
ENDENUM

//*****************************
/* IBrushOptionData HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FPDIBrushOptionDataSetBlendMode, (FPD_IBrushOptionData optionData,  FPD_ImageEditoBlendMode value))

INTERFACE(FPD_ImageEditoBlendMode, FPDIBrushOptionDataGetBlendMode, (FPD_IBrushOptionData optionData))

INTERFACE(void, FPDIBrushOptionDataSetFlow, (FPD_IBrushOptionData optionData,  FS_BYTE value))

INTERFACE(FS_BYTE, FPDIBrushOptionDataGetFlow, (FPD_IBrushOptionData optionData))

INTERFACE(void, FPDIBrushOptionDataSetOpacity, (FPD_IBrushOptionData optionData,  FS_BYTE value))

INTERFACE(FS_BYTE, FPDIBrushOptionDataGetOpacity, (FPD_IBrushOptionData optionData))

INTERFACE(void, FPDIBrushOptionDataSetBaseBrushDiameter, (FPD_IBrushOptionData optionData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDIBrushOptionDataGetBaseBrushDiameter, (FPD_IBrushOptionData optionData))

INTERFACE(void, FPDIBrushOptionDataSetBaseBrushHardness, (FPD_IBrushOptionData optionData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDIBrushOptionDataGetBaseBrushHardness, (FPD_IBrushOptionData optionData))

NumOfSelector(FPDIBrushOptionData)
ENDENUM

//*****************************
/* IEraserOptionData HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FPDIEraserOptionDataSetEraserMode, (FPD_IEraserOptionData optionData,  FPD_EraserMode value))

INTERFACE(FPD_EraserMode, FPDIEraserOptionDataGetEraserMode, (FPD_IEraserOptionData optionData))

INTERFACE(void, FPDIEraserOptionDataSetFlow, (FPD_IEraserOptionData optionData,  FS_BYTE value))

INTERFACE(FS_BYTE, FPDIEraserOptionDataGetFlow, (FPD_IEraserOptionData optionData))

INTERFACE(void, FPDIEraserOptionDataSetOpacity, (FPD_IEraserOptionData optionData,  FS_BYTE value))

INTERFACE(FS_BYTE, FPDIEraserOptionDataGetOpacity, (FPD_IEraserOptionData optionData))

INTERFACE(void, FPDIEraserOptionDataSetEraserHistory, (FPD_IEraserOptionData optionData,  FS_BOOL bSet))

INTERFACE(FS_BOOL, FPDIEraserOptionDataGetEraserHistory, (FPD_IEraserOptionData optionData))

INTERFACE(void, FPDIEraserOptionDataSetBaseBrushDiameter, (FPD_IEraserOptionData optionData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDIEraserOptionDataGetBaseBrushDiameter, (FPD_IEraserOptionData optionData))

INTERFACE(void, FPDIEraserOptionDataSetBaseBrushHardness, (FPD_IEraserOptionData optionData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDIEraserOptionDataGetBaseBrushHardness, (FPD_IEraserOptionData optionData))

NumOfSelector(FPDIEraserOptionData)
ENDENUM

//*****************************
/* IMagicWandOptionData HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FPDIMagicWandOptionDataSetTolerance, (FPD_IMagicWandOptionData optionData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDIMagicWandOptionDataGetTolerance, (FPD_IMagicWandOptionData optionData))

INTERFACE(void, FPDIMagicWandOptionDataSetIsContinuous, (FPD_IMagicWandOptionData optionData,  FS_BOOL value))

INTERFACE(FS_BOOL, FPDIMagicWandOptionDataGetIsContinuous, (FPD_IMagicWandOptionData optionData))

NumOfSelector(FPDIMagicWandOptionData)
ENDENUM

//*****************************
/* IDodgeOptionData HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FPDIDodgeOptionDataSetDodgeMode, (FPD_IDodgeOptionData optionData,  FPD_TonesMode value))

INTERFACE(FPD_TonesMode, FPDIDodgeOptionDataGetDodgeMode, (FPD_IDodgeOptionData optionData))

INTERFACE(void, FPDIDodgeOptionDataSetFlow, (FPD_IDodgeOptionData optionData,  FS_BYTE value))

INTERFACE(FS_BYTE, FPDIDodgeOptionDataGetFlow, (FPD_IDodgeOptionData optionData))

INTERFACE(void, FPDIDodgeOptionDataSetOpacity, (FPD_IDodgeOptionData optionData,  FS_BYTE value))

INTERFACE(FS_BYTE, FPDIDodgeOptionDataGetOpacity, (FPD_IDodgeOptionData optionData))

INTERFACE(void, FPDIDodgeOptionDataSetBaseBrushDiameter, (FPD_IDodgeOptionData optionData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDIDodgeOptionDataGetBaseBrushDiameter, (FPD_IDodgeOptionData optionData))

INTERFACE(void, FPDIDodgeOptionDataSetBaseBrushHardness, (FPD_IDodgeOptionData optionData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDIDodgeOptionDataGetBaseBrushHardness, (FPD_IDodgeOptionData optionData))

NumOfSelector(FPDIDodgeOptionData)
ENDENUM

//*****************************
/* IBurnOptionData HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FPDIBurnOptionDataSetFlow, (FPD_IBurnOptionData optionData,  FS_BYTE value))

INTERFACE(FS_BYTE, FPDIBurnOptionDataGetFlow, (FPD_IBurnOptionData optionData))

INTERFACE(void, FPDIBurnOptionDataSetOpacity, (FPD_IBurnOptionData optionData,  FS_BYTE value))

INTERFACE(FS_BYTE, FPDIBurnOptionDataGetOpacity, (FPD_IBurnOptionData optionData))

INTERFACE(void, FPDIBurnOptionDataSetBurnMode, (FPD_IBurnOptionData optionData,  FPD_TonesMode value))

INTERFACE(FPD_TonesMode, FPDIBurnOptionDataGetBurnMode, (FPD_IBurnOptionData optionData))

INTERFACE(void, FPDIBurnOptionDataSetBaseBrushDiameter, (FPD_IBurnOptionData optionData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDIBurnOptionDataGetBaseBrushDiameter, (FPD_IBurnOptionData optionData))

INTERFACE(void, FPDIBurnOptionDataSetBaseBrushHardness, (FPD_IBurnOptionData optionData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDIBurnOptionDataGetBaseBrushHardness, (FPD_IBurnOptionData optionData))

NumOfSelector(FPDIBurnOptionData)
ENDENUM

//*****************************
/* IEyedropperData HFT functions */
//*****************************

BEGINENUM
INTERFACE(FPD_EyedropperSampleType, FPDIEyedropperDataGetSampleType, (FPD_IEyedropperData optionData))

INTERFACE(void, FPDIEyedropperDataSetSampleType, (FPD_IEyedropperData optionData,  FPD_EyedropperSampleType sampleType))

NumOfSelector(FPDIEyedropperData)
ENDENUM

//*****************************
/* ICloneStampData HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FPDICloneStampDataSetBlendMode, (FPD_ICloneStampData stampData,  FPD_ImageEditoBlendMode value))

INTERFACE(FPD_ImageEditoBlendMode, FPDICloneStampDataGetBlendMode, (FPD_ICloneStampData stampData))

INTERFACE(void, FPDICloneStampDataSetFlow, (FPD_ICloneStampData stampData,  FS_BYTE value))

INTERFACE(FS_BYTE, FPDICloneStampDataGetFlow, (FPD_ICloneStampData stampData))

INTERFACE(void, FPDICloneStampDataSetOpacity, (FPD_ICloneStampData stampData,  FS_BYTE value))

INTERFACE(FS_BYTE, FPDICloneStampDataGetOpacity, (FPD_ICloneStampData stampData))

INTERFACE(void, FPDICloneStampDataSetIsAlineCheck, (FPD_ICloneStampData stampData,  FS_BOOL value))

INTERFACE(FS_BOOL, FPDICloneStampDataGetIsAlineCheck, (FPD_ICloneStampData stampData))

INTERFACE(void, FPDICloneStampDataSetBaseBrushDiameter, (FPD_ICloneStampData stampData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDICloneStampDataGetBaseBrushDiameter, (FPD_ICloneStampData stampData))

INTERFACE(void, FPDICloneStampDataSetBaseBrushHardness, (FPD_ICloneStampData stampData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDICloneStampDataGetBaseBrushHardness, (FPD_ICloneStampData stampData))

NumOfSelector(FPDICloneStampData)
ENDENUM

//*****************************
/* IPaintBucketOptionData HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FPDIPaintBucketOptionDataSetTolerance, (FPD_IPaintBucketOptionData optionData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDIPaintBucketOptionDataGetTolerance, (FPD_IPaintBucketOptionData optionData))

INTERFACE(void, FPDIPaintBucketOptionDataSetOpacity, (FPD_IPaintBucketOptionData optionData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDIPaintBucketOptionDataGetOpacity, (FPD_IPaintBucketOptionData optionData))

INTERFACE(void, FPDIPaintBucketOptionDataSetColorBlendMode, (FPD_IPaintBucketOptionData optionData,  FPD_ImageEditoBlendMode value))

INTERFACE(FPD_ImageEditoBlendMode, FPDIPaintBucketOptionDataGetColorBlendMode, (FPD_IPaintBucketOptionData optionData))

INTERFACE(void, FPDIPaintBucketOptionDataSetContinuous, (FPD_IPaintBucketOptionData optionData,  FS_BOOL bContinuous))

INTERFACE(FS_BOOL, FPDIPaintBucketOptionDataGetContinuous, (FPD_IPaintBucketOptionData optionData))

NumOfSelector(FPDIPaintBucketOptionData)
ENDENUM

//*****************************
/* ISpotHealingBrushData HFT functions */
//*****************************

BEGINENUM
INTERFACE(FS_INT32, FPDISpotHealingBrushDataGetDiameter, (FPD_ISpotHealingBrushData brushData))

INTERFACE(void, FPDISpotHealingBrushDataSetDiameter, (FPD_ISpotHealingBrushData brushData,  FS_INT32 value))

INTERFACE(FS_INT32, FPDISpotHealingBrushDataGetRoundness, (FPD_ISpotHealingBrushData brushData))

INTERFACE(void, FPDISpotHealingBrushDataSetRoundness, (FPD_ISpotHealingBrushData brushData,  FS_INT32 value))

INTERFACE(FS_BOOL, FPDISpotHealingBrushDataGetAline, (FPD_ISpotHealingBrushData brushData))

INTERFACE(void, FPDISpotHealingBrushDataSetAline, (FPD_ISpotHealingBrushData brushData,  FS_BOOL value))

INTERFACE(FPD_ImageMode, FPDISpotHealingBrushDataGetMode, (FPD_ISpotHealingBrushData brushData))

INTERFACE(void, FPDISpotHealingBrushDataSetMode, (FPD_ISpotHealingBrushData brushData,  FPD_ImageMode value))

NumOfSelector(FPDISpotHealingBrushData)
ENDENUM

//*****************************
/* CEditObject HFT functions */
//*****************************

BEGINENUM
INTERFACE(FPD_EditObject, FPDCEditObjectNew, (FPD_PageObject pPageObject,  FS_INT32 nIndex,  FS_POSITION pos,  FS_PtrArray vecTextRange,  FS_BOOL bIsInForm))

INTERFACE(FPD_EditObject, FPDCEditObjectNew2, (const FPD_EditObject& src))

INTERFACE(void, FPDCEditObjectDelete, (FPD_EditObject editObject))

INTERFACE(FPD_EditObject, FPDCEditObjectCombineObjct, (FPD_EditObject editObject,  const FPD_EditObject& src))

INTERFACE(FS_BOOL, FPDCEditObjectIsEqual, (FPD_EditObject editObject,  const FPD_EditObject& src))

INTERFACE(void, FPDCEditObjectCopyBaseInfo, (FPD_EditObject editObject,  const FPD_EditObject& src,  FS_BOOL bClone))

INTERFACE(void, FPDCEditObjectUpdateFormInfo, (FPD_EditObject editObject))

INTERFACE(FS_BOOL, FPDCEditObjectUpdateFormInfo2, (FPD_EditObject editObject,  FPD_Page page))

INTERFACE(FPD_PageObject, FPDCEditObjectGetLastFormObj, (FPD_EditObject editObject))

INTERFACE(FPD_PageObject, FPDCEditObjectGetFirstFormObj, (FPD_EditObject editObject))

INTERFACE(FPD_Page, FPDCEditObjectGetContainer, (FPD_EditObject editObject,  FPD_Page page))

INTERFACE(FS_FloatRect, FPDCEditObjectGetObjBBox, (FPD_EditObject editObject,  FS_BOOL bTypeSet))

INTERFACE(void, FPDCEditObjectReset, (FPD_EditObject editObject))

INTERFACE(void, FPDCEditObjectUndoRedoState, (FS_PtrArray& vecObj,  FS_BOOL bSort))

INTERFACE(void, FPDCEditObjectMergeTextObjRange, (FPD_EditObject& obj))

INTERFACE(void, FPDCEditObjectMergeTextObjRange2, (FS_PtrArray& vecObjs))

NumOfSelector(FPDCEditObject)
ENDENUM

//*****************************
/* IJoinSplit HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FPDIJoinSplitExitJoinEditing, (FPD_ITouchJoinSplit joinSplit))

INTERFACE(FS_BOOL, FPDIJoinSplitOnActivate, (FPD_ITouchJoinSplit joinSplit))

INTERFACE(FS_BOOL, FPDIJoinSplitOnDeactivate, (FPD_ITouchJoinSplit joinSplit))

INTERFACE(FS_BOOL, FPDIJoinSplitOnPaint, (FPD_ITouchJoinSplit joinSplit,  const FS_PtrArray& visibleEditorPage,  FPD_RenderDevice& device))

INTERFACE(FS_BOOL, FPDIJoinSplitOnLeftButtonDown, (FPD_ITouchJoinSplit joinSplit,  FPD_TouchEditorPage editorPage,  FS_DevicePoint pt))

INTERFACE(FS_BOOL, FPDIJoinSplitOnLeftButtonUp, (FPD_ITouchJoinSplit joinSplit,  FPD_TouchEditorPage editorPage,  FS_DevicePoint pt))

INTERFACE(FS_BOOL, FPDIJoinSplitOnMouseMove, (FPD_ITouchJoinSplit joinSplit,  FPD_TouchEditorPage editorPage,  FS_DevicePoint pt))

INTERFACE(FS_BOOL, FPDIJoinSplitOnRightButtonUp, (FPD_ITouchJoinSplit joinSplit,  FPD_TouchEditorPage editorPage,  FS_DevicePoint pt))

INTERFACE(FS_BOOL, FPDIJoinSplitIsProcessing, (FPD_ITouchJoinSplit joinSplit))

INTERFACE(FS_BOOL, FPDIJoinSplitBtnEnableStatus, (FPD_ITouchJoinSplit joinSplit,  FPD_TouchJoinSpiltOperationType opType))

INTERFACE(void, FPDIJoinSplitJoinBoxes, (FPD_ITouchJoinSplit joinSplit))

INTERFACE(void, FPDIJoinSplitSplitBoxes, (FPD_ITouchJoinSplit joinSplit))

INTERFACE(void, FPDIJoinSplitLinkBoxes, (FPD_ITouchJoinSplit joinSplit))

INTERFACE(void, FPDIJoinSplitUnlinkBoxes, (FPD_ITouchJoinSplit joinSplit))

INTERFACE(void, FPDIJoinSplitSelectNone, (FPD_ITouchJoinSplit joinSplit))

INTERFACE(void, FPDIJoinSplitOnAfterInsertPages, (FPD_ITouchJoinSplit joinSplit,  FS_INT32 nInsertAt,  FS_INT32 nCount))

INTERFACE(void, FPDIJoinSplitOnAfterDeletePages, (FPD_ITouchJoinSplit joinSplit,  const FS_WordArray &arrDelPages))

INTERFACE(void, FPDIJoinSplitOnAfterReplacePages, (FPD_ITouchJoinSplit joinSplit,  FS_INT32 nStart,  const FS_WordArray& arrSrcPages))

INTERFACE(void, FPDIJoinSplitOnAfterResizePage, (FPD_ITouchJoinSplit joinSplit,  FS_INT32 iPage))

INTERFACE(void, FPDIJoinSplitOnAfterRotatePage, (FPD_ITouchJoinSplit joinSplit,  FS_INT32 iPage,  FS_INT32 nRotate))

INTERFACE(void, FPDIJoinSplitOnAfterMovePages, (FPD_ITouchJoinSplit joinSplit,  FS_INT32 nMoveTo,  const FS_WordArray arrToMove))

NumOfSelector(FPDIJoinSplit)
ENDENUM

//*****************************
/* ITouchup HFT functions */
//*****************************

BEGINENUM
INTERFACE(FS_BOOL, FPDITouchupOnActivate, (FPD_ITouchup touchup))

INTERFACE(FS_BOOL, FPDITouchupOnDeactivate, (FPD_ITouchup touchup))

INTERFACE(void, FPDITouchupExistEditing, (FPD_ITouchup touchup,  FS_BOOL bEndDirectly))

INTERFACE(FS_BOOL, FPDITouchupOnPaint, (FPD_ITouchup touchup,  const FS_PtrArray& visibleEditorPage,  FPD_RenderDevice& device))

INTERFACE(FS_BOOL, FPDITouchupOnChar, (FPD_ITouchup touchup,  FS_WCHAR Key))

INTERFACE(FS_BOOL, FPDITouchupOnVKUP, (FPD_ITouchup touchup,  FS_UINT nKeyCode))

INTERFACE(FS_BOOL, FPDITouchupOnVKDOWN, (FPD_ITouchup touchup,  FS_UINT nKeyCode))

INTERFACE(FS_BOOL, FPDITouchupOnLeftButtonDown, (FPD_ITouchup touchup,  FPD_TouchEditorPage editorPage,  FS_DevicePoint pt))

INTERFACE(FS_BOOL, FPDITouchupOnLeftButtonUp, (FPD_ITouchup touchup,  FPD_TouchEditorPage editorPage,  FS_DevicePoint pt))

INTERFACE(FS_BOOL, FPDITouchupOnLeftButtonDblClk, (FPD_ITouchup touchup,  FPD_TouchEditorPage editorPage,  FS_DevicePoint pt))

INTERFACE(FS_BOOL, FPDITouchupOnMouseMove, (FPD_ITouchup touchup,  FPD_TouchEditorPage editorPage,  FS_DevicePoint pt))

INTERFACE(FS_BOOL, FPDITouchupOnMouseWheel, (FPD_ITouchup touchup,  FPD_TouchEditorPage editorPage,  FS_DevicePoint pt,  FS_SHORT zDelta))

INTERFACE(FS_BOOL, FPDITouchupOnRightButtonDown, (FPD_ITouchup touchup,  FPD_TouchEditorPage editorPage,  FS_DevicePoint pt))

INTERFACE(FS_BOOL, FPDITouchupOnRightButtonUp, (FPD_ITouchup touchup,  FPD_TouchEditorPage editorPage,  FS_DevicePoint pt))

INTERFACE(FS_BOOL, FPDITouchupCanSelectAll, (FPD_ITouchup touchup))

INTERFACE(FS_BOOL, FPDITouchupDoSelectAll, (FPD_ITouchup touchup))

INTERFACE(FS_BOOL, FPDITouchupCanDelete, (FPD_ITouchup touchup))

INTERFACE(FS_BOOL, FPDITouchupDoDelete, (FPD_ITouchup touchup))

INTERFACE(FS_BOOL, FPDITouchupCanCopy, (FPD_ITouchup touchup))

INTERFACE(FS_BOOL, FPDITouchupDoCopy, (FPD_ITouchup touchup))

INTERFACE(FS_BOOL, FPDITouchupCanCut, (FPD_ITouchup touchup))

INTERFACE(FS_BOOL, FPDITouchupDoCut, (FPD_ITouchup touchup))

INTERFACE(FS_BOOL, FPDITouchupCanPaste, (FPD_ITouchup touchup))

INTERFACE(FS_BOOL, FPDITouchupDoPaste, (FPD_ITouchup touchup))

INTERFACE(FS_BOOL, FPDITouchupCanDeselectAll, (FPD_ITouchup touchup))

INTERFACE(FS_BOOL, FPDITouchupDoDeselectAll, (FPD_ITouchup touchup))

INTERFACE(void, FPDITouchupOnFontNameChanged, (FPD_ITouchup touchup,  const FS_WideString& wsFontName))

INTERFACE(void, FPDITouchupOnFontSizeChanged, (FPD_ITouchup touchup,  FS_FLOAT fFontSize))

INTERFACE(void, FPDITouchupOnTextColorChanged, (FPD_ITouchup touchup,  FS_COLORREF textColor))

INTERFACE(void, FPDITouchupOnBoldItlicChanged, (FPD_ITouchup touchup,  const FS_WideString& cwFontName,  FS_BOOL bBold,  FS_BOOL bItlic,  FS_BOOL bIsBoldChanged))

INTERFACE(void, FPDITouchupOnAlignChanged, (FPD_ITouchup touchup,  FS_INT32 dwAlign))

INTERFACE(void, FPDITouchupOnCharSpaceChanged, (FPD_ITouchup touchup,  FS_FLOAT flSpace))

INTERFACE(void, FPDITouchupOnCharHorzScaleChanged, (FPD_ITouchup touchup,  FS_INT32 nScale))

INTERFACE(void, FPDITouchupOnLineLeadingChanged, (FPD_ITouchup touchup,  FS_FLOAT flLineLeading))

INTERFACE(void, FPDITouchupOnUnderlineChanged, (FPD_ITouchup touchup,  FS_BOOL bUnderline))

INTERFACE(void, FPDITouchupOnCrossChanged, (FPD_ITouchup touchup,  FS_BOOL bCross))

INTERFACE(void, FPDITouchupOnSuperScriptChanged, (FPD_ITouchup touchup,  FS_BOOL bSuperSet))

INTERFACE(void, FPDITouchupOnSubScriptChanged, (FPD_ITouchup touchup,  FS_BOOL bSubScript))

INTERFACE(void, FPDITouchupOnParagraphSpacingChanged, (FPD_ITouchup touchup,  FS_FLOAT fParaSpacing))

INTERFACE(void, FPDITouchupOnInDentChanged, (FPD_ITouchup touchup))

INTERFACE(void, FPDITouchupOnDeDentChanged, (FPD_ITouchup touchup))

INTERFACE(void, FPDITouchupOnAfterInsertPages, (FPD_ITouchup touchup,  FS_INT32 nInsertAt,  FS_INT32 nCount))

INTERFACE(void, FPDITouchupOnAfterDeletePages, (FPD_ITouchup touchup,  const FS_WordArray &arrDelPages))

INTERFACE(void, FPDITouchupOnAfterReplacePages, (FPD_ITouchup touchup,  FS_INT32 nStart,  const FS_WordArray& arrSrcPagess))

INTERFACE(void, FPDITouchupOnAfterResizePage, (FPD_ITouchup touchup,  FS_INT32 nPageIndex))

INTERFACE(void, FPDITouchupOnAfterRotatePage, (FPD_ITouchup touchup,  FS_INT32 iPage,  FS_INT32 nRotate))

INTERFACE(void, FPDITouchupOnAfterMovePages, (FPD_ITouchup touchup,  FS_INT32 nMoveTo,  const FS_WordArray arrToMove))

INTERFACE(FS_BOOL, FPDITouchupOnPageContentChange, (FPD_ITouchup touchup,  FS_PtrArray editorPages))

INTERFACE(FS_LPVOID, FPDITouchupGetFXEditor, (FPD_ITouchup touchup))

NumOfSelector(FPDITouchup)
ENDENUM

//*****************************
/* ITouchupManager HFT functions */
//*****************************

BEGINENUM
INTERFACE(FPD_ITouchupManager, FPDITouchupManagerCreateManager, (FPD_ITouchupProvider pProvider,  FPD_Document pPDFDoc))

INTERFACE(FPD_ITouchup, FPDITouchupManagerGetTouchup, (FPD_ITouchupManager touchMgr))

INTERFACE(FPD_ITouchJoinSplit, FPDITouchupManagerGetJoinSpilt, (FPD_ITouchupManager touchMgr))

NumOfSelector(FPDITouchupManager)
ENDENUM

//*****************************
/* ITouchUndoItem HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FPDITouchUndoItemOnUndo, (FPD_ITouchUndoItem item))

INTERFACE(void, FPDITouchUndoItemOnRedo, (FPD_ITouchUndoItem item))

INTERFACE(void, FPDITouchUndoItemOnRelease, (FPD_ITouchUndoItem item))

NumOfSelector(FPDITouchUndoItem)
ENDENUM

//*****************************
/* ITouchClipboard HFT functions */
//*****************************

BEGINENUM
INTERFACE(FPD_ITouchClipboardHandler, FPDITouchClipboardNew, (FPD_TouchClipBoardHandlerCallbacks callbacks))

INTERFACE(void, FPDITouchClipboardDestroy, (FPD_ITouchClipboardHandler handler))

NumOfSelector(FPDITouchClipboard)
ENDENUM

//*****************************
/* ITouchPopupMenu HFT functions */
//*****************************

BEGINENUM
INTERFACE(FPD_ITouchPopupMenuHandler, FPDITouchPopupMenuNew, (FPD_TouchPopupMenuHandlerCallbacks callbacks))

INTERFACE(void, FPDITouchPopupMenuDestroy, (FPD_ITouchPopupMenuHandler handler))

NumOfSelector(FPDITouchPopupMenu)
ENDENUM

//*****************************
/* ITouchProgressBar HFT functions */
//*****************************

BEGINENUM
INTERFACE(FPD_ITouchProgressBarHandler, FPDITouchProgressBarNew, (FPD_TouchProgressBarHandlerCallbacks callbacks))

INTERFACE(void, FPDITouchProgressBarDestroy, (FPD_ITouchProgressBarHandler handler))

NumOfSelector(FPDITouchProgressBar)
ENDENUM

//*****************************
/* ITouchOperationNotify HFT functions */
//*****************************

BEGINENUM
INTERFACE(FPD_ITouchOperationNotifyHandler, FPDITouchOperationNotifyNew, (FPD_ITouchOperationNotifyCallbacks callbacks))

INTERFACE(void, FPDITouchOperationNotifyDestroy, (FPD_ITouchOperationNotifyHandler handler))

NumOfSelector(FPDITouchOperationNotify)
ENDENUM

//*****************************
/* ITouchUndoHandler HFT functions */
//*****************************

BEGINENUM
INTERFACE(FPD_ITouchUndoHandler, FPDITouchUndoHandlerNew, (FPD_ITouchUndoHandlerCallbacks callbacks))

INTERFACE(void, FPDITouchUndoHandlerDestroy, (FPD_ITouchUndoHandler handler))

NumOfSelector(FPDITouchUndoHandler)
ENDENUM

//*****************************
/* ITouchTextFormatHandler HFT functions */
//*****************************

BEGINENUM
INTERFACE(FPD_ITouchTextFormatHandler, FPDITouchTextFormatHandlerNew, (FPD_ITouchTextFormatHandlerCallbacks callbacks))

INTERFACE(void, FPDITouchTextFormatHandlerDestroy, (FPD_ITouchTextFormatHandler handler))

NumOfSelector(FPDITouchTextFormatHandler)
ENDENUM

//*****************************
/* ITouchProvider HFT functions */
//*****************************

BEGINENUM
INTERFACE(FPD_ITouchupProvider, FPDITouchProviderNew, (FPD_ITouchProviderCallbacks callbacks))

INTERFACE(void, FPDITouchProviderDestroy, (FPD_ITouchupProvider handler))

NumOfSelector(FPDITouchProvider)
ENDENUM

