﻿/*****************************************************************************

Copyright (C) 2010 Foxit Corporation
All rights reserved.

NOTICE: Foxit permits you to use, modify, and distribute this file
in accordance with the terms of the Foxit license agreement
accompanying it. If you have received this file from a source other
than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
 
*****************************************************************************/

#ifndef FPD_PAGEOBJCALLS_H
#define FPD_PAGEOBJCALLS_H

#ifndef FS_COMMON_H
#include "../basic/fs_common.h"
#endif

#ifndef FS_BASICEXPT_H
#include "../basic/fs_basicExpT.h"
#endif

#ifndef FS_STRINGEXPT_H
#include "../basic/fs_stringExpT.h"
#endif

#ifndef FPD_PAGEOBJEXPT_H
#include "fpd_pageobjExpT.h"
#endif

#ifdef __cplusplus
extern "C"{
#endif

#undef INTERFACE
#undef BEGINENUM
#undef NumOfSelector
#undef ENDENUM

// Define macros to generate function selectors in the file fpd_pageobjImpl.h
#define BEGINENUM enum{
#define NumOfSelector(name) name##InterfacesNum
#define ENDENUM };
#define INTERFACE(returnType,interfaceName,params) interfaceName##SEL,
#include "fpd_pageobjTempl.h"

#undef INTERFACE
#undef BEGINENUM
#undef NumOfSelector
#undef ENDENUM

// Define macros to generate function PROTO in the file fpd_pageobjImpl.h
#define BEGINENUM
#define NumOfSelector(name)
#define ENDENUM
#define INTERFACE(returnType,interfaceName, params) \
typedef returnType (*interfaceName##SELPROTO)params;
#include "fpd_pageobjTempl.h"

//----------_V1----------
//*****************************
/* Path HFT functions */
//*****************************

#define FPDPathNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathNewSELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathNewSEL, _gPID)))

#define FPDPathDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathDestroySELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathDestroySEL, _gPID)))

#define FPDPathGetPointCount (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathGetPointCountSELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathGetPointCountSEL, _gPID)))

#define FPDPathGetFlag (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathGetFlagSELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathGetFlagSEL, _gPID)))

#define FPDPathGetPointX (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathGetPointXSELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathGetPointXSEL, _gPID)))

#define FPDPathGetPointY (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathGetPointYSELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathGetPointYSEL, _gPID)))

#define FPDPathGetPoint (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathGetPointSELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathGetPointSEL, _gPID)))

#define FPDPathGetBoundingBox (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathGetBoundingBoxSELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathGetBoundingBoxSEL, _gPID)))

#define FPDPathGetBoundingBox2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathGetBoundingBox2SELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathGetBoundingBox2SEL, _gPID)))

#define FPDPathTransform (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathTransformSELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathTransformSEL, _gPID)))

#define FPDPathAppend (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathAppendSELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathAppendSEL, _gPID)))

#define FPDPathAppendRect (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathAppendRectSELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathAppendRectSEL, _gPID)))

#define FPDPathIsRect (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathIsRectSELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathIsRectSEL, _gPID)))

#define FPDPathSetPointCount (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathSetPointCountSELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathSetPointCountSEL, _gPID)))

#define FPDPathSetPoint (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathSetPointSELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathSetPointSEL, _gPID)))

#define FPDPathGetModify (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathGetModifySELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathGetModifySEL, _gPID)))

#define FPDPathTrimPoints (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathTrimPointsSELPROTO)FRCOREROUTINE(FPDPathSEL,FPDPathTrimPointsSEL, _gPID)))

//*****************************
/* ClipPath HFT functions */
//*****************************

#define FPDClipPathNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDClipPathNewSELPROTO)FRCOREROUTINE(FPDClipPathSEL,FPDClipPathNewSEL, _gPID)))

#define FPDClipPathDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDClipPathDestroySELPROTO)FRCOREROUTINE(FPDClipPathSEL,FPDClipPathDestroySEL, _gPID)))

#define FPDClipPathAppendPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDClipPathAppendPathSELPROTO)FRCOREROUTINE(FPDClipPathSEL,FPDClipPathAppendPathSEL, _gPID)))

#define FPDClipPathDeletePath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDClipPathDeletePathSELPROTO)FRCOREROUTINE(FPDClipPathSEL,FPDClipPathDeletePathSEL, _gPID)))

#define FPDClipPathGetPathCount (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDClipPathGetPathCountSELPROTO)FRCOREROUTINE(FPDClipPathSEL,FPDClipPathGetPathCountSEL, _gPID)))

#define FPDClipPathTransform (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDClipPathTransformSELPROTO)FRCOREROUTINE(FPDClipPathSEL,FPDClipPathTransformSEL, _gPID)))

#define FPDClipPathGetPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDClipPathGetPathSELPROTO)FRCOREROUTINE(FPDClipPathSEL,FPDClipPathGetPathSEL, _gPID)))

#define FPDClipPathGetClipType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDClipPathGetClipTypeSELPROTO)FRCOREROUTINE(FPDClipPathSEL,FPDClipPathGetClipTypeSEL, _gPID)))

#define FPDClipPathGetTextCount (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDClipPathGetTextCountSELPROTO)FRCOREROUTINE(FPDClipPathSEL,FPDClipPathGetTextCountSEL, _gPID)))

#define FPDClipPathGetClipBox (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDClipPathGetClipBoxSELPROTO)FRCOREROUTINE(FPDClipPathSEL,FPDClipPathGetClipBoxSEL, _gPID)))

#define FPDClipPathGetText (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDClipPathGetTextSELPROTO)FRCOREROUTINE(FPDClipPathSEL,FPDClipPathGetTextSEL, _gPID)))

#define FPDClipPathAppendTexts (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDClipPathAppendTextsSELPROTO)FRCOREROUTINE(FPDClipPathSEL,FPDClipPathAppendTextsSEL, _gPID)))

#define FPDClipPathSetCount (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDClipPathSetCountSELPROTO)FRCOREROUTINE(FPDClipPathSEL,FPDClipPathSetCountSEL, _gPID)))

#define FPDClipPathIsNull (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDClipPathIsNullSELPROTO)FRCOREROUTINE(FPDClipPathSEL,FPDClipPathIsNullSEL, _gPID)))

#define FPDClipPathGetModify (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDClipPathGetModifySELPROTO)FRCOREROUTINE(FPDClipPathSEL,FPDClipPathGetModifySEL, _gPID)))

#define FPDClipPathGetPathPointer (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDClipPathGetPathPointerSELPROTO)FRCOREROUTINE(FPDClipPathSEL,FPDClipPathGetPathPointerSEL, _gPID)))

//*****************************
/* ColorState HFT functions */
//*****************************

#define FPDColorStateNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDColorStateNewSELPROTO)FRCOREROUTINE(FPDColorStateSEL,FPDColorStateNewSEL, _gPID)))

#define FPDColorStateDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDColorStateDestroySELPROTO)FRCOREROUTINE(FPDColorStateSEL,FPDColorStateDestroySEL, _gPID)))

#define FPDColorStateGetFillColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDColorStateGetFillColorSELPROTO)FRCOREROUTINE(FPDColorStateSEL,FPDColorStateGetFillColorSEL, _gPID)))

#define FPDColorStateGetStrokeColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDColorStateGetStrokeColorSELPROTO)FRCOREROUTINE(FPDColorStateSEL,FPDColorStateGetStrokeColorSEL, _gPID)))

#define FPDColorStateSetFillColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDColorStateSetFillColorSELPROTO)FRCOREROUTINE(FPDColorStateSEL,FPDColorStateSetFillColorSEL, _gPID)))

#define FPDColorStateSetStrokeColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDColorStateSetStrokeColorSELPROTO)FRCOREROUTINE(FPDColorStateSEL,FPDColorStateSetStrokeColorSEL, _gPID)))

#define FPDColorStateSetFillPatternColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDColorStateSetFillPatternColorSELPROTO)FRCOREROUTINE(FPDColorStateSEL,FPDColorStateSetFillPatternColorSEL, _gPID)))

#define FPDColorStateSetStrokePatternColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDColorStateSetStrokePatternColorSELPROTO)FRCOREROUTINE(FPDColorStateSEL,FPDColorStateSetStrokePatternColorSEL, _gPID)))

#define FPDColorStateIsNull (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDColorStateIsNullSELPROTO)FRCOREROUTINE(FPDColorStateSEL,FPDColorStateIsNullSEL, _gPID)))

#define FPDColorStateGetModify (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDColorStateGetModifySELPROTO)FRCOREROUTINE(FPDColorStateSEL,FPDColorStateGetModifySEL, _gPID)))

#define FPDColorStateNotUseFillColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDColorStateNotUseFillColorSELPROTO)FRCOREROUTINE(FPDColorStateSEL,FPDColorStateNotUseFillColorSEL, _gPID)))

//*****************************
/* TextState HFT functions */
//*****************************

#define FPDTextStateNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateNewSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateNewSEL, _gPID)))

#define FPDTextStateDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateDestroySELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateDestroySEL, _gPID)))

#define FPDTextStateGetFont (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateGetFontSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateGetFontSEL, _gPID)))

#define FPDTextStateSetFont (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateSetFontSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateSetFontSEL, _gPID)))

#define FPDTextStateGetFontSize (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateGetFontSizeSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateGetFontSizeSEL, _gPID)))

#define FPDTextStateGetMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateGetMatrixSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateGetMatrixSEL, _gPID)))

#define FPDTextStateGetFontSizeV (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateGetFontSizeVSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateGetFontSizeVSEL, _gPID)))

#define FPDTextStateGetFontSizeH (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateGetFontSizeHSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateGetFontSizeHSEL, _gPID)))

#define FPDTextStateGetBaselineAngle (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateGetBaselineAngleSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateGetBaselineAngleSEL, _gPID)))

#define FPDTextStateGetShearAngle (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateGetShearAngleSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateGetShearAngleSEL, _gPID)))

#define FPDTextStateSetFontSize (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateSetFontSizeSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateSetFontSizeSEL, _gPID)))

#define FPDTextStateSetCharSpace (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateSetCharSpaceSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateSetCharSpaceSEL, _gPID)))

#define FPDTextStateSetWordSpace (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateSetWordSpaceSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateSetWordSpaceSEL, _gPID)))

#define FPDTextStateSetMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateSetMatrixSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateSetMatrixSEL, _gPID)))

#define FPDTextStateSetTextMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateSetTextModeSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateSetTextModeSEL, _gPID)))

#define FPDTextStateSetTextCTM (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateSetTextCTMSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateSetTextCTMSEL, _gPID)))

#define FPDTextStateGetTextMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateGetTextModeSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateGetTextModeSEL, _gPID)))

#define FPDTextStateGetTextCTM (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateGetTextCTMSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateGetTextCTMSEL, _gPID)))

#define FPDTextStateGetCharSpace (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateGetCharSpaceSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateGetCharSpaceSEL, _gPID)))

#define FPDTextStateGetWordSpace (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateGetWordSpaceSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateGetWordSpaceSEL, _gPID)))

#define FPDTextStateIsNull (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateIsNullSELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateIsNullSEL, _gPID)))

#define FPDTextStateGetModify (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextStateGetModifySELPROTO)FRCOREROUTINE(FPDTextStateSEL,FPDTextStateGetModifySEL, _gPID)))

//*****************************
/* GeneralState HFT functions */
//*****************************

#define FPDGeneralStateNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGeneralStateNewSELPROTO)FRCOREROUTINE(FPDGeneralStateSEL,FPDGeneralStateNewSEL, _gPID)))

#define FPDGeneralStateDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGeneralStateDestroySELPROTO)FRCOREROUTINE(FPDGeneralStateSEL,FPDGeneralStateDestroySEL, _gPID)))

#define FPDGeneralStateSetRenderIntent (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGeneralStateSetRenderIntentSELPROTO)FRCOREROUTINE(FPDGeneralStateSEL,FPDGeneralStateSetRenderIntentSEL, _gPID)))

#define FPDGeneralStateGetBlendType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGeneralStateGetBlendTypeSELPROTO)FRCOREROUTINE(FPDGeneralStateSEL,FPDGeneralStateGetBlendTypeSEL, _gPID)))

#define FPDGeneralStateGetAlpha (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGeneralStateGetAlphaSELPROTO)FRCOREROUTINE(FPDGeneralStateSEL,FPDGeneralStateGetAlphaSEL, _gPID)))

#define FPDGeneralStateSetBlendMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGeneralStateSetBlendModeSELPROTO)FRCOREROUTINE(FPDGeneralStateSEL,FPDGeneralStateSetBlendModeSEL, _gPID)))

#define FPDGeneralStateSetBlendType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGeneralStateSetBlendTypeSELPROTO)FRCOREROUTINE(FPDGeneralStateSEL,FPDGeneralStateSetBlendTypeSEL, _gPID)))

#define FPDGeneralStateSetSoftMask (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGeneralStateSetSoftMaskSELPROTO)FRCOREROUTINE(FPDGeneralStateSEL,FPDGeneralStateSetSoftMaskSEL, _gPID)))

#define FPDGeneralStateSetSoftMaskMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGeneralStateSetSoftMaskMatrixSELPROTO)FRCOREROUTINE(FPDGeneralStateSEL,FPDGeneralStateSetSoftMaskMatrixSEL, _gPID)))

#define FPDGeneralStateSetStrokeAlpha (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGeneralStateSetStrokeAlphaSELPROTO)FRCOREROUTINE(FPDGeneralStateSEL,FPDGeneralStateSetStrokeAlphaSEL, _gPID)))

#define FPDGeneralStateSetFillAlpha (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGeneralStateSetFillAlphaSELPROTO)FRCOREROUTINE(FPDGeneralStateSEL,FPDGeneralStateSetFillAlphaSEL, _gPID)))

#define FPDGeneralStateIsNull (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGeneralStateIsNullSELPROTO)FRCOREROUTINE(FPDGeneralStateSEL,FPDGeneralStateIsNullSEL, _gPID)))

#define FPDGeneralStateGetModify (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGeneralStateGetModifySELPROTO)FRCOREROUTINE(FPDGeneralStateSEL,FPDGeneralStateGetModifySEL, _gPID)))

//*****************************
/* GraphState HFT functions */
//*****************************

#define FPDGraphStateNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateNewSELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateNewSEL, _gPID)))

#define FPDGraphStateDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateDestroySELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateDestroySEL, _gPID)))

#define FPDGraphStateSetDashCount (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateSetDashCountSELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateSetDashCountSEL, _gPID)))

#define FPDGraphStateGetDashCount (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateGetDashCountSELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateGetDashCountSEL, _gPID)))

#define FPDGraphStateGetDashArray (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateGetDashArraySELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateGetDashArraySEL, _gPID)))

#define FPDGraphStateGetDashPhase (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateGetDashPhaseSELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateGetDashPhaseSEL, _gPID)))

#define FPDGraphStateSetDashPhase (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateSetDashPhaseSELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateSetDashPhaseSEL, _gPID)))

#define FPDGraphStateGetLineCap (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateGetLineCapSELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateGetLineCapSEL, _gPID)))

#define FPDGraphStateSetLineCap (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateSetLineCapSELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateSetLineCapSEL, _gPID)))

#define FPDGraphStateGetLineJoin (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateGetLineJoinSELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateGetLineJoinSEL, _gPID)))

#define FPDGraphStateSetLineJoin (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateSetLineJoinSELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateSetLineJoinSEL, _gPID)))

#define FPDGraphStateGetMiterLimit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateGetMiterLimitSELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateGetMiterLimitSEL, _gPID)))

#define FPDGraphStateSetMiterLimit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateSetMiterLimitSELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateSetMiterLimitSEL, _gPID)))

#define FPDGraphStateGetLineWidth (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateGetLineWidthSELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateGetLineWidthSEL, _gPID)))

#define FPDGraphStateSetLineWidth (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateSetLineWidthSELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateSetLineWidthSEL, _gPID)))

#define FPDGraphStateIsNull (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateIsNullSELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateIsNullSEL, _gPID)))

#define FPDGraphStateGetModify (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateGetModifySELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateGetModifySEL, _gPID)))

#define FPDGraphStateSetDashArray (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphStateSetDashArraySELPROTO)FRCOREROUTINE(FPDGraphStateSEL,FPDGraphStateSetDashArraySEL, _gPID)))

//*****************************
/* PageObject HFT functions */
//*****************************

#define FPDPageObjectNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectNewSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectNewSEL, _gPID)))

#define FPDPageObjectDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectDestroySELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectDestroySEL, _gPID)))

#define FPDPageObjectClone (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectCloneSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectCloneSEL, _gPID)))

#define FPDPageObjectCopy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectCopySELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectCopySEL, _gPID)))

#define FPDPageObjectRemoveClipPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectRemoveClipPathSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectRemoveClipPathSEL, _gPID)))

#define FPDPageObjectAppendClipPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectAppendClipPathSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectAppendClipPathSEL, _gPID)))

#define FPDPageObjectCopyClipPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectCopyClipPathSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectCopyClipPathSEL, _gPID)))

#define FPDPageObjectTransformClipPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectTransformClipPathSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectTransformClipPathSEL, _gPID)))

#define FPDPageObjectSetColorState (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectSetColorStateSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectSetColorStateSEL, _gPID)))

#define FPDPageObjectGetBBox (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectGetBBoxSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectGetBBoxSEL, _gPID)))

#define FPDPageObjectGetOriginalBBox (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectGetOriginalBBoxSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectGetOriginalBBoxSEL, _gPID)))

#define FPDPageObjectGetType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectGetTypeSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectGetTypeSEL, _gPID)))

#define FPDPageObjectGetClipPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectGetClipPathSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectGetClipPathSEL, _gPID)))

#define FPDPageObjectGetGraphState (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectGetGraphStateSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectGetGraphStateSEL, _gPID)))

#define FPDPageObjectGetColorState (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectGetColorStateSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectGetColorStateSEL, _gPID)))

#define FPDPageObjectGetTextState (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectGetTextStateSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectGetTextStateSEL, _gPID)))

#define FPDPageObjectGetGeneralState (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectGetGeneralStateSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectGetGeneralStateSEL, _gPID)))

#define FPDPageObjectGetContentMark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectGetContentMarkSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectGetContentMarkSEL, _gPID)))

#define FPDPageObjectDefaultStates (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectDefaultStatesSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectDefaultStatesSEL, _gPID)))

#define FPDPageObjectCopyStates (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectCopyStatesSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectCopyStatesSEL, _gPID)))

#define FPDPageObjectSetGraphState (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectSetGraphStateSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectSetGraphStateSEL, _gPID)))

#define FPDPageObjectSetTextState (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectSetTextStateSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectSetTextStateSEL, _gPID)))

#define FPDPageObjectSetGeneralState (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectSetGeneralStateSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectSetGeneralStateSEL, _gPID)))

#define FPDPageObjectHasClipPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectHasClipPathSELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectHasClipPathSEL, _gPID)))

#define FPDPageObjectGetContentMark2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPageObjectGetContentMark2SELPROTO)FRCOREROUTINE(FPDPageObjectSEL,FPDPageObjectGetContentMark2SEL, _gPID)))

//*****************************
/* TextObject HFT functions */
//*****************************

#define FPDTextObjectNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectNewSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectNewSEL, _gPID)))

#define FPDTextObjectDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectDestroySELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectDestroySEL, _gPID)))

#define FPDTextObjectCountItems (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectCountItemsSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectCountItemsSEL, _gPID)))

#define FPDTextObjectGetItemInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectGetItemInfoSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectGetItemInfoSEL, _gPID)))

#define FPDTextObjectCountChars (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectCountCharsSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectCountCharsSEL, _gPID)))

#define FPDTextObjectGetCharInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectGetCharInfoSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectGetCharInfoSEL, _gPID)))

#define FPDTextObjectGetPosX (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectGetPosXSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectGetPosXSEL, _gPID)))

#define FPDTextObjectGetPosY (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectGetPosYSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectGetPosYSEL, _gPID)))

#define FPDTextObjectGetTextMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectGetTextMatrixSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectGetTextMatrixSEL, _gPID)))

#define FPDTextObjectGetFont (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectGetFontSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectGetFontSEL, _gPID)))

#define FPDTextObjectGetFontSize (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectGetFontSizeSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectGetFontSizeSEL, _gPID)))

#define FPDTextObjectSetEmpty (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectSetEmptySELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectSetEmptySEL, _gPID)))

#define FPDTextObjectSetText (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectSetTextSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectSetTextSEL, _gPID)))

#define FPDTextObjectSetText2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectSetText2SELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectSetText2SEL, _gPID)))

#define FPDTextObjectSetText3 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectSetText3SELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectSetText3SEL, _gPID)))

#define FPDTextObjectSetPosition (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectSetPositionSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectSetPositionSEL, _gPID)))

#define FPDTextObjectSetTextState (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectSetTextStateSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectSetTextStateSEL, _gPID)))

#define FPDTextObjectTransform (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectTransformSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectTransformSEL, _gPID)))

#define FPDTextObjectCalcCharPos (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectCalcCharPosSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectCalcCharPosSEL, _gPID)))

#define FPDTextObjectSetData (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectSetDataSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectSetDataSEL, _gPID)))

#define FPDTextObjectGetData (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectGetDataSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectGetDataSEL, _gPID)))

#define FPDTextObjectRecalcPositionData (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectRecalcPositionDataSELPROTO)FRCOREROUTINE(FPDTextObjectSEL,FPDTextObjectRecalcPositionDataSEL, _gPID)))

//*****************************
/* PathObject HFT functions */
//*****************************

#define FPDPathObjectNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectNewSELPROTO)FRCOREROUTINE(FPDPathObjectSEL,FPDPathObjectNewSEL, _gPID)))

#define FPDPathObjectDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectDestroySELPROTO)FRCOREROUTINE(FPDPathObjectSEL,FPDPathObjectDestroySEL, _gPID)))

#define FPDPathObjectTransform (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectTransformSELPROTO)FRCOREROUTINE(FPDPathObjectSEL,FPDPathObjectTransformSEL, _gPID)))

#define FPDPathObjectSetGraphState (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectSetGraphStateSELPROTO)FRCOREROUTINE(FPDPathObjectSEL,FPDPathObjectSetGraphStateSEL, _gPID)))

#define FPDPathObjectCalcBoundingBox (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectCalcBoundingBoxSELPROTO)FRCOREROUTINE(FPDPathObjectSEL,FPDPathObjectCalcBoundingBoxSEL, _gPID)))

#define FPDPathObjectGetTransformMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectGetTransformMatrixSELPROTO)FRCOREROUTINE(FPDPathObjectSEL,FPDPathObjectGetTransformMatrixSEL, _gPID)))

#define FPDPathObjectSetTransformMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectSetTransformMatrixSELPROTO)FRCOREROUTINE(FPDPathObjectSEL,FPDPathObjectSetTransformMatrixSEL, _gPID)))

#define FPDPathObjectGetPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectGetPathSELPROTO)FRCOREROUTINE(FPDPathObjectSEL,FPDPathObjectGetPathSEL, _gPID)))

#define FPDPathObjectIsStrokeMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectIsStrokeModeSELPROTO)FRCOREROUTINE(FPDPathObjectSEL,FPDPathObjectIsStrokeModeSEL, _gPID)))

#define FPDPathObjectSetStrokeMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectSetStrokeModeSELPROTO)FRCOREROUTINE(FPDPathObjectSEL,FPDPathObjectSetStrokeModeSEL, _gPID)))

#define FPDPathObjectGetFillMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectGetFillModeSELPROTO)FRCOREROUTINE(FPDPathObjectSEL,FPDPathObjectGetFillModeSEL, _gPID)))

#define FPDPathObjectSetFillMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectSetFillModeSELPROTO)FRCOREROUTINE(FPDPathObjectSEL,FPDPathObjectSetFillModeSEL, _gPID)))

//*****************************
/* ImageObject HFT functions */
//*****************************

#define FPDImageObjectNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageObjectNewSELPROTO)FRCOREROUTINE(FPDImageObjectSEL,FPDImageObjectNewSEL, _gPID)))

#define FPDImageObjectDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageObjectDestroySELPROTO)FRCOREROUTINE(FPDImageObjectSEL,FPDImageObjectDestroySEL, _gPID)))

#define FPDImageObjectTransform (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageObjectTransformSELPROTO)FRCOREROUTINE(FPDImageObjectSEL,FPDImageObjectTransformSEL, _gPID)))

#define FPDImageObjectCalcBoundingBox (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageObjectCalcBoundingBoxSELPROTO)FRCOREROUTINE(FPDImageObjectSEL,FPDImageObjectCalcBoundingBoxSEL, _gPID)))

#define FPDImageObjectGetTransformMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageObjectGetTransformMatrixSELPROTO)FRCOREROUTINE(FPDImageObjectSEL,FPDImageObjectGetTransformMatrixSEL, _gPID)))

#define FPDImageObjectSetTransformMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageObjectSetTransformMatrixSELPROTO)FRCOREROUTINE(FPDImageObjectSEL,FPDImageObjectSetTransformMatrixSEL, _gPID)))

#define FPDImageObjectGetImage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageObjectGetImageSELPROTO)FRCOREROUTINE(FPDImageObjectSEL,FPDImageObjectGetImageSEL, _gPID)))

#define FPDImageObjectSetImage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageObjectSetImageSELPROTO)FRCOREROUTINE(FPDImageObjectSEL,FPDImageObjectSetImageSEL, _gPID)))

//*****************************
/* ShadingObject HFT functions */
//*****************************

#define FPDShadingObjectNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectNewSELPROTO)FRCOREROUTINE(FPDShadingObjectSEL,FPDShadingObjectNewSEL, _gPID)))

#define FPDShadingObjectDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectDestroySELPROTO)FRCOREROUTINE(FPDShadingObjectSEL,FPDShadingObjectDestroySEL, _gPID)))

#define FPDShadingObjectTransform (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectTransformSELPROTO)FRCOREROUTINE(FPDShadingObjectSEL,FPDShadingObjectTransformSEL, _gPID)))

#define FPDShadingObjectCalcBoundingBox (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectCalcBoundingBoxSELPROTO)FRCOREROUTINE(FPDShadingObjectSEL,FPDShadingObjectCalcBoundingBoxSEL, _gPID)))

#define FPDShadingObjectGetTransformMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectGetTransformMatrixSELPROTO)FRCOREROUTINE(FPDShadingObjectSEL,FPDShadingObjectGetTransformMatrixSEL, _gPID)))

#define FPDShadingObjectSetTransformMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectSetTransformMatrixSELPROTO)FRCOREROUTINE(FPDShadingObjectSEL,FPDShadingObjectSetTransformMatrixSEL, _gPID)))

#define FPDShadingObjectGetPage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectGetPageSELPROTO)FRCOREROUTINE(FPDShadingObjectSEL,FPDShadingObjectGetPageSEL, _gPID)))

#define FPDShadingObjectSetPage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectSetPageSELPROTO)FRCOREROUTINE(FPDShadingObjectSEL,FPDShadingObjectSetPageSEL, _gPID)))

#define FPDShadingObjectGetShadingPattern (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectGetShadingPatternSELPROTO)FRCOREROUTINE(FPDShadingObjectSEL,FPDShadingObjectGetShadingPatternSEL, _gPID)))

#define FPDShadingObjectSetShadingPattern (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectSetShadingPatternSELPROTO)FRCOREROUTINE(FPDShadingObjectSEL,FPDShadingObjectSetShadingPatternSEL, _gPID)))

//*****************************
/* FormObject HFT functions */
//*****************************

#define FPDFormObjectNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDFormObjectNewSELPROTO)FRCOREROUTINE(FPDFormObjectSEL,FPDFormObjectNewSEL, _gPID)))

#define FPDFormObjectDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDFormObjectDestroySELPROTO)FRCOREROUTINE(FPDFormObjectSEL,FPDFormObjectDestroySEL, _gPID)))

#define FPDFormObjectTransform (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDFormObjectTransformSELPROTO)FRCOREROUTINE(FPDFormObjectSEL,FPDFormObjectTransformSEL, _gPID)))

#define FPDFormObjectCalcBoundingBox (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDFormObjectCalcBoundingBoxSELPROTO)FRCOREROUTINE(FPDFormObjectSEL,FPDFormObjectCalcBoundingBoxSEL, _gPID)))

#define FPDFormObjectGetTransformMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDFormObjectGetTransformMatrixSELPROTO)FRCOREROUTINE(FPDFormObjectSEL,FPDFormObjectGetTransformMatrixSEL, _gPID)))

#define FPDFormObjectSetTransformMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDFormObjectSetTransformMatrixSELPROTO)FRCOREROUTINE(FPDFormObjectSEL,FPDFormObjectSetTransformMatrixSEL, _gPID)))

#define FPDFormObjectGetForm (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDFormObjectGetFormSELPROTO)FRCOREROUTINE(FPDFormObjectSEL,FPDFormObjectGetFormSEL, _gPID)))

#define FPDFormObjectSetForm (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDFormObjectSetFormSELPROTO)FRCOREROUTINE(FPDFormObjectSEL,FPDFormObjectSetFormSEL, _gPID)))

//*****************************
/* InlineImages HFT functions */
//*****************************

#define FPDInlineImagesNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDInlineImagesNewSELPROTO)FRCOREROUTINE(FPDInlineImagesSEL,FPDInlineImagesNewSEL, _gPID)))

#define FPDInlineImagesDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDInlineImagesDestroySELPROTO)FRCOREROUTINE(FPDInlineImagesSEL,FPDInlineImagesDestroySEL, _gPID)))

#define FPDInlineImagesAddMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDInlineImagesAddMatrixSELPROTO)FRCOREROUTINE(FPDInlineImagesSEL,FPDInlineImagesAddMatrixSEL, _gPID)))

#define FPDInlineImagesCountMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDInlineImagesCountMatrixSELPROTO)FRCOREROUTINE(FPDInlineImagesSEL,FPDInlineImagesCountMatrixSEL, _gPID)))

#define FPDInlineImagesGetMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDInlineImagesGetMatrixSELPROTO)FRCOREROUTINE(FPDInlineImagesSEL,FPDInlineImagesGetMatrixSEL, _gPID)))

#define FPDInlineImagesGetStream (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDInlineImagesGetStreamSELPROTO)FRCOREROUTINE(FPDInlineImagesSEL,FPDInlineImagesGetStreamSEL, _gPID)))

#define FPDInlineImagesSetStream (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDInlineImagesSetStreamSELPROTO)FRCOREROUTINE(FPDInlineImagesSEL,FPDInlineImagesSetStreamSEL, _gPID)))

//*****************************
/* ContentMarkItem HFT functions */
//*****************************

#define FPDContentMarkItemNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkItemNewSELPROTO)FRCOREROUTINE(FPDContentMarkItemSEL,FPDContentMarkItemNewSEL, _gPID)))

#define FPDContentMarkItemDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkItemDestroySELPROTO)FRCOREROUTINE(FPDContentMarkItemSEL,FPDContentMarkItemDestroySEL, _gPID)))

#define FPDContentMarkItemGetName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkItemGetNameSELPROTO)FRCOREROUTINE(FPDContentMarkItemSEL,FPDContentMarkItemGetNameSEL, _gPID)))

#define FPDContentMarkItemGetParamType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkItemGetParamTypeSELPROTO)FRCOREROUTINE(FPDContentMarkItemSEL,FPDContentMarkItemGetParamTypeSEL, _gPID)))

#define FPDContentMarkItemGetParam (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkItemGetParamSELPROTO)FRCOREROUTINE(FPDContentMarkItemSEL,FPDContentMarkItemGetParamSEL, _gPID)))

#define FPDContentMarkItemSetName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkItemSetNameSELPROTO)FRCOREROUTINE(FPDContentMarkItemSEL,FPDContentMarkItemSetNameSEL, _gPID)))

#define FPDContentMarkItemSetParam (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkItemSetParamSELPROTO)FRCOREROUTINE(FPDContentMarkItemSEL,FPDContentMarkItemSetParamSEL, _gPID)))

//*****************************
/* ContentMark HFT functions */
//*****************************

#define FPDContentMarkNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkNewSELPROTO)FRCOREROUTINE(FPDContentMarkSEL,FPDContentMarkNewSEL, _gPID)))

#define FPDContentMarkDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkDestroySELPROTO)FRCOREROUTINE(FPDContentMarkSEL,FPDContentMarkDestroySEL, _gPID)))

#define FPDContentMarkGetMCID (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkGetMCIDSELPROTO)FRCOREROUTINE(FPDContentMarkSEL,FPDContentMarkGetMCIDSEL, _gPID)))

#define FPDContentMarkHasMark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkHasMarkSELPROTO)FRCOREROUTINE(FPDContentMarkSEL,FPDContentMarkHasMarkSEL, _gPID)))

#define FPDContentMarkLookupMark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkLookupMarkSELPROTO)FRCOREROUTINE(FPDContentMarkSEL,FPDContentMarkLookupMarkSEL, _gPID)))

#define FPDContentMarkCountItems (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkCountItemsSELPROTO)FRCOREROUTINE(FPDContentMarkSEL,FPDContentMarkCountItemsSEL, _gPID)))

#define FPDContentMarkGetItem (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkGetItemSELPROTO)FRCOREROUTINE(FPDContentMarkSEL,FPDContentMarkGetItemSEL, _gPID)))

#define FPDContentMarkAddMark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkAddMarkSELPROTO)FRCOREROUTINE(FPDContentMarkSEL,FPDContentMarkAddMarkSEL, _gPID)))

#define FPDContentMarkDeleteMark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkDeleteMarkSELPROTO)FRCOREROUTINE(FPDContentMarkSEL,FPDContentMarkDeleteMarkSEL, _gPID)))

#define FPDContentMarkDeleteLastMark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkDeleteLastMarkSELPROTO)FRCOREROUTINE(FPDContentMarkSEL,FPDContentMarkDeleteLastMarkSEL, _gPID)))

#define FPDContentMarkIsNull (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkIsNullSELPROTO)FRCOREROUTINE(FPDContentMarkSEL,FPDContentMarkIsNullSEL, _gPID)))

#define FPDContentMarkCopy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDContentMarkCopySELPROTO)FRCOREROUTINE(FPDContentMarkSEL,FPDContentMarkCopySEL, _gPID)))

//----------_V2----------
//----------_V3----------
//----------_V4----------
//----------_V5----------
//----------_V6----------
//----------_V7----------
//----------_V8----------
//----------_V9----------
//----------_V10----------
//----------_V11----------
//----------_V12----------
//----------_V13----------
//*****************************
/* PathObjectUtils HFT functions */
//*****************************

#define FPDPathObjectUtilsGetLineWidth (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectUtilsGetLineWidthSELPROTO)FRCOREROUTINE(FPDPathObjectUtilsSEL,FPDPathObjectUtilsGetLineWidthSEL, _gPID)))

#define FPDPathObjectUtilsSetLineWidth (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectUtilsSetLineWidthSELPROTO)FRCOREROUTINE(FPDPathObjectUtilsSEL,FPDPathObjectUtilsSetLineWidthSEL, _gPID)))

#define FPDPathObjectUtilsSetMiterLimit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectUtilsSetMiterLimitSELPROTO)FRCOREROUTINE(FPDPathObjectUtilsSEL,FPDPathObjectUtilsSetMiterLimitSEL, _gPID)))

#define FPDPathObjectUtilsGetMiterLimit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectUtilsGetMiterLimitSELPROTO)FRCOREROUTINE(FPDPathObjectUtilsSEL,FPDPathObjectUtilsGetMiterLimitSEL, _gPID)))

#define FPDPathObjectUtilsSetLineCap (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectUtilsSetLineCapSELPROTO)FRCOREROUTINE(FPDPathObjectUtilsSEL,FPDPathObjectUtilsSetLineCapSEL, _gPID)))

#define FPDPathObjectUtilsGetLineCap (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectUtilsGetLineCapSELPROTO)FRCOREROUTINE(FPDPathObjectUtilsSEL,FPDPathObjectUtilsGetLineCapSEL, _gPID)))

#define FPDPathObjectUtilsGetLineJoin (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectUtilsGetLineJoinSELPROTO)FRCOREROUTINE(FPDPathObjectUtilsSEL,FPDPathObjectUtilsGetLineJoinSEL, _gPID)))

#define FPDPathObjectUtilsSetLineJoin (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectUtilsSetLineJoinSELPROTO)FRCOREROUTINE(FPDPathObjectUtilsSEL,FPDPathObjectUtilsSetLineJoinSEL, _gPID)))

#define FPDPathObjectUtilsGetLineStyle (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectUtilsGetLineStyleSELPROTO)FRCOREROUTINE(FPDPathObjectUtilsSEL,FPDPathObjectUtilsGetLineStyleSEL, _gPID)))

#define FPDPathObjectUtilsSetLineStyle (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectUtilsSetLineStyleSELPROTO)FRCOREROUTINE(FPDPathObjectUtilsSEL,FPDPathObjectUtilsSetLineStyleSEL, _gPID)))

#define FPDPathObjectUtilsGetStrokeInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectUtilsGetStrokeInfoSELPROTO)FRCOREROUTINE(FPDPathObjectUtilsSEL,FPDPathObjectUtilsGetStrokeInfoSEL, _gPID)))

#define FPDPathObjectUtilsSetStrokeInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectUtilsSetStrokeInfoSELPROTO)FRCOREROUTINE(FPDPathObjectUtilsSEL,FPDPathObjectUtilsSetStrokeInfoSEL, _gPID)))

#define FPDPathObjectUtilsGetFillInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectUtilsGetFillInfoSELPROTO)FRCOREROUTINE(FPDPathObjectUtilsSEL,FPDPathObjectUtilsGetFillInfoSEL, _gPID)))

#define FPDPathObjectUtilsSetFillInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectUtilsSetFillInfoSELPROTO)FRCOREROUTINE(FPDPathObjectUtilsSEL,FPDPathObjectUtilsSetFillInfoSEL, _gPID)))

#define FPDPathObjectUtilsSetColorSpace (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathObjectUtilsSetColorSpaceSELPROTO)FRCOREROUTINE(FPDPathObjectUtilsSEL,FPDPathObjectUtilsSetColorSpaceSEL, _gPID)))

//*****************************
/* ShadingObjectUtils HFT functions */
//*****************************

#define FPDShadingObjectUtilsChangeShadingColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsChangeShadingColorSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsChangeShadingColorSEL, _gPID)))

#define FPDShadingObjectUtilsChangeColorUndoStart (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsChangeColorUndoStartSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsChangeColorUndoStartSEL, _gPID)))

#define FPDShadingObjectUtilsChangeColorUndoEnd (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsChangeColorUndoEndSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsChangeColorUndoEndSEL, _gPID)))

#define FPDShadingObjectUtilsChangeColorUndoCancle (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsChangeColorUndoCancleSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsChangeColorUndoCancleSEL, _gPID)))

#define FPDShadingObjectUtilsEditReferenceLine (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsEditReferenceLineSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsEditReferenceLineSEL, _gPID)))

#define FPDShadingObjectUtilsSetGridientCursorColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsSetGridientCursorColorSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsSetGridientCursorColorSEL, _gPID)))

#define FPDShadingObjectUtilsGetGridientCursorColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsGetGridientCursorColorSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsGetGridientCursorColorSEL, _gPID)))

#define FPDShadingObjectUtilsGetGridientColorAt (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsGetGridientColorAtSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsGetGridientColorAtSEL, _gPID)))

#define FPDShadingObjectUtilsGetGridientCursorLocation (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsGetGridientCursorLocationSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsGetGridientCursorLocationSEL, _gPID)))

#define FPDShadingObjectUtilsGetGridientCursorIndex (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsGetGridientCursorIndexSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsGetGridientCursorIndexSEL, _gPID)))

#define FPDShadingObjectUtilsSetGradientColorShowRect (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsSetGradientColorShowRectSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsSetGradientColorShowRectSEL, _gPID)))

#define FPDShadingObjectUtilsAddGradientCursorInfoBefore (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsAddGradientCursorInfoBeforeSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsAddGradientCursorInfoBeforeSEL, _gPID)))

#define FPDShadingObjectUtilsAddGradientCursorInfoExcute (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsAddGradientCursorInfoExcuteSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsAddGradientCursorInfoExcuteSEL, _gPID)))

#define FPDShadingObjectUtilsAddGradientCursorInfoEnd (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsAddGradientCursorInfoEndSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsAddGradientCursorInfoEndSEL, _gPID)))

#define FPDShadingObjectUtilsAddGradientCursorInfoDblClk (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsAddGradientCursorInfoDblClkSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsAddGradientCursorInfoDblClkSEL, _gPID)))

#define FPDShadingObjectUtilsDelGradientCursorInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsDelGradientCursorInfoSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsDelGradientCursorInfoSEL, _gPID)))

#define FPDShadingObjectUtilsDefaultGradientDataInit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsDefaultGradientDataInitSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsDefaultGradientDataInitSEL, _gPID)))

#define FPDShadingObjectUtilsSetGradientCurrentInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsSetGradientCurrentInfoSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsSetGradientCurrentInfoSEL, _gPID)))

#define FPDShadingObjectUtilsPaintGridient (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingObjectUtilsPaintGridientSELPROTO)FRCOREROUTINE(FPDShadingObjectUtilsSEL,FPDShadingObjectUtilsPaintGridientSEL, _gPID)))

//*****************************
/* PathEditor HFT functions */
//*****************************

#define FPDPathEditorHasStartEdit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathEditorHasStartEditSELPROTO)FRCOREROUTINE(FPDPathEditorSEL,FPDPathEditorHasStartEditSEL, _gPID)))

#define FPDPathEditorIsClipPathMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathEditorIsClipPathModeSELPROTO)FRCOREROUTINE(FPDPathEditorSEL,FPDPathEditorIsClipPathModeSEL, _gPID)))

#define FPDPathEditorAppendSubPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDPathEditorAppendSubPathSELPROTO)FRCOREROUTINE(FPDPathEditorSEL,FPDPathEditorAppendSubPathSEL, _gPID)))

//*****************************
/* ShadingEditor HFT functions */
//*****************************

#define FPDShadingEditorIsReferenceLineWorking (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDShadingEditorIsReferenceLineWorkingSELPROTO)FRCOREROUTINE(FPDShadingEditorSEL,FPDShadingEditorIsReferenceLineWorkingSEL, _gPID)))

//*****************************
/* TextEditor HFT functions */
//*****************************

#define FPDTextEditorOnFontNameChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorOnFontNameChangedSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorOnFontNameChangedSEL, _gPID)))

#define FPDTextEditorOnFontSizeChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorOnFontSizeChangedSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorOnFontSizeChangedSEL, _gPID)))

#define FPDTextEditorOnTextColorChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorOnTextColorChangedSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorOnTextColorChangedSEL, _gPID)))

#define FPDTextEditorOnBoldChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorOnBoldChangedSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorOnBoldChangedSEL, _gPID)))

#define FPDTextEditorOnItalicChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorOnItalicChangedSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorOnItalicChangedSEL, _gPID)))

#define FPDTextEditorOnAlignChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorOnAlignChangedSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorOnAlignChangedSEL, _gPID)))

#define FPDTextEditorOnCharSpaceChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorOnCharSpaceChangedSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorOnCharSpaceChangedSEL, _gPID)))

#define FPDTextEditorOnWordSpaceChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorOnWordSpaceChangedSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorOnWordSpaceChangedSEL, _gPID)))

#define FPDTextEditorOnCharHorzScaleChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorOnCharHorzScaleChangedSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorOnCharHorzScaleChangedSEL, _gPID)))

#define FPDTextEditorOnLineLeadingChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorOnLineLeadingChangedSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorOnLineLeadingChangedSEL, _gPID)))

#define FPDTextEditorOnUnderlineChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorOnUnderlineChangedSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorOnUnderlineChangedSEL, _gPID)))

#define FPDTextEditorOnCrossChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorOnCrossChangedSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorOnCrossChangedSEL, _gPID)))

#define FPDTextEditorOnSuperScriptChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorOnSuperScriptChangedSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorOnSuperScriptChangedSEL, _gPID)))

#define FPDTextEditorOnSubScriptChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorOnSubScriptChangedSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorOnSubScriptChangedSEL, _gPID)))

#define FPDTextEditorOnWritingDirctionChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorOnWritingDirctionChangedSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorOnWritingDirctionChangedSEL, _gPID)))

#define FPDTextEditorIsSelect (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorIsSelectSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorIsSelectSEL, _gPID)))

#define FPDTextEditorCopy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorCopySELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorCopySEL, _gPID)))

#define FPDTextEditorPaste (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorPasteSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorPasteSEL, _gPID)))

#define FPDTextEditorCut (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorCutSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorCutSEL, _gPID)))

#define FPDTextEditorDelete (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorDeleteSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorDeleteSEL, _gPID)))

#define FPDTextEditorSelectAll (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorSelectAllSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorSelectAllSEL, _gPID)))

#define FPDTextEditorIsAdd (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorIsAddSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorIsAddSEL, _gPID)))

#define FPDTextEditorGetICaretFormData (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorGetICaretFormDataSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorGetICaretFormDataSEL, _gPID)))

#define FPDTextEditorGetTextObjectMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextEditorGetTextObjectMatrixSELPROTO)FRCOREROUTINE(FPDTextEditorSEL,FPDTextEditorGetTextObjectMatrixSEL, _gPID)))

//*****************************
/* TextObjectUtils HFT functions */
//*****************************

#define FPDTextObjectUtilsGetFontSize (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsGetFontSizeSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsGetFontSizeSEL, _gPID)))

#define FPDTextObjectUtilsSetFontSize (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsSetFontSizeSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsSetFontSizeSEL, _gPID)))

#define FPDTextObjectUtilsGetHorizontalScale (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsGetHorizontalScaleSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsGetHorizontalScaleSEL, _gPID)))

#define FPDTextObjectUtilsSetHorizontalScale (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsSetHorizontalScaleSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsSetHorizontalScaleSEL, _gPID)))

#define FPDTextObjectUtilsSetCharSpace (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsSetCharSpaceSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsSetCharSpaceSEL, _gPID)))

#define FPDTextObjectUtilsGetCharSpace (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsGetCharSpaceSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsGetCharSpaceSEL, _gPID)))

#define FPDTextObjectUtilsGetWordSpace (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsGetWordSpaceSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsGetWordSpaceSEL, _gPID)))

#define FPDTextObjectUtilsSetWordSpace (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsSetWordSpaceSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsSetWordSpaceSEL, _gPID)))

#define FPDTextObjectUtilsSetFont (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsSetFontSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsSetFontSEL, _gPID)))

#define FPDTextObjectUtilsGetFont (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsGetFontSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsGetFontSEL, _gPID)))

#define FPDTextObjectUtilsGetTextMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsGetTextModeSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsGetTextModeSEL, _gPID)))

#define FPDTextObjectUtilsSetTextMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsSetTextModeSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsSetTextModeSEL, _gPID)))

#define FPDTextObjectUtilsCanRemoveTextKerning (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsCanRemoveTextKerningSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsCanRemoveTextKerningSEL, _gPID)))

#define FPDTextObjectUtilsRemoveTextKerning (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsRemoveTextKerningSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsRemoveTextKerningSEL, _gPID)))

#define FPDTextObjectUtilsCanMergeText (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsCanMergeTextSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsCanMergeTextSEL, _gPID)))

#define FPDTextObjectUtilsMergeText (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsMergeTextSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsMergeTextSEL, _gPID)))

#define FPDTextObjectUtilsCanSplitText (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsCanSplitTextSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsCanSplitTextSEL, _gPID)))

#define FPDTextObjectUtilsSplitText (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsSplitTextSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsSplitTextSEL, _gPID)))

#define FPDTextObjectUtilsCanText2Path (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsCanText2PathSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsCanText2PathSEL, _gPID)))

#define FPDTextObjectUtilsGetStrokeInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsGetStrokeInfoSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsGetStrokeInfoSEL, _gPID)))

#define FPDTextObjectUtilsSetStrokeInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsSetStrokeInfoSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsSetStrokeInfoSEL, _gPID)))

#define FPDTextObjectUtilsGetFillInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsGetFillInfoSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsGetFillInfoSEL, _gPID)))

#define FPDTextObjectUtilsSetFillInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsSetFillInfoSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsSetFillInfoSEL, _gPID)))

#define FPDTextObjectUtilsSetColorSpace (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsSetColorSpaceSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsSetColorSpaceSEL, _gPID)))

#define FPDTextObjectUtilsGetTextColorInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsGetTextColorInfoSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsGetTextColorInfoSEL, _gPID)))

#define FPDTextObjectUtilsIsWordSpaceValid (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsIsWordSpaceValidSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsIsWordSpaceValidSEL, _gPID)))

#define FPDTextObjectUtilsGetFontBoldInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsGetFontBoldInfoSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsGetFontBoldInfoSEL, _gPID)))

#define FPDTextObjectUtilsGetFontItalic (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDTextObjectUtilsGetFontItalicSELPROTO)FRCOREROUTINE(FPDTextObjectUtilsSEL,FPDTextObjectUtilsGetFontItalicSEL, _gPID)))

//*****************************
/* GraphicObject HFT functions */
//*****************************

#define FPDGraphicObjectCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectCreateSELPROTO)FRCOREROUTINE(FPDGraphicObjectSEL,FPDGraphicObjectCreateSEL, _gPID)))

#define FPDGraphicObjectDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectDestroySELPROTO)FRCOREROUTINE(FPDGraphicObjectSEL,FPDGraphicObjectDestroySEL, _gPID)))

#define FPDGraphicObjectGetPage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectGetPageSELPROTO)FRCOREROUTINE(FPDGraphicObjectSEL,FPDGraphicObjectGetPageSEL, _gPID)))

#define FPDGraphicObjectGetGraphicObject (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectGetGraphicObjectSELPROTO)FRCOREROUTINE(FPDGraphicObjectSEL,FPDGraphicObjectGetGraphicObjectSEL, _gPID)))

#define FPDGraphicObjectGetIndex (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectGetIndexSELPROTO)FRCOREROUTINE(FPDGraphicObjectSEL,FPDGraphicObjectGetIndexSEL, _gPID)))

#define FPDGraphicObjectGetPosition (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectGetPositionSELPROTO)FRCOREROUTINE(FPDGraphicObjectSEL,FPDGraphicObjectGetPositionSEL, _gPID)))

#define FPDGraphicObjectIsInForm (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectIsInFormSELPROTO)FRCOREROUTINE(FPDGraphicObjectSEL,FPDGraphicObjectIsInFormSEL, _gPID)))

#define FPDGraphicObjectGetFormIndex (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectGetFormIndexSELPROTO)FRCOREROUTINE(FPDGraphicObjectSEL,FPDGraphicObjectGetFormIndexSEL, _gPID)))

#define FPDGraphicObjectGetFormObjects (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectGetFormObjectsSELPROTO)FRCOREROUTINE(FPDGraphicObjectSEL,FPDGraphicObjectGetFormObjectsSEL, _gPID)))

#define FPDGraphicObjectGetFormMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectGetFormMatrixSELPROTO)FRCOREROUTINE(FPDGraphicObjectSEL,FPDGraphicObjectGetFormMatrixSEL, _gPID)))

//*****************************
/* GraphicEditor HFT functions */
//*****************************

#define FPDGraphicEditorGetType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicEditorGetTypeSELPROTO)FRCOREROUTINE(FPDGraphicEditorSEL,FPDGraphicEditorGetTypeSEL, _gPID)))

#define FPDGraphicEditorEndEdit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicEditorEndEditSELPROTO)FRCOREROUTINE(FPDGraphicEditorSEL,FPDGraphicEditorEndEditSEL, _gPID)))

//*****************************
/* GraphicObjectUtils HFT functions */
//*****************************

#define FPDGraphicObjectUtilsGetHeight (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsGetHeightSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsGetHeightSEL, _gPID)))

#define FPDGraphicObjectUtilsSetHeight (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsSetHeightSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsSetHeightSEL, _gPID)))

#define FPDGraphicObjectUtilsSetWidth (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsSetWidthSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsSetWidthSEL, _gPID)))

#define FPDGraphicObjectUtilsGetWidth (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsGetWidthSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsGetWidthSEL, _gPID)))

#define FPDGraphicObjectUtilsGetXPosition (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsGetXPositionSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsGetXPositionSEL, _gPID)))

#define FPDGraphicObjectUtilsSetXPosition (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsSetXPositionSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsSetXPositionSEL, _gPID)))

#define FPDGraphicObjectUtilsGetYPosition (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsGetYPositionSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsGetYPositionSEL, _gPID)))

#define FPDGraphicObjectUtilsSetYPosition (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsSetYPositionSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsSetYPositionSEL, _gPID)))

#define FPDGraphicObjectUtilsGetOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsGetOpacitySELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsGetOpacitySEL, _gPID)))

#define FPDGraphicObjectUtilsSetOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsSetOpacitySELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsSetOpacitySEL, _gPID)))

#define FPDGraphicObjectUtilsRotate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsRotateSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsRotateSEL, _gPID)))

#define FPDGraphicObjectUtilsFlip (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsFlipSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsFlipSEL, _gPID)))

#define FPDGraphicObjectUtilsShear (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsShearSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsShearSEL, _gPID)))

#define FPDGraphicObjectUtilsMove (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsMoveSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsMoveSEL, _gPID)))

#define FPDGraphicObjectUtilsAlign (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsAlignSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsAlignSEL, _gPID)))

#define FPDGraphicObjectUtilsCenter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsCenterSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsCenterSEL, _gPID)))

#define FPDGraphicObjectUtilsDistribute (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsDistributeSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsDistributeSEL, _gPID)))

#define FPDGraphicObjectUtilsSize (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsSizeSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsSizeSEL, _gPID)))

#define FPDGraphicObjectUtilsScale (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsScaleSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsScaleSEL, _gPID)))

#define FPDGraphicObjectUtilsScaleFloat (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsScaleFloatSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsScaleFloatSEL, _gPID)))

#define FPDGraphicObjectUtilsChangeRenderingOrder (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsChangeRenderingOrderSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsChangeRenderingOrderSEL, _gPID)))

#define FPDGraphicObjectUtilsCanChangeRenderingOrder (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsCanChangeRenderingOrderSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsCanChangeRenderingOrderSEL, _gPID)))

#define FPDGraphicObjectUtilsClearClipPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsClearClipPathSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsClearClipPathSEL, _gPID)))

#define FPDGraphicObjectUtilsEditClipPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsEditClipPathSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsEditClipPathSEL, _gPID)))

#define FPDGraphicObjectUtilsAddClipPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsAddClipPathSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsAddClipPathSEL, _gPID)))

#define FPDGraphicObjectUtilsGetType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsGetTypeSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsGetTypeSEL, _gPID)))

#define FPDGraphicObjectUtilsCut (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsCutSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsCutSEL, _gPID)))

#define FPDGraphicObjectUtilsCopy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsCopySELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsCopySEL, _gPID)))

#define FPDGraphicObjectUtilsDelete (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsDeleteSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsDeleteSEL, _gPID)))

#define FPDGraphicObjectUtilsSavePageArchive (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsSavePageArchiveSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsSavePageArchiveSEL, _gPID)))

#define FPDGraphicObjectUtilsReadPageArchive (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDGraphicObjectUtilsReadPageArchiveSELPROTO)FRCOREROUTINE(FPDGraphicObjectUtilsSEL,FPDGraphicObjectUtilsReadPageArchiveSEL, _gPID)))

//*****************************
/* ImageEditor HFT functions */
//*****************************

#define FPDImageEditorSaveAsImage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorSaveAsImageSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorSaveAsImageSEL, _gPID)))

#define FPDImageEditorPreviewFilter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorPreviewFilterSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorPreviewFilterSEL, _gPID)))

#define FPDImageEditorCancelPreviewFilter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorCancelPreviewFilterSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorCancelPreviewFilterSEL, _gPID)))

#define FPDImageEditorAdjustFilter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorAdjustFilterSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorAdjustFilterSEL, _gPID)))

#define FPDImageEditorRotateVertically (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorRotateVerticallySELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorRotateVerticallySEL, _gPID)))

#define FPDImageEditorRotateHorizontally (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorRotateHorizontallySELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorRotateHorizontallySEL, _gPID)))

#define FPDImageEditorReplace (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorReplaceSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorReplaceSEL, _gPID)))

#define FPDImageEditorCopy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorCopySELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorCopySEL, _gPID)))

#define FPDImageEditorPaste (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorPasteSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorPasteSEL, _gPID)))

#define FPDImageEditorDeleteRegion (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorDeleteRegionSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorDeleteRegionSEL, _gPID)))

#define FPDImageEditorZoomIn (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorZoomInSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorZoomInSEL, _gPID)))

#define FPDImageEditorZoomTo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorZoomToSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorZoomToSEL, _gPID)))

#define FPDImageEditorZoomOut (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorZoomOutSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorZoomOutSEL, _gPID)))

#define FPDImageEditorAdjustColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorAdjustColorSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorAdjustColorSEL, _gPID)))

#define FPDImageEditorLoadFromClipboard (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorLoadFromClipboardSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorLoadFromClipboardSEL, _gPID)))

#define FPDImageEditorOnClientRectChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorOnClientRectChangedSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorOnClientRectChangedSEL, _gPID)))

#define FPDImageEditorSetActionType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorSetActionTypeSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorSetActionTypeSEL, _gPID)))

#define FPDImageEditorGetActionType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorGetActionTypeSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorGetActionTypeSEL, _gPID)))

#define FPDImageEditorOnSetCursor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorOnSetCursorSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorOnSetCursorSEL, _gPID)))

#define FPDImageEditorOnSize (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorOnSizeSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorOnSizeSEL, _gPID)))

#define FPDImageEditorGetCurrentOptionData (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorGetCurrentOptionDataSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorGetCurrentOptionDataSEL, _gPID)))

#define FPDImageEditorGetPublicOptionData (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorGetPublicOptionDataSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorGetPublicOptionDataSEL, _gPID)))

#define FPDImageEditorGetImageHistogramData (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorGetImageHistogramDataSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorGetImageHistogramDataSEL, _gPID)))

#define FPDImageEditorCompositeImageObject (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorCompositeImageObjectSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorCompositeImageObjectSEL, _gPID)))

#define FPDImageEditorCancelEdit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDImageEditorCancelEditSELPROTO)FRCOREROUTINE(FPDImageEditorSEL,FPDImageEditorCancelEditSEL, _gPID)))

//*****************************
/* IPageEditor HFT functions */
//*****************************

#define FPDIPageEditorStartEdit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorStartEditSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorStartEditSEL, _gPID)))

#define FPDIPageEditorRestartEdit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorRestartEditSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorRestartEditSEL, _gPID)))

#define FPDIPageEditorEndEdit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorEndEditSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorEndEditSEL, _gPID)))

#define FPDIPageEditorHasSelection (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorHasSelectionSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorHasSelectionSEL, _gPID)))

#define FPDIPageEditorCountSelection (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorCountSelectionSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorCountSelectionSEL, _gPID)))

#define FPDIPageEditorGetSelection (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorGetSelectionSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorGetSelectionSEL, _gPID)))

#define FPDIPageEditorHaveFillColorInSelection (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorHaveFillColorInSelectionSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorHaveFillColorInSelectionSEL, _gPID)))

#define FPDIPageEditorGetActivePage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorGetActivePageSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorGetActivePageSEL, _gPID)))

#define FPDIPageEditorSetObjectFilter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorSetObjectFilterSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorSetObjectFilterSEL, _gPID)))

#define FPDIPageEditorOnVKUP (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorOnVKUPSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorOnVKUPSEL, _gPID)))

#define FPDIPageEditorOnVKDOWN (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorOnVKDOWNSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorOnVKDOWNSEL, _gPID)))

#define FPDIPageEditorOnLeftButtonDown (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorOnLeftButtonDownSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorOnLeftButtonDownSEL, _gPID)))

#define FPDIPageEditorOnLeftButtonUp (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorOnLeftButtonUpSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorOnLeftButtonUpSEL, _gPID)))

#define FPDIPageEditorOnLeftButtonDblClk (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorOnLeftButtonDblClkSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorOnLeftButtonDblClkSEL, _gPID)))

#define FPDIPageEditorOnMouseMove (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorOnMouseMoveSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorOnMouseMoveSEL, _gPID)))

#define FPDIPageEditorOnMouseWheel (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorOnMouseWheelSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorOnMouseWheelSEL, _gPID)))

#define FPDIPageEditorOnRightButtonDown (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorOnRightButtonDownSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorOnRightButtonDownSEL, _gPID)))

#define FPDIPageEditorOnRightButtonUp (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorOnRightButtonUpSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorOnRightButtonUpSEL, _gPID)))

#define FPDIPageEditorOnReleaseCapture (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorOnReleaseCaptureSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorOnReleaseCaptureSEL, _gPID)))

#define FPDIPageEditorOnPaint (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorOnPaintSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorOnPaintSEL, _gPID)))

#define FPDIPageEditorGetObjectAtPoint (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorGetObjectAtPointSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorGetObjectAtPointSEL, _gPID)))

#define FPDIPageEditorCanPaste (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorCanPasteSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorCanPasteSEL, _gPID)))

#define FPDIPageEditorPaste (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorPasteSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorPasteSEL, _gPID)))

#define FPDIPageEditorSelectAll (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorSelectAllSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorSelectAllSEL, _gPID)))

#define FPDIPageEditorGetGraphicEditor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorGetGraphicEditorSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorGetGraphicEditorSEL, _gPID)))

#define FPDIPageEditorGetGraphicObjectUtils (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorGetGraphicObjectUtilsSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorGetGraphicObjectUtilsSEL, _gPID)))

#define FPDIPageEditorCreatePath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorCreatePathSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorCreatePathSEL, _gPID)))

#define FPDIPageEditorCreateShading (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorCreateShadingSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorCreateShadingSEL, _gPID)))

#define FPDIPageEditorCreateImage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorCreateImageSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorCreateImageSEL, _gPID)))

#define FPDIPageEditorCreateText (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorCreateTextSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorCreateTextSEL, _gPID)))

#define FPDIPageEditorCreatePageEditor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorCreatePageEditorSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorCreatePageEditorSEL, _gPID)))

#define FPDIPageEditorDestroyPageEditor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorDestroyPageEditorSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorDestroyPageEditorSEL, _gPID)))

#define FPDIPageEditorClearSelection (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorClearSelectionSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorClearSelectionSEL, _gPID)))

#define FPDIPageEditorText2Path (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorText2PathSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorText2PathSEL, _gPID)))

#define FPDIPageEditorOnChar (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorOnCharSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorOnCharSEL, _gPID)))

#define FPDIPageEditorEditGraphicObject (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorEditGraphicObjectSELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorEditGraphicObjectSEL, _gPID)))

#define FPDIPageEditorCreateImage2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPageEditorCreateImage2SELPROTO)FRCOREROUTINE(FPDIPageEditorSEL,FPDIPageEditorCreateImage2SEL, _gPID)))

//*****************************
/* IUndoItem HFT functions */
//*****************************

#define FPDIUndoItemOnUndo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIUndoItemOnUndoSELPROTO)FRCOREROUTINE(FPDIUndoItemSEL,FPDIUndoItemOnUndoSEL, _gPID)))

#define FPDIUndoItemOnRedo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIUndoItemOnRedoSELPROTO)FRCOREROUTINE(FPDIUndoItemSEL,FPDIUndoItemOnRedoSEL, _gPID)))

#define FPDIUndoItemOnRelease (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIUndoItemOnReleaseSELPROTO)FRCOREROUTINE(FPDIUndoItemSEL,FPDIUndoItemOnReleaseSEL, _gPID)))

//*****************************
/* IClipboard HFT functions */
//*****************************

#define FPDIClipboardNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIClipboardNewSELPROTO)FRCOREROUTINE(FPDIClipboardSEL,FPDIClipboardNewSEL, _gPID)))

#define FPDIClipboardDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIClipboardDestroySELPROTO)FRCOREROUTINE(FPDIClipboardSEL,FPDIClipboardDestroySEL, _gPID)))

//*****************************
/* IPopupMenu HFT functions */
//*****************************

#define FPDIPopupMenuNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPopupMenuNewSELPROTO)FRCOREROUTINE(FPDIPopupMenuSEL,FPDIPopupMenuNewSEL, _gPID)))

#define FPDIPopupMenuDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPopupMenuDestroySELPROTO)FRCOREROUTINE(FPDIPopupMenuSEL,FPDIPopupMenuDestroySEL, _gPID)))

//*****************************
/* ITip HFT functions */
//*****************************

#define FPDITipNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITipNewSELPROTO)FRCOREROUTINE(FPDITipSEL,FPDITipNewSEL, _gPID)))

#define FPDITipDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITipDestroySELPROTO)FRCOREROUTINE(FPDITipSEL,FPDITipDestroySEL, _gPID)))

//*****************************
/* IOperationNotify HFT functions */
//*****************************

#define FPDIOperationNotifyNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIOperationNotifyNewSELPROTO)FRCOREROUTINE(FPDIOperationNotifySEL,FPDIOperationNotifyNewSEL, _gPID)))

#define FPDIOperationNotifyDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIOperationNotifyDestroySELPROTO)FRCOREROUTINE(FPDIOperationNotifySEL,FPDIOperationNotifyDestroySEL, _gPID)))

//*****************************
/* IPublicOptionData HFT functions */
//*****************************

#define FPDIPublicOptionDataSetForegroundColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPublicOptionDataSetForegroundColorSELPROTO)FRCOREROUTINE(FPDIPublicOptionDataSEL,FPDIPublicOptionDataSetForegroundColorSEL, _gPID)))

#define FPDIPublicOptionDataGetForegroundColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPublicOptionDataGetForegroundColorSELPROTO)FRCOREROUTINE(FPDIPublicOptionDataSEL,FPDIPublicOptionDataGetForegroundColorSEL, _gPID)))

#define FPDIPublicOptionDataSetBackgroundColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPublicOptionDataSetBackgroundColorSELPROTO)FRCOREROUTINE(FPDIPublicOptionDataSEL,FPDIPublicOptionDataSetBackgroundColorSEL, _gPID)))

#define FPDIPublicOptionDataGetBackgroundColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPublicOptionDataGetBackgroundColorSELPROTO)FRCOREROUTINE(FPDIPublicOptionDataSEL,FPDIPublicOptionDataGetBackgroundColorSEL, _gPID)))

#define FPDIPublicOptionDataSetLayerOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPublicOptionDataSetLayerOpacitySELPROTO)FRCOREROUTINE(FPDIPublicOptionDataSEL,FPDIPublicOptionDataSetLayerOpacitySEL, _gPID)))

#define FPDIPublicOptionDataGetLayerOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPublicOptionDataGetLayerOpacitySELPROTO)FRCOREROUTINE(FPDIPublicOptionDataSEL,FPDIPublicOptionDataGetLayerOpacitySEL, _gPID)))

#define FPDIPublicOptionDataSetLayerFill (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPublicOptionDataSetLayerFillSELPROTO)FRCOREROUTINE(FPDIPublicOptionDataSEL,FPDIPublicOptionDataSetLayerFillSEL, _gPID)))

#define FPDIPublicOptionDataGetLayerFill (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPublicOptionDataGetLayerFillSELPROTO)FRCOREROUTINE(FPDIPublicOptionDataSEL,FPDIPublicOptionDataGetLayerFillSEL, _gPID)))

#define FPDIPublicOptionDataSetLayerBlendMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPublicOptionDataSetLayerBlendModeSELPROTO)FRCOREROUTINE(FPDIPublicOptionDataSEL,FPDIPublicOptionDataSetLayerBlendModeSEL, _gPID)))

#define FPDIPublicOptionDataGetLayerBlendMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPublicOptionDataGetLayerBlendModeSELPROTO)FRCOREROUTINE(FPDIPublicOptionDataSEL,FPDIPublicOptionDataGetLayerBlendModeSEL, _gPID)))

//*****************************
/* IBaseBrushOptionData HFT functions */
//*****************************

#define FPDIBaseBrushOptionDataSetBrushDiameter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBaseBrushOptionDataSetBrushDiameterSELPROTO)FRCOREROUTINE(FPDIBaseBrushOptionDataSEL,FPDIBaseBrushOptionDataSetBrushDiameterSEL, _gPID)))

#define FPDIBaseBrushOptionDataGetBrushDiameter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBaseBrushOptionDataGetBrushDiameterSELPROTO)FRCOREROUTINE(FPDIBaseBrushOptionDataSEL,FPDIBaseBrushOptionDataGetBrushDiameterSEL, _gPID)))

#define FPDIBaseBrushOptionDataSetBrushHardness (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBaseBrushOptionDataSetBrushHardnessSELPROTO)FRCOREROUTINE(FPDIBaseBrushOptionDataSEL,FPDIBaseBrushOptionDataSetBrushHardnessSEL, _gPID)))

#define FPDIBaseBrushOptionDataGetBrushHardness (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBaseBrushOptionDataGetBrushHardnessSELPROTO)FRCOREROUTINE(FPDIBaseBrushOptionDataSEL,FPDIBaseBrushOptionDataGetBrushHardnessSEL, _gPID)))

//*****************************
/* IBrushOptionData HFT functions */
//*****************************

#define FPDIBrushOptionDataSetBlendMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBrushOptionDataSetBlendModeSELPROTO)FRCOREROUTINE(FPDIBrushOptionDataSEL,FPDIBrushOptionDataSetBlendModeSEL, _gPID)))

#define FPDIBrushOptionDataGetBlendMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBrushOptionDataGetBlendModeSELPROTO)FRCOREROUTINE(FPDIBrushOptionDataSEL,FPDIBrushOptionDataGetBlendModeSEL, _gPID)))

#define FPDIBrushOptionDataSetFlow (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBrushOptionDataSetFlowSELPROTO)FRCOREROUTINE(FPDIBrushOptionDataSEL,FPDIBrushOptionDataSetFlowSEL, _gPID)))

#define FPDIBrushOptionDataGetFlow (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBrushOptionDataGetFlowSELPROTO)FRCOREROUTINE(FPDIBrushOptionDataSEL,FPDIBrushOptionDataGetFlowSEL, _gPID)))

#define FPDIBrushOptionDataSetOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBrushOptionDataSetOpacitySELPROTO)FRCOREROUTINE(FPDIBrushOptionDataSEL,FPDIBrushOptionDataSetOpacitySEL, _gPID)))

#define FPDIBrushOptionDataGetOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBrushOptionDataGetOpacitySELPROTO)FRCOREROUTINE(FPDIBrushOptionDataSEL,FPDIBrushOptionDataGetOpacitySEL, _gPID)))

#define FPDIBrushOptionDataSetBaseBrushDiameter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBrushOptionDataSetBaseBrushDiameterSELPROTO)FRCOREROUTINE(FPDIBrushOptionDataSEL,FPDIBrushOptionDataSetBaseBrushDiameterSEL, _gPID)))

#define FPDIBrushOptionDataGetBaseBrushDiameter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBrushOptionDataGetBaseBrushDiameterSELPROTO)FRCOREROUTINE(FPDIBrushOptionDataSEL,FPDIBrushOptionDataGetBaseBrushDiameterSEL, _gPID)))

#define FPDIBrushOptionDataSetBaseBrushHardness (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBrushOptionDataSetBaseBrushHardnessSELPROTO)FRCOREROUTINE(FPDIBrushOptionDataSEL,FPDIBrushOptionDataSetBaseBrushHardnessSEL, _gPID)))

#define FPDIBrushOptionDataGetBaseBrushHardness (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBrushOptionDataGetBaseBrushHardnessSELPROTO)FRCOREROUTINE(FPDIBrushOptionDataSEL,FPDIBrushOptionDataGetBaseBrushHardnessSEL, _gPID)))

//*****************************
/* IEraserOptionData HFT functions */
//*****************************

#define FPDIEraserOptionDataSetEraserMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIEraserOptionDataSetEraserModeSELPROTO)FRCOREROUTINE(FPDIEraserOptionDataSEL,FPDIEraserOptionDataSetEraserModeSEL, _gPID)))

#define FPDIEraserOptionDataGetEraserMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIEraserOptionDataGetEraserModeSELPROTO)FRCOREROUTINE(FPDIEraserOptionDataSEL,FPDIEraserOptionDataGetEraserModeSEL, _gPID)))

#define FPDIEraserOptionDataSetFlow (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIEraserOptionDataSetFlowSELPROTO)FRCOREROUTINE(FPDIEraserOptionDataSEL,FPDIEraserOptionDataSetFlowSEL, _gPID)))

#define FPDIEraserOptionDataGetFlow (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIEraserOptionDataGetFlowSELPROTO)FRCOREROUTINE(FPDIEraserOptionDataSEL,FPDIEraserOptionDataGetFlowSEL, _gPID)))

#define FPDIEraserOptionDataSetOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIEraserOptionDataSetOpacitySELPROTO)FRCOREROUTINE(FPDIEraserOptionDataSEL,FPDIEraserOptionDataSetOpacitySEL, _gPID)))

#define FPDIEraserOptionDataGetOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIEraserOptionDataGetOpacitySELPROTO)FRCOREROUTINE(FPDIEraserOptionDataSEL,FPDIEraserOptionDataGetOpacitySEL, _gPID)))

#define FPDIEraserOptionDataSetEraserHistory (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIEraserOptionDataSetEraserHistorySELPROTO)FRCOREROUTINE(FPDIEraserOptionDataSEL,FPDIEraserOptionDataSetEraserHistorySEL, _gPID)))

#define FPDIEraserOptionDataGetEraserHistory (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIEraserOptionDataGetEraserHistorySELPROTO)FRCOREROUTINE(FPDIEraserOptionDataSEL,FPDIEraserOptionDataGetEraserHistorySEL, _gPID)))

#define FPDIEraserOptionDataSetBaseBrushDiameter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIEraserOptionDataSetBaseBrushDiameterSELPROTO)FRCOREROUTINE(FPDIEraserOptionDataSEL,FPDIEraserOptionDataSetBaseBrushDiameterSEL, _gPID)))

#define FPDIEraserOptionDataGetBaseBrushDiameter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIEraserOptionDataGetBaseBrushDiameterSELPROTO)FRCOREROUTINE(FPDIEraserOptionDataSEL,FPDIEraserOptionDataGetBaseBrushDiameterSEL, _gPID)))

#define FPDIEraserOptionDataSetBaseBrushHardness (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIEraserOptionDataSetBaseBrushHardnessSELPROTO)FRCOREROUTINE(FPDIEraserOptionDataSEL,FPDIEraserOptionDataSetBaseBrushHardnessSEL, _gPID)))

#define FPDIEraserOptionDataGetBaseBrushHardness (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIEraserOptionDataGetBaseBrushHardnessSELPROTO)FRCOREROUTINE(FPDIEraserOptionDataSEL,FPDIEraserOptionDataGetBaseBrushHardnessSEL, _gPID)))

//*****************************
/* IMagicWandOptionData HFT functions */
//*****************************

#define FPDIMagicWandOptionDataSetTolerance (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIMagicWandOptionDataSetToleranceSELPROTO)FRCOREROUTINE(FPDIMagicWandOptionDataSEL,FPDIMagicWandOptionDataSetToleranceSEL, _gPID)))

#define FPDIMagicWandOptionDataGetTolerance (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIMagicWandOptionDataGetToleranceSELPROTO)FRCOREROUTINE(FPDIMagicWandOptionDataSEL,FPDIMagicWandOptionDataGetToleranceSEL, _gPID)))

#define FPDIMagicWandOptionDataSetIsContinuous (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIMagicWandOptionDataSetIsContinuousSELPROTO)FRCOREROUTINE(FPDIMagicWandOptionDataSEL,FPDIMagicWandOptionDataSetIsContinuousSEL, _gPID)))

#define FPDIMagicWandOptionDataGetIsContinuous (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIMagicWandOptionDataGetIsContinuousSELPROTO)FRCOREROUTINE(FPDIMagicWandOptionDataSEL,FPDIMagicWandOptionDataGetIsContinuousSEL, _gPID)))

//*****************************
/* IDodgeOptionData HFT functions */
//*****************************

#define FPDIDodgeOptionDataSetDodgeMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIDodgeOptionDataSetDodgeModeSELPROTO)FRCOREROUTINE(FPDIDodgeOptionDataSEL,FPDIDodgeOptionDataSetDodgeModeSEL, _gPID)))

#define FPDIDodgeOptionDataGetDodgeMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIDodgeOptionDataGetDodgeModeSELPROTO)FRCOREROUTINE(FPDIDodgeOptionDataSEL,FPDIDodgeOptionDataGetDodgeModeSEL, _gPID)))

#define FPDIDodgeOptionDataSetFlow (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIDodgeOptionDataSetFlowSELPROTO)FRCOREROUTINE(FPDIDodgeOptionDataSEL,FPDIDodgeOptionDataSetFlowSEL, _gPID)))

#define FPDIDodgeOptionDataGetFlow (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIDodgeOptionDataGetFlowSELPROTO)FRCOREROUTINE(FPDIDodgeOptionDataSEL,FPDIDodgeOptionDataGetFlowSEL, _gPID)))

#define FPDIDodgeOptionDataSetOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIDodgeOptionDataSetOpacitySELPROTO)FRCOREROUTINE(FPDIDodgeOptionDataSEL,FPDIDodgeOptionDataSetOpacitySEL, _gPID)))

#define FPDIDodgeOptionDataGetOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIDodgeOptionDataGetOpacitySELPROTO)FRCOREROUTINE(FPDIDodgeOptionDataSEL,FPDIDodgeOptionDataGetOpacitySEL, _gPID)))

#define FPDIDodgeOptionDataSetBaseBrushDiameter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIDodgeOptionDataSetBaseBrushDiameterSELPROTO)FRCOREROUTINE(FPDIDodgeOptionDataSEL,FPDIDodgeOptionDataSetBaseBrushDiameterSEL, _gPID)))

#define FPDIDodgeOptionDataGetBaseBrushDiameter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIDodgeOptionDataGetBaseBrushDiameterSELPROTO)FRCOREROUTINE(FPDIDodgeOptionDataSEL,FPDIDodgeOptionDataGetBaseBrushDiameterSEL, _gPID)))

#define FPDIDodgeOptionDataSetBaseBrushHardness (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIDodgeOptionDataSetBaseBrushHardnessSELPROTO)FRCOREROUTINE(FPDIDodgeOptionDataSEL,FPDIDodgeOptionDataSetBaseBrushHardnessSEL, _gPID)))

#define FPDIDodgeOptionDataGetBaseBrushHardness (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIDodgeOptionDataGetBaseBrushHardnessSELPROTO)FRCOREROUTINE(FPDIDodgeOptionDataSEL,FPDIDodgeOptionDataGetBaseBrushHardnessSEL, _gPID)))

//*****************************
/* IBurnOptionData HFT functions */
//*****************************

#define FPDIBurnOptionDataSetFlow (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBurnOptionDataSetFlowSELPROTO)FRCOREROUTINE(FPDIBurnOptionDataSEL,FPDIBurnOptionDataSetFlowSEL, _gPID)))

#define FPDIBurnOptionDataGetFlow (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBurnOptionDataGetFlowSELPROTO)FRCOREROUTINE(FPDIBurnOptionDataSEL,FPDIBurnOptionDataGetFlowSEL, _gPID)))

#define FPDIBurnOptionDataSetOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBurnOptionDataSetOpacitySELPROTO)FRCOREROUTINE(FPDIBurnOptionDataSEL,FPDIBurnOptionDataSetOpacitySEL, _gPID)))

#define FPDIBurnOptionDataGetOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBurnOptionDataGetOpacitySELPROTO)FRCOREROUTINE(FPDIBurnOptionDataSEL,FPDIBurnOptionDataGetOpacitySEL, _gPID)))

#define FPDIBurnOptionDataSetBurnMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBurnOptionDataSetBurnModeSELPROTO)FRCOREROUTINE(FPDIBurnOptionDataSEL,FPDIBurnOptionDataSetBurnModeSEL, _gPID)))

#define FPDIBurnOptionDataGetBurnMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBurnOptionDataGetBurnModeSELPROTO)FRCOREROUTINE(FPDIBurnOptionDataSEL,FPDIBurnOptionDataGetBurnModeSEL, _gPID)))

#define FPDIBurnOptionDataSetBaseBrushDiameter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBurnOptionDataSetBaseBrushDiameterSELPROTO)FRCOREROUTINE(FPDIBurnOptionDataSEL,FPDIBurnOptionDataSetBaseBrushDiameterSEL, _gPID)))

#define FPDIBurnOptionDataGetBaseBrushDiameter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBurnOptionDataGetBaseBrushDiameterSELPROTO)FRCOREROUTINE(FPDIBurnOptionDataSEL,FPDIBurnOptionDataGetBaseBrushDiameterSEL, _gPID)))

#define FPDIBurnOptionDataSetBaseBrushHardness (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBurnOptionDataSetBaseBrushHardnessSELPROTO)FRCOREROUTINE(FPDIBurnOptionDataSEL,FPDIBurnOptionDataSetBaseBrushHardnessSEL, _gPID)))

#define FPDIBurnOptionDataGetBaseBrushHardness (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIBurnOptionDataGetBaseBrushHardnessSELPROTO)FRCOREROUTINE(FPDIBurnOptionDataSEL,FPDIBurnOptionDataGetBaseBrushHardnessSEL, _gPID)))

//*****************************
/* IEyedropperData HFT functions */
//*****************************

#define FPDIEyedropperDataGetSampleType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIEyedropperDataGetSampleTypeSELPROTO)FRCOREROUTINE(FPDIEyedropperDataSEL,FPDIEyedropperDataGetSampleTypeSEL, _gPID)))

#define FPDIEyedropperDataSetSampleType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIEyedropperDataSetSampleTypeSELPROTO)FRCOREROUTINE(FPDIEyedropperDataSEL,FPDIEyedropperDataSetSampleTypeSEL, _gPID)))

//*****************************
/* ICloneStampData HFT functions */
//*****************************

#define FPDICloneStampDataSetBlendMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDICloneStampDataSetBlendModeSELPROTO)FRCOREROUTINE(FPDICloneStampDataSEL,FPDICloneStampDataSetBlendModeSEL, _gPID)))

#define FPDICloneStampDataGetBlendMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDICloneStampDataGetBlendModeSELPROTO)FRCOREROUTINE(FPDICloneStampDataSEL,FPDICloneStampDataGetBlendModeSEL, _gPID)))

#define FPDICloneStampDataSetFlow (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDICloneStampDataSetFlowSELPROTO)FRCOREROUTINE(FPDICloneStampDataSEL,FPDICloneStampDataSetFlowSEL, _gPID)))

#define FPDICloneStampDataGetFlow (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDICloneStampDataGetFlowSELPROTO)FRCOREROUTINE(FPDICloneStampDataSEL,FPDICloneStampDataGetFlowSEL, _gPID)))

#define FPDICloneStampDataSetOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDICloneStampDataSetOpacitySELPROTO)FRCOREROUTINE(FPDICloneStampDataSEL,FPDICloneStampDataSetOpacitySEL, _gPID)))

#define FPDICloneStampDataGetOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDICloneStampDataGetOpacitySELPROTO)FRCOREROUTINE(FPDICloneStampDataSEL,FPDICloneStampDataGetOpacitySEL, _gPID)))

#define FPDICloneStampDataSetIsAlineCheck (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDICloneStampDataSetIsAlineCheckSELPROTO)FRCOREROUTINE(FPDICloneStampDataSEL,FPDICloneStampDataSetIsAlineCheckSEL, _gPID)))

#define FPDICloneStampDataGetIsAlineCheck (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDICloneStampDataGetIsAlineCheckSELPROTO)FRCOREROUTINE(FPDICloneStampDataSEL,FPDICloneStampDataGetIsAlineCheckSEL, _gPID)))

#define FPDICloneStampDataSetBaseBrushDiameter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDICloneStampDataSetBaseBrushDiameterSELPROTO)FRCOREROUTINE(FPDICloneStampDataSEL,FPDICloneStampDataSetBaseBrushDiameterSEL, _gPID)))

#define FPDICloneStampDataGetBaseBrushDiameter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDICloneStampDataGetBaseBrushDiameterSELPROTO)FRCOREROUTINE(FPDICloneStampDataSEL,FPDICloneStampDataGetBaseBrushDiameterSEL, _gPID)))

#define FPDICloneStampDataSetBaseBrushHardness (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDICloneStampDataSetBaseBrushHardnessSELPROTO)FRCOREROUTINE(FPDICloneStampDataSEL,FPDICloneStampDataSetBaseBrushHardnessSEL, _gPID)))

#define FPDICloneStampDataGetBaseBrushHardness (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDICloneStampDataGetBaseBrushHardnessSELPROTO)FRCOREROUTINE(FPDICloneStampDataSEL,FPDICloneStampDataGetBaseBrushHardnessSEL, _gPID)))

//*****************************
/* IPaintBucketOptionData HFT functions */
//*****************************

#define FPDIPaintBucketOptionDataSetTolerance (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPaintBucketOptionDataSetToleranceSELPROTO)FRCOREROUTINE(FPDIPaintBucketOptionDataSEL,FPDIPaintBucketOptionDataSetToleranceSEL, _gPID)))

#define FPDIPaintBucketOptionDataGetTolerance (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPaintBucketOptionDataGetToleranceSELPROTO)FRCOREROUTINE(FPDIPaintBucketOptionDataSEL,FPDIPaintBucketOptionDataGetToleranceSEL, _gPID)))

#define FPDIPaintBucketOptionDataSetOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPaintBucketOptionDataSetOpacitySELPROTO)FRCOREROUTINE(FPDIPaintBucketOptionDataSEL,FPDIPaintBucketOptionDataSetOpacitySEL, _gPID)))

#define FPDIPaintBucketOptionDataGetOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPaintBucketOptionDataGetOpacitySELPROTO)FRCOREROUTINE(FPDIPaintBucketOptionDataSEL,FPDIPaintBucketOptionDataGetOpacitySEL, _gPID)))

#define FPDIPaintBucketOptionDataSetColorBlendMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPaintBucketOptionDataSetColorBlendModeSELPROTO)FRCOREROUTINE(FPDIPaintBucketOptionDataSEL,FPDIPaintBucketOptionDataSetColorBlendModeSEL, _gPID)))

#define FPDIPaintBucketOptionDataGetColorBlendMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPaintBucketOptionDataGetColorBlendModeSELPROTO)FRCOREROUTINE(FPDIPaintBucketOptionDataSEL,FPDIPaintBucketOptionDataGetColorBlendModeSEL, _gPID)))

#define FPDIPaintBucketOptionDataSetContinuous (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPaintBucketOptionDataSetContinuousSELPROTO)FRCOREROUTINE(FPDIPaintBucketOptionDataSEL,FPDIPaintBucketOptionDataSetContinuousSEL, _gPID)))

#define FPDIPaintBucketOptionDataGetContinuous (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIPaintBucketOptionDataGetContinuousSELPROTO)FRCOREROUTINE(FPDIPaintBucketOptionDataSEL,FPDIPaintBucketOptionDataGetContinuousSEL, _gPID)))

//*****************************
/* ISpotHealingBrushData HFT functions */
//*****************************

#define FPDISpotHealingBrushDataGetDiameter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDISpotHealingBrushDataGetDiameterSELPROTO)FRCOREROUTINE(FPDISpotHealingBrushDataSEL,FPDISpotHealingBrushDataGetDiameterSEL, _gPID)))

#define FPDISpotHealingBrushDataSetDiameter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDISpotHealingBrushDataSetDiameterSELPROTO)FRCOREROUTINE(FPDISpotHealingBrushDataSEL,FPDISpotHealingBrushDataSetDiameterSEL, _gPID)))

#define FPDISpotHealingBrushDataGetRoundness (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDISpotHealingBrushDataGetRoundnessSELPROTO)FRCOREROUTINE(FPDISpotHealingBrushDataSEL,FPDISpotHealingBrushDataGetRoundnessSEL, _gPID)))

#define FPDISpotHealingBrushDataSetRoundness (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDISpotHealingBrushDataSetRoundnessSELPROTO)FRCOREROUTINE(FPDISpotHealingBrushDataSEL,FPDISpotHealingBrushDataSetRoundnessSEL, _gPID)))

#define FPDISpotHealingBrushDataGetAline (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDISpotHealingBrushDataGetAlineSELPROTO)FRCOREROUTINE(FPDISpotHealingBrushDataSEL,FPDISpotHealingBrushDataGetAlineSEL, _gPID)))

#define FPDISpotHealingBrushDataSetAline (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDISpotHealingBrushDataSetAlineSELPROTO)FRCOREROUTINE(FPDISpotHealingBrushDataSEL,FPDISpotHealingBrushDataSetAlineSEL, _gPID)))

#define FPDISpotHealingBrushDataGetMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDISpotHealingBrushDataGetModeSELPROTO)FRCOREROUTINE(FPDISpotHealingBrushDataSEL,FPDISpotHealingBrushDataGetModeSEL, _gPID)))

#define FPDISpotHealingBrushDataSetMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDISpotHealingBrushDataSetModeSELPROTO)FRCOREROUTINE(FPDISpotHealingBrushDataSEL,FPDISpotHealingBrushDataSetModeSEL, _gPID)))

//*****************************
/* CEditObject HFT functions */
//*****************************

#define FPDCEditObjectNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDCEditObjectNewSELPROTO)FRCOREROUTINE(FPDCEditObjectSEL,FPDCEditObjectNewSEL, _gPID)))

#define FPDCEditObjectNew2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDCEditObjectNew2SELPROTO)FRCOREROUTINE(FPDCEditObjectSEL,FPDCEditObjectNew2SEL, _gPID)))

#define FPDCEditObjectDelete (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDCEditObjectDeleteSELPROTO)FRCOREROUTINE(FPDCEditObjectSEL,FPDCEditObjectDeleteSEL, _gPID)))

#define FPDCEditObjectCombineObjct (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDCEditObjectCombineObjctSELPROTO)FRCOREROUTINE(FPDCEditObjectSEL,FPDCEditObjectCombineObjctSEL, _gPID)))

#define FPDCEditObjectIsEqual (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDCEditObjectIsEqualSELPROTO)FRCOREROUTINE(FPDCEditObjectSEL,FPDCEditObjectIsEqualSEL, _gPID)))

#define FPDCEditObjectCopyBaseInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDCEditObjectCopyBaseInfoSELPROTO)FRCOREROUTINE(FPDCEditObjectSEL,FPDCEditObjectCopyBaseInfoSEL, _gPID)))

#define FPDCEditObjectUpdateFormInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDCEditObjectUpdateFormInfoSELPROTO)FRCOREROUTINE(FPDCEditObjectSEL,FPDCEditObjectUpdateFormInfoSEL, _gPID)))

#define FPDCEditObjectUpdateFormInfo2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDCEditObjectUpdateFormInfo2SELPROTO)FRCOREROUTINE(FPDCEditObjectSEL,FPDCEditObjectUpdateFormInfo2SEL, _gPID)))

#define FPDCEditObjectGetLastFormObj (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDCEditObjectGetLastFormObjSELPROTO)FRCOREROUTINE(FPDCEditObjectSEL,FPDCEditObjectGetLastFormObjSEL, _gPID)))

#define FPDCEditObjectGetFirstFormObj (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDCEditObjectGetFirstFormObjSELPROTO)FRCOREROUTINE(FPDCEditObjectSEL,FPDCEditObjectGetFirstFormObjSEL, _gPID)))

#define FPDCEditObjectGetContainer (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDCEditObjectGetContainerSELPROTO)FRCOREROUTINE(FPDCEditObjectSEL,FPDCEditObjectGetContainerSEL, _gPID)))

#define FPDCEditObjectGetObjBBox (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDCEditObjectGetObjBBoxSELPROTO)FRCOREROUTINE(FPDCEditObjectSEL,FPDCEditObjectGetObjBBoxSEL, _gPID)))

#define FPDCEditObjectReset (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDCEditObjectResetSELPROTO)FRCOREROUTINE(FPDCEditObjectSEL,FPDCEditObjectResetSEL, _gPID)))

#define FPDCEditObjectUndoRedoState (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDCEditObjectUndoRedoStateSELPROTO)FRCOREROUTINE(FPDCEditObjectSEL,FPDCEditObjectUndoRedoStateSEL, _gPID)))

#define FPDCEditObjectMergeTextObjRange (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDCEditObjectMergeTextObjRangeSELPROTO)FRCOREROUTINE(FPDCEditObjectSEL,FPDCEditObjectMergeTextObjRangeSEL, _gPID)))

#define FPDCEditObjectMergeTextObjRange2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDCEditObjectMergeTextObjRange2SELPROTO)FRCOREROUTINE(FPDCEditObjectSEL,FPDCEditObjectMergeTextObjRange2SEL, _gPID)))

//*****************************
/* IJoinSplit HFT functions */
//*****************************

#define FPDIJoinSplitExitJoinEditing (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitExitJoinEditingSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitExitJoinEditingSEL, _gPID)))

#define FPDIJoinSplitOnActivate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitOnActivateSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitOnActivateSEL, _gPID)))

#define FPDIJoinSplitOnDeactivate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitOnDeactivateSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitOnDeactivateSEL, _gPID)))

#define FPDIJoinSplitOnPaint (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitOnPaintSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitOnPaintSEL, _gPID)))

#define FPDIJoinSplitOnLeftButtonDown (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitOnLeftButtonDownSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitOnLeftButtonDownSEL, _gPID)))

#define FPDIJoinSplitOnLeftButtonUp (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitOnLeftButtonUpSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitOnLeftButtonUpSEL, _gPID)))

#define FPDIJoinSplitOnMouseMove (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitOnMouseMoveSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitOnMouseMoveSEL, _gPID)))

#define FPDIJoinSplitOnRightButtonUp (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitOnRightButtonUpSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitOnRightButtonUpSEL, _gPID)))

#define FPDIJoinSplitIsProcessing (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitIsProcessingSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitIsProcessingSEL, _gPID)))

#define FPDIJoinSplitBtnEnableStatus (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitBtnEnableStatusSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitBtnEnableStatusSEL, _gPID)))

#define FPDIJoinSplitJoinBoxes (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitJoinBoxesSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitJoinBoxesSEL, _gPID)))

#define FPDIJoinSplitSplitBoxes (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitSplitBoxesSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitSplitBoxesSEL, _gPID)))

#define FPDIJoinSplitLinkBoxes (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitLinkBoxesSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitLinkBoxesSEL, _gPID)))

#define FPDIJoinSplitUnlinkBoxes (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitUnlinkBoxesSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitUnlinkBoxesSEL, _gPID)))

#define FPDIJoinSplitSelectNone (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitSelectNoneSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitSelectNoneSEL, _gPID)))

#define FPDIJoinSplitOnAfterInsertPages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitOnAfterInsertPagesSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitOnAfterInsertPagesSEL, _gPID)))

#define FPDIJoinSplitOnAfterDeletePages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitOnAfterDeletePagesSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitOnAfterDeletePagesSEL, _gPID)))

#define FPDIJoinSplitOnAfterReplacePages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitOnAfterReplacePagesSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitOnAfterReplacePagesSEL, _gPID)))

#define FPDIJoinSplitOnAfterResizePage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitOnAfterResizePageSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitOnAfterResizePageSEL, _gPID)))

#define FPDIJoinSplitOnAfterRotatePage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitOnAfterRotatePageSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitOnAfterRotatePageSEL, _gPID)))

#define FPDIJoinSplitOnAfterMovePages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDIJoinSplitOnAfterMovePagesSELPROTO)FRCOREROUTINE(FPDIJoinSplitSEL,FPDIJoinSplitOnAfterMovePagesSEL, _gPID)))

//*****************************
/* ITouchup HFT functions */
//*****************************

#define FPDITouchupOnActivate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnActivateSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnActivateSEL, _gPID)))

#define FPDITouchupOnDeactivate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnDeactivateSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnDeactivateSEL, _gPID)))

#define FPDITouchupExistEditing (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupExistEditingSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupExistEditingSEL, _gPID)))

#define FPDITouchupOnPaint (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnPaintSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnPaintSEL, _gPID)))

#define FPDITouchupOnChar (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnCharSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnCharSEL, _gPID)))

#define FPDITouchupOnVKUP (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnVKUPSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnVKUPSEL, _gPID)))

#define FPDITouchupOnVKDOWN (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnVKDOWNSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnVKDOWNSEL, _gPID)))

#define FPDITouchupOnLeftButtonDown (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnLeftButtonDownSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnLeftButtonDownSEL, _gPID)))

#define FPDITouchupOnLeftButtonUp (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnLeftButtonUpSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnLeftButtonUpSEL, _gPID)))

#define FPDITouchupOnLeftButtonDblClk (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnLeftButtonDblClkSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnLeftButtonDblClkSEL, _gPID)))

#define FPDITouchupOnMouseMove (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnMouseMoveSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnMouseMoveSEL, _gPID)))

#define FPDITouchupOnMouseWheel (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnMouseWheelSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnMouseWheelSEL, _gPID)))

#define FPDITouchupOnRightButtonDown (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnRightButtonDownSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnRightButtonDownSEL, _gPID)))

#define FPDITouchupOnRightButtonUp (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnRightButtonUpSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnRightButtonUpSEL, _gPID)))

#define FPDITouchupCanSelectAll (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupCanSelectAllSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupCanSelectAllSEL, _gPID)))

#define FPDITouchupDoSelectAll (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupDoSelectAllSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupDoSelectAllSEL, _gPID)))

#define FPDITouchupCanDelete (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupCanDeleteSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupCanDeleteSEL, _gPID)))

#define FPDITouchupDoDelete (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupDoDeleteSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupDoDeleteSEL, _gPID)))

#define FPDITouchupCanCopy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupCanCopySELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupCanCopySEL, _gPID)))

#define FPDITouchupDoCopy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupDoCopySELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupDoCopySEL, _gPID)))

#define FPDITouchupCanCut (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupCanCutSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupCanCutSEL, _gPID)))

#define FPDITouchupDoCut (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupDoCutSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupDoCutSEL, _gPID)))

#define FPDITouchupCanPaste (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupCanPasteSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupCanPasteSEL, _gPID)))

#define FPDITouchupDoPaste (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupDoPasteSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupDoPasteSEL, _gPID)))

#define FPDITouchupCanDeselectAll (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupCanDeselectAllSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupCanDeselectAllSEL, _gPID)))

#define FPDITouchupDoDeselectAll (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupDoDeselectAllSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupDoDeselectAllSEL, _gPID)))

#define FPDITouchupOnFontNameChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnFontNameChangedSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnFontNameChangedSEL, _gPID)))

#define FPDITouchupOnFontSizeChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnFontSizeChangedSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnFontSizeChangedSEL, _gPID)))

#define FPDITouchupOnTextColorChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnTextColorChangedSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnTextColorChangedSEL, _gPID)))

#define FPDITouchupOnBoldItlicChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnBoldItlicChangedSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnBoldItlicChangedSEL, _gPID)))

#define FPDITouchupOnAlignChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnAlignChangedSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnAlignChangedSEL, _gPID)))

#define FPDITouchupOnCharSpaceChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnCharSpaceChangedSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnCharSpaceChangedSEL, _gPID)))

#define FPDITouchupOnCharHorzScaleChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnCharHorzScaleChangedSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnCharHorzScaleChangedSEL, _gPID)))

#define FPDITouchupOnLineLeadingChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnLineLeadingChangedSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnLineLeadingChangedSEL, _gPID)))

#define FPDITouchupOnUnderlineChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnUnderlineChangedSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnUnderlineChangedSEL, _gPID)))

#define FPDITouchupOnCrossChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnCrossChangedSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnCrossChangedSEL, _gPID)))

#define FPDITouchupOnSuperScriptChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnSuperScriptChangedSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnSuperScriptChangedSEL, _gPID)))

#define FPDITouchupOnSubScriptChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnSubScriptChangedSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnSubScriptChangedSEL, _gPID)))

#define FPDITouchupOnParagraphSpacingChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnParagraphSpacingChangedSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnParagraphSpacingChangedSEL, _gPID)))

#define FPDITouchupOnInDentChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnInDentChangedSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnInDentChangedSEL, _gPID)))

#define FPDITouchupOnDeDentChanged (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnDeDentChangedSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnDeDentChangedSEL, _gPID)))

#define FPDITouchupOnAfterInsertPages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnAfterInsertPagesSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnAfterInsertPagesSEL, _gPID)))

#define FPDITouchupOnAfterDeletePages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnAfterDeletePagesSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnAfterDeletePagesSEL, _gPID)))

#define FPDITouchupOnAfterReplacePages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnAfterReplacePagesSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnAfterReplacePagesSEL, _gPID)))

#define FPDITouchupOnAfterResizePage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnAfterResizePageSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnAfterResizePageSEL, _gPID)))

#define FPDITouchupOnAfterRotatePage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnAfterRotatePageSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnAfterRotatePageSEL, _gPID)))

#define FPDITouchupOnAfterMovePages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnAfterMovePagesSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnAfterMovePagesSEL, _gPID)))

#define FPDITouchupOnPageContentChange (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupOnPageContentChangeSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupOnPageContentChangeSEL, _gPID)))

#define FPDITouchupGetFXEditor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupGetFXEditorSELPROTO)FRCOREROUTINE(FPDITouchupSEL,FPDITouchupGetFXEditorSEL, _gPID)))

//*****************************
/* ITouchupManager HFT functions */
//*****************************

#define FPDITouchupManagerCreateManager (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupManagerCreateManagerSELPROTO)FRCOREROUTINE(FPDITouchupManagerSEL,FPDITouchupManagerCreateManagerSEL, _gPID)))

#define FPDITouchupManagerGetTouchup (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupManagerGetTouchupSELPROTO)FRCOREROUTINE(FPDITouchupManagerSEL,FPDITouchupManagerGetTouchupSEL, _gPID)))

#define FPDITouchupManagerGetJoinSpilt (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchupManagerGetJoinSpiltSELPROTO)FRCOREROUTINE(FPDITouchupManagerSEL,FPDITouchupManagerGetJoinSpiltSEL, _gPID)))

//*****************************
/* ITouchUndoItem HFT functions */
//*****************************

#define FPDITouchUndoItemOnUndo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchUndoItemOnUndoSELPROTO)FRCOREROUTINE(FPDITouchUndoItemSEL,FPDITouchUndoItemOnUndoSEL, _gPID)))

#define FPDITouchUndoItemOnRedo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchUndoItemOnRedoSELPROTO)FRCOREROUTINE(FPDITouchUndoItemSEL,FPDITouchUndoItemOnRedoSEL, _gPID)))

#define FPDITouchUndoItemOnRelease (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchUndoItemOnReleaseSELPROTO)FRCOREROUTINE(FPDITouchUndoItemSEL,FPDITouchUndoItemOnReleaseSEL, _gPID)))

//*****************************
/* ITouchClipboard HFT functions */
//*****************************

#define FPDITouchClipboardNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchClipboardNewSELPROTO)FRCOREROUTINE(FPDITouchClipboardSEL,FPDITouchClipboardNewSEL, _gPID)))

#define FPDITouchClipboardDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchClipboardDestroySELPROTO)FRCOREROUTINE(FPDITouchClipboardSEL,FPDITouchClipboardDestroySEL, _gPID)))

//*****************************
/* ITouchPopupMenu HFT functions */
//*****************************

#define FPDITouchPopupMenuNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchPopupMenuNewSELPROTO)FRCOREROUTINE(FPDITouchPopupMenuSEL,FPDITouchPopupMenuNewSEL, _gPID)))

#define FPDITouchPopupMenuDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchPopupMenuDestroySELPROTO)FRCOREROUTINE(FPDITouchPopupMenuSEL,FPDITouchPopupMenuDestroySEL, _gPID)))

//*****************************
/* ITouchProgressBar HFT functions */
//*****************************

#define FPDITouchProgressBarNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchProgressBarNewSELPROTO)FRCOREROUTINE(FPDITouchProgressBarSEL,FPDITouchProgressBarNewSEL, _gPID)))

#define FPDITouchProgressBarDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchProgressBarDestroySELPROTO)FRCOREROUTINE(FPDITouchProgressBarSEL,FPDITouchProgressBarDestroySEL, _gPID)))

//*****************************
/* ITouchOperationNotify HFT functions */
//*****************************

#define FPDITouchOperationNotifyNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchOperationNotifyNewSELPROTO)FRCOREROUTINE(FPDITouchOperationNotifySEL,FPDITouchOperationNotifyNewSEL, _gPID)))

#define FPDITouchOperationNotifyDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchOperationNotifyDestroySELPROTO)FRCOREROUTINE(FPDITouchOperationNotifySEL,FPDITouchOperationNotifyDestroySEL, _gPID)))

//*****************************
/* ITouchUndoHandler HFT functions */
//*****************************

#define FPDITouchUndoHandlerNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchUndoHandlerNewSELPROTO)FRCOREROUTINE(FPDITouchUndoHandlerSEL,FPDITouchUndoHandlerNewSEL, _gPID)))

#define FPDITouchUndoHandlerDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchUndoHandlerDestroySELPROTO)FRCOREROUTINE(FPDITouchUndoHandlerSEL,FPDITouchUndoHandlerDestroySEL, _gPID)))

//*****************************
/* ITouchTextFormatHandler HFT functions */
//*****************************

#define FPDITouchTextFormatHandlerNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchTextFormatHandlerNewSELPROTO)FRCOREROUTINE(FPDITouchTextFormatHandlerSEL,FPDITouchTextFormatHandlerNewSEL, _gPID)))

#define FPDITouchTextFormatHandlerDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchTextFormatHandlerDestroySELPROTO)FRCOREROUTINE(FPDITouchTextFormatHandlerSEL,FPDITouchTextFormatHandlerDestroySEL, _gPID)))

//*****************************
/* ITouchProvider HFT functions */
//*****************************

#define FPDITouchProviderNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchProviderNewSELPROTO)FRCOREROUTINE(FPDITouchProviderSEL,FPDITouchProviderNewSEL, _gPID)))

#define FPDITouchProviderDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FPDITouchProviderDestroySELPROTO)FRCOREROUTINE(FPDITouchProviderSEL,FPDITouchProviderDestroySEL, _gPID)))

#ifdef __cplusplus
}
#endif

#endif