﻿/*********************************************************************

 Copyright (C) 2010 Foxit Corporation
 All rights reserved.
  
 NOTICE: Foxit permits you to use, modify, and distribute this file
 in accordance with the terms of the Foxit license agreement
 accompanying it. If you have received this file from a source other
 than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
	
 ---------------------------------------------------------------------
	  
fpd_pageobjExpT.h
		
 - Types, macros, structures, etc. required to use the FPDPageObj HFT.

*********************************************************************/

/**
 * @defgroup FPDLayer Foxit Portable Document Layer
 */

/*@{*/

/**
 * @addtogroup FPDPAGEOBJ
 * @{
 */

/**
 * @file
 * @brief FPD Page Object
 */

#ifndef FPD_PAGEOBJEXPT_H
#define FPD_PAGEOBJEXPT_H

#ifndef FS_COMMON_H
#include "../basic/fs_common.h"
#endif

#ifndef FS_BASICEXPT_H
#include "../basic/fs_basicExpT.h"
#endif

#ifndef FS_STRINGEXPT_H
#include "../basic/fs_stringExpT.h"
#endif

#ifdef __cplusplus
extern "C"{
#endif

/* @OBJSTART FPD_Path */
#ifndef FPD_PATH
#define FPD_PATH
/** @brief FPD path. 
 * A <a>FPD_Path</a> is a data container for a <a>FPD_PathObject</a> which is a graphic object representing a path in a page description. 
 * See <a>FPDPathNew</a>, <a>FPDPathDestroy</a>.
 */
typedef struct _t_FPD_Path* FPD_Path;
#endif
/* @OBJEND */

/* @OBJSTART FPD_ClipPath */
#ifndef FPD_CLIPPATH
#define FPD_CLIPPATH
/** @brief A clip path. See <a>FPDClipPathNew</a>, <a>FPDClipPathDestroy</a>.
 */
typedef struct _t_FPD_ClipPath* FPD_ClipPath;
#endif
/* @OBJEND */

/* @OBJSTART FPD_ColorState */
#ifndef FPD_COLORSTATE
#define FPD_COLORSTATE
/** @brief The color state for rendering. See <a>FPDColorStateNew</a>, <a>FPDColorStateDestroy</a>.
 */
typedef struct _t_FPD_ColorState* FPD_ColorState;
#endif
/* @OBJEND */

/* @OBJSTART FPD_GraphState */
#ifndef FPD_GRAPHSTATE
#define FPD_GRAPHSTATE
/**
 * @brief The graph state for graphic rendering.
 */
typedef struct _t_FPD_GraphState* FPD_GraphState;

/* @ENUMSTART */
/** @brief  Line cap style enumeration.
 */
enum FPD_LineCap
{
	FPD_LineCapButt=0, /**< Butt cap. The stroke is squared off at the endpoint of the path. */
	FPD_LineCapRound=1, /**< Round cap. A semicircular arc with a diameter equal to the line width is drawn around the endpoint and filled in. */ 
	FPD_LineCapSquare=2 /**< Projecting square cap. The stroke continues beyond the endpoint of the path for a distance equal to half the line width and is squared off. */	
};
/* @ENUMEND */

/* @ENUMSTART */
/** @brief Line join style enumeration.
 */

enum FPD_LineJoin
{
	FPD_LineJoinMiter=0, /**< Miter join. The outer edges of the strokes for the two segments are extended until they meet at an angle, as in a picture frame. */
	FPD_LineJoinRound=1,/**< Round join. An arc of a circle with a diameter equal to the line width is drawn around the point where the two segments meet, connecting the outer edges of the strokes for the two segments. */
	FPD_LineJoinBevel=2 /**< Bevel join. The two segments are finished with butt caps and the resulting notch beyond the ends of the segments is filled with a triangle. */

};
/* @ENUMEND */

#endif
/* @OBJEND */

/* @OBJSTART FPD_TextState */
#ifndef FPD_TEXTSTATE
#define FPD_TEXTSTATE
/** @brief The text state for page rendering. See <a>FPDTextStateNew</a>, <a>FPDTextStateDestroy</a>.
 */
typedef struct _t_FPD_TextState* FPD_TextState;
#endif
/* @OBJEND */

/* @OBJSTART FPD_GeneralState */
#ifndef FPD_GENERALSTATE
#define FPD_GENERALSTATE
/** @brief The general state for page rendering. See <a>FPDGeneralStateNew</a>, <a>FPDGeneralStateDestroy</a>.
 */
typedef struct _t_FPD_GeneralState* FPD_GeneralState;
#endif
/* @OBJEND */

/* @OBJSTART FPD_ContentMarkItem */
#ifndef FPD_CONTENTMARKITEM
#define FPD_CONTENTMARKITEM
/** @brief The content mark item for tagged pdf. See <a>FPDContentMarkItemNew</a>, <a>FPDContentMarkItemDestroy</a>.
 */
typedef struct _t_FPD_ContentMarkItem* FPD_ContentMarkItem;
#endif

/* @ENUMSTART */
/** @brief Parameter type enumeration of content mark item. See <a>FPDContentMarkItemGetParamType</a>, <a>FPDContentMarkItemSetParam</a>.
 */
enum FPD_MarkItemParamType
{
	NoneItem,			/**< None parameters. */
	PropertiesDict,		/**< The dictionary defined by named resource in the Properties sub-dictionary of the current resource dictionary. */
	DirectDict,			/**< The dictionary may be written inline in the content stream as a direct object. */
	MCID				/**< The dictionary contains an MCID entry, which is an integer marked-content identifier that uniquely identifies the marked-content sequence within its content stream. */
};
/* @ENUMEND */
/* @OBJEND */

/* @OBJSTART FPD_ContentMark */
#ifndef FPD_CONTENTMARK
#define FPD_CONTENTMARK
/** @brief The content marks for tagged pdf. See <a>FPDContentMarkNew</a>, <a>FPDContentMarkDestroy</a>.
 */
typedef struct _t_FPD_ContentMark* FPD_ContentMark;
#endif
/* @OBJEND */

/* @OBJSTART FPD_PageObject */
#ifndef FPD_PAGEOBJECT
#define FPD_PAGEOBJECT
/** 
 * @brief A <a>FPD_PageObject</a> is the abstract superclass of page objects. 
 *
 * You can find the type of any object 
 * with the <a>FPDPageObjectGetType</a>() method.
 *
 */
typedef struct _t_FPD_PageObject* FPD_PageObject;
#endif

/* @DEFGROUPSTART FPDPathPointFlags */

/**
 * @name Point flags used in FPD_Path.
 */
/*@{*/

/**
 * @brief Specifies that the figure is automatically closed after the <a>FPDPT_LINETO</a> or <a>FPDPT_BEZIERTO</a>
 * for this point is done. A line is drawn from this point to the most recent #FPDPT_MOVETO point.
 */
#define FPDPT_CLOSEFIGURE		0x01
/** @brief Specifies that a line is to be drawn from the current position to this point, which then becomes the new current position. */
#define FPDPT_LINETO				0x02
/** @brief Specifies that this point is a control point or ending point for a Bézier spline. */
#define FPDPT_BEZIERTO			0x04
/** @brief Specifies that this point starts a figure. This point becomes the new current position. */
#define FPDPT_MOVETO				0x06
/** @brief Denotes point type mask. Point types are: #FPDPT_MOVETO/#FPDPT_LINETO/#FPDPT_BEZIERTO. */
#define FPDPT_TYPE				0x06

/*@}*/
/* @DEFGROUPEND */

/* @DEFGROUPSTART FPDPageObjectTypes */
/**
* @name The types of page objects.
*/
/*@{*/

/** @brief text object. */
#define FPD_PAGEOBJ_TEXT		1
/** @brief path object. */
#define FPD_PAGEOBJ_PATH		2
/** @brief image object. */
#define FPD_PAGEOBJ_IMAGE		3
/** @brief shading object. */
#define FPD_PAGEOBJ_SHADING		4
/** @brief form object. */
#define FPD_PAGEOBJ_FORM		5
/** @brief inline image object. Inline image section. FOR EMBEDDED SYSTEM ONLY*/
#define FPD_PAGEOBJ_INLINES		6

/*@}*/
/* @DEFGROUPEND */

/* @STRUCTSTART FPD_TextObjectItem */
/** @brief A data structure for text object item. */
struct FPD_TextObjectItem
{
	/** The character code. -1 (0xffffffff) for spacing. */
	FS_DWORD			m_CharCode;
	/** X origin in text space, or the spacing. */
	FS_FLOAT			m_OriginX;
	/** Y origin in text space. */
	FS_FLOAT			m_OriginY;
};
/* @STRUCTEND */


/* @OBJEND */

/* @OBJSTART FPD_TextObject */
/** @brief A FPD_TextObject is a graphic object (a subclass of FPD_PageObject) representing one or more character 
 * strings on a page. 
 *
 * A text object consists of one or more character strings that identify sequences of 
 * glyphs to be painted. Like a path, text can be stroked, filled, or used as a clipping boundary.
 *
 *
 */
/* @OBJEND */

/* @OBJSTART FPD_PathObject */
/** 
 * @brief A FPD_PathObject is a graphic object (a subclass of FPD_PageObject) representing a path in a page description. 
 *
 * A path object is an arbitrary shape made up of straight lines, rectangles, and cubic Bézier curves. A path may intersect
 * itself and may have disconnected sections  and holes. A path object ends with one or more painting operators that 
 * specify whether the path is stroked, filled, used as a clipping boundary, or some combination of these operations.
 *
 *
 */
/* @OBJEND */

/* @OBJSTART FPD_ImageObject */
/** 
 * @brief A FPD_ImageObject is a sampled image or image mask, and corresponds to a PDF Image resource
 * (see <Italic>Stencil Masking in Section 4.8, Images, in the PDF Reference</Italic>).
 *
 *
 */
/* @OBJEND */

/* @OBJSTART FPD_ShadingObject */
/** 
 * @brief A FPD_PathObject is a graphic object (a subclass of FPD_PageObject) representing a smooth shading in a page 
 * description.
 *
 * FPD_PathObject describes geometric shapes whose color are an arbitrary function of position within the shape. 
 * (A shading can also be treated as a color when painting other graphics objects; it is not considered to be a separate 
 * graphics object in that case.)
 *
 */
/* @OBJEND */

/* @OBJSTART FPD_FormObject */
/** 
 * @brief This object corresponds to a PDF form XObject (<Italic>see Section 4.9, Form XObjects, in the PDF Reference</Italic>).
 *
 */
/* @OBJEND */

/* @OBJSTART FPD_InlineImages */
/** 
 * @brief A FPD_InlineImages is an image whose data is stored in the page description's contents stream instead of being 
 * stored as an image resource (see FPD_ImageObject). FPD_InlineImages is a subclass of FPD_PageObject and corresponds to
 * the PDF inline image operator (<Italic>see Section 4.8.6, In-Line Images, in the PDF Reference</Italic>).<br><br>
 *
 * Inline images generally are used for images with small amounts of data (up to several kilobytes), while image resources
 * are used for large images. 
 *
 */
/* @OBJEND */

/* @OBJSTART FPD_PathEditor */
#ifndef FPD_PATHEDITOR
#define FPD_PATHEDITOR
/**
 * @brief The path editor.
 */
typedef struct _t_FPD_PathEditor* FPD_PathEditor;

#endif
/* @OBJEND */


/* @OBJSTART FPD_PathObjectUtils */
#ifndef FPD_PATHOBJECTUTILS
#define FPD_PATHOBJECTUTILS
/**
 * @brief The path object utils.
 */
typedef struct _t_FPD_PathObjectUtils* FPD_PathObjectUtils;


#endif
/* @OBJEND */



/* @OBJSTART FPD_ShadingEditor */
#ifndef FPD_SHADINGEDIOTR
#define FPD_SHADINGEDIOTR
/**
 * @brief The path object utils.
 */
typedef struct _t_FPD_FPD_ShadingEditor* FPD_ShadingEditor;

#endif
/* @OBJEND */


/* @OBJSTART FPD_ShadingEditor */
#ifndef FPD_SHADINGOBJECTUTILS
#define FPD_SHADINGOBJECTUTILS
/**
 * @brief The shading object utils.
 */
typedef struct _t_FPD_ShadingObjectUtils* FPD_ShadingObjectUtils;

/* @STRUCTSTART FPD_EditorPage*/
typedef struct  __FPD_ShadingInfo__
{
	/**
	* @brief The shading info.
	*/
	FS_FLOAT fPostion = 0.0f;
	FS_COLORREF dColor = RGB(0, 0, 0);
}FPD_ShadingInfo;
/* @STRUCTEND */

#endif
/* @OBJEND */

/* @OBJSTART FPD_texteditor */
#ifndef FPD_TEXTEDITOR
#define FPD_TEXTEDITOR
/**
 * @brief The text editor.
 */
typedef struct _t_FPD_TextEditor* FPD_TextEditor;

#endif
/* @OBJEND */


#ifndef FPD_IPAGEEDITOR
#define FPD_IPAGEEDITOR
typedef struct _t_FPD_IPageEditor* FPD_IPageEditor;


#endif

#ifndef FPD_IEDITORCONTEXT
#define FPD_IEDITORCONTEXT
typedef struct _t_FPD_IEditorContext* FPD_IEditorContext;
#endif


#ifndef FPD_TEXTOBJECTUTILS
#define FPD_TEXTOBJECTUTILS
/**
 * @brief The text Object Utils.
 */
typedef struct _t_FPD_TextObjectUtils* FPD_TextObjectUtils;

/* @ENUMSTART */
/** @brief Parameter type enumeration of content mark item. See <a>FPD_FONTOPERATION</a>, <a>FPD_FONTOPERATION</a>.
 */
enum FPD_FONTOPERATION
{
	kName, /** < @brief kName.*/
	kBold, /** < @brief Bold.*/
	kItatic /** < @brief Itatic.*/
};
/* @ENUMEND */



/* @OBJEND */
#endif


#ifndef FPD_IMAGEEDITOR
#define FPD_IMAGEEDITOR
/* @ENUMSTART */
/** @brief Parameter type enumeration of content mark item. See <a>FPD_IMAGEEDITOR</a>, <a>FPD_IMAGEEDITOR</a>.
 */
typedef struct _t_FPD_ImageEditor* FPD_ImageEditor;

#endif

/* @OBJSTART FPD_GraphicObject */
#ifndef FPD_GRAPHICOBJECT
#define FPD_GRAPHICOBJECT
/** @brief The content marks for tagged pdf. See <a>FPDContentMarkNew</a>, <a>FPDContentMarkDestroy</a>.
 */
typedef struct _t_FPD_GraphicObjcet* FPD_GraphicObject;
#endif

/* @OBJSTART FPD_GraphicEditor */
#ifndef FPD_GRAPHICEDITOR
#define FPD_GRAPHICEDITOR
/** @brief The content marks for tagged pdf. See <a>FPDContentMarkNew</a>, <a>FPDContentMarkDestroy</a>.
 */
typedef struct _t_FPD_GraphicEditor* FPD_GraphicEditor;
#endif

/* @OBJSTART FPD_GraphicObjectUtils */
#ifndef FPD_GRAPHICOBJECTUTILS
#define FPD_GRAPHICOBJECTUTILS
/** @brief The content marks for tagged pdf. See <a>FPDContentMarkNew</a>, <a>FPDContentMarkDestroy</a>.
 */
typedef struct _t_FPD_GraphicObjectUtils* FPD_GraphicObjectUtils;
#endif
/* @OBJEND */

/* @OBJSTART FPD_GraphicObjectUtils */
#ifndef FPD_MIXEDOBJECTUTILS
#define FPD_MIXEDOBJECTUTILS
/** @brief The content marks for tagged pdf. See <a>FPDContentMarkNew</a>, <a>FPDContentMarkDestroy</a>.
 */
typedef struct _t_FPD_MIXEDOBJECTUTILS* FPD_MixedObjectUtils;
#endif
/* @OBJEND */

/* @OBJSTART FPD_GraphicObjectUtils */
#ifndef FPD_ICLIPBOARDHANDLER
#define FPD_ICLIPBOARDHANDLER
/** @brief The content marks for tagged pdf. See <a>FPDContentMarkNew</a>, <a>FPDContentMarkDestroy</a>.
 */
typedef struct _t_FPD_ICLIPBOARDHANDLER* FPD_IClipboardHandler;
#endif
/* @OBJEND */

/* @OBJSTART FPD_GraphicObjectUtils */
#ifndef FPD_IPOPUPMENUHANDLER
#define FPD_IPOPUPMENUHANDLER
/** @brief The content marks for tagged pdf. See <a>FPDContentMarkNew</a>, <a>FPDContentMarkDestroy</a>.
 */
typedef struct _t_FPD_IPOPUPMENUHANDLER* FPD_IPopupMenuHandler;
#endif
/* @OBJEND */


/* @OBJSTART FPD_GraphicObjectUtils */
#ifndef FPD_IUNDOITEM
#define FPD_IUNDOITEM
/** @brief The content marks for tagged pdf. See <a>FPDContentMarkNew</a>, <a>FPDContentMarkDestroy</a>.
 */
typedef struct _t_FPD_IUNDOITEM* FPD_IUndoItem;
#endif
/* @OBJEND */


/* @OBJSTART FPD_IOperationNotify */
#ifndef FPD_IOPERATIONNOTIFYHANDLER
#define FPD_IOPERATIONNOTIFYHANDLER
/** @brief The content marks for tagged pdf. See <a>FPD_IOperationNotifyNew</a>, <a>FPD_IOperationNotifyDestroy</a>.
 */
typedef struct _t_FPD_IOperationNotifyHandler* FPD_IOperationNotifyHandler;
#endif
/* @OBJEND */

/* @OBJSTART FPD_IOperationNotify */
#ifndef FPD_IITIPHANDLER
#define FPD_IITIPHANDLER
/** @brief The content marks for tagged pdf. See <a>FPD_ITipHandlerNew</a>, <a>FPD_ITipHandlerDestroy</a>.
 */
typedef struct _t_FPD_ITipHandler* FPD_ITipHandler;
#endif
/* @OBJEND */


/* @OBJSTART FPD_PageObject */
#ifndef FPD_PAGEOBJECT
#define FPD_PAGEOBJECT
/**
 * @brief A <a>FPD_PageObject</a> is the abstract superclass of page objects.
 *
 * You can find the type of any object
 * with the <a>FPDPageObjectGetType</a>() method.
 *
 */
typedef struct _t_FPD_PageObject* FPD_PageObject;
#endif


/* @ENUMSTART */
	/**
	* @name ObjectFilter Type. See <a>FPD_ObjectFilter</a>.
	*/
	/*@{*/
enum  FPD_ObjectFilter
{
	FSObjectFilter_kNone,
	FSObjectFilter_KAll,
	FSObjectFilter_KText,
	FSObjectFilter_kImage,
	FSObjectFilter_kPath,
	FSObjectFilter_kShading
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
	/**
	* @name GraphicObjectUtilsType Type. See <a>FPD_GraphicObjectUtilsType</a>.
	*/
	/*@{*/
enum FPD_GraphicObjectUtilsType
{
	FSGraphicUtilsType_kNone,
	FSGraphicUtilsType_kText,
	FSGraphicUtilsType_KPath,
	FSGraphicUtilsType_kImage,
	FSGraphicUtilsType_kShading,
	FSGraphicUtilsType_kMixed
};
/*@}*/
	/* @ENUMEND */


/* @ENUMSTART */
	/**
	* @name AlignModeType Type. See <a>FPD_AlignMode</a>.
	*/
	/*@{*/
enum FPD_AlignMode
{
	FSAlignMode_kLeft,
	FSAlignMode_kRight,
	FSAlignMode_kTop,
	FSAlignMode_kBottom,
	FSAlignMode_kVertical,
	FSAlignMode_kHorizontal
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
	/**
	* @name CenterModeType Type. See <a>FPD_CenterMode</a>.
	*/
	/*@{*/
enum FPD_CenterMode
{
	FSCenterMode_kVertical,
	FSCenterMode_kHorizontal,
	FSCenterMode_kBoth
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
	/**
	* @name SizeModeType Type. See <a>FPD_SizeMode</a>.
	*/
	/*@{*/
enum FPD_SizeMode
{
	FSSizeMode_kWidth,
	FSSizeMode_kHeight,
	FSSizeMode_kBoth
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
	/**
	* @name TextModeType Type. See <a>FPD_TextMode</a>.
	*/
	/*@{*/
enum FPD_TextMode
{
	FSTextMode_kFill,       //0 Fill text.
	FSTextMode_kStroke,     //1 Stroke text.
	FSTextMode_kFillStroke, //2 Fill, then stroke text.
	FSTextMode_kNone        //3 Neither fill nor stroke text(invisible).
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
	/**
	* @name FontOperationType Type. See <a>FPD_FontOperation</a>.
	*/
	/*@{*/
enum struct FPD_FontOperation
{
	FSFontOperation_kName,
	FSFontOperation_kBold,
	FSFontOperation_kItatic
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
/**
* @name Rending Order Type. See <a>FPD_RendingOrder</a>.
*/
/*@{*/
enum FPD_RendingOrder
{
	FSRendingOrder_kBack,
	FSRendingOrder_kBackward,
	FSRendingOrder_kFront,
	FSRendingOrder_kForward
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
/**
* @name Filter Type. See <a>FPD_Filter</a>.
*/
/*@{*/
enum  FPD_Filter
{
	FSFilter_kDodge,
	FSFilter_kVivify,
	FSFilter_kChromatic2grey,
	FSFilter_kSoftness,
	FSFilter_kSharpne,
	FSFilter_kBinarize,
	FSFilter_kConvex,
	FSFilter_kConcave,
	FSFilter_kOpposite,
	FSFilter_kGrey2chromatic
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
	/**
	* @name CursorType. See <a>FPD_UICursorResourceType</a>.
	*/
	/*@{*/
enum FPD_UICursorResourceType
{
	FSUICursor_kAll,
	FSUICursor_kCross,
	FSUICursor_kHbeam,
	FSUICursor_kSizeNESW,
	FSUICursor_kSizeNS,
	FSUICursor_kSizeNWSE,
	FSUICursor_kSizeWE,
	FSUICursor_kRotation,
	FSUICursor_kSelete,
	FSUICursor_kSelectCopy,
	FSUICursor_kSelectDrage,
	FSUICursor_kNoDrag,
	FSUICursor_kHand,
	FSUICursor_kArrow,
	FSUICursor_kCloneSpotPoint,
	FSUICursor_kPaintBrush,
	FSUICursor_kPaintErase,
	FSUICursor_kSelectRect,
	FSUICursor_kSelectCut,
	FSUICursor_kSelectPolygonal,
	FSUICursor_kLasso,
	FSUICursor_kMagicWand,
	FSUICursor_kPaintBucket,
	FSUICursor_kEyedropper,
	FSUICursor_kIBeam
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
enum FPD_TextEditorPopupMenuItem
{
	FPDTextEditor_kUndo = 200,
	FPDTextEditor_kRedo,
	FPDTextEditor_kCut,
	FPDTextEditor_kCopy,
	FPDTextEditor_kPaste,
	FPDTextEditor_kDelete,
	FPDTextEditor_kTextDirection,
	FPDTextEditor_kTextDirectionL2R,
	FPDTextEditor_kTextDirectionR2L,
	FPDTextEditor_kSelectAll
};
/* @ENUMEND */

/* @ENUMSTART */
enum  FPD_PAPopupMenuItem
{
	FPDPAP_kDelete = 300,
	FPDPAP_kInsertStartFlag,         //Insert子菜单开始标志
	FPDPAP_kInsertLine,
	FPDPAP_kInsertBessel,
	FPDPAP_kInsertEndFlag,           //Insert子菜单结束标志
	FPDPAP_kChangeToStartFlag,       //ChangeTo子菜单开始标志
	FPDPAP_kChangeToLine,
	FPDPAP_kChangeToBessel,
	FPDPAP_kChangeToEndFlag,         //ChangeTo子菜单结束标志
	FPDPAP_kClose,
	FPDPAP_kAddLine,
	FPDPAP_kAddRectangle,
	FPDPAP_kAddEllipse,
	FPDPAP_kAddLineDrawing,
	FPDPAP_kExit,
	FPDPAP_kMaxValue
};
/* @ENUMEND */

/* @ENUMSTART */
	/**
	* @name ShapeTypeType Type. See <a>FPD_ShapeType</a>.
	*/
	/*@{*/
enum FPD_ShapeType
{
	FSShapeType_kLine,
	FSShapeType_kRectangle,
	FSShapeType_kRoundRectangle,
	FSShapeType_kEllipse,
	FSShapeType_kPolyline,
	FSShapeType_kPolygon,
	FSShapeType_kBezier,
	FSShapeType_kCloseBezier,
	FSShapeType_kFreePaint
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
	/**
	* @name UI Dialog Resource ID. See <a>FPD_UIDialogResourceID</a>.
	*/
	/*@{*/
enum FPD_UIDialogResourceID
{
	FPDUIDialog_kPathConnectTwoLines,
	FPDUIDialog_kImageInsufficientMemory,
	FPDUIDialog_kImageImport,
	FPDUIDialog_kImageSelectRegion,
	FPDUIDialog_kPaintSpothealingCtrl,
	FPDUIDialog_kImageCloneStampCtrl,
	FPDUIDialog_KImageModifyConfirm,
    FPDUIDialog_kConvertPathTip
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
	/**
	* @name PngType Type. See <a>FPD_PngType</a>.
	*/
	/*@{*/
enum FPD_UIImageResourceID
{
	FPDUIImage_kBluePoint,
	FPDUIImage_kRedPoint,
	FPDUIImage_kGreenPoint
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
	/**
	* @name PathType Type. See <a>FPD_PathType</a>.
	*/
	/*@{*/
enum FPD_GeoDrawType
{
	FPDGeoDrawType_kLine,
	FPDGeoDrawType_kRectangle,
	FPDGeoDrawType_kRoundRect,
	FPDGeoDrawType_kEllipse,
	FPDGeoDrawType_kPolyline,
	FPDGeoDrawType_kPolygon,
	FPDGeoDrawType_Polybezier,
	FPDGeoDrawType_kCurve,
	FPDGeoDrawType_kFreedom,
	FPDGeoDrawType_kUnknow
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
/** @brief Parameter type enumeration of content mark item. See <a>FPD_GraphicObjectUtilsType</a>, <a>FPD_GraphicObjectUtilsType</a>.
 */
enum FPD_WritingDirection
{
	FPDWriting_kLeft2Right,	/** < @brief The writing direction of left-to-right.*/
	FPDWriting_kRight2Left		/** < @brief The writing direction of right-to-left.*/
};
/* @ENUMEND */

/* @ENUMSTART */
	/**
	* @name IdentifyMode Type. See <a>FPD_IdentifyMode</a>.
	*/
	/*@{*/
enum FPD_IdentifyMode
{
	FPDIdentifyMode_kPosition,
	FPDIdentifyMode_kCommand
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
	/**
	* @name SDPopupMenuItem Type. See <a>FPD_SDPopupMenuItem</a>.
	*/
	/*@{*/
enum  FPD_SDPopupMenuItem
{
	FPDSDPopupMenuItem_kDelete = 100,
	FPDSDPopupMenuItem_kInsertStartFlag,         //Insert子菜单开始标志
	FPDSDPopupMenuItem_kInsertLine,
	FPDSDPopupMenuItem_kInsertBessel,
	FPDSDPopupMenuItem_kInsertEndFlag,           //Insert子菜单结束标志
	FPDSDPopupMenuItem_kChangeToStartFlag,       //ChangeTo子菜单开始标志
	FPDSDPopupMenuItem_kChangeToLine,
	FPDSDPopupMenuItem_kChangeToBessel,
	FPDSDPopupMenuItem_kChangeToEndFlag,         //ChangeTo子菜单结束标志
	FPDSDPopupMenuItem_kClose,
	FPDSDPopupMenuItem_kExit,
	FPDSDPopupMenuItem_kMaxValue
};
/*@}*/
	/* @ENUMEND */


/* @ENUMSTART */
	/**
	* @name PopupMenuItem Type. See <a>FPD_PopupMenuItem</a>.
	*/
	/*@{*/
enum  FPD_PopupMenuItem
{
	FPDPopupMenuItem_kProperties = 0,
	FPDPopupMenuItem_kSetDefault,
	FPDPopupMenuItem_kEditObject,
	FPDPopupMenuItem_kBackGround,
	FPDPopupMenuItem_kForwardGround,
	FPDPopupMenuItem_kCut,
	FPDPopupMenuItem_kCopy,
	FPDPopupMenuItem_kPaste,
	FPDPopupMenuItem_kDelete,
	FPDPopupMenuItem_kAlignStartFlag,          //Align子菜单开始标志
	FPDPopupMenuItem_kAlignLeft,
	FPDPopupMenuItem_kAlignRight,
	FPDPopupMenuItem_kAlignTop,
	FPDPopupMenuItem_kAlignBottom,
	FPDPopupMenuItem_kAlignVertical,
	FPDPopupMenuItem_kAlignHorizontal,
	FPDPopupMenuItem_kAlignEndFlag,            //Align子菜单结束标志
	FPDPopupMenuItem_kCenterStartFlag,         //Center子菜单开始标志
	FPDPopupMenuItem_kCenterVertical,
	FPDPopupMenuItem_kCenterHorizontal,
	FPDPopupMenuItem_kCenterBoth,
	FPDPopupMenuItem_kCenterEndFlag,           //Center子菜单结束标志
	FPDPopupMenuItem_kDistributeStartFlag,     //Distribute子菜单开始标志
	FPDPopupMenuItem_kDistributeVertical,
	FPDPopupMenuItem_kDistributeHorizontal,
	FPDPopupMenuItem_kDistributeEndFlag,       //Distribute子菜单结束标志
	FPDPopupMenuItem_kSizeStartFlag,           //Size子菜单开始标志
	FPDPopupMenuItem_kSizeWidth,
	FPDPopupMenuItem_kSizeHeight,
	FPDPopupMenuItem_kSizeBoth,
	FPDPopupMenuItem_kSizeEndFlag,             //Size子菜单结束标志
	FPDPopupMenuItem_kRotateStartFlag,         //Roatate子菜单开始标志
	FPDPopupMenuItem_kRotateClockwise,
	FPDPopupMenuItem_kRotateCounterClockwise,
	FPDPopupMenuItem_kRotateMoreOptions,
	FPDPopupMenuItem_kRotateEndFlag,           //Roatate子菜单结束标志
	FPDPopupMenuItem_kFlipStartFlag,           //Flip子菜单开始标志
	FPDPopupMenuItem_kFlipVertical,
	FPDPopupMenuItem_kFlipHorizontal,
	FPDPopupMenuItem_kFilpEndFlag,             //Flip子菜单结束标志
	FPDPopupMenuItem_kShear,
	FPDPopupMenuItem_kAddClipStartFlag,        //AddClip子菜单开始标志
	FPDPopupMenuItem_kAddClipRectangle,
	FPDPopupMenuItem_kAddClipElipse,
	FPDPopupMenuItem_kAddClipRoundRectangle,
	FPDPopupMenuItem_kAddClipPolygon,
	FPDPopupMenuItem_kAddClipCurve,
	FPDPopupMenuItem_kAddClipDrawing,
	FPDPopupMenuItem_kAddClipEndFlag,         //AddClip子菜单结束标志
	FPDPopupMenuItem_kEditClip,
	FPDPopupMenuItem_kClearClip,
	FPDPopupMenuItem_kConvert2Shape,
	FPDPopupMenuItem_kMaxValue
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
	/**
	* @name UIString Resource ID. See <a>FPD_UIStringResourceID</a>.
	*/
	/*@{*/
enum  FPD_UIStringResourceID
{
	FPDUIString_kNone,
	FPDUIString_kTextTip,
	FPDUIString_kPathTip,
	FPDUIString_kImageTip,
	FPDUIString_kShadingTip,
	FPDUIString_kFormTip,
	FPDUIString_kHintSizeTip,
	FPDUIString_kBitmapFilter,
	FPDUIString_kSaveBitmapFilter,
	FPDUIString_kPathTipString,
    FPDUIString_kUnkownFontName
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
	/**
	* @name ExistType Type. See <a>FPD_ExistType</a>.
	*/
	/*@{*/
enum  FPD_ExistType
{
	FPDExistType_kNone,
	FPDExistType_kAll,
	FPDExistType_kMixed
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
	/**
	* @name ValidFlag Type. See <a>FPD_ValidFlag</a>.
	*/
	/*@{*/
enum  FPD_ValidFlag
{
	FPDValidFlag_kInvalid,
	FPDValidFlag_kValid,
	FPDValidFlag_kMixed
};
/*@}*/
	/* @ENUMEND */


/* @ENUMSTART */
	/**
	* @name ColorType Type. See <a>FPD_ColorType</a>.
	*/
	/*@{*/
enum  FPD_ColorType
{
	FPDColorType_kRGB,
	FPDColorType_kCMYK,
	FPDColorType_KGRAY
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
	/**
	* @name SelectFilter Type. See <a>FPD_SelectFilter</a>.
	*/
	/*@{*/
enum FPD_Operation
{
	FPD_Operation_kAdd,
	FPD_Operation_kDelete,
	FPD_Operation_KEdit,
	FPD_Operation_KSelect
};
/*@}*/
	/* @ENUMEND */

/* @ENUMSTART */
enum FPD_ImageEditorToolActionType
{
	FPDImageEditor_kUnSupported = -1,
	FPDImageEditor_kSelectPath = 0,
	FPDImageEditor_kSelectRect = 1,
	FPDImageEditor_kSelectEllipse = 2,
	FPDImageEditor_kSelectAntline = 3,
	FPDImageEditor_kSelectMagicWand = 4,
	FPDImageEditor_kSelectPolygonal = 5,
	FPDImageEditor_kPaintEraser = 6,
	FPDImageEditor_kPaintPencil = 7,
	FPDImageEditor_kPaintBrush = 8,
	FPDImageEditor_kPaintBucket = 9,
	FPDImageEditor_kPaintSpothealing = 10,
	FPDImageEditor_kPaintCloneStamp = 11,
	FPDImageEditor_kPaintBrighten = 12,
	FPDImageEditor_kPaintBurn = 13,
	//KAdjustZoom = 14,
	FPDImageEditor_kEyedropper = 15,
	FPDImageEditor_kCloneStamp = 16
};
/* @ENUMEND */

/* @ENUMSTART */
enum FPD_ImageEditoBlendMode
{
	FPDImageEditor_kUnsupported = -1,
	FPDImageEditor_kColorBurn,
	FPDImageEditor_kColorDodge,
	FPDImageEditor_kDarken,
	FPDImageEditor_kDifference,
	FPDImageEditor_kExclusion,
	FPDImageEditor_kHardLight,
	FPDImageEditor_kHardMix,
	FPDImageEditor_kLighten,
	FPDImageEditor_kLinearDark,
	FPDImageEditor_kLinearLight,
	FPDImageEditor_kMultiply,
	FPDImageEditor_kNormal,
	FPDImageEditor_kOverlay,
	FPDImageEditor_kPinLight,
	FPDImageEditor_kScreen,
	FPDImageEditor_kSoftLight,
};
/* @ENUMEND */

/* @ENUMSTART */
enum  FPD_EraserMode
{
	FPDEraserMode_kUnsupported,
	FPDEraserMode_kBlock,
	FPDEraserMode_kPencil,
	FPDEraserMode_kBrush
};
/* @ENUMEND */


/* @ENUMSTART */
enum FPD_TonesMode
{
	FPDTones_kUnsupported,
	FPDTones_kShadows,
	FPDTones_kMidtones,
	FPDTones_kHighlights
};
/* @ENUMEND */

/* @OBJSTART FPD_GraphicObjectUtils */
#ifndef FPD_IOPERATIONNOTIFY
#define FPD_IOPERATIONNOTIFY
/** @brief The content marks for tagged pdf. See <a>FPD_IOperationNotify</a>, <a>FPD_IOperationNotify</a>.
 */
typedef struct _t_FPD_IOperationNotify* FPD_IOperationNotify;
#endif
/* @OBJEND */


#ifndef FPD_EDITORCONTEXTHANDLER
#define FPD_EDITORCONTEXTHANDLER
typedef struct _t_FPD_EditorContextHandler* FPD_EditorContextHandler;
#endif

/* @STRUCTSTART FPD_RgbQuad*/
typedef struct __FPD_RgbQuad__
{
	FS_BYTE rgbBlue;
	FS_BYTE rgbGreen;
	FS_BYTE rgbRed;
	FS_BYTE rgbReserved;
}FPD_RgbQuad, *PFPD_RgbQuad;


/* @STRUCTSTART FPD_BitMapFileHeader*/
typedef struct __FPD_BitMapFileHeader__ {
	FS_WORD	bfType;
	FS_DWORD bfSize;
	FS_WORD bfReserved1;
	FS_WORD bfReserved2;
	FS_DWORD bfOffBits;
}FPD_BitMapFileHeader, *PFPD_BitMapFileHeader;


/* @STRUCTSTART FPD_BitMapInfoHeader*/
typedef struct __FPD_BitMapInfoHeader__ {
	FS_DWORD biSize;
	FS_INT32 biWidth;
	FS_INT32 biHeight;
	FS_WORD  biPlanes;
	FS_WORD  biBitCount;
	FS_DWORD biCompression;
	FS_DWORD biSizeImage;
	FS_INT32 biXPelsPerMeter;
	FS_INT32 biYPelsPerMeter;
	FS_DWORD biClrUsed;
	FS_DWORD biClrImportant;
}FPD_BitMapInfoHeader, *PFPD_BitMapInfoHeader;


/* @STRUCTSTART FPD_BitMapInfoHeader*/
typedef struct __FPD_BitMapInfo__ {
	FPD_BitMapInfoHeader bmiHeader;
	FPD_RgbQuad bimColors[1];
}FPD_BitMapInfo, *PFPD_BitMapInfo;

/* @STRUCTSTART FPD_ImageHistogramData*/
typedef struct __FPD_ImageHistogramData__ {
	FS_INT32 rowBytes;
	FS_INT32 height;
	FS_INT32 width;
	FS_LPBYTE buf;
} FPD_ImageHistogramData, *PFPD_ImageHistogramData;

/* @ENUMSTART */
enum  FPD_EyedropperSampleType
{
	FPDEyed_kPixelColor = 0,
	FPDEyed_kThreeAverage,
	FPDEyed_kFiveAverage
};
/* @ENUMEND */

/* @ENUMSTART */
enum  FPD_TextProperty
{
	FPDTextProperty_kBold,
	FPDTextProperty_kItalic,
	FPDTextProperty_kWritingDirection,
	FPDTextProperty_kUnderline,
	FPDTextProperty_kCross,
	FPDTextProperty_kSuperScript,
	FPDTextProperty_kSubScript,
	FPDTextProperty_kColor,
	FPDTextProperty_kAlign,
	FPDTextProperty_kFontName,
	FPDTextProperty_kFontSize
};
/* @ENUMEND */

enum  FPD_AdjustRgb
{
	FPD_AdjustkBlue = 0,
	FPD_AdjustkGreen = 1,
	FPD_AdjustkRed = 2
};

enum  FPD_ImageMode
{
	FPD_kReplace
};


typedef struct _t_FPD_IPublicOptionData* FPD_IPublicOptionData;

typedef struct _t_FPD_IOptionData* FPD_IOptionData;

typedef struct _t_FPD_IBaseBrushOptionData* FPD_IBaseBrushOptionData;

typedef struct _t_FPD_IBrushOptionData* FPD_IBrushOptionData;

typedef struct _t_FPD_IEraserOptionData* FPD_IEraserOptionData;

typedef struct _t_FPD_IMagicWandOptionData* FPD_IMagicWandOptionData;

typedef struct _t_FPD_IDodgeOptionData* FPD_IDodgeOptionData;

typedef struct _t_FPD_IBurnOptionData* FPD_IBurnOptionData;

typedef struct _t_FPD_IEyedropperData* FPD_IEyedropperData;

typedef struct _t_FPD_ICloneStampData* FPD_ICloneStampData;

typedef struct _t_FPD_IPaintBucketOptionData* FPD_IPaintBucketOptionData;

typedef struct _t_FPD_ISpotHealingBrushData* FPD_ISpotHealingBrushData;


/* @STRUCTSTART FPD_TEXTRANGE*/
typedef struct __FPD_TEXTRANGE__
{
	int nStart;
	int nLen;

	FS_BOOL operator < (const __FPD_TEXTRANGE__ &TRB)const
	{
		return nStart < TRB.nStart;
	}
	FS_BOOL operator==(const  __FPD_TEXTRANGE__& src) const
	{
		return (nStart == src.nStart && nLen == src.nLen);
	}
} FPD_TEXTRANGE, *PFPD_TEXTRANGE;


typedef struct _t_FPD_EditObject* FPD_EditObject;

typedef struct _t_FPD_TouchJoinSplit* FPD_ITouchJoinSplit;

typedef struct _t_FPD_ITouchup* FPD_ITouchup;

typedef struct _t_FPD_ITouchupManager* FPD_ITouchupManager;
typedef struct _t_FPD_ITouchupProvider* FPD_ITouchupProvider;
typedef struct _t_FPD_ITouchUndoItem* FPD_ITouchUndoItem;
typedef struct _t_FPD_ITouchClipboardHandler* FPD_ITouchClipboardHandler;
typedef struct _t_FPD_ITouchPopupMenuHandler* FPD_ITouchPopupMenuHandler;
typedef struct _t_FPD_ITouchProgressBarHandler* FPD_ITouchProgressBarHandler;
typedef struct _t_FPD_ITouchOperationNotifyHandler* FPD_ITouchOperationNotifyHandler;
typedef struct _t_FPD_ITouchUndoHandler* FPD_ITouchUndoHandler;
typedef struct _t_FPD_ITouchTextFormatHandler* FPD_ITouchTextFormatHandler;


#define FPDMBF_OK                       0x00000000L
#define FPDMBF_OKCANCEL                 0x00000001L
#define FPDMBF_ABORTRETRYIGNORE         0x00000002L
#define FPDMBF_YESNOCANCEL              0x00000003L
#define FPDMBF_YESNO                    0x00000004L
#define FPDMBF_RETRYCANCEL              0x00000005L
#define FPDMBF_ICONHAND                 0x00000010L
#define FPDMBF_ICONQUESTION             0x00000020L
#define FPDMBF_ICONEXCLAMATION          0x00000030L
#define FPDMBF_ICONINFORMATION          0x00000040L


#ifndef FPD_TOUCHCLIPBOARD
#define FPD_TOUCHCLIPBOARD
/** @brief A callback for <a>FPD_EnumPageHandler</a>. See <a>FPDDocEnumPages</a>.**/
typedef struct
{
	/* The size of data structure. It must be set to <Italic>sizeof</Italic>(<a>FPD_EDITCONTEXT</a>).  */
	unsigned long lStructSize;

	/** The user-supplied data. */
	FS_LPVOID		clientData;
	
	FS_WideString(*FPDGetClipboardText)(FS_LPVOID clientData);
	void(*FPDSetClipboardText)(FS_LPVOID clientData, FS_WideString sSelText);
}FPD_TouchClipboardHandlerCallbacksRec, *FPD_TouchClipBoardHandlerCallbacks;

#endif
/* @COMMONEND */


enum FPD_TouchOperation
{
	FPDTkAdd,
	FPDTkDelete,
	FPDTKEdit,
};

enum FPD_TouchJoinSpiltOperationType
{
	FPDTkJoin = 0,
	FPDTkJoinSpilt,
	FPDTkJoinLink,
	FPDTkJoinUnlink,
	FPDTkJoinSelectNone,
	FPDTkJoinClose
};

#ifdef __cplusplus
};
#endif

#endif

/** @} */

/*@}*/