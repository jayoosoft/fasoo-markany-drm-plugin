﻿/*****************************************************************************

Copyright (C) 2010 Foxit Corporation
All rights reserved.

NOTICE: Foxit permits you to use, modify, and distribute this file
in accordance with the terms of the Foxit license agreement
accompanying it. If you have received this file from a source other
than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
 
*****************************************************************************/

// Auto Generator Code Start, Please don't edit this file!
#ifndef FS_FUNCTABINITIATOR_H_
#define FS_FUNCTABINITIATOR_H_

#include "../pdf/fpd_parserCalls.h"
#include "../pdf/impl/fpd_parserImpl.h"
#include "../pdf/fpd_docCalls.h"
#include "../pdf/impl/fpd_docImpl.h"
#include "../pdf/fpd_objsCalls.h"
#include "../pdf/impl/fpd_objsImpl.h"
#include "../pdf/fpd_pageCalls.h"
#include "../pdf/impl/fpd_pageImpl.h"
#include "../pdf/fpd_pageobjCalls.h"
#include "../pdf/impl/fpd_pageobjImpl.h"
#include "../pdf/fpd_renderCalls.h"
#include "../pdf/impl/fpd_renderImpl.h"
#include "../pdf/fpd_resourceCalls.h"
#include "../pdf/impl/fpd_resourceImpl.h"
#include "../pdf/fpd_textCalls.h"
#include "../pdf/impl/fpd_textImpl.h"
#include "../pdf/fpd_serialCalls.h"
#include "../pdf/impl/fpd_serialImpl.h"
#include "../pdf/fpd_epubCalls.h"
#include "../pdf/impl/fpd_epubImpl.h"
#include "../ofd/fofd_basicCalls.h"
#include "../ofd/impl/fofd_basicImpl.h"
#include "../ofd/fofd_pageCalls.h"
#include "../ofd/impl/fofd_pageImpl.h"
#include "../ofd/fofd_docCalls.h"
#include "../ofd/impl/fofd_docImpl.h"
#include "../ofd/fofd_renderCalls.h"
#include "../ofd/impl/fofd_renderImpl.h"
#include "../ofd/fofd_sigCalls.h"
#include "../ofd/impl/fofd_sigImpl.h"
#include "../frd/fr_appCalls.h"
#include "../frd/impl/fr_appImpl.h"
#include "../frd/fr_fxnetappCalls.h"
#include "../frd/impl/fr_fxnetappImpl.h"
#include "../frd/fr_barCalls.h"
#include "../frd/impl/fr_barImpl.h"
#include "../basic/fs_basicCalls.h"
#include "../basic/impl/fs_basicImpl.h"
#include "../frd/fr_docCalls.h"
#include "../frd/impl/fr_docImpl.h"
#include "../frd/fr_menuCalls.h"
#include "../frd/impl/fr_menuImpl.h"
#include "../basic/fs_stringCalls.h"
#include "../basic/impl/fs_stringImpl.h"
#include "../frd/fr_sysCalls.h"
#include "../frd/impl/fr_sysImpl.h"
#include "../frd/fr_viewCalls.h"
#include "../frd/impl/fr_viewImpl.h"

#include "../fdrm/fdrm_descCalls.h"
#include "../fdrm/impl/fdrm_descImpl.h"

#include "../fdrm/fdrm_managerCalls.h"
#include "../fdrm/impl/fdrm_managerImpl.h"

#include "../fdrm/fdrm_pdfCalls.h"
#include "../fdrm/impl/fdrm_pdfImpl.h"

#include "../fdrm/fdrm_pkiCalls.h"
#include "../fdrm/impl/fdrm_pkiImpl.h"

#ifndef FX_READER_DLL

//----------_V1----------
// In file fs_basicImpl.h
class CFS_ExtensionHFTMgr_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSExtensionHFTMgrSEL, FSExtensionHFTMgrNewHFTSEL, (void*)CFS_ExtensionHFTMgr_V1::NewHFT);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSExtensionHFTMgrSEL, FSExtensionHFTMgrAddHFTSEL, (void*)CFS_ExtensionHFTMgr_V1::AddHFT);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSExtensionHFTMgrSEL, FSExtensionHFTMgrGetHFTSEL, (void*)CFS_ExtensionHFTMgr_V1::GetHFT);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSExtensionHFTMgrSEL, FSExtensionHFTMgrReplaceEntrySEL, (void*)CFS_ExtensionHFTMgr_V1::ReplaceEntry);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSExtensionHFTMgrSEL, FSExtensionHFTMgrGetEntrySEL, (void*)CFS_ExtensionHFTMgr_V1::GetEntry);
	}
};

class CFS_AffineMatrix_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixIsScaledSEL, (void*)CFS_AffineMatrix_V1::IsScaled);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixIs90RotatedSEL, (void*)CFS_AffineMatrix_V1::Is90Rotated);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixGetReverseSEL, (void*)CFS_AffineMatrix_V1::GetReverse);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixTransformPointSEL, (void*)CFS_AffineMatrix_V1::TransformPoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixTransformRectSEL, (void*)CFS_AffineMatrix_V1::TransformRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixConcatSEL, (void*)CFS_AffineMatrix_V1::Concat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixTranslateSEL, (void*)CFS_AffineMatrix_V1::Translate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixTranslateISEL, (void*)CFS_AffineMatrix_V1::TranslateI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixScaleSEL, (void*)CFS_AffineMatrix_V1::Scale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixConcatInverseSEL, (void*)CFS_AffineMatrix_V1::ConcatInverse);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixMatchRectSEL, (void*)CFS_AffineMatrix_V1::MatchRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixGetUnitRectSEL, (void*)CFS_AffineMatrix_V1::GetUnitRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixGetUnitAreaSEL, (void*)CFS_AffineMatrix_V1::GetUnitArea);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixGetXUnitSEL, (void*)CFS_AffineMatrix_V1::GetXUnit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixGetYUnitSEL, (void*)CFS_AffineMatrix_V1::GetYUnit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixTransformDistanceSEL, (void*)CFS_AffineMatrix_V1::TransformDistance);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSAffineMatrixSEL, FSAffineMatrixRotateSEL, (void*)CFS_AffineMatrix_V1::Rotate);
	}
};

class CFS_DIBitmap_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapNewSEL, (void*)CFS_DIBitmap_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapCreateSEL, (void*)CFS_DIBitmap_V1::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapDestroySEL, (void*)CFS_DIBitmap_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetWidthSEL, (void*)CFS_DIBitmap_V1::GetWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetHeightSEL, (void*)CFS_DIBitmap_V1::GetHeight);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetFormatSEL, (void*)CFS_DIBitmap_V1::GetFormat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetPitchSEL, (void*)CFS_DIBitmap_V1::GetPitch);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetPaletteSEL, (void*)CFS_DIBitmap_V1::GetPalette);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetBufferSEL, (void*)CFS_DIBitmap_V1::GetBuffer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetScanlineSEL, (void*)CFS_DIBitmap_V1::GetScanline);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapDownSampleScanlineSEL, (void*)CFS_DIBitmap_V1::DownSampleScanline);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapTakeOverSEL, (void*)CFS_DIBitmap_V1::TakeOver);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapConvertFormatSEL, (void*)CFS_DIBitmap_V1::ConvertFormat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapClearSEL, (void*)CFS_DIBitmap_V1::Clear);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetPixelSEL, (void*)CFS_DIBitmap_V1::GetPixel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapSetPixelSEL, (void*)CFS_DIBitmap_V1::SetPixel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapLoadChannelSEL, (void*)CFS_DIBitmap_V1::LoadChannel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapLoadChannel2SEL, (void*)CFS_DIBitmap_V1::LoadChannel2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapMultiplyAlphaSEL, (void*)CFS_DIBitmap_V1::MultiplyAlpha);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapMultiplyAlpha2SEL, (void*)CFS_DIBitmap_V1::MultiplyAlpha2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapTransferBitmapSEL, (void*)CFS_DIBitmap_V1::TransferBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapTransferMaskSEL, (void*)CFS_DIBitmap_V1::TransferMask);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapCompositeBitmapSEL, (void*)CFS_DIBitmap_V1::CompositeBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapCompositeRectSEL, (void*)CFS_DIBitmap_V1::CompositeRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGammaAdjustSEL, (void*)CFS_DIBitmap_V1::GammaAdjust);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapConvertColorScaleSEL, (void*)CFS_DIBitmap_V1::ConvertColorScale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapDitherFSSEL, (void*)CFS_DIBitmap_V1::DitherFS);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetBPPSEL, (void*)CFS_DIBitmap_V1::GetBPP);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapIsAlphaMaskSEL, (void*)CFS_DIBitmap_V1::IsAlphaMask);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapHasAlphaSEL, (void*)CFS_DIBitmap_V1::HasAlpha);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapIsOpaqueImageSEL, (void*)CFS_DIBitmap_V1::IsOpaqueImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetPaletteSizeSEL, (void*)CFS_DIBitmap_V1::GetPaletteSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetPaletteArgbSEL, (void*)CFS_DIBitmap_V1::GetPaletteArgb);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapSetPaletteArgbSEL, (void*)CFS_DIBitmap_V1::SetPaletteArgb);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapCloneSEL, (void*)CFS_DIBitmap_V1::Clone);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapCloneConvertSEL, (void*)CFS_DIBitmap_V1::CloneConvert);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetAlphaMaskSEL, (void*)CFS_DIBitmap_V1::GetAlphaMask);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapStretchToSEL, (void*)CFS_DIBitmap_V1::StretchTo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapTransformToSEL, (void*)CFS_DIBitmap_V1::TransformTo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapSwapXYSEL, (void*)CFS_DIBitmap_V1::SwapXY);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapFlipImageSEL, (void*)CFS_DIBitmap_V1::FlipImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapLoadFromPNGIconSEL, (void*)CFS_DIBitmap_V1::LoadFromPNGIcon);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapLoadFromPNGIcon2SEL, (void*)CFS_DIBitmap_V1::LoadFromPNGIcon2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapCopySEL, (void*)CFS_DIBitmap_V1::Copy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapLoadInfoSEL, (void*)CFS_DIBitmap_V1::LoadInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapDestroyDIBAttributeSEL, (void*)CFS_DIBitmap_V1::DestroyDIBAttribute);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetXDPISEL, (void*)CFS_DIBitmap_V1::GetXDPI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetYDPISEL, (void*)CFS_DIBitmap_V1::GetYDPI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetDPIUnitSEL, (void*)CFS_DIBitmap_V1::GetDPIUnit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetExifInfoSEL, (void*)CFS_DIBitmap_V1::GetExifInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapLoadFromPNGIcon3SEL, (void*)CFS_DIBitmap_V1::LoadFromPNGIcon3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapLoadFromImageSEL, (void*)CFS_DIBitmap_V1::LoadFromImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDIBitmapSEL, FSDIBitmapGetBitmapAlphaMaskSEL, (void*)CFS_DIBitmap_V1::GetBitmapAlphaMask);
	}
};

class CFS_MapPtrToPtr_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapPtrToPtrSEL, FSMapPtrToPtrNewSEL, (void*)CFS_MapPtrToPtr_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapPtrToPtrSEL, FSMapPtrToPtrDestroySEL, (void*)CFS_MapPtrToPtr_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapPtrToPtrSEL, FSMapPtrToPtrGetCountSEL, (void*)CFS_MapPtrToPtr_V1::GetCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapPtrToPtrSEL, FSMapPtrToPtrIsEmptySEL, (void*)CFS_MapPtrToPtr_V1::IsEmpty);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapPtrToPtrSEL, FSMapPtrToPtrLookupSEL, (void*)CFS_MapPtrToPtr_V1::Lookup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapPtrToPtrSEL, FSMapPtrToPtrGetValueAtSEL, (void*)CFS_MapPtrToPtr_V1::GetValueAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapPtrToPtrSEL, FSMapPtrToPtrSetAtSEL, (void*)CFS_MapPtrToPtr_V1::SetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapPtrToPtrSEL, FSMapPtrToPtrRemoveKeySEL, (void*)CFS_MapPtrToPtr_V1::RemoveKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapPtrToPtrSEL, FSMapPtrToPtrRemoveAllSEL, (void*)CFS_MapPtrToPtr_V1::RemoveAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapPtrToPtrSEL, FSMapPtrToPtrGetStartPositionSEL, (void*)CFS_MapPtrToPtr_V1::GetStartPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapPtrToPtrSEL, FSMapPtrToPtrGetNextAssocSEL, (void*)CFS_MapPtrToPtr_V1::GetNextAssoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapPtrToPtrSEL, FSMapPtrToPtrGetHashTableSizeSEL, (void*)CFS_MapPtrToPtr_V1::GetHashTableSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapPtrToPtrSEL, FSMapPtrToPtrInitHashTableSEL, (void*)CFS_MapPtrToPtr_V1::InitHashTable);
	}
};

class CFS_PtrArray_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArrayNewSEL, (void*)CFS_PtrArray_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArrayDestroySEL, (void*)CFS_PtrArray_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArrayGetSizeSEL, (void*)CFS_PtrArray_V1::GetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArrayGetUpperBoundSEL, (void*)CFS_PtrArray_V1::GetUpperBound);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArraySetSizeSEL, (void*)CFS_PtrArray_V1::SetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArrayRemoveAllSEL, (void*)CFS_PtrArray_V1::RemoveAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArrayGetAtSEL, (void*)CFS_PtrArray_V1::GetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArraySetAtSEL, (void*)CFS_PtrArray_V1::SetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArraySetAtGrowSEL, (void*)CFS_PtrArray_V1::SetAtGrow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArrayAddSEL, (void*)CFS_PtrArray_V1::Add);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArrayAppendSEL, (void*)CFS_PtrArray_V1::Append);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArrayCopySEL, (void*)CFS_PtrArray_V1::Copy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArrayGetDataPtrSEL, (void*)CFS_PtrArray_V1::GetDataPtr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArrayInsertAtSEL, (void*)CFS_PtrArray_V1::InsertAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArrayRemoveAtSEL, (void*)CFS_PtrArray_V1::RemoveAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArrayInsertNewArrayAtSEL, (void*)CFS_PtrArray_V1::InsertNewArrayAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPtrArraySEL, FSPtrArrayFindSEL, (void*)CFS_PtrArray_V1::Find);
	}
};

class CFS_ByteArray_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArrayNewSEL, (void*)CFS_ByteArray_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArrayDestroySEL, (void*)CFS_ByteArray_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArrayGetSizeSEL, (void*)CFS_ByteArray_V1::GetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArrayGetUpperBoundSEL, (void*)CFS_ByteArray_V1::GetUpperBound);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArraySetSizeSEL, (void*)CFS_ByteArray_V1::SetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArrayRemoveAllSEL, (void*)CFS_ByteArray_V1::RemoveAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArrayGetAtSEL, (void*)CFS_ByteArray_V1::GetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArraySetAtSEL, (void*)CFS_ByteArray_V1::SetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArraySetAtGrowSEL, (void*)CFS_ByteArray_V1::SetAtGrow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArrayAddSEL, (void*)CFS_ByteArray_V1::Add);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArrayAppendSEL, (void*)CFS_ByteArray_V1::Append);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArrayCopySEL, (void*)CFS_ByteArray_V1::Copy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArrayGetDataPtrSEL, (void*)CFS_ByteArray_V1::GetDataPtr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArrayInsertAtSEL, (void*)CFS_ByteArray_V1::InsertAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArrayRemoveAtSEL, (void*)CFS_ByteArray_V1::RemoveAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArrayInsertNewArrayAtSEL, (void*)CFS_ByteArray_V1::InsertNewArrayAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteArraySEL, FSByteArrayFindSEL, (void*)CFS_ByteArray_V1::Find);
	}
};

class CFS_WordArray_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArrayNewSEL, (void*)CFS_WordArray_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArrayDestroySEL, (void*)CFS_WordArray_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArrayGetSizeSEL, (void*)CFS_WordArray_V1::GetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArrayGetUpperBoundSEL, (void*)CFS_WordArray_V1::GetUpperBound);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArraySetSizeSEL, (void*)CFS_WordArray_V1::SetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArrayRemoveAllSEL, (void*)CFS_WordArray_V1::RemoveAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArrayGetAtSEL, (void*)CFS_WordArray_V1::GetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArraySetAtSEL, (void*)CFS_WordArray_V1::SetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArraySetAtGrowSEL, (void*)CFS_WordArray_V1::SetAtGrow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArrayAddSEL, (void*)CFS_WordArray_V1::Add);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArrayAppendSEL, (void*)CFS_WordArray_V1::Append);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArrayCopySEL, (void*)CFS_WordArray_V1::Copy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArrayGetDataPtrSEL, (void*)CFS_WordArray_V1::GetDataPtr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArrayInsertAtSEL, (void*)CFS_WordArray_V1::InsertAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArrayRemoveAtSEL, (void*)CFS_WordArray_V1::RemoveAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArrayInsertNewArrayAtSEL, (void*)CFS_WordArray_V1::InsertNewArrayAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWordArraySEL, FSWordArrayFindSEL, (void*)CFS_WordArray_V1::Find);
	}
};

class CFS_DWordArray_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArrayNewSEL, (void*)CFS_DWordArray_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArrayDestroySEL, (void*)CFS_DWordArray_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArrayGetSizeSEL, (void*)CFS_DWordArray_V1::GetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArrayGetUpperBoundSEL, (void*)CFS_DWordArray_V1::GetUpperBound);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArraySetSizeSEL, (void*)CFS_DWordArray_V1::SetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArrayRemoveAllSEL, (void*)CFS_DWordArray_V1::RemoveAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArrayGetAtSEL, (void*)CFS_DWordArray_V1::GetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArraySetAtSEL, (void*)CFS_DWordArray_V1::SetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArraySetAtGrowSEL, (void*)CFS_DWordArray_V1::SetAtGrow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArrayAddSEL, (void*)CFS_DWordArray_V1::Add);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArrayAppendSEL, (void*)CFS_DWordArray_V1::Append);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArrayCopySEL, (void*)CFS_DWordArray_V1::Copy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArrayGetDataPtrSEL, (void*)CFS_DWordArray_V1::GetDataPtr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArrayInsertAtSEL, (void*)CFS_DWordArray_V1::InsertAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArrayRemoveAtSEL, (void*)CFS_DWordArray_V1::RemoveAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArrayInsertNewArrayAtSEL, (void*)CFS_DWordArray_V1::InsertNewArrayAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSDWordArraySEL, FSDWordArrayFindSEL, (void*)CFS_DWordArray_V1::Find);
	}
};

class CFS_ByteStringArray_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringArraySEL, FSByteStringArrayNewSEL, (void*)CFS_ByteStringArray_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringArraySEL, FSByteStringArrayDestroySEL, (void*)CFS_ByteStringArray_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringArraySEL, FSByteStringArrayGetAtSEL, (void*)CFS_ByteStringArray_V1::GetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringArraySEL, FSByteStringArrayGetSizeSEL, (void*)CFS_ByteStringArray_V1::GetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringArraySEL, FSByteStringArrayRemoveAllSEL, (void*)CFS_ByteStringArray_V1::RemoveAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringArraySEL, FSByteStringArrayAddSEL, (void*)CFS_ByteStringArray_V1::Add);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringArraySEL, FSByteStringArrayRemoveAtSEL, (void*)CFS_ByteStringArray_V1::RemoveAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringArraySEL, FSByteStringArrayAdd2SEL, (void*)CFS_ByteStringArray_V1::Add2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringArraySEL, FSByteStringArrayGetDataPtrSEL, (void*)CFS_ByteStringArray_V1::GetDataPtr);
	}
};

class CFS_WideStringArray_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringArraySEL, FSWideStringArrayNewSEL, (void*)CFS_WideStringArray_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringArraySEL, FSWideStringArrayDestroySEL, (void*)CFS_WideStringArray_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringArraySEL, FSWideStringArrayGetAtSEL, (void*)CFS_WideStringArray_V1::GetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringArraySEL, FSWideStringArrayGetSizeSEL, (void*)CFS_WideStringArray_V1::GetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringArraySEL, FSWideStringArrayRemoveAllSEL, (void*)CFS_WideStringArray_V1::RemoveAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringArraySEL, FSWideStringArrayAddSEL, (void*)CFS_WideStringArray_V1::Add);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringArraySEL, FSWideStringArrayRemoveAtSEL, (void*)CFS_WideStringArray_V1::RemoveAt);
	}
};

class CFS_CodeTransformation_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCodeTransformationSEL, FSCodeTransformationNameDecodeSEL, (void*)CFS_CodeTransformation_V1::NameDecode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCodeTransformationSEL, FSCodeTransformationNameEncodeSEL, (void*)CFS_CodeTransformation_V1::NameEncode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCodeTransformationSEL, FSCodeTransformationEncodeStringSEL, (void*)CFS_CodeTransformation_V1::EncodeString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCodeTransformationSEL, FSCodeTransformationDecodeTextSEL, (void*)CFS_CodeTransformation_V1::DecodeText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCodeTransformationSEL, FSCodeTransformationDecodeText2SEL, (void*)CFS_CodeTransformation_V1::DecodeText2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCodeTransformationSEL, FSCodeTransformationEncodeTextSEL, (void*)CFS_CodeTransformation_V1::EncodeText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCodeTransformationSEL, FSCodeTransformationFlateEncodeProcSEL, (void*)CFS_CodeTransformation_V1::FlateEncodeProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCodeTransformationSEL, FSCodeTransformationFlateDecodeProcSEL, (void*)CFS_CodeTransformation_V1::FlateDecodeProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCodeTransformationSEL, FSCodeTransformationRunLengthDecodeProcSEL, (void*)CFS_CodeTransformation_V1::RunLengthDecodeProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCodeTransformationSEL, FSCodeTransformationRunLengthEncodeProcSEL, (void*)CFS_CodeTransformation_V1::RunLengthEncodeProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCodeTransformationSEL, FSCodeTransformationMemFreeSEL, (void*)CFS_CodeTransformation_V1::MemFree);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCodeTransformationSEL, FSCodeTransformationBasicModuleA85EncodeSEL, (void*)CFS_CodeTransformation_V1::BasicModuleA85Encode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCodeTransformationSEL, FSCodeTransformationFlateModuleEncodeSEL, (void*)CFS_CodeTransformation_V1::FlateModuleEncode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCodeTransformationSEL, FSCodeTransformationFlateModuleEncode2SEL, (void*)CFS_CodeTransformation_V1::FlateModuleEncode2);
	}
};

class CFS_FloatRectArray_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatRectArraySEL, FSFloatRectArrayNewSEL, (void*)CFS_FloatRectArray_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatRectArraySEL, FSFloatRectArrayDestroySEL, (void*)CFS_FloatRectArray_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatRectArraySEL, FSFloatRectArrayGetSizeSEL, (void*)CFS_FloatRectArray_V1::GetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatRectArraySEL, FSFloatRectArrayGetUpperBoundSEL, (void*)CFS_FloatRectArray_V1::GetUpperBound);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatRectArraySEL, FSFloatRectArraySetSizeSEL, (void*)CFS_FloatRectArray_V1::SetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatRectArraySEL, FSFloatRectArrayRemoveAllSEL, (void*)CFS_FloatRectArray_V1::RemoveAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatRectArraySEL, FSFloatRectArrayGetAtSEL, (void*)CFS_FloatRectArray_V1::GetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatRectArraySEL, FSFloatRectArraySetAtSEL, (void*)CFS_FloatRectArray_V1::SetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatRectArraySEL, FSFloatRectArraySetAtGrowSEL, (void*)CFS_FloatRectArray_V1::SetAtGrow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatRectArraySEL, FSFloatRectArrayAddSEL, (void*)CFS_FloatRectArray_V1::Add);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatRectArraySEL, FSFloatRectArrayAppendSEL, (void*)CFS_FloatRectArray_V1::Append);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatRectArraySEL, FSFloatRectArrayCopySEL, (void*)CFS_FloatRectArray_V1::Copy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatRectArraySEL, FSFloatRectArrayInsertAtSEL, (void*)CFS_FloatRectArray_V1::InsertAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatRectArraySEL, FSFloatRectArrayRemoveAtSEL, (void*)CFS_FloatRectArray_V1::RemoveAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatRectArraySEL, FSFloatRectArrayInsertNewArrayAtSEL, (void*)CFS_FloatRectArray_V1::InsertNewArrayAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatRectArraySEL, FSFloatRectArrayFindSEL, (void*)CFS_FloatRectArray_V1::Find);
	}
};

class CFS_BinaryBuf_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufNewSEL, (void*)CFS_BinaryBuf_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufDestroySEL, (void*)CFS_BinaryBuf_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufClearSEL, (void*)CFS_BinaryBuf_V1::Clear);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufEstimateSizeSEL, (void*)CFS_BinaryBuf_V1::EstimateSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufAppendBlockSEL, (void*)CFS_BinaryBuf_V1::AppendBlock);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufAppendFillSEL, (void*)CFS_BinaryBuf_V1::AppendFill);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufAppendStringSEL, (void*)CFS_BinaryBuf_V1::AppendString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufAppendByteSEL, (void*)CFS_BinaryBuf_V1::AppendByte);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufInsertBlockSEL, (void*)CFS_BinaryBuf_V1::InsertBlock);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufAttachDataSEL, (void*)CFS_BinaryBuf_V1::AttachData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufCopyDataSEL, (void*)CFS_BinaryBuf_V1::CopyData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufTakeOverSEL, (void*)CFS_BinaryBuf_V1::TakeOver);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufDeleteSEL, (void*)CFS_BinaryBuf_V1::Delete);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufGetBufferSEL, (void*)CFS_BinaryBuf_V1::GetBuffer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufGetSizeSEL, (void*)CFS_BinaryBuf_V1::GetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufGetByteStringSEL, (void*)CFS_BinaryBuf_V1::GetByteString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBinaryBufSEL, FSBinaryBufDetachBufferSEL, (void*)CFS_BinaryBuf_V1::DetachBuffer);
	}
};

class CFS_PauseHandler_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPauseHandlerSEL, FSPauseHandlerCreateSEL, (void*)CFS_PauseHandler_V1::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSPauseHandlerSEL, FSPauseHandlerDestroySEL, (void*)CFS_PauseHandler_V1::Destroy);
	}
};

class CFS_FileReadHandler_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFileReadHandlerSEL, FSFileReadHandlerNewSEL, (void*)CFS_FileReadHandler_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFileReadHandlerSEL, FSFileReadHandlerDestroySEL, (void*)CFS_FileReadHandler_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFileReadHandlerSEL, FSFileReadHandlerGetSizeSEL, (void*)CFS_FileReadHandler_V1::GetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFileReadHandlerSEL, FSFileReadHandlerReadBlockSEL, (void*)CFS_FileReadHandler_V1::ReadBlock);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFileReadHandlerSEL, FSFileReadHandlerNew2SEL, (void*)CFS_FileReadHandler_V1::New2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFileReadHandlerSEL, FSFileReadHandlerNew3SEL, (void*)CFS_FileReadHandler_V1::New3);
	}
};

class CFS_StreamWriteHandler_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSStreamWriteHandlerSEL, FSStreamWriteHandlerNewSEL, (void*)CFS_StreamWriteHandler_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSStreamWriteHandlerSEL, FSStreamWriteHandlerDestroySEL, (void*)CFS_StreamWriteHandler_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSStreamWriteHandlerSEL, FSStreamWriteHandlerWriteBlockSEL, (void*)CFS_StreamWriteHandler_V1::WriteBlock);
	}
};

// fs_basicImpl.h end

// In file fs_stringImpl.h
class CFS_CharMap_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCharMapSEL, FSCharMapNewSEL, (void*)CFS_CharMap_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCharMapSEL, FSCharMapReleaseSEL, (void*)CFS_CharMap_V1::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCharMapSEL, FSCharMapGetDefaultMapperSEL, (void*)CFS_CharMap_V1::GetDefaultMapper);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSCharMapSEL, FSCharMapGetDefaultMapper2SEL, (void*)CFS_CharMap_V1::GetDefaultMapper2);
	}
};

class CFS_ByteString_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringNewSEL, (void*)CFS_ByteString_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringNew2SEL, (void*)CFS_ByteString_V1::New2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringNew3SEL, (void*)CFS_ByteString_V1::New3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringNew4SEL, (void*)CFS_ByteString_V1::New4);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringFromUnicodeSEL, (void*)CFS_ByteString_V1::FromUnicode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringFromUnicode2SEL, (void*)CFS_ByteString_V1::FromUnicode2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringDestroySEL, (void*)CFS_ByteString_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringGetLengthSEL, (void*)CFS_ByteString_V1::GetLength);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringIsEmptySEL, (void*)CFS_ByteString_V1::IsEmpty);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringCompareSEL, (void*)CFS_ByteString_V1::Compare);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringEqualSEL, (void*)CFS_ByteString_V1::Equal);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringEqualNoCaseSEL, (void*)CFS_ByteString_V1::EqualNoCase);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringCopySEL, (void*)CFS_ByteString_V1::Copy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringFillSEL, (void*)CFS_ByteString_V1::Fill);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringConcatSEL, (void*)CFS_ByteString_V1::Concat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringConcat2SEL, (void*)CFS_ByteString_V1::Concat2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringEmptySEL, (void*)CFS_ByteString_V1::Empty);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringGetAtSEL, (void*)CFS_ByteString_V1::GetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringSetAtSEL, (void*)CFS_ByteString_V1::SetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringInsertSEL, (void*)CFS_ByteString_V1::Insert);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringDeleteSEL, (void*)CFS_ByteString_V1::Delete);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringFormatSEL, (void*)CFS_ByteString_V1::Format);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringReserveSEL, (void*)CFS_ByteString_V1::Reserve);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringMidSEL, (void*)CFS_ByteString_V1::Mid);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringMid2SEL, (void*)CFS_ByteString_V1::Mid2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringLeftSEL, (void*)CFS_ByteString_V1::Left);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringRightSEL, (void*)CFS_ByteString_V1::Right);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringFindSEL, (void*)CFS_ByteString_V1::Find);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringFind2SEL, (void*)CFS_ByteString_V1::Find2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringMakeLowerSEL, (void*)CFS_ByteString_V1::MakeLower);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringMakeUpperSEL, (void*)CFS_ByteString_V1::MakeUpper);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringTrimRightSEL, (void*)CFS_ByteString_V1::TrimRight);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringTrimRight2SEL, (void*)CFS_ByteString_V1::TrimRight2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringTrimRight3SEL, (void*)CFS_ByteString_V1::TrimRight3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringTrimLeftSEL, (void*)CFS_ByteString_V1::TrimLeft);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringTrimLeft2SEL, (void*)CFS_ByteString_V1::TrimLeft2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringTrimLeft3SEL, (void*)CFS_ByteString_V1::TrimLeft3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringReplaceSEL, (void*)CFS_ByteString_V1::Replace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringRemoveSEL, (void*)CFS_ByteString_V1::Remove);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringGetIDSEL, (void*)CFS_ByteString_V1::GetID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringFormatIntegerSEL, (void*)CFS_ByteString_V1::FormatInteger);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringFormatFloatSEL, (void*)CFS_ByteString_V1::FormatFloat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringCastToLPCSTRSEL, (void*)CFS_ByteString_V1::CastToLPCSTR);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringUTF8DecodeSEL, (void*)CFS_ByteString_V1::UTF8Decode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringFormatVSEL, (void*)CFS_ByteString_V1::FormatV);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringCastToLPCBYTESEL, (void*)CFS_ByteString_V1::CastToLPCBYTE);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSByteStringSEL, FSByteStringConvertFromSEL, (void*)CFS_ByteString_V1::ConvertFrom);
	}
};

class CFS_WideString_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringNewSEL, (void*)CFS_WideString_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringNew2SEL, (void*)CFS_WideString_V1::New2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringNew3SEL, (void*)CFS_WideString_V1::New3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringDestroySEL, (void*)CFS_WideString_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringGetLengthSEL, (void*)CFS_WideString_V1::GetLength);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringIsEmptySEL, (void*)CFS_WideString_V1::IsEmpty);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringCompareSEL, (void*)CFS_WideString_V1::Compare);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringCompare2SEL, (void*)CFS_WideString_V1::Compare2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringEqualSEL, (void*)CFS_WideString_V1::Equal);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringEqual2SEL, (void*)CFS_WideString_V1::Equal2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringCopySEL, (void*)CFS_WideString_V1::Copy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringFillSEL, (void*)CFS_WideString_V1::Fill);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringConcatSEL, (void*)CFS_WideString_V1::Concat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringConcat2SEL, (void*)CFS_WideString_V1::Concat2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringEmptySEL, (void*)CFS_WideString_V1::Empty);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringGetAtSEL, (void*)CFS_WideString_V1::GetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringSetAtSEL, (void*)CFS_WideString_V1::SetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringInsertSEL, (void*)CFS_WideString_V1::Insert);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringDeleteSEL, (void*)CFS_WideString_V1::Delete);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringFormatSEL, (void*)CFS_WideString_V1::Format);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringReserveSEL, (void*)CFS_WideString_V1::Reserve);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringMidSEL, (void*)CFS_WideString_V1::Mid);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringMid2SEL, (void*)CFS_WideString_V1::Mid2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringLeftSEL, (void*)CFS_WideString_V1::Left);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringRightSEL, (void*)CFS_WideString_V1::Right);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringFindSEL, (void*)CFS_WideString_V1::Find);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringFind2SEL, (void*)CFS_WideString_V1::Find2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringMakeLowerSEL, (void*)CFS_WideString_V1::MakeLower);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringMakeUpperSEL, (void*)CFS_WideString_V1::MakeUpper);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringTrimRightSEL, (void*)CFS_WideString_V1::TrimRight);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringTrimRight2SEL, (void*)CFS_WideString_V1::TrimRight2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringTrimRight3SEL, (void*)CFS_WideString_V1::TrimRight3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringTrimLeftSEL, (void*)CFS_WideString_V1::TrimLeft);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringTrimLeft2SEL, (void*)CFS_WideString_V1::TrimLeft2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringTrimLeft3SEL, (void*)CFS_WideString_V1::TrimLeft3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringReplaceSEL, (void*)CFS_WideString_V1::Replace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringRemoveSEL, (void*)CFS_WideString_V1::Remove);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringGetIntegerSEL, (void*)CFS_WideString_V1::GetInteger);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringFromUTF8SEL, (void*)CFS_WideString_V1::FromUTF8);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringFromUTF16LESEL, (void*)CFS_WideString_V1::FromUTF16LE);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringUTF8EncodeSEL, (void*)CFS_WideString_V1::UTF8Encode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringUTF16LE_EncodeSEL, (void*)CFS_WideString_V1::UTF16LE_Encode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringCastToLPCWSTRSEL, (void*)CFS_WideString_V1::CastToLPCWSTR);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringFormatVSEL, (void*)CFS_WideString_V1::FormatV);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringFromLocalSEL, (void*)CFS_WideString_V1::FromLocal);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringFromLocal2SEL, (void*)CFS_WideString_V1::FromLocal2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringCompareNoCaseSEL, (void*)CFS_WideString_V1::CompareNoCase);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringCompareNoCase2SEL, (void*)CFS_WideString_V1::CompareNoCase2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSWideStringSEL, FSWideStringConvertFromSEL, (void*)CFS_WideString_V1::ConvertFrom);
	}
};

// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
class CFR_Tool_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolSEL, FRToolNewSEL, (void*)CFR_Tool_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolSEL, FRToolReleaseSEL, (void*)CFR_Tool_V1::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolSEL, FRToolGetNameSEL, (void*)CFR_Tool_V1::GetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolSEL, FRToolSetAssociatedMousePtHandlerTypeSEL, (void*)CFR_Tool_V1::SetAssociatedMousePtHandlerType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolSEL, FRToolSetAssociatedSelectionHandlerTypeSEL, (void*)CFR_Tool_V1::SetAssociatedSelectionHandlerType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolSEL, FRToolTool_GetBeginPointSEL, (void*)CFR_Tool_V1::Tool_GetBeginPoint);
	}
};

class CFR_App_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetNameSEL, (void*)CFR_App_V1::GetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetVersionSEL, (void*)CFR_App_V1::GetVersion);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetAppTitleSEL, (void*)CFR_App_V1::GetAppTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetAppDataPathSEL, (void*)CFR_App_V1::GetAppDataPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCanQuitSEL, (void*)CFR_App_V1::CanQuit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCountToolbarsSEL, (void*)CFR_App_V1::CountToolbars);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetToolBarSEL, (void*)CFR_App_V1::GetToolBar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetToolBarByNameSEL, (void*)CFR_App_V1::GetToolBarByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetMenuBarSEL, (void*)CFR_App_V1::GetMenuBar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppShowMenuBarSEL, (void*)CFR_App_V1::ShowMenuBar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterToolSEL, (void*)CFR_App_V1::RegisterTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetToolByNameSEL, (void*)CFR_App_V1::GetToolByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCountToolsSEL, (void*)CFR_App_V1::CountTools);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetToolByIndexSEL, (void*)CFR_App_V1::GetToolByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppSetActiveToolSEL, (void*)CFR_App_V1::SetActiveTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetActiveToolSEL, (void*)CFR_App_V1::GetActiveTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterPreferencePageHandlerSEL, (void*)CFR_App_V1::RegisterPreferencePageHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppAddPreferencePageSEL, (void*)CFR_App_V1::AddPreferencePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterDocPropertyPageHandlerSEL, (void*)CFR_App_V1::RegisterDocPropertyPageHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppAddDocPropertyPageSEL, (void*)CFR_App_V1::AddDocPropertyPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterNavPanelViewSEL, (void*)CFR_App_V1::RegisterNavPanelView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterAppEventHandlerSEL, (void*)CFR_App_V1::RegisterAppEventHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCountDocsOfPDDocSEL, (void*)CFR_App_V1::CountDocsOfPDDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetDocOfPDDocSEL, (void*)CFR_App_V1::GetDocOfPDDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetActiveDocOfPDDocSEL, (void*)CFR_App_V1::GetActiveDocOfPDDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppSetActiveDocOfPDDocSEL, (void*)CFR_App_V1::SetActiveDocOfPDDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterDocHandlerOfPDDocSEL, (void*)CFR_App_V1::RegisterDocHandlerOfPDDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppIsFullScreenSEL, (void*)CFR_App_V1::IsFullScreen);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppShowFullScreenSEL, (void*)CFR_App_V1::ShowFullScreen);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppEndFullScreenSEL, (void*)CFR_App_V1::EndFullScreen);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetMousePosSEL, (void*)CFR_App_V1::GetMousePos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppModalWindowIsOpenSEL, (void*)CFR_App_V1::ModalWindowIsOpen);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterSecurityHandlerSEL, (void*)CFR_App_V1::RegisterSecurityHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppUnRegisterSecurityHandlerSEL, (void*)CFR_App_V1::UnRegisterSecurityHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetPassphraseSEL, (void*)CFR_App_V1::GetPassphrase);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterContentProviderSEL, (void*)CFR_App_V1::RegisterContentProvider);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetMainFrameWndSEL, (void*)CFR_App_V1::GetMainFrameWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppAddUndoRedoItemSEL, (void*)CFR_App_V1::AddUndoRedoItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterPageHandlerOfPDDocSEL, (void*)CFR_App_V1::RegisterPageHandlerOfPDDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterForContextMenuAdditionSEL, (void*)CFR_App_V1::RegisterForContextMenuAddition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterSelectionHandlerSEL, (void*)CFR_App_V1::RegisterSelectionHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppUnRegisterSelectionHandlerSEL, (void*)CFR_App_V1::UnRegisterSelectionHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterCaptureHandlerSEL, (void*)CFR_App_V1::RegisterCaptureHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppUnRegisterCaptureHandlerSEL, (void*)CFR_App_V1::UnRegisterCaptureHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegistryGetProfilePathSEL, (void*)CFR_App_V1::RegistryGetProfilePath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegistryWriteIntSEL, (void*)CFR_App_V1::RegistryWriteInt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegistryWriteBinarySEL, (void*)CFR_App_V1::RegistryWriteBinary);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegistryWriteStringSEL, (void*)CFR_App_V1::RegistryWriteString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegistryGetIntSEL, (void*)CFR_App_V1::RegistryGetInt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegistryGetBinarySEL, (void*)CFR_App_V1::RegistryGetBinary);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegistryGetStringSEL, (void*)CFR_App_V1::RegistryGetString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegistryGetEntryCountSEL, (void*)CFR_App_V1::RegistryGetEntryCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegistryGetEntryNameSEL, (void*)CFR_App_V1::RegistryGetEntryName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegistryRemoveSectionSEL, (void*)CFR_App_V1::RegistryRemoveSection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegistryRemoveEntrySEL, (void*)CFR_App_V1::RegistryRemoveEntry);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetGEModuleSEL, (void*)CFR_App_V1::GetGEModule);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetModuleMgrSEL, (void*)CFR_App_V1::GetModuleMgr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCreateAnEmptyFrameWndSEL, (void*)CFR_App_V1::CreateAnEmptyFrameWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppIsRunEmbeddedSEL, (void*)CFR_App_V1::IsRunEmbedded);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterExtraPrintInfoProviderSEL, (void*)CFR_App_V1::RegisterExtraPrintInfoProvider);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppIsRibbonModeSEL, (void*)CFR_App_V1::IsRibbonMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetStartMenuOfTabbedToobarModeSEL, (void*)CFR_App_V1::GetStartMenuOfTabbedToobarMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterTaskPaneViewSEL, (void*)CFR_App_V1::RegisterTaskPaneView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppShowTaskPaneSEL, (void*)CFR_App_V1::ShowTaskPane);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetTaskPaneWndHandleByDocSEL, (void*)CFR_App_V1::GetTaskPaneWndHandleByDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCommandLineHasSwitchSEL, (void*)CFR_App_V1::CommandLineHasSwitch);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCommandLineGetSafeArgumentSEL, (void*)CFR_App_V1::CommandLineGetSafeArgument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCommandLineGetArgumentSEL, (void*)CFR_App_V1::CommandLineGetArgument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCommandLineGetArgumentCountSEL, (void*)CFR_App_V1::CommandLineGetArgumentCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterWinMSGHandlerSEL, (void*)CFR_App_V1::RegisterWinMSGHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetRecentFileListSEL, (void*)CFR_App_V1::GetRecentFileList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppAddFileToRecentFileListSEL, (void*)CFR_App_V1::AddFileToRecentFileList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppClearRecentFileListSEL, (void*)CFR_App_V1::ClearRecentFileList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterSecurityMethodSEL, (void*)CFR_App_V1::RegisterSecurityMethod);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppPopDocPropertyPageSEL, (void*)CFR_App_V1::PopDocPropertyPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCountDocPropertyPagesSEL, (void*)CFR_App_V1::CountDocPropertyPages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCheckModuleLicenseSEL, (void*)CFR_App_V1::CheckModuleLicense);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterOwnerFileTypeSEL, (void*)CFR_App_V1::RegisterOwnerFileType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegistryGetKeyCountsSEL, (void*)CFR_App_V1::RegistryGetKeyCounts);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegistryGetKeyNameSEL, (void*)CFR_App_V1::RegistryGetKeyName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetRibbonBarSEL, (void*)CFR_App_V1::GetRibbonBar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetRibbonBarBackGroundColorSEL, (void*)CFR_App_V1::GetRibbonBarBackGroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetRibbonBarBtnBackGroundColorSEL, (void*)CFR_App_V1::GetRibbonBarBtnBackGroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppLoadLibrarySEL, (void*)CFR_App_V1::LoadLibrary);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppIsCurDocShowInBrowserSEL, (void*)CFR_App_V1::IsCurDocShowInBrowser);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRemoveFileFromRecentFileListSEL, (void*)CFR_App_V1::RemoveFileFromRecentFileList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCreateCustomRecentFileListSEL, (void*)CFR_App_V1::CreateCustomRecentFileList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRemoveFileFromCustomRecentFileListSEL, (void*)CFR_App_V1::RemoveFileFromCustomRecentFileList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppAddFileToCustomRecentFileListSEL, (void*)CFR_App_V1::AddFileToCustomRecentFileList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppClearCustomRecentFileListSEL, (void*)CFR_App_V1::ClearCustomRecentFileList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterMousePtHandlerSEL, (void*)CFR_App_V1::RegisterMousePtHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppUnRegisterMousePtHandlerSEL, (void*)CFR_App_V1::UnRegisterMousePtHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppIsLicenseValidOrInTrialSEL, (void*)CFR_App_V1::IsLicenseValidOrInTrial);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppShowPreferenceDlgSEL, (void*)CFR_App_V1::ShowPreferenceDlg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppUndoRedoIsEditingSEL, (void*)CFR_App_V1::UndoRedoIsEditing);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppUndoRedoBeginEditSEL, (void*)CFR_App_V1::UndoRedoBeginEdit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppUndoRedoEndEditSEL, (void*)CFR_App_V1::UndoRedoEndEdit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterWndProviderSEL, (void*)CFR_App_V1::RegisterWndProvider);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppUnRegisterWndProviderSEL, (void*)CFR_App_V1::UnRegisterWndProvider);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetEditionTypeSEL, (void*)CFR_App_V1::GetEditionType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetRecentFileListSizeSEL, (void*)CFR_App_V1::GetRecentFileListSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetModuleFileNameSEL, (void*)CFR_App_V1::GetModuleFileName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetToolbarLockedSEL, (void*)CFR_App_V1::GetToolbarLocked);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppSetToolbarLockedSEL, (void*)CFR_App_V1::SetToolbarLocked);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterCmdMsgEventHandlerSEL, (void*)CFR_App_V1::RegisterCmdMsgEventHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppUnRegisterCmdMsgEventHandlerSEL, (void*)CFR_App_V1::UnRegisterCmdMsgEventHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRemoveTaskPanelByNameSEL, (void*)CFR_App_V1::RemoveTaskPanelByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppDisableAllPanelSEL, (void*)CFR_App_V1::DisableAllPanel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppIsDisableAllPanelSEL, (void*)CFR_App_V1::IsDisableAllPanel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetPageContextMenuPosSEL, (void*)CFR_App_V1::GetPageContextMenuPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppOnCmdMsgByNameSEL, (void*)CFR_App_V1::OnCmdMsgByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppSetOwnUndoModeSEL, (void*)CFR_App_V1::SetOwnUndoMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppExitOwnUndoModeSEL, (void*)CFR_App_V1::ExitOwnUndoMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterActionHandlerSEL, (void*)CFR_App_V1::RegisterActionHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRedrawRecentFileListSEL, (void*)CFR_App_V1::RedrawRecentFileList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppSetCustomRecentFileListLabelSEL, (void*)CFR_App_V1::SetCustomRecentFileListLabel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppSetCustomRecentFileListItemBitmapSEL, (void*)CFR_App_V1::SetCustomRecentFileListItemBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppSetCustomRecentFileListMaxSizeSEL, (void*)CFR_App_V1::SetCustomRecentFileListMaxSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppSetCustomRecentFileListItemTitleSEL, (void*)CFR_App_V1::SetCustomRecentFileListItemTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppSetCustomRecentFileListItemTooltipSEL, (void*)CFR_App_V1::SetCustomRecentFileListItemTooltip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetCustomRecentFileListItemCountSEL, (void*)CFR_App_V1::GetCustomRecentFileListItemCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppIsRTLLanguageSEL, (void*)CFR_App_V1::IsRTLLanguage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetOEMVersionSEL, (void*)CFR_App_V1::GetOEMVersion);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterDataCollectionHadlerSEL, (void*)CFR_App_V1::RegisterDataCollectionHadler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetUIParentWndByTaskPaneSEL, (void*)CFR_App_V1::GetUIParentWndByTaskPane);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppAdvWndSetCustomizeSEL, (void*)CFR_App_V1::AdvWndSetCustomize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetAdvWndSEL, (void*)CFR_App_V1::GetAdvWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppShowAdvWndSEL, (void*)CFR_App_V1::ShowAdvWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterPOEventHandlerSEL, (void*)CFR_App_V1::RegisterPOEventHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCreateBlankDocSEL, (void*)CFR_App_V1::CreateBlankDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetLocalFontNameSEL, (void*)CFR_App_V1::GetLocalFontName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppReLoadStartPageSEL, (void*)CFR_App_V1::ReLoadStartPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppSetRecentFileListImageByExtSEL, (void*)CFR_App_V1::SetRecentFileListImageByExt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetCurMeasurementUnitsSEL, (void*)CFR_App_V1::GetCurMeasurementUnits);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetNavPanelViewByNameSEL, (void*)CFR_App_V1::GetNavPanelViewByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetNavPanelNameByIndexSEL, (void*)CFR_App_V1::GetNavPanelNameByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetNavPanelCountSEL, (void*)CFR_App_V1::GetNavPanelCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetStartAppModeSEL, (void*)CFR_App_V1::GetStartAppMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRestartProcessSEL, (void*)CFR_App_V1::RestartProcess);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetStatusBarBkGroundColorSEL, (void*)CFR_App_V1::GetStatusBarBkGroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetStatusBarBkGroundPathSEL, (void*)CFR_App_V1::GetStatusBarBkGroundPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCountAllTypesVisibleDocumentsSEL, (void*)CFR_App_V1::CountAllTypesVisibleDocuments);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppAddFileToCustomRecentFileList2SEL, (void*)CFR_App_V1::AddFileToCustomRecentFileList2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterRibbonRecentFileEventHandlerSEL, (void*)CFR_App_V1::RegisterRibbonRecentFileEventHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppDestroyRibbonRecentFileEventHandlerSEL, (void*)CFR_App_V1::DestroyRibbonRecentFileEventHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppSetMetadataOptionSEL, (void*)CFR_App_V1::SetMetadataOption);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppSetSubscriptionProviderSEL, (void*)CFR_App_V1::SetSubscriptionProvider);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppDestroySubscriptionProviderSEL, (void*)CFR_App_V1::DestroySubscriptionProvider);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetKeyfileStartAndExpireDateSEL, (void*)CFR_App_V1::GetKeyfileStartAndExpireDate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetRibbonCategoryColorSEL, (void*)CFR_App_V1::GetRibbonCategoryColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetRibbonElementColorSEL, (void*)CFR_App_V1::GetRibbonElementColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetRibbonBaseBorderColorSEL, (void*)CFR_App_V1::GetRibbonBaseBorderColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetRecentFolderListSizeSEL, (void*)CFR_App_V1::GetRecentFolderListSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCreateEmptyFrameViewEventHandlerSEL, (void*)CFR_App_V1::CreateEmptyFrameViewEventHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppDestroyEmptyFrameViewEventHandlerSEL, (void*)CFR_App_V1::DestroyEmptyFrameViewEventHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCreateAnEmptyFrameWnd2SEL, (void*)CFR_App_V1::CreateAnEmptyFrameWnd2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppIsReadModeSEL, (void*)CFR_App_V1::IsReadMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppExitReadModeSEL, (void*)CFR_App_V1::ExitReadMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetCurrentChildFrmSEL, (void*)CFR_App_V1::GetCurrentChildFrm);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCollectDocActionDataSEL, (void*)CFR_App_V1::CollectDocActionData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppActiveChildFrameSEL, (void*)CFR_App_V1::ActiveChildFrame);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCollectNormalDataSEL, (void*)CFR_App_V1::CollectNormalData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppSetMainframeShowSEL, (void*)CFR_App_V1::SetMainframeShow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetMainframeShowSEL, (void*)CFR_App_V1::GetMainframeShow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetRibbonTooltipBorderColorSEL, (void*)CFR_App_V1::GetRibbonTooltipBorderColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetRecentFileIndexSEL, (void*)CFR_App_V1::GetRecentFileIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppFZipUnzipSEL, (void*)CFR_App_V1::FZipUnzip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppIsJSEnabledSEL, (void*)CFR_App_V1::IsJSEnabled);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppSetEnableJSSEL, (void*)CFR_App_V1::SetEnableJS);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCollectNormalData2SEL, (void*)CFR_App_V1::CollectNormalData2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCollectBitmapDataSEL, (void*)CFR_App_V1::CollectBitmapData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppIsNeedCollectDataSEL, (void*)CFR_App_V1::IsNeedCollectData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppIsFipsModeSEL, (void*)CFR_App_V1::IsFipsMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppAddNavPanelViewSEL, (void*)CFR_App_V1::AddNavPanelView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppReleaseNavPanelViewSEL, (void*)CFR_App_V1::ReleaseNavPanelView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppAddFileAttachmentSEL, (void*)CFR_App_V1::AddFileAttachment);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppOpenFileAttachmentSEL, (void*)CFR_App_V1::OpenFileAttachment);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppIsPDF2DocSEL, (void*)CFR_App_V1::IsPDF2Doc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppConvertToPDFSEL, (void*)CFR_App_V1::ConvertToPDF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterPDFAPluginHandlerSEL, (void*)CFR_App_V1::RegisterPDFAPluginHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppConvertToPDFASEL, (void*)CFR_App_V1::ConvertToPDFA);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppAddLogSEL, (void*)CFR_App_V1::AddLog);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppIsReaderOnlyModeSEL, (void*)CFR_App_V1::IsReaderOnlyMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetMainFrameWndCountSEL, (void*)CFR_App_V1::GetMainFrameWndCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetMainFrameWndByIndexSEL, (void*)CFR_App_V1::GetMainFrameWndByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetModelessParentWndSEL, (void*)CFR_App_V1::GetModelessParentWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppIsMainfrmActivatingSEL, (void*)CFR_App_V1::IsMainfrmActivating);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetRibbonBar2SEL, (void*)CFR_App_V1::GetRibbonBar2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRefreshLayerPanelViewSEL, (void*)CFR_App_V1::RefreshLayerPanelView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterDRMSecurityHandlerSEL, (void*)CFR_App_V1::RegisterDRMSecurityHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppAddECMFileDialogSEL, (void*)CFR_App_V1::AddECMFileDialog);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppECMFileDialogAddMenuItemSEL, (void*)CFR_App_V1::ECMFileDialogAddMenuItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppECMFileDialogItemSelectedSEL, (void*)CFR_App_V1::ECMFileDialogItemSelected);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetECMFileDialogSEL, (void*)CFR_App_V1::GetECMFileDialog);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetSelectedECMPluginNameSEL, (void*)CFR_App_V1::GetSelectedECMPluginName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetMainClientAreaWndSEL, (void*)CFR_App_V1::GetMainClientAreaWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterPortfolioEventHandlerSEL, (void*)CFR_App_V1::RegisterPortfolioEventHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppAddECMUploadProcSEL, (void*)CFR_App_V1::AddECMUploadProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetConvertPDFErrMsgSEL, (void*)CFR_App_V1::GetConvertPDFErrMsg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppECMFileDialogUploadProcSEL, (void*)CFR_App_V1::ECMFileDialogUploadProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppECMFileDialogRemoveMenuItemSEL, (void*)CFR_App_V1::ECMFileDialogRemoveMenuItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCollectSensorDataSEL, (void*)CFR_App_V1::CollectSensorData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetSystemHandlerSEL, (void*)CFR_App_V1::GetSystemHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppCreateAndRegisterFormDesignerToolHandlerSEL, (void*)CFR_App_V1::CreateAndRegisterFormDesignerToolHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterFoxitSignHandlerSEL, (void*)CFR_App_V1::RegisterFoxitSignHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppShowModuleNoLicenseDlgSEL, (void*)CFR_App_V1::ShowModuleNoLicenseDlg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppDeleteAnnotSEL, (void*)CFR_App_V1::DeleteAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppDoModulePreferDlgSEL, (void*)CFR_App_V1::DoModulePreferDlg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterAnnotHandlerSEL, (void*)CFR_App_V1::RegisterAnnotHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppAddUndoItemCRSASEL, (void*)CFR_App_V1::AddUndoItemCRSA);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppBaseAnnotLoadCursorSEL, (void*)CFR_App_V1::BaseAnnotLoadCursor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppAddDragPointSEL, (void*)CFR_App_V1::AddDragPoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetSystemDefaultFontSEL, (void*)CFR_App_V1::GetSystemDefaultFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetRSAAnnotElementSEL, (void*)CFR_App_V1::GetRSAAnnotElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetPopupElementSEL, (void*)CFR_App_V1::GetPopupElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetJSEngineHRuntimeSEL, (void*)CFR_App_V1::GetJSEngineHRuntime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppUnRegisterDocHandlerOfPDDocSEL, (void*)CFR_App_V1::UnRegisterDocHandlerOfPDDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetOperatorPermissionSEL, (void*)CFR_App_V1::GetOperatorPermission);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetHContextSEL, (void*)CFR_App_V1::GetHContext);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppJSGetGlobalObjectSEL, (void*)CFR_App_V1::JSGetGlobalObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppJSSetUndefinedSEL, (void*)CFR_App_V1::JSSetUndefined);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppJSValueSetSEL, (void*)CFR_App_V1::JSValueSet);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppJSValueReleaseSEL, (void*)CFR_App_V1::JSValueRelease);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppRegisterTransitionHandlerSEL, (void*)CFR_App_V1::RegisterTransitionHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetPreferenceIdentitySEL, (void*)CFR_App_V1::GetPreferenceIdentity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppJSRuntimeNewContextSEL, (void*)CFR_App_V1::JSRuntimeNewContext);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppJSRuntimeReleaseContextSEL, (void*)CFR_App_V1::JSRuntimeReleaseContext);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppIsRunIPFrameSEL, (void*)CFR_App_V1::IsRunIPFrame);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppOwnerFileTypeHandlerDoEmailSEL, (void*)CFR_App_V1::OwnerFileTypeHandlerDoEmail);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetEmailTabDocInfoSEL, (void*)CFR_App_V1::GetEmailTabDocInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetAULIBCurrentLanguageSEL, (void*)CFR_App_V1::GetAULIBCurrentLanguage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppGetIsEmailChoosePageRangeSEL, (void*)CFR_App_V1::GetIsEmailChoosePageRange);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppSEL, FRAppAddECMFileDialog2SEL, (void*)CFR_App_V1::AddECMFileDialog2);
	}
};

class CFR_Language_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRLanguageSEL, FRLanguageCreateSEL, (void*)CFR_Language_V1::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRLanguageSEL, FRLanguageReleaseSEL, (void*)CFR_Language_V1::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRLanguageSEL, FRLanguageChangeSEL, (void*)CFR_Language_V1::Change);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRLanguageSEL, FRLanguageGetCurrentIDSEL, (void*)CFR_Language_V1::GetCurrentID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRLanguageSEL, FRLanguageTranslateMenuSEL, (void*)CFR_Language_V1::TranslateMenu);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRLanguageSEL, FRLanguageTranslateDialogSEL, (void*)CFR_Language_V1::TranslateDialog);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRLanguageSEL, FRLanguageLoadStringSEL, (void*)CFR_Language_V1::LoadString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRLanguageSEL, FRLanguageLoadVersionResSEL, (void*)CFR_Language_V1::LoadVersionRes);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRLanguageSEL, FRLanguageGetLocalLangNameSEL, (void*)CFR_Language_V1::GetLocalLangName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRLanguageSEL, FRLanguageTranslateDialog2SEL, (void*)CFR_Language_V1::TranslateDialog2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRLanguageSEL, FRLanguageModifyLogFontSEL, (void*)CFR_Language_V1::ModifyLogFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRLanguageSEL, FRLanguageFormatExSEL, (void*)CFR_Language_V1::FormatEx);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRLanguageSEL, FRLanguageJSPluginGetMessageSEL, (void*)CFR_Language_V1::JSPluginGetMessage);
	}
};

class CFR_UIProgress_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRUIProgressSEL, FRUIProgressCreateSEL, (void*)CFR_UIProgress_V1::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRUIProgressSEL, FRUIProgressSetTextSEL, (void*)CFR_UIProgress_V1::SetText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRUIProgressSEL, FRUIProgressSetRangeSEL, (void*)CFR_UIProgress_V1::SetRange);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRUIProgressSEL, FRUIProgressSetCurrValueSEL, (void*)CFR_UIProgress_V1::SetCurrValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRUIProgressSEL, FRUIProgressPeekAndPumpSEL, (void*)CFR_UIProgress_V1::PeekAndPump);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRUIProgressSEL, FRUIProgressIsCancelledSEL, (void*)CFR_UIProgress_V1::IsCancelled);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRUIProgressSEL, FRUIProgressGetCurrentValueSEL, (void*)CFR_UIProgress_V1::GetCurrentValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRUIProgressSEL, FRUIProgressDestroySEL, (void*)CFR_UIProgress_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRUIProgressSEL, FRUIProgressDoCancelSEL, (void*)CFR_UIProgress_V1::DoCancel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRUIProgressSEL, FRUIProgressCreate2SEL, (void*)CFR_UIProgress_V1::Create2);
	}
};

// fr_appImpl.h end

// In file fr_barImpl.h
class CFR_ToolButton_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonNewSEL, (void*)CFR_ToolButton_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonReleaseSEL, (void*)CFR_ToolButton_V1::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonGetNameSEL, (void*)CFR_ToolButton_V1::GetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonGetLabelTextSEL, (void*)CFR_ToolButton_V1::GetLabelText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonSetLabelTextSEL, (void*)CFR_ToolButton_V1::SetLabelText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonSetHelpTextSEL, (void*)CFR_ToolButton_V1::SetHelpText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonSetIconSEL, (void*)CFR_ToolButton_V1::SetIcon);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonIsSeparatorSEL, (void*)CFR_ToolButton_V1::IsSeparator);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonIsVisibleSEL, (void*)CFR_ToolButton_V1::IsVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonSetExecuteProcSEL, (void*)CFR_ToolButton_V1::SetExecuteProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonSetEnableProcSEL, (void*)CFR_ToolButton_V1::SetEnableProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonSetMarkedProcSEL, (void*)CFR_ToolButton_V1::SetMarkedProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonSetDropDownProcSEL, (void*)CFR_ToolButton_V1::SetDropDownProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonGetClientDataSEL, (void*)CFR_ToolButton_V1::GetClientData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonExecuteProcSEL, (void*)CFR_ToolButton_V1::ExecuteProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonSetFlyoutToolBarSEL, (void*)CFR_ToolButton_V1::SetFlyoutToolBar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonGetMapIdSEL, (void*)CFR_ToolButton_V1::GetMapId);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolButtonSEL, FRToolButtonSetClientDataSEL, (void*)CFR_ToolButton_V1::SetClientData);
	}
};

class CFR_ToolBar_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarNewSEL, (void*)CFR_ToolBar_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarNewFlyoutSEL, (void*)CFR_ToolBar_V1::NewFlyout);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarReleaseSEL, (void*)CFR_ToolBar_V1::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarGetNameSEL, (void*)CFR_ToolBar_V1::GetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarCountButtonsSEL, (void*)CFR_ToolBar_V1::CountButtons);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarGetButtonSEL, (void*)CFR_ToolBar_V1::GetButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarGetTitleSEL, (void*)CFR_ToolBar_V1::GetTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarSetTitleSEL, (void*)CFR_ToolBar_V1::SetTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarGetButtonByNameSEL, (void*)CFR_ToolBar_V1::GetButtonByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarAddButtonSEL, (void*)CFR_ToolBar_V1::AddButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarInsertButtonSEL, (void*)CFR_ToolBar_V1::InsertButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarShowToolBarSEL, (void*)CFR_ToolBar_V1::ShowToolBar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarHideToolBarSEL, (void*)CFR_ToolBar_V1::HideToolBar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarShowToolButtonSEL, (void*)CFR_ToolBar_V1::ShowToolButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarHideToolButtonSEL, (void*)CFR_ToolBar_V1::HideToolButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarRemoveButtonSEL, (void*)CFR_ToolBar_V1::RemoveButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarUpdateButtonStatesSEL, (void*)CFR_ToolBar_V1::UpdateButtonStates);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarIsFlyOutToolbarSEL, (void*)CFR_ToolBar_V1::IsFlyOutToolbar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarSetMenuTitleSEL, (void*)CFR_ToolBar_V1::SetMenuTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarSetDefaultToolbarSEL, (void*)CFR_ToolBar_V1::SetDefaultToolbar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarHideButtonInBrowserSEL, (void*)CFR_ToolBar_V1::HideButtonInBrowser);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarIsVisibleSEL, (void*)CFR_ToolBar_V1::IsVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarIsDisableSEL, (void*)CFR_ToolBar_V1::IsDisable);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarSetDisableSEL, (void*)CFR_ToolBar_V1::SetDisable);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolBarSEL, FRToolBarDockSEL, (void*)CFR_ToolBar_V1::Dock);
	}
};

class CFR_MessageBar_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarCreateSEL, (void*)CFR_MessageBar_V1::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarDestroySEL, (void*)CFR_MessageBar_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarShowSEL, (void*)CFR_MessageBar_V1::Show);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarIsVisibleSEL, (void*)CFR_MessageBar_V1::IsVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarSetTextSEL, (void*)CFR_MessageBar_V1::SetText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarSetBitmapSEL, (void*)CFR_MessageBar_V1::SetBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarAddButtonSEL, (void*)CFR_MessageBar_V1::AddButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarSetButtonAlignmentSEL, (void*)CFR_MessageBar_V1::SetButtonAlignment);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarSetButtonDropDownProcSEL, (void*)CFR_MessageBar_V1::SetButtonDropDownProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarSetButtonExecuteProcSEL, (void*)CFR_MessageBar_V1::SetButtonExecuteProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarSetButtonHelpTextSEL, (void*)CFR_MessageBar_V1::SetButtonHelpText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarEnableButtonSEL, (void*)CFR_MessageBar_V1::EnableButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarSetButtonPressedSEL, (void*)CFR_MessageBar_V1::SetButtonPressed);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarSetClientDataSEL, (void*)CFR_MessageBar_V1::SetClientData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarAddButtonImageSEL, (void*)CFR_MessageBar_V1::AddButtonImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarGetButtonImageSEL, (void*)CFR_MessageBar_V1::GetButtonImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarChangeButtonSEL, (void*)CFR_MessageBar_V1::ChangeButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarIsButtonEnableSEL, (void*)CFR_MessageBar_V1::IsButtonEnable);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarCountVisibleMessageBarsSEL, (void*)CFR_MessageBar_V1::CountVisibleMessageBars);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarGetVisibleMessageBarSEL, (void*)CFR_MessageBar_V1::GetVisibleMessageBar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarSetBitmap2SEL, (void*)CFR_MessageBar_V1::SetBitmap2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarAddButton2SEL, (void*)CFR_MessageBar_V1::AddButton2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMessageBarSEL, FRMessageBarAddButtonImage2SEL, (void*)CFR_MessageBar_V1::AddButtonImage2);
	}
};

// fr_barImpl.h end

// In file fr_docImpl.h
class CFR_Doc_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocOpenFromFileSEL, (void*)CFR_Doc_V1::OpenFromFile);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocOpenFromPDDocSEL, (void*)CFR_Doc_V1::OpenFromPDDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocFromPDDocSEL, (void*)CFR_Doc_V1::FromPDDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocCloseSEL, (void*)CFR_Doc_V1::Close);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetPDDocSEL, (void*)CFR_Doc_V1::GetPDDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetCustomSecuritySEL, (void*)CFR_Doc_V1::SetCustomSecurity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocDoSaveSEL, (void*)CFR_Doc_V1::DoSave);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocDoSaveAsSEL, (void*)CFR_Doc_V1::DoSaveAs);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetChangeMarkSEL, (void*)CFR_Doc_V1::SetChangeMark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetChangeMarkSEL, (void*)CFR_Doc_V1::GetChangeMark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocClearChangeMarkSEL, (void*)CFR_Doc_V1::ClearChangeMark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocWillInsertPagesSEL, (void*)CFR_Doc_V1::WillInsertPages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocDidInsertPagesSEL, (void*)CFR_Doc_V1::DidInsertPages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocWillDeletePagesSEL, (void*)CFR_Doc_V1::WillDeletePages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocDidDeletePagesSEL, (void*)CFR_Doc_V1::DidDeletePages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocWillRotatePageSEL, (void*)CFR_Doc_V1::WillRotatePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocDidRotatePageSEL, (void*)CFR_Doc_V1::DidRotatePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocWillResizePageSEL, (void*)CFR_Doc_V1::WillResizePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocDidResizePageSEL, (void*)CFR_Doc_V1::DidResizePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocDoPrintSEL, (void*)CFR_Doc_V1::DoPrint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocPrintPagesSEL, (void*)CFR_Doc_V1::PrintPages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocPrintSetupSEL, (void*)CFR_Doc_V1::PrintSetup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocCountDocViewsSEL, (void*)CFR_Doc_V1::CountDocViews);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetDocViewSEL, (void*)CFR_Doc_V1::GetDocView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetCurrentDocViewSEL, (void*)CFR_Doc_V1::GetCurrentDocView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetPermissionsSEL, (void*)CFR_Doc_V1::GetPermissions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetPermissionsSEL, (void*)CFR_Doc_V1::SetPermissions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetFilePathSEL, (void*)CFR_Doc_V1::GetFilePath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetCurSelectionSEL, (void*)CFR_Doc_V1::SetCurSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocAddToCurrentSelectionSEL, (void*)CFR_Doc_V1::AddToCurrentSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocRemoveFromSelectionSEL, (void*)CFR_Doc_V1::RemoveFromSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetCurSelectionSEL, (void*)CFR_Doc_V1::GetCurSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetCurSelectionTypeSEL, (void*)CFR_Doc_V1::GetCurSelectionType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocShowSelectionSEL, (void*)CFR_Doc_V1::ShowSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocClearSelectionSEL, (void*)CFR_Doc_V1::ClearSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocDeleteSelectionSEL, (void*)CFR_Doc_V1::DeleteSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocCopySelectionSEL, (void*)CFR_Doc_V1::CopySelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetCurCaptureSEL, (void*)CFR_Doc_V1::SetCurCapture);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetCurCaptureSEL, (void*)CFR_Doc_V1::GetCurCapture);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetCurCaptureTypeSEL, (void*)CFR_Doc_V1::GetCurCaptureType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocReleaseCurCaptureSEL, (void*)CFR_Doc_V1::ReleaseCurCapture);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetMenuEnableByNameSEL, (void*)CFR_Doc_V1::SetMenuEnableByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetParserSEL, (void*)CFR_Doc_V1::GetParser);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetPDFCreatorSEL, (void*)CFR_Doc_V1::GetPDFCreator);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocDoPrintSilentlySEL, (void*)CFR_Doc_V1::DoPrintSilently);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetTextSelectToolSEL, (void*)CFR_Doc_V1::GetTextSelectTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetDocumentTypeSEL, (void*)CFR_Doc_V1::GetDocumentType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocCanSecurityMethodBeModifiedSEL, (void*)CFR_Doc_V1::CanSecurityMethodBeModified);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocUpdateSecurityMethodSEL, (void*)CFR_Doc_V1::UpdateSecurityMethod);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocIsEncryptedSEL, (void*)CFR_Doc_V1::IsEncrypted);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocRemoveSecurityMethodSEL, (void*)CFR_Doc_V1::RemoveSecurityMethod);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocEnableRunScriptSEL, (void*)CFR_Doc_V1::EnableRunScript);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocIsEnableRunScriptSEL, (void*)CFR_Doc_V1::IsEnableRunScript);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocChangeDocShowTitleSEL, (void*)CFR_Doc_V1::ChangeDocShowTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocIsMemoryDocSEL, (void*)CFR_Doc_V1::IsMemoryDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetCurrentSecurityMethodNameSEL, (void*)CFR_Doc_V1::GetCurrentSecurityMethodName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetCurrentWndProviderSEL, (void*)CFR_Doc_V1::GetCurrentWndProvider);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetCurrentWndProviderSEL, (void*)CFR_Doc_V1::SetCurrentWndProvider);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetWndProviderByNameSEL, (void*)CFR_Doc_V1::GetWndProviderByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetReviewTypeSEL, (void*)CFR_Doc_V1::GetReviewType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetReviewTypeSEL, (void*)CFR_Doc_V1::SetReviewType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetAppPermissionsSEL, (void*)CFR_Doc_V1::SetAppPermissions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetAppPermissionsSEL, (void*)CFR_Doc_V1::GetAppPermissions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetAppPermissionsIISEL, (void*)CFR_Doc_V1::GetAppPermissionsII);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetPermissionsIISEL, (void*)CFR_Doc_V1::GetPermissionsII);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetMergedPermissionsSEL, (void*)CFR_Doc_V1::GetMergedPermissions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocKillFocusAnnotSEL, (void*)CFR_Doc_V1::KillFocusAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetPropertiesPDFVersionSEL, (void*)CFR_Doc_V1::SetPropertiesPDFVersion);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetPropertiesFilePathSEL, (void*)CFR_Doc_V1::SetPropertiesFilePath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocDoSaveAs2SEL, (void*)CFR_Doc_V1::DoSaveAs2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocShowSaveProgressCancelButtonSEL, (void*)CFR_Doc_V1::ShowSaveProgressCancelButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetInputPasswordProcSEL, (void*)CFR_Doc_V1::SetInputPasswordProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocCheckInDocumentOLESEL, (void*)CFR_Doc_V1::CheckInDocumentOLE);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocIsShowInBrowserSEL, (void*)CFR_Doc_V1::IsShowInBrowser);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetUIParentWndSEL, (void*)CFR_Doc_V1::GetUIParentWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetDocFrameHandlerSEL, (void*)CFR_Doc_V1::GetDocFrameHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocCreateNewViewByWndProviderSEL, (void*)CFR_Doc_V1::CreateNewViewByWndProvider);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocLoadAnnotsSEL, (void*)CFR_Doc_V1::LoadAnnots);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetDocShowTitleSEL, (void*)CFR_Doc_V1::GetDocShowTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocDoSave2SEL, (void*)CFR_Doc_V1::DoSave2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocResetDocTitleColorSEL, (void*)CFR_Doc_V1::ResetDocTitleColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetDocTitleColorSEL, (void*)CFR_Doc_V1::SetDocTitleColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetOriginalTypeSEL, (void*)CFR_Doc_V1::GetOriginalType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetOriginalTypeSEL, (void*)CFR_Doc_V1::SetOriginalType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetFocusAnnotSEL, (void*)CFR_Doc_V1::SetFocusAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGenerateUR3PermissionSEL, (void*)CFR_Doc_V1::GenerateUR3Permission);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocHasRedactAnnotSEL, (void*)CFR_Doc_V1::HasRedactAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGenerateRedactionsSEL, (void*)CFR_Doc_V1::GenerateRedactions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocReloadPageSEL, (void*)CFR_Doc_V1::ReloadPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocForbidChangeMarkSEL, (void*)CFR_Doc_V1::ForbidChangeMark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocOpenFromPDDoc2SEL, (void*)CFR_Doc_V1::OpenFromPDDoc2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetCreateDocSourceSEL, (void*)CFR_Doc_V1::GetCreateDocSource);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetCreateDocSourceSEL, (void*)CFR_Doc_V1::SetCreateDocSource);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetCreateDocSourceFileNameSEL, (void*)CFR_Doc_V1::GetCreateDocSourceFileName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocParsePageSEL, (void*)CFR_Doc_V1::ParsePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocIsValidAnnotSEL, (void*)CFR_Doc_V1::IsValidAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocIsWillReopenSEL, (void*)CFR_Doc_V1::IsWillReopen);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocOpenFromFile2SEL, (void*)CFR_Doc_V1::OpenFromFile2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetSignaturePermissionsSEL, (void*)CFR_Doc_V1::GetSignaturePermissions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocConvertPdfToOtherFormatSEL, (void*)CFR_Doc_V1::ConvertPdfToOtherFormat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocDoPassWordEncryptSEL, (void*)CFR_Doc_V1::DoPassWordEncrypt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocIsInProtectedViewModeSEL, (void*)CFR_Doc_V1::IsInProtectedViewMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetCreateDocSourceFilePathSEL, (void*)CFR_Doc_V1::GetCreateDocSourceFilePath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocIsImageBasedDocumentSEL, (void*)CFR_Doc_V1::IsImageBasedDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetDocEncryptedSEL, (void*)CFR_Doc_V1::SetDocEncrypted);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocIsVisibleSEL, (void*)CFR_Doc_V1::IsVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSetDRMSecuritySEL, (void*)CFR_Doc_V1::SetDRMSecurity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocConvertPdfToOtherFormat2SEL, (void*)CFR_Doc_V1::ConvertPdfToOtherFormat2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocAddWatermarkSEL, (void*)CFR_Doc_V1::AddWatermark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocAddAndUpdateWatermarkSEL, (void*)CFR_Doc_V1::AddAndUpdateWatermark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocRemoveWatermarkSEL, (void*)CFR_Doc_V1::RemoveWatermark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocRemoveAndUpdateWatermarkSEL, (void*)CFR_Doc_V1::RemoveAndUpdateWatermark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocAddHeaderFooterSEL, (void*)CFR_Doc_V1::AddHeaderFooter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocAddAndUpdateHeaderFooterSEL, (void*)CFR_Doc_V1::AddAndUpdateHeaderFooter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocRemoveHeaderFooterSEL, (void*)CFR_Doc_V1::RemoveHeaderFooter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocRemoveAndUpdateHeaderFooterSEL, (void*)CFR_Doc_V1::RemoveAndUpdateHeaderFooter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocIsReadSafeModeSEL, (void*)CFR_Doc_V1::IsReadSafeMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGeneratePageContentSEL, (void*)CFR_Doc_V1::GeneratePageContent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetFocusAnnotSEL, (void*)CFR_Doc_V1::GetFocusAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetMenuEnableByNameSEL, (void*)CFR_Doc_V1::GetMenuEnableByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocClearAllSelectionSEL, (void*)CFR_Doc_V1::ClearAllSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocAddSelectionSEL, (void*)CFR_Doc_V1::AddSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetPageFirstAnnotSEL, (void*)CFR_Doc_V1::GetPageFirstAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocCreateFormControlSEL, (void*)CFR_Doc_V1::CreateFormControl);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocReCalTabOrderSEL, (void*)CFR_Doc_V1::ReCalTabOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetInterFormSEL, (void*)CFR_Doc_V1::GetInterForm);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocNotifyAddWidgetSEL, (void*)CFR_Doc_V1::NotifyAddWidget);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocUpdateAllViewsSEL, (void*)CFR_Doc_V1::UpdateAllViews);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetAnnotByDictSEL, (void*)CFR_Doc_V1::GetAnnotByDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocAddAnnotSEL, (void*)CFR_Doc_V1::AddAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocAddAnnot2SEL, (void*)CFR_Doc_V1::AddAnnot2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocReplaceTextToSelectedTextSEL, (void*)CFR_Doc_V1::ReplaceTextToSelectedText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocReplacePagesSEL, (void*)CFR_Doc_V1::ReplacePages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocInsertPagesSEL, (void*)CFR_Doc_V1::InsertPages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocExtractPagesSEL, (void*)CFR_Doc_V1::ExtractPages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetDocSecurityCanBeModifiedSEL, (void*)CFR_Doc_V1::GetDocSecurityCanBeModified);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetDocumentSEL, (void*)CFR_Doc_V1::GetDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetAnnotByDict2SEL, (void*)CFR_Doc_V1::GetAnnotByDict2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocGetPageIndexSEL, (void*)CFR_Doc_V1::GetPageIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocRemoveSecurityInfoSEL, (void*)CFR_Doc_V1::RemoveSecurityInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocUpdateDocAllViewsSEL, (void*)CFR_Doc_V1::UpdateDocAllViews);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocSendAsAttachmentSEL, (void*)CFR_Doc_V1::SendAsAttachment);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocOnBeforeNotifySumbitSEL, (void*)CFR_Doc_V1::OnBeforeNotifySumbit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocSEL, FRDocOnAfterNotifySumbitSEL, (void*)CFR_Doc_V1::OnAfterNotifySumbit);
	}
};

// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
class CFR_MenuBar_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuBarSEL, FRMenuBarGetMenuCountSEL, (void*)CFR_MenuBar_V1::GetMenuCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuBarSEL, FRMenuBarGetMenuByIndexSEL, (void*)CFR_MenuBar_V1::GetMenuByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuBarSEL, FRMenuBarGetMenuByNameSEL, (void*)CFR_MenuBar_V1::GetMenuByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuBarSEL, FRMenuBarAddMenuSEL, (void*)CFR_MenuBar_V1::AddMenu);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuBarSEL, FRMenuBarGetMenuIndexSEL, (void*)CFR_MenuBar_V1::GetMenuIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuBarSEL, FRMenuBarDeleteMenuSEL, (void*)CFR_MenuBar_V1::DeleteMenu);
	}
};

class CFR_Menu_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuNewSEL, (void*)CFR_Menu_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuReleaseSEL, (void*)CFR_Menu_V1::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuGetParentMenuItemSEL, (void*)CFR_Menu_V1::GetParentMenuItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuGetMenuItemByIndexSEL, (void*)CFR_Menu_V1::GetMenuItemByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuGetMenuItemByNameSEL, (void*)CFR_Menu_V1::GetMenuItemByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuGetMenuItemCountSEL, (void*)CFR_Menu_V1::GetMenuItemCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuAddMenuItemSEL, (void*)CFR_Menu_V1::AddMenuItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuDeleteMenuItemSEL, (void*)CFR_Menu_V1::DeleteMenuItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuGetMenuItemIndexSEL, (void*)CFR_Menu_V1::GetMenuItemIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuDeleteOwnerDrawHandleSEL, (void*)CFR_Menu_V1::DeleteOwnerDrawHandle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuRegisterOwnerDrawHandleSEL, (void*)CFR_Menu_V1::RegisterOwnerDrawHandle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuTrackPopupSEL, (void*)CFR_Menu_V1::TrackPopup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuSetVisibleSEL, (void*)CFR_Menu_V1::SetVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuGetVisibleSEL, (void*)CFR_Menu_V1::GetVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuSetTitleSEL, (void*)CFR_Menu_V1::SetTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuNewIISEL, (void*)CFR_Menu_V1::NewII);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuTrackPopupMenuSEL, (void*)CFR_Menu_V1::TrackPopupMenu);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuSEL, FRMenuCloseActivePopupMenuSEL, (void*)CFR_Menu_V1::CloseActivePopupMenu);
	}
};

class CFR_MenuItem_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemNewSEL, (void*)CFR_MenuItem_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemReleaseSEL, (void*)CFR_MenuItem_V1::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemGetSubMenuSEL, (void*)CFR_MenuItem_V1::GetSubMenu);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemSetSubMenuSEL, (void*)CFR_MenuItem_V1::SetSubMenu);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemGetParentMenuSEL, (void*)CFR_MenuItem_V1::GetParentMenu);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemGetIconSEL, (void*)CFR_MenuItem_V1::GetIcon);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemSetIconSEL, (void*)CFR_MenuItem_V1::SetIcon);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemGetTitleSEL, (void*)CFR_MenuItem_V1::GetTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemSetTitleSEL, (void*)CFR_MenuItem_V1::SetTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemGetNameSEL, (void*)CFR_MenuItem_V1::GetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemSetToolTipSEL, (void*)CFR_MenuItem_V1::SetToolTip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemSetDescribeTextSEL, (void*)CFR_MenuItem_V1::SetDescribeText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemIsSeparatorSEL, (void*)CFR_MenuItem_V1::IsSeparator);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemSetExecuteProcSEL, (void*)CFR_MenuItem_V1::SetExecuteProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemSetComputeEnabledProcSEL, (void*)CFR_MenuItem_V1::SetComputeEnabledProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemSetComputeMarkedProcSEL, (void*)CFR_MenuItem_V1::SetComputeMarkedProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemDoExecuteProcSEL, (void*)CFR_MenuItem_V1::DoExecuteProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemIsEnabledSEL, (void*)CFR_MenuItem_V1::IsEnabled);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemIsMarkedSEL, (void*)CFR_MenuItem_V1::IsMarked);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemGetClientDataSEL, (void*)CFR_MenuItem_V1::GetClientData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemSetAccelKeySEL, (void*)CFR_MenuItem_V1::SetAccelKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemGetCmdIDSEL, (void*)CFR_MenuItem_V1::GetCmdID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemSetClientDataSEL, (void*)CFR_MenuItem_V1::SetClientData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemSetVisibleSEL, (void*)CFR_MenuItem_V1::SetVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMenuItemSEL, FRMenuItemGetVisibleSEL, (void*)CFR_MenuItem_V1::GetVisible);
	}
};

// fr_menuImpl.h end

// In file fr_sysImpl.h
class CFR_Sys_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSysSEL, FRSysLoadStandarCursorSEL, (void*)CFR_Sys_V1::LoadStandarCursor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSysSEL, FRSysGetCursorSEL, (void*)CFR_Sys_V1::GetCursor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSysSEL, FRSysSetCursorSEL, (void*)CFR_Sys_V1::SetCursor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSysSEL, FRSysShowMessageBoxSEL, (void*)CFR_Sys_V1::ShowMessageBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSysSEL, FRSysInstallDialogSkinThemeSEL, (void*)CFR_Sys_V1::InstallDialogSkinTheme);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSysSEL, FRSysInstallDialogScrollBarSEL, (void*)CFR_Sys_V1::InstallDialogScrollBar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSysSEL, FRSysGetSkinCountSEL, (void*)CFR_Sys_V1::GetSkinCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSysSEL, FRSysGetSkinNameByIndexSEL, (void*)CFR_Sys_V1::GetSkinNameByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSysSEL, FRSysSetActiveSkinByIndexSEL, (void*)CFR_Sys_V1::SetActiveSkinByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSysSEL, FRSysSetActiveSkinByNameSEL, (void*)CFR_Sys_V1::SetActiveSkinByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSysSEL, FRSysShowStopLaunchDlgSEL, (void*)CFR_Sys_V1::ShowStopLaunchDlg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSysSEL, FRSysShowMessageBox2SEL, (void*)CFR_Sys_V1::ShowMessageBox2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSysSEL, FRSysShowMessageBox3SEL, (void*)CFR_Sys_V1::ShowMessageBox3);
	}
};

// fr_sysImpl.h end

// In file fr_viewImpl.h
class CFR_DocView_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetDocumentSEL, (void*)CFR_DocView_V1::GetDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewCountPageViewsSEL, (void*)CFR_DocView_V1::CountPageViews);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetPageViewSEL, (void*)CFR_DocView_V1::GetPageView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetPageViewAtPointSEL, (void*)CFR_DocView_V1::GetPageViewAtPoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewCountVisiblePageViewsSEL, (void*)CFR_DocView_V1::CountVisiblePageViews);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetVisiblePageViewSEL, (void*)CFR_DocView_V1::GetVisiblePageView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGotoPageViewSEL, (void*)CFR_DocView_V1::GotoPageView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGotoPageViewByPointSEL, (void*)CFR_DocView_V1::GotoPageViewByPoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGotoPageViewByRectSEL, (void*)CFR_DocView_V1::GotoPageViewByRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetCurrentPageViewSEL, (void*)CFR_DocView_V1::GetCurrentPageView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetRotationSEL, (void*)CFR_DocView_V1::GetRotation);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewSetRotationSEL, (void*)CFR_DocView_V1::SetRotation);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetLayoutModeSEL, (void*)CFR_DocView_V1::GetLayoutMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewSetLayoutModeSEL, (void*)CFR_DocView_V1::SetLayoutMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetZoomSEL, (void*)CFR_DocView_V1::GetZoom);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetZoomTypeSEL, (void*)CFR_DocView_V1::GetZoomType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewZoomToSEL, (void*)CFR_DocView_V1::ZoomTo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGoBackSEL, (void*)CFR_DocView_V1::GoBack);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGoForwardSEL, (void*)CFR_DocView_V1::GoForward);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewShowAnnotsSEL, (void*)CFR_DocView_V1::ShowAnnots);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewProcActionSEL, (void*)CFR_DocView_V1::ProcAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewScrollToSEL, (void*)CFR_DocView_V1::ScrollTo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetMaxScrollingSizeSEL, (void*)CFR_DocView_V1::GetMaxScrollingSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewDrawNowSEL, (void*)CFR_DocView_V1::DrawNow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewInvalidateRectSEL, (void*)CFR_DocView_V1::InvalidateRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewDrawRectSEL, (void*)CFR_DocView_V1::DrawRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewDoPopUpMenuSEL, (void*)CFR_DocView_V1::DoPopUpMenu);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetMachinePortSEL, (void*)CFR_DocView_V1::GetMachinePort);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewReleaseMachinePortSEL, (void*)CFR_DocView_V1::ReleaseMachinePort);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetOCContextSEL, (void*)CFR_DocView_V1::GetOCContext);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetCurrentSnapshotSEL, (void*)CFR_DocView_V1::GetCurrentSnapshot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetThumbnailViewSEL, (void*)CFR_DocView_V1::GetThumbnailView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGotoPageViewByStateSEL, (void*)CFR_DocView_V1::GotoPageViewByState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewMovePageSEL, (void*)CFR_DocView_V1::MovePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewIsValidPageViewSEL, (void*)CFR_DocView_V1::IsValidPageView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetTagDocViewTextSEL, (void*)CFR_DocView_V1::GetTagDocViewText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetLRDocViewTextSEL, (void*)CFR_DocView_V1::GetLRDocViewText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRDocViewSEL, FRDocViewGetHwndSEL, (void*)CFR_DocView_V1::GetHwnd);
	}
};

class CFR_PageView_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewGetDocumentSEL, (void*)CFR_PageView_V1::GetDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewGetDocViewSEL, (void*)CFR_PageView_V1::GetDocView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewGetPDPageSEL, (void*)CFR_PageView_V1::GetPDPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewGetPageIndexSEL, (void*)CFR_PageView_V1::GetPageIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewCountAnnotSEL, (void*)CFR_PageView_V1::CountAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewGetCurrentMatrixSEL, (void*)CFR_PageView_V1::GetCurrentMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewDevicePointToPageSEL, (void*)CFR_PageView_V1::DevicePointToPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewDeviceRectToPageSEL, (void*)CFR_PageView_V1::DeviceRectToPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewPointToDeviceSEL, (void*)CFR_PageView_V1::PointToDevice);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewRectToDeviceSEL, (void*)CFR_PageView_V1::RectToDevice);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewIsVisibleSEL, (void*)CFR_PageView_V1::IsVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewGetPageRectSEL, (void*)CFR_PageView_V1::GetPageRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewGetPageVisibleRectSEL, (void*)CFR_PageView_V1::GetPageVisibleRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewDidContentChangedSEL, (void*)CFR_PageView_V1::DidContentChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewGetHWndSEL, (void*)CFR_PageView_V1::GetHWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewDidTextObjectChangedSEL, (void*)CFR_PageView_V1::DidTextObjectChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewGetAnnotByIndexSEL, (void*)CFR_PageView_V1::GetAnnotByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewGetPageStateSEL, (void*)CFR_PageView_V1::GetPageState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewDeleteAnnotSEL, (void*)CFR_PageView_V1::DeleteAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewGetFocusAnnotSEL, (void*)CFR_PageView_V1::GetFocusAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewGetPageScaleSEL, (void*)CFR_PageView_V1::GetPageScale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewGetAnnotAtPointSEL, (void*)CFR_PageView_V1::GetAnnotAtPoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewUpdateAllViewsSEL, (void*)CFR_PageView_V1::UpdateAllViews);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewAddAnnotSEL, (void*)CFR_PageView_V1::AddAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewGetRenderOptionsSEL, (void*)CFR_PageView_V1::GetRenderOptions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewDidContentChanged2SEL, (void*)CFR_PageView_V1::DidContentChanged2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewDidContentChanged3SEL, (void*)CFR_PageView_V1::DidContentChanged3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewSetContentModificationSEL, (void*)CFR_PageView_V1::SetContentModification);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewGenerateContentSEL, (void*)CFR_PageView_V1::GenerateContent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPageViewSEL, FRPageViewUpdateAllViewsRectSEL, (void*)CFR_PageView_V1::UpdateAllViewsRect);
	}
};

class CFR_TextSelectTool_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTextSelectToolSEL, FRTextSelectToolCreateSEL, (void*)CFR_TextSelectTool_V1::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTextSelectToolSEL, FRTextSelectToolDestroyTextSelectToolSEL, (void*)CFR_TextSelectTool_V1::DestroyTextSelectTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTextSelectToolSEL, FRTextSelectToolGetSelectedTextSEL, (void*)CFR_TextSelectTool_V1::GetSelectedText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTextSelectToolSEL, FRTextSelectToolDrawSelectionSEL, (void*)CFR_TextSelectTool_V1::DrawSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTextSelectToolSEL, FRTextSelectToolClearSelectionSEL, (void*)CFR_TextSelectTool_V1::ClearSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTextSelectToolSEL, FRTextSelectToolDoLButtonUpSEL, (void*)CFR_TextSelectTool_V1::DoLButtonUp);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTextSelectToolSEL, FRTextSelectToolDoLButtonDownSEL, (void*)CFR_TextSelectTool_V1::DoLButtonDown);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTextSelectToolSEL, FRTextSelectToolDoLButtonDblClkSEL, (void*)CFR_TextSelectTool_V1::DoLButtonDblClk);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTextSelectToolSEL, FRTextSelectToolDoRButtonUpSEL, (void*)CFR_TextSelectTool_V1::DoRButtonUp);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTextSelectToolSEL, FRTextSelectToolDoMouseMoveSEL, (void*)CFR_TextSelectTool_V1::DoMouseMove);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTextSelectToolSEL, FRTextSelectToolDoMouseWheelSEL, (void*)CFR_TextSelectTool_V1::DoMouseWheel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTextSelectToolSEL, FRTextSelectToolIsSelectingSEL, (void*)CFR_TextSelectTool_V1::IsSelecting);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTextSelectToolSEL, FRTextSelectToolEnumTextObjectRectSEL, (void*)CFR_TextSelectTool_V1::EnumTextObjectRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTextSelectToolSEL, FRTextSelectToolSelect_GetPageRangeSEL, (void*)CFR_TextSelectTool_V1::Select_GetPageRange);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTextSelectToolSEL, FRTextSelectToolAddSelectSEL, (void*)CFR_TextSelectTool_V1::AddSelect);
	}
};

// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
class CFPD_Doc_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocNewSEL, (void*)CFPD_Doc_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocDestroySEL, (void*)CFPD_Doc_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocOpenSEL, (void*)CFPD_Doc_V1::Open);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocOpenMemDocumentSEL, (void*)CFPD_Doc_V1::OpenMemDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocStartProgressiveLoadSEL, (void*)CFPD_Doc_V1::StartProgressiveLoad);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocContinueLoadSEL, (void*)CFPD_Doc_V1::ContinueLoad);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocCloseSEL, (void*)CFPD_Doc_V1::Close);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetRootSEL, (void*)CFPD_Doc_V1::GetRoot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetInfoSEL, (void*)CFPD_Doc_V1::GetInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetIDSEL, (void*)CFPD_Doc_V1::GetID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetPageCountSEL, (void*)CFPD_Doc_V1::GetPageCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetPageSEL, (void*)CFPD_Doc_V1::GetPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetPageIndexSEL, (void*)CFPD_Doc_V1::GetPageIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetUserPermissionsSEL, (void*)CFPD_Doc_V1::GetUserPermissions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocIsOwnerSEL, (void*)CFPD_Doc_V1::IsOwner);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocIsFormStreamSEL, (void*)CFPD_Doc_V1::IsFormStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocLoadFontSEL, (void*)CFPD_Doc_V1::LoadFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocLoadColorSpaceSEL, (void*)CFPD_Doc_V1::LoadColorSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocLoadPatternSEL, (void*)CFPD_Doc_V1::LoadPattern);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocLoadImageFSEL, (void*)CFPD_Doc_V1::LoadImageF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocLoadFontFileSEL, (void*)CFPD_Doc_V1::LoadFontFile);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetInfoObjNumSEL, (void*)CFPD_Doc_V1::GetInfoObjNum);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetRootObjNumSEL, (void*)CFPD_Doc_V1::GetRootObjNum);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocEnumPagesSEL, (void*)CFPD_Doc_V1::EnumPages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocNewEnumPageHandlerSEL, (void*)CFPD_Doc_V1::NewEnumPageHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocDeleteEnumPageHandlerSEL, (void*)CFPD_Doc_V1::DeleteEnumPageHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocSetRootObjNumSEL, (void*)CFPD_Doc_V1::SetRootObjNum);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocSetInfoObjNumSEL, (void*)CFPD_Doc_V1::SetInfoObjNum);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocSetIDSEL, (void*)CFPD_Doc_V1::SetID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocAddWindowsFontSEL, (void*)CFPD_Doc_V1::AddWindowsFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocAddWindowsFontWSEL, (void*)CFPD_Doc_V1::AddWindowsFontW);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocAddStandardFontSEL, (void*)CFPD_Doc_V1::AddStandardFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocBuildResourceListSEL, (void*)CFPD_Doc_V1::BuildResourceList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocCreateNewPageSEL, (void*)CFPD_Doc_V1::CreateNewPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocDeletePageSEL, (void*)CFPD_Doc_V1::DeletePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocConvertIndirectObjectsSEL, (void*)CFPD_Doc_V1::ConvertIndirectObjects);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetPageContentModifySEL, (void*)CFPD_Doc_V1::GetPageContentModify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocQuickSearchSEL, (void*)CFPD_Doc_V1::QuickSearch);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocReloadPagesSEL, (void*)CFPD_Doc_V1::ReloadPages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocLoadDocSEL, (void*)CFPD_Doc_V1::LoadDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetIndirectObjectSEL, (void*)CFPD_Doc_V1::GetIndirectObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetIndirectTypeSEL, (void*)CFPD_Doc_V1::GetIndirectType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocAddIndirectObjectSEL, (void*)CFPD_Doc_V1::AddIndirectObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocReleaseIndirectObjectSEL, (void*)CFPD_Doc_V1::ReleaseIndirectObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocDeleteIndirectObjectSEL, (void*)CFPD_Doc_V1::DeleteIndirectObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocImportIndirectObjectSEL, (void*)CFPD_Doc_V1::ImportIndirectObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocImportExternalObjectSEL, (void*)CFPD_Doc_V1::ImportExternalObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocInsertIndirectObjectSEL, (void*)CFPD_Doc_V1::InsertIndirectObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetLastobjNumSEL, (void*)CFPD_Doc_V1::GetLastobjNum);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocReloadFileStreamsSEL, (void*)CFPD_Doc_V1::ReloadFileStreams);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetStartPositionSEL, (void*)CFPD_Doc_V1::GetStartPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetNextAssocSEL, (void*)CFPD_Doc_V1::GetNextAssoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocIsModifiedSEL, (void*)CFPD_Doc_V1::IsModified);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocClearModifiedSEL, (void*)CFPD_Doc_V1::ClearModified);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocSaveSEL, (void*)CFPD_Doc_V1::Save);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetParserSEL, (void*)CFPD_Doc_V1::GetParser);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocCheckSignatureSEL, (void*)CFPD_Doc_V1::CheckSignature);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGenerateFileIDSEL, (void*)CFPD_Doc_V1::GenerateFileID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetReviewTypeSEL, (void*)CFPD_Doc_V1::GetReviewType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocIsPortfolioSEL, (void*)CFPD_Doc_V1::IsPortfolio);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocRemoveUR3SEL, (void*)CFPD_Doc_V1::RemoveUR3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocAddFXFontSEL, (void*)CFPD_Doc_V1::AddFXFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocGetIndirectObjsCountSEL, (void*)CFPD_Doc_V1::GetIndirectObjsCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocSave2SEL, (void*)CFPD_Doc_V1::Save2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocInsertDocumentAtPosSEL, (void*)CFPD_Doc_V1::InsertDocumentAtPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocMetadataGetAllCustomKeysSEL, (void*)CFPD_Doc_V1::MetadataGetAllCustomKeys);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocMetadataDeleteCustomKeySEL, (void*)CFPD_Doc_V1::MetadataDeleteCustomKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocMetadataGetStringSEL, (void*)CFPD_Doc_V1::MetadataGetString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocMetadataSetStringSEL, (void*)CFPD_Doc_V1::MetadataSetString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocSEL, FPDDocMetadataSyncUpdateSEL, (void*)CFPD_Doc_V1::MetadataSyncUpdate);
	}
};

class CFPD_NameTree_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNameTreeSEL, FPDNameTreeNewSEL, (void*)CFPD_NameTree_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNameTreeSEL, FPDNameTreeNew2SEL, (void*)CFPD_NameTree_V1::New2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNameTreeSEL, FPDNameTreeDestroySEL, (void*)CFPD_NameTree_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNameTreeSEL, FPDNameTreeLookupValueSEL, (void*)CFPD_NameTree_V1::LookupValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNameTreeSEL, FPDNameTreeLookupValueByNameSEL, (void*)CFPD_NameTree_V1::LookupValueByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNameTreeSEL, FPDNameTreeLookupNamedDestSEL, (void*)CFPD_NameTree_V1::LookupNamedDest);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNameTreeSEL, FPDNameTreeSetValueSEL, (void*)CFPD_NameTree_V1::SetValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNameTreeSEL, FPDNameTreeGetIndexSEL, (void*)CFPD_NameTree_V1::GetIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNameTreeSEL, FPDNameTreeRemoveSEL, (void*)CFPD_NameTree_V1::Remove);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNameTreeSEL, FPDNameTreeGetCountSEL, (void*)CFPD_NameTree_V1::GetCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNameTreeSEL, FPDNameTreeGetRootSEL, (void*)CFPD_NameTree_V1::GetRoot);
	}
};

class CFPD_Bookmark_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDBookmarkSEL, FPDBookmarkNewSEL, (void*)CFPD_Bookmark_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDBookmarkSEL, FPDBookmarkDestroySEL, (void*)CFPD_Bookmark_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDBookmarkSEL, FPDBookmarkIsVaildSEL, (void*)CFPD_Bookmark_V1::IsVaild);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDBookmarkSEL, FPDBookmarkGetColorRefSEL, (void*)CFPD_Bookmark_V1::GetColorRef);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDBookmarkSEL, FPDBookmarkGetFontStyleSEL, (void*)CFPD_Bookmark_V1::GetFontStyle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDBookmarkSEL, FPDBookmarkGetTitleSEL, (void*)CFPD_Bookmark_V1::GetTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDBookmarkSEL, FPDBookmarkGetDestSEL, (void*)CFPD_Bookmark_V1::GetDest);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDBookmarkSEL, FPDBookmarkGetActionSEL, (void*)CFPD_Bookmark_V1::GetAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDBookmarkSEL, FPDBookmarkGetDictionarySEL, (void*)CFPD_Bookmark_V1::GetDictionary);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDBookmarkSEL, FPDBookmarkGetFirstChildSEL, (void*)CFPD_Bookmark_V1::GetFirstChild);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDBookmarkSEL, FPDBookmarkGetNextSiblingSEL, (void*)CFPD_Bookmark_V1::GetNextSibling);
	}
};

class CFPD_Dest_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDestSEL, FPDDestNewSEL, (void*)CFPD_Dest_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDestSEL, FPDDestDestroySEL, (void*)CFPD_Dest_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDestSEL, FPDDestGetRemoteNameSEL, (void*)CFPD_Dest_V1::GetRemoteName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDestSEL, FPDDestGetPageIndexSEL, (void*)CFPD_Dest_V1::GetPageIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDestSEL, FPDDestGetPageObjNumSEL, (void*)CFPD_Dest_V1::GetPageObjNum);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDestSEL, FPDDestGetZoomModeSEL, (void*)CFPD_Dest_V1::GetZoomMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDestSEL, FPDDestGetParamSEL, (void*)CFPD_Dest_V1::GetParam);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDestSEL, FPDDestGetPDFObjectSEL, (void*)CFPD_Dest_V1::GetPDFObject);
	}
};

class CFPD_OCContext_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCContextSEL, FPDOCContextNewSEL, (void*)CFPD_OCContext_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCContextSEL, FPDOCContextDestroySEL, (void*)CFPD_OCContext_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCContextSEL, FPDOCContextGetDocumentSEL, (void*)CFPD_OCContext_V1::GetDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCContextSEL, FPDOCContextGetUsageTypeSEL, (void*)CFPD_OCContext_V1::GetUsageType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCContextSEL, FPDOCContextCheckOCGVisibleSEL, (void*)CFPD_OCContext_V1::CheckOCGVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCContextSEL, FPDOCContextResetOCContextSEL, (void*)CFPD_OCContext_V1::ResetOCContext);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCContextSEL, FPDOCContextSetOCGStateSEL, (void*)CFPD_OCContext_V1::SetOCGState);
	}
};

class CFPD_OCGroup_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSEL, FPDOCGroupNewSEL, (void*)CFPD_OCGroup_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSEL, FPDOCGroupDestroySEL, (void*)CFPD_OCGroup_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSEL, FPDOCGroupGetNameSEL, (void*)CFPD_OCGroup_V1::GetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSEL, FPDOCGroupSetNameSEL, (void*)CFPD_OCGroup_V1::SetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSEL, FPDOCGroupHasIntentSEL, (void*)CFPD_OCGroup_V1::HasIntent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSEL, FPDOCGroupGetCreatorInfoSEL, (void*)CFPD_OCGroup_V1::GetCreatorInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSEL, FPDOCGroupGetLanguageInfoSEL, (void*)CFPD_OCGroup_V1::GetLanguageInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSEL, FPDOCGroupGetExportStateSEL, (void*)CFPD_OCGroup_V1::GetExportState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSEL, FPDOCGroupGetZoomRangeSEL, (void*)CFPD_OCGroup_V1::GetZoomRange);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSEL, FPDOCGroupGetPrintInfoSEL, (void*)CFPD_OCGroup_V1::GetPrintInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSEL, FPDOCGroupGetViewStateSEL, (void*)CFPD_OCGroup_V1::GetViewState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSEL, FPDOCGroupGetUserTypeSEL, (void*)CFPD_OCGroup_V1::GetUserType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSEL, FPDOCGroupGetPageElementTypeSEL, (void*)CFPD_OCGroup_V1::GetPageElementType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSEL, FPDOCGroupGetDictionarySEL, (void*)CFPD_OCGroup_V1::GetDictionary);
	}
};

class CFPD_OCGroupSet_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSetSEL, FPDOCGroupSetNewSEL, (void*)CFPD_OCGroupSet_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSetSEL, FPDOCGroupSetDestroySEL, (void*)CFPD_OCGroupSet_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSetSEL, FPDOCGroupSetCountElementsSEL, (void*)CFPD_OCGroupSet_V1::CountElements);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSetSEL, FPDOCGroupSetIsSubGroupSetSEL, (void*)CFPD_OCGroupSet_V1::IsSubGroupSet);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSetSEL, FPDOCGroupSetGetGroupSEL, (void*)CFPD_OCGroupSet_V1::GetGroup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSetSEL, FPDOCGroupSetGetSubGroupSetSEL, (void*)CFPD_OCGroupSet_V1::GetSubGroupSet);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSetSEL, FPDOCGroupSetFindGroupSEL, (void*)CFPD_OCGroupSet_V1::FindGroup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCGroupSetSEL, FPDOCGroupSetGetSubGroupSetNameSEL, (void*)CFPD_OCGroupSet_V1::GetSubGroupSetName);
	}
};

class CFPD_OCNotify_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCNotifySEL, FPDOCNotifyFPD_OCNotifyNewSEL, (void*)CFPD_OCNotify_V1::FPD_OCNotifyNew);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCNotifySEL, FPDOCNotifyFPD_OCNotifyDestroySEL, (void*)CFPD_OCNotify_V1::FPD_OCNotifyDestroy);
	}
};

class CFPD_OCProperties_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCPropertiesSEL, FPDOCPropertiesNewSEL, (void*)CFPD_OCProperties_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCPropertiesSEL, FPDOCPropertiesDestroySEL, (void*)CFPD_OCProperties_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCPropertiesSEL, FPDOCPropertiesGetDocumentSEL, (void*)CFPD_OCProperties_V1::GetDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCPropertiesSEL, FPDOCPropertiesGetOCGroupsSEL, (void*)CFPD_OCProperties_V1::GetOCGroups);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCPropertiesSEL, FPDOCPropertiesIsOCGroupSEL, (void*)CFPD_OCProperties_V1::IsOCGroup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCPropertiesSEL, FPDOCPropertiesRetrieveOCGPagesSEL, (void*)CFPD_OCProperties_V1::RetrieveOCGPages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCPropertiesSEL, FPDOCPropertiesIsOCGInPageSEL, (void*)CFPD_OCProperties_V1::IsOCGInPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCPropertiesSEL, FPDOCPropertiesGetOCGroupOrderSEL, (void*)CFPD_OCProperties_V1::GetOCGroupOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCPropertiesSEL, FPDOCPropertiesCountConfigsSEL, (void*)CFPD_OCProperties_V1::CountConfigs);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCPropertiesSEL, FPDOCPropertiesGetConfigSEL, (void*)CFPD_OCProperties_V1::GetConfig);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCPropertiesSEL, FPDOCPropertiesAddOCNotifySEL, (void*)CFPD_OCProperties_V1::AddOCNotify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOCPropertiesSEL, FPDOCPropertiesRemoveOCNotifySEL, (void*)CFPD_OCProperties_V1::RemoveOCNotify);
	}
};

class CFPD_LWinParam_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLWinParamSEL, FPDLWinParamNewSEL, (void*)CFPD_LWinParam_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLWinParamSEL, FPDLWinParamDestroySEL, (void*)CFPD_LWinParam_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLWinParamSEL, FPDLWinParamGetFileNameSEL, (void*)CFPD_LWinParam_V1::GetFileName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLWinParamSEL, FPDLWinParamSetFileNameSEL, (void*)CFPD_LWinParam_V1::SetFileName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLWinParamSEL, FPDLWinParamGetDefaultDirectorySEL, (void*)CFPD_LWinParam_V1::GetDefaultDirectory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLWinParamSEL, FPDLWinParamSetDefaultDirectorySEL, (void*)CFPD_LWinParam_V1::SetDefaultDirectory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLWinParamSEL, FPDLWinParamGetOperationSEL, (void*)CFPD_LWinParam_V1::GetOperation);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLWinParamSEL, FPDLWinParamSetOperationSEL, (void*)CFPD_LWinParam_V1::SetOperation);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLWinParamSEL, FPDLWinParamGetParamSEL, (void*)CFPD_LWinParam_V1::GetParam);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLWinParamSEL, FPDLWinParamSetParamSEL, (void*)CFPD_LWinParam_V1::SetParam);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLWinParamSEL, FPDLWinParamGetDictSEL, (void*)CFPD_LWinParam_V1::GetDict);
	}
};

class CFPD_ActionFields_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionFieldsSEL, FPDActionFieldsNewSEL, (void*)CFPD_ActionFields_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionFieldsSEL, FPDActionFieldsDestroySEL, (void*)CFPD_ActionFields_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionFieldsSEL, FPDActionFieldsGetFieldsCountSEL, (void*)CFPD_ActionFields_V1::GetFieldsCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionFieldsSEL, FPDActionFieldsGetAllFieldsSEL, (void*)CFPD_ActionFields_V1::GetAllFields);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionFieldsSEL, FPDActionFieldsGetFieldSEL, (void*)CFPD_ActionFields_V1::GetField);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionFieldsSEL, FPDActionFieldsInsertFieldSEL, (void*)CFPD_ActionFields_V1::InsertField);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionFieldsSEL, FPDActionFieldsRemoveFieldSEL, (void*)CFPD_ActionFields_V1::RemoveField);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionFieldsSEL, FPDActionFieldsGetActionSEL, (void*)CFPD_ActionFields_V1::GetAction);
	}
};

class CFPD_Action_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionNewSEL, (void*)CFPD_Action_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionNew2SEL, (void*)CFPD_Action_V1::New2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionNew3SEL, (void*)CFPD_Action_V1::New3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionDestroySEL, (void*)CFPD_Action_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetTypeNameSEL, (void*)CFPD_Action_V1::GetTypeName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetTypeSEL, (void*)CFPD_Action_V1::GetType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetDestSEL, (void*)CFPD_Action_V1::GetDest);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionSetDestSEL, (void*)CFPD_Action_V1::SetDest);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetFilePathSEL, (void*)CFPD_Action_V1::GetFilePath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionSetFilePathSEL, (void*)CFPD_Action_V1::SetFilePath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetNewWindowSEL, (void*)CFPD_Action_V1::GetNewWindow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionSetNewWindowSEL, (void*)CFPD_Action_V1::SetNewWindow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetWinParamSEL, (void*)CFPD_Action_V1::GetWinParam);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionSetWinParamSEL, (void*)CFPD_Action_V1::SetWinParam);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetURISEL, (void*)CFPD_Action_V1::GetURI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionSetURISEL, (void*)CFPD_Action_V1::SetURI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetMouseMapSEL, (void*)CFPD_Action_V1::GetMouseMap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionSetMouseMapSEL, (void*)CFPD_Action_V1::SetMouseMap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetWidgetsSEL, (void*)CFPD_Action_V1::GetWidgets);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetHideStatusSEL, (void*)CFPD_Action_V1::GetHideStatus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionSetHideStatusSEL, (void*)CFPD_Action_V1::SetHideStatus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetNameActionSEL, (void*)CFPD_Action_V1::GetNameAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionSetNameActionSEL, (void*)CFPD_Action_V1::SetNameAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetFlagsSEL, (void*)CFPD_Action_V1::GetFlags);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionSetFlagsSEL, (void*)CFPD_Action_V1::SetFlags);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetJavaScriptSEL, (void*)CFPD_Action_V1::GetJavaScript);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionSetJavaScriptSEL, (void*)CFPD_Action_V1::SetJavaScript);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionSetJavaScriptWSEL, (void*)CFPD_Action_V1::SetJavaScriptW);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionCountRenditionsSEL, (void*)CFPD_Action_V1::CountRenditions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetRenditionSEL, (void*)CFPD_Action_V1::GetRendition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionInsertRenditionSEL, (void*)CFPD_Action_V1::InsertRendition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionRemoveRenditionSEL, (void*)CFPD_Action_V1::RemoveRendition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetAnnotSEL, (void*)CFPD_Action_V1::GetAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionSetAnnotSEL, (void*)CFPD_Action_V1::SetAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetOperationTypeSEL, (void*)CFPD_Action_V1::GetOperationType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionSetOperationTypeSEL, (void*)CFPD_Action_V1::SetOperationType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetSoundStreamSEL, (void*)CFPD_Action_V1::GetSoundStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetVolumeSEL, (void*)CFPD_Action_V1::GetVolume);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionIsSynchronousSEL, (void*)CFPD_Action_V1::IsSynchronous);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionIsRepeatSEL, (void*)CFPD_Action_V1::IsRepeat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionIsMixPlaySEL, (void*)CFPD_Action_V1::IsMixPlay);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionCountOCGStatesSEL, (void*)CFPD_Action_V1::CountOCGStates);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetOCGStatesSEL, (void*)CFPD_Action_V1::GetOCGStates);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionInsertOCGStatesSEL, (void*)CFPD_Action_V1::InsertOCGStates);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionReplaceOCGStatesSEL, (void*)CFPD_Action_V1::ReplaceOCGStates);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionRemoveOCGStatesSEL, (void*)CFPD_Action_V1::RemoveOCGStates);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionIsStatePreservedSEL, (void*)CFPD_Action_V1::IsStatePreserved);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionSetStatePreservedSEL, (void*)CFPD_Action_V1::SetStatePreserved);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetSubActionsCountSEL, (void*)CFPD_Action_V1::GetSubActionsCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetSubActionSEL, (void*)CFPD_Action_V1::GetSubAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionInsertSubActionSEL, (void*)CFPD_Action_V1::InsertSubAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionRemoveSubActionSEL, (void*)CFPD_Action_V1::RemoveSubAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionRemoveAllSubActionsSEL, (void*)CFPD_Action_V1::RemoveAllSubActions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDActionSEL, FPDActionGetDictSEL, (void*)CFPD_Action_V1::GetDict);
	}
};

class CFPD_AAction_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAActionSEL, FPDAActionNewSEL, (void*)CFPD_AAction_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAActionSEL, FPDAActionDestroySEL, (void*)CFPD_AAction_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAActionSEL, FPDAActionActionExistSEL, (void*)CFPD_AAction_V1::ActionExist);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAActionSEL, FPDAActionGetActionSEL, (void*)CFPD_AAction_V1::GetAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAActionSEL, FPDAActionSetActionSEL, (void*)CFPD_AAction_V1::SetAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAActionSEL, FPDAActionRemoveActionSEL, (void*)CFPD_AAction_V1::RemoveAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAActionSEL, FPDAActionGetStartPosSEL, (void*)CFPD_AAction_V1::GetStartPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAActionSEL, FPDAActionGetNextActionSEL, (void*)CFPD_AAction_V1::GetNextAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAActionSEL, FPDAActionGetDictionarySEL, (void*)CFPD_AAction_V1::GetDictionary);
	}
};

class CFPD_DocJSActions_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocJSActionsSEL, FPDDocJSActionsNewSEL, (void*)CFPD_DocJSActions_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocJSActionsSEL, FPDDocJSActionsDestroySEL, (void*)CFPD_DocJSActions_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocJSActionsSEL, FPDDocJSActionsCountJSActionsSEL, (void*)CFPD_DocJSActions_V1::CountJSActions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocJSActionsSEL, FPDDocJSActionsGetJSActionSEL, (void*)CFPD_DocJSActions_V1::GetJSAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocJSActionsSEL, FPDDocJSActionsGetJSAction2SEL, (void*)CFPD_DocJSActions_V1::GetJSAction2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocJSActionsSEL, FPDDocJSActionsSetJSActionSEL, (void*)CFPD_DocJSActions_V1::SetJSAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocJSActionsSEL, FPDDocJSActionsFindJSActionSEL, (void*)CFPD_DocJSActions_V1::FindJSAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocJSActionsSEL, FPDDocJSActionsRemoveJSActionSEL, (void*)CFPD_DocJSActions_V1::RemoveJSAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDocJSActionsSEL, FPDDocJSActionsGetDocumentSEL, (void*)CFPD_DocJSActions_V1::GetDocument);
	}
};

class CFPD_FileSpec_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFileSpecSEL, FPDFileSpecNewSEL, (void*)CFPD_FileSpec_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFileSpecSEL, FPDFileSpecNewFromObjSEL, (void*)CFPD_FileSpec_V1::NewFromObj);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFileSpecSEL, FPDFileSpecDestroySEL, (void*)CFPD_FileSpec_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFileSpecSEL, FPDFileSpecIsURLSEL, (void*)CFPD_FileSpec_V1::IsURL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFileSpecSEL, FPDFileSpecGetFileNameSEL, (void*)CFPD_FileSpec_V1::GetFileName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFileSpecSEL, FPDFileSpecGetFileStreamSEL, (void*)CFPD_FileSpec_V1::GetFileStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFileSpecSEL, FPDFileSpecSetFileNameSEL, (void*)CFPD_FileSpec_V1::SetFileName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFileSpecSEL, FPDFileSpecSetEmbeddedFileSEL, (void*)CFPD_FileSpec_V1::SetEmbeddedFile);
	}
};

class CFPD_MediaPlayer_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDMediaPlayerSEL, FPDMediaPlayerNewSEL, (void*)CFPD_MediaPlayer_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDMediaPlayerSEL, FPDMediaPlayerNewFromDictSEL, (void*)CFPD_MediaPlayer_V1::NewFromDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDMediaPlayerSEL, FPDMediaPlayerDestroySEL, (void*)CFPD_MediaPlayer_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDMediaPlayerSEL, FPDMediaPlayerGetSoftwareURISEL, (void*)CFPD_MediaPlayer_V1::GetSoftwareURI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDMediaPlayerSEL, FPDMediaPlayerSetSoftwareURISEL, (void*)CFPD_MediaPlayer_V1::SetSoftwareURI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDMediaPlayerSEL, FPDMediaPlayerGetOSArraySEL, (void*)CFPD_MediaPlayer_V1::GetOSArray);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDMediaPlayerSEL, FPDMediaPlayerSetOSArraySEL, (void*)CFPD_MediaPlayer_V1::SetOSArray);
	}
};

class CFPD_Rendition_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionNewSEL, (void*)CFPD_Rendition_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionNewFromDictSEL, (void*)CFPD_Rendition_V1::NewFromDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionDestroySEL, (void*)CFPD_Rendition_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionHasMediaClipSEL, (void*)CFPD_Rendition_V1::HasMediaClip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetRenditionNameSEL, (void*)CFPD_Rendition_V1::GetRenditionName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetRenditionNameSEL, (void*)CFPD_Rendition_V1::SetRenditionName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetMediaClipNameSEL, (void*)CFPD_Rendition_V1::GetMediaClipName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetMediaClipNameSEL, (void*)CFPD_Rendition_V1::SetMediaClipName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetMediaClipFileSEL, (void*)CFPD_Rendition_V1::GetMediaClipFile);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetMediaClipFileSEL, (void*)CFPD_Rendition_V1::SetMediaClipFile);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetMediaClipContentTypeSEL, (void*)CFPD_Rendition_V1::GetMediaClipContentType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetMediaClipContentTypeSEL, (void*)CFPD_Rendition_V1::SetMediaClipContentType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetPermissionSEL, (void*)CFPD_Rendition_V1::GetPermission);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetPermissionSEL, (void*)CFPD_Rendition_V1::SetPermission);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetMediaDescriptionsSEL, (void*)CFPD_Rendition_V1::GetMediaDescriptions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetMediaDescriptionsSEL, (void*)CFPD_Rendition_V1::SetMediaDescriptions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetMediaBaseURLSEL, (void*)CFPD_Rendition_V1::GetMediaBaseURL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetMediaBaseURLSEL, (void*)CFPD_Rendition_V1::SetMediaBaseURL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionCountMediaPlayersSEL, (void*)CFPD_Rendition_V1::CountMediaPlayers);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetMediaPlayerSEL, (void*)CFPD_Rendition_V1::GetMediaPlayer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionAddMediaPlayerSEL, (void*)CFPD_Rendition_V1::AddMediaPlayer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionRemoveMediaPlayerSEL, (void*)CFPD_Rendition_V1::RemoveMediaPlayer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetVolumnSEL, (void*)CFPD_Rendition_V1::GetVolumn);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetVolumnSEL, (void*)CFPD_Rendition_V1::SetVolumn);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionIsControlBarVisibleSEL, (void*)CFPD_Rendition_V1::IsControlBarVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionEnableControlBarVisibleSEL, (void*)CFPD_Rendition_V1::EnableControlBarVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetFitStyleSEL, (void*)CFPD_Rendition_V1::GetFitStyle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetFitStyleSEL, (void*)CFPD_Rendition_V1::SetFitStyle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetDurationSEL, (void*)CFPD_Rendition_V1::GetDuration);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetDurationSEL, (void*)CFPD_Rendition_V1::SetDuration);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionAutoPlaySEL, (void*)CFPD_Rendition_V1::AutoPlay);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionEnableAutoPlaySEL, (void*)CFPD_Rendition_V1::EnableAutoPlay);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionRepeatCountSEL, (void*)CFPD_Rendition_V1::RepeatCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetRepeatCountSEL, (void*)CFPD_Rendition_V1::SetRepeatCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetWindowStatusSEL, (void*)CFPD_Rendition_V1::GetWindowStatus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetWindowStatusSEL, (void*)CFPD_Rendition_V1::SetWindowStatus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetBackgroundColorSEL, (void*)CFPD_Rendition_V1::GetBackgroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetBackgroundColorSEL, (void*)CFPD_Rendition_V1::SetBackgroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetBackgroundOpacitySEL, (void*)CFPD_Rendition_V1::GetBackgroundOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetBackgroundOpacitySEL, (void*)CFPD_Rendition_V1::SetBackgroundOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetMonitorSEL, (void*)CFPD_Rendition_V1::GetMonitor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetMonitorSEL, (void*)CFPD_Rendition_V1::SetMonitor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetFloatingWindowSizeSEL, (void*)CFPD_Rendition_V1::GetFloatingWindowSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetFloatingWindowSizeSEL, (void*)CFPD_Rendition_V1::SetFloatingWindowSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetFloatingWindowRelativeTypeSEL, (void*)CFPD_Rendition_V1::GetFloatingWindowRelativeType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetFloatingWindowRelativeTypeSEL, (void*)CFPD_Rendition_V1::SetFloatingWindowRelativeType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetFloatingWindowPositionSEL, (void*)CFPD_Rendition_V1::GetFloatingWindowPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetFloatingWindowPositionSEL, (void*)CFPD_Rendition_V1::SetFloatingWindowPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetFloatingWindowOffscreenSEL, (void*)CFPD_Rendition_V1::GetFloatingWindowOffscreen);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetFloatingWindowOffscreenSEL, (void*)CFPD_Rendition_V1::SetFloatingWindowOffscreen);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionHasFloatingWindowTitleBarSEL, (void*)CFPD_Rendition_V1::HasFloatingWindowTitleBar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionEnableFloatingWindowTitleBarSEL, (void*)CFPD_Rendition_V1::EnableFloatingWindowTitleBar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionHasFloatingWindowCloseButtonSEL, (void*)CFPD_Rendition_V1::HasFloatingWindowCloseButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionEnableFloatingWindowCloseButtonSEL, (void*)CFPD_Rendition_V1::EnableFloatingWindowCloseButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetFloatingWindowResizeTypeSEL, (void*)CFPD_Rendition_V1::GetFloatingWindowResizeType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetFloatingWindowResizeTypeSEL, (void*)CFPD_Rendition_V1::SetFloatingWindowResizeType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionGetFloatingWindowTitleSEL, (void*)CFPD_Rendition_V1::GetFloatingWindowTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenditionSEL, FPDRenditionSetFloatingWindowTitleSEL, (void*)CFPD_Rendition_V1::SetFloatingWindowTitle);
	}
};

class CFPD_Link_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLinkSEL, FPDLinkNewSEL, (void*)CFPD_Link_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLinkSEL, FPDLinkDestroySEL, (void*)CFPD_Link_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLinkSEL, FPDLinkGetLinkAtPointSEL, (void*)CFPD_Link_V1::GetLinkAtPoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLinkSEL, FPDLinkCountLinksSEL, (void*)CFPD_Link_V1::CountLinks);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLinkSEL, FPDLinkGetLinkSEL, (void*)CFPD_Link_V1::GetLink);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLinkSEL, FPDLinkGetRectSEL, (void*)CFPD_Link_V1::GetRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLinkSEL, FPDLinkGetDestSEL, (void*)CFPD_Link_V1::GetDest);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLinkSEL, FPDLinkGetActionSEL, (void*)CFPD_Link_V1::GetAction);
	}
};

class CFPD_Annot_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotSEL, FPDAnnotNewSEL, (void*)CFPD_Annot_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotSEL, FPDAnnotDestroySEL, (void*)CFPD_Annot_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotSEL, FPDAnnotGetAnnotDictSEL, (void*)CFPD_Annot_V1::GetAnnotDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotSEL, FPDAnnotGetSubTypeSEL, (void*)CFPD_Annot_V1::GetSubType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotSEL, FPDAnnotGetFlagsSEL, (void*)CFPD_Annot_V1::GetFlags);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotSEL, FPDAnnotGetRectSEL, (void*)CFPD_Annot_V1::GetRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotSEL, FPDAnnotDrawAppearanceSEL, (void*)CFPD_Annot_V1::DrawAppearance);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotSEL, FPDAnnotDrawInContextSEL, (void*)CFPD_Annot_V1::DrawInContext);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotSEL, FPDAnnotClearCachedAPSEL, (void*)CFPD_Annot_V1::ClearCachedAP);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotSEL, FPDAnnotDrawBorderSEL, (void*)CFPD_Annot_V1::DrawBorder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotSEL, FPDAnnotCountIRTNotesSEL, (void*)CFPD_Annot_V1::CountIRTNotes);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotSEL, FPDAnnotGetIRTNoteSEL, (void*)CFPD_Annot_V1::GetIRTNote);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotSEL, FPDAnnotGetAPFormSEL, (void*)CFPD_Annot_V1::GetAPForm);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotSEL, FPDAnnotSetPrivateDataSEL, (void*)CFPD_Annot_V1::SetPrivateData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotSEL, FPDAnnotGetPrivateDataSEL, (void*)CFPD_Annot_V1::GetPrivateData);
	}
};

class CFPD_AnnotList_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListNewSEL, (void*)CFPD_AnnotList_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListDestroySEL, (void*)CFPD_AnnotList_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListDisplayAnnotsSEL, (void*)CFPD_AnnotList_V1::DisplayAnnots);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListDisplayAnnotsExSEL, (void*)CFPD_AnnotList_V1::DisplayAnnotsEx);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListGetAtSEL, (void*)CFPD_AnnotList_V1::GetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListCountSEL, (void*)CFPD_AnnotList_V1::Count);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListGetIndexSEL, (void*)CFPD_AnnotList_V1::GetIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListRemoveAllSEL, (void*)CFPD_AnnotList_V1::RemoveAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListReplaceSEL, (void*)CFPD_AnnotList_V1::Replace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListInsertSEL, (void*)CFPD_AnnotList_V1::Insert);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListRemoveSEL, (void*)CFPD_AnnotList_V1::Remove);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListRemoveTempSEL, (void*)CFPD_AnnotList_V1::RemoveTemp);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListMoveToFirstSEL, (void*)CFPD_AnnotList_V1::MoveToFirst);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListMoveToLastSEL, (void*)CFPD_AnnotList_V1::MoveToLast);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListMoveToSEL, (void*)CFPD_AnnotList_V1::MoveTo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListGetDocumentSEL, (void*)CFPD_AnnotList_V1::GetDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListSetFixedIconParamsSEL, (void*)CFPD_AnnotList_V1::SetFixedIconParams);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListGetAnnotMatrixSEL, (void*)CFPD_AnnotList_V1::GetAnnotMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDAnnotListSEL, FPDAnnotListGetAnnotRectSEL, (void*)CFPD_AnnotList_V1::GetAnnotRect);
	}
};

class CFPD_DefaultAppearance_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDefaultAppearanceSEL, FPDDefaultAppearanceNewSEL, (void*)CFPD_DefaultAppearance_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDefaultAppearanceSEL, FPDDefaultAppearanceDestroySEL, (void*)CFPD_DefaultAppearance_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDefaultAppearanceSEL, FPDDefaultAppearanceHasFontSEL, (void*)CFPD_DefaultAppearance_V1::HasFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDefaultAppearanceSEL, FPDDefaultAppearanceGetFontStringSEL, (void*)CFPD_DefaultAppearance_V1::GetFontString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDefaultAppearanceSEL, FPDDefaultAppearanceGetFontSEL, (void*)CFPD_DefaultAppearance_V1::GetFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDefaultAppearanceSEL, FPDDefaultAppearanceSetFontSEL, (void*)CFPD_DefaultAppearance_V1::SetFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDefaultAppearanceSEL, FPDDefaultAppearanceHasColorSEL, (void*)CFPD_DefaultAppearance_V1::HasColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDefaultAppearanceSEL, FPDDefaultAppearanceGetColorStringSEL, (void*)CFPD_DefaultAppearance_V1::GetColorString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDefaultAppearanceSEL, FPDDefaultAppearanceGetColorInclueCMYKSEL, (void*)CFPD_DefaultAppearance_V1::GetColorInclueCMYK);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDefaultAppearanceSEL, FPDDefaultAppearanceGetColorSEL, (void*)CFPD_DefaultAppearance_V1::GetColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDefaultAppearanceSEL, FPDDefaultAppearanceSetColorSEL, (void*)CFPD_DefaultAppearance_V1::SetColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDefaultAppearanceSEL, FPDDefaultAppearanceHasTextMatrixSEL, (void*)CFPD_DefaultAppearance_V1::HasTextMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDefaultAppearanceSEL, FPDDefaultAppearanceGetTextMatrixStringSEL, (void*)CFPD_DefaultAppearance_V1::GetTextMatrixString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDefaultAppearanceSEL, FPDDefaultAppearanceGetTextMatrixSEL, (void*)CFPD_DefaultAppearance_V1::GetTextMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDefaultAppearanceSEL, FPDDefaultAppearanceSetTextMatrixSEL, (void*)CFPD_DefaultAppearance_V1::SetTextMatrix);
	}
};

class CFPD_FormNotify_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormNotifySEL, FPDFormNotifyNewSEL, (void*)CFPD_FormNotify_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormNotifySEL, FPDFormNotifyDestroySEL, (void*)CFPD_FormNotify_V1::Destroy);
	}
};

class CFPD_InterForm_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormNewSEL, (void*)CFPD_InterForm_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormDestroySEL, (void*)CFPD_InterForm_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormEnableUpdateAPSEL, (void*)CFPD_InterForm_V1::EnableUpdateAP);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormUpdatingAPEnabledSEL, (void*)CFPD_InterForm_V1::UpdatingAPEnabled);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGenerateNewResourceNameSEL, (void*)CFPD_InterForm_V1::GenerateNewResourceName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormAddSystemDefaultFontSEL, (void*)CFPD_InterForm_V1::AddSystemDefaultFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormAddSystemFontSEL, (void*)CFPD_InterForm_V1::AddSystemFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormAddSystemFontWSEL, (void*)CFPD_InterForm_V1::AddSystemFontW);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormAddStandardFontSEL, (void*)CFPD_InterForm_V1::AddStandardFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetNativeFontSEL, (void*)CFPD_InterForm_V1::GetNativeFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetNativeFont2SEL, (void*)CFPD_InterForm_V1::GetNativeFont2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormAddNativeFontSEL, (void*)CFPD_InterForm_V1::AddNativeFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormAddNativeFont2SEL, (void*)CFPD_InterForm_V1::AddNativeFont2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormValidateFieldNameSEL, (void*)CFPD_InterForm_V1::ValidateFieldName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormValidateFieldName2SEL, (void*)CFPD_InterForm_V1::ValidateFieldName2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormValidateFieldName3SEL, (void*)CFPD_InterForm_V1::ValidateFieldName3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormNewFieldSEL, (void*)CFPD_InterForm_V1::NewField);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormNewControlSEL, (void*)CFPD_InterForm_V1::NewControl);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormCountFieldsSEL, (void*)CFPD_InterForm_V1::CountFields);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetFieldSEL, (void*)CFPD_InterForm_V1::GetField);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetAllFieldNamesSEL, (void*)CFPD_InterForm_V1::GetAllFieldNames);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormIsValidFormFieldSEL, (void*)CFPD_InterForm_V1::IsValidFormField);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetFieldByDictSEL, (void*)CFPD_InterForm_V1::GetFieldByDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormCountControlsSEL, (void*)CFPD_InterForm_V1::CountControls);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetControlSEL, (void*)CFPD_InterForm_V1::GetControl);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormIsValidFormControlSEL, (void*)CFPD_InterForm_V1::IsValidFormControl);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormCountPageControlsSEL, (void*)CFPD_InterForm_V1::CountPageControls);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetPageControlSEL, (void*)CFPD_InterForm_V1::GetPageControl);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetControlAtPointSEL, (void*)CFPD_InterForm_V1::GetControlAtPoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetControlByDictSEL, (void*)CFPD_InterForm_V1::GetControlByDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormRenameFieldSEL, (void*)CFPD_InterForm_V1::RenameField);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormRenameField2SEL, (void*)CFPD_InterForm_V1::RenameField2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormRenameControlSEL, (void*)CFPD_InterForm_V1::RenameControl);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormDeleteFieldSEL, (void*)CFPD_InterForm_V1::DeleteField);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormDeleteField2SEL, (void*)CFPD_InterForm_V1::DeleteField2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormDeleteControlSEL, (void*)CFPD_InterForm_V1::DeleteControl);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormCountInternalFieldsSEL, (void*)CFPD_InterForm_V1::CountInternalFields);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetInternalFieldSEL, (void*)CFPD_InterForm_V1::GetInternalField);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetDocumentSEL, (void*)CFPD_InterForm_V1::GetDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetFormDictSEL, (void*)CFPD_InterForm_V1::GetFormDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormNeedConstructAPSEL, (void*)CFPD_InterForm_V1::NeedConstructAP);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormNeedConstructAP2SEL, (void*)CFPD_InterForm_V1::NeedConstructAP2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormCountFieldsInCalculationOrderSEL, (void*)CFPD_InterForm_V1::CountFieldsInCalculationOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetFieldInCalculationOrderSEL, (void*)CFPD_InterForm_V1::GetFieldInCalculationOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormFindFieldInCalculationOrderSEL, (void*)CFPD_InterForm_V1::FindFieldInCalculationOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormInsertFieldInCalculationOrderSEL, (void*)CFPD_InterForm_V1::InsertFieldInCalculationOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormMoveFieldInCalculationOrderSEL, (void*)CFPD_InterForm_V1::MoveFieldInCalculationOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormRemoveFieldInCalculationOrderSEL, (void*)CFPD_InterForm_V1::RemoveFieldInCalculationOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormCountFormFontsSEL, (void*)CFPD_InterForm_V1::CountFormFonts);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetFormFontSEL, (void*)CFPD_InterForm_V1::GetFormFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetFormFont2SEL, (void*)CFPD_InterForm_V1::GetFormFont2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetFormFont3SEL, (void*)CFPD_InterForm_V1::GetFormFont3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetNativeFormFontSEL, (void*)CFPD_InterForm_V1::GetNativeFormFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetNativeFormFont2SEL, (void*)CFPD_InterForm_V1::GetNativeFormFont2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormFindFormFontSEL, (void*)CFPD_InterForm_V1::FindFormFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormFindFormFont2SEL, (void*)CFPD_InterForm_V1::FindFormFont2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormFindFormFont3SEL, (void*)CFPD_InterForm_V1::FindFormFont3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormAddFormFontSEL, (void*)CFPD_InterForm_V1::AddFormFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormAddNativeFormFontSEL, (void*)CFPD_InterForm_V1::AddNativeFormFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormAddNativeFormFont2SEL, (void*)CFPD_InterForm_V1::AddNativeFormFont2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormRemoveFormFontSEL, (void*)CFPD_InterForm_V1::RemoveFormFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormRemoveFormFont2SEL, (void*)CFPD_InterForm_V1::RemoveFormFont2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetDefaultAppearanceSEL, (void*)CFPD_InterForm_V1::GetDefaultAppearance);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormSetDefaultAppearanceSEL, (void*)CFPD_InterForm_V1::SetDefaultAppearance);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetDefaultFormFontSEL, (void*)CFPD_InterForm_V1::GetDefaultFormFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormSetDefaultFormFontSEL, (void*)CFPD_InterForm_V1::SetDefaultFormFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetFormAlignmentSEL, (void*)CFPD_InterForm_V1::GetFormAlignment);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormSetFormAlignmentSEL, (void*)CFPD_InterForm_V1::SetFormAlignment);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormCheckRequiredFieldsSEL, (void*)CFPD_InterForm_V1::CheckRequiredFields);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormExportToFDFSEL, (void*)CFPD_InterForm_V1::ExportToFDF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormExportToFDF2SEL, (void*)CFPD_InterForm_V1::ExportToFDF2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormImportFromFDFSEL, (void*)CFPD_InterForm_V1::ImportFromFDF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormResetFormSEL, (void*)CFPD_InterForm_V1::ResetForm);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormResetForm2SEL, (void*)CFPD_InterForm_V1::ResetForm2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormReloadFormSEL, (void*)CFPD_InterForm_V1::ReloadForm);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetFormNotifySEL, (void*)CFPD_InterForm_V1::GetFormNotify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormSetFormNotifySEL, (void*)CFPD_InterForm_V1::SetFormNotify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormGetPageWithWidgetSEL, (void*)CFPD_InterForm_V1::GetPageWithWidget);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormIsUpdatedSEL, (void*)CFPD_InterForm_V1::IsUpdated);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormClearUpdatedFlagSEL, (void*)CFPD_InterForm_V1::ClearUpdatedFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormFixPageFieldsSEL, (void*)CFPD_InterForm_V1::FixPageFields);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInterFormSEL, FPDInterFormAddControlSEL, (void*)CFPD_InterForm_V1::AddControl);
	}
};

class CFPD_FormField_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetFullNameSEL, (void*)CFPD_FormField_V1::GetFullName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetTypeSEL, (void*)CFPD_FormField_V1::GetType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetFlagsSEL, (void*)CFPD_FormField_V1::GetFlags);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetInterFormSEL, (void*)CFPD_FormField_V1::GetInterForm);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetFieldDictSEL, (void*)CFPD_FormField_V1::GetFieldDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldResetFieldSEL, (void*)CFPD_FormField_V1::ResetField);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldCountControlsSEL, (void*)CFPD_FormField_V1::CountControls);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetControlSEL, (void*)CFPD_FormField_V1::GetControl);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetControlIndexSEL, (void*)CFPD_FormField_V1::GetControlIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetFieldTypeSEL, (void*)CFPD_FormField_V1::GetFieldType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetAdditionalActionSEL, (void*)CFPD_FormField_V1::GetAdditionalAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetAdditionalActionSEL, (void*)CFPD_FormField_V1::SetAdditionalAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetAlternateNameSEL, (void*)CFPD_FormField_V1::GetAlternateName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetAlternateNameSEL, (void*)CFPD_FormField_V1::SetAlternateName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetAlternateNameWSEL, (void*)CFPD_FormField_V1::SetAlternateNameW);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetMappingNameSEL, (void*)CFPD_FormField_V1::GetMappingName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetMappingNameSEL, (void*)CFPD_FormField_V1::SetMappingName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetMappingNameWSEL, (void*)CFPD_FormField_V1::SetMappingNameW);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetFieldFlagsSEL, (void*)CFPD_FormField_V1::GetFieldFlags);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetFieldFlagsSEL, (void*)CFPD_FormField_V1::SetFieldFlags);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetDefaultStyleSEL, (void*)CFPD_FormField_V1::GetDefaultStyle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetDefaultStyleSEL, (void*)CFPD_FormField_V1::SetDefaultStyle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetRichTextStringSEL, (void*)CFPD_FormField_V1::GetRichTextString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetRichTextStringSEL, (void*)CFPD_FormField_V1::SetRichTextString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetRichTextStringWSEL, (void*)CFPD_FormField_V1::SetRichTextStringW);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetValueSEL, (void*)CFPD_FormField_V1::GetValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetDefaultValueSEL, (void*)CFPD_FormField_V1::GetDefaultValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetValueSEL, (void*)CFPD_FormField_V1::SetValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetDefaultValueSEL, (void*)CFPD_FormField_V1::SetDefaultValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetMaxLenSEL, (void*)CFPD_FormField_V1::GetMaxLen);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetMaxLenSEL, (void*)CFPD_FormField_V1::SetMaxLen);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldCountSelectedItemsSEL, (void*)CFPD_FormField_V1::CountSelectedItems);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetSelectedIndexSEL, (void*)CFPD_FormField_V1::GetSelectedIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldClearSelectionSEL, (void*)CFPD_FormField_V1::ClearSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldIsItemSelectedSEL, (void*)CFPD_FormField_V1::IsItemSelected);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetItemSelectionSEL, (void*)CFPD_FormField_V1::SetItemSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldIsItemDefaultSelectedSEL, (void*)CFPD_FormField_V1::IsItemDefaultSelected);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetItemDefaultSelectionSEL, (void*)CFPD_FormField_V1::SetItemDefaultSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetDefaultSelectedItemSEL, (void*)CFPD_FormField_V1::GetDefaultSelectedItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldCountOptionsSEL, (void*)CFPD_FormField_V1::CountOptions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetOptionLabelSEL, (void*)CFPD_FormField_V1::GetOptionLabel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetOptionValueSEL, (void*)CFPD_FormField_V1::GetOptionValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldInsertOptionSEL, (void*)CFPD_FormField_V1::InsertOption);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldFindOptionSEL, (void*)CFPD_FormField_V1::FindOption);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldFindOptionValueSEL, (void*)CFPD_FormField_V1::FindOptionValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetOptionLabelSEL, (void*)CFPD_FormField_V1::SetOptionLabel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetOptionValueSEL, (void*)CFPD_FormField_V1::SetOptionValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldDeleteOptionSEL, (void*)CFPD_FormField_V1::DeleteOption);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldClearOptionsSEL, (void*)CFPD_FormField_V1::ClearOptions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldCheckControlSEL, (void*)CFPD_FormField_V1::CheckControl);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldDefaultCheckControlSEL, (void*)CFPD_FormField_V1::DefaultCheckControl);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldUpdateUnisonStatusSEL, (void*)CFPD_FormField_V1::UpdateUnisonStatus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetTopVisibleIndexSEL, (void*)CFPD_FormField_V1::GetTopVisibleIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSetTopVisibleIndexSEL, (void*)CFPD_FormField_V1::SetTopVisibleIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldCountSelectedOptionsSEL, (void*)CFPD_FormField_V1::CountSelectedOptions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetSelectedOptionIndexSEL, (void*)CFPD_FormField_V1::GetSelectedOptionIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldIsOptionSelectedSEL, (void*)CFPD_FormField_V1::IsOptionSelected);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldSelectOptionSEL, (void*)CFPD_FormField_V1::SelectOption);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldClearSelectedOptionsSEL, (void*)CFPD_FormField_V1::ClearSelectedOptions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetFontSizeSEL, (void*)CFPD_FormField_V1::GetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormFieldSEL, FPDFormFieldGetFontSEL, (void*)CFPD_FormField_V1::GetFont);
	}
};

class CFPD_IconFit_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIconFitSEL, FPDIconFitNewSEL, (void*)CFPD_IconFit_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIconFitSEL, FPDIconFitDestroySEL, (void*)CFPD_IconFit_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIconFitSEL, FPDIconFitGetScaleMethodSEL, (void*)CFPD_IconFit_V1::GetScaleMethod);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIconFitSEL, FPDIconFitSetScaleMethodSEL, (void*)CFPD_IconFit_V1::SetScaleMethod);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIconFitSEL, FPDIconFitIsProportionalScaleSEL, (void*)CFPD_IconFit_V1::IsProportionalScale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIconFitSEL, FPDIconFitProportionalScaleSEL, (void*)CFPD_IconFit_V1::ProportionalScale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIconFitSEL, FPDIconFitGetIconPositionSEL, (void*)CFPD_IconFit_V1::GetIconPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIconFitSEL, FPDIconFitSetIconPositionSEL, (void*)CFPD_IconFit_V1::SetIconPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIconFitSEL, FPDIconFitGetFittingBoundsSEL, (void*)CFPD_IconFit_V1::GetFittingBounds);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIconFitSEL, FPDIconFitSetFittingBoundsSEL, (void*)CFPD_IconFit_V1::SetFittingBounds);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIconFitSEL, FPDIconFitGetDictSEL, (void*)CFPD_IconFit_V1::GetDict);
	}
};

class CFPD_FormControl_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetTypeSEL, (void*)CFPD_FormControl_V1::GetType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetInterFormSEL, (void*)CFPD_FormControl_V1::GetInterForm);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetFieldSEL, (void*)CFPD_FormControl_V1::GetField);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetWidgetSEL, (void*)CFPD_FormControl_V1::GetWidget);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetRectSEL, (void*)CFPD_FormControl_V1::GetRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlDrawControlSEL, (void*)CFPD_FormControl_V1::DrawControl);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetCheckedAPStateSEL, (void*)CFPD_FormControl_V1::GetCheckedAPState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetExportValueSEL, (void*)CFPD_FormControl_V1::GetExportValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetExportValueSEL, (void*)CFPD_FormControl_V1::SetExportValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlIsCheckedSEL, (void*)CFPD_FormControl_V1::IsChecked);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlIsDefaultCheckedSEL, (void*)CFPD_FormControl_V1::IsDefaultChecked);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetHighlightingModeSEL, (void*)CFPD_FormControl_V1::GetHighlightingMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetHighlightingModeSEL, (void*)CFPD_FormControl_V1::SetHighlightingMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlHasMKEntrySEL, (void*)CFPD_FormControl_V1::HasMKEntry);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlRemoveMKEntrySEL, (void*)CFPD_FormControl_V1::RemoveMKEntry);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetRotationSEL, (void*)CFPD_FormControl_V1::GetRotation);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetRotationSEL, (void*)CFPD_FormControl_V1::SetRotation);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetBorderColorSEL, (void*)CFPD_FormControl_V1::GetBorderColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetOriginalBorderColorSEL, (void*)CFPD_FormControl_V1::GetOriginalBorderColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetOriginalBorderColor2SEL, (void*)CFPD_FormControl_V1::GetOriginalBorderColor2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetBorderColorSEL, (void*)CFPD_FormControl_V1::SetBorderColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetBackgroundColorSEL, (void*)CFPD_FormControl_V1::GetBackgroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetOriginalBackgroundColorSEL, (void*)CFPD_FormControl_V1::GetOriginalBackgroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetOriginalBackgroundColor2SEL, (void*)CFPD_FormControl_V1::GetOriginalBackgroundColor2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetBackgroundColorSEL, (void*)CFPD_FormControl_V1::SetBackgroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetNormalCaptionSEL, (void*)CFPD_FormControl_V1::GetNormalCaption);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetNormalCaptionSEL, (void*)CFPD_FormControl_V1::SetNormalCaption);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetNormalCaptionWSEL, (void*)CFPD_FormControl_V1::SetNormalCaptionW);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetRolloverCaptionSEL, (void*)CFPD_FormControl_V1::GetRolloverCaption);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetRolloverCaptionSEL, (void*)CFPD_FormControl_V1::SetRolloverCaption);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetRolloverCaptionWSEL, (void*)CFPD_FormControl_V1::SetRolloverCaptionW);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetDownCaptionSEL, (void*)CFPD_FormControl_V1::GetDownCaption);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetDownCaptionSEL, (void*)CFPD_FormControl_V1::SetDownCaption);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetDownCaptionWSEL, (void*)CFPD_FormControl_V1::SetDownCaptionW);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetNormalIconSEL, (void*)CFPD_FormControl_V1::GetNormalIcon);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetNormalIconSEL, (void*)CFPD_FormControl_V1::SetNormalIcon);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetRolloverIconSEL, (void*)CFPD_FormControl_V1::GetRolloverIcon);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetRolloverIconSEL, (void*)CFPD_FormControl_V1::SetRolloverIcon);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetDownIconSEL, (void*)CFPD_FormControl_V1::GetDownIcon);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetDownIconSEL, (void*)CFPD_FormControl_V1::SetDownIcon);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetIconFitSEL, (void*)CFPD_FormControl_V1::GetIconFit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetIconFitSEL, (void*)CFPD_FormControl_V1::SetIconFit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetTextPositionSEL, (void*)CFPD_FormControl_V1::GetTextPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetTextPositionSEL, (void*)CFPD_FormControl_V1::SetTextPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetActionSEL, (void*)CFPD_FormControl_V1::GetAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetActionSEL, (void*)CFPD_FormControl_V1::SetAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetAdditionalActionSEL, (void*)CFPD_FormControl_V1::GetAdditionalAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetAdditionalActionSEL, (void*)CFPD_FormControl_V1::SetAdditionalAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetDefaultAppearanceSEL, (void*)CFPD_FormControl_V1::GetDefaultAppearance);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetDefaultAppearanceSEL, (void*)CFPD_FormControl_V1::SetDefaultAppearance);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetDefaultControlFontSEL, (void*)CFPD_FormControl_V1::GetDefaultControlFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetDefaultControlFontSEL, (void*)CFPD_FormControl_V1::SetDefaultControlFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlGetControlAlignmentSEL, (void*)CFPD_FormControl_V1::GetControlAlignment);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormControlSEL, FPDFormControlSetControlAlignmentSEL, (void*)CFPD_FormControl_V1::SetControlAlignment);
	}
};

class CFPD_FDFDoc_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocNewSEL, (void*)CFPD_FDFDoc_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocOpenFromFileSEL, (void*)CFPD_FDFDoc_V1::OpenFromFile);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocPareMemorySEL, (void*)CFPD_FDFDoc_V1::PareMemory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocDestroySEL, (void*)CFPD_FDFDoc_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocWriteFileSEL, (void*)CFPD_FDFDoc_V1::WriteFile);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocGetRootSEL, (void*)CFPD_FDFDoc_V1::GetRoot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocGetWin32PathSEL, (void*)CFPD_FDFDoc_V1::GetWin32Path);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocGetIndirectObjectSEL, (void*)CFPD_FDFDoc_V1::GetIndirectObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocGetIndirectTypeSEL, (void*)CFPD_FDFDoc_V1::GetIndirectType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocAddIndirectObjectSEL, (void*)CFPD_FDFDoc_V1::AddIndirectObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocReleaseIndirectObjectSEL, (void*)CFPD_FDFDoc_V1::ReleaseIndirectObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocDeleteIndirectObjectSEL, (void*)CFPD_FDFDoc_V1::DeleteIndirectObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocImportIndirectObjectSEL, (void*)CFPD_FDFDoc_V1::ImportIndirectObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocImportExternalObjectSEL, (void*)CFPD_FDFDoc_V1::ImportExternalObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocInsertIndirectObjectSEL, (void*)CFPD_FDFDoc_V1::InsertIndirectObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocGetLastobjNumSEL, (void*)CFPD_FDFDoc_V1::GetLastobjNum);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocReloadFileStreamsSEL, (void*)CFPD_FDFDoc_V1::ReloadFileStreams);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocGetStartPositionSEL, (void*)CFPD_FDFDoc_V1::GetStartPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocGetNextAssocSEL, (void*)CFPD_FDFDoc_V1::GetNextAssoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocIsModifiedSEL, (void*)CFPD_FDFDoc_V1::IsModified);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocClearModifiedSEL, (void*)CFPD_FDFDoc_V1::ClearModified);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocWriteBufSEL, (void*)CFPD_FDFDoc_V1::WriteBuf);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocGetAnnotCountSEL, (void*)CFPD_FDFDoc_V1::GetAnnotCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocGetAnnotDictSEL, (void*)CFPD_FDFDoc_V1::GetAnnotDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocGetAnnotPageIndexSEL, (void*)CFPD_FDFDoc_V1::GetAnnotPageIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocExportAnnotToPDFPageSEL, (void*)CFPD_FDFDoc_V1::ExportAnnotToPDFPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocImportPDFAnnotSEL, (void*)CFPD_FDFDoc_V1::ImportPDFAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFDFDocSEL, FPDFDFDocSetPDFPathSEL, (void*)CFPD_FDFDoc_V1::SetPDFPath);
	}
};

// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
class CFPD_Object_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectGetTypeSEL, (void*)CFPD_Object_V1::GetType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectGetobjNumSEL, (void*)CFPD_Object_V1::GetobjNum);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectIsIdenticalSEL, (void*)CFPD_Object_V1::IsIdentical);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectCloneSEL, (void*)CFPD_Object_V1::Clone);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectCloneRefToDocSEL, (void*)CFPD_Object_V1::CloneRefToDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectCloneRefToFDFDocSEL, (void*)CFPD_Object_V1::CloneRefToFDFDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectGetDirectSEL, (void*)CFPD_Object_V1::GetDirect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectDestroySEL, (void*)CFPD_Object_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectGetStringSEL, (void*)CFPD_Object_V1::GetString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectGetUnicodeTextSEL, (void*)CFPD_Object_V1::GetUnicodeText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectGetNumberSEL, (void*)CFPD_Object_V1::GetNumber);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectGetIntegerSEL, (void*)CFPD_Object_V1::GetInteger);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectGetDictSEL, (void*)CFPD_Object_V1::GetDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectGetArraySEL, (void*)CFPD_Object_V1::GetArray);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectSetStringSEL, (void*)CFPD_Object_V1::SetString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectSetUnicodeTextSEL, (void*)CFPD_Object_V1::SetUnicodeText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectGetDirectTypeSEL, (void*)CFPD_Object_V1::GetDirectType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectIsModifiedSEL, (void*)CFPD_Object_V1::IsModified);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectSetModifiedSEL, (void*)CFPD_Object_V1::SetModified);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectGetContainerSEL, (void*)CFPD_Object_V1::GetContainer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectSetContainerSEL, (void*)CFPD_Object_V1::SetContainer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjectSEL, FPDObjectParseStringSEL, (void*)CFPD_Object_V1::ParseString);
	}
};

class CFPD_Boolean_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDBooleanSEL, FPDBooleanNewSEL, (void*)CFPD_Boolean_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDBooleanSEL, FPDBooleanIdenticalSEL, (void*)CFPD_Boolean_V1::Identical);
	}
};

class CFPD_Number_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNumberSEL, FPDNumberNewByIntSEL, (void*)CFPD_Number_V1::NewByInt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNumberSEL, FPDNumberNewByFloatSEL, (void*)CFPD_Number_V1::NewByFloat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNumberSEL, FPDNumberNewByStrSEL, (void*)CFPD_Number_V1::NewByStr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNumberSEL, FPDNumberNewFromDataSEL, (void*)CFPD_Number_V1::NewFromData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNumberSEL, FPDNumberIdenticalSEL, (void*)CFPD_Number_V1::Identical);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNumberSEL, FPDNumberGetStringSEL, (void*)CFPD_Number_V1::GetString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNumberSEL, FPDNumberSetStringSEL, (void*)CFPD_Number_V1::SetString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNumberSEL, FPDNumberIsIntegerSEL, (void*)CFPD_Number_V1::IsInteger);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNumberSEL, FPDNumberGetIntegerSEL, (void*)CFPD_Number_V1::GetInteger);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNumberSEL, FPDNumberGetNumberSEL, (void*)CFPD_Number_V1::GetNumber);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNumberSEL, FPDNumberSetNumberSEL, (void*)CFPD_Number_V1::SetNumber);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNumberSEL, FPDNumberGetFloatSEL, (void*)CFPD_Number_V1::GetFloat);
	}
};

class CFPD_String_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStringSEL, FPDStringNewSEL, (void*)CFPD_String_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStringSEL, FPDStringNewWSEL, (void*)CFPD_String_V1::NewW);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStringSEL, FPDStringGetStringSEL, (void*)CFPD_String_V1::GetString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStringSEL, FPDStringIdenticalSEL, (void*)CFPD_String_V1::Identical);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStringSEL, FPDStringSetHexSEL, (void*)CFPD_String_V1::SetHex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStringSEL, FPDStringIsHexSEL, (void*)CFPD_String_V1::IsHex);
	}
};

class CFPD_Name_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNameSEL, FPDNameNewSEL, (void*)CFPD_Name_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNameSEL, FPDNameGetStringSEL, (void*)CFPD_Name_V1::GetString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNameSEL, FPDNameIdenticalSEL, (void*)CFPD_Name_V1::Identical);
	}
};

class CFPD_Array_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayNewSEL, (void*)CFPD_Array_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayGetCountSEL, (void*)CFPD_Array_V1::GetCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayGetElementSEL, (void*)CFPD_Array_V1::GetElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayGetElementValueSEL, (void*)CFPD_Array_V1::GetElementValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayGetMatrixSEL, (void*)CFPD_Array_V1::GetMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayGetRectSEL, (void*)CFPD_Array_V1::GetRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayGetStringSEL, (void*)CFPD_Array_V1::GetString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayGetIntegerSEL, (void*)CFPD_Array_V1::GetInteger);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayGetNumberSEL, (void*)CFPD_Array_V1::GetNumber);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayGetDictSEL, (void*)CFPD_Array_V1::GetDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayGetStreamSEL, (void*)CFPD_Array_V1::GetStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayGetArraySEL, (void*)CFPD_Array_V1::GetArray);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayGetFloatSEL, (void*)CFPD_Array_V1::GetFloat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArraySetAtSEL, (void*)CFPD_Array_V1::SetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayInsertAtSEL, (void*)CFPD_Array_V1::InsertAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayRemoveAtSEL, (void*)CFPD_Array_V1::RemoveAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayAddSEL, (void*)CFPD_Array_V1::Add);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayAddNumberSEL, (void*)CFPD_Array_V1::AddNumber);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayAddIntegerSEL, (void*)CFPD_Array_V1::AddInteger);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayAddStringSEL, (void*)CFPD_Array_V1::AddString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayAddNameSEL, (void*)CFPD_Array_V1::AddName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayAddReferenceToDocSEL, (void*)CFPD_Array_V1::AddReferenceToDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayAddReferenceToFDFDocSEL, (void*)CFPD_Array_V1::AddReferenceToFDFDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayAddReference2ToDocSEL, (void*)CFPD_Array_V1::AddReference2ToDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayAddReference2ToFDFDocSEL, (void*)CFPD_Array_V1::AddReference2ToFDFDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDArraySEL, FPDArrayIsIdenticalSEL, (void*)CFPD_Array_V1::IsIdentical);
	}
};

class CFPD_Dictionary_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryNewSEL, (void*)CFPD_Dictionary_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetElementSEL, (void*)CFPD_Dictionary_V1::GetElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetElementValueSEL, (void*)CFPD_Dictionary_V1::GetElementValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetStringSEL, (void*)CFPD_Dictionary_V1::GetString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetUnicodeTextSEL, (void*)CFPD_Dictionary_V1::GetUnicodeText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetIntegerSEL, (void*)CFPD_Dictionary_V1::GetInteger);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetInteger2SEL, (void*)CFPD_Dictionary_V1::GetInteger2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetBooleanSEL, (void*)CFPD_Dictionary_V1::GetBoolean);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetNumberSEL, (void*)CFPD_Dictionary_V1::GetNumber);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetDictSEL, (void*)CFPD_Dictionary_V1::GetDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetStreamSEL, (void*)CFPD_Dictionary_V1::GetStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetArraySEL, (void*)CFPD_Dictionary_V1::GetArray);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetRectSEL, (void*)CFPD_Dictionary_V1::GetRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetMatrixSEL, (void*)CFPD_Dictionary_V1::GetMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetFloatSEL, (void*)CFPD_Dictionary_V1::GetFloat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryKeyExistSEL, (void*)CFPD_Dictionary_V1::KeyExist);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetStartPositionSEL, (void*)CFPD_Dictionary_V1::GetStartPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetNextElementSEL, (void*)CFPD_Dictionary_V1::GetNextElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionarySetAtSEL, (void*)CFPD_Dictionary_V1::SetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionarySetAtNameSEL, (void*)CFPD_Dictionary_V1::SetAtName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionarySetAtStringSEL, (void*)CFPD_Dictionary_V1::SetAtString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionarySetAtIntegerSEL, (void*)CFPD_Dictionary_V1::SetAtInteger);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionarySetAtNumberSEL, (void*)CFPD_Dictionary_V1::SetAtNumber);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionarySetAtReferenceToDocSEL, (void*)CFPD_Dictionary_V1::SetAtReferenceToDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionarySetAtReferenceToFDFDocSEL, (void*)CFPD_Dictionary_V1::SetAtReferenceToFDFDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionarySetAtReference2ToDocSEL, (void*)CFPD_Dictionary_V1::SetAtReference2ToDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionarySetAtReference2ToFDFDocSEL, (void*)CFPD_Dictionary_V1::SetAtReference2ToFDFDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionarySetAtRectSEL, (void*)CFPD_Dictionary_V1::SetAtRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionarySetAtMatrixSEL, (void*)CFPD_Dictionary_V1::SetAtMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionarySetAtBooleanSEL, (void*)CFPD_Dictionary_V1::SetAtBoolean);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryRemoveAtSEL, (void*)CFPD_Dictionary_V1::RemoveAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryReplaceKeySEL, (void*)CFPD_Dictionary_V1::ReplaceKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryIdenticalSEL, (void*)CFPD_Dictionary_V1::Identical);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetCountSEL, (void*)CFPD_Dictionary_V1::GetCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryAddValueSEL, (void*)CFPD_Dictionary_V1::AddValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDDictionarySEL, FPDDictionaryGetBoolean2SEL, (void*)CFPD_Dictionary_V1::GetBoolean2);
	}
};

class CFPD_Stream_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamSEL, FPDStreamNewSEL, (void*)CFPD_Stream_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamSEL, FPDStreamInitStreamSEL, (void*)CFPD_Stream_V1::InitStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamSEL, FPDStreamGetDictSEL, (void*)CFPD_Stream_V1::GetDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamSEL, FPDStreamSetDataSEL, (void*)CFPD_Stream_V1::SetData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamSEL, FPDStreamIdenticalSEL, (void*)CFPD_Stream_V1::Identical);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamSEL, FPDStreamGetStreamFilterSEL, (void*)CFPD_Stream_V1::GetStreamFilter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamSEL, FPDStreamGetRawSizeSEL, (void*)CFPD_Stream_V1::GetRawSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamSEL, FPDStreamReadRawDataSEL, (void*)CFPD_Stream_V1::ReadRawData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamSEL, FPDStreamIsMemoryBasedSEL, (void*)CFPD_Stream_V1::IsMemoryBased);
	}
};

class CFPD_StreamAcc_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamAccSEL, FPDStreamAccNewSEL, (void*)CFPD_StreamAcc_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamAccSEL, FPDStreamAccDestroySEL, (void*)CFPD_StreamAcc_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamAccSEL, FPDStreamAccLoadAllDataSEL, (void*)CFPD_StreamAcc_V1::LoadAllData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamAccSEL, FPDStreamAccGetStreamSEL, (void*)CFPD_StreamAcc_V1::GetStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamAccSEL, FPDStreamAccGetDictSEL, (void*)CFPD_StreamAcc_V1::GetDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamAccSEL, FPDStreamAccGetDataSEL, (void*)CFPD_StreamAcc_V1::GetData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamAccSEL, FPDStreamAccGetSizeSEL, (void*)CFPD_StreamAcc_V1::GetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamAccSEL, FPDStreamAccDetachDataSEL, (void*)CFPD_StreamAcc_V1::DetachData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamAccSEL, FPDStreamAccGetImageDecoderSEL, (void*)CFPD_StreamAcc_V1::GetImageDecoder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamAccSEL, FPDStreamAccGetImageParamSEL, (void*)CFPD_StreamAcc_V1::GetImageParam);
	}
};

class CFPD_StreamFilter_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamFilterSEL, FPDStreamFilterDestroySEL, (void*)CFPD_StreamFilter_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamFilterSEL, FPDStreamFilterReadBlockSEL, (void*)CFPD_StreamFilter_V1::ReadBlock);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamFilterSEL, FPDStreamFilterGetSrcPosSEL, (void*)CFPD_StreamFilter_V1::GetSrcPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStreamFilterSEL, FPDStreamFilterGetStreamSEL, (void*)CFPD_StreamFilter_V1::GetStream);
	}
};

class CFPD_Null_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDNullSEL, FPDNullNewSEL, (void*)CFPD_Null_V1::New);
	}
};

class CFPD_Reference_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDReferenceSEL, FPDReferenceNewSEL, (void*)CFPD_Reference_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDReferenceSEL, FPDReferenceNew2SEL, (void*)CFPD_Reference_V1::New2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDReferenceSEL, FPDReferenceGetRefObjNumSEL, (void*)CFPD_Reference_V1::GetRefObjNum);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDReferenceSEL, FPDReferenceSetRefToDocSEL, (void*)CFPD_Reference_V1::SetRefToDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDReferenceSEL, FPDReferenceSetRefToFDFDocSEL, (void*)CFPD_Reference_V1::SetRefToFDFDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDReferenceSEL, FPDReferenceIdenticalSEL, (void*)CFPD_Reference_V1::Identical);
	}
};

// fpd_objsImpl.h end

// In file fpd_pageImpl.h
class CFPD_Page_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageNewSEL, (void*)CFPD_Page_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageDestroySEL, (void*)CFPD_Page_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageContinueParseSEL, (void*)CFPD_Page_V1::ContinueParse);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetParseStateSEL, (void*)CFPD_Page_V1::GetParseState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageIsParsedSEL, (void*)CFPD_Page_V1::IsParsed);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageEstimateParseProgressSEL, (void*)CFPD_Page_V1::EstimateParseProgress);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetFirstObjectPositionSEL, (void*)CFPD_Page_V1::GetFirstObjectPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetLastObjectPositionSEL, (void*)CFPD_Page_V1::GetLastObjectPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetNextObjectSEL, (void*)CFPD_Page_V1::GetNextObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetPrevObjectSEL, (void*)CFPD_Page_V1::GetPrevObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetObjectAtSEL, (void*)CFPD_Page_V1::GetObjectAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageCountObjectsSEL, (void*)CFPD_Page_V1::CountObjects);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetObjectIndexSEL, (void*)CFPD_Page_V1::GetObjectIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetObjectByIndexSEL, (void*)CFPD_Page_V1::GetObjectByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageReplaceObjectSEL, (void*)CFPD_Page_V1::ReplaceObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageInsertObjectSEL, (void*)CFPD_Page_V1::InsertObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageRemoveObjectSEL, (void*)CFPD_Page_V1::RemoveObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageMoveObjectSEL, (void*)CFPD_Page_V1::MoveObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageDebugOutputSEL, (void*)CFPD_Page_V1::DebugOutput);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageTransformSEL, (void*)CFPD_Page_V1::Transform);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageBackgroundAlphaNeededSEL, (void*)CFPD_Page_V1::BackgroundAlphaNeeded);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageCalcBoundingBoxSEL, (void*)CFPD_Page_V1::CalcBoundingBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetDictSEL, (void*)CFPD_Page_V1::GetDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetDocumentSEL, (void*)CFPD_Page_V1::GetDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageRealizeResourceSEL, (void*)CFPD_Page_V1::RealizeResource);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageFindCSNameSEL, (void*)CFPD_Page_V1::FindCSName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageFindFontNameSEL, (void*)CFPD_Page_V1::FindFontName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageLoadSEL, (void*)CFPD_Page_V1::Load);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageStartParseSEL, (void*)CFPD_Page_V1::StartParse);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageParseContentSEL, (void*)CFPD_Page_V1::ParseContent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetDisplayMatrixSEL, (void*)CFPD_Page_V1::GetDisplayMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetPageWidthSEL, (void*)CFPD_Page_V1::GetPageWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetPageHeightSEL, (void*)CFPD_Page_V1::GetPageHeight);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetPageBBoxSEL, (void*)CFPD_Page_V1::GetPageBBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetPageMatrixSEL, (void*)CFPD_Page_V1::GetPageMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetPageAttrSEL, (void*)CFPD_Page_V1::GetPageAttr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetRenderCacheSEL, (void*)CFPD_Page_V1::GetRenderCache);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageClearRenderCacheSEL, (void*)CFPD_Page_V1::ClearRenderCache);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGenerateContentSEL, (void*)CFPD_Page_V1::GenerateContent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetPageTextSEL, (void*)CFPD_Page_V1::GetPageText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetPageText_UnicodeSEL, (void*)CFPD_Page_V1::GetPageText_Unicode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageNewContentGeneratorSEL, (void*)CFPD_Page_V1::NewContentGenerator);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageDestroyContentGeneratorSEL, (void*)CFPD_Page_V1::DestroyContentGenerator);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageStartGenerateContentSEL, (void*)CFPD_Page_V1::StartGenerateContent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageContinueGenerateContentSEL, (void*)CFPD_Page_V1::ContinueGenerateContent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageGetResourcesDictionarySEL, (void*)CFPD_Page_V1::GetResourcesDictionary);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageSetResourcesDictionarySEL, (void*)CFPD_Page_V1::SetResourcesDictionary);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageSEL, FPDPageStartGenerateContent2SEL, (void*)CFPD_Page_V1::StartGenerateContent2);
	}
};

class CFPD_ParseOptions_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParseOptionsSEL, FPDParseOptionsNewSEL, (void*)CFPD_ParseOptions_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParseOptionsSEL, FPDParseOptionsDestroySEL, (void*)CFPD_ParseOptions_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParseOptionsSEL, FPDParseOptionsSetTextFlagSEL, (void*)CFPD_ParseOptions_V1::SetTextFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParseOptionsSEL, FPDParseOptionsSetMarkedContentLoadFlagSEL, (void*)CFPD_ParseOptions_V1::SetMarkedContentLoadFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParseOptionsSEL, FPDParseOptionsSetFormGenerateFlagSEL, (void*)CFPD_ParseOptions_V1::SetFormGenerateFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParseOptionsSEL, FPDParseOptionsSetInlineImageDecodeFlagSEL, (void*)CFPD_ParseOptions_V1::SetInlineImageDecodeFlag);
	}
};

class CFPD_Form_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormNewSEL, (void*)CFPD_Form_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormDestroySEL, (void*)CFPD_Form_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormContinueParseSEL, (void*)CFPD_Form_V1::ContinueParse);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormGetParseStateSEL, (void*)CFPD_Form_V1::GetParseState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormIsParsedSEL, (void*)CFPD_Form_V1::IsParsed);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormEstimateParseProgressSEL, (void*)CFPD_Form_V1::EstimateParseProgress);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormGetFirstObjectPositionSEL, (void*)CFPD_Form_V1::GetFirstObjectPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormGetLastObjectPositionSEL, (void*)CFPD_Form_V1::GetLastObjectPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormGetNextObjectSEL, (void*)CFPD_Form_V1::GetNextObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormGetPrevObjectSEL, (void*)CFPD_Form_V1::GetPrevObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormGetObjectAtSEL, (void*)CFPD_Form_V1::GetObjectAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormCountObjectsSEL, (void*)CFPD_Form_V1::CountObjects);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormGetObjectIndexSEL, (void*)CFPD_Form_V1::GetObjectIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormGetObjectByIndexSEL, (void*)CFPD_Form_V1::GetObjectByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormReplaceObjectSEL, (void*)CFPD_Form_V1::ReplaceObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormInsertObjectSEL, (void*)CFPD_Form_V1::InsertObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormRemoveObjectSEL, (void*)CFPD_Form_V1::RemoveObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormMoveObjectSEL, (void*)CFPD_Form_V1::MoveObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormDebugOutputSEL, (void*)CFPD_Form_V1::DebugOutput);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormTransformSEL, (void*)CFPD_Form_V1::Transform);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormBackgroundAlphaNeededSEL, (void*)CFPD_Form_V1::BackgroundAlphaNeeded);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormCalcBoundingBoxSEL, (void*)CFPD_Form_V1::CalcBoundingBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormGetDictSEL, (void*)CFPD_Form_V1::GetDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormGetDocumentSEL, (void*)CFPD_Form_V1::GetDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormRealizeResourceSEL, (void*)CFPD_Form_V1::RealizeResource);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormFindCSNameSEL, (void*)CFPD_Form_V1::FindCSName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormFindFontNameSEL, (void*)CFPD_Form_V1::FindFontName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormGetFormStreamSEL, (void*)CFPD_Form_V1::GetFormStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormStartParseSEL, (void*)CFPD_Form_V1::StartParse);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormParseContentSEL, (void*)CFPD_Form_V1::ParseContent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormCloneSEL, (void*)CFPD_Form_V1::Clone);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormGenerateContentSEL, (void*)CFPD_Form_V1::GenerateContent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormNewContentGeneratorSEL, (void*)CFPD_Form_V1::NewContentGenerator);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormDestroyContentGeneratorSEL, (void*)CFPD_Form_V1::DestroyContentGenerator);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormStartGenerateContentSEL, (void*)CFPD_Form_V1::StartGenerateContent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormContinueGenerateContentSEL, (void*)CFPD_Form_V1::ContinueGenerateContent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormGetResourcesDictionarySEL, (void*)CFPD_Form_V1::GetResourcesDictionary);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormSEL, FPDFormSetResourcesDictionarySEL, (void*)CFPD_Form_V1::SetResourcesDictionary);
	}
};

// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
class CFPD_Path_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathNewSEL, (void*)CFPD_Path_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathDestroySEL, (void*)CFPD_Path_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathGetPointCountSEL, (void*)CFPD_Path_V1::GetPointCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathGetFlagSEL, (void*)CFPD_Path_V1::GetFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathGetPointXSEL, (void*)CFPD_Path_V1::GetPointX);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathGetPointYSEL, (void*)CFPD_Path_V1::GetPointY);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathGetPointSEL, (void*)CFPD_Path_V1::GetPoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathGetBoundingBoxSEL, (void*)CFPD_Path_V1::GetBoundingBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathGetBoundingBox2SEL, (void*)CFPD_Path_V1::GetBoundingBox2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathTransformSEL, (void*)CFPD_Path_V1::Transform);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathAppendSEL, (void*)CFPD_Path_V1::Append);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathAppendRectSEL, (void*)CFPD_Path_V1::AppendRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathIsRectSEL, (void*)CFPD_Path_V1::IsRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathSetPointCountSEL, (void*)CFPD_Path_V1::SetPointCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathSetPointSEL, (void*)CFPD_Path_V1::SetPoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathGetModifySEL, (void*)CFPD_Path_V1::GetModify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathSEL, FPDPathTrimPointsSEL, (void*)CFPD_Path_V1::TrimPoints);
	}
};

class CFPD_ClipPath_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDClipPathSEL, FPDClipPathNewSEL, (void*)CFPD_ClipPath_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDClipPathSEL, FPDClipPathDestroySEL, (void*)CFPD_ClipPath_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDClipPathSEL, FPDClipPathAppendPathSEL, (void*)CFPD_ClipPath_V1::AppendPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDClipPathSEL, FPDClipPathDeletePathSEL, (void*)CFPD_ClipPath_V1::DeletePath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDClipPathSEL, FPDClipPathGetPathCountSEL, (void*)CFPD_ClipPath_V1::GetPathCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDClipPathSEL, FPDClipPathTransformSEL, (void*)CFPD_ClipPath_V1::Transform);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDClipPathSEL, FPDClipPathGetPathSEL, (void*)CFPD_ClipPath_V1::GetPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDClipPathSEL, FPDClipPathGetClipTypeSEL, (void*)CFPD_ClipPath_V1::GetClipType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDClipPathSEL, FPDClipPathGetTextCountSEL, (void*)CFPD_ClipPath_V1::GetTextCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDClipPathSEL, FPDClipPathGetClipBoxSEL, (void*)CFPD_ClipPath_V1::GetClipBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDClipPathSEL, FPDClipPathGetTextSEL, (void*)CFPD_ClipPath_V1::GetText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDClipPathSEL, FPDClipPathAppendTextsSEL, (void*)CFPD_ClipPath_V1::AppendTexts);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDClipPathSEL, FPDClipPathSetCountSEL, (void*)CFPD_ClipPath_V1::SetCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDClipPathSEL, FPDClipPathIsNullSEL, (void*)CFPD_ClipPath_V1::IsNull);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDClipPathSEL, FPDClipPathGetModifySEL, (void*)CFPD_ClipPath_V1::GetModify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDClipPathSEL, FPDClipPathGetPathPointerSEL, (void*)CFPD_ClipPath_V1::GetPathPointer);
	}
};

class CFPD_ColorState_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorStateSEL, FPDColorStateNewSEL, (void*)CFPD_ColorState_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorStateSEL, FPDColorStateDestroySEL, (void*)CFPD_ColorState_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorStateSEL, FPDColorStateGetFillColorSEL, (void*)CFPD_ColorState_V1::GetFillColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorStateSEL, FPDColorStateGetStrokeColorSEL, (void*)CFPD_ColorState_V1::GetStrokeColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorStateSEL, FPDColorStateSetFillColorSEL, (void*)CFPD_ColorState_V1::SetFillColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorStateSEL, FPDColorStateSetStrokeColorSEL, (void*)CFPD_ColorState_V1::SetStrokeColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorStateSEL, FPDColorStateSetFillPatternColorSEL, (void*)CFPD_ColorState_V1::SetFillPatternColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorStateSEL, FPDColorStateSetStrokePatternColorSEL, (void*)CFPD_ColorState_V1::SetStrokePatternColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorStateSEL, FPDColorStateIsNullSEL, (void*)CFPD_ColorState_V1::IsNull);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorStateSEL, FPDColorStateGetModifySEL, (void*)CFPD_ColorState_V1::GetModify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorStateSEL, FPDColorStateNotUseFillColorSEL, (void*)CFPD_ColorState_V1::NotUseFillColor);
	}
};

class CFPD_TextState_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateNewSEL, (void*)CFPD_TextState_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateDestroySEL, (void*)CFPD_TextState_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateGetFontSEL, (void*)CFPD_TextState_V1::GetFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateSetFontSEL, (void*)CFPD_TextState_V1::SetFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateGetFontSizeSEL, (void*)CFPD_TextState_V1::GetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateGetMatrixSEL, (void*)CFPD_TextState_V1::GetMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateGetFontSizeVSEL, (void*)CFPD_TextState_V1::GetFontSizeV);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateGetFontSizeHSEL, (void*)CFPD_TextState_V1::GetFontSizeH);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateGetBaselineAngleSEL, (void*)CFPD_TextState_V1::GetBaselineAngle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateGetShearAngleSEL, (void*)CFPD_TextState_V1::GetShearAngle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateSetFontSizeSEL, (void*)CFPD_TextState_V1::SetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateSetCharSpaceSEL, (void*)CFPD_TextState_V1::SetCharSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateSetWordSpaceSEL, (void*)CFPD_TextState_V1::SetWordSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateSetMatrixSEL, (void*)CFPD_TextState_V1::SetMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateSetTextModeSEL, (void*)CFPD_TextState_V1::SetTextMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateSetTextCTMSEL, (void*)CFPD_TextState_V1::SetTextCTM);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateGetTextModeSEL, (void*)CFPD_TextState_V1::GetTextMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateGetTextCTMSEL, (void*)CFPD_TextState_V1::GetTextCTM);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateGetCharSpaceSEL, (void*)CFPD_TextState_V1::GetCharSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateGetWordSpaceSEL, (void*)CFPD_TextState_V1::GetWordSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateIsNullSEL, (void*)CFPD_TextState_V1::IsNull);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextStateSEL, FPDTextStateGetModifySEL, (void*)CFPD_TextState_V1::GetModify);
	}
};

class CFPD_GeneralState_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGeneralStateSEL, FPDGeneralStateNewSEL, (void*)CFPD_GeneralState_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGeneralStateSEL, FPDGeneralStateDestroySEL, (void*)CFPD_GeneralState_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGeneralStateSEL, FPDGeneralStateSetRenderIntentSEL, (void*)CFPD_GeneralState_V1::SetRenderIntent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGeneralStateSEL, FPDGeneralStateGetBlendTypeSEL, (void*)CFPD_GeneralState_V1::GetBlendType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGeneralStateSEL, FPDGeneralStateGetAlphaSEL, (void*)CFPD_GeneralState_V1::GetAlpha);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGeneralStateSEL, FPDGeneralStateSetBlendModeSEL, (void*)CFPD_GeneralState_V1::SetBlendMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGeneralStateSEL, FPDGeneralStateSetBlendTypeSEL, (void*)CFPD_GeneralState_V1::SetBlendType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGeneralStateSEL, FPDGeneralStateSetSoftMaskSEL, (void*)CFPD_GeneralState_V1::SetSoftMask);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGeneralStateSEL, FPDGeneralStateSetSoftMaskMatrixSEL, (void*)CFPD_GeneralState_V1::SetSoftMaskMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGeneralStateSEL, FPDGeneralStateSetStrokeAlphaSEL, (void*)CFPD_GeneralState_V1::SetStrokeAlpha);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGeneralStateSEL, FPDGeneralStateSetFillAlphaSEL, (void*)CFPD_GeneralState_V1::SetFillAlpha);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGeneralStateSEL, FPDGeneralStateIsNullSEL, (void*)CFPD_GeneralState_V1::IsNull);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGeneralStateSEL, FPDGeneralStateGetModifySEL, (void*)CFPD_GeneralState_V1::GetModify);
	}
};

class CFPD_GraphState_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateNewSEL, (void*)CFPD_GraphState_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateDestroySEL, (void*)CFPD_GraphState_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateSetDashCountSEL, (void*)CFPD_GraphState_V1::SetDashCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateGetDashCountSEL, (void*)CFPD_GraphState_V1::GetDashCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateGetDashArraySEL, (void*)CFPD_GraphState_V1::GetDashArray);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateGetDashPhaseSEL, (void*)CFPD_GraphState_V1::GetDashPhase);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateSetDashPhaseSEL, (void*)CFPD_GraphState_V1::SetDashPhase);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateGetLineCapSEL, (void*)CFPD_GraphState_V1::GetLineCap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateSetLineCapSEL, (void*)CFPD_GraphState_V1::SetLineCap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateGetLineJoinSEL, (void*)CFPD_GraphState_V1::GetLineJoin);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateSetLineJoinSEL, (void*)CFPD_GraphState_V1::SetLineJoin);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateGetMiterLimitSEL, (void*)CFPD_GraphState_V1::GetMiterLimit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateSetMiterLimitSEL, (void*)CFPD_GraphState_V1::SetMiterLimit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateGetLineWidthSEL, (void*)CFPD_GraphState_V1::GetLineWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateSetLineWidthSEL, (void*)CFPD_GraphState_V1::SetLineWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateIsNullSEL, (void*)CFPD_GraphState_V1::IsNull);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateGetModifySEL, (void*)CFPD_GraphState_V1::GetModify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphStateSEL, FPDGraphStateSetDashArraySEL, (void*)CFPD_GraphState_V1::SetDashArray);
	}
};

class CFPD_PageObject_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectNewSEL, (void*)CFPD_PageObject_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectDestroySEL, (void*)CFPD_PageObject_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectCloneSEL, (void*)CFPD_PageObject_V1::Clone);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectCopySEL, (void*)CFPD_PageObject_V1::Copy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectRemoveClipPathSEL, (void*)CFPD_PageObject_V1::RemoveClipPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectAppendClipPathSEL, (void*)CFPD_PageObject_V1::AppendClipPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectCopyClipPathSEL, (void*)CFPD_PageObject_V1::CopyClipPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectTransformClipPathSEL, (void*)CFPD_PageObject_V1::TransformClipPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectSetColorStateSEL, (void*)CFPD_PageObject_V1::SetColorState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectGetBBoxSEL, (void*)CFPD_PageObject_V1::GetBBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectGetOriginalBBoxSEL, (void*)CFPD_PageObject_V1::GetOriginalBBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectGetTypeSEL, (void*)CFPD_PageObject_V1::GetType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectGetClipPathSEL, (void*)CFPD_PageObject_V1::GetClipPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectGetGraphStateSEL, (void*)CFPD_PageObject_V1::GetGraphState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectGetColorStateSEL, (void*)CFPD_PageObject_V1::GetColorState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectGetTextStateSEL, (void*)CFPD_PageObject_V1::GetTextState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectGetGeneralStateSEL, (void*)CFPD_PageObject_V1::GetGeneralState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectGetContentMarkSEL, (void*)CFPD_PageObject_V1::GetContentMark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectDefaultStatesSEL, (void*)CFPD_PageObject_V1::DefaultStates);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectCopyStatesSEL, (void*)CFPD_PageObject_V1::CopyStates);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectSetGraphStateSEL, (void*)CFPD_PageObject_V1::SetGraphState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectSetTextStateSEL, (void*)CFPD_PageObject_V1::SetTextState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectSetGeneralStateSEL, (void*)CFPD_PageObject_V1::SetGeneralState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectHasClipPathSEL, (void*)CFPD_PageObject_V1::HasClipPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageObjectSEL, FPDPageObjectGetContentMark2SEL, (void*)CFPD_PageObject_V1::GetContentMark2);
	}
};

class CFPD_TextObject_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectNewSEL, (void*)CFPD_TextObject_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectDestroySEL, (void*)CFPD_TextObject_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectCountItemsSEL, (void*)CFPD_TextObject_V1::CountItems);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectGetItemInfoSEL, (void*)CFPD_TextObject_V1::GetItemInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectCountCharsSEL, (void*)CFPD_TextObject_V1::CountChars);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectGetCharInfoSEL, (void*)CFPD_TextObject_V1::GetCharInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectGetPosXSEL, (void*)CFPD_TextObject_V1::GetPosX);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectGetPosYSEL, (void*)CFPD_TextObject_V1::GetPosY);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectGetTextMatrixSEL, (void*)CFPD_TextObject_V1::GetTextMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectGetFontSEL, (void*)CFPD_TextObject_V1::GetFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectGetFontSizeSEL, (void*)CFPD_TextObject_V1::GetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectSetEmptySEL, (void*)CFPD_TextObject_V1::SetEmpty);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectSetTextSEL, (void*)CFPD_TextObject_V1::SetText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectSetText2SEL, (void*)CFPD_TextObject_V1::SetText2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectSetText3SEL, (void*)CFPD_TextObject_V1::SetText3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectSetPositionSEL, (void*)CFPD_TextObject_V1::SetPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectSetTextStateSEL, (void*)CFPD_TextObject_V1::SetTextState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectTransformSEL, (void*)CFPD_TextObject_V1::Transform);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectCalcCharPosSEL, (void*)CFPD_TextObject_V1::CalcCharPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectSetDataSEL, (void*)CFPD_TextObject_V1::SetData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectGetDataSEL, (void*)CFPD_TextObject_V1::GetData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectSEL, FPDTextObjectRecalcPositionDataSEL, (void*)CFPD_TextObject_V1::RecalcPositionData);
	}
};

class CFPD_PathObject_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectSEL, FPDPathObjectNewSEL, (void*)CFPD_PathObject_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectSEL, FPDPathObjectDestroySEL, (void*)CFPD_PathObject_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectSEL, FPDPathObjectTransformSEL, (void*)CFPD_PathObject_V1::Transform);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectSEL, FPDPathObjectSetGraphStateSEL, (void*)CFPD_PathObject_V1::SetGraphState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectSEL, FPDPathObjectCalcBoundingBoxSEL, (void*)CFPD_PathObject_V1::CalcBoundingBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectSEL, FPDPathObjectGetTransformMatrixSEL, (void*)CFPD_PathObject_V1::GetTransformMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectSEL, FPDPathObjectSetTransformMatrixSEL, (void*)CFPD_PathObject_V1::SetTransformMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectSEL, FPDPathObjectGetPathSEL, (void*)CFPD_PathObject_V1::GetPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectSEL, FPDPathObjectIsStrokeModeSEL, (void*)CFPD_PathObject_V1::IsStrokeMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectSEL, FPDPathObjectSetStrokeModeSEL, (void*)CFPD_PathObject_V1::SetStrokeMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectSEL, FPDPathObjectGetFillModeSEL, (void*)CFPD_PathObject_V1::GetFillMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectSEL, FPDPathObjectSetFillModeSEL, (void*)CFPD_PathObject_V1::SetFillMode);
	}
};

class CFPD_ImageObject_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageObjectSEL, FPDImageObjectNewSEL, (void*)CFPD_ImageObject_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageObjectSEL, FPDImageObjectDestroySEL, (void*)CFPD_ImageObject_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageObjectSEL, FPDImageObjectTransformSEL, (void*)CFPD_ImageObject_V1::Transform);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageObjectSEL, FPDImageObjectCalcBoundingBoxSEL, (void*)CFPD_ImageObject_V1::CalcBoundingBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageObjectSEL, FPDImageObjectGetTransformMatrixSEL, (void*)CFPD_ImageObject_V1::GetTransformMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageObjectSEL, FPDImageObjectSetTransformMatrixSEL, (void*)CFPD_ImageObject_V1::SetTransformMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageObjectSEL, FPDImageObjectGetImageSEL, (void*)CFPD_ImageObject_V1::GetImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageObjectSEL, FPDImageObjectSetImageSEL, (void*)CFPD_ImageObject_V1::SetImage);
	}
};

class CFPD_ShadingObject_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectSEL, FPDShadingObjectNewSEL, (void*)CFPD_ShadingObject_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectSEL, FPDShadingObjectDestroySEL, (void*)CFPD_ShadingObject_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectSEL, FPDShadingObjectTransformSEL, (void*)CFPD_ShadingObject_V1::Transform);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectSEL, FPDShadingObjectCalcBoundingBoxSEL, (void*)CFPD_ShadingObject_V1::CalcBoundingBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectSEL, FPDShadingObjectGetTransformMatrixSEL, (void*)CFPD_ShadingObject_V1::GetTransformMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectSEL, FPDShadingObjectSetTransformMatrixSEL, (void*)CFPD_ShadingObject_V1::SetTransformMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectSEL, FPDShadingObjectGetPageSEL, (void*)CFPD_ShadingObject_V1::GetPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectSEL, FPDShadingObjectSetPageSEL, (void*)CFPD_ShadingObject_V1::SetPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectSEL, FPDShadingObjectGetShadingPatternSEL, (void*)CFPD_ShadingObject_V1::GetShadingPattern);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectSEL, FPDShadingObjectSetShadingPatternSEL, (void*)CFPD_ShadingObject_V1::SetShadingPattern);
	}
};

class CFPD_FormObject_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormObjectSEL, FPDFormObjectNewSEL, (void*)CFPD_FormObject_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormObjectSEL, FPDFormObjectDestroySEL, (void*)CFPD_FormObject_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormObjectSEL, FPDFormObjectTransformSEL, (void*)CFPD_FormObject_V1::Transform);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormObjectSEL, FPDFormObjectCalcBoundingBoxSEL, (void*)CFPD_FormObject_V1::CalcBoundingBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormObjectSEL, FPDFormObjectGetTransformMatrixSEL, (void*)CFPD_FormObject_V1::GetTransformMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormObjectSEL, FPDFormObjectSetTransformMatrixSEL, (void*)CFPD_FormObject_V1::SetTransformMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormObjectSEL, FPDFormObjectGetFormSEL, (void*)CFPD_FormObject_V1::GetForm);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFormObjectSEL, FPDFormObjectSetFormSEL, (void*)CFPD_FormObject_V1::SetForm);
	}
};

class CFPD_InlineImages_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInlineImagesSEL, FPDInlineImagesNewSEL, (void*)CFPD_InlineImages_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInlineImagesSEL, FPDInlineImagesDestroySEL, (void*)CFPD_InlineImages_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInlineImagesSEL, FPDInlineImagesAddMatrixSEL, (void*)CFPD_InlineImages_V1::AddMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInlineImagesSEL, FPDInlineImagesCountMatrixSEL, (void*)CFPD_InlineImages_V1::CountMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInlineImagesSEL, FPDInlineImagesGetMatrixSEL, (void*)CFPD_InlineImages_V1::GetMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInlineImagesSEL, FPDInlineImagesGetStreamSEL, (void*)CFPD_InlineImages_V1::GetStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDInlineImagesSEL, FPDInlineImagesSetStreamSEL, (void*)CFPD_InlineImages_V1::SetStream);
	}
};

class CFPD_ContentMarkItem_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkItemSEL, FPDContentMarkItemNewSEL, (void*)CFPD_ContentMarkItem_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkItemSEL, FPDContentMarkItemDestroySEL, (void*)CFPD_ContentMarkItem_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkItemSEL, FPDContentMarkItemGetNameSEL, (void*)CFPD_ContentMarkItem_V1::GetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkItemSEL, FPDContentMarkItemGetParamTypeSEL, (void*)CFPD_ContentMarkItem_V1::GetParamType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkItemSEL, FPDContentMarkItemGetParamSEL, (void*)CFPD_ContentMarkItem_V1::GetParam);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkItemSEL, FPDContentMarkItemSetNameSEL, (void*)CFPD_ContentMarkItem_V1::SetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkItemSEL, FPDContentMarkItemSetParamSEL, (void*)CFPD_ContentMarkItem_V1::SetParam);
	}
};

class CFPD_ContentMark_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkSEL, FPDContentMarkNewSEL, (void*)CFPD_ContentMark_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkSEL, FPDContentMarkDestroySEL, (void*)CFPD_ContentMark_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkSEL, FPDContentMarkGetMCIDSEL, (void*)CFPD_ContentMark_V1::GetMCID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkSEL, FPDContentMarkHasMarkSEL, (void*)CFPD_ContentMark_V1::HasMark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkSEL, FPDContentMarkLookupMarkSEL, (void*)CFPD_ContentMark_V1::LookupMark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkSEL, FPDContentMarkCountItemsSEL, (void*)CFPD_ContentMark_V1::CountItems);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkSEL, FPDContentMarkGetItemSEL, (void*)CFPD_ContentMark_V1::GetItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkSEL, FPDContentMarkAddMarkSEL, (void*)CFPD_ContentMark_V1::AddMark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkSEL, FPDContentMarkDeleteMarkSEL, (void*)CFPD_ContentMark_V1::DeleteMark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkSEL, FPDContentMarkDeleteLastMarkSEL, (void*)CFPD_ContentMark_V1::DeleteLastMark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkSEL, FPDContentMarkIsNullSEL, (void*)CFPD_ContentMark_V1::IsNull);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDContentMarkSEL, FPDContentMarkCopySEL, (void*)CFPD_ContentMark_V1::Copy);
	}
};

// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
class CFPD_Parser_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserNewSEL, (void*)CFPD_Parser_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserDestroySEL, (void*)CFPD_Parser_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserStartParseSEL, (void*)CFPD_Parser_V1::StartParse);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserStartParseWSEL, (void*)CFPD_Parser_V1::StartParseW);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserStartParseFormMemSEL, (void*)CFPD_Parser_V1::StartParseFormMem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserStartParseCustomFileSEL, (void*)CFPD_Parser_V1::StartParseCustomFile);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserCloseParserSEL, (void*)CFPD_Parser_V1::CloseParser);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetPermissionsSEL, (void*)CFPD_Parser_V1::GetPermissions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserIsOwnerSEL, (void*)CFPD_Parser_V1::IsOwner);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserSetPasswordSEL, (void*)CFPD_Parser_V1::SetPassword);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetPasswordSEL, (void*)CFPD_Parser_V1::GetPassword);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetTrailerSEL, (void*)CFPD_Parser_V1::GetTrailer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetLastXRefOffsetSEL, (void*)CFPD_Parser_V1::GetLastXRefOffset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetDocumentSEL, (void*)CFPD_Parser_V1::GetDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserCountOtherTrailersSEL, (void*)CFPD_Parser_V1::CountOtherTrailers);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetOtherTrailerByIndexSEL, (void*)CFPD_Parser_V1::GetOtherTrailerByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetRootObjNumSEL, (void*)CFPD_Parser_V1::GetRootObjNum);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetInfoObjNumSEL, (void*)CFPD_Parser_V1::GetInfoObjNum);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetIDArraySEL, (void*)CFPD_Parser_V1::GetIDArray);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetEncryptDictSEL, (void*)CFPD_Parser_V1::GetEncryptDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserParseIndirectObjectSEL, (void*)CFPD_Parser_V1::ParseIndirectObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetLastObjNumSEL, (void*)CFPD_Parser_V1::GetLastObjNum);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserIsFormStreamSEL, (void*)CFPD_Parser_V1::IsFormStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserReloadFileStreamSEL, (void*)CFPD_Parser_V1::ReloadFileStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserDeleteIndirectObjectSEL, (void*)CFPD_Parser_V1::DeleteIndirectObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetLengthOfVersionsSEL, (void*)CFPD_Parser_V1::GetLengthOfVersions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetObjectOffsetSEL, (void*)CFPD_Parser_V1::GetObjectOffset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetObjectSizeSEL, (void*)CFPD_Parser_V1::GetObjectSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetObjectVersionSEL, (void*)CFPD_Parser_V1::GetObjectVersion);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetIndirectBinarySEL, (void*)CFPD_Parser_V1::GetIndirectBinary);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserParseStreamPosSEL, (void*)CFPD_Parser_V1::ParseStreamPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserParseIndirectObjectsAtRangeSEL, (void*)CFPD_Parser_V1::ParseIndirectObjectsAtRange);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetFileStreamOptionSEL, (void*)CFPD_Parser_V1::GetFileStreamOption);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserSetFileStreamOptionSEL, (void*)CFPD_Parser_V1::SetFileStreamOption);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetFileAccessSEL, (void*)CFPD_Parser_V1::GetFileAccess);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetFileVersionSEL, (void*)CFPD_Parser_V1::GetFileVersion);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserIsXRefStreamSEL, (void*)CFPD_Parser_V1::IsXRefStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserParseIndirectObjectAtSEL, (void*)CFPD_Parser_V1::ParseIndirectObjectAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserStartAsynParseSEL, (void*)CFPD_Parser_V1::StartAsynParse);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetFirstPageNoSEL, (void*)CFPD_Parser_V1::GetFirstPageNo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetSecurityPermissionsSEL, (void*)CFPD_Parser_V1::GetSecurityPermissions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserIsSecurityOwnerSEL, (void*)CFPD_Parser_V1::IsSecurityOwner);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetSecurityCryptInfoSEL, (void*)CFPD_Parser_V1::GetSecurityCryptInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserIsMetadataEncryptedBySecuritySEL, (void*)CFPD_Parser_V1::IsMetadataEncryptedBySecurity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetStandardSecurityUserPasswordSEL, (void*)CFPD_Parser_V1::GetStandardSecurityUserPassword);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetStandardSecurityVersionSEL, (void*)CFPD_Parser_V1::GetStandardSecurityVersion);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetStandardSecurityRevisionSEL, (void*)CFPD_Parser_V1::GetStandardSecurityRevision);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserCheckStandardSecurityPasswordSEL, (void*)CFPD_Parser_V1::CheckStandardSecurityPassword);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserCheckEmbeddedSecuritySEL, (void*)CFPD_Parser_V1::CheckEmbeddedSecurity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserLoadAttachmentStreamSEL, (void*)CFPD_Parser_V1::LoadAttachmentStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserSetUnicodePasswordSEL, (void*)CFPD_Parser_V1::SetUnicodePassword);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserGetUnicodePasswordSEL, (void*)CFPD_Parser_V1::GetUnicodePassword);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDParserSEL, FPDParserCheckPasswordSEL, (void*)CFPD_Parser_V1::CheckPassword);
	}
};

// fpd_parserImpl.h end

// In file fpd_renderImpl.h
class CFPD_RenderOptions_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderOptionsSEL, FPDRenderOptionsNewSEL, (void*)CFPD_RenderOptions_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderOptionsSEL, FPDRenderOptionsDestroySEL, (void*)CFPD_RenderOptions_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderOptionsSEL, FPDRenderOptionsGetColorModeSEL, (void*)CFPD_RenderOptions_V1::GetColorMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderOptionsSEL, FPDRenderOptionsSetColorModeSEL, (void*)CFPD_RenderOptions_V1::SetColorMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderOptionsSEL, FPDRenderOptionsGetBackColorSEL, (void*)CFPD_RenderOptions_V1::GetBackColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderOptionsSEL, FPDRenderOptionsSetBackColorSEL, (void*)CFPD_RenderOptions_V1::SetBackColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderOptionsSEL, FPDRenderOptionsGetForeColorSEL, (void*)CFPD_RenderOptions_V1::GetForeColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderOptionsSEL, FPDRenderOptionsSetForeColorSEL, (void*)CFPD_RenderOptions_V1::SetForeColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderOptionsSEL, FPDRenderOptionsGetRenderFlagSEL, (void*)CFPD_RenderOptions_V1::GetRenderFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderOptionsSEL, FPDRenderOptionsSetRenderFlagSEL, (void*)CFPD_RenderOptions_V1::SetRenderFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderOptionsSEL, FPDRenderOptionsGetAddtionalFlagSEL, (void*)CFPD_RenderOptions_V1::GetAddtionalFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderOptionsSEL, FPDRenderOptionsSetAddtionalFlagSEL, (void*)CFPD_RenderOptions_V1::SetAddtionalFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderOptionsSEL, FPDRenderOptionsSetOCCHandlerSEL, (void*)CFPD_RenderOptions_V1::SetOCCHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderOptionsSEL, FPDRenderOptionsTranslateColorSEL, (void*)CFPD_RenderOptions_V1::TranslateColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderOptionsSEL, FPDRenderOptionsCreateOCContextHandlerSEL, (void*)CFPD_RenderOptions_V1::CreateOCContextHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderOptionsSEL, FPDRenderOptionsDeleteOCContextHandlerSEL, (void*)CFPD_RenderOptions_V1::DeleteOCContextHandler);
	}
};

class CFPD_RenderContext_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderContextSEL, FPDRenderContextNewSEL, (void*)CFPD_RenderContext_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderContextSEL, FPDRenderContextNew2SEL, (void*)CFPD_RenderContext_V1::New2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderContextSEL, FPDRenderContextDestroySEL, (void*)CFPD_RenderContext_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderContextSEL, FPDRenderContextDrawStreamSEL, (void*)CFPD_RenderContext_V1::DrawStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderContextSEL, FPDRenderContextAppendPageSEL, (void*)CFPD_RenderContext_V1::AppendPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderContextSEL, FPDRenderContextAppendFormSEL, (void*)CFPD_RenderContext_V1::AppendForm);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderContextSEL, FPDRenderContextSetBackgroundSEL, (void*)CFPD_RenderContext_V1::SetBackground);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderContextSEL, FPDRenderContextCreateBackgroundDrawHandlerSEL, (void*)CFPD_RenderContext_V1::CreateBackgroundDrawHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderContextSEL, FPDRenderContextDeleteBackgroundDrawHandlerSEL, (void*)CFPD_RenderContext_V1::DeleteBackgroundDrawHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderContextSEL, FPDRenderContextRenderSEL, (void*)CFPD_RenderContext_V1::Render);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderContextSEL, FPDRenderContextDrawPageSEL, (void*)CFPD_RenderContext_V1::DrawPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderContextSEL, FPDRenderContextDrawFormSEL, (void*)CFPD_RenderContext_V1::DrawForm);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderContextSEL, FPDRenderContextQuickDrawSEL, (void*)CFPD_RenderContext_V1::QuickDraw);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderContextSEL, FPDRenderContextGetBackgroundSEL, (void*)CFPD_RenderContext_V1::GetBackground);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderContextSEL, FPDRenderContextGetPageCacheSEL, (void*)CFPD_RenderContext_V1::GetPageCache);
	}
};

class CFPD_ProgressiveRender_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDProgressiveRenderSEL, FPDProgressiveRenderNewSEL, (void*)CFPD_ProgressiveRender_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDProgressiveRenderSEL, FPDProgressiveRenderDestroySEL, (void*)CFPD_ProgressiveRender_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDProgressiveRenderSEL, FPDProgressiveRenderStartSEL, (void*)CFPD_ProgressiveRender_V1::Start);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDProgressiveRenderSEL, FPDProgressiveRenderContinueSEL, (void*)CFPD_ProgressiveRender_V1::Continue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDProgressiveRenderSEL, FPDProgressiveRenderEstimateProgressSEL, (void*)CFPD_ProgressiveRender_V1::EstimateProgress);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDProgressiveRenderSEL, FPDProgressiveRenderClearSEL, (void*)CFPD_ProgressiveRender_V1::Clear);
	}
};

class CFPD_RenderDevice_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceNewSEL, (void*)CFPD_RenderDevice_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceDestroySEL, (void*)CFPD_RenderDevice_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceStartRenderingSEL, (void*)CFPD_RenderDevice_V1::StartRendering);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceEndRenderingSEL, (void*)CFPD_RenderDevice_V1::EndRendering);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceSaveStateSEL, (void*)CFPD_RenderDevice_V1::SaveState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceRestoreStateSEL, (void*)CFPD_RenderDevice_V1::RestoreState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceGetWidthSEL, (void*)CFPD_RenderDevice_V1::GetWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceGetHeightSEL, (void*)CFPD_RenderDevice_V1::GetHeight);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceGetBPPSEL, (void*)CFPD_RenderDevice_V1::GetBPP);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceGetRenderCapsSEL, (void*)CFPD_RenderDevice_V1::GetRenderCaps);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceGetDeviceCapsXYSEL, (void*)CFPD_RenderDevice_V1::GetDeviceCapsXY);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceGetBitmapSEL, (void*)CFPD_RenderDevice_V1::GetBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceSetBitmapSEL, (void*)CFPD_RenderDevice_V1::SetBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceSetPixelSEL, (void*)CFPD_RenderDevice_V1::SetPixel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceSetClip_RectSEL, (void*)CFPD_RenderDevice_V1::SetClip_Rect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceCreateCompatibleBitmapSEL, (void*)CFPD_RenderDevice_V1::CreateCompatibleBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceFillRectSEL, (void*)CFPD_RenderDevice_V1::FillRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceDrawCosmeticLineSEL, (void*)CFPD_RenderDevice_V1::DrawCosmeticLine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceGetDIBitsSEL, (void*)CFPD_RenderDevice_V1::GetDIBits);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceSetDIBitsSEL, (void*)CFPD_RenderDevice_V1::SetDIBits);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceStretchDIBitsSEL, (void*)CFPD_RenderDevice_V1::StretchDIBits);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceSetBitMaskSEL, (void*)CFPD_RenderDevice_V1::SetBitMask);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceStretchBitMaskSEL, (void*)CFPD_RenderDevice_V1::StretchBitMask);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceGetDitherBitsSEL, (void*)CFPD_RenderDevice_V1::GetDitherBits);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceDrawTextStringSEL, (void*)CFPD_RenderDevice_V1::DrawTextString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceDrawTextString2SEL, (void*)CFPD_RenderDevice_V1::DrawTextString2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceDrawTextPathSEL, (void*)CFPD_RenderDevice_V1::DrawTextPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceDrawNormalTextSEL, (void*)CFPD_RenderDevice_V1::DrawNormalText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceDrawType3TextSEL, (void*)CFPD_RenderDevice_V1::DrawType3Text);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceDrawPathSEL, (void*)CFPD_RenderDevice_V1::DrawPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceSetClip_PathFillSEL, (void*)CFPD_RenderDevice_V1::SetClip_PathFill);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDRenderDeviceSEL, FPDRenderDeviceSetClip_PathFill2SEL, (void*)CFPD_RenderDevice_V1::SetClip_PathFill2);
	}
};

class CFPD_FxgeDevice_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFxgeDeviceSEL, FPDFxgeDeviceNewSEL, (void*)CFPD_FxgeDevice_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFxgeDeviceSEL, FPDFxgeDeviceCreateSEL, (void*)CFPD_FxgeDevice_V1::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFxgeDeviceSEL, FPDFxgeDeviceDestroySEL, (void*)CFPD_FxgeDevice_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFxgeDeviceSEL, FPDFxgeDeviceAttachSEL, (void*)CFPD_FxgeDevice_V1::Attach);
	}
};

class CFPD_WindowsDevice_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDWindowsDeviceSEL, FPDWindowsDeviceNewSEL, (void*)CFPD_WindowsDevice_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDWindowsDeviceSEL, FPDWindowsDeviceDestroySEL, (void*)CFPD_WindowsDevice_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDWindowsDeviceSEL, FPDWindowsDeviceGetDCSEL, (void*)CFPD_WindowsDevice_V1::GetDC);
	}
};

class CFPD_PageRenderCache_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageRenderCacheSEL, FPDPageRenderCacheNewSEL, (void*)CFPD_PageRenderCache_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageRenderCacheSEL, FPDPageRenderCacheDestroySEL, (void*)CFPD_PageRenderCache_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageRenderCacheSEL, FPDPageRenderCacheClearAllSEL, (void*)CFPD_PageRenderCache_V1::ClearAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageRenderCacheSEL, FPDPageRenderCacheClearImageDataSEL, (void*)CFPD_PageRenderCache_V1::ClearImageData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageRenderCacheSEL, FPDPageRenderCacheEstimateSizeSEL, (void*)CFPD_PageRenderCache_V1::EstimateSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageRenderCacheSEL, FPDPageRenderCacheGetCachedBitmapSEL, (void*)CFPD_PageRenderCache_V1::GetCachedBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageRenderCacheSEL, FPDPageRenderCacheResetBitmapSEL, (void*)CFPD_PageRenderCache_V1::ResetBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageRenderCacheSEL, FPDPageRenderCacheCalcBitmapMarginSEL, (void*)CFPD_PageRenderCache_V1::CalcBitmapMargin);
	}
};

// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
class CFPD_Font_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontNewSEL, (void*)CFPD_Font_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontDestroySEL, (void*)CFPD_Font_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetStockFontSEL, (void*)CFPD_Font_V1::GetStockFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetFontTypeSEL, (void*)CFPD_Font_V1::GetFontType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetFontTypeNameSEL, (void*)CFPD_Font_V1::GetFontTypeName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetBaseFontSEL, (void*)CFPD_Font_V1::GetBaseFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetSubstFontSEL, (void*)CFPD_Font_V1::GetSubstFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetFlagsSEL, (void*)CFPD_Font_V1::GetFlags);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetType1FontSEL, (void*)CFPD_Font_V1::GetType1Font);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetTrueTypeFontSEL, (void*)CFPD_Font_V1::GetTrueTypeFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetCIDFontSEL, (void*)CFPD_Font_V1::GetCIDFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetType3FontSEL, (void*)CFPD_Font_V1::GetType3Font);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontIsEmbeddedSEL, (void*)CFPD_Font_V1::IsEmbedded);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontIsUnicodeCompatibleSEL, (void*)CFPD_Font_V1::IsUnicodeCompatible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetFontFileSEL, (void*)CFPD_Font_V1::GetFontFile);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetFontDictSEL, (void*)CFPD_Font_V1::GetFontDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontIsStandardFontSEL, (void*)CFPD_Font_V1::IsStandardFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetFaceSEL, (void*)CFPD_Font_V1::GetFace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetNextCharSEL, (void*)CFPD_Font_V1::GetNextChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontCountCharSEL, (void*)CFPD_Font_V1::CountChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontAppendCharSEL, (void*)CFPD_Font_V1::AppendChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontAppendChar2SEL, (void*)CFPD_Font_V1::AppendChar2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetCharSizeSEL, (void*)CFPD_Font_V1::GetCharSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGlyphFromCharCodeSEL, (void*)CFPD_Font_V1::GlyphFromCharCode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontUnicodeFromCharCodeSEL, (void*)CFPD_Font_V1::UnicodeFromCharCode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontCharCodeFromUnicodeSEL, (void*)CFPD_Font_V1::CharCodeFromUnicode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontIsCharEmbeddedSEL, (void*)CFPD_Font_V1::IsCharEmbedded);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetCharMapSEL, (void*)CFPD_Font_V1::GetCharMap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontEncodeStringSEL, (void*)CFPD_Font_V1::EncodeString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontDecodeStringSEL, (void*)CFPD_Font_V1::DecodeString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontRecognizeCharSEL, (void*)CFPD_Font_V1::RecognizeChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetFontBBoxSEL, (void*)CFPD_Font_V1::GetFontBBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetTypeAscentSEL, (void*)CFPD_Font_V1::GetTypeAscent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetTypeDescentSEL, (void*)CFPD_Font_V1::GetTypeDescent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetItalicAngleSEL, (void*)CFPD_Font_V1::GetItalicAngle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetStringWidthSEL, (void*)CFPD_Font_V1::GetStringWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetCharWidthFSEL, (void*)CFPD_Font_V1::GetCharWidthF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetCharTypeWidthSEL, (void*)CFPD_Font_V1::GetCharTypeWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetCharBBoxSEL, (void*)CFPD_Font_V1::GetCharBBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetPDFDocSEL, (void*)CFPD_Font_V1::GetPDFDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontLoadGlyphPathSEL, (void*)CFPD_Font_V1::LoadGlyphPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetSubstFontCharsetSEL, (void*)CFPD_Font_V1::GetSubstFontCharset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontIsVertWritingSEL, (void*)CFPD_Font_V1::IsVertWriting);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetFXFontSEL, (void*)CFPD_Font_V1::GetFXFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontFXFontGetFaceNameSEL, (void*)CFPD_Font_V1::FXFontGetFaceName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontFXFontGetPsNameSEL, (void*)CFPD_Font_V1::FXFontGetPsName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontFXFontGetFamilyNameSEL, (void*)CFPD_Font_V1::FXFontGetFamilyName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontFXFontIsItalicSEL, (void*)CFPD_Font_V1::FXFontIsItalic);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontFXFontIsBoldSEL, (void*)CFPD_Font_V1::FXFontIsBold);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontUnicodeFromCharCodeExSEL, (void*)CFPD_Font_V1::UnicodeFromCharCodeEx);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetSubstFontWeightSEL, (void*)CFPD_Font_V1::GetSubstFontWeight);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontFXFontLoadSubstSEL, (void*)CFPD_Font_V1::FXFontLoadSubst);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontSEL, FPDFontGetUnicodeBaseFontSEL, (void*)CFPD_Font_V1::GetUnicodeBaseFont);
	}
};

class CFPD_FontEncoding_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontEncodingSEL, FPDFontEncodingNewSEL, (void*)CFPD_FontEncoding_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontEncodingSEL, FPDFontEncodingNew2SEL, (void*)CFPD_FontEncoding_V1::New2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontEncodingSEL, FPDFontEncodingDestroySEL, (void*)CFPD_FontEncoding_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontEncodingSEL, FPDFontEncodingLoadEncodingSEL, (void*)CFPD_FontEncoding_V1::LoadEncoding);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontEncodingSEL, FPDFontEncodingIsIdenticalSEL, (void*)CFPD_FontEncoding_V1::IsIdentical);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontEncodingSEL, FPDFontEncodingUnicodeFromCharCodeSEL, (void*)CFPD_FontEncoding_V1::UnicodeFromCharCode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontEncodingSEL, FPDFontEncodingCharCodeFromUnicodeSEL, (void*)CFPD_FontEncoding_V1::CharCodeFromUnicode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontEncodingSEL, FPDFontEncodingSetUnicodeSEL, (void*)CFPD_FontEncoding_V1::SetUnicode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFontEncodingSEL, FPDFontEncodingRealizeSEL, (void*)CFPD_FontEncoding_V1::Realize);
	}
};

class CFPD_Type1Font_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType1FontSEL, FPDType1FontNewSEL, (void*)CFPD_Type1Font_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType1FontSEL, FPDType1FontDestroySEL, (void*)CFPD_Type1Font_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType1FontSEL, FPDType1FontGetEncodingSEL, (void*)CFPD_Type1Font_V1::GetEncoding);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType1FontSEL, FPDType1FontGetCharWidthFSEL, (void*)CFPD_Type1Font_V1::GetCharWidthF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType1FontSEL, FPDType1FontGetCharBBoxSEL, (void*)CFPD_Type1Font_V1::GetCharBBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType1FontSEL, FPDType1FontGlyphFromCharCodeSEL, (void*)CFPD_Type1Font_V1::GlyphFromCharCode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType1FontSEL, FPDType1FontIsUnicodeCompatibleSEL, (void*)CFPD_Type1Font_V1::IsUnicodeCompatible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType1FontSEL, FPDType1FontGetBase14FontSEL, (void*)CFPD_Type1Font_V1::GetBase14Font);
	}
};

class CFPD_TrueTypeFont_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTrueTypeFontSEL, FPDTrueTypeFontNewSEL, (void*)CFPD_TrueTypeFont_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTrueTypeFontSEL, FPDTrueTypeFontDestroySEL, (void*)CFPD_TrueTypeFont_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTrueTypeFontSEL, FPDTrueTypeFontGetEncodingSEL, (void*)CFPD_TrueTypeFont_V1::GetEncoding);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTrueTypeFontSEL, FPDTrueTypeFontGetCharWidthFSEL, (void*)CFPD_TrueTypeFont_V1::GetCharWidthF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTrueTypeFontSEL, FPDTrueTypeFontGetCharBBoxSEL, (void*)CFPD_TrueTypeFont_V1::GetCharBBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTrueTypeFontSEL, FPDTrueTypeFontGlyphFromCharCodeSEL, (void*)CFPD_TrueTypeFont_V1::GlyphFromCharCode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTrueTypeFontSEL, FPDTrueTypeFontIsUnicodeCompatibleSEL, (void*)CFPD_TrueTypeFont_V1::IsUnicodeCompatible);
	}
};

class CFPD_Type3Char_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3CharSEL, FPDType3CharNewSEL, (void*)CFPD_Type3Char_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3CharSEL, FPDType3CharDestroySEL, (void*)CFPD_Type3Char_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3CharSEL, FPDType3CharIsColoredSEL, (void*)CFPD_Type3Char_V1::IsColored);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3CharSEL, FPDType3CharIsPageRequiredSEL, (void*)CFPD_Type3Char_V1::IsPageRequired);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3CharSEL, FPDType3CharGetFormSEL, (void*)CFPD_Type3Char_V1::GetForm);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3CharSEL, FPDType3CharGetImageMatrixSEL, (void*)CFPD_Type3Char_V1::GetImageMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3CharSEL, FPDType3CharGetDIBitmapSEL, (void*)CFPD_Type3Char_V1::GetDIBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3CharSEL, FPDType3CharGetWidthSEL, (void*)CFPD_Type3Char_V1::GetWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3CharSEL, FPDType3CharGetBBoxSEL, (void*)CFPD_Type3Char_V1::GetBBox);
	}
};

class CFPD_Type3Font_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3FontSEL, FPDType3FontNewSEL, (void*)CFPD_Type3Font_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3FontSEL, FPDType3FontDestroySEL, (void*)CFPD_Type3Font_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3FontSEL, FPDType3FontGetEncodingSEL, (void*)CFPD_Type3Font_V1::GetEncoding);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3FontSEL, FPDType3FontGetCharWidthFSEL, (void*)CFPD_Type3Font_V1::GetCharWidthF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3FontSEL, FPDType3FontGetCharBBoxSEL, (void*)CFPD_Type3Font_V1::GetCharBBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3FontSEL, FPDType3FontGlyphFromCharCodeSEL, (void*)CFPD_Type3Font_V1::GlyphFromCharCode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3FontSEL, FPDType3FontIsUnicodeCompatibleSEL, (void*)CFPD_Type3Font_V1::IsUnicodeCompatible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3FontSEL, FPDType3FontSetPageResourcesSEL, (void*)CFPD_Type3Font_V1::SetPageResources);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3FontSEL, FPDType3FontLoadCharSEL, (void*)CFPD_Type3Font_V1::LoadChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3FontSEL, FPDType3FontGetCharTypeWidthSEL, (void*)CFPD_Type3Font_V1::GetCharTypeWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDType3FontSEL, FPDType3FontGetFontMatrixSEL, (void*)CFPD_Type3Font_V1::GetFontMatrix);
	}
};

class CFPD_CIDFont_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontNewSEL, (void*)CFPD_CIDFont_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontDestroySEL, (void*)CFPD_CIDFont_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontGetCharWidthFSEL, (void*)CFPD_CIDFont_V1::GetCharWidthF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontGetCharBBoxSEL, (void*)CFPD_CIDFont_V1::GetCharBBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontGlyphFromCharCodeSEL, (void*)CFPD_CIDFont_V1::GlyphFromCharCode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontIsUnicodeCompatibleSEL, (void*)CFPD_CIDFont_V1::IsUnicodeCompatible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontGetNextCharSEL, (void*)CFPD_CIDFont_V1::GetNextChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontCountCharSEL, (void*)CFPD_CIDFont_V1::CountChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontAppendCharSEL, (void*)CFPD_CIDFont_V1::AppendChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontGetCharSizeSEL, (void*)CFPD_CIDFont_V1::GetCharSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontLoadGB2312SEL, (void*)CFPD_CIDFont_V1::LoadGB2312);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontCIDFromCharCodeSEL, (void*)CFPD_CIDFont_V1::CIDFromCharCode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontIsTrueTypeSEL, (void*)CFPD_CIDFont_V1::IsTrueType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontGetCharsetSEL, (void*)CFPD_CIDFont_V1::GetCharset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontGetCIDTransformSEL, (void*)CFPD_CIDFont_V1::GetCIDTransform);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontIsVertWritingSEL, (void*)CFPD_CIDFont_V1::IsVertWriting);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontGetVertWidthSEL, (void*)CFPD_CIDFont_V1::GetVertWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDFontSEL, FPDCIDFontGetVertOriginSEL, (void*)CFPD_CIDFont_V1::GetVertOrigin);
	}
};

class CFPD_CIDUtil_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCIDUtilSEL, FPDCIDUtilIsVerticalJapanCIDSEL, (void*)CFPD_CIDUtil_V1::IsVerticalJapanCID);
	}
};

class CFPD_ColorSpace_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceGetStockCSSEL, (void*)CFPD_ColorSpace_V1::GetStockCS);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceLoadSEL, (void*)CFPD_ColorSpace_V1::Load);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceReleaseCSSEL, (void*)CFPD_ColorSpace_V1::ReleaseCS);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceGetBufSizeSEL, (void*)CFPD_ColorSpace_V1::GetBufSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceCreateBufSEL, (void*)CFPD_ColorSpace_V1::CreateBuf);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceGetDefaultColorSEL, (void*)CFPD_ColorSpace_V1::GetDefaultColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceCountComponentsSEL, (void*)CFPD_ColorSpace_V1::CountComponents);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceGetFamilySEL, (void*)CFPD_ColorSpace_V1::GetFamily);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceGetDefaultValueSEL, (void*)CFPD_ColorSpace_V1::GetDefaultValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpacesRGBSEL, (void*)CFPD_ColorSpace_V1::sRGB);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceGetRGBSEL, (void*)CFPD_ColorSpace_V1::GetRGB);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceSetRGBSEL, (void*)CFPD_ColorSpace_V1::SetRGB);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceGetCMYKSEL, (void*)CFPD_ColorSpace_V1::GetCMYK);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceSetCMYKSEL, (void*)CFPD_ColorSpace_V1::SetCMYK);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceTranslateImageLineSEL, (void*)CFPD_ColorSpace_V1::TranslateImageLine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceGetArraySEL, (void*)CFPD_ColorSpace_V1::GetArray);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceGetMaxIndexSEL, (void*)CFPD_ColorSpace_V1::GetMaxIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSpaceSEL, FPDColorSpaceGetBaseCSSEL, (void*)CFPD_ColorSpace_V1::GetBaseCS);
	}
};

class CFPD_Color_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSEL, FPDColorNewSEL, (void*)CFPD_Color_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSEL, FPDColorDestroySEL, (void*)CFPD_Color_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSEL, FPDColorIsNullSEL, (void*)CFPD_Color_V1::IsNull);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSEL, FPDColorIsEqualSEL, (void*)CFPD_Color_V1::IsEqual);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSEL, FPDColorIsPatternSEL, (void*)CFPD_Color_V1::IsPattern);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSEL, FPDColorCopySEL, (void*)CFPD_Color_V1::Copy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSEL, FPDColorSetColorSpaceSEL, (void*)CFPD_Color_V1::SetColorSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSEL, FPDColorGetColorSpaceSEL, (void*)CFPD_Color_V1::GetColorSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSEL, FPDColorSetValueSEL, (void*)CFPD_Color_V1::SetValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSEL, FPDColorSetValue2SEL, (void*)CFPD_Color_V1::SetValue2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSEL, FPDColorGetRGBSEL, (void*)CFPD_Color_V1::GetRGB);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSEL, FPDColorGetPatternSEL, (void*)CFPD_Color_V1::GetPattern);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSEL, FPDColorGetPatternCSSEL, (void*)CFPD_Color_V1::GetPatternCS);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSEL, FPDColorGetPatternColorSEL, (void*)CFPD_Color_V1::GetPatternColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSEL, FPDColorGetColorBufferSEL, (void*)CFPD_Color_V1::GetColorBuffer);
	}
};

class CFPD_Pattern_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPatternSEL, FPDPatternDestroySEL, (void*)CFPD_Pattern_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPatternSEL, FPDPatternGetPatternObjSEL, (void*)CFPD_Pattern_V1::GetPatternObj);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPatternSEL, FPDPatternGetPatternTypeSEL, (void*)CFPD_Pattern_V1::GetPatternType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPatternSEL, FPDPatternGetPatternMatrixSEL, (void*)CFPD_Pattern_V1::GetPatternMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPatternSEL, FPDPatternGetPDDocSEL, (void*)CFPD_Pattern_V1::GetPDDoc);
	}
};

class CFPD_TilingPattern_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTilingPatternSEL, FPDTilingPatternNewSEL, (void*)CFPD_TilingPattern_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTilingPatternSEL, FPDTilingPatternDestroySEL, (void*)CFPD_TilingPattern_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTilingPatternSEL, FPDTilingPatternGetPatternObjSEL, (void*)CFPD_TilingPattern_V1::GetPatternObj);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTilingPatternSEL, FPDTilingPatternGetPatternTypeSEL, (void*)CFPD_TilingPattern_V1::GetPatternType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTilingPatternSEL, FPDTilingPatternGetPatternMatrixSEL, (void*)CFPD_TilingPattern_V1::GetPatternMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTilingPatternSEL, FPDTilingPatternGetPDDocSEL, (void*)CFPD_TilingPattern_V1::GetPDDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTilingPatternSEL, FPDTilingPatternLoadSEL, (void*)CFPD_TilingPattern_V1::Load);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTilingPatternSEL, FPDTilingPatternIsColoredSEL, (void*)CFPD_TilingPattern_V1::IsColored);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTilingPatternSEL, FPDTilingPatternGetBBoxSEL, (void*)CFPD_TilingPattern_V1::GetBBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTilingPatternSEL, FPDTilingPatternGetXStepSEL, (void*)CFPD_TilingPattern_V1::GetXStep);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTilingPatternSEL, FPDTilingPatternGetYStepSEL, (void*)CFPD_TilingPattern_V1::GetYStep);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTilingPatternSEL, FPDTilingPatternGetFormSEL, (void*)CFPD_TilingPattern_V1::GetForm);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTilingPatternSEL, FPDTilingPatternNewIISEL, (void*)CFPD_TilingPattern_V1::NewII);
	}
};

class CFPD_ShadingPattern_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingPatternSEL, FPDShadingPatternNewSEL, (void*)CFPD_ShadingPattern_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingPatternSEL, FPDShadingPatternDestroySEL, (void*)CFPD_ShadingPattern_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingPatternSEL, FPDShadingPatternGetPatternObjSEL, (void*)CFPD_ShadingPattern_V1::GetPatternObj);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingPatternSEL, FPDShadingPatternGetPatternTypeSEL, (void*)CFPD_ShadingPattern_V1::GetPatternType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingPatternSEL, FPDShadingPatternGetPatternMatrixSEL, (void*)CFPD_ShadingPattern_V1::GetPatternMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingPatternSEL, FPDShadingPatternGetPDDocSEL, (void*)CFPD_ShadingPattern_V1::GetPDDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingPatternSEL, FPDShadingPatternLoadSEL, (void*)CFPD_ShadingPattern_V1::Load);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingPatternSEL, FPDShadingPatternReLoadSEL, (void*)CFPD_ShadingPattern_V1::ReLoad);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingPatternSEL, FPDShadingPatternGetColorSpaceSEL, (void*)CFPD_ShadingPattern_V1::GetColorSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingPatternSEL, FPDShadingPatternGetFuncSEL, (void*)CFPD_ShadingPattern_V1::GetFunc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingPatternSEL, FPDShadingPatternGetFuncsCountSEL, (void*)CFPD_ShadingPattern_V1::GetFuncsCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingPatternSEL, FPDShadingPatternSetColorSpaceSEL, (void*)CFPD_ShadingPattern_V1::SetColorSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingPatternSEL, FPDShadingPatternGetShadingObjectSEL, (void*)CFPD_ShadingPattern_V1::GetShadingObject);
	}
};

class CFPD_MeshStream_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDMeshStreamSEL, FPDMeshStreamNewSEL, (void*)CFPD_MeshStream_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDMeshStreamSEL, FPDMeshStreamDestroySEL, (void*)CFPD_MeshStream_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDMeshStreamSEL, FPDMeshStreamGetFlagSEL, (void*)CFPD_MeshStream_V1::GetFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDMeshStreamSEL, FPDMeshStreamGetCoordsSEL, (void*)CFPD_MeshStream_V1::GetCoords);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDMeshStreamSEL, FPDMeshStreamGetColorSEL, (void*)CFPD_MeshStream_V1::GetColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDMeshStreamSEL, FPDMeshStreamGetVertexSEL, (void*)CFPD_MeshStream_V1::GetVertex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDMeshStreamSEL, FPDMeshStreamGetVertexRowSEL, (void*)CFPD_MeshStream_V1::GetVertexRow);
	}
};

class CFPD_Image_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageNewSEL, (void*)CFPD_Image_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageDestroySEL, (void*)CFPD_Image_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageLoadImageFSEL, (void*)CFPD_Image_V1::LoadImageF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageCloneSEL, (void*)CFPD_Image_V1::Clone);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageGetStreamSEL, (void*)CFPD_Image_V1::GetStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageGetDictSEL, (void*)CFPD_Image_V1::GetDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageGetOCSEL, (void*)CFPD_Image_V1::GetOC);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageGetDocumentSEL, (void*)CFPD_Image_V1::GetDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageGetPixelHeightSEL, (void*)CFPD_Image_V1::GetPixelHeight);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageGetPixelWidthSEL, (void*)CFPD_Image_V1::GetPixelWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageIsMaskSEL, (void*)CFPD_Image_V1::IsMask);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageIsInterpolSEL, (void*)CFPD_Image_V1::IsInterpol);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageLoadDIBitmapSEL, (void*)CFPD_Image_V1::LoadDIBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageSetImageSEL, (void*)CFPD_Image_V1::SetImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageSetJpegImageSEL, (void*)CFPD_Image_V1::SetJpegImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageResetCacheSEL, (void*)CFPD_Image_V1::ResetCache);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageLoadDIBitmapProgressiveSEL, (void*)CFPD_Image_V1::LoadDIBitmapProgressive);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageGetDIBSourceSEL, (void*)CFPD_Image_V1::GetDIBSource);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageGetMaskSEL, (void*)CFPD_Image_V1::GetMask);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageSEL, FPDImageGetMatteColorSEL, (void*)CFPD_Image_V1::GetMatteColor);
	}
};

// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
class CFPD_ObjArchiveSaver_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveSaverSEL, FPDObjArchiveSaverNewSEL, (void*)CFPD_ObjArchiveSaver_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveSaverSEL, FPDObjArchiveSaverDestroySEL, (void*)CFPD_ObjArchiveSaver_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveSaverSEL, FPDObjArchiveSaverSaveObjectSEL, (void*)CFPD_ObjArchiveSaver_V1::SaveObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveSaverSEL, FPDObjArchiveSaverGetLengthSEL, (void*)CFPD_ObjArchiveSaver_V1::GetLength);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveSaverSEL, FPDObjArchiveSaverGetBufferSEL, (void*)CFPD_ObjArchiveSaver_V1::GetBuffer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveSaverSEL, FPDObjArchiveSaverSaveByteSEL, (void*)CFPD_ObjArchiveSaver_V1::SaveByte);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveSaverSEL, FPDObjArchiveSaverSaveIntegerSEL, (void*)CFPD_ObjArchiveSaver_V1::SaveInteger);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveSaverSEL, FPDObjArchiveSaverSaveDWORDSEL, (void*)CFPD_ObjArchiveSaver_V1::SaveDWORD);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveSaverSEL, FPDObjArchiveSaverSaveFloatSEL, (void*)CFPD_ObjArchiveSaver_V1::SaveFloat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveSaverSEL, FPDObjArchiveSaverSaveByteStringSEL, (void*)CFPD_ObjArchiveSaver_V1::SaveByteString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveSaverSEL, FPDObjArchiveSaverSaveWideStringSEL, (void*)CFPD_ObjArchiveSaver_V1::SaveWideString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveSaverSEL, FPDObjArchiveSaverSaveWideStringIISEL, (void*)CFPD_ObjArchiveSaver_V1::SaveWideStringII);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveSaverSEL, FPDObjArchiveSaverWriteSEL, (void*)CFPD_ObjArchiveSaver_V1::Write);
	}
};

class CFPD_ObjArchiveLoader_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveLoaderSEL, FPDObjArchiveLoaderNewSEL, (void*)CFPD_ObjArchiveLoader_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveLoaderSEL, FPDObjArchiveLoaderDestroySEL, (void*)CFPD_ObjArchiveLoader_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveLoaderSEL, FPDObjArchiveLoaderLoadObjectSEL, (void*)CFPD_ObjArchiveLoader_V1::LoadObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveLoaderSEL, FPDObjArchiveLoaderIsEOFSEL, (void*)CFPD_ObjArchiveLoader_V1::IsEOF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveLoaderSEL, FPDObjArchiveLoaderReadSEL, (void*)CFPD_ObjArchiveLoader_V1::Read);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveLoaderSEL, FPDObjArchiveLoaderLoadByteSEL, (void*)CFPD_ObjArchiveLoader_V1::LoadByte);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveLoaderSEL, FPDObjArchiveLoaderLoadIntegerSEL, (void*)CFPD_ObjArchiveLoader_V1::LoadInteger);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveLoaderSEL, FPDObjArchiveLoaderLoadDWORDSEL, (void*)CFPD_ObjArchiveLoader_V1::LoadDWORD);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveLoaderSEL, FPDObjArchiveLoaderLoadFloatSEL, (void*)CFPD_ObjArchiveLoader_V1::LoadFloat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveLoaderSEL, FPDObjArchiveLoaderLoadByteStringSEL, (void*)CFPD_ObjArchiveLoader_V1::LoadByteString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDObjArchiveLoaderSEL, FPDObjArchiveLoaderLoadWideStringSEL, (void*)CFPD_ObjArchiveLoader_V1::LoadWideString);
	}
};

class CFPD_PageArchiveSaver_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageArchiveSaverSEL, FPDPageArchiveSaverNewSEL, (void*)CFPD_PageArchiveSaver_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageArchiveSaverSEL, FPDPageArchiveSaverDestroySEL, (void*)CFPD_PageArchiveSaver_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageArchiveSaverSEL, FPDPageArchiveSaverSavePageObjectSEL, (void*)CFPD_PageArchiveSaver_V1::SavePageObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageArchiveSaverSEL, FPDPageArchiveSaverSaveClipPathSEL, (void*)CFPD_PageArchiveSaver_V1::SaveClipPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageArchiveSaverSEL, FPDPageArchiveSaverSaveGraphStateSEL, (void*)CFPD_PageArchiveSaver_V1::SaveGraphState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageArchiveSaverSEL, FPDPageArchiveSaverSaveTextStateSEL, (void*)CFPD_PageArchiveSaver_V1::SaveTextState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageArchiveSaverSEL, FPDPageArchiveSaverSaveColorStateSEL, (void*)CFPD_PageArchiveSaver_V1::SaveColorState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageArchiveSaverSEL, FPDPageArchiveSaverSaveGeneralStateSEL, (void*)CFPD_PageArchiveSaver_V1::SaveGeneralState);
	}
};

class CFPD_PageArchiveLoader_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageArchiveLoaderSEL, FPDPageArchiveLoaderNewSEL, (void*)CFPD_PageArchiveLoader_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageArchiveLoaderSEL, FPDPageArchiveLoaderDestroySEL, (void*)CFPD_PageArchiveLoader_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageArchiveLoaderSEL, FPDPageArchiveLoaderLoadObjectSEL, (void*)CFPD_PageArchiveLoader_V1::LoadObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageArchiveLoaderSEL, FPDPageArchiveLoaderLoadClipPathSEL, (void*)CFPD_PageArchiveLoader_V1::LoadClipPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageArchiveLoaderSEL, FPDPageArchiveLoaderLoadGraphStateSEL, (void*)CFPD_PageArchiveLoader_V1::LoadGraphState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageArchiveLoaderSEL, FPDPageArchiveLoaderLoadTextStateSEL, (void*)CFPD_PageArchiveLoader_V1::LoadTextState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageArchiveLoaderSEL, FPDPageArchiveLoaderLoadColorStateSEL, (void*)CFPD_PageArchiveLoader_V1::LoadColorState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageArchiveLoaderSEL, FPDPageArchiveLoaderLoadGeneralStateSEL, (void*)CFPD_PageArchiveLoader_V1::LoadGeneralState);
	}
};

class CFPD_Creator_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorNewSEL, (void*)CFPD_Creator_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorDestroySEL, (void*)CFPD_Creator_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorCompressSEL, (void*)CFPD_Creator_V1::Compress);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorSetStandardSecuritySEL, (void*)CFPD_Creator_V1::SetStandardSecurity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorModifyR5SecuritySEL, (void*)CFPD_Creator_V1::ModifyR5Security);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorSetCustomSecuritySEL, (void*)CFPD_Creator_V1::SetCustomSecurity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorReleaseCustomSecurityDataSEL, (void*)CFPD_Creator_V1::ReleaseCustomSecurityData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorRemoveSecuritySEL, (void*)CFPD_Creator_V1::RemoveSecurity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorCreateSEL, (void*)CFPD_Creator_V1::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorCreate2SEL, (void*)CFPD_Creator_V1::Create2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorCreate3SEL, (void*)CFPD_Creator_V1::Create3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorGetObjectOffsetSEL, (void*)CFPD_Creator_V1::GetObjectOffset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorGetObjectSizeSEL, (void*)CFPD_Creator_V1::GetObjectSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorSetProgressiveEncryptHandlerSEL, (void*)CFPD_Creator_V1::SetProgressiveEncryptHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorReleaseProgressiveEncryptHandlerSEL, (void*)CFPD_Creator_V1::ReleaseProgressiveEncryptHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorSetOptionSEL, (void*)CFPD_Creator_V1::SetOption);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorReleaseOptionSEL, (void*)CFPD_Creator_V1::ReleaseOption);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorSetDRMSecuritySEL, (void*)CFPD_Creator_V1::SetDRMSecurity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorReleaseDRMSecurityDataSEL, (void*)CFPD_Creator_V1::ReleaseDRMSecurityData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorSetDRMProgressiveEncryptHandlerSEL, (void*)CFPD_Creator_V1::SetDRMProgressiveEncryptHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorReleaseDRMProgressiveEncryptHandlerSEL, (void*)CFPD_Creator_V1::ReleaseDRMProgressiveEncryptHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorSetProgressiveEncryptHandler2SEL, (void*)CFPD_Creator_V1::SetProgressiveEncryptHandler2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorSetCustomSecurity2SEL, (void*)CFPD_Creator_V1::SetCustomSecurity2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCreatorSEL, FPDCreatorSetCustomSecurity3SEL, (void*)CFPD_Creator_V1::SetCustomSecurity3);
	}
};

// fpd_serialImpl.h end

// In file fpd_textImpl.h
class CFPD_ProgressiveSearch_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDProgressiveSearchSEL, FPDProgressiveSearchNewSEL, (void*)CFPD_ProgressiveSearch_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDProgressiveSearchSEL, FPDProgressiveSearchDestroySEL, (void*)CFPD_ProgressiveSearch_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDProgressiveSearchSEL, FPDProgressiveSearchGetStatusSEL, (void*)CFPD_ProgressiveSearch_V1::GetStatus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDProgressiveSearchSEL, FPDProgressiveSearchFindFromSEL, (void*)CFPD_ProgressiveSearch_V1::FindFrom);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDProgressiveSearchSEL, FPDProgressiveSearchContinueSEL, (void*)CFPD_ProgressiveSearch_V1::Continue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDProgressiveSearchSEL, FPDProgressiveSearchFindNextSEL, (void*)CFPD_ProgressiveSearch_V1::FindNext);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDProgressiveSearchSEL, FPDProgressiveSearchFindPrevSEL, (void*)CFPD_ProgressiveSearch_V1::FindPrev);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDProgressiveSearchSEL, FPDProgressiveSearchCountRectsSEL, (void*)CFPD_ProgressiveSearch_V1::CountRects);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDProgressiveSearchSEL, FPDProgressiveSearchGetRectSEL, (void*)CFPD_ProgressiveSearch_V1::GetRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDProgressiveSearchSEL, FPDProgressiveSearchGetPosSEL, (void*)CFPD_ProgressiveSearch_V1::GetPos);
	}
};

class CFPD_TextPage_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageNewSEL, (void*)CFPD_TextPage_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageDestroySEL, (void*)CFPD_TextPage_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageParseTextPageSEL, (void*)CFPD_TextPage_V1::ParseTextPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageIsParseredSEL, (void*)CFPD_TextPage_V1::IsParsered);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageCountCharsSEL, (void*)CFPD_TextPage_V1::CountChars);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageGetCharInfoSEL, (void*)CFPD_TextPage_V1::GetCharInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageGetRectArraySEL, (void*)CFPD_TextPage_V1::GetRectArray);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageGetIndexAtPosSEL, (void*)CFPD_TextPage_V1::GetIndexAtPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageGetOrderByDirectionSEL, (void*)CFPD_TextPage_V1::GetOrderByDirection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageGetTextByRectSEL, (void*)CFPD_TextPage_V1::GetTextByRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageGetRectsArrayByRectSEL, (void*)CFPD_TextPage_V1::GetRectsArrayByRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageCountRectsSEL, (void*)CFPD_TextPage_V1::CountRects);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageGetRectSEL, (void*)CFPD_TextPage_V1::GetRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageCountBoundedSegmentsSEL, (void*)CFPD_TextPage_V1::CountBoundedSegments);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageGetBoundedSegmentSEL, (void*)CFPD_TextPage_V1::GetBoundedSegment);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageGetWordBreakSEL, (void*)CFPD_TextPage_V1::GetWordBreak);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageSEL, FPDTextPageGetPageTextSEL, (void*)CFPD_TextPage_V1::GetPageText);
	}
};

class CFPD_TextPageFind_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageFindSEL, FPDTextPageFindNewSEL, (void*)CFPD_TextPageFind_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageFindSEL, FPDTextPageFindDestroySEL, (void*)CFPD_TextPageFind_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageFindSEL, FPDTextPageFindFindFirstSEL, (void*)CFPD_TextPageFind_V1::FindFirst);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageFindSEL, FPDTextPageFindFindNextSEL, (void*)CFPD_TextPageFind_V1::FindNext);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageFindSEL, FPDTextPageFindFindPrevSEL, (void*)CFPD_TextPageFind_V1::FindPrev);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageFindSEL, FPDTextPageFindGetRectArraySEL, (void*)CFPD_TextPageFind_V1::GetRectArray);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageFindSEL, FPDTextPageFindGetCurOrderSEL, (void*)CFPD_TextPageFind_V1::GetCurOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextPageFindSEL, FPDTextPageFindGetMatchedCountSEL, (void*)CFPD_TextPageFind_V1::GetMatchedCount);
	}
};

class CFPD_LinkExtract_V1_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLinkExtractSEL, FPDLinkExtractNewSEL, (void*)CFPD_LinkExtract_V1::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLinkExtractSEL, FPDLinkExtractDestroySEL, (void*)CFPD_LinkExtract_V1::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLinkExtractSEL, FPDLinkExtractExtractLinksSEL, (void*)CFPD_LinkExtract_V1::ExtractLinks);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLinkExtractSEL, FPDLinkExtractCountLinksSEL, (void*)CFPD_LinkExtract_V1::CountLinks);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLinkExtractSEL, FPDLinkExtractGetURLSEL, (void*)CFPD_LinkExtract_V1::GetURL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDLinkExtractSEL, FPDLinkExtractGetRectsSEL, (void*)CFPD_LinkExtract_V1::GetRects);
	}
};

// fpd_textImpl.h end

//----------_V2----------
// In file fs_basicImpl.h
class CFS_Base64Encoder_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBase64EncoderSEL, FSBase64EncoderNewSEL, (void*)CFS_Base64Encoder_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBase64EncoderSEL, FSBase64EncoderDestroySEL, (void*)CFS_Base64Encoder_V2::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBase64EncoderSEL, FSBase64EncoderSetEncoderSEL, (void*)CFS_Base64Encoder_V2::SetEncoder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBase64EncoderSEL, FSBase64EncoderEncodeSEL, (void*)CFS_Base64Encoder_V2::Encode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBase64EncoderSEL, FSBase64EncoderEncode2SEL, (void*)CFS_Base64Encoder_V2::Encode2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBase64EncoderSEL, FSBase64EncoderEncode3SEL, (void*)CFS_Base64Encoder_V2::Encode3);
	}
};

class CFS_Base64Decoder_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBase64DecoderSEL, FSBase64DecoderNewSEL, (void*)CFS_Base64Decoder_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBase64DecoderSEL, FSBase64DecoderDestroySEL, (void*)CFS_Base64Decoder_V2::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBase64DecoderSEL, FSBase64DecoderSetDecoderSEL, (void*)CFS_Base64Decoder_V2::SetDecoder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBase64DecoderSEL, FSBase64DecoderDecodeSEL, (void*)CFS_Base64Decoder_V2::Decode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBase64DecoderSEL, FSBase64DecoderDecode2SEL, (void*)CFS_Base64Decoder_V2::Decode2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSBase64DecoderSEL, FSBase64DecoderDecode3SEL, (void*)CFS_Base64Decoder_V2::Decode3);
	}
};

class CFS_FileWriteHandler_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFileWriteHandlerSEL, FSFileWriteHandlerNewSEL, (void*)CFS_FileWriteHandler_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFileWriteHandlerSEL, FSFileWriteHandlerDestroySEL, (void*)CFS_FileWriteHandler_V2::Destroy);
	}
};

class CFS_XMLElement_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementparseSEL, (void*)CFS_XMLElement_V2::parse);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementparse2SEL, (void*)CFS_XMLElement_V2::parse2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementNewSEL, (void*)CFS_XMLElement_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementNew2SEL, (void*)CFS_XMLElement_V2::New2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementNew3SEL, (void*)CFS_XMLElement_V2::New3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementDestroySEL, (void*)CFS_XMLElement_V2::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementGetTagNameSEL, (void*)CFS_XMLElement_V2::GetTagName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementGetNamespaceSEL, (void*)CFS_XMLElement_V2::GetNamespace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementGetNamespaceURISEL, (void*)CFS_XMLElement_V2::GetNamespaceURI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementGetParentSEL, (void*)CFS_XMLElement_V2::GetParent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementCountAttrsSEL, (void*)CFS_XMLElement_V2::CountAttrs);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementGetAttrByIndexSEL, (void*)CFS_XMLElement_V2::GetAttrByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementHasAttrSEL, (void*)CFS_XMLElement_V2::HasAttr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementGetAttrValueSEL, (void*)CFS_XMLElement_V2::GetAttrValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementGetAttrValue2SEL, (void*)CFS_XMLElement_V2::GetAttrValue2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementGetAttrIntegerSEL, (void*)CFS_XMLElement_V2::GetAttrInteger);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementGetAttrInteger2SEL, (void*)CFS_XMLElement_V2::GetAttrInteger2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementGetAttrFloatSEL, (void*)CFS_XMLElement_V2::GetAttrFloat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementGetAttrFloat2SEL, (void*)CFS_XMLElement_V2::GetAttrFloat2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementCountChildrenSEL, (void*)CFS_XMLElement_V2::CountChildren);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementGetChildTypeSEL, (void*)CFS_XMLElement_V2::GetChildType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementGetContentSEL, (void*)CFS_XMLElement_V2::GetContent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementGetElementSEL, (void*)CFS_XMLElement_V2::GetElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementGetElement2SEL, (void*)CFS_XMLElement_V2::GetElement2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementGetElement3SEL, (void*)CFS_XMLElement_V2::GetElement3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementCountElementsSEL, (void*)CFS_XMLElement_V2::CountElements);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementFindElementSEL, (void*)CFS_XMLElement_V2::FindElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementSetTagSEL, (void*)CFS_XMLElement_V2::SetTag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementSetTag2SEL, (void*)CFS_XMLElement_V2::SetTag2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementSetAttrValueSEL, (void*)CFS_XMLElement_V2::SetAttrValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementSetAttrValue2SEL, (void*)CFS_XMLElement_V2::SetAttrValue2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementSetAttrValue3SEL, (void*)CFS_XMLElement_V2::SetAttrValue3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementRemoveAttrSEL, (void*)CFS_XMLElement_V2::RemoveAttr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementAddChildElementSEL, (void*)CFS_XMLElement_V2::AddChildElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementAddChildContentSEL, (void*)CFS_XMLElement_V2::AddChildContent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementInsertChildElementSEL, (void*)CFS_XMLElement_V2::InsertChildElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementInsertChildContentSEL, (void*)CFS_XMLElement_V2::InsertChildContent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementRemoveChildrenSEL, (void*)CFS_XMLElement_V2::RemoveChildren);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementRemoveChildSEL, (void*)CFS_XMLElement_V2::RemoveChild);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementOutputStreamSEL, (void*)CFS_XMLElement_V2::OutputStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementOutputStream2SEL, (void*)CFS_XMLElement_V2::OutputStream2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSXMLElementSEL, FSXMLElementCloneSEL, (void*)CFS_XMLElement_V2::Clone);
	}
};

// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
class CFDRM_CategoryRead_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryReadSEL, FDRMCategoryReadCountSubCategoriesSEL, (void*)CFDRM_CategoryRead_V2::CountSubCategories);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryReadSEL, FDRMCategoryReadGetSubCategorySEL, (void*)CFDRM_CategoryRead_V2::GetSubCategory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryReadSEL, FDRMCategoryReadFindSubCategorySEL, (void*)CFDRM_CategoryRead_V2::FindSubCategory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryReadSEL, FDRMCategoryReadGetParentCategorySEL, (void*)CFDRM_CategoryRead_V2::GetParentCategory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryReadSEL, FDRMCategoryReadGetCategoryNameSEL, (void*)CFDRM_CategoryRead_V2::GetCategoryName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryReadSEL, FDRMCategoryReadCountAttributesSEL, (void*)CFDRM_CategoryRead_V2::CountAttributes);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryReadSEL, FDRMCategoryReadGetAttributeSEL, (void*)CFDRM_CategoryRead_V2::GetAttribute);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryReadSEL, FDRMCategoryReadGetAttributeValueSEL, (void*)CFDRM_CategoryRead_V2::GetAttributeValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryReadSEL, FDRMCategoryReadGetCategoryDataSEL, (void*)CFDRM_CategoryRead_V2::GetCategoryData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryReadSEL, FDRMCategoryReadDestroySEL, (void*)CFDRM_CategoryRead_V2::Destroy);
	}
};

class CFDRM_CategoryWrite_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryWriteSEL, FDRMCategoryWriteAddCategorySEL, (void*)CFDRM_CategoryWrite_V2::AddCategory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryWriteSEL, FDRMCategoryWriteAddCategory2SEL, (void*)CFDRM_CategoryWrite_V2::AddCategory2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryWriteSEL, FDRMCategoryWriteSetAttributeSEL, (void*)CFDRM_CategoryWrite_V2::SetAttribute);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryWriteSEL, FDRMCategoryWriteRemoveAttributeSEL, (void*)CFDRM_CategoryWrite_V2::RemoveAttribute);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryWriteSEL, FDRMCategoryWriteSetCategoryDataSEL, (void*)CFDRM_CategoryWrite_V2::SetCategoryData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryWriteSEL, FDRMCategoryWriteRemoveCategorySEL, (void*)CFDRM_CategoryWrite_V2::RemoveCategory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryWriteSEL, FDRMCategoryWriteRemoveCategory2SEL, (void*)CFDRM_CategoryWrite_V2::RemoveCategory2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryWriteSEL, FDRMCategoryWriteRemoveCategory3SEL, (void*)CFDRM_CategoryWrite_V2::RemoveCategory3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryWriteSEL, FDRMCategoryWriteCastToCategoryReadSEL, (void*)CFDRM_CategoryWrite_V2::CastToCategoryRead);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMCategoryWriteSEL, FDRMCategoryWriteDestroySEL, (void*)CFDRM_CategoryWrite_V2::Destroy);
	}
};

class CFDRM_DescData_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescDataSEL, FDRMDescDataNewSEL, (void*)CFDRM_DescData_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescDataSEL, FDRMDescDataDestroySEL, (void*)CFDRM_DescData_V2::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescDataSEL, FDRMDescDataGetPackageNameSEL, (void*)CFDRM_DescData_V2::GetPackageName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescDataSEL, FDRMDescDataGetDefNameSpaceSEL, (void*)CFDRM_DescData_V2::GetDefNameSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescDataSEL, FDRMDescDataGetFDRMNameSpaceSEL, (void*)CFDRM_DescData_V2::GetFDRMNameSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescDataSEL, FDRMDescDataSetPackageNameSEL, (void*)CFDRM_DescData_V2::SetPackageName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescDataSEL, FDRMDescDataSetDefNameSpaceSEL, (void*)CFDRM_DescData_V2::SetDefNameSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescDataSEL, FDRMDescDataSetFDRMNameSpaceSEL, (void*)CFDRM_DescData_V2::SetFDRMNameSpace);
	}
};

class CFDRM_ScriptData_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMScriptDataSEL, FDRMScriptDataNewSEL, (void*)CFDRM_ScriptData_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMScriptDataSEL, FDRMScriptDataDestroySEL, (void*)CFDRM_ScriptData_V2::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMScriptDataSEL, FDRMScriptDataGetDivisionSEL, (void*)CFDRM_ScriptData_V2::GetDivision);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMScriptDataSEL, FDRMScriptDataGetFormatOrgSEL, (void*)CFDRM_ScriptData_V2::GetFormatOrg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMScriptDataSEL, FDRMScriptDataGetFormatVerSEL, (void*)CFDRM_ScriptData_V2::GetFormatVer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMScriptDataSEL, FDRMScriptDataGetAppOrgSEL, (void*)CFDRM_ScriptData_V2::GetAppOrg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMScriptDataSEL, FDRMScriptDataGetAppVerSEL, (void*)CFDRM_ScriptData_V2::GetAppVer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMScriptDataSEL, FDRMScriptDataGetAuthoritySEL, (void*)CFDRM_ScriptData_V2::GetAuthority);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMScriptDataSEL, FDRMScriptDataSetDivisionSEL, (void*)CFDRM_ScriptData_V2::SetDivision);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMScriptDataSEL, FDRMScriptDataSetFormatOrgSEL, (void*)CFDRM_ScriptData_V2::SetFormatOrg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMScriptDataSEL, FDRMScriptDataSetFormatVerSEL, (void*)CFDRM_ScriptData_V2::SetFormatVer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMScriptDataSEL, FDRMScriptDataSetAppOrgSEL, (void*)CFDRM_ScriptData_V2::SetAppOrg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMScriptDataSEL, FDRMScriptDataSetAppVerSEL, (void*)CFDRM_ScriptData_V2::SetAppVer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMScriptDataSEL, FDRMScriptDataSetAuthoritySEL, (void*)CFDRM_ScriptData_V2::SetAuthority);
	}
};

class CFDRM_PresentationData_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPresentationDataSEL, FDRMPresentationDataNewSEL, (void*)CFDRM_PresentationData_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPresentationDataSEL, FDRMPresentationDataDestroySEL, (void*)CFDRM_PresentationData_V2::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPresentationDataSEL, FDRMPresentationDataGetDivisionSEL, (void*)CFDRM_PresentationData_V2::GetDivision);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPresentationDataSEL, FDRMPresentationDataGetAuthoritySEL, (void*)CFDRM_PresentationData_V2::GetAuthority);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPresentationDataSEL, FDRMPresentationDataSetDivisionSEL, (void*)CFDRM_PresentationData_V2::SetDivision);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPresentationDataSEL, FDRMPresentationDataSetAuthoritySEL, (void*)CFDRM_PresentationData_V2::SetAuthority);
	}
};

class CFDRM_SignatureData_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMSignatureDataSEL, FDRMSignatureDataNewSEL, (void*)CFDRM_SignatureData_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMSignatureDataSEL, FDRMSignatureDataDestroySEL, (void*)CFDRM_SignatureData_V2::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMSignatureDataSEL, FDRMSignatureDataGetDivisionSEL, (void*)CFDRM_SignatureData_V2::GetDivision);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMSignatureDataSEL, FDRMSignatureDataGetAgentOrgSEL, (void*)CFDRM_SignatureData_V2::GetAgentOrg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMSignatureDataSEL, FDRMSignatureDataGetAgentVerSEL, (void*)CFDRM_SignatureData_V2::GetAgentVer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMSignatureDataSEL, FDRMSignatureDataGetFormatOrgSEL, (void*)CFDRM_SignatureData_V2::GetFormatOrg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMSignatureDataSEL, FDRMSignatureDataGetFormatVerSEL, (void*)CFDRM_SignatureData_V2::GetFormatVer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMSignatureDataSEL, FDRMSignatureDataGetKeySEL, (void*)CFDRM_SignatureData_V2::GetKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMSignatureDataSEL, FDRMSignatureDataSetDivisionSEL, (void*)CFDRM_SignatureData_V2::SetDivision);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMSignatureDataSEL, FDRMSignatureDataSetAgentOrgSEL, (void*)CFDRM_SignatureData_V2::SetAgentOrg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMSignatureDataSEL, FDRMSignatureDataSetAgentVerSEL, (void*)CFDRM_SignatureData_V2::SetAgentVer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMSignatureDataSEL, FDRMSignatureDataSetFormatOrgSEL, (void*)CFDRM_SignatureData_V2::SetFormatOrg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMSignatureDataSEL, FDRMSignatureDataSetFormatVerSEL, (void*)CFDRM_SignatureData_V2::SetFormatVer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMSignatureDataSEL, FDRMSignatureDataSetKeySEL, (void*)CFDRM_SignatureData_V2::SetKey);
	}
};

class CFDRM_DescRead_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadNewSEL, (void*)CFDRM_DescRead_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadDestroySEL, (void*)CFDRM_DescRead_V2::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadLoadSEL, (void*)CFDRM_DescRead_V2::Load);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadVerifyDescriptorSEL, (void*)CFDRM_DescRead_V2::VerifyDescriptor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadGetDescTypeSEL, (void*)CFDRM_DescRead_V2::GetDescType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadGetScriptSEL, (void*)CFDRM_DescRead_V2::GetScript);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadCountScriptsSEL, (void*)CFDRM_DescRead_V2::CountScripts);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadGetScript2SEL, (void*)CFDRM_DescRead_V2::GetScript2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadGetScriptDataSEL, (void*)CFDRM_DescRead_V2::GetScriptData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadGetScriptParamsSEL, (void*)CFDRM_DescRead_V2::GetScriptParams);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadGetPresentationSEL, (void*)CFDRM_DescRead_V2::GetPresentation);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadGetPresentationDataSEL, (void*)CFDRM_DescRead_V2::GetPresentationData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadGetPresentationParamsSEL, (void*)CFDRM_DescRead_V2::GetPresentationParams);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadGetSignatureSEL, (void*)CFDRM_DescRead_V2::GetSignature);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadGetSignatureDataSEL, (void*)CFDRM_DescRead_V2::GetSignatureData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadGetSignatureParamsSEL, (void*)CFDRM_DescRead_V2::GetSignatureParams);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadGetSignatureData2SEL, (void*)CFDRM_DescRead_V2::GetSignatureData2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadCheckSignatureSEL, (void*)CFDRM_DescRead_V2::CheckSignature);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescReadSEL, FDRMDescReadVerifyValidationSEL, (void*)CFDRM_DescRead_V2::VerifyValidation);
	}
};

class CFDRM_DescWrite_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteNewSEL, (void*)CFDRM_DescWrite_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteDestroySEL, (void*)CFDRM_DescWrite_V2::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteInitDescriptorSEL, (void*)CFDRM_DescWrite_V2::InitDescriptor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteRemoveScriptSEL, (void*)CFDRM_DescWrite_V2::RemoveScript);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteAddScriptSEL, (void*)CFDRM_DescWrite_V2::AddScript);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteSetScriptFormatSEL, (void*)CFDRM_DescWrite_V2::SetScriptFormat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteSetScriptApplicationSEL, (void*)CFDRM_DescWrite_V2::SetScriptApplication);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteSetScriptAuthoritySEL, (void*)CFDRM_DescWrite_V2::SetScriptAuthority);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteSetScriptParamsSEL, (void*)CFDRM_DescWrite_V2::SetScriptParams);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteRemovePresentationSEL, (void*)CFDRM_DescWrite_V2::RemovePresentation);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteAddPresentationSEL, (void*)CFDRM_DescWrite_V2::AddPresentation);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteSetPresentationAuthoritySEL, (void*)CFDRM_DescWrite_V2::SetPresentationAuthority);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteSetPresentationParamsSEL, (void*)CFDRM_DescWrite_V2::SetPresentationParams);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteRemoveSignatureSEL, (void*)CFDRM_DescWrite_V2::RemoveSignature);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteAddSignatureSEL, (void*)CFDRM_DescWrite_V2::AddSignature);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteSetSignatureAgentSEL, (void*)CFDRM_DescWrite_V2::SetSignatureAgent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteSetSignatureFormatSEL, (void*)CFDRM_DescWrite_V2::SetSignatureFormat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteSetSignatureParamsSEL, (void*)CFDRM_DescWrite_V2::SetSignatureParams);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteSetSignatureKeySEL, (void*)CFDRM_DescWrite_V2::SetSignatureKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteSaveSEL, (void*)CFDRM_DescWrite_V2::Save);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMDescWriteSEL, FDRMDescWriteCastToDescReadSEL, (void*)CFDRM_DescWrite_V2::CastToDescRead);
	}
};

class CFDRM_FoacRead_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacReadSEL, FDRMFoacReadNewSEL, (void*)CFDRM_FoacRead_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacReadSEL, FDRMFoacReadDestroySEL, (void*)CFDRM_FoacRead_V2::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacReadSEL, FDRMFoacReadGetDescReadSEL, (void*)CFDRM_FoacRead_V2::GetDescRead);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacReadSEL, FDRMFoacReadVerifyFoacSEL, (void*)CFDRM_FoacRead_V2::VerifyFoac);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacReadSEL, FDRMFoacReadGetTypeSEL, (void*)CFDRM_FoacRead_V2::GetType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacReadSEL, FDRMFoacReadGetSessionIDSEL, (void*)CFDRM_FoacRead_V2::GetSessionID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacReadSEL, FDRMFoacReadGetRequestIDSEL, (void*)CFDRM_FoacRead_V2::GetRequestID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacReadSEL, FDRMFoacReadGetRequestDataSEL, (void*)CFDRM_FoacRead_V2::GetRequestData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacReadSEL, FDRMFoacReadGetAnswerStateSEL, (void*)CFDRM_FoacRead_V2::GetAnswerState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacReadSEL, FDRMFoacReadGetAnswerDataSEL, (void*)CFDRM_FoacRead_V2::GetAnswerData);
	}
};

class CFDRM_FoacWrite_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacWriteSEL, FDRMFoacWriteNewSEL, (void*)CFDRM_FoacWrite_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacWriteSEL, FDRMFoacWriteDestroySEL, (void*)CFDRM_FoacWrite_V2::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacWriteSEL, FDRMFoacWriteGetDescWriteSEL, (void*)CFDRM_FoacWrite_V2::GetDescWrite);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacWriteSEL, FDRMFoacWriteInitFoacSEL, (void*)CFDRM_FoacWrite_V2::InitFoac);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacWriteSEL, FDRMFoacWriteSetSessionIDSEL, (void*)CFDRM_FoacWrite_V2::SetSessionID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacWriteSEL, FDRMFoacWriteSetRequestIDSEL, (void*)CFDRM_FoacWrite_V2::SetRequestID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacWriteSEL, FDRMFoacWriteSetRequestDataSEL, (void*)CFDRM_FoacWrite_V2::SetRequestData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacWriteSEL, FDRMFoacWriteSetAnswerStateSEL, (void*)CFDRM_FoacWrite_V2::SetAnswerState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacWriteSEL, FDRMFoacWriteSetAnswerDataSEL, (void*)CFDRM_FoacWrite_V2::SetAnswerData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMFoacWriteSEL, FDRMFoacWriteCastToFoacReadSEL, (void*)CFDRM_FoacWrite_V2::CastToFoacRead);
	}
};

class CFDRM_EnvelopeRead_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadNewSEL, (void*)CFDRM_EnvelopeRead_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadDestroySEL, (void*)CFDRM_EnvelopeRead_V2::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadLoadEnvelopeSEL, (void*)CFDRM_EnvelopeRead_V2::LoadEnvelope);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadGetFormatSEL, (void*)CFDRM_EnvelopeRead_V2::GetFormat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadGetApplicationSEL, (void*)CFDRM_EnvelopeRead_V2::GetApplication);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadGetEnvelopeSNSEL, (void*)CFDRM_EnvelopeRead_V2::GetEnvelopeSN);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadGetFileIDSEL, (void*)CFDRM_EnvelopeRead_V2::GetFileID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadGetIssuerSEL, (void*)CFDRM_EnvelopeRead_V2::GetIssuer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadGetConsumerIdSEL, (void*)CFDRM_EnvelopeRead_V2::GetConsumerId);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadCountAuthKeysSEL, (void*)CFDRM_EnvelopeRead_V2::CountAuthKeys);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadGetAuthKeySEL, (void*)CFDRM_EnvelopeRead_V2::GetAuthKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadCountLimitsSEL, (void*)CFDRM_EnvelopeRead_V2::CountLimits);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadGetLimitSEL, (void*)CFDRM_EnvelopeRead_V2::GetLimit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadCountRightsSEL, (void*)CFDRM_EnvelopeRead_V2::CountRights);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadGetRightSEL, (void*)CFDRM_EnvelopeRead_V2::GetRight);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadGetCipherSEL, (void*)CFDRM_EnvelopeRead_V2::GetCipher);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEnvelopeReadSEL, FDRMEnvelopeReadValidateEnvelopeSEL, (void*)CFDRM_EnvelopeRead_V2::ValidateEnvelope);
	}
};

// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
class CFDRM_Mgr_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMMgrSEL, FDRMMgrCreateDefSEL, (void*)CFDRM_Mgr_V2::CreateDef);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMMgrSEL, FDRMMgrGetCurrentUseSEL, (void*)CFDRM_Mgr_V2::GetCurrentUse);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMMgrSEL, FDRMMgrSetCurrentUseSEL, (void*)CFDRM_Mgr_V2::SetCurrentUse);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMMgrSEL, FDRMMgrDestroySEL, (void*)CFDRM_Mgr_V2::Destroy);
	}
};

// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
class CFDRM_PDFSecurityHandler_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSecurityHandlerSEL, FDRMPDFSecurityHandlerNewSEL, (void*)CFDRM_PDFSecurityHandler_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSecurityHandlerSEL, FDRMPDFSecurityHandlerDestroySEL, (void*)CFDRM_PDFSecurityHandler_V2::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSecurityHandlerSEL, FDRMPDFSecurityHandlerInitSEL, (void*)CFDRM_PDFSecurityHandler_V2::Init);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSecurityHandlerSEL, FDRMPDFSecurityHandlerGetPermissionsSEL, (void*)CFDRM_PDFSecurityHandler_V2::GetPermissions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSecurityHandlerSEL, FDRMPDFSecurityHandlerIsOwnerSEL, (void*)CFDRM_PDFSecurityHandler_V2::IsOwner);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSecurityHandlerSEL, FDRMPDFSecurityHandlerGetCryptInfoSEL, (void*)CFDRM_PDFSecurityHandler_V2::GetCryptInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSecurityHandlerSEL, FDRMPDFSecurityHandlerSetSubFilterSEL, (void*)CFDRM_PDFSecurityHandler_V2::SetSubFilter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSecurityHandlerSEL, FDRMPDFSecurityHandlerSetIdentityEntrySEL, (void*)CFDRM_PDFSecurityHandler_V2::SetIdentityEntry);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSecurityHandlerSEL, FDRMPDFSecurityHandlerGetSubFilterSEL, (void*)CFDRM_PDFSecurityHandler_V2::GetSubFilter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSecurityHandlerSEL, FDRMPDFSecurityHandlerGetIdentityEntrySEL, (void*)CFDRM_PDFSecurityHandler_V2::GetIdentityEntry);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSecurityHandlerSEL, FDRMPDFSecurityHandlerSetCryptInfoSEL, (void*)CFDRM_PDFSecurityHandler_V2::SetCryptInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSecurityHandlerSEL, FDRMPDFSecurityHandlerCheckValiditySEL, (void*)CFDRM_PDFSecurityHandler_V2::CheckValidity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSecurityHandlerSEL, FDRMPDFSecurityHandlerCreateCryptoHandlerSEL, (void*)CFDRM_PDFSecurityHandler_V2::CreateCryptoHandler);
	}
};

class CFDRM_PDFSchema_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSchemaSEL, FDRMPDFSchemaNewSEL, (void*)CFDRM_PDFSchema_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSchemaSEL, FDRMPDFSchemaDestroySEL, (void*)CFDRM_PDFSchema_V2::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSchemaSEL, FDRMPDFSchemaLoadSchemaSEL, (void*)CFDRM_PDFSchema_V2::LoadSchema);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSchemaSEL, FDRMPDFSchemaGetPDFFileSizeSEL, (void*)CFDRM_PDFSchema_V2::GetPDFFileSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSchemaSEL, FDRMPDFSchemaGetXRefOffsetSEL, (void*)CFDRM_PDFSchema_V2::GetXRefOffset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSchemaSEL, FDRMPDFSchemaCountPagesSEL, (void*)CFDRM_PDFSchema_V2::CountPages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSchemaSEL, FDRMPDFSchemaGetSchemaInfoSEL, (void*)CFDRM_PDFSchema_V2::GetSchemaInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPDFSchemaSEL, FDRMPDFSchemaGetPageSizeSEL, (void*)CFDRM_PDFSchema_V2::GetPageSize);
	}
};

class CFDRM_EncryptDictRead_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptDictReadSEL, FDRMEncryptDictReadNewSEL, (void*)CFDRM_EncryptDictRead_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptDictReadSEL, FDRMEncryptDictReadDestroySEL, (void*)CFDRM_EncryptDictRead_V2::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptDictReadSEL, FDRMEncryptDictReadVerifySEL, (void*)CFDRM_EncryptDictRead_V2::Verify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptDictReadSEL, FDRMEncryptDictReadGetSubFilterSEL, (void*)CFDRM_EncryptDictRead_V2::GetSubFilter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptDictReadSEL, FDRMEncryptDictReadGetIssuerSEL, (void*)CFDRM_EncryptDictRead_V2::GetIssuer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptDictReadSEL, FDRMEncryptDictReadGetCreatorSEL, (void*)CFDRM_EncryptDictRead_V2::GetCreator);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptDictReadSEL, FDRMEncryptDictReadGetFileIdSEL, (void*)CFDRM_EncryptDictRead_V2::GetFileId);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptDictReadSEL, FDRMEncryptDictReadGetFlowCodeSEL, (void*)CFDRM_EncryptDictRead_V2::GetFlowCode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptDictReadSEL, FDRMEncryptDictReadGetOrderSEL, (void*)CFDRM_EncryptDictRead_V2::GetOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptDictReadSEL, FDRMEncryptDictReadGetUserSEL, (void*)CFDRM_EncryptDictRead_V2::GetUser);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptDictReadSEL, FDRMEncryptDictReadGetServiceURLSEL, (void*)CFDRM_EncryptDictRead_V2::GetServiceURL);
	}
};

class CFDRM_Encryptor_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptorSEL, FDRMEncryptorArcFourCryptBlockSEL, (void*)CFDRM_Encryptor_V2::ArcFourCryptBlock);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptorSEL, FDRMEncryptorArcFourSetupSEL, (void*)CFDRM_Encryptor_V2::ArcFourSetup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptorSEL, FDRMEncryptorArcFourCryptSEL, (void*)CFDRM_Encryptor_V2::ArcFourCrypt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptorSEL, FDRMEncryptorSHA256StartSEL, (void*)CFDRM_Encryptor_V2::SHA256Start);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptorSEL, FDRMEncryptorSHA256UpdateSEL, (void*)CFDRM_Encryptor_V2::SHA256Update);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptorSEL, FDRMEncryptorSHA256FinishSEL, (void*)CFDRM_Encryptor_V2::SHA256Finish);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptorSEL, FDRMEncryptorSHA256DigestGenerateSEL, (void*)CFDRM_Encryptor_V2::SHA256DigestGenerate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptorSEL, FDRMEncryptorHashMD5128SEL, (void*)CFDRM_Encryptor_V2::HashMD5128);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptorSEL, FDRMEncryptorAESSetKeySEL, (void*)CFDRM_Encryptor_V2::AESSetKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptorSEL, FDRMEncryptorAESSetIVSEL, (void*)CFDRM_Encryptor_V2::AESSetIV);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptorSEL, FDRMEncryptorAESDecryptSEL, (void*)CFDRM_Encryptor_V2::AESDecrypt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptorSEL, FDRMEncryptorAESEncryptSEL, (void*)CFDRM_Encryptor_V2::AESEncrypt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptorSEL, FDRMEncryptorAESSetKey2SEL, (void*)CFDRM_Encryptor_V2::AESSetKey2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptorSEL, FDRMEncryptorMD5StartSEL, (void*)CFDRM_Encryptor_V2::MD5Start);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptorSEL, FDRMEncryptorMD5UpdateSEL, (void*)CFDRM_Encryptor_V2::MD5Update);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMEncryptorSEL, FDRMEncryptorMD5FinishSEL, (void*)CFDRM_Encryptor_V2::MD5Finish);
	}
};

// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
class CFDRM_PKI_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPKISEL, FDRMPKICreateRsaKeySEL, (void*)CFDRM_PKI_V2::CreateRsaKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPKISEL, FDRMPKIRsaEncryptSEL, (void*)CFDRM_PKI_V2::RsaEncrypt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPKISEL, FDRMPKIRsaDecryptSEL, (void*)CFDRM_PKI_V2::RsaDecrypt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPKISEL, FDRMPKIRsaSignSEL, (void*)CFDRM_PKI_V2::RsaSign);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPKISEL, FDRMPKIVerifyRsaSignSEL, (void*)CFDRM_PKI_V2::VerifyRsaSign);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPKISEL, FDRMPKICreateDsaKeySEL, (void*)CFDRM_PKI_V2::CreateDsaKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPKISEL, FDRMPKIDsaSignSEL, (void*)CFDRM_PKI_V2::DsaSign);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPKISEL, FDRMPKIVerifyDsaSignSEL, (void*)CFDRM_PKI_V2::VerifyDsaSign);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPKISEL, FDRMPKIVerifyRsaKeySEL, (void*)CFDRM_PKI_V2::VerifyRsaKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPKISEL, FDRMPKIVerifyDasKeySEL, (void*)CFDRM_PKI_V2::VerifyDasKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FDRMPKISEL, FDRMPKIGenMD5DigestSEL, (void*)CFDRM_PKI_V2::GenMD5Digest);
	}
};

// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
class CFPD_WrapperCreator_V2_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDWrapperCreatorSEL, FPDWrapperCreatorNewSEL, (void*)CFPD_WrapperCreator_V2::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDWrapperCreatorSEL, FPDWrapperCreatorDestroySEL, (void*)CFPD_WrapperCreator_V2::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDWrapperCreatorSEL, FPDWrapperCreatorSetWrapperDataSEL, (void*)CFPD_WrapperCreator_V2::SetWrapperData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDWrapperCreatorSEL, FPDWrapperCreatorCreateSEL, (void*)CFPD_WrapperCreator_V2::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDWrapperCreatorSEL, FPDWrapperCreatorSetStandardSecuritySEL, (void*)CFPD_WrapperCreator_V2::SetStandardSecurity);
	}
};

// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V3----------
// In file fs_basicImpl.h
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
class CFR_ThumbnailView_V3_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRThumbnailViewSEL, FRThumbnailViewCountPageSEL, (void*)CFR_ThumbnailView_V3::CountPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRThumbnailViewSEL, FRThumbnailViewCountVisiblePageSEL, (void*)CFR_ThumbnailView_V3::CountVisiblePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRThumbnailViewSEL, FRThumbnailViewGetPageRectSEL, (void*)CFR_ThumbnailView_V3::GetPageRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRThumbnailViewSEL, FRThumbnailViewGetVisiblePageRangeSEL, (void*)CFR_ThumbnailView_V3::GetVisiblePageRange);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRThumbnailViewSEL, FRThumbnailViewGetPDPageSEL, (void*)CFR_ThumbnailView_V3::GetPDPage);
	}
};

// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V4----------
// In file fs_basicImpl.h
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
class CFR_TabBand_V4_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTabBandSEL, FRTabBandGetSEL, (void*)CFR_TabBand_V4::Get);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTabBandSEL, FRTabBandGetTabWndSEL, (void*)CFR_TabBand_V4::GetTabWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTabBandSEL, FRTabBandGetActiveTabWndSEL, (void*)CFR_TabBand_V4::GetActiveTabWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTabBandSEL, FRTabBandGetTabsNumSEL, (void*)CFR_TabBand_V4::GetTabsNum);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTabBandSEL, FRTabBandCloseTabWndSEL, (void*)CFR_TabBand_V4::CloseTabWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTabBandSEL, FRTabBandGetActiveTabSEL, (void*)CFR_TabBand_V4::GetActiveTab);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTabBandSEL, FRTabBandSetActiveTabSEL, (void*)CFR_TabBand_V4::SetActiveTab);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTabBandSEL, FRTabBandRegisterAddBtnHandlerSEL, (void*)CFR_TabBand_V4::RegisterAddBtnHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTabBandSEL, FRTabBandSetTabTitleSEL, (void*)CFR_TabBand_V4::SetTabTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTabBandSEL, FRTabBandSetTabIconSEL, (void*)CFR_TabBand_V4::SetTabIcon);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRTabBandSEL, FRTabBandGet2SEL, (void*)CFR_TabBand_V4::Get2);
	}
};

// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V5----------
// In file fs_basicImpl.h
class CFS_UTF8Decoder_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUTF8DecoderSEL, FSUTF8DecoderNewSEL, (void*)CFS_UTF8Decoder_V5::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUTF8DecoderSEL, FSUTF8DecoderDestroySEL, (void*)CFS_UTF8Decoder_V5::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUTF8DecoderSEL, FSUTF8DecoderClearSEL, (void*)CFS_UTF8Decoder_V5::Clear);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUTF8DecoderSEL, FSUTF8DecoderInputSEL, (void*)CFS_UTF8Decoder_V5::Input);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUTF8DecoderSEL, FSUTF8DecoderAppendCharSEL, (void*)CFS_UTF8Decoder_V5::AppendChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUTF8DecoderSEL, FSUTF8DecoderClearStatusSEL, (void*)CFS_UTF8Decoder_V5::ClearStatus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUTF8DecoderSEL, FSUTF8DecoderGetResultSEL, (void*)CFS_UTF8Decoder_V5::GetResult);
	}
};

class CFS_UTF8Encoder_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUTF8EncoderSEL, FSUTF8EncoderNewSEL, (void*)CFS_UTF8Encoder_V5::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUTF8EncoderSEL, FSUTF8EncoderDestroySEL, (void*)CFS_UTF8Encoder_V5::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUTF8EncoderSEL, FSUTF8EncoderInputSEL, (void*)CFS_UTF8Encoder_V5::Input);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUTF8EncoderSEL, FSUTF8EncoderAppendStrSEL, (void*)CFS_UTF8Encoder_V5::AppendStr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUTF8EncoderSEL, FSUTF8EncoderGetResultSEL, (void*)CFS_UTF8Encoder_V5::GetResult);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUTF8EncoderSEL, FSUTF8EncoderIsUTF8DataSEL, (void*)CFS_UTF8Encoder_V5::IsUTF8Data);
	}
};

// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
class CFR_Internal_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalDoPrintCommentSummarySEL, (void*)CFR_Internal_V5::DoPrintCommentSummary);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalOptimizerModuleFlatDocumentSEL, (void*)CFR_Internal_V5::OptimizerModuleFlatDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGlobalWndOpenPassWordSEL, (void*)CFR_Internal_V5::GlobalWndOpenPassWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGlobalWndCheckFileSizeSEL, (void*)CFR_Internal_V5::GlobalWndCheckFileSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalOpenCertifiedPDFSEL, (void*)CFR_Internal_V5::OpenCertifiedPDF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndCreationParamNewSEL, (void*)CFR_Internal_V5::PWLWndCreationParamNew);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndCreationParamSetFontMapSEL, (void*)CFR_Internal_V5::PWLWndCreationParamSetFontMap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndCreationParamSetFontIndexSEL, (void*)CFR_Internal_V5::PWLWndCreationParamSetFontIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndCreationParamSetFlagSEL, (void*)CFR_Internal_V5::PWLWndCreationParamSetFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndCreationParamSetFontSizeSEL, (void*)CFR_Internal_V5::PWLWndCreationParamSetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndCreationParamSetTextColorSEL, (void*)CFR_Internal_V5::PWLWndCreationParamSetTextColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndCreationParamSetTextColorIISEL, (void*)CFR_Internal_V5::PWLWndCreationParamSetTextColorII);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndCreationParamSetRectSEL, (void*)CFR_Internal_V5::PWLWndCreationParamSetRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndCreationParamDestroySEL, (void*)CFR_Internal_V5::PWLWndCreationParamDestroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndEditNewSEL, (void*)CFR_Internal_V5::PWLWndEditNew);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndEditSetTextColorSEL, (void*)CFR_Internal_V5::PWLWndEditSetTextColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndEditSetTextColorIISEL, (void*)CFR_Internal_V5::PWLWndEditSetTextColorII);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndEditSetTextSEL, (void*)CFR_Internal_V5::PWLWndEditSetText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndEditGetContentRectSEL, (void*)CFR_Internal_V5::PWLWndEditGetContentRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndEditGeneratePageObjectsSEL, (void*)CFR_Internal_V5::PWLWndEditGeneratePageObjects);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndEditGeneratePageObjectsIISEL, (void*)CFR_Internal_V5::PWLWndEditGeneratePageObjectsII);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalPWLWndEditDestroySEL, (void*)CFR_Internal_V5::PWLWndEditDestroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSetReviewJSSEL, (void*)CFR_Internal_V5::SetReviewJS);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetReviewJSSEL, (void*)CFR_Internal_V5::GetReviewJS);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalRemoveReviewJSSEL, (void*)CFR_Internal_V5::RemoveReviewJS);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalExportAnnotToXFDFSEL, (void*)CFR_Internal_V5::ExportAnnotToXFDF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalImprotAnnotFromXFDFSEL, (void*)CFR_Internal_V5::ImprotAnnotFromXFDF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEnableAnnotSEL, (void*)CFR_Internal_V5::EnableAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalDeleteAnnotSEL, (void*)CFR_Internal_V5::DeleteAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalUpdateAnnotAuthorSEL, (void*)CFR_Internal_V5::UpdateAnnotAuthor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalUpdateAnnotAuthor2SEL, (void*)CFR_Internal_V5::UpdateAnnotAuthor2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSetIcommentsAnnotNotifySEL, (void*)CFR_Internal_V5::SetIcommentsAnnotNotify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalRemoveIcommentsAnnotNotifySEL, (void*)CFR_Internal_V5::RemoveIcommentsAnnotNotify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalShowChildScrollBarSEL, (void*)CFR_Internal_V5::ShowChildScrollBar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalChangeZoomScaleInfoSEL, (void*)CFR_Internal_V5::ChangeZoomScaleInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalAdvEditingSEL, (void*)CFR_Internal_V5::AdvEditing);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalToolbarModuleGetSysFontFaceNameSEL, (void*)CFR_Internal_V5::ToolbarModuleGetSysFontFaceName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPlaceNewSEL, (void*)CFR_Internal_V5::WordPlaceNew);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPlaceDestroySEL, (void*)CFR_Internal_V5::WordPlaceDestroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPlaceSetDefaultSEL, (void*)CFR_Internal_V5::WordPlaceSetDefault);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPlaceEqualSEL, (void*)CFR_Internal_V5::WordPlaceEqual);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPlaceWordCmpSEL, (void*)CFR_Internal_V5::WordPlaceWordCmp);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPlaceLineCmpSEL, (void*)CFR_Internal_V5::WordPlaceLineCmp);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPlaceSecCmpSEL, (void*)CFR_Internal_V5::WordPlaceSecCmp);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPlaceSetSectionIndexSEL, (void*)CFR_Internal_V5::WordPlaceSetSectionIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPlaceGetSectionIndexSEL, (void*)CFR_Internal_V5::WordPlaceGetSectionIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPlaceSetLineIndexSEL, (void*)CFR_Internal_V5::WordPlaceSetLineIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPlaceGetLineIndexSEL, (void*)CFR_Internal_V5::WordPlaceGetLineIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPlaceSetWordIndexSEL, (void*)CFR_Internal_V5::WordPlaceSetWordIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPlaceGetWordIndexSEL, (void*)CFR_Internal_V5::WordPlaceGetWordIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordRangeNewSEL, (void*)CFR_Internal_V5::WordRangeNew);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordRangeDestroySEL, (void*)CFR_Internal_V5::WordRangeDestroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordRangeSetDefaultSEL, (void*)CFR_Internal_V5::WordRangeSetDefault);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordRangeSetSEL, (void*)CFR_Internal_V5::WordRangeSet);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordRangeSetBeginPosSEL, (void*)CFR_Internal_V5::WordRangeSetBeginPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordRangeSetEndPosSEL, (void*)CFR_Internal_V5::WordRangeSetEndPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordRangeIsExistSEL, (void*)CFR_Internal_V5::WordRangeIsExist);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordRangeEqualSEL, (void*)CFR_Internal_V5::WordRangeEqual);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordRangeSwapWordPlaceSEL, (void*)CFR_Internal_V5::WordRangeSwapWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordRangeGetBeginPosSEL, (void*)CFR_Internal_V5::WordRangeGetBeginPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordRangeGetEndPosSEL, (void*)CFR_Internal_V5::WordRangeGetEndPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSecPropsNewSEL, (void*)CFR_Internal_V5::SecPropsNew);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSecPropsNewIISEL, (void*)CFR_Internal_V5::SecPropsNewII);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSecPropsDestroySEL, (void*)CFR_Internal_V5::SecPropsDestroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSecPropsSetLineLeadingSEL, (void*)CFR_Internal_V5::SecPropsSetLineLeading);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSecPropsGetLineLeadingSEL, (void*)CFR_Internal_V5::SecPropsGetLineLeading);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSecPropsSetLineIndentSEL, (void*)CFR_Internal_V5::SecPropsSetLineIndent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSecPropsGetLineIndentSEL, (void*)CFR_Internal_V5::SecPropsGetLineIndent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSecPropsSetAlignmentSEL, (void*)CFR_Internal_V5::SecPropsSetAlignment);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSecPropsGetAlignmentSEL, (void*)CFR_Internal_V5::SecPropsGetAlignment);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSecPropsSetSoftReturnSEL, (void*)CFR_Internal_V5::SecPropsSetSoftReturn);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSecPropsGetSoftReturnSEL, (void*)CFR_Internal_V5::SecPropsGetSoftReturn);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSecPropsSetRTLSEL, (void*)CFR_Internal_V5::SecPropsSetRTL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSecPropsGetRTLSEL, (void*)CFR_Internal_V5::SecPropsGetRTL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsNewSEL, (void*)CFR_Internal_V5::WordPropsNew);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsNewIISEL, (void*)CFR_Internal_V5::WordPropsNewII);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsDestroySEL, (void*)CFR_Internal_V5::WordPropsDestroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsSetFontIndexSEL, (void*)CFR_Internal_V5::WordPropsSetFontIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsGetFontIndexSEL, (void*)CFR_Internal_V5::WordPropsGetFontIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsSetFontSizeSEL, (void*)CFR_Internal_V5::WordPropsSetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsGetFontSizeSEL, (void*)CFR_Internal_V5::WordPropsGetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsSetWordColorSEL, (void*)CFR_Internal_V5::WordPropsSetWordColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsGetWordColorSEL, (void*)CFR_Internal_V5::WordPropsGetWordColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsSetStrokeColorSEL, (void*)CFR_Internal_V5::WordPropsSetStrokeColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsGetStrokeColorSEL, (void*)CFR_Internal_V5::WordPropsGetStrokeColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsSetScriptTypeSEL, (void*)CFR_Internal_V5::WordPropsSetScriptType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsGetScriptTypeSEL, (void*)CFR_Internal_V5::WordPropsGetScriptType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsSetWordStyleSEL, (void*)CFR_Internal_V5::WordPropsSetWordStyle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsGetWordStyleSEL, (void*)CFR_Internal_V5::WordPropsGetWordStyle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsSetCharSpaceSEL, (void*)CFR_Internal_V5::WordPropsSetCharSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsGetCharSpaceSEL, (void*)CFR_Internal_V5::WordPropsGetCharSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsSetHorzScaleSEL, (void*)CFR_Internal_V5::WordPropsSetHorzScale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsGetHorzScaleSEL, (void*)CFR_Internal_V5::WordPropsGetHorzScale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsSetXOffsetSEL, (void*)CFR_Internal_V5::WordPropsSetXOffset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsGetXOffsetSEL, (void*)CFR_Internal_V5::WordPropsGetXOffset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsSetYOffsetSEL, (void*)CFR_Internal_V5::WordPropsSetYOffset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsGetYOffsetSEL, (void*)CFR_Internal_V5::WordPropsGetYOffset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsSetRenderingModeSEL, (void*)CFR_Internal_V5::WordPropsSetRenderingMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsGetRenderingModeSEL, (void*)CFR_Internal_V5::WordPropsGetRenderingMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsSetClientDataSEL, (void*)CFR_Internal_V5::WordPropsSetClientData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsGetClientDataSEL, (void*)CFR_Internal_V5::WordPropsGetClientData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsSetLineWidthSEL, (void*)CFR_Internal_V5::WordPropsSetLineWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsGetLineWidthSEL, (void*)CFR_Internal_V5::WordPropsGetLineWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsSetWordSpaceSEL, (void*)CFR_Internal_V5::WordPropsSetWordSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsGetWordSpaceSEL, (void*)CFR_Internal_V5::WordPropsGetWordSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsSetCharCodeSEL, (void*)CFR_Internal_V5::WordPropsSetCharCode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsGetCharCodeSEL, (void*)CFR_Internal_V5::WordPropsGetCharCode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsSetAlphaSEL, (void*)CFR_Internal_V5::WordPropsSetAlpha);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordPropsGetAlphaSEL, (void*)CFR_Internal_V5::WordPropsGetAlpha);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordNewSEL, (void*)CFR_Internal_V5::WordNew);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordDestroySEL, (void*)CFR_Internal_V5::WordDestroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordSetWordSEL, (void*)CFR_Internal_V5::WordSetWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordGetWordSEL, (void*)CFR_Internal_V5::WordGetWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordSetWordDisplaySEL, (void*)CFR_Internal_V5::WordSetWordDisplay);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordGetWordDisplaySEL, (void*)CFR_Internal_V5::WordGetWordDisplay);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordSetBidiOrderSEL, (void*)CFR_Internal_V5::WordSetBidiOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordGetBidiOrderSEL, (void*)CFR_Internal_V5::WordGetBidiOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordSetCharsetSEL, (void*)CFR_Internal_V5::WordSetCharset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordGetCharsetSEL, (void*)CFR_Internal_V5::WordGetCharset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordSetWordPlaceSEL, (void*)CFR_Internal_V5::WordSetWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordGetWordPlaceSEL, (void*)CFR_Internal_V5::WordGetWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordSetWordPointSEL, (void*)CFR_Internal_V5::WordSetWordPoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordGetWordPointSEL, (void*)CFR_Internal_V5::WordGetWordPoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordSetAscentSEL, (void*)CFR_Internal_V5::WordSetAscent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordGetAscentSEL, (void*)CFR_Internal_V5::WordGetAscent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordSetDescentSEL, (void*)CFR_Internal_V5::WordSetDescent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordGetDescentSEL, (void*)CFR_Internal_V5::WordGetDescent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordSetWidthSEL, (void*)CFR_Internal_V5::WordSetWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordGetWidthSEL, (void*)CFR_Internal_V5::WordGetWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordSetFontIndexSEL, (void*)CFR_Internal_V5::WordSetFontIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordGetFontIndexSEL, (void*)CFR_Internal_V5::WordGetFontIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordSetFontSizeSEL, (void*)CFR_Internal_V5::WordSetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordGetFontSizeSEL, (void*)CFR_Internal_V5::WordGetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordSetWordPropsSEL, (void*)CFR_Internal_V5::WordSetWordProps);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWordGetWordPropsSEL, (void*)CFR_Internal_V5::WordGetWordProps);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalLineNewSEL, (void*)CFR_Internal_V5::LineNew);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalLineDestroySEL, (void*)CFR_Internal_V5::LineDestroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalLineSetStartWordPlaceSEL, (void*)CFR_Internal_V5::LineSetStartWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalLineGetStartWordPlaceSEL, (void*)CFR_Internal_V5::LineGetStartWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalLineSetEndWordPlaceSEL, (void*)CFR_Internal_V5::LineSetEndWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalLineGetEndWordPlaceSEL, (void*)CFR_Internal_V5::LineGetEndWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalLineSetOriginalPositionSEL, (void*)CFR_Internal_V5::LineSetOriginalPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalLineGetOriginalPositionSEL, (void*)CFR_Internal_V5::LineGetOriginalPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalLineSetWidthSEL, (void*)CFR_Internal_V5::LineSetWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalLineGetWidthSEL, (void*)CFR_Internal_V5::LineGetWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalLineSetAscentSEL, (void*)CFR_Internal_V5::LineSetAscent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalLineGetAscentSEL, (void*)CFR_Internal_V5::LineGetAscent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalLineSetDescentSEL, (void*)CFR_Internal_V5::LineSetDescent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalLineGetDescentSEL, (void*)CFR_Internal_V5::LineGetDescent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSectionNewSEL, (void*)CFR_Internal_V5::SectionNew);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSectionDestroySEL, (void*)CFR_Internal_V5::SectionDestroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSectionSetStartWordPlaceSEL, (void*)CFR_Internal_V5::SectionSetStartWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSectionGetStartWordPlaceSEL, (void*)CFR_Internal_V5::SectionGetStartWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSectionSetRectSEL, (void*)CFR_Internal_V5::SectionSetRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSectionGetRectSEL, (void*)CFR_Internal_V5::SectionGetRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSectionSetSectionPropertiesSEL, (void*)CFR_Internal_V5::SectionSetSectionProperties);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSectionGetSectionPropertiesSEL, (void*)CFR_Internal_V5::SectionGetSectionProperties);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSectionSetWordPropertiesSEL, (void*)CFR_Internal_V5::SectionSetWordProperties);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSectionGetWordPropertiesSEL, (void*)CFR_Internal_V5::SectionGetWordProperties);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVTIteratorNextWordSEL, (void*)CFR_Internal_V5::VTIteratorNextWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVTIteratorPrevWordSEL, (void*)CFR_Internal_V5::VTIteratorPrevWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVTIteratorNextLineSEL, (void*)CFR_Internal_V5::VTIteratorNextLine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVTIteratorPrevLineSEL, (void*)CFR_Internal_V5::VTIteratorPrevLine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVTIteratorNextSectionSEL, (void*)CFR_Internal_V5::VTIteratorNextSection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVTIteratorPrevSectionSEL, (void*)CFR_Internal_V5::VTIteratorPrevSection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVTIteratorGetWordSEL, (void*)CFR_Internal_V5::VTIteratorGetWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVTIteratorSetWordSEL, (void*)CFR_Internal_V5::VTIteratorSetWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVTIteratorGetLineSEL, (void*)CFR_Internal_V5::VTIteratorGetLine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVTIteratorGetSectionSEL, (void*)CFR_Internal_V5::VTIteratorGetSection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVTIteratorSetSectionSEL, (void*)CFR_Internal_V5::VTIteratorSetSection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVTIteratorSetAtSEL, (void*)CFR_Internal_V5::VTIteratorSetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVTIteratorSetAtIISEL, (void*)CFR_Internal_V5::VTIteratorSetAtII);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVTIteratorGetAtSEL, (void*)CFR_Internal_V5::VTIteratorGetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVTIteratorGetDisplayWordSEL, (void*)CFR_Internal_V5::VTIteratorGetDisplayWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextNewSEL, (void*)CFR_Internal_V5::VariableTextNew);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextDestroySEL, (void*)CFR_Internal_V5::VariableTextDestroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextProviderNewSEL, (void*)CFR_Internal_V5::VariableTextProviderNew);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextProviderDestroySEL, (void*)CFR_Internal_V5::VariableTextProviderDestroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetProviderSEL, (void*)CFR_Internal_V5::VariableTextSetProvider);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetIteratorSEL, (void*)CFR_Internal_V5::VariableTextGetIterator);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetPlateRectSEL, (void*)CFR_Internal_V5::VariableTextSetPlateRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextIsVerticalWritingSEL, (void*)CFR_Internal_V5::VariableTextIsVerticalWriting);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetAlignmentSEL, (void*)CFR_Internal_V5::VariableTextSetAlignment);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetPasswordCharSEL, (void*)CFR_Internal_V5::VariableTextSetPasswordChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetLimitCharSEL, (void*)CFR_Internal_V5::VariableTextSetLimitChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetCharArraySEL, (void*)CFR_Internal_V5::VariableTextSetCharArray);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetCharSpaceSEL, (void*)CFR_Internal_V5::VariableTextSetCharSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetWordSpaceSEL, (void*)CFR_Internal_V5::VariableTextSetWordSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetHorzScaleSEL, (void*)CFR_Internal_V5::VariableTextSetHorzScale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetMultiLineSEL, (void*)CFR_Internal_V5::VariableTextSetMultiLine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetAutoReturnSEL, (void*)CFR_Internal_V5::VariableTextSetAutoReturn);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetAutoFontSizeSEL, (void*)CFR_Internal_V5::VariableTextSetAutoFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetFontSizeSEL, (void*)CFR_Internal_V5::VariableTextSetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetLineLeadingSEL, (void*)CFR_Internal_V5::VariableTextSetLineLeading);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetRichTextSEL, (void*)CFR_Internal_V5::VariableTextSetRichText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetDirectionRTLSEL, (void*)CFR_Internal_V5::VariableTextSetDirectionRTL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextInitializeSEL, (void*)CFR_Internal_V5::VariableTextInitialize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextIsValidSEL, (void*)CFR_Internal_V5::VariableTextIsValid);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextIsRichTextSEL, (void*)CFR_Internal_V5::VariableTextIsRichText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextRearrangeAllSEL, (void*)CFR_Internal_V5::VariableTextRearrangeAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextRearrangePartSEL, (void*)CFR_Internal_V5::VariableTextRearrangePart);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextResetAllSEL, (void*)CFR_Internal_V5::VariableTextResetAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSetTextSEL, (void*)CFR_Internal_V5::VariableTextSetText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextInsertWordSEL, (void*)CFR_Internal_V5::VariableTextInsertWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextInsertSectionSEL, (void*)CFR_Internal_V5::VariableTextInsertSection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextInsertTextSEL, (void*)CFR_Internal_V5::VariableTextInsertText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextDeleteWordsSEL, (void*)CFR_Internal_V5::VariableTextDeleteWords);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextDeleteWordSEL, (void*)CFR_Internal_V5::VariableTextDeleteWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextBackSpaceWordSEL, (void*)CFR_Internal_V5::VariableTextBackSpaceWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetPlateRectSEL, (void*)CFR_Internal_V5::VariableTextGetPlateRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetContentRectSEL, (void*)CFR_Internal_V5::VariableTextGetContentRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetTotalWordsSEL, (void*)CFR_Internal_V5::VariableTextGetTotalWords);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetFontSizeSEL, (void*)CFR_Internal_V5::VariableTextGetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetAlignmentSEL, (void*)CFR_Internal_V5::VariableTextGetAlignment);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetWordSpaceSEL, (void*)CFR_Internal_V5::VariableTextGetWordSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetPasswordCharSEL, (void*)CFR_Internal_V5::VariableTextGetPasswordChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetCharArraySEL, (void*)CFR_Internal_V5::VariableTextGetCharArray);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetLimitCharSEL, (void*)CFR_Internal_V5::VariableTextGetLimitChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextIsMultiLineSEL, (void*)CFR_Internal_V5::VariableTextIsMultiLine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextIsAutoReturnSEL, (void*)CFR_Internal_V5::VariableTextIsAutoReturn);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetHorzScaleSEL, (void*)CFR_Internal_V5::VariableTextGetHorzScale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetCharSpaceSEL, (void*)CFR_Internal_V5::VariableTextGetCharSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetBeginWordPlaceSEL, (void*)CFR_Internal_V5::VariableTextGetBeginWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetEndWordPlaceSEL, (void*)CFR_Internal_V5::VariableTextGetEndWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetPrevWordPlaceSEL, (void*)CFR_Internal_V5::VariableTextGetPrevWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetNextWordPlaceSEL, (void*)CFR_Internal_V5::VariableTextGetNextWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextSearchWordPlaceSEL, (void*)CFR_Internal_V5::VariableTextSearchWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetUpWordPlaceSEL, (void*)CFR_Internal_V5::VariableTextGetUpWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetDownWordPlaceSEL, (void*)CFR_Internal_V5::VariableTextGetDownWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetLineBeginPlaceSEL, (void*)CFR_Internal_V5::VariableTextGetLineBeginPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetLineEndPlaceSEL, (void*)CFR_Internal_V5::VariableTextGetLineEndPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetSectionBeginPlaceSEL, (void*)CFR_Internal_V5::VariableTextGetSectionBeginPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextGetSectionEndPlaceSEL, (void*)CFR_Internal_V5::VariableTextGetSectionEndPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextUpdateWordPlaceSEL, (void*)CFR_Internal_V5::VariableTextUpdateWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextAjustLineHeaderSEL, (void*)CFR_Internal_V5::VariableTextAjustLineHeader);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextWordPlaceToWordIndexSEL, (void*)CFR_Internal_V5::VariableTextWordPlaceToWordIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextWordIndexToWordPlaceSEL, (void*)CFR_Internal_V5::VariableTextWordIndexToWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextIsSectionComplexScriptSEL, (void*)CFR_Internal_V5::VariableTextIsSectionComplexScript);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalVariableTextIsVerticalFontSEL, (void*)CFR_Internal_V5::VariableTextIsVerticalFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapGetDocFontMapSEL, (void*)CFR_Internal_V5::EditFontMapGetDocFontMap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapEncodeFontAliasSEL, (void*)CFR_Internal_V5::EditFontMapEncodeFontAlias);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapGetFontNameSEL, (void*)CFR_Internal_V5::EditFontMapGetFontName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapGetCharsetSEL, (void*)CFR_Internal_V5::EditFontMapGetCharset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapGetFXFontSEL, (void*)CFR_Internal_V5::EditFontMapGetFXFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapGetPDFFontSEL, (void*)CFR_Internal_V5::EditFontMapGetPDFFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapGetCharWidthFSEL, (void*)CFR_Internal_V5::EditFontMapGetCharWidthF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapCharCodeFromUnicodeSEL, (void*)CFR_Internal_V5::EditFontMapCharCodeFromUnicode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapGlyphFromCharCodeSEL, (void*)CFR_Internal_V5::EditFontMapGlyphFromCharCode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapCharSetFromUnicodeSEL, (void*)CFR_Internal_V5::EditFontMapCharSetFromUnicode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapGetFontIndexByUnicodeSEL, (void*)CFR_Internal_V5::EditFontMapGetFontIndexByUnicode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapGetFontIndexSEL, (void*)CFR_Internal_V5::EditFontMapGetFontIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalIsEmbeddedSEL, (void*)CFR_Internal_V5::IsEmbedded);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSetFXFMEditFontSEL, (void*)CFR_Internal_V5::SetFXFMEditFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGlyphFromCharCode2SEL, (void*)CFR_Internal_V5::GlyphFromCharCode2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapAddFXFontSEL, (void*)CFR_Internal_V5::EditFontMapAddFXFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapAddPDFFontSEL, (void*)CFR_Internal_V5::EditFontMapAddPDFFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapGetDocumentSEL, (void*)CFR_Internal_V5::EditFontMapGetDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapLoadAllFontByDictSEL, (void*)CFR_Internal_V5::EditFontMapLoadAllFontByDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapCheckCharacterIsSupportedSEL, (void*)CFR_Internal_V5::EditFontMapCheckCharacterIsSupported);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapGetStandardFontSEL, (void*)CFR_Internal_V5::EditFontMapGetStandardFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapIsExternalEmbedFontSEL, (void*)CFR_Internal_V5::EditFontMapIsExternalEmbedFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIteratorNextWordSEL, (void*)CFR_Internal_V5::EditIteratorNextWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIteratorNextLineSEL, (void*)CFR_Internal_V5::EditIteratorNextLine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIteratorNextSectionSEL, (void*)CFR_Internal_V5::EditIteratorNextSection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIteratorPrevWordSEL, (void*)CFR_Internal_V5::EditIteratorPrevWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIteratorPrevLineSEL, (void*)CFR_Internal_V5::EditIteratorPrevLine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIteratorPrevSectionSEL, (void*)CFR_Internal_V5::EditIteratorPrevSection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIteratorGetWordSEL, (void*)CFR_Internal_V5::EditIteratorGetWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIteratorGetDisplayWordSEL, (void*)CFR_Internal_V5::EditIteratorGetDisplayWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIteratorGetLineSEL, (void*)CFR_Internal_V5::EditIteratorGetLine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIteratorGetSectionSEL, (void*)CFR_Internal_V5::EditIteratorGetSection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIteratorSetAtSEL, (void*)CFR_Internal_V5::EditIteratorSetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIteratorSetAtIISEL, (void*)CFR_Internal_V5::EditIteratorSetAtII);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIteratorGetAtSEL, (void*)CFR_Internal_V5::EditIteratorGetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIteratorGetEditSEL, (void*)CFR_Internal_V5::EditIteratorGetEdit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditNewSEL, (void*)CFR_Internal_V5::EditNew);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditDelSEL, (void*)CFR_Internal_V5::EditDel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetFontMapSEL, (void*)CFR_Internal_V5::EditSetFontMap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetVTProviderSEL, (void*)CFR_Internal_V5::EditSetVTProvider);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetNotifySEL, (void*)CFR_Internal_V5::EditSetNotify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetOprNotifySEL, (void*)CFR_Internal_V5::EditSetOprNotify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetIteratorSEL, (void*)CFR_Internal_V5::EditGetIterator);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetVariableTextSEL, (void*)CFR_Internal_V5::EditGetVariableText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetFontMapSEL, (void*)CFR_Internal_V5::EditGetFontMap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditInitializeSEL, (void*)CFR_Internal_V5::EditInitialize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetPlateRectSEL, (void*)CFR_Internal_V5::EditSetPlateRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetScrollPosSEL, (void*)CFR_Internal_V5::EditSetScrollPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIsVerticalWritingSEL, (void*)CFR_Internal_V5::EditIsVerticalWriting);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetAlignmentHSEL, (void*)CFR_Internal_V5::EditSetAlignmentH);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetAlignmentVSEL, (void*)CFR_Internal_V5::EditSetAlignmentV);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetPasswordCharSEL, (void*)CFR_Internal_V5::EditSetPasswordChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetLimitCharSEL, (void*)CFR_Internal_V5::EditSetLimitChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetCharArraySEL, (void*)CFR_Internal_V5::EditSetCharArray);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetCharSpaceSEL, (void*)CFR_Internal_V5::EditSetCharSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetWordSpaceSEL, (void*)CFR_Internal_V5::EditSetWordSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetHorzScaleSEL, (void*)CFR_Internal_V5::EditSetHorzScale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetLineLeadingSEL, (void*)CFR_Internal_V5::EditSetLineLeading);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetMultiLineSEL, (void*)CFR_Internal_V5::EditSetMultiLine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetAutoReturnSEL, (void*)CFR_Internal_V5::EditSetAutoReturn);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetAutoFontSizeSEL, (void*)CFR_Internal_V5::EditSetAutoFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetAutoScrollSEL, (void*)CFR_Internal_V5::EditSetAutoScroll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetFontSizeSEL, (void*)CFR_Internal_V5::EditSetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetTextOverflowSEL, (void*)CFR_Internal_V5::EditSetTextOverflow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSetDirectionRTLSEL, (void*)CFR_Internal_V5::SetDirectionRTL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIsRichTextSEL, (void*)CFR_Internal_V5::EditIsRichText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichTextSEL, (void*)CFR_Internal_V5::EditSetRichText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichTextFontSEL, (void*)CFR_Internal_V5::EditSetRichTextFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSetRichTextIsBoldSEL, (void*)CFR_Internal_V5::SetRichTextIsBold);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSetRichTextIsItalicSEL, (void*)CFR_Internal_V5::SetRichTextIsItalic);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichFontSizeSEL, (void*)CFR_Internal_V5::EditSetRichFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichTextColorSEL, (void*)CFR_Internal_V5::EditSetRichTextColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichTextStrokeColorSEL, (void*)CFR_Internal_V5::EditSetRichTextStrokeColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichRenderingModeSEL, (void*)CFR_Internal_V5::EditSetRichRenderingMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichTextScriptSEL, (void*)CFR_Internal_V5::EditSetRichTextScript);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichTextUnderlineSEL, (void*)CFR_Internal_V5::EditSetRichTextUnderline);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichTextCrossoutSEL, (void*)CFR_Internal_V5::EditSetRichTextCrossout);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichTextCharSpaceSEL, (void*)CFR_Internal_V5::EditSetRichTextCharSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichTextWordSpaceSEL, (void*)CFR_Internal_V5::EditSetRichTextWordSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichTextHorzScaleSEL, (void*)CFR_Internal_V5::EditSetRichTextHorzScale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichTextLineLeadingSEL, (void*)CFR_Internal_V5::EditSetRichTextLineLeading);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichTextLineIndentSEL, (void*)CFR_Internal_V5::EditSetRichTextLineIndent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichTextAlignmentSEL, (void*)CFR_Internal_V5::EditSetRichTextAlignment);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichTextSoftReturnSEL, (void*)CFR_Internal_V5::EditSetRichTextSoftReturn);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetSelSEL, (void*)CFR_Internal_V5::EditSetSel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetSelSEL, (void*)CFR_Internal_V5::EditGetSel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSelectAllSEL, (void*)CFR_Internal_V5::EditSelectAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSelectNoneSEL, (void*)CFR_Internal_V5::EditSelectNone);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetCaretSEL, (void*)CFR_Internal_V5::EditGetCaret);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetCaretWordPlaceSEL, (void*)CFR_Internal_V5::EditGetCaretWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetSelTextSEL, (void*)CFR_Internal_V5::EditGetSelText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetTextSEL, (void*)CFR_Internal_V5::EditGetText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetRichTextXMLSEL, (void*)CFR_Internal_V5::EditGetRichTextXML);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIsSelectedSEL, (void*)CFR_Internal_V5::EditIsSelected);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetScrollPosSEL, (void*)CFR_Internal_V5::EditGetScrollPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetPlateRectSEL, (void*)CFR_Internal_V5::EditGetPlateRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetContentRectSEL, (void*)CFR_Internal_V5::EditGetContentRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetVisibleWordRangeSEL, (void*)CFR_Internal_V5::EditGetVisibleWordRange);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetWholeWordRangeSEL, (void*)CFR_Internal_V5::EditGetWholeWordRange);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetSelectWordRangeSEL, (void*)CFR_Internal_V5::EditGetSelectWordRange);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditOnMouseDownSEL, (void*)CFR_Internal_V5::EditOnMouseDown);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditOnMouseMoveSEL, (void*)CFR_Internal_V5::EditOnMouseMove);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditOnVK_UPSEL, (void*)CFR_Internal_V5::EditOnVK_UP);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditOnVK_DOWNSEL, (void*)CFR_Internal_V5::EditOnVK_DOWN);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditOnVK_LEFTSEL, (void*)CFR_Internal_V5::EditOnVK_LEFT);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditOnVK_RIGHTSEL, (void*)CFR_Internal_V5::EditOnVK_RIGHT);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditOnVK_HOMESEL, (void*)CFR_Internal_V5::EditOnVK_HOME);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditOnVK_ENDSEL, (void*)CFR_Internal_V5::EditOnVK_END);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetTextSEL, (void*)CFR_Internal_V5::EditSetText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichTextByXMLSEL, (void*)CFR_Internal_V5::EditSetRichTextByXML);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditInsertWordSEL, (void*)CFR_Internal_V5::EditInsertWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditInsertReturnSEL, (void*)CFR_Internal_V5::EditInsertReturn);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditInsertTextSEL, (void*)CFR_Internal_V5::EditInsertText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditBackspaceSEL, (void*)CFR_Internal_V5::EditBackspace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditDeleteSEL, (void*)CFR_Internal_V5::EditDelete);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditClearSEL, (void*)CFR_Internal_V5::EditClear);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditRedoSEL, (void*)CFR_Internal_V5::EditRedo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditUndoSEL, (void*)CFR_Internal_V5::EditUndo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetCaretSEL, (void*)CFR_Internal_V5::EditSetCaret);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditPaintSEL, (void*)CFR_Internal_V5::EditPaint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditEnableRefreshSEL, (void*)CFR_Internal_V5::EditEnableRefresh);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditRefreshWordRangeSEL, (void*)CFR_Internal_V5::EditRefreshWordRange);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditEnableUndoSEL, (void*)CFR_Internal_V5::EditEnableUndo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditEnableNotifySEL, (void*)CFR_Internal_V5::EditEnableNotify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditEnableOprNotifySEL, (void*)CFR_Internal_V5::EditEnableOprNotify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditWordPlaceToWordIndexSEL, (void*)CFR_Internal_V5::EditWordPlaceToWordIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditWordIndexToWordPlaceSEL, (void*)CFR_Internal_V5::EditWordIndexToWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetLineBeginPlaceSEL, (void*)CFR_Internal_V5::EditGetLineBeginPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetLineEndPlaceSEL, (void*)CFR_Internal_V5::EditGetLineEndPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetSectionBeginPlaceSEL, (void*)CFR_Internal_V5::EditGetSectionBeginPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetSectionEndPlaceSEL, (void*)CFR_Internal_V5::EditGetSectionEndPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSearchWordPlaceSEL, (void*)CFR_Internal_V5::EditSearchWordPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetFontSizeSEL, (void*)CFR_Internal_V5::EditGetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetPasswordCharSEL, (void*)CFR_Internal_V5::EditGetPasswordChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetCharArraySEL, (void*)CFR_Internal_V5::EditGetCharArray);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetHorzScaleSEL, (void*)CFR_Internal_V5::EditGetHorzScale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetCharSpaceSEL, (void*)CFR_Internal_V5::EditGetCharSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetRangeTextSEL, (void*)CFR_Internal_V5::EditGetRangeText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIsTextFullSEL, (void*)CFR_Internal_V5::EditIsTextFull);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditCanUndoSEL, (void*)CFR_Internal_V5::EditCanUndo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditCanRedoSEL, (void*)CFR_Internal_V5::EditCanRedo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditResetUndoSEL, (void*)CFR_Internal_V5::EditResetUndo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIsModifiedSEL, (void*)CFR_Internal_V5::EditIsModified);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetTotalWordsSEL, (void*)CFR_Internal_V5::EditGetTotalWords);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditAddUndoItemSEL, (void*)CFR_Internal_V5::EditAddUndoItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalBeginGroupUndoSEL, (void*)CFR_Internal_V5::BeginGroupUndo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEndGroupUndoSEL, (void*)CFR_Internal_V5::EndGroupUndo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSaveLineOffsetSEL, (void*)CFR_Internal_V5::EditSaveLineOffset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetEditAppearanceStreamSEL, (void*)CFR_Internal_V5::EditGetEditAppearanceStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetRichEditAppearanceStreamSEL, (void*)CFR_Internal_V5::EditGetRichEditAppearanceStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetSelectAppearanceStreamSEL, (void*)CFR_Internal_V5::EditGetSelectAppearanceStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditDrawEditSEL, (void*)CFR_Internal_V5::EditDrawEdit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditDrawUnderlineSEL, (void*)CFR_Internal_V5::EditDrawUnderline);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditDrawRichEditSEL, (void*)CFR_Internal_V5::EditDrawRichEdit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGeneratePageObjectsSEL, (void*)CFR_Internal_V5::EditGeneratePageObjects);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGenerateRichPageObjectsSEL, (void*)CFR_Internal_V5::EditGenerateRichPageObjects);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGenerateUnderlineObjectsSEL, (void*)CFR_Internal_V5::EditGenerateUnderlineObjects);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetRichTextPlainTextSEL, (void*)CFR_Internal_V5::EditGetRichTextPlainText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetTouchupSEL, (void*)CFR_Internal_V5::GetTouchup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalBeginEditTextObjectSEL, (void*)CFR_Internal_V5::BeginEditTextObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEndEditTextObjectSEL, (void*)CFR_Internal_V5::EndEditTextObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEmailUtilSendMailUseConfigSEL, (void*)CFR_Internal_V5::EmailUtilSendMailUseConfig);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEmailUtilShowAddressBookSEL, (void*)CFR_Internal_V5::EmailUtilShowAddressBook);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalIsKeyAndTrialValidSEL, (void*)CFR_Internal_V5::IsKeyAndTrialValid);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalCheckLicenseSEL, (void*)CFR_Internal_V5::CheckLicense);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIsPunctuationSEL, (void*)CFR_Internal_V5::EditIsPunctuation);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIsCJKSEL, (void*)CFR_Internal_V5::EditIsCJK);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIsLatinSEL, (void*)CFR_Internal_V5::EditIsLatin);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditIsNeedToConvertSpaceSEL, (void*)CFR_Internal_V5::EditIsNeedToConvertSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetCustomStringSEL, (void*)CFR_Internal_V5::GetCustomString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditDestroyNotifySEL, (void*)CFR_Internal_V5::EditDestroyNotify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditDestroyOprNotifySEL, (void*)CFR_Internal_V5::EditDestroyOprNotify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSetDocReviewIdentitySEL, (void*)CFR_Internal_V5::SetDocReviewIdentity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetDocReviewIdentityNameSEL, (void*)CFR_Internal_V5::GetDocReviewIdentityName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetDocReviewIdentityEmailSEL, (void*)CFR_Internal_V5::GetDocReviewIdentityEmail);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalHasVisibleTextObjSEL, (void*)CFR_Internal_V5::HasVisibleTextObj);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetProfStoreManagerSEL, (void*)CFR_Internal_V5::GetProfStoreManager);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalProfStoreGetStringSEL, (void*)CFR_Internal_V5::ProfStoreGetString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalProfStoreGetIntSEL, (void*)CFR_Internal_V5::ProfStoreGetInt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalProfStoreGetBooleanSEL, (void*)CFR_Internal_V5::ProfStoreGetBoolean);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalProfStoreEnumAllKeyNameSEL, (void*)CFR_Internal_V5::ProfStoreEnumAllKeyName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalProfStoreEnumKeyNameSEL, (void*)CFR_Internal_V5::ProfStoreEnumKeyName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalProfStoreEnumKeyValueNameSEL, (void*)CFR_Internal_V5::ProfStoreEnumKeyValueName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalProfStoreEnumKeyValueSEL, (void*)CFR_Internal_V5::ProfStoreEnumKeyValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGWL10NHelperAutoResizeDropWidthSEL, (void*)CFR_Internal_V5::GWL10NHelperAutoResizeDropWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGWL10NHelperResetItemDropWidthSEL, (void*)CFR_Internal_V5::GWL10NHelperResetItemDropWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetActivePopupMenuMessageWndSEL, (void*)CFR_Internal_V5::GetActivePopupMenuMessageWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetPhoneNumberSEL, (void*)CFR_Internal_V5::GetPhoneNumber);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetEditionTitleSEL, (void*)CFR_Internal_V5::GetEditionTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetEmailSEL, (void*)CFR_Internal_V5::GetEmail);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditGetWordLatinSmallLigatureSEL, (void*)CFR_Internal_V5::EditGetWordLatinSmallLigature);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetClonedDocSEL, (void*)CFR_Internal_V5::GetClonedDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapReleaseDocFontMapSEL, (void*)CFR_Internal_V5::EditFontMapReleaseDocFontMap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalCreateActionWizardHandlerMgrSEL, (void*)CFR_Internal_V5::CreateActionWizardHandlerMgr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalDestroyActionWizardHandlerMgrSEL, (void*)CFR_Internal_V5::DestroyActionWizardHandlerMgr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSetActionWizardHandlerMgrSEL, (void*)CFR_Internal_V5::SetActionWizardHandlerMgr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetActionWizardHandlerMgrSEL, (void*)CFR_Internal_V5::GetActionWizardHandlerMgr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalCreateActionWizardHandlerSEL, (void*)CFR_Internal_V5::CreateActionWizardHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalDestroyActionWizardHandlerSEL, (void*)CFR_Internal_V5::DestroyActionWizardHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalRegisterActionWizardHandlerSEL, (void*)CFR_Internal_V5::RegisterActionWizardHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalUnRegisterActionWizardHandlerSEL, (void*)CFR_Internal_V5::UnRegisterActionWizardHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardHandlerGetNameSEL, (void*)CFR_Internal_V5::ActionWizardHandlerGetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardHandlerGetCommandNameSEL, (void*)CFR_Internal_V5::ActionWizardHandlerGetCommandName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardHandlerGetPresetFlagSEL, (void*)CFR_Internal_V5::ActionWizardHandlerGetPresetFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardHandlerSettingSEL, (void*)CFR_Internal_V5::ActionWizardHandlerSetting);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardHandlerExecuteSEL, (void*)CFR_Internal_V5::ActionWizardHandlerExecute);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardHandlerGetTitleSEL, (void*)CFR_Internal_V5::ActionWizardHandlerGetTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardHandlerNeedReopenSEL, (void*)CFR_Internal_V5::ActionWizardHandlerNeedReopen);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardHandlerNeedChangeExecuteDocSEL, (void*)CFR_Internal_V5::ActionWizardHandlerNeedChangeExecuteDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalExportAnnotToXFDF2SEL, (void*)CFR_Internal_V5::ExportAnnotToXFDF2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalImprotAnnotFromXFDF2SEL, (void*)CFR_Internal_V5::ImprotAnnotFromXFDF2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSubscribeShowRibbonUISEL, (void*)CFR_Internal_V5::SubscribeShowRibbonUI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSubscribeStartWorkflowSEL, (void*)CFR_Internal_V5::SubscribeStartWorkflow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSubscribeIsLicenseRevokedSEL, (void*)CFR_Internal_V5::SubscribeIsLicenseRevoked);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSubscribeShowFlashSEL, (void*)CFR_Internal_V5::SubscribeShowFlash);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalCPDFPluginProviderSetSEL, (void*)CFR_Internal_V5::CPDFPluginProviderSet);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalCPDFPluginProviderDestroySEL, (void*)CFR_Internal_V5::CPDFPluginProviderDestroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalCPDFPluginProviderGetSEL, (void*)CFR_Internal_V5::CPDFPluginProviderGet);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalCPDFPluginProviderShowConnectedPDFAdDialogSEL, (void*)CFR_Internal_V5::CPDFPluginProviderShowConnectedPDFAdDialog);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetRemainDaysSEL, (void*)CFR_Internal_V5::GetRemainDays);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalCommentsAnnotSetUpdateServerTimeSEL, (void*)CFR_Internal_V5::CommentsAnnotSetUpdateServerTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetLicenseSNSEL, (void*)CFR_Internal_V5::GetLicenseSN);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetKeyfileTypeSEL, (void*)CFR_Internal_V5::GetKeyfileType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetFormatSymbolStringSEL, (void*)CFR_Internal_V5::GetFormatSymbolString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetLocalFormatTimeStringSEL, (void*)CFR_Internal_V5::GetLocalFormatTimeString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalConvertToPDFGetFileTypeSEL, (void*)CFR_Internal_V5::ConvertToPDFGetFileType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetFileFilterSEL, (void*)CFR_Internal_V5::GetFileFilter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalRegisterDocHandlerOfPDDocForActionWizardSEL, (void*)CFR_Internal_V5::RegisterDocHandlerOfPDDocForActionWizard);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalCommentsAnnotSetIconStateSEL, (void*)CFR_Internal_V5::CommentsAnnotSetIconState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetAnnotInputFontNameSEL, (void*)CFR_Internal_V5::GetAnnotInputFontName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalIsDisableCPDFSEL, (void*)CFR_Internal_V5::IsDisableCPDF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalRunJSPluginByFlagSEL, (void*)CFR_Internal_V5::RunJSPluginByFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalRunJSPluginByNameSEL, (void*)CFR_Internal_V5::RunJSPluginByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalIsJSPanelActivatedSEL, (void*)CFR_Internal_V5::IsJSPanelActivated);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalExpandJSPanelSEL, (void*)CFR_Internal_V5::ExpandJSPanel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetDrawNotifySEL, (void*)CFR_Internal_V5::EditSetDrawNotify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditDestroyDrawNotifySEL, (void*)CFR_Internal_V5::EditDestroyDrawNotify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEmailUtilRegisterPreSendMailHandlerSEL, (void*)CFR_Internal_V5::EmailUtilRegisterPreSendMailHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetCurEditonTitleSEL, (void*)CFR_Internal_V5::GetCurEditonTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalIsDisableInternetSEL, (void*)CFR_Internal_V5::IsDisableInternet);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalIsUseSystemSelectionColorSEL, (void*)CFR_Internal_V5::IsUseSystemSelectionColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetSelBkColorSEL, (void*)CFR_Internal_V5::EditSetSelBkColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditDrawRichEdit2SEL, (void*)CFR_Internal_V5::EditDrawRichEdit2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalIsJSPluginDisabledSEL, (void*)CFR_Internal_V5::IsJSPluginDisabled);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetAccReadingOrderModeSEL, (void*)CFR_Internal_V5::GetAccReadingOrderMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalIsAccOverrideReadingOrderSEL, (void*)CFR_Internal_V5::IsAccOverrideReadingOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetDocAccReadingOrderSEL, (void*)CFR_Internal_V5::GetDocAccReadingOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetDocAccReadingModeSEL, (void*)CFR_Internal_V5::GetDocAccReadingMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSetReadActivateStatusSEL, (void*)CFR_Internal_V5::SetReadActivateStatus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetReadActivateStatusSEL, (void*)CFR_Internal_V5::GetReadActivateStatus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWIPPluginProviderSetSEL, (void*)CFR_Internal_V5::WIPPluginProviderSet);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWIPPluginProviderDestroySEL, (void*)CFR_Internal_V5::WIPPluginProviderDestroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWIPPluginProviderGetSEL, (void*)CFR_Internal_V5::WIPPluginProviderGet);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalWIPPluginProviderAllowedPasteSEL, (void*)CFR_Internal_V5::WIPPluginProviderAllowedPaste);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalThemeCheckHCMTSEL, (void*)CFR_Internal_V5::ThemeCheckHCMT);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalThemeGetTextColorSEL, (void*)CFR_Internal_V5::ThemeGetTextColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalThemeGetDisableTextColorSEL, (void*)CFR_Internal_V5::ThemeGetDisableTextColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalThemeGetHyperlinkTextColorSEL, (void*)CFR_Internal_V5::ThemeGetHyperlinkTextColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalThemeGetSelectedTextColorSEL, (void*)CFR_Internal_V5::ThemeGetSelectedTextColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalThemeGetButtonTextColorSEL, (void*)CFR_Internal_V5::ThemeGetButtonTextColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalThemeGetTooltipColorSEL, (void*)CFR_Internal_V5::ThemeGetTooltipColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalThemeGetBackgroundColorSEL, (void*)CFR_Internal_V5::ThemeGetBackgroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEmailUtilRegisterAttachmentNameModifierSEL, (void*)CFR_Internal_V5::EmailUtilRegisterAttachmentNameModifier);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetAppSaveAsLocationSEL, (void*)CFR_Internal_V5::GetAppSaveAsLocation);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalLoadJSPluginSEL, (void*)CFR_Internal_V5::LoadJSPlugin);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardHandlerIsEnableExecuteSEL, (void*)CFR_Internal_V5::ActionWizardHandlerIsEnableExecute);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardHandlerExecuteDoneSEL, (void*)CFR_Internal_V5::ActionWizardHandlerExecuteDone);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapGetFontIndexByUnicode2SEL, (void*)CFR_Internal_V5::EditFontMapGetFontIndexByUnicode2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapGetFontIndex2SEL, (void*)CFR_Internal_V5::EditFontMapGetFontIndex2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalCompareGetDifInfosSEL, (void*)CFR_Internal_V5::CompareGetDifInfos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalCompareReleaseDifInfosSEL, (void*)CFR_Internal_V5::CompareReleaseDifInfos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalCompareDrawNoteAPSEL, (void*)CFR_Internal_V5::CompareDrawNoteAP);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetCreationDateSEL, (void*)CFR_Internal_V5::GetCreationDate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSetReadStatusSEL, (void*)CFR_Internal_V5::SetReadStatus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetReadStatusSEL, (void*)CFR_Internal_V5::GetReadStatus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalCheckActualLicenseSEL, (void*)CFR_Internal_V5::CheckActualLicense);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalOpenCreatedPDFDocSEL, (void*)CFR_Internal_V5::OpenCreatedPDFDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetIsPheeModeSEL, (void*)CFR_Internal_V5::GetIsPheeMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetEastenArabicNumeralModeSEL, (void*)CFR_Internal_V5::EditSetEastenArabicNumeralMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardLogCreateSEL, (void*)CFR_Internal_V5::ActionWizardLogCreate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardLogWriteDetailInfoSEL, (void*)CFR_Internal_V5::ActionWizardLogWriteDetailInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardLogWriteCurrentTimeSEL, (void*)CFR_Internal_V5::ActionWizardLogWriteCurrentTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardLogWriteBoundaryInfoSEL, (void*)CFR_Internal_V5::ActionWizardLogWriteBoundaryInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardLogGenerateSEL, (void*)CFR_Internal_V5::ActionWizardLogGenerate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardLogCloseSEL, (void*)CFR_Internal_V5::ActionWizardLogClose);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardLogWriteSEL, (void*)CFR_Internal_V5::ActionWizardLogWrite);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardLogGetStateSEL, (void*)CFR_Internal_V5::ActionWizardLogGetState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardLogGetLastFilePathSEL, (void*)CFR_Internal_V5::ActionWizardLogGetLastFilePath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardLogSetCurFilePathSEL, (void*)CFR_Internal_V5::ActionWizardLogSetCurFilePath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalActionWizardLogSetExcuteFileStatusSEL, (void*)CFR_Internal_V5::ActionWizardLogSetExcuteFileStatus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetUpgExpDateSEL, (void*)CFR_Internal_V5::GetUpgExpDate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalIsUsedAnotherPerpetualLicenseSEL, (void*)CFR_Internal_V5::IsUsedAnotherPerpetualLicense);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalBaseAnnoteIsSnap2GridEnabledSEL, (void*)CFR_Internal_V5::BaseAnnoteIsSnap2GridEnabled);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalBaseAnnoteGetGridLineSEL, (void*)CFR_Internal_V5::BaseAnnoteGetGridLine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalBaseAnnoteGetGridMainLineSEL, (void*)CFR_Internal_V5::BaseAnnoteGetGridMainLine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalAddDocToPrivilegedListSEL, (void*)CFR_Internal_V5::AddDocToPrivilegedList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalIsPrivilegedFileSEL, (void*)CFR_Internal_V5::IsPrivilegedFile);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalIsDisableInternet2SEL, (void*)CFR_Internal_V5::IsDisableInternet2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalIsSimpleChineseVersionSEL, (void*)CFR_Internal_V5::IsSimpleChineseVersion);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapGetFontIndexByUnicode3SEL, (void*)CFR_Internal_V5::EditFontMapGetFontIndexByUnicode3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapGetFontIndex3SEL, (void*)CFR_Internal_V5::EditFontMapGetFontIndex3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapAddFXFont2SEL, (void*)CFR_Internal_V5::EditFontMapAddFXFont2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapAddPDFFont2SEL, (void*)CFR_Internal_V5::EditFontMapAddPDFFont2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetRichTextFont2SEL, (void*)CFR_Internal_V5::EditSetRichTextFont2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditFontMapGetFontName2SEL, (void*)CFR_Internal_V5::EditFontMapGetFontName2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalClearDragRectsSEL, (void*)CFR_Internal_V5::ClearDragRects);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalOnMovedByPointSEL, (void*)CFR_Internal_V5::OnMovedByPoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalOnMovedByRectSEL, (void*)CFR_Internal_V5::OnMovedByRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSaveKeepToolSelectedToRegistrySEL, (void*)CFR_Internal_V5::SaveKeepToolSelectedToRegistry);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEnableKeepToolSelectedSEL, (void*)CFR_Internal_V5::EnableKeepToolSelected);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalChangeKeepToolSelectedStateFromRegistrySEL, (void*)CFR_Internal_V5::ChangeKeepToolSelectedStateFromRegistry);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalAddDragRectSEL, (void*)CFR_Internal_V5::AddDragRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalResetSEL, (void*)CFR_Internal_V5::Reset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalRunPageFlatSEL, (void*)CFR_Internal_V5::RunPageFlat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetCurrentUserNameSEL, (void*)CFR_Internal_V5::GetCurrentUserName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetLicenseeSEL, (void*)CFR_Internal_V5::GetLicensee);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEncryptAttachmentSEL, (void*)CFR_Internal_V5::EncryptAttachment);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetAllFormatFontSizeSEL, (void*)CFR_Internal_V5::GetAllFormatFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetAtAllFormatFontSEL, (void*)CFR_Internal_V5::GetAtAllFormatFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetFontFaceNameBoldItalicSEL, (void*)CFR_Internal_V5::GetFontFaceNameBoldItalic);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetFontStyleSEL, (void*)CFR_Internal_V5::GetFontStyle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEditSetEnableMLAutoFullFontSizeSEL, (void*)CFR_Internal_V5::EditSetEnableMLAutoFullFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalFlattenDynamicXFADocSEL, (void*)CFR_Internal_V5::FlattenDynamicXFADoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalIsSelfSignCertGenSEL, (void*)CFR_Internal_V5::IsSelfSignCertGen);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetIsCheckPrivateKeyCertSEL, (void*)CFR_Internal_V5::GetIsCheckPrivateKeyCert);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalParseMailtoURISEL, (void*)CFR_Internal_V5::ParseMailtoURI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalIsMailtoURISEL, (void*)CFR_Internal_V5::IsMailtoURI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalAsyncUpdateCheckerInfoSEL, (void*)CFR_Internal_V5::AsyncUpdateCheckerInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalIsCurrentToolSEL, (void*)CFR_Internal_V5::IsCurrentTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSetToolModeSEL, (void*)CFR_Internal_V5::SetToolMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetToolModeSEL, (void*)CFR_Internal_V5::GetToolMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSetPreToolHandlerSEL, (void*)CFR_Internal_V5::SetPreToolHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSetSignatureTypeSEL, (void*)CFR_Internal_V5::SetSignatureType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetSignatureTypeSEL, (void*)CFR_Internal_V5::GetSignatureType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalEmailUtilSendMailUseConfig2SEL, (void*)CFR_Internal_V5::EmailUtilSendMailUseConfig2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalDIBSourceToBmpSEL, (void*)CFR_Internal_V5::DIBSourceToBmp);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalBeginBulletEditingSEL, (void*)CFR_Internal_V5::BeginBulletEditing);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalBeginBulletEditing2SEL, (void*)CFR_Internal_V5::BeginBulletEditing2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalSetNotifySEL, (void*)CFR_Internal_V5::SetNotify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalOnCharTabSEL, (void*)CFR_Internal_V5::OnCharTab);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalSEL, FRInternalGetLicenseEditonSEL, (void*)CFR_Internal_V5::GetLicenseEditon);
	}
};

class CFR_SpellCheck_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSpellCheckSEL, FRSpellCheckNewSEL, (void*)CFR_SpellCheck_V5::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSpellCheckSEL, FRSpellCheckDestroySEL, (void*)CFR_SpellCheck_V5::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSpellCheckSEL, FRSpellCheckCheckWordSEL, (void*)CFR_SpellCheck_V5::CheckWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSpellCheckSEL, FRSpellCheckSuggestWordsSEL, (void*)CFR_SpellCheck_V5::SuggestWords);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSpellCheckSEL, FRSpellCheckIgnoreSEL, (void*)CFR_SpellCheck_V5::Ignore);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSpellCheckSEL, FRSpellCheckCheckStringSEL, (void*)CFR_SpellCheck_V5::CheckString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRSpellCheckSEL, FRSpellCheckAddDicSEL, (void*)CFR_SpellCheck_V5::AddDic);
	}
};

// fr_appImpl.h end

// In file fr_barImpl.h
class CFR_RibbonBar_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarAddCategorySEL, (void*)CFR_RibbonBar_V5::AddCategory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarAddRibbonContextCategorySEL, (void*)CFR_RibbonBar_V5::AddRibbonContextCategory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarActivateContextCategorySEL, (void*)CFR_RibbonBar_V5::ActivateContextCategory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarShowContextCategoriesSEL, (void*)CFR_RibbonBar_V5::ShowContextCategories);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarGetCategoryCountSEL, (void*)CFR_RibbonBar_V5::GetCategoryCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarGetCategoryByIndexSEL, (void*)CFR_RibbonBar_V5::GetCategoryByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarGetCategoryByNameSEL, (void*)CFR_RibbonBar_V5::GetCategoryByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarAddAsQATSEL, (void*)CFR_RibbonBar_V5::AddAsQAT);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarAddToTabsSEL, (void*)CFR_RibbonBar_V5::AddToTabs);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarAddHiddenSEL, (void*)CFR_RibbonBar_V5::AddHidden);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarAddBackStageViewItemSEL, (void*)CFR_RibbonBar_V5::AddBackStageViewItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarGetBackStageViewItemSEL, (void*)CFR_RibbonBar_V5::GetBackStageViewItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarSelectBackStageViewItemSEL, (void*)CFR_RibbonBar_V5::SelectBackStageViewItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarAddButtonToAddPlaceSEL, (void*)CFR_RibbonBar_V5::AddButtonToAddPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarCloseFilePageSEL, (void*)CFR_RibbonBar_V5::CloseFilePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarFindElementByNameSEL, (void*)CFR_RibbonBar_V5::FindElementByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarIsBackStageViewActiveSEL, (void*)CFR_RibbonBar_V5::IsBackStageViewActive);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarRecalcLayoutSEL, (void*)CFR_RibbonBar_V5::RecalcLayout);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarGetActiveCategorySEL, (void*)CFR_RibbonBar_V5::GetActiveCategory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarSetActiveCategorySEL, (void*)CFR_RibbonBar_V5::SetActiveCategory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarSetActiveCategory2SEL, (void*)CFR_RibbonBar_V5::SetActiveCategory2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarIsMinimizeSEL, (void*)CFR_RibbonBar_V5::IsMinimize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarAddCaptionButtonSEL, (void*)CFR_RibbonBar_V5::AddCaptionButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarRegisterFilePageEventHandlerSEL, (void*)CFR_RibbonBar_V5::RegisterFilePageEventHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarDestroyFilePageEventHandlerSEL, (void*)CFR_RibbonBar_V5::DestroyFilePageEventHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarAddCategory2SEL, (void*)CFR_RibbonBar_V5::AddCategory2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarShowButtonInAddPlaceSEL, (void*)CFR_RibbonBar_V5::ShowButtonInAddPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBarSEL, FRRibbonBarUpdateCmdUISEL, (void*)CFR_RibbonBar_V5::UpdateCmdUI);
	}
};

class CFR_RibbonCategory_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategorySetTitleSEL, (void*)CFR_RibbonCategory_V5::SetTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryGetTitleSEL, (void*)CFR_RibbonCategory_V5::GetTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategorySetContextTitleSEL, (void*)CFR_RibbonCategory_V5::SetContextTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryGetContextTitleSEL, (void*)CFR_RibbonCategory_V5::GetContextTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategorySetKeySEL, (void*)CFR_RibbonCategory_V5::SetKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryGetKeySEL, (void*)CFR_RibbonCategory_V5::GetKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategorySetVisibleSEL, (void*)CFR_RibbonCategory_V5::SetVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryGetVisibleSEL, (void*)CFR_RibbonCategory_V5::GetVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryAddPanelSEL, (void*)CFR_RibbonCategory_V5::AddPanel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryGetPanelCountSEL, (void*)CFR_RibbonCategory_V5::GetPanelCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryGetPanelByIndexSEL, (void*)CFR_RibbonCategory_V5::GetPanelByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryGetPanelByNameSEL, (void*)CFR_RibbonCategory_V5::GetPanelByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryGetNameSEL, (void*)CFR_RibbonCategory_V5::GetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryShowInQATCustomizeToolsDlgSEL, (void*)CFR_RibbonCategory_V5::ShowInQATCustomizeToolsDlg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryCopyPanelSEL, (void*)CFR_RibbonCategory_V5::CopyPanel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryRecalcLayoutSEL, (void*)CFR_RibbonCategory_V5::RecalcLayout);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryAddPanel2SEL, (void*)CFR_RibbonCategory_V5::AddPanel2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryAddDialogSEL, (void*)CFR_RibbonCategory_V5::AddDialog);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryPreTranslateMessageSEL, (void*)CFR_RibbonCategory_V5::PreTranslateMessage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryAddDialogToRightSEL, (void*)CFR_RibbonCategory_V5::AddDialogToRight);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryGetPosSEL, (void*)CFR_RibbonCategory_V5::GetPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategorySetPosSEL, (void*)CFR_RibbonCategory_V5::SetPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategorySetHighlightSEL, (void*)CFR_RibbonCategory_V5::SetHighlight);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCategorySEL, FRRibbonCategoryGetHighlightColorSEL, (void*)CFR_RibbonCategory_V5::GetHighlightColor);
	}
};

class CFR_RibbonPanel_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelAddElementSEL, (void*)CFR_RibbonPanel_V5::AddElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelChangeElementTypeSEL, (void*)CFR_RibbonPanel_V5::ChangeElementType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelAddElementToGroupSEL, (void*)CFR_RibbonPanel_V5::AddElementToGroup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelSetLaunchButtonSEL, (void*)CFR_RibbonPanel_V5::SetLaunchButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelCopyElementToPanelSEL, (void*)CFR_RibbonPanel_V5::CopyElementToPanel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelSetTitleSEL, (void*)CFR_RibbonPanel_V5::SetTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelGetTitleSEL, (void*)CFR_RibbonPanel_V5::GetTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelSetVisibleSEL, (void*)CFR_RibbonPanel_V5::SetVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelGetVisibleSEL, (void*)CFR_RibbonPanel_V5::GetVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelGetElementByNameSEL, (void*)CFR_RibbonPanel_V5::GetElementByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelGetElementCountSEL, (void*)CFR_RibbonPanel_V5::GetElementCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelGetElementByIndexSEL, (void*)CFR_RibbonPanel_V5::GetElementByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelSetPanelImageSEL, (void*)CFR_RibbonPanel_V5::SetPanelImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelGetNameSEL, (void*)CFR_RibbonPanel_V5::GetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelShowInQATCustomizeToolsDlgSEL, (void*)CFR_RibbonPanel_V5::ShowInQATCustomizeToolsDlg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelSetImageInitProcSEL, (void*)CFR_RibbonPanel_V5::SetImageInitProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelSetKeySEL, (void*)CFR_RibbonPanel_V5::SetKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelAddDialogSEL, (void*)CFR_RibbonPanel_V5::AddDialog);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelPreTranslateMessageSEL, (void*)CFR_RibbonPanel_V5::PreTranslateMessage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelEnableAddToCustomizeDlgSEL, (void*)CFR_RibbonPanel_V5::EnableAddToCustomizeDlg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPanelSEL, FRRibbonPanelSetShowDefaultButtonAtLastSEL, (void*)CFR_RibbonPanel_V5::SetShowDefaultButtonAtLast);
	}
};

class CFR_RibbonElement_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementAddSubItemSEL, (void*)CFR_RibbonElement_V5::AddSubItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetExecuteProcSEL, (void*)CFR_RibbonElement_V5::SetExecuteProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetComputeEnabledProcSEL, (void*)CFR_RibbonElement_V5::SetComputeEnabledProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetComputeMarkedProcSEL, (void*)CFR_RibbonElement_V5::SetComputeMarkedProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementDoExecuteProcSEL, (void*)CFR_RibbonElement_V5::DoExecuteProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementIsEnabledSEL, (void*)CFR_RibbonElement_V5::IsEnabled);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementIsMarkedSEL, (void*)CFR_RibbonElement_V5::IsMarked);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetClientDataSEL, (void*)CFR_RibbonElement_V5::SetClientData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetClientDataSEL, (void*)CFR_RibbonElement_V5::GetClientData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetAccelSEL, (void*)CFR_RibbonElement_V5::SetAccel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetAccelSEL, (void*)CFR_RibbonElement_V5::GetAccel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetKeySEL, (void*)CFR_RibbonElement_V5::SetKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetKeySEL, (void*)CFR_RibbonElement_V5::GetKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetTooltipSEL, (void*)CFR_RibbonElement_V5::SetTooltip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetTooltipSEL, (void*)CFR_RibbonElement_V5::GetTooltip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetDescriptionSEL, (void*)CFR_RibbonElement_V5::SetDescription);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetDescriptionSEL, (void*)CFR_RibbonElement_V5::GetDescription);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetTitleSEL, (void*)CFR_RibbonElement_V5::SetTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetTitleSEL, (void*)CFR_RibbonElement_V5::GetTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetImageSEL, (void*)CFR_RibbonElement_V5::SetImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementAddChangeImageSEL, (void*)CFR_RibbonElement_V5::AddChangeImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementChangeImageSEL, (void*)CFR_RibbonElement_V5::ChangeImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetTooltipImageSEL, (void*)CFR_RibbonElement_V5::SetTooltipImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetTooltipImageIISEL, (void*)CFR_RibbonElement_V5::SetTooltipImageII);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetVisibleSEL, (void*)CFR_RibbonElement_V5::SetVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetVisibleSEL, (void*)CFR_RibbonElement_V5::GetVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementRemoveSEL, (void*)CFR_RibbonElement_V5::Remove);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetTypeSEL, (void*)CFR_RibbonElement_V5::GetType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetOriginalIDSEL, (void*)CFR_RibbonElement_V5::GetOriginalID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetNameSEL, (void*)CFR_RibbonElement_V5::GetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetSubItemCountSEL, (void*)CFR_RibbonElement_V5::GetSubItemCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementFindSubElementByIndexSEL, (void*)CFR_RibbonElement_V5::FindSubElementByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetSubElementByNameSEL, (void*)CFR_RibbonElement_V5::GetSubElementByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementCopyElementAsSubItemSEL, (void*)CFR_RibbonElement_V5::CopyElementAsSubItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetCorrespondingButtonSEL, (void*)CFR_RibbonElement_V5::GetCorrespondingButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetAsSubElementSEL, (void*)CFR_RibbonElement_V5::SetAsSubElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetImplicitLargeBitmapSEL, (void*)CFR_RibbonElement_V5::SetImplicitLargeBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetShowModeSEL, (void*)CFR_RibbonElement_V5::SetShowMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetDropdownProcSEL, (void*)CFR_RibbonElement_V5::SetDropdownProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetClientRectSEL, (void*)CFR_RibbonElement_V5::GetClientRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetWindowRectSEL, (void*)CFR_RibbonElement_V5::GetWindowRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementShowInQATCustomizeToolsDlgSEL, (void*)CFR_RibbonElement_V5::ShowInQATCustomizeToolsDlg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetImageInitProcSEL, (void*)CFR_RibbonElement_V5::SetImageInitProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetButtonMapIdSEL, (void*)CFR_RibbonElement_V5::GetButtonMapId);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetBackstageViewTabElementKeepStateSEL, (void*)CFR_RibbonElement_V5::SetBackstageViewTabElementKeepState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementIsBackstageViewTabElementKeepStateSEL, (void*)CFR_RibbonElement_V5::IsBackstageViewTabElementKeepState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetSelectOnlySEL, (void*)CFR_RibbonElement_V5::SetSelectOnly);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetCategoryNameSEL, (void*)CFR_RibbonElement_V5::GetCategoryName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementGetPanelNameSEL, (void*)CFR_RibbonElement_V5::GetPanelName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonElementSEL, FRRibbonElementSetHighlightSEL, (void*)CFR_RibbonElement_V5::SetHighlight);
	}
};

class CFR_RibbonButton_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonButtonSEL, FRRibbonButtonGetElementSEL, (void*)CFR_RibbonButton_V5::GetElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonButtonSEL, FRRibbonButtonSetDefaultCommandSEL, (void*)CFR_RibbonButton_V5::SetDefaultCommand);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonButtonSEL, FRRibbonButtonSetAlwaysShowDescriptionSEL, (void*)CFR_RibbonButton_V5::SetAlwaysShowDescription);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonButtonSEL, FRRibbonButtonSetButtonPopupWndSEL, (void*)CFR_RibbonButton_V5::SetButtonPopupWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonButtonSEL, FRRibbonButtonCloseButtonPopupWndSEL, (void*)CFR_RibbonButton_V5::CloseButtonPopupWnd);
	}
};

class CFR_RibbonEdit_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonEditSEL, FRRibbonEditGetElementSEL, (void*)CFR_RibbonEdit_V5::GetElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonEditSEL, FRRibbonEditGetTextSEL, (void*)CFR_RibbonEdit_V5::GetText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonEditSEL, FRRibbonEditSetTextSEL, (void*)CFR_RibbonEdit_V5::SetText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonEditSEL, FRRibbonEditEnableSpinButtonsSEL, (void*)CFR_RibbonEdit_V5::EnableSpinButtons);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonEditSEL, FRRibbonEditSetSearchModeSEL, (void*)CFR_RibbonEdit_V5::SetSearchMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonEditSEL, FRRibbonEditSetWidthSEL, (void*)CFR_RibbonEdit_V5::SetWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonEditSEL, FRRibbonEditGetHWndSEL, (void*)CFR_RibbonEdit_V5::GetHWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonEditSEL, FRRibbonEditSetTextFlagSEL, (void*)CFR_RibbonEdit_V5::SetTextFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonEditSEL, FRRibbonEditSetFocusSEL, (void*)CFR_RibbonEdit_V5::SetFocus);
	}
};

class CFR_RibbonLabel_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonLabelSEL, FRRibbonLabelGetElementSEL, (void*)CFR_RibbonLabel_V5::GetElement);
	}
};

class CFR_RibbonCheckBox_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCheckBoxSEL, FRRibbonCheckBoxGetElementSEL, (void*)CFR_RibbonCheckBox_V5::GetElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCheckBoxSEL, FRRibbonCheckBoxIsCheckedSEL, (void*)CFR_RibbonCheckBox_V5::IsChecked);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonCheckBoxSEL, FRRibbonCheckBoxSetCheckSEL, (void*)CFR_RibbonCheckBox_V5::SetCheck);
	}
};

class CFR_RibbonRadioButton_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonRadioButtonSEL, FRRibbonRadioButtonGetElementSEL, (void*)CFR_RibbonRadioButton_V5::GetElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonRadioButtonSEL, FRRibbonRadioButtonIsCheckedSEL, (void*)CFR_RibbonRadioButton_V5::IsChecked);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonRadioButtonSEL, FRRibbonRadioButtonSetCheckSEL, (void*)CFR_RibbonRadioButton_V5::SetCheck);
	}
};

class CFR_RibbonComboBox_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxGetElementSEL, (void*)CFR_RibbonComboBox_V5::GetElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxAddItemSEL, (void*)CFR_RibbonComboBox_V5::AddItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxInsertItemSEL, (void*)CFR_RibbonComboBox_V5::InsertItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxGetItemCountSEL, (void*)CFR_RibbonComboBox_V5::GetItemCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxGetItemSEL, (void*)CFR_RibbonComboBox_V5::GetItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxGetCurSelSEL, (void*)CFR_RibbonComboBox_V5::GetCurSel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxRemoveAllItemsSEL, (void*)CFR_RibbonComboBox_V5::RemoveAllItems);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxSelectItemSEL, (void*)CFR_RibbonComboBox_V5::SelectItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxDeleteItemSEL, (void*)CFR_RibbonComboBox_V5::DeleteItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxFindItemSEL, (void*)CFR_RibbonComboBox_V5::FindItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxSetWidthSEL, (void*)CFR_RibbonComboBox_V5::SetWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxSetEditBoxSEL, (void*)CFR_RibbonComboBox_V5::SetEditBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxGetEditTextSEL, (void*)CFR_RibbonComboBox_V5::GetEditText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxSetEditTextSEL, (void*)CFR_RibbonComboBox_V5::SetEditText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxEnableDropDownListResizeSEL, (void*)CFR_RibbonComboBox_V5::EnableDropDownListResize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxGetHWndSEL, (void*)CFR_RibbonComboBox_V5::GetHWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxSetTextFlagSEL, (void*)CFR_RibbonComboBox_V5::SetTextFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonComboBoxSEL, FRRibbonComboBoxSetFocusSEL, (void*)CFR_RibbonComboBox_V5::SetFocus);
	}
};

class CFR_RibbonFontComboBox_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxGetElementSEL, (void*)CFR_RibbonFontComboBox_V5::GetElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxGetItemSEL, (void*)CFR_RibbonFontComboBox_V5::GetItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxGetCurSelSEL, (void*)CFR_RibbonFontComboBox_V5::GetCurSel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxSelectItemSEL, (void*)CFR_RibbonFontComboBox_V5::SelectItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxSetWidthSEL, (void*)CFR_RibbonFontComboBox_V5::SetWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxSetEditBoxSEL, (void*)CFR_RibbonFontComboBox_V5::SetEditBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxGetEditTextSEL, (void*)CFR_RibbonFontComboBox_V5::GetEditText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxSetEditTextSEL, (void*)CFR_RibbonFontComboBox_V5::SetEditText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxEnableDropDownListResizeSEL, (void*)CFR_RibbonFontComboBox_V5::EnableDropDownListResize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxGetHWndSEL, (void*)CFR_RibbonFontComboBox_V5::GetHWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxAddFontSEL, (void*)CFR_RibbonFontComboBox_V5::AddFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxInsertFontSEL, (void*)CFR_RibbonFontComboBox_V5::InsertFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxGetFontIndexSEL, (void*)CFR_RibbonFontComboBox_V5::GetFontIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxGetFontNameSEL, (void*)CFR_RibbonFontComboBox_V5::GetFontName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxGetScriptNameSEL, (void*)CFR_RibbonFontComboBox_V5::GetScriptName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxSelectItem2SEL, (void*)CFR_RibbonFontComboBox_V5::SelectItem2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxRemoveFontSEL, (void*)CFR_RibbonFontComboBox_V5::RemoveFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxRemoveFont2SEL, (void*)CFR_RibbonFontComboBox_V5::RemoveFont2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxGetFontCountSEL, (void*)CFR_RibbonFontComboBox_V5::GetFontCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxSetFocusSEL, (void*)CFR_RibbonFontComboBox_V5::SetFocus);
	}
};

class CFR_RibbonPaletteButton_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonGetElementSEL, (void*)CFR_RibbonPaletteButton_V5::GetElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonAddGroupSEL, (void*)CFR_RibbonPaletteButton_V5::AddGroup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonSetGroupTitleSEL, (void*)CFR_RibbonPaletteButton_V5::SetGroupTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonSetItemToolTipSEL, (void*)CFR_RibbonPaletteButton_V5::SetItemToolTip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonSetScrollButtonTooltipSEL, (void*)CFR_RibbonPaletteButton_V5::SetScrollButtonTooltip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonSetMenuButtonTooltipSEL, (void*)CFR_RibbonPaletteButton_V5::SetMenuButtonTooltip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonGetGroupTitleSEL, (void*)CFR_RibbonPaletteButton_V5::GetGroupTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonGetItemToolTipSEL, (void*)CFR_RibbonPaletteButton_V5::GetItemToolTip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonGetScrollButtonTooltipSEL, (void*)CFR_RibbonPaletteButton_V5::GetScrollButtonTooltip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonGetMenuButtonTooltipSEL, (void*)CFR_RibbonPaletteButton_V5::GetMenuButtonTooltip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonGetGroupItemCountSEL, (void*)CFR_RibbonPaletteButton_V5::GetGroupItemCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonGetGroupCountSEL, (void*)CFR_RibbonPaletteButton_V5::GetGroupCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonIsGroupNameExistSEL, (void*)CFR_RibbonPaletteButton_V5::IsGroupNameExist);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonSetButtonModeSEL, (void*)CFR_RibbonPaletteButton_V5::SetButtonMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonSetIconsInRowSEL, (void*)CFR_RibbonPaletteButton_V5::SetIconsInRow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonEnableResizeSEL, (void*)CFR_RibbonPaletteButton_V5::EnableResize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonGetSelectedItemSEL, (void*)CFR_RibbonPaletteButton_V5::GetSelectedItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonInsertItemToGroupLastSEL, (void*)CFR_RibbonPaletteButton_V5::InsertItemToGroupLast);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonSetDefaultGroupSEL, (void*)CFR_RibbonPaletteButton_V5::SetDefaultGroup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonRemoveGroupSEL, (void*)CFR_RibbonPaletteButton_V5::RemoveGroup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonRemoveItemFromGroupSEL, (void*)CFR_RibbonPaletteButton_V5::RemoveItemFromGroup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonSetItemDataSEL, (void*)CFR_RibbonPaletteButton_V5::SetItemData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonGetItemDataSEL, (void*)CFR_RibbonPaletteButton_V5::GetItemData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonSetGroupItemEnableSEL, (void*)CFR_RibbonPaletteButton_V5::SetGroupItemEnable);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonSetSelectedItemSEL, (void*)CFR_RibbonPaletteButton_V5::SetSelectedItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonSetRowsSEL, (void*)CFR_RibbonPaletteButton_V5::SetRows);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonRemoveAllGroupSEL, (void*)CFR_RibbonPaletteButton_V5::RemoveAllGroup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonSetInitSizeSEL, (void*)CFR_RibbonPaletteButton_V5::SetInitSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonGetItemInfoSEL, (void*)CFR_RibbonPaletteButton_V5::GetItemInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonSetHighlightItemProcSEL, (void*)CFR_RibbonPaletteButton_V5::SetHighlightItemProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonSetItemAccNameTitleSEL, (void*)CFR_RibbonPaletteButton_V5::SetItemAccNameTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonAddGroupPtrSEL, (void*)CFR_RibbonPaletteButton_V5::AddGroupPtr);
	}
};

class CFR_RibbonColorButton_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonGetElementSEL, (void*)CFR_RibbonColorButton_V5::GetElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonEnableAutomaticButtonSEL, (void*)CFR_RibbonColorButton_V5::EnableAutomaticButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonSetAutomaticButtonLabelSEL, (void*)CFR_RibbonColorButton_V5::SetAutomaticButtonLabel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonSetAutomaticButtonToolTipSEL, (void*)CFR_RibbonColorButton_V5::SetAutomaticButtonToolTip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonEnableOtherButtonSEL, (void*)CFR_RibbonColorButton_V5::EnableOtherButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonSetOtherButtonLabelSEL, (void*)CFR_RibbonColorButton_V5::SetOtherButtonLabel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonSetOtherButtonToolTipSEL, (void*)CFR_RibbonColorButton_V5::SetOtherButtonToolTip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonGetAutomaticButtonLabelSEL, (void*)CFR_RibbonColorButton_V5::GetAutomaticButtonLabel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonGetAutomaticButtonToolTipSEL, (void*)CFR_RibbonColorButton_V5::GetAutomaticButtonToolTip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonGetOtherButtonLabelSEL, (void*)CFR_RibbonColorButton_V5::GetOtherButtonLabel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonGetOtherButtonToolTipSEL, (void*)CFR_RibbonColorButton_V5::GetOtherButtonToolTip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonAddColorsGroupSEL, (void*)CFR_RibbonColorButton_V5::AddColorsGroup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonAddStandardColorSEL, (void*)CFR_RibbonColorButton_V5::AddStandardColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonAddAdditionalColorSEL, (void*)CFR_RibbonColorButton_V5::AddAdditionalColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonAddMainColorSEL, (void*)CFR_RibbonColorButton_V5::AddMainColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonSetGroupLabelSEL, (void*)CFR_RibbonColorButton_V5::SetGroupLabel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonGetGroupLabelSEL, (void*)CFR_RibbonColorButton_V5::GetGroupLabel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonSetColorSEL, (void*)CFR_RibbonColorButton_V5::SetColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonGetColorSEL, (void*)CFR_RibbonColorButton_V5::GetColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonSetColumnsSEL, (void*)CFR_RibbonColorButton_V5::SetColumns);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonSetColorBoxSizeSEL, (void*)CFR_RibbonColorButton_V5::SetColorBoxSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonSetDefaultCommandSEL, (void*)CFR_RibbonColorButton_V5::SetDefaultCommand);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonColorButtonSEL, FRRibbonColorButtonIsAutomaticButtonClickSEL, (void*)CFR_RibbonColorButton_V5::IsAutomaticButtonClick);
	}
};

class CFR_RibbonSlider_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonSliderSEL, FRRibbonSliderGetElementSEL, (void*)CFR_RibbonSlider_V5::GetElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonSliderSEL, FRRibbonSliderSetZoomButtonsSEL, (void*)CFR_RibbonSlider_V5::SetZoomButtons);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonSliderSEL, FRRibbonSliderSetRangeSEL, (void*)CFR_RibbonSlider_V5::SetRange);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonSliderSEL, FRRibbonSliderGetRangeMinSEL, (void*)CFR_RibbonSlider_V5::GetRangeMin);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonSliderSEL, FRRibbonSliderGetRangeMaxSEL, (void*)CFR_RibbonSlider_V5::GetRangeMax);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonSliderSEL, FRRibbonSliderSetPosSEL, (void*)CFR_RibbonSlider_V5::SetPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonSliderSEL, FRRibbonSliderGetPosSEL, (void*)CFR_RibbonSlider_V5::GetPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonSliderSEL, FRRibbonSliderSetWidthSEL, (void*)CFR_RibbonSlider_V5::SetWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonSliderSEL, FRRibbonSliderSetStyleSEL, (void*)CFR_RibbonSlider_V5::SetStyle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonSliderSEL, FRRibbonSliderGetMouseStatusSEL, (void*)CFR_RibbonSlider_V5::GetMouseStatus);
	}
};

class CFR_RibbonListButton_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonListButtonSEL, FRRibbonListButtonGetElementSEL, (void*)CFR_RibbonListButton_V5::GetElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonListButtonSEL, FRRibbonListButtonAddGroupSEL, (void*)CFR_RibbonListButton_V5::AddGroup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonListButtonSEL, FRRibbonListButtonGetGroupCountSEL, (void*)CFR_RibbonListButton_V5::GetGroupCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonListButtonSEL, FRRibbonListButtonIsGroupNameExistSEL, (void*)CFR_RibbonListButton_V5::IsGroupNameExist);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonListButtonSEL, FRRibbonListButtonEnableResizeSEL, (void*)CFR_RibbonListButton_V5::EnableResize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonListButtonSEL, FRRibbonListButtonSetGroupTitleSEL, (void*)CFR_RibbonListButton_V5::SetGroupTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonListButtonSEL, FRRibbonListButtonSetItemToolTipSEL, (void*)CFR_RibbonListButton_V5::SetItemToolTip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonListButtonSEL, FRRibbonListButtonSetItemTitleSEL, (void*)CFR_RibbonListButton_V5::SetItemTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonListButtonSEL, FRRibbonListButtonGetGroupTitleSEL, (void*)CFR_RibbonListButton_V5::GetGroupTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonListButtonSEL, FRRibbonListButtonGetItemToolTipSEL, (void*)CFR_RibbonListButton_V5::GetItemToolTip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonListButtonSEL, FRRibbonListButtonGetItemTitleSEL, (void*)CFR_RibbonListButton_V5::GetItemTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonListButtonSEL, FRRibbonListButtonGetItemCountSEL, (void*)CFR_RibbonListButton_V5::GetItemCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonListButtonSEL, FRRibbonListButtonGetSelectedItemSEL, (void*)CFR_RibbonListButton_V5::GetSelectedItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonListButtonSEL, FRRibbonListButtonSetAlignedSideSEL, (void*)CFR_RibbonListButton_V5::SetAlignedSide);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonListButtonSEL, FRRibbonListButtonSetIconsInRowSEL, (void*)CFR_RibbonListButton_V5::SetIconsInRow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonListButtonSEL, FRRibbonListButtonSetSelectedItemSEL, (void*)CFR_RibbonListButton_V5::SetSelectedItem);
	}
};

class CFR_RibbonBackStageViewItem_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemSetTitleSEL, (void*)CFR_RibbonBackStageViewItem_V5::SetTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemAddDialogSEL, (void*)CFR_RibbonBackStageViewItem_V5::AddDialog);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemAddPropertySheetPageSEL, (void*)CFR_RibbonBackStageViewItem_V5::AddPropertySheetPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemRemovePropertySheetPageSEL, (void*)CFR_RibbonBackStageViewItem_V5::RemovePropertySheetPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemAddPropertySheetGroupSEL, (void*)CFR_RibbonBackStageViewItem_V5::AddPropertySheetGroup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemSetGroupTitleSEL, (void*)CFR_RibbonBackStageViewItem_V5::SetGroupTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemSetVisibleSEL, (void*)CFR_RibbonBackStageViewItem_V5::SetVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemSetDisableSEL, (void*)CFR_RibbonBackStageViewItem_V5::SetDisable);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemAddAnchorSEL, (void*)CFR_RibbonBackStageViewItem_V5::AddAnchor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemModifiedToRibbonStyleButtonSEL, (void*)CFR_RibbonBackStageViewItem_V5::ModifiedToRibbonStyleButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemSetPropertyActivePageSEL, (void*)CFR_RibbonBackStageViewItem_V5::SetPropertyActivePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemSetPageTitleSEL, (void*)CFR_RibbonBackStageViewItem_V5::SetPageTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemIsPropertySheetPageExistSEL, (void*)CFR_RibbonBackStageViewItem_V5::IsPropertySheetPageExist);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemIsDialogExistSEL, (void*)CFR_RibbonBackStageViewItem_V5::IsDialogExist);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemGetElementSEL, (void*)CFR_RibbonBackStageViewItem_V5::GetElement);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemEnablePropertyPageRemoveSEL, (void*)CFR_RibbonBackStageViewItem_V5::EnablePropertyPageRemove);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemRedrawPropertySheetSEL, (void*)CFR_RibbonBackStageViewItem_V5::RedrawPropertySheet);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemSetDlgItemMaxSizeSEL, (void*)CFR_RibbonBackStageViewItem_V5::SetDlgItemMaxSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemSetPageSelectProcSEL, (void*)CFR_RibbonBackStageViewItem_V5::SetPageSelectProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemBCGPreTranslateMessageSEL, (void*)CFR_RibbonBackStageViewItem_V5::BCGPreTranslateMessage);
	}
};

class CFR_RibbonStyleButton_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleButtonSEL, FRRibbonStyleButtonSetImageSEL, (void*)CFR_RibbonStyleButton_V5::SetImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleButtonSEL, FRRibbonStyleButtonSetAlignStyleSEL, (void*)CFR_RibbonStyleButton_V5::SetAlignStyle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleButtonSEL, FRRibbonStyleButtonSetImageSideSEL, (void*)CFR_RibbonStyleButton_V5::SetImageSide);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleButtonSEL, FRRibbonStyleButtonSetFlatStyleSEL, (void*)CFR_RibbonStyleButton_V5::SetFlatStyle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleButtonSEL, FRRibbonStyleButtonSetVertMarginSEL, (void*)CFR_RibbonStyleButton_V5::SetVertMargin);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleButtonSEL, FRRibbonStyleButtonGetMFCButtonSEL, (void*)CFR_RibbonStyleButton_V5::GetMFCButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleButtonSEL, FRRibbonStyleButtonReleaseSEL, (void*)CFR_RibbonStyleButton_V5::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleButtonSEL, FRRibbonStyleButtonSetImage2SEL, (void*)CFR_RibbonStyleButton_V5::SetImage2);
	}
};

class CFR_RibbonStyleListBox_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxAddImageSEL, (void*)CFR_RibbonStyleListBox_V5::AddImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxAddStringSEL, (void*)CFR_RibbonStyleListBox_V5::AddString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxAddCaptionSEL, (void*)CFR_RibbonStyleListBox_V5::AddCaption);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxDeleteStringSEL, (void*)CFR_RibbonStyleListBox_V5::DeleteString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxInsertStringSEL, (void*)CFR_RibbonStyleListBox_V5::InsertString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxGetItemDataSEL, (void*)CFR_RibbonStyleListBox_V5::GetItemData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxSetItemDataSEL, (void*)CFR_RibbonStyleListBox_V5::SetItemData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxGetMFCListBoxSEL, (void*)CFR_RibbonStyleListBox_V5::GetMFCListBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxReleaseSEL, (void*)CFR_RibbonStyleListBox_V5::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxAddImage2SEL, (void*)CFR_RibbonStyleListBox_V5::AddImage2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxEnablePinsSEL, (void*)CFR_RibbonStyleListBox_V5::EnablePins);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxHasPinsSEL, (void*)CFR_RibbonStyleListBox_V5::HasPins);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxSetItemPinnedSEL, (void*)CFR_RibbonStyleListBox_V5::SetItemPinned);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxGetItemPinnedStateSEL, (void*)CFR_RibbonStyleListBox_V5::GetItemPinnedState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxIsSeparatorItemSEL, (void*)CFR_RibbonStyleListBox_V5::IsSeparatorItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxRedrawListSEL, (void*)CFR_RibbonStyleListBox_V5::RedrawList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxSetClickItemProcSEL, (void*)CFR_RibbonStyleListBox_V5::SetClickItemProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxSetClickPinProcSEL, (void*)CFR_RibbonStyleListBox_V5::SetClickPinProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxSetClientDataSEL, (void*)CFR_RibbonStyleListBox_V5::SetClientData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxClearSEL, (void*)CFR_RibbonStyleListBox_V5::Clear);
	}
};

class CFR_RibbonStyleStatic_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleStaticSEL, FRRibbonStyleStaticSetImageSEL, (void*)CFR_RibbonStyleStatic_V5::SetImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleStaticSEL, FRRibbonStyleStaticGetMFCStaticSEL, (void*)CFR_RibbonStyleStatic_V5::GetMFCStatic);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleStaticSEL, FRRibbonStyleStaticSetFontStyleSEL, (void*)CFR_RibbonStyleStatic_V5::SetFontStyle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleStaticSEL, FRRibbonStyleStaticSetTextColorSEL, (void*)CFR_RibbonStyleStatic_V5::SetTextColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleStaticSEL, FRRibbonStyleStaticSetBackgroundColorSEL, (void*)CFR_RibbonStyleStatic_V5::SetBackgroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleStaticSEL, FRRibbonStyleStaticSetTextDrawFormatSEL, (void*)CFR_RibbonStyleStatic_V5::SetTextDrawFormat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleStaticSEL, FRRibbonStyleStaticReleaseSEL, (void*)CFR_RibbonStyleStatic_V5::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleStaticSEL, FRRibbonStyleStaticSetImage2SEL, (void*)CFR_RibbonStyleStatic_V5::SetImage2);
	}
};

class CFR_FormatTools_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetSEL, (void*)CFR_FormatTools_V5::Get);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetEventSEL, (void*)CFR_FormatTools_V5::SetEvent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetFontNameSEL, (void*)CFR_FormatTools_V5::SetFontName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetFontNameFirstSEL, (void*)CFR_FormatTools_V5::SetFontNameFirst);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetFontSizeSEL, (void*)CFR_FormatTools_V5::SetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetFontSizeFirstSEL, (void*)CFR_FormatTools_V5::SetFontSizeFirst);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetTextColorSEL, (void*)CFR_FormatTools_V5::SetTextColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetLineColorSEL, (void*)CFR_FormatTools_V5::SetLineColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetFillColorSEL, (void*)CFR_FormatTools_V5::SetFillColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetBoldSEL, (void*)CFR_FormatTools_V5::SetBold);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetItalicSEL, (void*)CFR_FormatTools_V5::SetItalic);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetAlignSEL, (void*)CFR_FormatTools_V5::SetAlign);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetCharSpaceSEL, (void*)CFR_FormatTools_V5::SetCharSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetCharHorzScaleSEL, (void*)CFR_FormatTools_V5::SetCharHorzScale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetLineLeadingSEL, (void*)CFR_FormatTools_V5::SetLineLeading);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetSuperScriptSEL, (void*)CFR_FormatTools_V5::SetSuperScript);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetSubScriptSEL, (void*)CFR_FormatTools_V5::SetSubScript);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetUnderlineSEL, (void*)CFR_FormatTools_V5::SetUnderline);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetCrossSEL, (void*)CFR_FormatTools_V5::SetCross);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetWordSpaceSEL, (void*)CFR_FormatTools_V5::SetWordSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetFontNameSEL, (void*)CFR_FormatTools_V5::GetFontName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetFontSizeSEL, (void*)CFR_FormatTools_V5::GetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetTextColorSEL, (void*)CFR_FormatTools_V5::GetTextColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetLineColorSEL, (void*)CFR_FormatTools_V5::GetLineColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetFillColorSEL, (void*)CFR_FormatTools_V5::GetFillColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetAlignSEL, (void*)CFR_FormatTools_V5::GetAlign);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetCharSpaceSEL, (void*)CFR_FormatTools_V5::GetCharSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetCharHorzScaleSEL, (void*)CFR_FormatTools_V5::GetCharHorzScale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetLineLeadingSEL, (void*)CFR_FormatTools_V5::GetLineLeading);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetUnderlineSEL, (void*)CFR_FormatTools_V5::GetUnderline);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetCrossSEL, (void*)CFR_FormatTools_V5::GetCross);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetSuperScriptSEL, (void*)CFR_FormatTools_V5::GetSuperScript);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetSubScriptSEL, (void*)CFR_FormatTools_V5::GetSubScript);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsIsBoldSEL, (void*)CFR_FormatTools_V5::IsBold);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsIsItalicSEL, (void*)CFR_FormatTools_V5::IsItalic);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetWordSpaceSEL, (void*)CFR_FormatTools_V5::GetWordSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsIsVisibledSEL, (void*)CFR_FormatTools_V5::IsVisibled);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsShowSEL, (void*)CFR_FormatTools_V5::Show);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsIsEnabledSEL, (void*)CFR_FormatTools_V5::IsEnabled);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsEnableSEL, (void*)CFR_FormatTools_V5::Enable);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsIsButtonVisibledSEL, (void*)CFR_FormatTools_V5::IsButtonVisibled);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsHideButtonSEL, (void*)CFR_FormatTools_V5::HideButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsIsButtonEnabledSEL, (void*)CFR_FormatTools_V5::IsButtonEnabled);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsEnableButtonSEL, (void*)CFR_FormatTools_V5::EnableButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetOwnerFontNameArrSEL, (void*)CFR_FormatTools_V5::SetOwnerFontNameArr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsCleanOwnerFontNameArrSEL, (void*)CFR_FormatTools_V5::CleanOwnerFontNameArr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsFindFontNameSEL, (void*)CFR_FormatTools_V5::FindFontName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsCountFontListSEL, (void*)CFR_FormatTools_V5::CountFontList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetFontListItemSEL, (void*)CFR_FormatTools_V5::GetFontListItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetFormatContextTitleSEL, (void*)CFR_FormatTools_V5::SetFormatContextTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsActivateFormatCategorySEL, (void*)CFR_FormatTools_V5::ActivateFormatCategory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsSetWritingDirectionSEL, (void*)CFR_FormatTools_V5::SetWritingDirection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetWritingDirectionSEL, (void*)CFR_FormatTools_V5::GetWritingDirection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsGetFontFaceNameSEL, (void*)CFR_FormatTools_V5::GetFontFaceName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFormatToolsSEL, FRFormatToolsReleaseEventSEL, (void*)CFR_FormatTools_V5::ReleaseEvent);
	}
};

class CFR_PropertyTools_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPropertyToolsSEL, FRPropertyToolsGetSEL, (void*)CFR_PropertyTools_V5::Get);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPropertyToolsSEL, FRPropertyToolsSetEventSEL, (void*)CFR_PropertyTools_V5::SetEvent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPropertyToolsSEL, FRPropertyToolsSetColorSEL, (void*)CFR_PropertyTools_V5::SetColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPropertyToolsSEL, FRPropertyToolsSetOpacitySEL, (void*)CFR_PropertyTools_V5::SetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPropertyToolsSEL, FRPropertyToolsIsVisibledSEL, (void*)CFR_PropertyTools_V5::IsVisibled);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPropertyToolsSEL, FRPropertyToolsShowSEL, (void*)CFR_PropertyTools_V5::Show);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPropertyToolsSEL, FRPropertyToolsIsEnabledSEL, (void*)CFR_PropertyTools_V5::IsEnabled);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPropertyToolsSEL, FRPropertyToolsEnableSEL, (void*)CFR_PropertyTools_V5::Enable);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPropertyToolsSEL, FRPropertyToolsReleaseEventSEL, (void*)CFR_PropertyTools_V5::ReleaseEvent);
	}
};

// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
class CFR_WindowsDIB_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRWindowsDIBSEL, FRWindowsDIBNewSEL, (void*)CFR_WindowsDIB_V5::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRWindowsDIBSEL, FRWindowsDIBDestroySEL, (void*)CFR_WindowsDIB_V5::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRWindowsDIBSEL, FRWindowsDIBGetBitmapInfoSEL, (void*)CFR_WindowsDIB_V5::GetBitmapInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRWindowsDIBSEL, FRWindowsDIBLoadFromBufSEL, (void*)CFR_WindowsDIB_V5::LoadFromBuf);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRWindowsDIBSEL, FRWindowsDIBGetDDBitmapSEL, (void*)CFR_WindowsDIB_V5::GetDDBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRWindowsDIBSEL, FRWindowsDIBLoadFromDDBSEL, (void*)CFR_WindowsDIB_V5::LoadFromDDB);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRWindowsDIBSEL, FRWindowsDIBLoadFromFileSEL, (void*)CFR_WindowsDIB_V5::LoadFromFile);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRWindowsDIBSEL, FRWindowsDIBLoadFromFileIISEL, (void*)CFR_WindowsDIB_V5::LoadFromFileII);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRWindowsDIBSEL, FRWindowsDIBLoadDIBitmapSEL, (void*)CFR_WindowsDIB_V5::LoadDIBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRWindowsDIBSEL, FRWindowsDIBGetDCSEL, (void*)CFR_WindowsDIB_V5::GetDC);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRWindowsDIBSEL, FRWindowsDIBGetWindowsBitmapSEL, (void*)CFR_WindowsDIB_V5::GetWindowsBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRWindowsDIBSEL, FRWindowsDIBLoadFromDeviceSEL, (void*)CFR_WindowsDIB_V5::LoadFromDevice);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRWindowsDIBSEL, FRWindowsDIBSetToDeviceSEL, (void*)CFR_WindowsDIB_V5::SetToDevice);
	}
};

// fr_sysImpl.h end

// In file fr_viewImpl.h
class CFR_Annot_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAnnotSEL, FRAnnotGetPDFAnnotSEL, (void*)CFR_Annot_V5::GetPDFAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAnnotSEL, FRAnnotGetTypeSEL, (void*)CFR_Annot_V5::GetType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAnnotSEL, FRAnnotGetSubTypeSEL, (void*)CFR_Annot_V5::GetSubType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAnnotSEL, FRAnnotSetVisibleSEL, (void*)CFR_Annot_V5::SetVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAnnotSEL, FRAnnotGetPageVewSEL, (void*)CFR_Annot_V5::GetPageVew);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAnnotSEL, FRAnnotGetTabOrderSEL, (void*)CFR_Annot_V5::GetTabOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAnnotSEL, FRAnnotSetFlagsSEL, (void*)CFR_Annot_V5::SetFlags);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAnnotSEL, FRAnnotDrawAppearanceSEL, (void*)CFR_Annot_V5::DrawAppearance);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAnnotSEL, FRAnnotIsSelectedSEL, (void*)CFR_Annot_V5::IsSelected);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAnnotSEL, FRAnnotSetAuthorSEL, (void*)CFR_Annot_V5::SetAuthor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAnnotSEL, FRAnnotGetFRDocByAnnotSEL, (void*)CFR_Annot_V5::GetFRDocByAnnot);
	}
};

class CFR_ResourcePropertyBox_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRResourcePropertyBoxSEL, FRResourcePropertyBoxGetSEL, (void*)CFR_ResourcePropertyBox_V5::Get);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRResourcePropertyBoxSEL, FRResourcePropertyBoxRegisterPropertyPageSEL, (void*)CFR_ResourcePropertyBox_V5::RegisterPropertyPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRResourcePropertyBoxSEL, FRResourcePropertyBoxRegisterSourceTypeSEL, (void*)CFR_ResourcePropertyBox_V5::RegisterSourceType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRResourcePropertyBoxSEL, FRResourcePropertyBoxGetSourceTypeSEL, (void*)CFR_ResourcePropertyBox_V5::GetSourceType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRResourcePropertyBoxSEL, FRResourcePropertyBoxGetSourceFuncSEL, (void*)CFR_ResourcePropertyBox_V5::GetSourceFunc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRResourcePropertyBoxSEL, FRResourcePropertyBoxUpdatePropertyBoxSEL, (void*)CFR_ResourcePropertyBox_V5::UpdatePropertyBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRResourcePropertyBoxSEL, FRResourcePropertyBoxOpenPropertyBoxSEL, (void*)CFR_ResourcePropertyBox_V5::OpenPropertyBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRResourcePropertyBoxSEL, FRResourcePropertyBoxClosePropertyBoxSEL, (void*)CFR_ResourcePropertyBox_V5::ClosePropertyBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRResourcePropertyBoxSEL, FRResourcePropertyBoxIsPropertyBoxVisibleSEL, (void*)CFR_ResourcePropertyBox_V5::IsPropertyBoxVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRResourcePropertyBoxSEL, FRResourcePropertyBoxGetWndSEL, (void*)CFR_ResourcePropertyBox_V5::GetWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRResourcePropertyBoxSEL, FRResourcePropertyBoxGetPropertyPageSEL, (void*)CFR_ResourcePropertyBox_V5::GetPropertyPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRResourcePropertyBoxSEL, FRResourcePropertyBoxSetCurrentPropertyPageSEL, (void*)CFR_ResourcePropertyBox_V5::SetCurrentPropertyPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRResourcePropertyBoxSEL, FRResourcePropertyBoxRegisterPropertyPage2SEL, (void*)CFR_ResourcePropertyBox_V5::RegisterPropertyPage2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRResourcePropertyBoxSEL, FRResourcePropertyBoxDestroyPageSEL, (void*)CFR_ResourcePropertyBox_V5::DestroyPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRResourcePropertyBoxSEL, FRResourcePropertyBoxRegisterSourceType2SEL, (void*)CFR_ResourcePropertyBox_V5::RegisterSourceType2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRResourcePropertyBoxSEL, FRResourcePropertyBoxDestroySourceSEL, (void*)CFR_ResourcePropertyBox_V5::DestroySource);
	}
};

// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
class CFPD_FXFontEncoding_V5_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFXFontEncodingSEL, FPDFXFontEncodingNewSEL, (void*)CFPD_FXFontEncoding_V5::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFXFontEncodingSEL, FPDFXFontEncodingDestroySEL, (void*)CFPD_FXFontEncoding_V5::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFXFontEncodingSEL, FPDFXFontEncodingGlyphFromCharCodeSEL, (void*)CFPD_FXFontEncoding_V5::GlyphFromCharCode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFXFontEncodingSEL, FPDFXFontEncodingUnicodeFromCharCodeSEL, (void*)CFPD_FXFontEncoding_V5::UnicodeFromCharCode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFXFontEncodingSEL, FPDFXFontEncodingCharCodeFromUnicodeSEL, (void*)CFPD_FXFontEncoding_V5::CharCodeFromUnicode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFXFontEncodingSEL, FPDFXFontEncodingIsUnicodeCompatibleSEL, (void*)CFPD_FXFontEncoding_V5::IsUnicodeCompatible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFXFontEncodingSEL, FPDFXFontEncodingGlyphFromCharCodeExSEL, (void*)CFPD_FXFontEncoding_V5::GlyphFromCharCodeEx);
	}
};

// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V6----------
// In file fs_basicImpl.h
class CFS_FileStream_V6_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFileStreamSEL, FSFileStreamNewSEL, (void*)CFS_FileStream_V6::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFileStreamSEL, FSFileStreamNew2SEL, (void*)CFS_FileStream_V6::New2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFileStreamSEL, FSFileStreamDestroySEL, (void*)CFS_FileStream_V6::Destroy);
	}
};

// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
class CFR_ScrollBarThumbnailView_V6_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRScrollBarThumbnailViewSEL, FRScrollBarThumbnailViewGetCurPageIndexSEL, (void*)CFR_ScrollBarThumbnailView_V6::GetCurPageIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRScrollBarThumbnailViewSEL, FRScrollBarThumbnailViewGetPageRectSEL, (void*)CFR_ScrollBarThumbnailView_V6::GetPageRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRScrollBarThumbnailViewSEL, FRScrollBarThumbnailViewGetPDPageSEL, (void*)CFR_ScrollBarThumbnailView_V6::GetPDPage);
	}
};

// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V7----------
// In file fs_basicImpl.h
class CFS_GUID_V7_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSGUIDSEL, FSGUIDCreateSEL, (void*)CFS_GUID_V7::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSGUIDSEL, FSGUIDToStringSEL, (void*)CFS_GUID_V7::ToString);
	}
};

// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
class CFR_HTMLMgr_V7_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRHTMLMgrSEL, FRHTMLMgrGetSEL, (void*)CFR_HTMLMgr_V7::Get);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRHTMLMgrSEL, FRHTMLMgrRegisterHTMLEventHandlerSEL, (void*)CFR_HTMLMgr_V7::RegisterHTMLEventHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRHTMLMgrSEL, FRHTMLMgrOpenHTMLFromNewTabSEL, (void*)CFR_HTMLMgr_V7::OpenHTMLFromNewTab);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRHTMLMgrSEL, FRHTMLMgrOpenHTMLDocumentSEL, (void*)CFR_HTMLMgr_V7::OpenHTMLDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRHTMLMgrSEL, FRHTMLMgrGetActiveHtmlViewSEL, (void*)CFR_HTMLMgr_V7::GetActiveHtmlView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRHTMLMgrSEL, FRHTMLMgrRegisterFoxitBrowserEventHandlerSEL, (void*)CFR_HTMLMgr_V7::RegisterFoxitBrowserEventHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRHTMLMgrSEL, FRHTMLMgrSetFoxitBrowserHomeSEL, (void*)CFR_HTMLMgr_V7::SetFoxitBrowserHome);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRHTMLMgrSEL, FRHTMLMgrSetFavoritesLinkSEL, (void*)CFR_HTMLMgr_V7::SetFavoritesLink);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRHTMLMgrSEL, FRHTMLMgrGetHtmlViewFromHandleSEL, (void*)CFR_HTMLMgr_V7::GetHtmlViewFromHandle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRHTMLMgrSEL, FRHTMLMgrOpenHTMLFromNewTab2SEL, (void*)CFR_HTMLMgr_V7::OpenHTMLFromNewTab2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRHTMLMgrSEL, FRHTMLMgrOpenHTMLDocument2SEL, (void*)CFR_HTMLMgr_V7::OpenHTMLDocument2);
	}
};

class CFR_PanelMgr_V7_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrGetPanelMgrFromChildFrmSEL, (void*)CFR_PanelMgr_V7::GetPanelMgrFromChildFrm);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrNewPanelMgrSEL, (void*)CFR_PanelMgr_V7::NewPanelMgr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrDeletePanelMgrSEL, (void*)CFR_PanelMgr_V7::DeletePanelMgr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrCreatePanelEventHandlerSEL, (void*)CFR_PanelMgr_V7::CreatePanelEventHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrDestroyPanelEventHandlerSEL, (void*)CFR_PanelMgr_V7::DestroyPanelEventHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrCreateSEL, (void*)CFR_PanelMgr_V7::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrDockToFrameWindowSEL, (void*)CFR_PanelMgr_V7::DockToFrameWindow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrAddPanelSEL, (void*)CFR_PanelMgr_V7::AddPanel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrShowAllPanelSEL, (void*)CFR_PanelMgr_V7::ShowAllPanel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrShowPanelByNameSEL, (void*)CFR_PanelMgr_V7::ShowPanelByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrResetAllPanelsSEL, (void*)CFR_PanelMgr_V7::ResetAllPanels);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrGetPanelHwndByNameSEL, (void*)CFR_PanelMgr_V7::GetPanelHwndByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrIsAllPanelHideSEL, (void*)CFR_PanelMgr_V7::IsAllPanelHide);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrHasPanelFloatingSEL, (void*)CFR_PanelMgr_V7::HasPanelFloating);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrIsPanelHideSEL, (void*)CFR_PanelMgr_V7::IsPanelHide);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrGetParentFrameSEL, (void*)CFR_PanelMgr_V7::GetParentFrame);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrGetEnabledAlignmentSEL, (void*)CFR_PanelMgr_V7::GetEnabledAlignment);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrGetPanelViewByNameSEL, (void*)CFR_PanelMgr_V7::GetPanelViewByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrGetPanelNameByIndexSEL, (void*)CFR_PanelMgr_V7::GetPanelNameByIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrGetPanelCountSEL, (void*)CFR_PanelMgr_V7::GetPanelCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrGetPanelTabRectByPtSEL, (void*)CFR_PanelMgr_V7::GetPanelTabRectByPt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrGetPanelTabNameByPtSEL, (void*)CFR_PanelMgr_V7::GetPanelTabNameByPt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrGetPanelTabRectByNameSEL, (void*)CFR_PanelMgr_V7::GetPanelTabRectByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrHidePanelTabByNameSEL, (void*)CFR_PanelMgr_V7::HidePanelTabByName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrHasHistorySEL, (void*)CFR_PanelMgr_V7::HasHistory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrLockAllPanelSEL, (void*)CFR_PanelMgr_V7::LockAllPanel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrShowPanelByName2SEL, (void*)CFR_PanelMgr_V7::ShowPanelByName2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrIsPanelSpreadOutSEL, (void*)CFR_PanelMgr_V7::IsPanelSpreadOut);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrAddPanel2SEL, (void*)CFR_PanelMgr_V7::AddPanel2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrRedockAllFloatToInitialSEL, (void*)CFR_PanelMgr_V7::RedockAllFloatToInitial);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrIsPanelFloatingSEL, (void*)CFR_PanelMgr_V7::IsPanelFloating);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRPanelMgrSEL, FRPanelMgrShrinkPanelByNameSEL, (void*)CFR_PanelMgr_V7::ShrinkPanelByName);
	}
};

// fr_appImpl.h end

// In file fr_barImpl.h
class CFR_FuncBtn_V7_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFuncBtnSEL, FRFuncBtnCreateSEL, (void*)CFR_FuncBtn_V7::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFuncBtnSEL, FRFuncBtnAddToPanelSEL, (void*)CFR_FuncBtn_V7::AddToPanel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFuncBtnSEL, FRFuncBtnGetClientDataSEL, (void*)CFR_FuncBtn_V7::GetClientData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFuncBtnSEL, FRFuncBtnSetClientDataSEL, (void*)CFR_FuncBtn_V7::SetClientData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFuncBtnSEL, FRFuncBtnGetToolTipSEL, (void*)CFR_FuncBtn_V7::GetToolTip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFuncBtnSEL, FRFuncBtnSetToolTipSEL, (void*)CFR_FuncBtn_V7::SetToolTip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFuncBtnSEL, FRFuncBtnSetNameSEL, (void*)CFR_FuncBtn_V7::SetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFuncBtnSEL, FRFuncBtnGetNameSEL, (void*)CFR_FuncBtn_V7::GetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFuncBtnSEL, FRFuncBtnSetExecuteProcSEL, (void*)CFR_FuncBtn_V7::SetExecuteProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFuncBtnSEL, FRFuncBtnSetComputeEnabledProcSEL, (void*)CFR_FuncBtn_V7::SetComputeEnabledProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFuncBtnSEL, FRFuncBtnSetComputeMarkedProcSEL, (void*)CFR_FuncBtn_V7::SetComputeMarkedProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFuncBtnSEL, FRFuncBtnUpdateImageSEL, (void*)CFR_FuncBtn_V7::UpdateImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFuncBtnSEL, FRFuncBtnGetRectSEL, (void*)CFR_FuncBtn_V7::GetRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRFuncBtnSEL, FRFuncBtnAddToTabBandSEL, (void*)CFR_FuncBtn_V7::AddToTabBand);
	}
};

class CFR_StatusBar_V7_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRStatusBarSEL, FRStatusBarAddWndExSEL, (void*)CFR_StatusBar_V7::AddWndEx);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRStatusBarSEL, FRStatusBarResetSEL, (void*)CFR_StatusBar_V7::Reset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRStatusBarSEL, FRStatusBarSetZoomSliderRangeSEL, (void*)CFR_StatusBar_V7::SetZoomSliderRange);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRStatusBarSEL, FRStatusBarSetZoomSliderPosSEL, (void*)CFR_StatusBar_V7::SetZoomSliderPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRStatusBarSEL, FRStatusBarGetZoomSliderPosSEL, (void*)CFR_StatusBar_V7::GetZoomSliderPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRStatusBarSEL, FRStatusBarSetZoomPaneTextSEL, (void*)CFR_StatusBar_V7::SetZoomPaneText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRStatusBarSEL, FRStatusBarSetComboBoxPageCountSEL, (void*)CFR_StatusBar_V7::SetComboBoxPageCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRStatusBarSEL, FRStatusBarSelectComboBoxItemSEL, (void*)CFR_StatusBar_V7::SelectComboBoxItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRStatusBarSEL, FRStatusBarGetComboBoxPageIndexSEL, (void*)CFR_StatusBar_V7::GetComboBoxPageIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRStatusBarSEL, FRStatusBarGetComboBoxPageTextSEL, (void*)CFR_StatusBar_V7::GetComboBoxPageText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRStatusBarSEL, FRStatusBarGetBkGroundColorSEL, (void*)CFR_StatusBar_V7::GetBkGroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRStatusBarSEL, FRStatusBarGetBkGroundPathSEL, (void*)CFR_StatusBar_V7::GetBkGroundPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRStatusBarSEL, FRStatusBarRecalcLayoutSEL, (void*)CFR_StatusBar_V7::RecalcLayout);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRStatusBarSEL, FRStatusBarSetComboBoxPageCount2SEL, (void*)CFR_StatusBar_V7::SetComboBoxPageCount2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRStatusBarSEL, FRStatusBarSelectComboBoxItem2SEL, (void*)CFR_StatusBar_V7::SelectComboBoxItem2);
	}
};

// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
class CFPD_ConnectedInfo_V7_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoNewSEL, (void*)CFPD_ConnectedInfo_V7::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoDestroySEL, (void*)CFPD_ConnectedInfo_V7::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoSetIdSEL, (void*)CFPD_ConnectedInfo_V7::SetId);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoGetIdSEL, (void*)CFPD_ConnectedInfo_V7::GetId);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoIsConnectedPDFSEL, (void*)CFPD_ConnectedInfo_V7::IsConnectedPDF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoIsConnectedPDF2SEL, (void*)CFPD_ConnectedInfo_V7::IsConnectedPDF2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoSetTrackingSEL, (void*)CFPD_ConnectedInfo_V7::SetTracking);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoGetTrackingSEL, (void*)CFPD_ConnectedInfo_V7::GetTracking);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoUpdateSEL, (void*)CFPD_ConnectedInfo_V7::Update);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoSetEncryptOfflineSEL, (void*)CFPD_ConnectedInfo_V7::SetEncryptOffline);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoSetEncryptEnvelopeSEL, (void*)CFPD_ConnectedInfo_V7::SetEncryptEnvelope);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoGetEncryptOfflineSEL, (void*)CFPD_ConnectedInfo_V7::GetEncryptOffline);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoGetEncryptEnvelopeSEL, (void*)CFPD_ConnectedInfo_V7::GetEncryptEnvelope);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoSetOpenActionURLSEL, (void*)CFPD_ConnectedInfo_V7::SetOpenActionURL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoSetOpenActionURL2SEL, (void*)CFPD_ConnectedInfo_V7::SetOpenActionURL2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoCheckSettingOpenActionSEL, (void*)CFPD_ConnectedInfo_V7::CheckSettingOpenAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoDeleteOpenActionSEL, (void*)CFPD_ConnectedInfo_V7::DeleteOpenAction);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoSetEndpointSEL, (void*)CFPD_ConnectedInfo_V7::SetEndpoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDConnectedInfoSEL, FPDConnectedInfoGetEndpointSEL, (void*)CFPD_ConnectedInfo_V7::GetEndpoint);
	}
};

// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V8----------
// In file fs_basicImpl.h
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
class CFR_CustomSignature_V8_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCustomSignatureSEL, FRCustomSignatureGenerateSignInfoSEL, (void*)CFR_CustomSignature_V8::GenerateSignInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCustomSignatureSEL, FRCustomSignatureGetDefaultServerSEL, (void*)CFR_CustomSignature_V8::GetDefaultServer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCustomSignatureSEL, FRCustomSignatureCreateSignatureHandlerSEL, (void*)CFR_CustomSignature_V8::CreateSignatureHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCustomSignatureSEL, FRCustomSignatureRegisterSignatureHandlerSEL, (void*)CFR_CustomSignature_V8::RegisterSignatureHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCustomSignatureSEL, FRCustomSignatureDestroySignatureHandlerSEL, (void*)CFR_CustomSignature_V8::DestroySignatureHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCustomSignatureSEL, FRCustomSignatureSetSignatureVerifySEL, (void*)CFR_CustomSignature_V8::SetSignatureVerify);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCustomSignatureSEL, FRCustomSignatureGetDocSigatureCountSEL, (void*)CFR_CustomSignature_V8::GetDocSigatureCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCustomSignatureSEL, FRCustomSignatureGetSignatureBaseInfoSEL, (void*)CFR_CustomSignature_V8::GetSignatureBaseInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCustomSignatureSEL, FRCustomSignatureClearSignatureSEL, (void*)CFR_CustomSignature_V8::ClearSignature);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCustomSignatureSEL, FRCustomSignatureFoxitSignInScopeSEL, (void*)CFR_CustomSignature_V8::FoxitSignInScope);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCustomSignatureSEL, FRCustomSignatureFoixtCreateSignatureFSEL, (void*)CFR_CustomSignature_V8::FoixtCreateSignatureF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCustomSignatureSEL, FRCustomSignatureSetPositionSEL, (void*)CFR_CustomSignature_V8::SetPosition);
	}
};

// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V9----------
// In file fs_basicImpl.h
class CFS_UUID_V9_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUUIDSEL, FSUUIDGenerateSEL, (void*)CFS_UUID_V9::Generate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUUIDSEL, FSUUIDGenerateTimeSEL, (void*)CFS_UUID_V9::GenerateTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUUIDSEL, FSUUIDGenerateRandomSEL, (void*)CFS_UUID_V9::GenerateRandom);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUUIDSEL, FSUUIDSetTsPathSEL, (void*)CFS_UUID_V9::SetTsPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUUIDSEL, FSUUIDSetStateSEL, (void*)CFS_UUID_V9::SetState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSUUIDSEL, FSUUIDSetUserDataSEL, (void*)CFS_UUID_V9::SetUserData);
	}
};

class CFS_MapByteStringToPtr_V9_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapByteStringToPtrSEL, FSMapByteStringToPtrNewSEL, (void*)CFS_MapByteStringToPtr_V9::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapByteStringToPtrSEL, FSMapByteStringToPtrDestroySEL, (void*)CFS_MapByteStringToPtr_V9::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapByteStringToPtrSEL, FSMapByteStringToPtrGetCountSEL, (void*)CFS_MapByteStringToPtr_V9::GetCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapByteStringToPtrSEL, FSMapByteStringToPtrIsEmptySEL, (void*)CFS_MapByteStringToPtr_V9::IsEmpty);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapByteStringToPtrSEL, FSMapByteStringToPtrLookupSEL, (void*)CFS_MapByteStringToPtr_V9::Lookup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapByteStringToPtrSEL, FSMapByteStringToPtrSetAtSEL, (void*)CFS_MapByteStringToPtr_V9::SetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapByteStringToPtrSEL, FSMapByteStringToPtrRemoveKeySEL, (void*)CFS_MapByteStringToPtr_V9::RemoveKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapByteStringToPtrSEL, FSMapByteStringToPtrRemoveAllSEL, (void*)CFS_MapByteStringToPtr_V9::RemoveAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapByteStringToPtrSEL, FSMapByteStringToPtrGetStartPositionSEL, (void*)CFS_MapByteStringToPtr_V9::GetStartPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapByteStringToPtrSEL, FSMapByteStringToPtrGetNextAssocSEL, (void*)CFS_MapByteStringToPtr_V9::GetNextAssoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapByteStringToPtrSEL, FSMapByteStringToPtrGetNextValueSEL, (void*)CFS_MapByteStringToPtr_V9::GetNextValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapByteStringToPtrSEL, FSMapByteStringToPtrGetHashTableSizeSEL, (void*)CFS_MapByteStringToPtr_V9::GetHashTableSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapByteStringToPtrSEL, FSMapByteStringToPtrInitHashTableSEL, (void*)CFS_MapByteStringToPtr_V9::InitHashTable);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMapByteStringToPtrSEL, FSMapByteStringToPtrHashKeySEL, (void*)CFS_MapByteStringToPtr_V9::HashKey);
	}
};

// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
class CFR_CloudLoginProvider_V9_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCloudLoginProviderSEL, FRCloudLoginProviderSetSEL, (void*)CFR_CloudLoginProvider_V9::Set);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCloudLoginProviderSEL, FRCloudLoginProviderDestroySEL, (void*)CFR_CloudLoginProvider_V9::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCloudLoginProviderSEL, FRCloudLoginProviderGetSEL, (void*)CFR_CloudLoginProvider_V9::Get);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCloudLoginProviderSEL, FRCloudLoginProviderIsLogInSEL, (void*)CFR_CloudLoginProvider_V9::IsLogIn);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCloudLoginProviderSEL, FRCloudLoginProviderSignInSEL, (void*)CFR_CloudLoginProvider_V9::SignIn);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCloudLoginProviderSEL, FRCloudLoginProviderSignOutSEL, (void*)CFR_CloudLoginProvider_V9::SignOut);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCloudLoginProviderSEL, FRCloudLoginProviderGetUserInfoSEL, (void*)CFR_CloudLoginProvider_V9::GetUserInfo);
	}
};

// fr_appImpl.h end

// In file fr_barImpl.h
class CFR_BulbMsgCenter_V9_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRBulbMsgCenterSEL, FRBulbMsgCenterAddMessageSEL, (void*)CFR_BulbMsgCenter_V9::AddMessage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRBulbMsgCenterSEL, FRBulbMsgCenterShowMessageSEL, (void*)CFR_BulbMsgCenter_V9::ShowMessage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRBulbMsgCenterSEL, FRBulbMsgCenterSetCheckSEL, (void*)CFR_BulbMsgCenter_V9::SetCheck);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRBulbMsgCenterSEL, FRBulbMsgCenterSetOpBtnEnableSEL, (void*)CFR_BulbMsgCenter_V9::SetOpBtnEnable);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRBulbMsgCenterSEL, FRBulbMsgCenterIsMessageExistSEL, (void*)CFR_BulbMsgCenter_V9::IsMessageExist);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRBulbMsgCenterSEL, FRBulbMsgCenterIsOpBtnEnableSEL, (void*)CFR_BulbMsgCenter_V9::IsOpBtnEnable);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRBulbMsgCenterSEL, FRBulbMsgCenterSetOpBtnTitleSEL, (void*)CFR_BulbMsgCenter_V9::SetOpBtnTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRBulbMsgCenterSEL, FRBulbMsgCenterSetMessageContentSEL, (void*)CFR_BulbMsgCenter_V9::SetMessageContent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRBulbMsgCenterSEL, FRBulbMsgCenterGetButtonRectSEL, (void*)CFR_BulbMsgCenter_V9::GetButtonRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRBulbMsgCenterSEL, FRBulbMsgCenterAddMessage2SEL, (void*)CFR_BulbMsgCenter_V9::AddMessage2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRBulbMsgCenterSEL, FRBulbMsgCenterShowMessage2SEL, (void*)CFR_BulbMsgCenter_V9::ShowMessage2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRBulbMsgCenterSEL, FRBulbMsgCenterAddMessage3SEL, (void*)CFR_BulbMsgCenter_V9::AddMessage3);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRBulbMsgCenterSEL, FRBulbMsgCenterAddMessage4SEL, (void*)CFR_BulbMsgCenter_V9::AddMessage4);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRBulbMsgCenterSEL, FRBulbMsgCenterAddMessage5SEL, (void*)CFR_BulbMsgCenter_V9::AddMessage5);
	}
};

// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
class CFPD_EPUB_V9_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBOpenSEL, (void*)CFPD_EPUB_V9::Open);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBReleaseDocumentSEL, (void*)CFPD_EPUB_V9::ReleaseDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBCloseSEL, (void*)CFPD_EPUB_V9::Close);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBUnLoadDocPageSEL, (void*)CFPD_EPUB_V9::UnLoadDocPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBLoadPageSEL, (void*)CFPD_EPUB_V9::LoadPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBUnloadPageSEL, (void*)CFPD_EPUB_V9::UnloadPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBLoadSectionSEL, (void*)CFPD_EPUB_V9::LoadSection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBUnloadSectionSEL, (void*)CFPD_EPUB_V9::UnloadSection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetParserSEL, (void*)CFPD_EPUB_V9::GetParser);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetPageSEL, (void*)CFPD_EPUB_V9::GetPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetDocumentSectionSEL, (void*)CFPD_EPUB_V9::GetDocumentSection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBCountSectionsSEL, (void*)CFPD_EPUB_V9::CountSections);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetSectionIndexSEL, (void*)CFPD_EPUB_V9::GetSectionIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBCountPagesSEL, (void*)CFPD_EPUB_V9::CountPages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetBookmarkSEL, (void*)CFPD_EPUB_V9::GetBookmark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetNamedDestSEL, (void*)CFPD_EPUB_V9::GetNamedDest);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBReParseSEL, (void*)CFPD_EPUB_V9::ReParse);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetPageSectionSEL, (void*)CFPD_EPUB_V9::GetPageSection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetPageRectSEL, (void*)CFPD_EPUB_V9::GetPageRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetParentBookmarkSEL, (void*)CFPD_EPUB_V9::GetParentBookmark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBCountSubBookmarksSEL, (void*)CFPD_EPUB_V9::CountSubBookmarks);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetSubBookmarkSEL, (void*)CFPD_EPUB_V9::GetSubBookmark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetBookmarkStylesSEL, (void*)CFPD_EPUB_V9::GetBookmarkStyles);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetBookmarkTitleSEL, (void*)CFPD_EPUB_V9::GetBookmarkTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetBookmarkExternalDocNameSEL, (void*)CFPD_EPUB_V9::GetBookmarkExternalDocName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetBookmarkURISEL, (void*)CFPD_EPUB_V9::GetBookmarkURI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetBookmarkNamedStringSEL, (void*)CFPD_EPUB_V9::GetBookmarkNamedString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBIsBookmarkOpenedSEL, (void*)CFPD_EPUB_V9::IsBookmarkOpened);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBCreateTextSelectionSEL, (void*)CFPD_EPUB_V9::CreateTextSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBReleaseTextSelectionSEL, (void*)CFPD_EPUB_V9::ReleaseTextSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBTextSelectionAttachPageSEL, (void*)CFPD_EPUB_V9::TextSelectionAttachPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBTextSelectionResetSEL, (void*)CFPD_EPUB_V9::TextSelectionReset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBSetSelectStartPosSEL, (void*)CFPD_EPUB_V9::SetSelectStartPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetSelectStartPosSEL, (void*)CFPD_EPUB_V9::GetSelectStartPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetSelectCurPosSEL, (void*)CFPD_EPUB_V9::GetSelectCurPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBSelectWordSEL, (void*)CFPD_EPUB_V9::SelectWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBSelectRectangleTextSEL, (void*)CFPD_EPUB_V9::SelectRectangleText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBSelectParagraphTextSEL, (void*)CFPD_EPUB_V9::SelectParagraphText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBSelectNextCharSEL, (void*)CFPD_EPUB_V9::SelectNextChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBSelectPrevCharSEL, (void*)CFPD_EPUB_V9::SelectPrevChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBSelectNextWordSEL, (void*)CFPD_EPUB_V9::SelectNextWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBSelectPrevWordSEL, (void*)CFPD_EPUB_V9::SelectPrevWord);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBSelectNextLineSEL, (void*)CFPD_EPUB_V9::SelectNextLine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBSelectPrevLineSEL, (void*)CFPD_EPUB_V9::SelectPrevLine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBCountSelectRectsSEL, (void*)CFPD_EPUB_V9::CountSelectRects);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetSelectRectSEL, (void*)CFPD_EPUB_V9::GetSelectRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBCopySelectTextSEL, (void*)CFPD_EPUB_V9::CopySelectText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBCreateTextSearchSEL, (void*)CFPD_EPUB_V9::CreateTextSearch);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBReleaseTextSearchSEL, (void*)CFPD_EPUB_V9::ReleaseTextSearch);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBTextSearchAttachPageSEL, (void*)CFPD_EPUB_V9::TextSearchAttachPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBFindFirstSEL, (void*)CFPD_EPUB_V9::FindFirst);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBFindNextSEL, (void*)CFPD_EPUB_V9::FindNext);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBFindPrevSEL, (void*)CFPD_EPUB_V9::FindPrev);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetSearchPosSEL, (void*)CFPD_EPUB_V9::GetSearchPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBCountSearchRectsSEL, (void*)CFPD_EPUB_V9::CountSearchRects);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBGetSearchRectSEL, (void*)CFPD_EPUB_V9::GetSearchRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBTransfer2DocPointSEL, (void*)CFPD_EPUB_V9::Transfer2DocPoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBTransfer2DocPointRectSEL, (void*)CFPD_EPUB_V9::Transfer2DocPointRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBFDEGetPageMatrixSEL, (void*)CFPD_EPUB_V9::FDEGetPageMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBCreateRenderDeviceSEL, (void*)CFPD_EPUB_V9::CreateRenderDevice);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBReleaseRenderDeviceSEL, (void*)CFPD_EPUB_V9::ReleaseRenderDevice);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBRenderDeviceSetClipRectSEL, (void*)CFPD_EPUB_V9::RenderDeviceSetClipRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBCreateRenderContextSEL, (void*)CFPD_EPUB_V9::CreateRenderContext);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBReleaseRenderContextSEL, (void*)CFPD_EPUB_V9::ReleaseRenderContext);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBRenderContextRenderingSEL, (void*)CFPD_EPUB_V9::RenderContextRendering);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBDoRenderSEL, (void*)CFPD_EPUB_V9::DoRender);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBEpubArgbEncodeSEL, (void*)CFPD_EPUB_V9::EpubArgbEncode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBInitFDEMgrSEL, (void*)CFPD_EPUB_V9::InitFDEMgr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDEPUBSEL, FPDEPUBReleaseFDEMgrSEL, (void*)CFPD_EPUB_V9::ReleaseFDEMgr);
	}
};

// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V10----------
// In file fs_basicImpl.h
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
class CFOFD_CryptoDict_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictNewSEL, (void*)CFOFD_CryptoDict_V10::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictDestroySEL, (void*)CFOFD_CryptoDict_V10::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictGetAdminPwdSEL, (void*)CFOFD_CryptoDict_V10::GetAdminPwd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictGetUserPwdSEL, (void*)CFOFD_CryptoDict_V10::GetUserPwd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictGetSecurityTypeSEL, (void*)CFOFD_CryptoDict_V10::GetSecurityType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictGetCryptoTypeSEL, (void*)CFOFD_CryptoDict_V10::GetCryptoType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictGetFilterSEL, (void*)CFOFD_CryptoDict_V10::GetFilter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictGetSubFilterSEL, (void*)CFOFD_CryptoDict_V10::GetSubFilter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictGetCipherSEL, (void*)CFOFD_CryptoDict_V10::GetCipher);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictCountRecipientsSEL, (void*)CFOFD_CryptoDict_V10::CountRecipients);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictGetRecipientSEL, (void*)CFOFD_CryptoDict_V10::GetRecipient);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictGetKeyLengthSEL, (void*)CFOFD_CryptoDict_V10::GetKeyLength);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictSetAdminPwdSEL, (void*)CFOFD_CryptoDict_V10::SetAdminPwd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictSetUserPwdSEL, (void*)CFOFD_CryptoDict_V10::SetUserPwd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictSetSecurityTypeSEL, (void*)CFOFD_CryptoDict_V10::SetSecurityType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictSetCryptoTypeSEL, (void*)CFOFD_CryptoDict_V10::SetCryptoType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictSetFilterSEL, (void*)CFOFD_CryptoDict_V10::SetFilter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictSetSubFilterSEL, (void*)CFOFD_CryptoDict_V10::SetSubFilter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictSetCipherSEL, (void*)CFOFD_CryptoDict_V10::SetCipher);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictAddRecipientSEL, (void*)CFOFD_CryptoDict_V10::AddRecipient);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoDictSEL, FOFDCryptoDictSetKeyLengthSEL, (void*)CFOFD_CryptoDict_V10::SetKeyLength);
	}
};

class CFOFD_SecurityHandler_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDSecurityHandlerSEL, FOFDSecurityHandlerReleaseSEL, (void*)CFOFD_SecurityHandler_V10::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDSecurityHandlerSEL, FOFDSecurityHandlerIsAdminSEL, (void*)CFOFD_SecurityHandler_V10::IsAdmin);
	}
};

class CFOFD_StdSecurityHandler_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDStdSecurityHandlerSEL, FOFDStdSecurityHandlerCreateSEL, (void*)CFOFD_StdSecurityHandler_V10::Create);
	}
};

class CFOFD_StdCertSecurityHandler_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDStdCertSecurityHandlerSEL, FOFDStdCertSecurityHandlerCreateSEL, (void*)CFOFD_StdCertSecurityHandler_V10::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDStdCertSecurityHandlerSEL, FOFDStdCertSecurityHandlerSetPKCS12InfoSEL, (void*)CFOFD_StdCertSecurityHandler_V10::SetPKCS12Info);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDStdCertSecurityHandlerSEL, FOFDStdCertSecurityHandlerInitParserSEL, (void*)CFOFD_StdCertSecurityHandler_V10::InitParser);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDStdCertSecurityHandlerSEL, FOFDStdCertSecurityHandlerGetPermissonsSEL, (void*)CFOFD_StdCertSecurityHandler_V10::GetPermissons);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDStdCertSecurityHandlerSEL, FOFDStdCertSecurityHandlerInitCreatorSEL, (void*)CFOFD_StdCertSecurityHandler_V10::InitCreator);
	}
};

class CFOFD_SMSecurityHandler_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDSMSecurityHandlerSEL, FOFDSMSecurityHandlerCreateSEL, (void*)CFOFD_SMSecurityHandler_V10::Create);
	}
};

class CFOFD_CryptoHandler_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCryptoHandlerSEL, FOFDCryptoHandlerReleaseSEL, (void*)CFOFD_CryptoHandler_V10::Release);
	}
};

class CFOFD_StdCryptoHandler_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDStdCryptoHandlerSEL, FOFDStdCryptoHandlerCreateSEL, (void*)CFOFD_StdCryptoHandler_V10::Create);
	}
};

class CFOFD_SM4CryptoHandler_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDSM4CryptoHandlerSEL, FOFDSM4CryptoHandlerCreateSEL, (void*)CFOFD_SM4CryptoHandler_V10::Create);
	}
};

class CFOFD_FileStream_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDFileStreamSEL, FOFDFileStreamCreateMemoryStreamSEL, (void*)CFOFD_FileStream_V10::CreateMemoryStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDFileStreamSEL, FOFDFileStreamCreateMemoryStream2SEL, (void*)CFOFD_FileStream_V10::CreateMemoryStream2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDFileStreamSEL, FOFDFileStreamReleaseSEL, (void*)CFOFD_FileStream_V10::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDFileStreamSEL, FOFDFileStreamGetSizeSEL, (void*)CFOFD_FileStream_V10::GetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDFileStreamSEL, FOFDFileStreamIsEOFSEL, (void*)CFOFD_FileStream_V10::IsEOF);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDFileStreamSEL, FOFDFileStreamGetPositionSEL, (void*)CFOFD_FileStream_V10::GetPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDFileStreamSEL, FOFDFileStreamReadBlockSEL, (void*)CFOFD_FileStream_V10::ReadBlock);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDFileStreamSEL, FOFDFileStreamReadBlock2SEL, (void*)CFOFD_FileStream_V10::ReadBlock2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDFileStreamSEL, FOFDFileStreamWriteBlockSEL, (void*)CFOFD_FileStream_V10::WriteBlock);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDFileStreamSEL, FOFDFileStreamWriteBlock2SEL, (void*)CFOFD_FileStream_V10::WriteBlock2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDFileStreamSEL, FOFDFileStreamFlushSEL, (void*)CFOFD_FileStream_V10::Flush);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDFileStreamSEL, FOFDFileStreamGetCurrentFileNameSEL, (void*)CFOFD_FileStream_V10::GetCurrentFileName);
	}
};

class CFOFD_PauseHandler_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPauseHandlerSEL, FOFDPauseHandlerCreateSEL, (void*)CFOFD_PauseHandler_V10::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPauseHandlerSEL, FOFDPauseHandlerDestroySEL, (void*)CFOFD_PauseHandler_V10::Destroy);
	}
};

class CFOFD_UIMgr_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDUIMgrSEL, FOFDUIMgrSetPanelIndexSEL, (void*)CFOFD_UIMgr_V10::SetPanelIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDUIMgrSEL, FOFDUIMgrTriggerPanelSEL, (void*)CFOFD_UIMgr_V10::TriggerPanel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDUIMgrSEL, FOFDUIMgrFormatComboBoxSEL, (void*)CFOFD_UIMgr_V10::FormatComboBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDUIMgrSEL, FOFDUIMgrShowAutoHideStatusBarSEL, (void*)CFOFD_UIMgr_V10::ShowAutoHideStatusBar);
	}
};

class CFOFD_DIBAttribute_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeCreateSEL, (void*)CFOFD_DIBAttribute_V10::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeGetXDPISEL, (void*)CFOFD_DIBAttribute_V10::GetXDPI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeSetXDPISEL, (void*)CFOFD_DIBAttribute_V10::SetXDPI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeGetYDPISEL, (void*)CFOFD_DIBAttribute_V10::GetYDPI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeSetYDPISEL, (void*)CFOFD_DIBAttribute_V10::SetYDPI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeGetAspectRatioSEL, (void*)CFOFD_DIBAttribute_V10::GetAspectRatio);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeSetAspectRatioSEL, (void*)CFOFD_DIBAttribute_V10::SetAspectRatio);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeGetDPIUnitSEL, (void*)CFOFD_DIBAttribute_V10::GetDPIUnit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeSetDPIUnitSEL, (void*)CFOFD_DIBAttribute_V10::SetDPIUnit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeGetAuthorSEL, (void*)CFOFD_DIBAttribute_V10::GetAuthor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeSetAuthorSEL, (void*)CFOFD_DIBAttribute_V10::SetAuthor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeGetTimeSEL, (void*)CFOFD_DIBAttribute_V10::GetTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeSetTimeSEL, (void*)CFOFD_DIBAttribute_V10::SetTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeGetGifLeftSEL, (void*)CFOFD_DIBAttribute_V10::GetGifLeft);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeSetGifLeftSEL, (void*)CFOFD_DIBAttribute_V10::SetGifLeft);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeGetGifTopSEL, (void*)CFOFD_DIBAttribute_V10::GetGifTop);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeSetGifTopSEL, (void*)CFOFD_DIBAttribute_V10::SetGifTop);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeGetGifLocalPaletteSEL, (void*)CFOFD_DIBAttribute_V10::GetGifLocalPalette);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeSetGifLocalPaletteSEL, (void*)CFOFD_DIBAttribute_V10::SetGifLocalPalette);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeGetGifLocalPalNumSEL, (void*)CFOFD_DIBAttribute_V10::GetGifLocalPalNum);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeSetGifLocalPalNumSEL, (void*)CFOFD_DIBAttribute_V10::SetGifLocalPalNum);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeGetBmpCompressTypeSEL, (void*)CFOFD_DIBAttribute_V10::GetBmpCompressType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeSetBmpCompressTypeSEL, (void*)CFOFD_DIBAttribute_V10::SetBmpCompressType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeGetTiffFrameCompressTypeSEL, (void*)CFOFD_DIBAttribute_V10::GetTiffFrameCompressType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeSetTiffFrameCompressTypeSEL, (void*)CFOFD_DIBAttribute_V10::SetTiffFrameCompressType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeGetTiffFrameCompressOptionsSEL, (void*)CFOFD_DIBAttribute_V10::GetTiffFrameCompressOptions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeSetTiffFrameCompressOptionsSEL, (void*)CFOFD_DIBAttribute_V10::SetTiffFrameCompressOptions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeGetTiffFrameFillOrderSEL, (void*)CFOFD_DIBAttribute_V10::GetTiffFrameFillOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeSetTiffFrameFillOrderSEL, (void*)CFOFD_DIBAttribute_V10::SetTiffFrameFillOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeGetTiffFrameCompressJpegQualitySEL, (void*)CFOFD_DIBAttribute_V10::GetTiffFrameCompressJpegQuality);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDIBAttributeSEL, FOFDDIBAttributeSetTiffFrameCompressJpegQualitySEL, (void*)CFOFD_DIBAttribute_V10::SetTiffFrameCompressJpegQuality);
	}
};

class CFOFD_CodeC_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCodeCSEL, FOFDCodeCPngEncodeSEL, (void*)CFOFD_CodeC_V10::PngEncode);
	}
};

class CFOFD_PrintSetting_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPrintSettingSEL, FOFDPrintSettingGetGlobalUnitSEL, (void*)CFOFD_PrintSetting_V10::GetGlobalUnit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPrintSettingSEL, FOFDPrintSettingGetDefaultPrinterNameSEL, (void*)CFOFD_PrintSetting_V10::GetDefaultPrinterName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPrintSettingSEL, FOFDPrintSettingGetDefaultPrinterDevModeSEL, (void*)CFOFD_PrintSetting_V10::GetDefaultPrinterDevMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPrintSettingSEL, FOFDPrintSettingSetDefaultPrinterDevModeSEL, (void*)CFOFD_PrintSetting_V10::SetDefaultPrinterDevMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPrintSettingSEL, FOFDPrintSettingIsPrintUseGraphicsSEL, (void*)CFOFD_PrintSetting_V10::IsPrintUseGraphics);
	}
};

class CFOFD_Sys_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDSysSEL, FOFDSysInitSSLModuleSEL, (void*)CFOFD_Sys_V10::InitSSLModule);
	}
};

// fofd_basicImpl.h end

// In file fofd_docImpl.h
class CFOFD_Package_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPackageSEL, FOFDPackageCreateSEL, (void*)CFOFD_Package_V10::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPackageSEL, FOFDPackageCreate2SEL, (void*)CFOFD_Package_V10::Create2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPackageSEL, FOFDPackageReleaseSEL, (void*)CFOFD_Package_V10::Release);
	}
};

class CFOFD_Parser_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDParserSEL, FOFDParserCreateSEL, (void*)CFOFD_Parser_V10::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDParserSEL, FOFDParserReleaseSEL, (void*)CFOFD_Parser_V10::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDParserSEL, FOFDParserCountDocumentsSEL, (void*)CFOFD_Parser_V10::CountDocuments);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDParserSEL, FOFDParserGetDocumentSEL, (void*)CFOFD_Parser_V10::GetDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDParserSEL, FOFDParserGetDocument2SEL, (void*)CFOFD_Parser_V10::GetDocument2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDParserSEL, FOFDParserGetDocumentIndexSEL, (void*)CFOFD_Parser_V10::GetDocumentIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDParserSEL, FOFDParserGetCryptoDictSEL, (void*)CFOFD_Parser_V10::GetCryptoDict);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDParserSEL, FOFDParserCreate2SEL, (void*)CFOFD_Parser_V10::Create2);
	}
};

class CFOFD_Doc_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDocSEL, FOFDDocGetParserSEL, (void*)CFOFD_Doc_V10::GetParser);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDocSEL, FOFDDocCountPagesSEL, (void*)CFOFD_Doc_V10::CountPages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDocSEL, FOFDDocGetPageSEL, (void*)CFOFD_Doc_V10::GetPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDocSEL, FOFDDocGetPageByIDSEL, (void*)CFOFD_Doc_V10::GetPageByID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDocSEL, FOFDDocGetPageIndexSEL, (void*)CFOFD_Doc_V10::GetPageIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDocSEL, FOFDDocGenerateOutlineSEL, (void*)CFOFD_Doc_V10::GenerateOutline);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDocSEL, FOFDDocGetBookmarksSEL, (void*)CFOFD_Doc_V10::GetBookmarks);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDocSEL, FOFDDocGetVPreferencesSEL, (void*)CFOFD_Doc_V10::GetVPreferences);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDocSEL, FOFDDocGetPermissionsSEL, (void*)CFOFD_Doc_V10::GetPermissions);
	}
};

class CFOFD_WriteDoc_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDWriteDocSEL, FOFDWriteDocCreateSEL, (void*)CFOFD_WriteDoc_V10::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDWriteDocSEL, FOFDWriteDocReleaseSEL, (void*)CFOFD_WriteDoc_V10::Release);
	}
};

class CFOFD_Creator_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCreatorSEL, FOFDCreatorCreateSEL, (void*)CFOFD_Creator_V10::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCreatorSEL, FOFDCreatorReleaseSEL, (void*)CFOFD_Creator_V10::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCreatorSEL, FOFDCreatorInsertDocumentSEL, (void*)CFOFD_Creator_V10::InsertDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCreatorSEL, FOFDCreatorInitParserSEL, (void*)CFOFD_Creator_V10::InitParser);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDCreatorSEL, FOFDCreatorPackageSEL, (void*)CFOFD_Creator_V10::Package);
	}
};

class CFOFD_Perms_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPermsSEL, FOFDPermsIsEditSEL, (void*)CFOFD_Perms_V10::IsEdit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPermsSEL, FOFDPermsIsAnnotSEL, (void*)CFOFD_Perms_V10::IsAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPermsSEL, FOFDPermsIsExportSEL, (void*)CFOFD_Perms_V10::IsExport);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPermsSEL, FOFDPermsIsSignatureSEL, (void*)CFOFD_Perms_V10::IsSignature);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPermsSEL, FOFDPermsIsWatermarkSEL, (void*)CFOFD_Perms_V10::IsWatermark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPermsSEL, FOFDPermsIsPrintScreenSEL, (void*)CFOFD_Perms_V10::IsPrintScreen);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPermsSEL, FOFDPermsIsPrintableSEL, (void*)CFOFD_Perms_V10::IsPrintable);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPermsSEL, FOFDPermsGetPrintCopiesSEL, (void*)CFOFD_Perms_V10::GetPrintCopies);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPermsSEL, FOFDPermsGetStartDateSEL, (void*)CFOFD_Perms_V10::GetStartDate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPermsSEL, FOFDPermsGetEndDateSEL, (void*)CFOFD_Perms_V10::GetEndDate);
	}
};

class CFOFD_VPrefers_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDVPrefersSEL, FOFDVPrefersGetPageModeSEL, (void*)CFOFD_VPrefers_V10::GetPageMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDVPrefersSEL, FOFDVPrefersGetPageLayoutSEL, (void*)CFOFD_VPrefers_V10::GetPageLayout);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDVPrefersSEL, FOFDVPrefersGetTabDisplaySEL, (void*)CFOFD_VPrefers_V10::GetTabDisplay);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDVPrefersSEL, FOFDVPrefersIsHideToolbarSEL, (void*)CFOFD_VPrefers_V10::IsHideToolbar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDVPrefersSEL, FOFDVPrefersIsHideMenubarSEL, (void*)CFOFD_VPrefers_V10::IsHideMenubar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDVPrefersSEL, FOFDVPrefersIsHideWindowUISEL, (void*)CFOFD_VPrefers_V10::IsHideWindowUI);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDVPrefersSEL, FOFDVPrefersGetZoomModeSEL, (void*)CFOFD_VPrefers_V10::GetZoomMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDVPrefersSEL, FOFDVPrefersGetZoomSEL, (void*)CFOFD_VPrefers_V10::GetZoom);
	}
};

class CFOFD_Bookmarks_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDBookmarksSEL, FOFDBookmarksCountBookmarksSEL, (void*)CFOFD_Bookmarks_V10::CountBookmarks);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDBookmarksSEL, FOFDBookmarksGetBookmarkSEL, (void*)CFOFD_Bookmarks_V10::GetBookmark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDBookmarksSEL, FOFDBookmarksGetBookmark2SEL, (void*)CFOFD_Bookmarks_V10::GetBookmark2);
	}
};

class CFOFD_Bookmark_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDBookmarkSEL, FOFDBookmarkGetNameSEL, (void*)CFOFD_Bookmark_V10::GetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDBookmarkSEL, FOFDBookmarkGetDestSEL, (void*)CFOFD_Bookmark_V10::GetDest);
	}
};

class CFOFD_Outline_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDOutlineSEL, FOFDOutlineCreateSEL, (void*)CFOFD_Outline_V10::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDOutlineSEL, FOFDOutlineReleaseSEL, (void*)CFOFD_Outline_V10::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDOutlineSEL, FOFDOutlineIsNullSEL, (void*)CFOFD_Outline_V10::IsNull);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDOutlineSEL, FOFDOutlineIsIdenticalSEL, (void*)CFOFD_Outline_V10::IsIdentical);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDOutlineSEL, FOFDOutlineGenerateParentSEL, (void*)CFOFD_Outline_V10::GenerateParent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDOutlineSEL, FOFDOutlineCloneSEL, (void*)CFOFD_Outline_V10::Clone);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDOutlineSEL, FOFDOutlineCountSubOutlinesSEL, (void*)CFOFD_Outline_V10::CountSubOutlines);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDOutlineSEL, FOFDOutlineGenerateSubOutlineSEL, (void*)CFOFD_Outline_V10::GenerateSubOutline);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDOutlineSEL, FOFDOutlineIsExpandedSEL, (void*)CFOFD_Outline_V10::IsExpanded);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDOutlineSEL, FOFDOutlineGetTitleSEL, (void*)CFOFD_Outline_V10::GetTitle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDOutlineSEL, FOFDOutlineGetActionsSEL, (void*)CFOFD_Outline_V10::GetActions);
	}
};

class CFOFD_Actions_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionsSEL, FOFDActionsCountActionsSEL, (void*)CFOFD_Actions_V10::CountActions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionsSEL, FOFDActionsGetActionSEL, (void*)CFOFD_Actions_V10::GetAction);
	}
};

class CFOFD_Action_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionSEL, FOFDActionGetEventSEL, (void*)CFOFD_Action_V10::GetEvent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionSEL, FOFDActionGenerateActionRegionSEL, (void*)CFOFD_Action_V10::GenerateActionRegion);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionSEL, FOFDActionGetActionTypeSEL, (void*)CFOFD_Action_V10::GetActionType);
	}
};

class CFOFD_ActionGoto_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionGotoSEL, FOFDActionGotoGetDestSEL, (void*)CFOFD_ActionGoto_V10::GetDest);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionGotoSEL, FOFDActionGotoGetBookmarkSEL, (void*)CFOFD_ActionGoto_V10::GetBookmark);
	}
};

class CFOFD_ActionGotoA_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionGotoASEL, FOFDActionGotoAGetDestSEL, (void*)CFOFD_ActionGotoA_V10::GetDest);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionGotoASEL, FOFDActionGotoAGetBookmarkSEL, (void*)CFOFD_ActionGotoA_V10::GetBookmark);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionGotoASEL, FOFDActionGotoAGetAttachIDSEL, (void*)CFOFD_ActionGotoA_V10::GetAttachID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionGotoASEL, FOFDActionGotoAIsNewWindowSEL, (void*)CFOFD_ActionGotoA_V10::IsNewWindow);
	}
};

class CFOFD_ActionRegion_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionRegionSEL, FOFDActionRegionNewSEL, (void*)CFOFD_ActionRegion_V10::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionRegionSEL, FOFDActionRegionReleaseSEL, (void*)CFOFD_ActionRegion_V10::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionRegionSEL, FOFDActionRegionIsEmptySEL, (void*)CFOFD_ActionRegion_V10::IsEmpty);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionRegionSEL, FOFDActionRegionCountAreasSEL, (void*)CFOFD_ActionRegion_V10::CountAreas);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionRegionSEL, FOFDActionRegionGenerateAreaSEL, (void*)CFOFD_ActionRegion_V10::GenerateArea);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDActionRegionSEL, FOFDActionRegionGeneratePathSEL, (void*)CFOFD_ActionRegion_V10::GeneratePath);
	}
};

// fofd_docImpl.h end

// In file fofd_pageImpl.h
class CFOFD_Page_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPageSEL, FOFDPageGetIDSEL, (void*)CFOFD_Page_V10::GetID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPageSEL, FOFDPageGetDocumentSEL, (void*)CFOFD_Page_V10::GetDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPageSEL, FOFDPageGetPageRectSEL, (void*)CFOFD_Page_V10::GetPageRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPageSEL, FOFDPageGetPageRotateSEL, (void*)CFOFD_Page_V10::GetPageRotate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPageSEL, FOFDPageLoadPageSEL, (void*)CFOFD_Page_V10::LoadPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPageSEL, FOFDPageUnloadPageSEL, (void*)CFOFD_Page_V10::UnloadPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPageSEL, FOFDPageIsPageLoadedSEL, (void*)CFOFD_Page_V10::IsPageLoaded);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPageSEL, FOFDPageReleaseCatchImageSEL, (void*)CFOFD_Page_V10::ReleaseCatchImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPageSEL, FOFDPageGetMatrixSEL, (void*)CFOFD_Page_V10::GetMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDPageSEL, FOFDPageGetCropPageRectSEL, (void*)CFOFD_Page_V10::GetCropPageRect);
	}
};

class CFOFD_Dest_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDestSEL, FOFDDestCreateSEL, (void*)CFOFD_Dest_V10::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDestSEL, FOFDDestReleaseSEL, (void*)CFOFD_Dest_V10::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDestSEL, FOFDDestIsValidSEL, (void*)CFOFD_Dest_V10::IsValid);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDestSEL, FOFDDestGetPageIDSEL, (void*)CFOFD_Dest_V10::GetPageID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDestSEL, FOFDDestGetDestTypeSEL, (void*)CFOFD_Dest_V10::GetDestType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDestSEL, FOFDDestGetLeftSEL, (void*)CFOFD_Dest_V10::GetLeft);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDestSEL, FOFDDestGetTopSEL, (void*)CFOFD_Dest_V10::GetTop);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDestSEL, FOFDDestGetRightSEL, (void*)CFOFD_Dest_V10::GetRight);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDestSEL, FOFDDestGetBottomSEL, (void*)CFOFD_Dest_V10::GetBottom);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDDestSEL, FOFDDestGetZoomSEL, (void*)CFOFD_Dest_V10::GetZoom);
	}
};

// fofd_pageImpl.h end

// In file fofd_renderImpl.h
class CFOFD_RenderOptions_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderOptionsSEL, FOFDRenderOptionsCreateSEL, (void*)CFOFD_RenderOptions_V10::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderOptionsSEL, FOFDRenderOptionsReleaseSEL, (void*)CFOFD_RenderOptions_V10::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderOptionsSEL, FOFDRenderOptionsSetColorModeSEL, (void*)CFOFD_RenderOptions_V10::SetColorMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderOptionsSEL, FOFDRenderOptionsGetColorModeSEL, (void*)CFOFD_RenderOptions_V10::GetColorMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderOptionsSEL, FOFDRenderOptionsSetPrintingSEL, (void*)CFOFD_RenderOptions_V10::SetPrinting);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderOptionsSEL, FOFDRenderOptionsGetPrintingSEL, (void*)CFOFD_RenderOptions_V10::GetPrinting);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderOptionsSEL, FOFDRenderOptionsSetBackColorSEL, (void*)CFOFD_RenderOptions_V10::SetBackColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderOptionsSEL, FOFDRenderOptionsGetBackColorSEL, (void*)CFOFD_RenderOptions_V10::GetBackColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderOptionsSEL, FOFDRenderOptionsSetForeColorSEL, (void*)CFOFD_RenderOptions_V10::SetForeColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderOptionsSEL, FOFDRenderOptionsGetForeColorSEL, (void*)CFOFD_RenderOptions_V10::GetForeColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderOptionsSEL, FOFDRenderOptionsSetRotateSEL, (void*)CFOFD_RenderOptions_V10::SetRotate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderOptionsSEL, FOFDRenderOptionsGetRotateSEL, (void*)CFOFD_RenderOptions_V10::GetRotate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderOptionsSEL, FOFDRenderOptionsSetRenderFlagSEL, (void*)CFOFD_RenderOptions_V10::SetRenderFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderOptionsSEL, FOFDRenderOptionsGetRenderFlagSEL, (void*)CFOFD_RenderOptions_V10::GetRenderFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderOptionsSEL, FOFDRenderOptionsSetLayerFlagSEL, (void*)CFOFD_RenderOptions_V10::SetLayerFlag);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderOptionsSEL, FOFDRenderOptionsGetLayerFlagSEL, (void*)CFOFD_RenderOptions_V10::GetLayerFlag);
	}
};

class CFOFD_RenderContext_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderContextSEL, FOFDRenderContextCreateSEL, (void*)CFOFD_RenderContext_V10::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderContextSEL, FOFDRenderContextReleaseSEL, (void*)CFOFD_RenderContext_V10::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderContextSEL, FOFDRenderContextAddPageSEL, (void*)CFOFD_RenderContext_V10::AddPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderContextSEL, FOFDRenderContextAddPageObjectSEL, (void*)CFOFD_RenderContext_V10::AddPageObject);
	}
};

class CFOFD_RenderDevice_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceNewSEL, (void*)CFOFD_RenderDevice_V10::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceDestroySEL, (void*)CFOFD_RenderDevice_V10::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceSaveBitmapSEL, (void*)CFOFD_RenderDevice_V10::SaveBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceCreateSEL, (void*)CFOFD_RenderDevice_V10::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceCreate2SEL, (void*)CFOFD_RenderDevice_V10::Create2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceGetWidthSEL, (void*)CFOFD_RenderDevice_V10::GetWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceGetHeightSEL, (void*)CFOFD_RenderDevice_V10::GetHeight);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceGetDeviceClassSEL, (void*)CFOFD_RenderDevice_V10::GetDeviceClass);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceGetBPPSEL, (void*)CFOFD_RenderDevice_V10::GetBPP);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceGetRenderCapsSEL, (void*)CFOFD_RenderDevice_V10::GetRenderCaps);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceGetDeviceCapsSEL, (void*)CFOFD_RenderDevice_V10::GetDeviceCaps);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceGetDpiXSEL, (void*)CFOFD_RenderDevice_V10::GetDpiX);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceGetDpiYSEL, (void*)CFOFD_RenderDevice_V10::GetDpiY);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceGetBitmapSEL, (void*)CFOFD_RenderDevice_V10::GetBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceSetBitmapSEL, (void*)CFOFD_RenderDevice_V10::SetBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceCreateCompatibleBitmapSEL, (void*)CFOFD_RenderDevice_V10::CreateCompatibleBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceGetClipBoxSEL, (void*)CFOFD_RenderDevice_V10::GetClipBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceSetClip_PathFillSEL, (void*)CFOFD_RenderDevice_V10::SetClip_PathFill);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceSaveStateSEL, (void*)CFOFD_RenderDevice_V10::SaveState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceRestoreStateSEL, (void*)CFOFD_RenderDevice_V10::RestoreState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceSetClip_RectSEL, (void*)CFOFD_RenderDevice_V10::SetClip_Rect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceFillRectSEL, (void*)CFOFD_RenderDevice_V10::FillRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceGetDIBitsSEL, (void*)CFOFD_RenderDevice_V10::GetDIBits);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceSetDIBitsSEL, (void*)CFOFD_RenderDevice_V10::SetDIBits);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceStretchDIBitsSEL, (void*)CFOFD_RenderDevice_V10::StretchDIBits);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceStartDIBitsSEL, (void*)CFOFD_RenderDevice_V10::StartDIBits);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceContinueDIBitsSEL, (void*)CFOFD_RenderDevice_V10::ContinueDIBits);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceCancelDIBitsSEL, (void*)CFOFD_RenderDevice_V10::CancelDIBits);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceSaveDeviceSEL, (void*)CFOFD_RenderDevice_V10::SaveDevice);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceRestoreDeviceSEL, (void*)CFOFD_RenderDevice_V10::RestoreDevice);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceSetClipPathFillSEL, (void*)CFOFD_RenderDevice_V10::SetClipPathFill);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceSetRenderOptionsSEL, (void*)CFOFD_RenderDevice_V10::SetRenderOptions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceGetRenderDeviceSEL, (void*)CFOFD_RenderDevice_V10::GetRenderDevice);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDRenderDeviceSEL, FOFDRenderDeviceGetDriverDeviceSEL, (void*)CFOFD_RenderDevice_V10::GetDriverDevice);
	}
};

class CFOFD_ProgressiveRenderer_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDProgressiveRendererSEL, FOFDProgressiveRendererCreateSEL, (void*)CFOFD_ProgressiveRenderer_V10::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDProgressiveRendererSEL, FOFDProgressiveRendererReleaseSEL, (void*)CFOFD_ProgressiveRenderer_V10::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDProgressiveRendererSEL, FOFDProgressiveRendererStartRenderSEL, (void*)CFOFD_ProgressiveRenderer_V10::StartRender);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDProgressiveRendererSEL, FOFDProgressiveRendererContinueSEL, (void*)CFOFD_ProgressiveRenderer_V10::Continue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDProgressiveRendererSEL, FOFDProgressiveRendererGetStatusSEL, (void*)CFOFD_ProgressiveRenderer_V10::GetStatus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDProgressiveRendererSEL, FOFDProgressiveRendererDoRenderSEL, (void*)CFOFD_ProgressiveRenderer_V10::DoRender);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDProgressiveRendererSEL, FOFDProgressiveRendererRenderAnnotsSEL, (void*)CFOFD_ProgressiveRenderer_V10::RenderAnnots);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDProgressiveRendererSEL, FOFDProgressiveRendererRenderAnnotSEL, (void*)CFOFD_ProgressiveRenderer_V10::RenderAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDProgressiveRendererSEL, FOFDProgressiveRendererRenderStampAnnotsSEL, (void*)CFOFD_ProgressiveRenderer_V10::RenderStampAnnots);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDProgressiveRendererSEL, FOFDProgressiveRendererStopRenderSEL, (void*)CFOFD_ProgressiveRenderer_V10::StopRender);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDProgressiveRendererSEL, FOFDProgressiveRendererGetPrinterBackgourdSEL, (void*)CFOFD_ProgressiveRenderer_V10::GetPrinterBackgourd);
	}
};

// fofd_renderImpl.h end

// In file fofd_sigImpl.h
class CFOFD_Sign_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FOFDSignSEL, FOFDSignCreateSealBitmapSEL, (void*)CFOFD_Sign_V10::CreateSealBitmap);
	}
};

// fofd_sigImpl.h end

// In file fpd_docImpl.h
class CFPD_WrapperDoc_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDWrapperDocSEL, FPDWrapperDocNewSEL, (void*)CFPD_WrapperDoc_V10::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDWrapperDocSEL, FPDWrapperDocDestroySEL, (void*)CFPD_WrapperDoc_V10::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDWrapperDocSEL, FPDWrapperDocGetWrapperTypeSEL, (void*)CFPD_WrapperDoc_V10::GetWrapperType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDWrapperDocSEL, FPDWrapperDocGetCryptographicFilterSEL, (void*)CFPD_WrapperDoc_V10::GetCryptographicFilter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDWrapperDocSEL, FPDWrapperDocGetPayLoadSizeSEL, (void*)CFPD_WrapperDoc_V10::GetPayLoadSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDWrapperDocSEL, FPDWrapperDocGetPayloadFileNameSEL, (void*)CFPD_WrapperDoc_V10::GetPayloadFileName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDWrapperDocSEL, FPDWrapperDocStartGetPayloadSEL, (void*)CFPD_WrapperDoc_V10::StartGetPayload);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDWrapperDocSEL, FPDWrapperDocContinueSEL, (void*)CFPD_WrapperDoc_V10::Continue);
	}
};

// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
class CFPD_UnencryptedWrapperCreator_V10_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDUnencryptedWrapperCreatorSEL, FPDUnencryptedWrapperCreatorNewSEL, (void*)CFPD_UnencryptedWrapperCreator_V10::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDUnencryptedWrapperCreatorSEL, FPDUnencryptedWrapperCreatorDestroySEL, (void*)CFPD_UnencryptedWrapperCreator_V10::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDUnencryptedWrapperCreatorSEL, FPDUnencryptedWrapperCreatorSetPayloadInfoSEL, (void*)CFPD_UnencryptedWrapperCreator_V10::SetPayloadInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDUnencryptedWrapperCreatorSEL, FPDUnencryptedWrapperCreatorSetPayLoadSEL, (void*)CFPD_UnencryptedWrapperCreator_V10::SetPayLoad);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDUnencryptedWrapperCreatorSEL, FPDUnencryptedWrapperCreatorCreateSEL, (void*)CFPD_UnencryptedWrapperCreator_V10::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDUnencryptedWrapperCreatorSEL, FPDUnencryptedWrapperCreatorContinueSEL, (void*)CFPD_UnencryptedWrapperCreator_V10::Continue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDUnencryptedWrapperCreatorSEL, FPDUnencryptedWrapperCreatorSetStandardSecuritySEL, (void*)CFPD_UnencryptedWrapperCreator_V10::SetStandardSecurity);
	}
};

// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V11----------
// In file fs_basicImpl.h
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
class CFR_AppFxNet_V11_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAppFxNetSEL, FRAppFxNetRegisterAppEventHandlerFxNetSEL, (void*)CFR_AppFxNet_V11::RegisterAppEventHandlerFxNet);
	}
};

class CFR_InternalFxNet_V11_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetIsFRDGLoginSEL, (void*)CFR_InternalFxNet_V11::IsFRDGLogin);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetShowLoginDlgSEL, (void*)CFR_InternalFxNet_V11::ShowLoginDlg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetDllGetTicketSEL, (void*)CFR_InternalFxNet_V11::DllGetTicket);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetCheckActionPermissionSEL, (void*)CFR_InternalFxNet_V11::CheckActionPermission);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetCheckPRDTimestampPermissionSEL, (void*)CFR_InternalFxNet_V11::CheckPRDTimestampPermission);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetCheckPermissionByFaceNameSEL, (void*)CFR_InternalFxNet_V11::CheckPermissionByFaceName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetGetLoginHwndSEL, (void*)CFR_InternalFxNet_V11::GetLoginHwnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetPreTranMsgToSdkSEL, (void*)CFR_InternalFxNet_V11::PreTranMsgToSdk);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetDllShowPaymentDlgSEL, (void*)CFR_InternalFxNet_V11::DllShowPaymentDlg);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetGetUserIdSEL, (void*)CFR_InternalFxNet_V11::GetUserId);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetGetNickNameSEL, (void*)CFR_InternalFxNet_V11::GetNickName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetGetHeadIconSEL, (void*)CFR_InternalFxNet_V11::GetHeadIcon);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetGetUserTypeSEL, (void*)CFR_InternalFxNet_V11::GetUserType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetAddAnnotToMarkupPanelSEL, (void*)CFR_InternalFxNet_V11::AddAnnotToMarkupPanel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetShowAnnotNoteSEL, (void*)CFR_InternalFxNet_V11::ShowAnnotNote);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetGetCurrentImageSelectObjSEL, (void*)CFR_InternalFxNet_V11::GetCurrentImageSelectObj);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetGetImageBufSEL, (void*)CFR_InternalFxNet_V11::GetImageBuf);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInternalFxNetSEL, FRInternalFxNetGetOCRLanInfoSEL, (void*)CFR_InternalFxNet_V11::GetOCRLanInfo);
	}
};

// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
class CFPD_ColorSeparator_V11_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSeparatorSEL, FPDColorSeparatorNewSEL, (void*)CFPD_ColorSeparator_V11::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSeparatorSEL, FPDColorSeparatorDestroySEL, (void*)CFPD_ColorSeparator_V11::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSeparatorSEL, FPDColorSeparatorCountColorantsSEL, (void*)CFPD_ColorSeparator_V11::CountColorants);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSeparatorSEL, FPDColorSeparatorGetColorantNameSEL, (void*)CFPD_ColorSeparator_V11::GetColorantName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSeparatorSEL, FPDColorSeparatorSeparateColorantSEL, (void*)CFPD_ColorSeparator_V11::SeparateColorant);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSeparatorSEL, FPDColorSeparatorGetResultDocSEL, (void*)CFPD_ColorSeparator_V11::GetResultDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorSeparatorSEL, FPDColorSeparatorSetColorConverterSEL, (void*)CFPD_ColorSeparator_V11::SetColorConverter);
	}
};

// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
class CFPD_ColorConvertor_V11_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorConvertorSEL, FPDColorConvertorNewSEL, (void*)CFPD_ColorConvertor_V11::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorConvertorSEL, FPDColorConvertorDestroySEL, (void*)CFPD_ColorConvertor_V11::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorConvertorSEL, FPDColorConvertorSetICCProfilesPathSEL, (void*)CFPD_ColorConvertor_V11::SetICCProfilesPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorConvertorSEL, FPDColorConvertorGetObjColorSpaceSEL, (void*)CFPD_ColorConvertor_V11::GetObjColorSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorConvertorSEL, FPDColorConvertorIsCalibrateColorSpaceSEL, (void*)CFPD_ColorConvertor_V11::IsCalibrateColorSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorConvertorSEL, FPDColorConvertorDecalibrateColorSEL, (void*)CFPD_ColorConvertor_V11::DecalibrateColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDColorConvertorSEL, FPDColorConvertorConvertColorSEL, (void*)CFPD_ColorConvertor_V11::ConvertColor);
	}
};

// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V12----------
// In file fs_basicImpl.h
class CFS_Image_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageCreateSEL, (void*)CFS_Image_V12::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageReleaseSEL, (void*)CFS_Image_V12::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageLoadSEL, (void*)CFS_Image_V12::Load);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageGetTypeSEL, (void*)CFS_Image_V12::GetType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageEnableTransparentSEL, (void*)CFS_Image_V12::EnableTransparent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageSetBackgroundColorSEL, (void*)CFS_Image_V12::SetBackgroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageSetOpacitySEL, (void*)CFS_Image_V12::SetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageCountFramesSEL, (void*)CFS_Image_V12::CountFrames);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageLoadFrameSEL, (void*)CFS_Image_V12::LoadFrame);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageNeedLoadPrevFrameSEL, (void*)CFS_Image_V12::NeedLoadPrevFrame);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageGetFrameSizeSEL, (void*)CFS_Image_V12::GetFrameSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageGetFrameSEL, (void*)CFS_Image_V12::GetFrame);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageGetFrameColorKeySEL, (void*)CFS_Image_V12::GetFrameColorKey);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageGetFrameWithTransparencySEL, (void*)CFS_Image_V12::GetFrameWithTransparency);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageGetFrameRawDataSEL, (void*)CFS_Image_V12::GetFrameRawData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageGetFrameCompressSEL, (void*)CFS_Image_V12::GetFrameCompress);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageGetFramePhotoInterpretSEL, (void*)CFS_Image_V12::GetFramePhotoInterpret);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageFreeSEL, (void*)CFS_Image_V12::Free);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageGetAttributeSEL, (void*)CFS_Image_V12::GetAttribute);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageGetWidthSEL, (void*)CFS_Image_V12::GetWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageGetHeightSEL, (void*)CFS_Image_V12::GetHeight);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageCreateObjectSEL, (void*)CFS_Image_V12::CreateObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageInsertToPDFPageSEL, (void*)CFS_Image_V12::InsertToPDFPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSImageSEL, FSImageInsertToPDFDocumentSEL, (void*)CFS_Image_V12::InsertToPDFDocument);
	}
};

// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
class CFR_AssistantMgr_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAssistantMgrSEL, FRAssistantMgrGetSEL, (void*)CFR_AssistantMgr_V12::Get);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAssistantMgrSEL, FRAssistantMgrSetCurAsstTrackWndSEL, (void*)CFR_AssistantMgr_V12::SetCurAsstTrackWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAssistantMgrSEL, FRAssistantMgrUpdateAssistantHandlerSEL, (void*)CFR_AssistantMgr_V12::UpdateAssistantHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAssistantMgrSEL, FRAssistantMgrNotificationSEL, (void*)CFR_AssistantMgr_V12::Notification);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAssistantMgrSEL, FRAssistantMgrClearNotificationSEL, (void*)CFR_AssistantMgr_V12::ClearNotification);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAssistantMgrSEL, FRAssistantMgrRegisterAsstUIHandlerSEL, (void*)CFR_AssistantMgr_V12::RegisterAsstUIHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAssistantMgrSEL, FRAssistantMgrUnRegisterAsstUIHandlerSEL, (void*)CFR_AssistantMgr_V12::UnRegisterAsstUIHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAssistantMgrSEL, FRAssistantMgrRegisterAssistantHandlerSEL, (void*)CFR_AssistantMgr_V12::RegisterAssistantHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRAssistantMgrSEL, FRAssistantMgrUnRegisterAssistantHandlerSEL, (void*)CFR_AssistantMgr_V12::UnRegisterAssistantHandler);
	}
};

// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
class CFPD_3dContext_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dContextSEL, FPD3dContextNewSEL, (void*)CFPD_3dContext_V12::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dContextSEL, FPD3dContextDestroySEL, (void*)CFPD_3dContext_V12::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dContextSEL, FPD3dContextInitDefaultEngineSEL, (void*)CFPD_3dContext_V12::InitDefaultEngine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dContextSEL, FPD3dContextInitializeWithEngineSEL, (void*)CFPD_3dContext_V12::InitializeWithEngine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dContextSEL, FPD3dContextRegisterFormatLoaderSEL, (void*)CFPD_3dContext_V12::RegisterFormatLoader);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dContextSEL, FPD3dContextGetDocumentSEL, (void*)CFPD_3dContext_V12::GetDocument);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dContextSEL, FPD3dContextGetEngineSEL, (void*)CFPD_3dContext_V12::GetEngine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dContextSEL, FPD3dContextIsPDF20SEL, (void*)CFPD_3dContext_V12::IsPDF20);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dContextSEL, FPD3dContextLoadAnnotDataSEL, (void*)CFPD_3dContext_V12::LoadAnnotData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dContextSEL, FPD3dContextRegisterScriptProviderSEL, (void*)CFPD_3dContext_V12::RegisterScriptProvider);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dContextSEL, FPD3dContextRegisterCompositionProviderSEL, (void*)CFPD_3dContext_V12::RegisterCompositionProvider);
	}
};

class CFPD_3dAnnotData_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dAnnotDataSEL, FPD3dAnnotDataAs3DArtworkSEL, (void*)CFPD_3dAnnotData_V12::As3DArtwork);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dAnnotDataSEL, FPD3dAnnotDataAsRichMediaSEL, (void*)CFPD_3dAnnotData_V12::AsRichMedia);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dAnnotDataSEL, FPD3dAnnotDataReleaseSEL, (void*)CFPD_3dAnnotData_V12::Release);
	}
};

class CFPD_3dAnnotData3dArtwork_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dAnnotData3dArtworkSEL, FPD3dAnnotData3dArtworkDeactivateSEL, (void*)CFPD_3dAnnotData3dArtwork_V12::Deactivate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dAnnotData3dArtworkSEL, FPD3dAnnotData3dArtworkGetSceneSEL, (void*)CFPD_3dAnnotData3dArtwork_V12::GetScene);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dAnnotData3dArtworkSEL, FPD3dAnnotData3dArtworkActivateAndAllocCanvasEmbeddedSEL, (void*)CFPD_3dAnnotData3dArtwork_V12::ActivateAndAllocCanvasEmbedded);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dAnnotData3dArtworkSEL, FPD3dAnnotData3dArtworkAllocCanvasEmbeddedSEL, (void*)CFPD_3dAnnotData3dArtwork_V12::AllocCanvasEmbedded);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dAnnotData3dArtworkSEL, FPD3dAnnotData3dArtworkGetActivationOptionsSEL, (void*)CFPD_3dAnnotData3dArtwork_V12::GetActivationOptions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dAnnotData3dArtworkSEL, FPD3dAnnotData3dArtworkSetActivationOptionsSEL, (void*)CFPD_3dAnnotData3dArtwork_V12::SetActivationOptions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dAnnotData3dArtworkSEL, FPD3dAnnotData3dArtworkLoadActivationOptionsSEL, (void*)CFPD_3dAnnotData3dArtwork_V12::LoadActivationOptions);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dAnnotData3dArtworkSEL, FPD3dAnnotData3dArtworkReleaseCanvasSEL, (void*)CFPD_3dAnnotData3dArtwork_V12::ReleaseCanvas);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dAnnotData3dArtworkSEL, FPD3dAnnotData3dArtworkGetAssetSEL, (void*)CFPD_3dAnnotData3dArtwork_V12::GetAsset);
	}
};

class CFPD_3dScene_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dSceneSEL, FPD3dSceneGetAssetForAnnotDataSEL, (void*)CFPD_3dScene_V12::GetAssetForAnnotData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dSceneSEL, FPD3dSceneGetFormatSEL, (void*)CFPD_3dScene_V12::GetFormat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dSceneSEL, FPD3dSceneRetrievePresetViewListSEL, (void*)CFPD_3dScene_V12::RetrievePresetViewList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dSceneSEL, FPD3dSceneGetDefaultPresetViewSEL, (void*)CFPD_3dScene_V12::GetDefaultPresetView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dSceneSEL, FPD3dSceneUpdatePresetViewListSEL, (void*)CFPD_3dScene_V12::UpdatePresetViewList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dSceneSEL, FPD3dSceneCreatePresetViewSEL, (void*)CFPD_3dScene_V12::CreatePresetView);
	}
};

class CFPD_3deAsset_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deAssetSEL, FPD3deAssetGetParentNodeSEL, (void*)CFPD_3deAsset_V12::GetParentNode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deAssetSEL, FPD3deAssetGetNextSiblingNodeSEL, (void*)CFPD_3deAsset_V12::GetNextSiblingNode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deAssetSEL, FPD3deAssetGetFirstChildNodeSEL, (void*)CFPD_3deAsset_V12::GetFirstChildNode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deAssetSEL, FPD3deAssetGetNodeNameSEL, (void*)CFPD_3deAsset_V12::GetNodeName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deAssetSEL, FPD3deAssetNarrowScopeSEL, (void*)CFPD_3deAsset_V12::NarrowScope);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deAssetSEL, FPD3deAssetGetRootNodeSEL, (void*)CFPD_3deAsset_V12::GetRootNode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deAssetSEL, FPD3deAssetGetNodeMetaDataCountSEL, (void*)CFPD_3deAsset_V12::GetNodeMetaDataCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deAssetSEL, FPD3deAssetGetNodeMetaDataItemSEL, (void*)CFPD_3deAsset_V12::GetNodeMetaDataItem);
	}
};

class CFPD_3deRuntime_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deRuntimeSEL, FPD3deRuntimeCreateRuntimeSEL, (void*)CFPD_3deRuntime_V12::CreateRuntime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deRuntimeSEL, FPD3deRuntimeCreateRuntimeWithBackendSEL, (void*)CFPD_3deRuntime_V12::CreateRuntimeWithBackend);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deRuntimeSEL, FPD3deRuntimeCreateAssetSEL, (void*)CFPD_3deRuntime_V12::CreateAsset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deRuntimeSEL, FPD3deRuntimeTickSEL, (void*)CFPD_3deRuntime_V12::Tick);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deRuntimeSEL, FPD3deRuntimeCountInstantiatedCanvasesSEL, (void*)CFPD_3deRuntime_V12::CountInstantiatedCanvases);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deRuntimeSEL, FPD3deRuntimeQueryAvailableBackendListSEL, (void*)CFPD_3deRuntime_V12::QueryAvailableBackendList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deRuntimeSEL, FPD3deRuntimeGetBackendFriendlyNameSEL, (void*)CFPD_3deRuntime_V12::GetBackendFriendlyName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deRuntimeSEL, FPD3deRuntimeUpdateTextProviderSEL, (void*)CFPD_3deRuntime_V12::UpdateTextProvider);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deRuntimeSEL, FPD3deRuntimeUpdatePreference_AngleUnitDisplaySEL, (void*)CFPD_3deRuntime_V12::UpdatePreference_AngleUnitDisplay);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deRuntimeSEL, FPD3deRuntimeUpdatePreference_NodeMetadataStoragePathSEL, (void*)CFPD_3deRuntime_V12::UpdatePreference_NodeMetadataStoragePath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deRuntimeSEL, FPD3deRuntimeUpdateProvider_I18nSEL, (void*)CFPD_3deRuntime_V12::UpdateProvider_I18n);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deRuntimeSEL, FPD3deRuntimeUpdatePreference_AntiAliasingSEL, (void*)CFPD_3deRuntime_V12::UpdatePreference_AntiAliasing);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deRuntimeSEL, FPD3deRuntimeQueryPreference_ForceDoubleSidedRenderingSEL, (void*)CFPD_3deRuntime_V12::QueryPreference_ForceDoubleSidedRendering);
	}
};

class CFPD_3deCanvas_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasGetNativeWindowSEL, (void*)CFPD_3deCanvas_V12::GetNativeWindow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasInstantiateSEL, (void*)CFPD_3deCanvas_V12::Instantiate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasInstantiateAndLiveSEL, (void*)CFPD_3deCanvas_V12::InstantiateAndLive);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasUninstantiateSEL, (void*)CFPD_3deCanvas_V12::Uninstantiate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasGetStateSEL, (void*)CFPD_3deCanvas_V12::GetState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasAcquireCurrentViewSEL, (void*)CFPD_3deCanvas_V12::AcquireCurrentView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasReleaseCurrentViewSEL, (void*)CFPD_3deCanvas_V12::ReleaseCurrentView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasGetPrimaryAssetSEL, (void*)CFPD_3deCanvas_V12::GetPrimaryAsset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasSetControllerToolSEL, (void*)CFPD_3deCanvas_V12::SetControllerTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasRelocateInPlaceSEL, (void*)CFPD_3deCanvas_V12::RelocateInPlace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasSetNotificationCallback_ContextMenuSEL, (void*)CFPD_3deCanvas_V12::SetNotificationCallback_ContextMenu);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasSetNotificationCallback_PointerFocusSEL, (void*)CFPD_3deCanvas_V12::SetNotificationCallback_PointerFocus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasHaveAnimationSEL, (void*)CFPD_3deCanvas_V12::HaveAnimation);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasIsAnimationPlayingSEL, (void*)CFPD_3deCanvas_V12::IsAnimationPlaying);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasToggleAnimationSEL, (void*)CFPD_3deCanvas_V12::ToggleAnimation);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasSeekAnimationProgressSEL, (void*)CFPD_3deCanvas_V12::SeekAnimationProgress);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasGetAnimationProgressSEL, (void*)CFPD_3deCanvas_V12::GetAnimationProgress);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasGetChosenNodeSEL, (void*)CFPD_3deCanvas_V12::GetChosenNode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasSetChosenNodeSEL, (void*)CFPD_3deCanvas_V12::SetChosenNode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasSetNotificationCallback_ClaimMeasureSEL, (void*)CFPD_3deCanvas_V12::SetNotificationCallback_ClaimMeasure);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasGetChosenMeasureSEL, (void*)CFPD_3deCanvas_V12::GetChosenMeasure);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasSetChosenMeasureSEL, (void*)CFPD_3deCanvas_V12::SetChosenMeasure);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasSEL, FPD3deCanvasSnapshotSEL, (void*)CFPD_3deCanvas_V12::Snapshot);
	}
};

class CFPD_3deView_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewGetNameSEL, (void*)CFPD_3deView_V12::GetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewGetDisplayNameSEL, (void*)CFPD_3deView_V12::GetDisplayName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewGetCenterOfOrbitSEL, (void*)CFPD_3deView_V12::GetCenterOfOrbit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewGetProjectionSEL, (void*)CFPD_3deView_V12::GetProjection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewGetBackgroundSEL, (void*)CFPD_3deView_V12::GetBackground);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewGetRenderModeSEL, (void*)CFPD_3deView_V12::GetRenderMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewGetLightingSchemeSEL, (void*)CFPD_3deView_V12::GetLightingScheme);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewSetCameraToWorldMatrixWithNodeSEL, (void*)CFPD_3deView_V12::SetCameraToWorldMatrixWithNode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewSetCenterOfOrbitSEL, (void*)CFPD_3deView_V12::SetCenterOfOrbit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewSetProjectionSEL, (void*)CFPD_3deView_V12::SetProjection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewSetBackgroundSEL, (void*)CFPD_3deView_V12::SetBackground);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewSetRenderModeSEL, (void*)CFPD_3deView_V12::SetRenderMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewSetLightingSchemeSEL, (void*)CFPD_3deView_V12::SetLightingScheme);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewImportViewConfigSEL, (void*)CFPD_3deView_V12::ImportViewConfig);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewSetNameAndDisplayNameSEL, (void*)CFPD_3deView_V12::SetNameAndDisplayName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewSetCrossSectionsSEL, (void*)CFPD_3deView_V12::SetCrossSections);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewClearViewConfigSEL, (void*)CFPD_3deView_V12::ClearViewConfig);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewGetCrossSectionsSEL, (void*)CFPD_3deView_V12::GetCrossSections);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewClearAndImportViewConfigSEL, (void*)CFPD_3deView_V12::ClearAndImportViewConfig);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewGetMeasurementsSEL, (void*)CFPD_3deView_V12::GetMeasurements);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewSetMeasurementsSEL, (void*)CFPD_3deView_V12::SetMeasurements);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewSetTrackingViewSEL, (void*)CFPD_3deView_V12::SetTrackingView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewSEL, FPD3deViewF3DViewGetDictSEL, (void*)CFPD_3deView_V12::F3DViewGetDict);
	}
};

class CFPD_3deViewRenderMode_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewRenderModeSEL, FPD3deViewRenderModeSwitchKindSEL, (void*)CFPD_3deViewRenderMode_V12::SwitchKind);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewRenderModeSEL, FPD3deViewRenderModeCreateSEL, (void*)CFPD_3deViewRenderMode_V12::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewRenderModeSEL, FPD3deViewRenderModeReleaseSEL, (void*)CFPD_3deViewRenderMode_V12::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewRenderModeSEL, FPD3deViewRenderModeQueryModeTypeSEL, (void*)CFPD_3deViewRenderMode_V12::QueryModeType);
	}
};

class CFPD_3deViewLightingScheme_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewLightingSchemeSEL, FPD3deViewLightingSchemeCreateSEL, (void*)CFPD_3deViewLightingScheme_V12::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewLightingSchemeSEL, FPD3deViewLightingSchemeDestroySEL, (void*)CFPD_3deViewLightingScheme_V12::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewLightingSchemeSEL, FPD3deViewLightingSchemeQuerySchemeTypeSEL, (void*)CFPD_3deViewLightingScheme_V12::QuerySchemeType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewLightingSchemeSEL, FPD3deViewLightingSchemeSwitchSchemeTypeSEL, (void*)CFPD_3deViewLightingScheme_V12::SwitchSchemeType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewLightingSchemeSEL, FPD3deViewLightingSchemeQueryArtworkSchemeExistSEL, (void*)CFPD_3deViewLightingScheme_V12::QueryArtworkSchemeExist);
	}
};

class CFPD_3deCanvasControllerTool_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasControllerToolSEL, FPD3deCanvasControllerToolGetRotateToolSEL, (void*)CFPD_3deCanvasControllerTool_V12::GetRotateTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasControllerToolSEL, FPD3deCanvasControllerToolGetSpinToolSEL, (void*)CFPD_3deCanvasControllerTool_V12::GetSpinTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasControllerToolSEL, FPD3deCanvasControllerToolGetPanToolSEL, (void*)CFPD_3deCanvasControllerTool_V12::GetPanTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasControllerToolSEL, FPD3deCanvasControllerToolGetZoomToolSEL, (void*)CFPD_3deCanvasControllerTool_V12::GetZoomTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasControllerToolSEL, FPD3deCanvasControllerToolGetWalkToolSEL, (void*)CFPD_3deCanvasControllerTool_V12::GetWalkTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasControllerToolSEL, FPD3deCanvasControllerToolGetFlyToolSEL, (void*)CFPD_3deCanvasControllerTool_V12::GetFlyTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasControllerToolSEL, FPD3deCanvasControllerToolGetSimpleControllerToolSEL, (void*)CFPD_3deCanvasControllerTool_V12::GetSimpleControllerTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasControllerToolSEL, FPD3deCanvasControllerToolCreateCommentControllerToolSEL, (void*)CFPD_3deCanvasControllerTool_V12::CreateCommentControllerTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasControllerToolSEL, FPD3deCanvasControllerToolReleaseCommentControllerToolSEL, (void*)CFPD_3deCanvasControllerTool_V12::ReleaseCommentControllerTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasControllerToolSEL, FPD3deCanvasControllerToolCreateMeasureControllerToolSEL, (void*)CFPD_3deCanvasControllerTool_V12::CreateMeasureControllerTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasControllerToolSEL, FPD3deCanvasControllerToolReleaseMeasureControllerToolSEL, (void*)CFPD_3deCanvasControllerTool_V12::ReleaseMeasureControllerTool);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasControllerToolSEL, FPD3deCanvasControllerToolSetMeasureControllerTooMeasureTypeSEL, (void*)CFPD_3deCanvasControllerTool_V12::SetMeasureControllerTooMeasureType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deCanvasControllerToolSEL, FPD3deCanvasControllerToolSetMeasureScaleSEL, (void*)CFPD_3deCanvasControllerTool_V12::SetMeasureScale);
	}
};

class CFPD_3dVendor_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dVendorSEL, FPD3dVendorCreateHandlerSEL, (void*)CFPD_3dVendor_V12::CreateHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dVendorSEL, FPD3dVendorReleaseHandlerSEL, (void*)CFPD_3dVendor_V12::ReleaseHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dVendorSEL, FPD3dVendorSetPDFModuleMgrIfNecessarySEL, (void*)CFPD_3dVendor_V12::SetPDFModuleMgrIfNecessary);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dVendorSEL, FPD3dVendorSetTempFileCallbackSEL, (void*)CFPD_3dVendor_V12::SetTempFileCallback);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dVendorSEL, FPD3dVendorModelLoaderSEL, (void*)CFPD_3dVendor_V12::ModelLoader);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dVendorSEL, FPD3dVendorPRCLoaderSEL, (void*)CFPD_3dVendor_V12::PRCLoader);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3dVendorSEL, FPD3dVendorGetNodeMetadataStorageKeySEL, (void*)CFPD_3dVendor_V12::GetNodeMetadataStorageKey);
	}
};

class CFPD_3deViewBackground_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewBackgroundSEL, FPD3deViewBackgroundCreateSEL, (void*)CFPD_3deViewBackground_V12::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewBackgroundSEL, FPD3deViewBackgroundReleaseSEL, (void*)CFPD_3deViewBackground_V12::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewBackgroundSEL, FPD3deViewBackgroundSetBackgroundColorSEL, (void*)CFPD_3deViewBackground_V12::SetBackgroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewBackgroundSEL, FPD3deViewBackgroundQueryBackgroundColorSEL, (void*)CFPD_3deViewBackground_V12::QueryBackgroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewBackgroundSEL, FPD3deViewBackgroundDefaultBackgroundColorSEL, (void*)CFPD_3deViewBackground_V12::DefaultBackgroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewBackgroundSEL, FPD3deViewBackgroundQueryEntireAnnotationSEL, (void*)CFPD_3deViewBackground_V12::QueryEntireAnnotation);
	}
};

class CFPD_3deViewCameraParam_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCameraParamSEL, FPD3deViewCameraParamQueryProjectionModeSEL, (void*)CFPD_3deViewCameraParam_V12::QueryProjectionMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCameraParamSEL, FPD3deViewCameraParamSwitchProjectionModeSEL, (void*)CFPD_3deViewCameraParam_V12::SwitchProjectionMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCameraParamSEL, FPD3deViewCameraParamImportCameraConfig1SEL, (void*)CFPD_3deViewCameraParam_V12::ImportCameraConfig1);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCameraParamSEL, FPD3deViewCameraParamImportCameraConfig2SEL, (void*)CFPD_3deViewCameraParam_V12::ImportCameraConfig2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCameraParamSEL, FPD3deViewCameraParamExportCameraConfigSEL, (void*)CFPD_3deViewCameraParam_V12::ExportCameraConfig);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCameraParamSEL, FPD3deViewCameraParamFitToVisibleSEL, (void*)CFPD_3deViewCameraParam_V12::FitToVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCameraParamSEL, FPD3deViewCameraParamZoomToPartSEL, (void*)CFPD_3deViewCameraParam_V12::ZoomToPart);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCameraParamSEL, FPD3deViewCameraParamIsolateNodeSEL, (void*)CFPD_3deViewCameraParam_V12::IsolateNode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCameraParamSEL, FPD3deViewCameraParamIsolateParentNodeSEL, (void*)CFPD_3deViewCameraParam_V12::IsolateParentNode);
	}
};

class CFPD_3deViewCrossSection_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionToggleCrossSectionSEL, (void*)CFPD_3deViewCrossSection_V12::ToggleCrossSection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionImportCrossSectionConfigSEL, (void*)CFPD_3deViewCrossSection_V12::ImportCrossSectionConfig);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionExportCrossSectionConfigSEL, (void*)CFPD_3deViewCrossSection_V12::ExportCrossSectionConfig);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionGetCenterPosSEL, (void*)CFPD_3deViewCrossSection_V12::GetCenterPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionSetCenterPosSEL, (void*)CFPD_3deViewCrossSection_V12::SetCenterPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionGetOrientationSEL, (void*)CFPD_3deViewCrossSection_V12::GetOrientation);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionSetOrientationSEL, (void*)CFPD_3deViewCrossSection_V12::SetOrientation);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionGetPlaneSEL, (void*)CFPD_3deViewCrossSection_V12::GetPlane);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionSetPlaneSEL, (void*)CFPD_3deViewCrossSection_V12::SetPlane);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionGetIntersectionSEL, (void*)CFPD_3deViewCrossSection_V12::GetIntersection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionSetIntersectionSEL, (void*)CFPD_3deViewCrossSection_V12::SetIntersection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionGetShowTransparentSEL, (void*)CFPD_3deViewCrossSection_V12::GetShowTransparent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionSetShowTransparentSEL, (void*)CFPD_3deViewCrossSection_V12::SetShowTransparent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionGetSectionCappingSEL, (void*)CFPD_3deViewCrossSection_V12::GetSectionCapping);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionSetSectionCappingSEL, (void*)CFPD_3deViewCrossSection_V12::SetSectionCapping);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionGetIgnoreChosenNodeSEL, (void*)CFPD_3deViewCrossSection_V12::GetIgnoreChosenNode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionSetIgnoreChosenNodeSEL, (void*)CFPD_3deViewCrossSection_V12::SetIgnoreChosenNode);
	}
};

class CFPD_3deViewNode_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewNodeSEL, FPD3deViewNodeStartModifyNodeSEL, (void*)CFPD_3deViewNode_V12::StartModifyNode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewNodeSEL, FPD3deViewNodeFinishModifyNodeSEL, (void*)CFPD_3deViewNode_V12::FinishModifyNode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewNodeSEL, FPD3deViewNodeStartQueryNodeSEL, (void*)CFPD_3deViewNode_V12::StartQueryNode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewNodeSEL, FPD3deViewNodeFinishQueryNodeSEL, (void*)CFPD_3deViewNode_V12::FinishQueryNode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewNodeSEL, FPD3deViewNodeGetNameSEL, (void*)CFPD_3deViewNode_V12::GetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewNodeSEL, FPD3deViewNodeSetNameSEL, (void*)CFPD_3deViewNode_V12::SetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewNodeSEL, FPD3deViewNodeGetVisibilitySEL, (void*)CFPD_3deViewNode_V12::GetVisibility);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewNodeSEL, FPD3deViewNodeSetVisibilitySEL, (void*)CFPD_3deViewNode_V12::SetVisibility);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewNodeSEL, FPD3deViewNodeGetOpacitySEL, (void*)CFPD_3deViewNode_V12::GetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewNodeSEL, FPD3deViewNodeSetOpacitySEL, (void*)CFPD_3deViewNode_V12::SetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewNodeSEL, FPD3deViewNodeGetMatrixSEL, (void*)CFPD_3deViewNode_V12::GetMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewNodeSEL, FPD3deViewNodeSetMatrixSEL, (void*)CFPD_3deViewNode_V12::SetMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewNodeSEL, FPD3deViewNodeGetRenderModeSEL, (void*)CFPD_3deViewNode_V12::GetRenderMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewNodeSEL, FPD3deViewNodeSetRenderModeSEL, (void*)CFPD_3deViewNode_V12::SetRenderMode);
	}
};

class CFPD_3deMeasure_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetTypeSEL, (void*)CFPD_3deMeasure_V12::GetType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetViewSEL, (void*)CFPD_3deMeasure_V12::GetView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetNameSEL, (void*)CFPD_3deMeasure_V12::GetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetPlaneSEL, (void*)CFPD_3deMeasure_V12::GetPlane);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetAnchorPointsSEL, (void*)CFPD_3deMeasure_V12::GetAnchorPoints);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureReleaseFS3DeVectorArraySEL, (void*)CFPD_3deMeasure_V12::ReleaseFS3DeVectorArray);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetAnchorNamesSEL, (void*)CFPD_3deMeasure_V12::GetAnchorNames);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetExtraAnchorPointsSEL, (void*)CFPD_3deMeasure_V12::GetExtraAnchorPoints);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetDirVectorsSEL, (void*)CFPD_3deMeasure_V12::GetDirVectors);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetTextPosSEL, (void*)CFPD_3deMeasure_V12::GetTextPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetTextXDirSEL, (void*)CFPD_3deMeasure_V12::GetTextXDir);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetTextYDirSEL, (void*)CFPD_3deMeasure_V12::GetTextYDir);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetTextBoxSizeSEL, (void*)CFPD_3deMeasure_V12::GetTextBoxSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetExtensionLineLengthSEL, (void*)CFPD_3deMeasure_V12::GetExtensionLineLength);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetTextSizeSEL, (void*)CFPD_3deMeasure_V12::GetTextSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetColorSEL, (void*)CFPD_3deMeasure_V12::GetColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetValueSEL, (void*)CFPD_3deMeasure_V12::GetValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetUnitsSEL, (void*)CFPD_3deMeasure_V12::GetUnits);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetPrecisionSEL, (void*)CFPD_3deMeasure_V12::GetPrecision);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetUserTextSEL, (void*)CFPD_3deMeasure_V12::GetUserText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetAngleTypeSEL, (void*)CFPD_3deMeasure_V12::GetAngleType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetShowCircleSEL, (void*)CFPD_3deMeasure_V12::GetShowCircle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetRadialTypeSEL, (void*)CFPD_3deMeasure_V12::GetRadialType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureSetNameSEL, (void*)CFPD_3deMeasure_V12::SetName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureSetTextPosSEL, (void*)CFPD_3deMeasure_V12::SetTextPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureSetAnchorPointsAndNamesSEL, (void*)CFPD_3deMeasure_V12::SetAnchorPointsAndNames);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureSetPlaneSEL, (void*)CFPD_3deMeasure_V12::SetPlane);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureSetTextXDirSEL, (void*)CFPD_3deMeasure_V12::SetTextXDir);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureSetTextYDirSEL, (void*)CFPD_3deMeasure_V12::SetTextYDir);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureSetValueSEL, (void*)CFPD_3deMeasure_V12::SetValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureSetColorSEL, (void*)CFPD_3deMeasure_V12::SetColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureSetAngularShowArcSEL, (void*)CFPD_3deMeasure_V12::SetAngularShowArc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureSetDirVectorsSEL, (void*)CFPD_3deMeasure_V12::SetDirVectors);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureSetUserTextSEL, (void*)CFPD_3deMeasure_V12::SetUserText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureSetShowSEL, (void*)CFPD_3deMeasure_V12::SetShow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureSetTextBoxSizeSEL, (void*)CFPD_3deMeasure_V12::SetTextBoxSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureSetUnitsSEL, (void*)CFPD_3deMeasure_V12::SetUnits);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureControllerGetCurrentMeasureSEL, (void*)CFPD_3deMeasure_V12::ControllerGetCurrentMeasure);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureGetCurrentCollectedPointCountSEL, (void*)CFPD_3deMeasure_V12::GetCurrentCollectedPointCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureSetMeasureValueChangeCallbackSEL, (void*)CFPD_3deMeasure_V12::SetMeasureValueChangeCallback);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deMeasureSEL, FPD3deMeasureMeasure_GetDictSEL, (void*)CFPD_3deMeasure_V12::Measure_GetDict);
	}
};

class CFPD_3deViewMiscOptions_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewMiscOptionsSEL, FPD3deViewMiscOptionsSetHighlightColorSEL, (void*)CFPD_3deViewMiscOptions_V12::SetHighlightColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewMiscOptionsSEL, FPD3deViewMiscOptionsSetBoundingBoxVisibleSEL, (void*)CFPD_3deViewMiscOptions_V12::SetBoundingBoxVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewMiscOptionsSEL, FPD3deViewMiscOptionsSetBoundingBoxColorSEL, (void*)CFPD_3deViewMiscOptions_V12::SetBoundingBoxColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewMiscOptionsSEL, FPD3deViewMiscOptionsSetShowGridSEL, (void*)CFPD_3deViewMiscOptions_V12::SetShowGrid);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewMiscOptionsSEL, FPD3deViewMiscOptionsGetShowGridSEL, (void*)CFPD_3deViewMiscOptions_V12::GetShowGrid);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewMiscOptionsSEL, FPD3deViewMiscOptionsSetGridModeSEL, (void*)CFPD_3deViewMiscOptions_V12::SetGridMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewMiscOptionsSEL, FPD3deViewMiscOptionsGetGridModeSEL, (void*)CFPD_3deViewMiscOptions_V12::GetGridMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewMiscOptionsSEL, FPD3deViewMiscOptionsSetGridSizeSEL, (void*)CFPD_3deViewMiscOptions_V12::SetGridSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deViewMiscOptionsSEL, FPD3deViewMiscOptionsGetGridSizeSEL, (void*)CFPD_3deViewMiscOptions_V12::GetGridSize);
	}
};

class CFPD_3deTextProvider_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deTextProviderSEL, FPD3deTextProviderCreateSEL, (void*)CFPD_3deTextProvider_V12::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deTextProviderSEL, FPD3deTextProviderReleaseSEL, (void*)CFPD_3deTextProvider_V12::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3deTextProviderSEL, FPD3deTextProviderSetTextScalingRatioSEL, (void*)CFPD_3deTextProvider_V12::SetTextScalingRatio);
	}
};

class CFPD_ScriptHostHostProvider_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDScriptHostHostProviderSEL, FPDScriptHostHostProviderNewSEL, (void*)CFPD_ScriptHostHostProvider_V12::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDScriptHostHostProviderSEL, FPDScriptHostHostProviderDestroySEL, (void*)CFPD_ScriptHostHostProvider_V12::Destroy);
	}
};

class CFPD_Script3DEngine_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDScript3DEngineSEL, FPDScript3DEngineNewSEL, (void*)CFPD_Script3DEngine_V12::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDScript3DEngineSEL, FPDScript3DEngineDestroySEL, (void*)CFPD_Script3DEngine_V12::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDScript3DEngineSEL, FPDScript3DEngineGet3DScriptContextSEL, (void*)CFPD_Script3DEngine_V12::Get3DScriptContext);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDScript3DEngineSEL, FPDScript3DEngineRegisterHostProviderSEL, (void*)CFPD_Script3DEngine_V12::RegisterHostProvider);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDScript3DEngineSEL, FPDScript3DEngineGet3DScriptProviderSEL, (void*)CFPD_Script3DEngine_V12::Get3DScriptProvider);
	}
};

class CFPD_3DI18NProviderHandler_V12_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3DI18NProviderHandlerSEL, FPD3DI18NProviderHandlerNewSEL, (void*)CFPD_3DI18NProviderHandler_V12::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3DI18NProviderHandlerSEL, FPD3DI18NProviderHandlerDestroySEL, (void*)CFPD_3DI18NProviderHandler_V12::Destroy);
	}
};

// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V13----------
// In file fs_basicImpl.h
class CFS_ReaderDateTime_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeCreateDateTimeDefaultSEL, (void*)CFS_ReaderDateTime_V13::CreateDateTimeDefault);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeCreateDateTimeStrSEL, (void*)CFS_ReaderDateTime_V13::CreateDateTimeStr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeCreateDateTimeSEL, (void*)CFS_ReaderDateTime_V13::CreateDateTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeCreateDateTimeSysTimeSEL, (void*)CFS_ReaderDateTime_V13::CreateDateTimeSysTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeFromPDFDateTimeStringSEL, (void*)CFS_ReaderDateTime_V13::FromPDFDateTimeString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeToCommonDateTimeStringSEL, (void*)CFS_ReaderDateTime_V13::ToCommonDateTimeString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeToPDFDateTimeStringSEL, (void*)CFS_ReaderDateTime_V13::ToPDFDateTimeString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeToSystemTimeSEL, (void*)CFS_ReaderDateTime_V13::ToSystemTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeToGMTSEL, (void*)CFS_ReaderDateTime_V13::ToGMT);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeToLocalTimeSEL, (void*)CFS_ReaderDateTime_V13::ToLocalTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeAddDaysSEL, (void*)CFS_ReaderDateTime_V13::AddDays);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeAddSecondsSEL, (void*)CFS_ReaderDateTime_V13::AddSeconds);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeIsEmptySEL, (void*)CFS_ReaderDateTime_V13::IsEmpty);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeResetDateTimeSEL, (void*)CFS_ReaderDateTime_V13::ResetDateTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeIsLeapYearSEL, (void*)CFS_ReaderDateTime_V13::IsLeapYear);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeIsValidSEL, (void*)CFS_ReaderDateTime_V13::IsValid);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeFillDateTimeSEL, (void*)CFS_ReaderDateTime_V13::FillDateTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeFillSystemtimeSEL, (void*)CFS_ReaderDateTime_V13::FillSystemtime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeIsEqualSEL, (void*)CFS_ReaderDateTime_V13::IsEqual);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeIsUnEqualSEL, (void*)CFS_ReaderDateTime_V13::IsUnEqual);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeIsGreaterSEL, (void*)CFS_ReaderDateTime_V13::IsGreater);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeIsEqualGreaterSEL, (void*)CFS_ReaderDateTime_V13::IsEqualGreater);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeIsLessSEL, (void*)CFS_ReaderDateTime_V13::IsLess);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeIsEqualLessSEL, (void*)CFS_ReaderDateTime_V13::IsEqualLess);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSReaderDateTimeSEL, FSReaderDateTimeDestroySEL, (void*)CFS_ReaderDateTime_V13::Destroy);
	}
};

class CFS_Monoscale_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMonoscaleSEL, FSMonoscaleNewSEL, (void*)CFS_Monoscale_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMonoscaleSEL, FSMonoscaleDestroySEL, (void*)CFS_Monoscale_V13::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMonoscaleSEL, FSMonoscaleLoadDIBSourceSEL, (void*)CFS_Monoscale_V13::LoadDIBSource);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSMonoscaleSEL, FSMonoscaleGetMonoscaleBitmapSEL, (void*)CFS_Monoscale_V13::GetMonoscaleBitmap);
	}
};

class CFS_FloatArray_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArrayNewSEL, (void*)CFS_FloatArray_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArrayDestroySEL, (void*)CFS_FloatArray_V13::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArrayGetSizeSEL, (void*)CFS_FloatArray_V13::GetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArrayGetUpperBoundSEL, (void*)CFS_FloatArray_V13::GetUpperBound);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArraySetSizeSEL, (void*)CFS_FloatArray_V13::SetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArrayRemoveAllSEL, (void*)CFS_FloatArray_V13::RemoveAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArrayGetAtSEL, (void*)CFS_FloatArray_V13::GetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArraySetAtSEL, (void*)CFS_FloatArray_V13::SetAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArraySetAtGrowSEL, (void*)CFS_FloatArray_V13::SetAtGrow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArrayAddSEL, (void*)CFS_FloatArray_V13::Add);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArrayAppendSEL, (void*)CFS_FloatArray_V13::Append);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArrayCopySEL, (void*)CFS_FloatArray_V13::Copy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArrayGetDataPtrSEL, (void*)CFS_FloatArray_V13::GetDataPtr);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArrayInsertAtSEL, (void*)CFS_FloatArray_V13::InsertAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArrayRemoveAtSEL, (void*)CFS_FloatArray_V13::RemoveAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArrayInsertNewArrayAtSEL, (void*)CFS_FloatArray_V13::InsertNewArrayAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FSFloatArraySEL, FSFloatArrayFindSEL, (void*)CFS_FloatArray_V13::Find);
	}
};

// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
class CFR_IReader_PageView_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRIReaderSEL, FRIReaderGetInputMethodSEL, (void*)CFR_IReader_PageView_V13::GetInputMethod);
	}
};

class CFR_IFX_InputMethod_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRIFXSEL, FRIFXOnWindowProcSEL, (void*)CFR_IFX_InputMethod_V13::OnWindowProc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRIFXSEL, FRIFXRegisterInputHandlerSEL, (void*)CFR_IFX_InputMethod_V13::RegisterInputHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRIFXSEL, FRIFXUnRegisterInputHandlerSEL, (void*)CFR_IFX_InputMethod_V13::UnRegisterInputHandler);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRIFXSEL, FRIFXEnterInputStatusSEL, (void*)CFR_IFX_InputMethod_V13::EnterInputStatus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRIFXSEL, FRIFXExitInputStatusSEL, (void*)CFR_IFX_InputMethod_V13::ExitInputStatus);
	}
};

class CFR_InputMethodHandler_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInputMethodHandlerSEL, FRInputMethodHandlerNewSEL, (void*)CFR_InputMethodHandler_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRInputMethodHandlerSEL, FRInputMethodHandlerDestroySEL, (void*)CFR_InputMethodHandler_V13::Destroy);
	}
};

class CFR_MarkAnnotExtendHandler_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkAnnotExtendHandlerSEL, FRMarkAnnotExtendHandlerBeginTimerSEL, (void*)CFR_MarkAnnotExtendHandler_V13::BeginTimer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkAnnotExtendHandlerSEL, FRMarkAnnotExtendHandlerEndTimerSEL, (void*)CFR_MarkAnnotExtendHandler_V13::EndTimer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkAnnotExtendHandlerSEL, FRMarkAnnotExtendHandlerOnMoveOutPageSEL, (void*)CFR_MarkAnnotExtendHandler_V13::OnMoveOutPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkAnnotExtendHandlerSEL, FRMarkAnnotExtendHandlerCopyReplySEL, (void*)CFR_MarkAnnotExtendHandler_V13::CopyReply);
	}
};

// fr_appImpl.h end

// In file fr_barImpl.h
class CFR_RibbonStyleRadioBox_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleRadioBoxSEL, FRRibbonStyleRadioBoxGetMFCButtonSEL, (void*)CFR_RibbonStyleRadioBox_V13::GetMFCButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleRadioBoxSEL, FRRibbonStyleRadioBoxReleaseSEL, (void*)CFR_RibbonStyleRadioBox_V13::Release);
	}
};

class CFR_RibbonStyleCheckBox_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleCheckBoxSEL, FRRibbonStyleCheckBoxIsCheckedSEL, (void*)CFR_RibbonStyleCheckBox_V13::IsChecked);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleCheckBoxSEL, FRRibbonStyleCheckBoxGetCheckStateSEL, (void*)CFR_RibbonStyleCheckBox_V13::GetCheckState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleCheckBoxSEL, FRRibbonStyleCheckBoxSetCheckBox3StateSEL, (void*)CFR_RibbonStyleCheckBox_V13::SetCheckBox3State);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleCheckBoxSEL, FRRibbonStyleCheckBoxGetMFCButtonSEL, (void*)CFR_RibbonStyleCheckBox_V13::GetMFCButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleCheckBoxSEL, FRRibbonStyleCheckBoxReleaseSEL, (void*)CFR_RibbonStyleCheckBox_V13::Release);
	}
};

class CFR_RibbonStyleEdit_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleEditSEL, FRRibbonStyleEditGetEditButtonSEL, (void*)CFR_RibbonStyleEdit_V13::GetEditButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleEditSEL, FRRibbonStyleEditReleaseSEL, (void*)CFR_RibbonStyleEdit_V13::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleEditSEL, FRRibbonStyleEditSetPromptTextSEL, (void*)CFR_RibbonStyleEdit_V13::SetPromptText);
	}
};

class CFR_RibbonStyleLinkButton_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleLinkButtonSEL, FRRibbonStyleLinkButtonSetAlwaysUnderLineTextSEL, (void*)CFR_RibbonStyleLinkButton_V13::SetAlwaysUnderLineText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleLinkButtonSEL, FRRibbonStyleLinkButtonSetMultilineTextSEL, (void*)CFR_RibbonStyleLinkButton_V13::SetMultilineText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleLinkButtonSEL, FRRibbonStyleLinkButtonSetAlignStyleSEL, (void*)CFR_RibbonStyleLinkButton_V13::SetAlignStyle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleLinkButtonSEL, FRRibbonStyleLinkButtonGetMFCButtonSEL, (void*)CFR_RibbonStyleLinkButton_V13::GetMFCButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleLinkButtonSEL, FRRibbonStyleLinkButtonReleaseSEL, (void*)CFR_RibbonStyleLinkButton_V13::Release);
	}
};

class CFR_RibbonStyleSliderCtrl_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleSliderCtrlSEL, FRRibbonStyleSliderCtrlGetSliderButtonSEL, (void*)CFR_RibbonStyleSliderCtrl_V13::GetSliderButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleSliderCtrlSEL, FRRibbonStyleSliderCtrlReleaseSEL, (void*)CFR_RibbonStyleSliderCtrl_V13::Release);
	}
};

class CFR_RibbonStyleColorButton_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleColorButtonSEL, FRRibbonStyleColorButtonGetButtonSEL, (void*)CFR_RibbonStyleColorButton_V13::GetButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleColorButtonSEL, FRRibbonStyleColorButtonReleaseSEL, (void*)CFR_RibbonStyleColorButton_V13::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleColorButtonSEL, FRRibbonStyleColorButtonSetColorSEL, (void*)CFR_RibbonStyleColorButton_V13::SetColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleColorButtonSEL, FRRibbonStyleColorButtonGetColorSEL, (void*)CFR_RibbonStyleColorButton_V13::GetColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleColorButtonSEL, FRRibbonStyleColorButtonSetColumnsNumberSEL, (void*)CFR_RibbonStyleColorButton_V13::SetColumnsNumber);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleColorButtonSEL, FRRibbonStyleColorButtonSetImageSEL, (void*)CFR_RibbonStyleColorButton_V13::SetImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleColorButtonSEL, FRRibbonStyleColorButtonSetAppearanceSEL, (void*)CFR_RibbonStyleColorButton_V13::SetAppearance);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleColorButtonSEL, FRRibbonStyleColorButtonEnableAutomaticButtonSEL, (void*)CFR_RibbonStyleColorButton_V13::EnableAutomaticButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonStyleColorButtonSEL, FRRibbonStyleColorButtonEnableOtherButtonSEL, (void*)CFR_RibbonStyleColorButton_V13::EnableOtherButton);
	}
};

class CFR_RibbonIFXBCGPComboBox_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonIFXBCGPComboBoxSEL, FRRibbonIFXBCGPComboBoxGetButtonSEL, (void*)CFR_RibbonIFXBCGPComboBox_V13::GetButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonIFXBCGPComboBoxSEL, FRRibbonIFXBCGPComboBoxSetPromptSEL, (void*)CFR_RibbonIFXBCGPComboBox_V13::SetPrompt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonIFXBCGPComboBoxSEL, FRRibbonIFXBCGPComboBoxReleaseSEL, (void*)CFR_RibbonIFXBCGPComboBox_V13::Release);
	}
};

class CFR_RibbonIFXBCGPFontComboBox_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonIFXBCGPFontComboBoxSEL, FRRibbonIFXBCGPFontComboBoxGetButtonSEL, (void*)CFR_RibbonIFXBCGPFontComboBox_V13::GetButton);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonIFXBCGPFontComboBoxSEL, FRRibbonIFXBCGPFontComboBoxReleaseSEL, (void*)CFR_RibbonIFXBCGPFontComboBox_V13::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonIFXBCGPFontComboBoxSEL, FRRibbonIFXBCGPFontComboBoxSelectFontSEL, (void*)CFR_RibbonIFXBCGPFontComboBox_V13::SelectFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonIFXBCGPFontComboBoxSEL, FRRibbonIFXBCGPFontComboBoxInsertFontSEL, (void*)CFR_RibbonIFXBCGPFontComboBox_V13::InsertFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRRibbonIFXBCGPFontComboBoxSEL, FRRibbonIFXBCGPFontComboBoxAddFontSEL, (void*)CFR_RibbonIFXBCGPFontComboBox_V13::AddFont);
	}
};

class CFR_CFXBCGStyleGalleryCtrl_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCFXBCGStyleGalleryCtrlSEL, FRCFXBCGStyleGalleryCtrlGetWndSEL, (void*)CFR_CFXBCGStyleGalleryCtrl_V13::GetWnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCFXBCGStyleGalleryCtrlSEL, FRCFXBCGStyleGalleryCtrlReleaseSEL, (void*)CFR_CFXBCGStyleGalleryCtrl_V13::Release);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCFXBCGStyleGalleryCtrlSEL, FRCFXBCGStyleGalleryCtrlAddGroupSEL, (void*)CFR_CFXBCGStyleGalleryCtrl_V13::AddGroup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCFXBCGStyleGalleryCtrlSEL, FRCFXBCGStyleGalleryCtrlAddGroup1SEL, (void*)CFR_CFXBCGStyleGalleryCtrl_V13::AddGroup1);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCFXBCGStyleGalleryCtrlSEL, FRCFXBCGStyleGalleryCtrlSelectItemSEL, (void*)CFR_CFXBCGStyleGalleryCtrl_V13::SelectItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCFXBCGStyleGalleryCtrlSEL, FRCFXBCGStyleGalleryCtrlGetSelectedItemSEL, (void*)CFR_CFXBCGStyleGalleryCtrl_V13::GetSelectedItem);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCFXBCGStyleGalleryCtrlSEL, FRCFXBCGStyleGalleryCtrlSetItemSizeSEL, (void*)CFR_CFXBCGStyleGalleryCtrl_V13::SetItemSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCFXBCGStyleGalleryCtrlSEL, FRCFXBCGStyleGalleryCtrlSetImageMarginSEL, (void*)CFR_CFXBCGStyleGalleryCtrl_V13::SetImageMargin);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCFXBCGStyleGalleryCtrlSEL, FRCFXBCGStyleGalleryCtrlSetAppearanceSEL, (void*)CFR_CFXBCGStyleGalleryCtrl_V13::SetAppearance);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCFXBCGStyleGalleryCtrlSEL, FRCFXBCGStyleGalleryCtrlSetAlignedSideSEL, (void*)CFR_CFXBCGStyleGalleryCtrl_V13::SetAlignedSide);
	}
};

// fr_barImpl.h end

// In file fr_docImpl.h
class CFR_ReaderInterform_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRReaderInterformSEL, FRReaderInterformGetInterFormSEL, (void*)CFR_ReaderInterform_V13::GetInterForm);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRReaderInterformSEL, FRReaderInterformLoadImageFromFileSEL, (void*)CFR_ReaderInterform_V13::LoadImageFromFile);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRReaderInterformSEL, FRReaderInterformResetFieldAppearanceSEL, (void*)CFR_ReaderInterform_V13::ResetFieldAppearance);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRReaderInterformSEL, FRReaderInterformOnCalculateSEL, (void*)CFR_ReaderInterform_V13::OnCalculate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRReaderInterformSEL, FRReaderInterformOnCalculate2SEL, (void*)CFR_ReaderInterform_V13::OnCalculate2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRReaderInterformSEL, FRReaderInterformIsFormFieldInPageSEL, (void*)CFR_ReaderInterform_V13::IsFormFieldInPage);
	}
};

class CFR_UndoItemCreateWidget_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRUndoItemCreateWidgetSEL, FRUndoItemCreateWidgetCreateUndoWidgetSEL, (void*)CFR_UndoItemCreateWidget_V13::CreateUndoWidget);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRUndoItemCreateWidgetSEL, FRUndoItemCreateWidgetUndoSEL, (void*)CFR_UndoItemCreateWidget_V13::Undo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRUndoItemCreateWidgetSEL, FRUndoItemCreateWidgetRedoSEL, (void*)CFR_UndoItemCreateWidget_V13::Redo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRUndoItemCreateWidgetSEL, FRUndoItemCreateWidgetGetDescrSEL, (void*)CFR_UndoItemCreateWidget_V13::GetDescr);
	}
};

class CFR_CRSAStamp_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampNewSEL, (void*)CFR_CRSAStamp_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampDestroySEL, (void*)CFR_CRSAStamp_V13::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetLeftValueSEL, (void*)CFR_CRSAStamp_V13::GetLeftValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetTopValueSEL, (void*)CFR_CRSAStamp_V13::GetTopValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetDefaultCropWValueSEL, (void*)CFR_CRSAStamp_V13::GetDefaultCropWValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetDefaultCropWValueSEL, (void*)CFR_CRSAStamp_V13::SetDefaultCropWValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetDefaultCropHValueSEL, (void*)CFR_CRSAStamp_V13::GetDefaultCropHValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetDefaultCropHValueSEL, (void*)CFR_CRSAStamp_V13::SetDefaultCropHValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetDefaultWValueSEL, (void*)CFR_CRSAStamp_V13::GetDefaultWValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetDefaultWValueSEL, (void*)CFR_CRSAStamp_V13::SetDefaultWValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetDefaultHValueSEL, (void*)CFR_CRSAStamp_V13::GetDefaultHValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetDefaultHValueSEL, (void*)CFR_CRSAStamp_V13::SetDefaultHValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetWidthValueSEL, (void*)CFR_CRSAStamp_V13::GetWidthValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetWidthValueSEL, (void*)CFR_CRSAStamp_V13::SetWidthValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetHeightValueSEL, (void*)CFR_CRSAStamp_V13::GetHeightValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetHeightValueSEL, (void*)CFR_CRSAStamp_V13::SetHeightValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampIsPathSEL, (void*)CFR_CRSAStamp_V13::IsPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetPathSEL, (void*)CFR_CRSAStamp_V13::SetPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetDIBitmapSEL, (void*)CFR_CRSAStamp_V13::GetDIBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetDIBitmapSEL, (void*)CFR_CRSAStamp_V13::SetDIBitmap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampIsPDFSignStampSEL, (void*)CFR_CRSAStamp_V13::IsPDFSignStamp);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetPDFPathSEL, (void*)CFR_CRSAStamp_V13::GetPDFPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampIsDynamicSEL, (void*)CFR_CRSAStamp_V13::IsDynamic);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetEncodeSEL, (void*)CFR_CRSAStamp_V13::GetEncode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetStampTypeSEL, (void*)CFR_CRSAStamp_V13::GetStampType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetPageIdxSEL, (void*)CFR_CRSAStamp_V13::SetPageIdx);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetPageIdxSEL, (void*)CFR_CRSAStamp_V13::GetPageIdx);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampPreViewArtBoxFileSEL, (void*)CFR_CRSAStamp_V13::PreViewArtBoxFile);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetStampNameSEL, (void*)CFR_CRSAStamp_V13::GetStampName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampIsFavoriteSEL, (void*)CFR_CRSAStamp_V13::IsFavorite);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetPagePieceInfoSEL, (void*)CFR_CRSAStamp_V13::SetPagePieceInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetPagePieceInfoByDocSEL, (void*)CFR_CRSAStamp_V13::SetPagePieceInfoByDoc);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetIsRotateSEL, (void*)CFR_CRSAStamp_V13::GetIsRotate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampRemovePageSEL, (void*)CFR_CRSAStamp_V13::RemovePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampCopySEL, (void*)CFR_CRSAStamp_V13::Copy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetPageFromNameSEL, (void*)CFR_CRSAStamp_V13::GetPageFromName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetStampNameSEL, (void*)CFR_CRSAStamp_V13::SetStampName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetPDFPathSEL, (void*)CFR_CRSAStamp_V13::SetPDFPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetAppCloseDeleteSEL, (void*)CFR_CRSAStamp_V13::SetAppCloseDelete);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetTypeSEL, (void*)CFR_CRSAStamp_V13::SetType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetEncodeSEL, (void*)CFR_CRSAStamp_V13::SetEncode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetSignListIdxSEL, (void*)CFR_CRSAStamp_V13::SetSignListIdx);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetPDFStampSEL, (void*)CFR_CRSAStamp_V13::GetPDFStamp);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSaveStampToPageSEL, (void*)CFR_CRSAStamp_V13::SaveStampToPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetSampInfoDicSEL, (void*)CFR_CRSAStamp_V13::GetSampInfoDic);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampAddPageNameSEL, (void*)CFR_CRSAStamp_V13::AddPageName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetLeftValueSEL, (void*)CFR_CRSAStamp_V13::SetLeftValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetTopValueSEL, (void*)CFR_CRSAStamp_V13::SetTopValue);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetDynamicSEL, (void*)CFR_CRSAStamp_V13::SetDynamic);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetFavoriteSEL, (void*)CFR_CRSAStamp_V13::SetFavorite);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetTypeSEL, (void*)CFR_CRSAStamp_V13::GetType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetIsRotateSEL, (void*)CFR_CRSAStamp_V13::SetIsRotate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampCheckPDFPermissionSEL, (void*)CFR_CRSAStamp_V13::CheckPDFPermission);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampFSFadeOutImageBackgroundSEL, (void*)CFR_CRSAStamp_V13::FSFadeOutImageBackground);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetTipSEL, (void*)CFR_CRSAStamp_V13::SetTip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetTemplateNameSEL, (void*)CFR_CRSAStamp_V13::SetTemplateName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetThumbNailSEL, (void*)CFR_CRSAStamp_V13::SetThumbNail);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetCanParserSEL, (void*)CFR_CRSAStamp_V13::SetCanParser);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetImageSEL, (void*)CFR_CRSAStamp_V13::SetImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetOpacitySEL, (void*)CFR_CRSAStamp_V13::SetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetSignFilePwdSEL, (void*)CFR_CRSAStamp_V13::GetSignFilePwd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetStampFormPageSEL, (void*)CFR_CRSAStamp_V13::GetStampFormPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSaveStampToPage2SEL, (void*)CFR_CRSAStamp_V13::SaveStampToPage2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetSignListIdxSEL, (void*)CFR_CRSAStamp_V13::GetSignListIdx);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampSetStampGuidSEL, (void*)CFR_CRSAStamp_V13::SetStampGuid);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSAStampSEL, FRCRSAStampGetStampGuidSEL, (void*)CFR_CRSAStamp_V13::GetStampGuid);
	}
};

class CFR_MarkupAnnot_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupAnnotSEL, FRMarkupAnnotCreateSEL, (void*)CFR_MarkupAnnot_V13::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupAnnotSEL, FRMarkupAnnotDestroySEL, (void*)CFR_MarkupAnnot_V13::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupAnnotSEL, FRMarkupAnnotIsGroupSEL, (void*)CFR_MarkupAnnot_V13::IsGroup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupAnnotSEL, FRMarkupAnnotIsHeaderSEL, (void*)CFR_MarkupAnnot_V13::IsHeader);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupAnnotSEL, FRMarkupAnnotGetPopupSEL, (void*)CFR_MarkupAnnot_V13::GetPopup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupAnnotSEL, FRMarkupAnnotResetAppearanceSEL, (void*)CFR_MarkupAnnot_V13::ResetAppearance);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupAnnotSEL, FRMarkupAnnotSetModifiedDateTimeSEL, (void*)CFR_MarkupAnnot_V13::SetModifiedDateTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupAnnotSEL, FRMarkupAnnotGetModifiedDateTimeSEL, (void*)CFR_MarkupAnnot_V13::GetModifiedDateTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupAnnotSEL, FRMarkupAnnotGetHotPointSEL, (void*)CFR_MarkupAnnot_V13::GetHotPoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupAnnotSEL, FRMarkupAnnotGetColorSEL, (void*)CFR_MarkupAnnot_V13::GetColor);
	}
};

class CFR_MarkupPopup_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPopupSEL, FRMarkupPopupUpdateDataTimeSEL, (void*)CFR_MarkupPopup_V13::UpdateDataTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPopupSEL, FRMarkupPopupSetNoteAnchorSEL, (void*)CFR_MarkupPopup_V13::SetNoteAnchor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPopupSEL, FRMarkupPopupShowRopeSEL, (void*)CFR_MarkupPopup_V13::ShowRope);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPopupSEL, FRMarkupPopupIsNoteVisibleSEL, (void*)CFR_MarkupPopup_V13::IsNoteVisible);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPopupSEL, FRMarkupPopupUpdateNoteSEL, (void*)CFR_MarkupPopup_V13::UpdateNote);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPopupSEL, FRMarkupPopupResetNotePositionSEL, (void*)CFR_MarkupPopup_V13::ResetNotePosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPopupSEL, FRMarkupPopupGetPopupAnnotSEL, (void*)CFR_MarkupPopup_V13::GetPopupAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPopupSEL, FRMarkupPopupGetRectSEL, (void*)CFR_MarkupPopup_V13::GetRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPopupSEL, FRMarkupPopupSetStateSEL, (void*)CFR_MarkupPopup_V13::SetState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPopupSEL, FRMarkupPopupShowNoteSEL, (void*)CFR_MarkupPopup_V13::ShowNote);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPopupSEL, FRMarkupPopupSetNoteFocusSEL, (void*)CFR_MarkupPopup_V13::SetNoteFocus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPopupSEL, FRMarkupPopupAddReplySEL, (void*)CFR_MarkupPopup_V13::AddReply);
	}
};

class CFR_MarkupPanel_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPanelSEL, FRMarkupPanelGetMarkupPanelSEL, (void*)CFR_MarkupPanel_V13::GetMarkupPanel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPanelSEL, FRMarkupPanelRefreshAnnotSEL, (void*)CFR_MarkupPanel_V13::RefreshAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPanelSEL, FRMarkupPanelReloadAnnotsSEL, (void*)CFR_MarkupPanel_V13::ReloadAnnots);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPanelSEL, FRMarkupPanelAddAnnotSEL, (void*)CFR_MarkupPanel_V13::AddAnnot);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPanelSEL, FRMarkupPanelSetFocusSEL, (void*)CFR_MarkupPanel_V13::SetFocus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPanelSEL, FRMarkupPanelUpdatePropertyBoxSEL, (void*)CFR_MarkupPanel_V13::UpdatePropertyBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPanelSEL, FRMarkupPanelAddToCreateListSEL, (void*)CFR_MarkupPanel_V13::AddToCreateList);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPanelSEL, FRMarkupPanelHideHintSEL, (void*)CFR_MarkupPanel_V13::HideHint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRMarkupPanelSEL, FRMarkupPanelRemoveAnnotSEL, (void*)CFR_MarkupPanel_V13::RemoveAnnot);
	}
};

class CFR_CRSASStampAnnot_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotNewSEL, (void*)CFR_CRSASStampAnnot_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotDestroySEL, (void*)CFR_CRSASStampAnnot_V13::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotSetStampSEL, (void*)CFR_CRSASStampAnnot_V13::SetStamp);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotAfterMovePagesSEL, (void*)CFR_CRSASStampAnnot_V13::AfterMovePages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotResetAppearanceSEL, (void*)CFR_CRSASStampAnnot_V13::ResetAppearance);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotSetColorSEL, (void*)CFR_CRSASStampAnnot_V13::SetColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotSetOpacitySEL, (void*)CFR_CRSASStampAnnot_V13::SetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotSetCreationDateTimeSEL, (void*)CFR_CRSASStampAnnot_V13::SetCreationDateTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotGetModifiedDateTimeSEL, (void*)CFR_CRSASStampAnnot_V13::GetModifiedDateTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotGetPopupSEL, (void*)CFR_CRSASStampAnnot_V13::GetPopup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotGetHotPointSEL, (void*)CFR_CRSASStampAnnot_V13::GetHotPoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotGetTypeSEL, (void*)CFR_CRSASStampAnnot_V13::GetType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotIsGroupSEL, (void*)CFR_CRSASStampAnnot_V13::IsGroup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotIsHeaderSEL, (void*)CFR_CRSASStampAnnot_V13::IsHeader);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotSetModifiedDateTimeSEL, (void*)CFR_CRSASStampAnnot_V13::SetModifiedDateTime);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotGetColorSEL, (void*)CFR_CRSASStampAnnot_V13::GetColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCRSASStampAnnotSEL, FRCRSASStampAnnotGetReaderPageSEL, (void*)CFR_CRSASStampAnnot_V13::GetReaderPage);
	}
};

class CFR_EncryptPermisson_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FREncryptPermissonSEL, FREncryptPermissonCreateEncryptPermissonSEL, (void*)CFR_EncryptPermisson_V13::CreateEncryptPermisson);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FREncryptPermissonSEL, FREncryptPermissonSetEncryptPermissonSEL, (void*)CFR_EncryptPermisson_V13::SetEncryptPermisson);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FREncryptPermissonSEL, FREncryptPermissonGetEncryptPermissonSEL, (void*)CFR_EncryptPermisson_V13::GetEncryptPermisson);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FREncryptPermissonSEL, FREncryptPermissonGetPropertyPermissonSEL, (void*)CFR_EncryptPermisson_V13::GetPropertyPermisson);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FREncryptPermissonSEL, FREncryptPermissonGetAllPermissionSEL, (void*)CFR_EncryptPermisson_V13::GetAllPermission);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FREncryptPermissonSEL, FREncryptPermissonDeleteEncryptPermissionSEL, (void*)CFR_EncryptPermisson_V13::DeleteEncryptPermission);
	}
};

class CFR_CSGCertFileManage_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCSGCertFileManageSEL, FRCSGCertFileManageCreateCertFileManageSEL, (void*)CFR_CSGCertFileManage_V13::CreateCertFileManage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCSGCertFileManageSEL, FRCSGCertFileManageDestroySEL, (void*)CFR_CSGCertFileManage_V13::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCSGCertFileManageSEL, FRCSGCertFileManageGetCertFileStorePathSEL, (void*)CFR_CSGCertFileManage_V13::GetCertFileStorePath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCSGCertFileManageSEL, FRCSGCertFileManageNewCertDataSEL, (void*)CFR_CSGCertFileManage_V13::NewCertData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCSGCertFileManageSEL, FRCSGCertFileManageLoadStoreSEL, (void*)CFR_CSGCertFileManage_V13::LoadStore);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCSGCertFileManageSEL, FRCSGCertFileManageGetPathAndPasswordSEL, (void*)CFR_CSGCertFileManage_V13::GetPathAndPassword);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCSGCertFileManageSEL, FRCSGCertFileManageUpdateFileSEL, (void*)CFR_CSGCertFileManage_V13::UpdateFile);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCSGCertFileManageSEL, FRCSGCertFileManageBase64ToStringSEL, (void*)CFR_CSGCertFileManage_V13::Base64ToString);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCSGCertFileManageSEL, FRCSGCertFileManageStringToBase64SEL, (void*)CFR_CSGCertFileManage_V13::StringToBase64);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCSGCertFileManageSEL, FRCSGCertFileManageReloadAllFileSEL, (void*)CFR_CSGCertFileManage_V13::ReloadAllFile);
	}
};

class CFR_CSG_CreateCert_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCSGSEL, FRCSGCreateSEL, (void*)CFR_CSG_CreateCert_V13::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCSGSEL, FRCSGDestroySEL, (void*)CFR_CSG_CreateCert_V13::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCSGSEL, FRCSGCreateCertSEL, (void*)CFR_CSG_CreateCert_V13::CreateCert);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCSGSEL, FRCSGGetTmpNameSEL, (void*)CFR_CSG_CreateCert_V13::GetTmpName);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCSGSEL, FRCSGOpenPFXStoreSEL, (void*)CFR_CSG_CreateCert_V13::OpenPFXStore);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRCSGSEL, FRCSGCreateOpenSSLCertSEL, (void*)CFR_CSG_CreateCert_V13::CreateOpenSSLCert);
	}
};

// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
class CFR_Reader_DispViewerEx_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRReaderSEL, FRReaderCreateDispViewerSEL, (void*)CFR_Reader_DispViewerEx_V13::CreateDispViewer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRReaderSEL, FRReaderGetDocViewSEL, (void*)CFR_Reader_DispViewerEx_V13::GetDocView);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRReaderSEL, FRReaderSetSizeSEL, (void*)CFR_Reader_DispViewerEx_V13::SetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRReaderSEL, FRReaderGetRenderDataSEL, (void*)CFR_Reader_DispViewerEx_V13::GetRenderData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRReaderSEL, FRReaderContinueRenderingSEL, (void*)CFR_Reader_DispViewerEx_V13::ContinueRendering);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRReaderSEL, FRReaderSetCenterPosSEL, (void*)CFR_Reader_DispViewerEx_V13::SetCenterPos);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRReaderSEL, FRReaderDocToWindowSEL, (void*)CFR_Reader_DispViewerEx_V13::DocToWindow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRReaderSEL, FRReaderGetCurrentMatrixSEL, (void*)CFR_Reader_DispViewerEx_V13::GetCurrentMatrix);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRReaderSEL, FRReaderGetSizeSEL, (void*)CFR_Reader_DispViewerEx_V13::GetSize);
	}
};

class CFR_IReaderDispViewerHandler_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRIReaderDispViewerHandlerSEL, FRIReaderDispViewerHandlerNewSEL, (void*)CFR_IReaderDispViewerHandler_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRIReaderDispViewerHandlerSEL, FRIReaderDispViewerHandlerDestroySEL, (void*)CFR_IReaderDispViewerHandler_V13::Destroy);
	}
};

class CFR_IPDFViewerEventHandler_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRIPDFViewerEventHandlerSEL, FRIPDFViewerEventHandlerNewSEL, (void*)CFR_IPDFViewerEventHandler_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRIPDFViewerEventHandlerSEL, FRIPDFViewerEventHandlerDestroySEL, (void*)CFR_IPDFViewerEventHandler_V13::Destroy);
	}
};

class CFR_ToolFormatMgr_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolFormatMgrSEL, FRToolFormatMgrGetSEL, (void*)CFR_ToolFormatMgr_V13::Get);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolFormatMgrSEL, FRToolFormatMgrRegistFormatEventSEL, (void*)CFR_ToolFormatMgr_V13::RegistFormatEvent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolFormatMgrSEL, FRToolFormatMgrUnregisterFormatEventSEL, (void*)CFR_ToolFormatMgr_V13::UnregisterFormatEvent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolFormatMgrSEL, FRToolFormatMgrAddGroupSEL, (void*)CFR_ToolFormatMgr_V13::AddGroup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolFormatMgrSEL, FRToolFormatMgrAddGroupToFormatSEL, (void*)CFR_ToolFormatMgr_V13::AddGroupToFormat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolFormatMgrSEL, FRToolFormatMgrShowToolFormatSEL, (void*)CFR_ToolFormatMgr_V13::ShowToolFormat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolFormatMgrSEL, FRToolFormatMgrIsToolFormatExistSEL, (void*)CFR_ToolFormatMgr_V13::IsToolFormatExist);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolFormatMgrSEL, FRToolFormatMgrGetFormatGroupsSEL, (void*)CFR_ToolFormatMgr_V13::GetFormatGroups);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FRToolFormatMgrSEL, FRToolFormatMgrChangeFormatSizeSEL, (void*)CFR_ToolFormatMgr_V13::ChangeFormatSize);
	}
};

// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
class CFPD_CertStore_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCertStoreSEL, FPDCertStoreNewSEL, (void*)CFPD_CertStore_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCertStoreSEL, FPDCertStoreDestroySEL, (void*)CFPD_CertStore_V13::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCertStoreSEL, FPDCertStoreGetHCertStoreSEL, (void*)CFPD_CertStore_V13::GetHCertStore);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCertStoreSEL, FPDCertStoreLoadMYSystemCertsSEL, (void*)CFPD_CertStore_V13::LoadMYSystemCerts);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCertStoreSEL, FPDCertStoreGetCertFromCertSNIdSEL, (void*)CFPD_CertStore_V13::GetCertFromCertSNId);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCertStoreSEL, FPDCertStoreGetCertSNIDSEL, (void*)CFPD_CertStore_V13::GetCertSNID);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCertStoreSEL, FPDCertStoreGetCertCountSEL, (void*)CFPD_CertStore_V13::GetCertCount);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCertStoreSEL, FPDCertStoreGetCertsArraySEL, (void*)CFPD_CertStore_V13::GetCertsArray);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCertStoreSEL, FPDCertStoreIsPrivateKeyCertSEL, (void*)CFPD_CertStore_V13::IsPrivateKeyCert);
	}
};

class CFPD_3DCompositionProvider_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3DCompositionProviderSEL, FPD3DCompositionProviderNewSEL, (void*)CFPD_3DCompositionProvider_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPD3DCompositionProviderSEL, FPD3DCompositionProviderDestroySEL, (void*)CFPD_3DCompositionProvider_V13::Destroy);
	}
};

class CFPD_PageLabel_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageLabelSEL, FPDPageLabelNewSEL, (void*)CFPD_PageLabel_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageLabelSEL, FPDPageLabelDestroySEL, (void*)CFPD_PageLabel_V13::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageLabelSEL, FPDPageLabelGetLabelSEL, (void*)CFPD_PageLabel_V13::GetLabel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageLabelSEL, FPDPageLabelGetPageByLabelSEL, (void*)CFPD_PageLabel_V13::GetPageByLabel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPageLabelSEL, FPDPageLabelGetPageByLabel2SEL, (void*)CFPD_PageLabel_V13::GetPageByLabel2);
	}
};

// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
class CFPD_PathObjectUtils_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectUtilsSEL, FPDPathObjectUtilsGetLineWidthSEL, (void*)CFPD_PathObjectUtils_V13::GetLineWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectUtilsSEL, FPDPathObjectUtilsSetLineWidthSEL, (void*)CFPD_PathObjectUtils_V13::SetLineWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectUtilsSEL, FPDPathObjectUtilsSetMiterLimitSEL, (void*)CFPD_PathObjectUtils_V13::SetMiterLimit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectUtilsSEL, FPDPathObjectUtilsGetMiterLimitSEL, (void*)CFPD_PathObjectUtils_V13::GetMiterLimit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectUtilsSEL, FPDPathObjectUtilsSetLineCapSEL, (void*)CFPD_PathObjectUtils_V13::SetLineCap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectUtilsSEL, FPDPathObjectUtilsGetLineCapSEL, (void*)CFPD_PathObjectUtils_V13::GetLineCap);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectUtilsSEL, FPDPathObjectUtilsGetLineJoinSEL, (void*)CFPD_PathObjectUtils_V13::GetLineJoin);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectUtilsSEL, FPDPathObjectUtilsSetLineJoinSEL, (void*)CFPD_PathObjectUtils_V13::SetLineJoin);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectUtilsSEL, FPDPathObjectUtilsGetLineStyleSEL, (void*)CFPD_PathObjectUtils_V13::GetLineStyle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectUtilsSEL, FPDPathObjectUtilsSetLineStyleSEL, (void*)CFPD_PathObjectUtils_V13::SetLineStyle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectUtilsSEL, FPDPathObjectUtilsGetStrokeInfoSEL, (void*)CFPD_PathObjectUtils_V13::GetStrokeInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectUtilsSEL, FPDPathObjectUtilsSetStrokeInfoSEL, (void*)CFPD_PathObjectUtils_V13::SetStrokeInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectUtilsSEL, FPDPathObjectUtilsGetFillInfoSEL, (void*)CFPD_PathObjectUtils_V13::GetFillInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectUtilsSEL, FPDPathObjectUtilsSetFillInfoSEL, (void*)CFPD_PathObjectUtils_V13::SetFillInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathObjectUtilsSEL, FPDPathObjectUtilsSetColorSpaceSEL, (void*)CFPD_PathObjectUtils_V13::SetColorSpace);
	}
};

class CFPD_ShadingObjectUtils_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsChangeShadingColorSEL, (void*)CFPD_ShadingObjectUtils_V13::ChangeShadingColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsChangeColorUndoStartSEL, (void*)CFPD_ShadingObjectUtils_V13::ChangeColorUndoStart);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsChangeColorUndoEndSEL, (void*)CFPD_ShadingObjectUtils_V13::ChangeColorUndoEnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsChangeColorUndoCancleSEL, (void*)CFPD_ShadingObjectUtils_V13::ChangeColorUndoCancle);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsEditReferenceLineSEL, (void*)CFPD_ShadingObjectUtils_V13::EditReferenceLine);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsSetGridientCursorColorSEL, (void*)CFPD_ShadingObjectUtils_V13::SetGridientCursorColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsGetGridientCursorColorSEL, (void*)CFPD_ShadingObjectUtils_V13::GetGridientCursorColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsGetGridientColorAtSEL, (void*)CFPD_ShadingObjectUtils_V13::GetGridientColorAt);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsGetGridientCursorLocationSEL, (void*)CFPD_ShadingObjectUtils_V13::GetGridientCursorLocation);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsGetGridientCursorIndexSEL, (void*)CFPD_ShadingObjectUtils_V13::GetGridientCursorIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsSetGradientColorShowRectSEL, (void*)CFPD_ShadingObjectUtils_V13::SetGradientColorShowRect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsAddGradientCursorInfoBeforeSEL, (void*)CFPD_ShadingObjectUtils_V13::AddGradientCursorInfoBefore);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsAddGradientCursorInfoExcuteSEL, (void*)CFPD_ShadingObjectUtils_V13::AddGradientCursorInfoExcute);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsAddGradientCursorInfoEndSEL, (void*)CFPD_ShadingObjectUtils_V13::AddGradientCursorInfoEnd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsAddGradientCursorInfoDblClkSEL, (void*)CFPD_ShadingObjectUtils_V13::AddGradientCursorInfoDblClk);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsDelGradientCursorInfoSEL, (void*)CFPD_ShadingObjectUtils_V13::DelGradientCursorInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsDefaultGradientDataInitSEL, (void*)CFPD_ShadingObjectUtils_V13::DefaultGradientDataInit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsSetGradientCurrentInfoSEL, (void*)CFPD_ShadingObjectUtils_V13::SetGradientCurrentInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsPaintGridientSEL, (void*)CFPD_ShadingObjectUtils_V13::PaintGridient);
	}
};

class CFPD_PathEditor_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathEditorSEL, FPDPathEditorHasStartEditSEL, (void*)CFPD_PathEditor_V13::HasStartEdit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathEditorSEL, FPDPathEditorIsClipPathModeSEL, (void*)CFPD_PathEditor_V13::IsClipPathMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDPathEditorSEL, FPDPathEditorAppendSubPathSEL, (void*)CFPD_PathEditor_V13::AppendSubPath);
	}
};

class CFPD_ShadingEditor_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDShadingEditorSEL, FPDShadingEditorIsReferenceLineWorkingSEL, (void*)CFPD_ShadingEditor_V13::IsReferenceLineWorking);
	}
};

class CFPD_TextEditor_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorOnFontNameChangedSEL, (void*)CFPD_TextEditor_V13::OnFontNameChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorOnFontSizeChangedSEL, (void*)CFPD_TextEditor_V13::OnFontSizeChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorOnTextColorChangedSEL, (void*)CFPD_TextEditor_V13::OnTextColorChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorOnBoldChangedSEL, (void*)CFPD_TextEditor_V13::OnBoldChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorOnItalicChangedSEL, (void*)CFPD_TextEditor_V13::OnItalicChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorOnAlignChangedSEL, (void*)CFPD_TextEditor_V13::OnAlignChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorOnCharSpaceChangedSEL, (void*)CFPD_TextEditor_V13::OnCharSpaceChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorOnWordSpaceChangedSEL, (void*)CFPD_TextEditor_V13::OnWordSpaceChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorOnCharHorzScaleChangedSEL, (void*)CFPD_TextEditor_V13::OnCharHorzScaleChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorOnLineLeadingChangedSEL, (void*)CFPD_TextEditor_V13::OnLineLeadingChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorOnUnderlineChangedSEL, (void*)CFPD_TextEditor_V13::OnUnderlineChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorOnCrossChangedSEL, (void*)CFPD_TextEditor_V13::OnCrossChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorOnSuperScriptChangedSEL, (void*)CFPD_TextEditor_V13::OnSuperScriptChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorOnSubScriptChangedSEL, (void*)CFPD_TextEditor_V13::OnSubScriptChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorOnWritingDirctionChangedSEL, (void*)CFPD_TextEditor_V13::OnWritingDirctionChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorIsSelectSEL, (void*)CFPD_TextEditor_V13::IsSelect);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorCopySEL, (void*)CFPD_TextEditor_V13::Copy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorPasteSEL, (void*)CFPD_TextEditor_V13::Paste);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorCutSEL, (void*)CFPD_TextEditor_V13::Cut);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorDeleteSEL, (void*)CFPD_TextEditor_V13::Delete);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorSelectAllSEL, (void*)CFPD_TextEditor_V13::SelectAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorIsAddSEL, (void*)CFPD_TextEditor_V13::IsAdd);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorGetICaretFormDataSEL, (void*)CFPD_TextEditor_V13::GetICaretFormData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextEditorSEL, FPDTextEditorGetTextObjectMatrixSEL, (void*)CFPD_TextEditor_V13::GetTextObjectMatrix);
	}
};

class CFPD_TextObjectUtils_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsGetFontSizeSEL, (void*)CFPD_TextObjectUtils_V13::GetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsSetFontSizeSEL, (void*)CFPD_TextObjectUtils_V13::SetFontSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsGetHorizontalScaleSEL, (void*)CFPD_TextObjectUtils_V13::GetHorizontalScale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsSetHorizontalScaleSEL, (void*)CFPD_TextObjectUtils_V13::SetHorizontalScale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsSetCharSpaceSEL, (void*)CFPD_TextObjectUtils_V13::SetCharSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsGetCharSpaceSEL, (void*)CFPD_TextObjectUtils_V13::GetCharSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsGetWordSpaceSEL, (void*)CFPD_TextObjectUtils_V13::GetWordSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsSetWordSpaceSEL, (void*)CFPD_TextObjectUtils_V13::SetWordSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsSetFontSEL, (void*)CFPD_TextObjectUtils_V13::SetFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsGetFontSEL, (void*)CFPD_TextObjectUtils_V13::GetFont);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsGetTextModeSEL, (void*)CFPD_TextObjectUtils_V13::GetTextMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsSetTextModeSEL, (void*)CFPD_TextObjectUtils_V13::SetTextMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsCanRemoveTextKerningSEL, (void*)CFPD_TextObjectUtils_V13::CanRemoveTextKerning);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsRemoveTextKerningSEL, (void*)CFPD_TextObjectUtils_V13::RemoveTextKerning);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsCanMergeTextSEL, (void*)CFPD_TextObjectUtils_V13::CanMergeText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsMergeTextSEL, (void*)CFPD_TextObjectUtils_V13::MergeText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsCanSplitTextSEL, (void*)CFPD_TextObjectUtils_V13::CanSplitText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsSplitTextSEL, (void*)CFPD_TextObjectUtils_V13::SplitText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsCanText2PathSEL, (void*)CFPD_TextObjectUtils_V13::CanText2Path);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsGetStrokeInfoSEL, (void*)CFPD_TextObjectUtils_V13::GetStrokeInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsSetStrokeInfoSEL, (void*)CFPD_TextObjectUtils_V13::SetStrokeInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsGetFillInfoSEL, (void*)CFPD_TextObjectUtils_V13::GetFillInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsSetFillInfoSEL, (void*)CFPD_TextObjectUtils_V13::SetFillInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsSetColorSpaceSEL, (void*)CFPD_TextObjectUtils_V13::SetColorSpace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsGetTextColorInfoSEL, (void*)CFPD_TextObjectUtils_V13::GetTextColorInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsIsWordSpaceValidSEL, (void*)CFPD_TextObjectUtils_V13::IsWordSpaceValid);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsGetFontBoldInfoSEL, (void*)CFPD_TextObjectUtils_V13::GetFontBoldInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDTextObjectUtilsSEL, FPDTextObjectUtilsGetFontItalicSEL, (void*)CFPD_TextObjectUtils_V13::GetFontItalic);
	}
};

class CFPD_GraphicObject_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectSEL, FPDGraphicObjectCreateSEL, (void*)CFPD_GraphicObject_V13::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectSEL, FPDGraphicObjectDestroySEL, (void*)CFPD_GraphicObject_V13::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectSEL, FPDGraphicObjectGetPageSEL, (void*)CFPD_GraphicObject_V13::GetPage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectSEL, FPDGraphicObjectGetGraphicObjectSEL, (void*)CFPD_GraphicObject_V13::GetGraphicObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectSEL, FPDGraphicObjectGetIndexSEL, (void*)CFPD_GraphicObject_V13::GetIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectSEL, FPDGraphicObjectGetPositionSEL, (void*)CFPD_GraphicObject_V13::GetPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectSEL, FPDGraphicObjectIsInFormSEL, (void*)CFPD_GraphicObject_V13::IsInForm);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectSEL, FPDGraphicObjectGetFormIndexSEL, (void*)CFPD_GraphicObject_V13::GetFormIndex);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectSEL, FPDGraphicObjectGetFormObjectsSEL, (void*)CFPD_GraphicObject_V13::GetFormObjects);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectSEL, FPDGraphicObjectGetFormMatrixSEL, (void*)CFPD_GraphicObject_V13::GetFormMatrix);
	}
};

class CFPD_GraphicEditor_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicEditorSEL, FPDGraphicEditorGetTypeSEL, (void*)CFPD_GraphicEditor_V13::GetType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicEditorSEL, FPDGraphicEditorEndEditSEL, (void*)CFPD_GraphicEditor_V13::EndEdit);
	}
};

class CFPD_GraphicObjectUtils_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsGetHeightSEL, (void*)CFPD_GraphicObjectUtils_V13::GetHeight);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsSetHeightSEL, (void*)CFPD_GraphicObjectUtils_V13::SetHeight);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsSetWidthSEL, (void*)CFPD_GraphicObjectUtils_V13::SetWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsGetWidthSEL, (void*)CFPD_GraphicObjectUtils_V13::GetWidth);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsGetXPositionSEL, (void*)CFPD_GraphicObjectUtils_V13::GetXPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsSetXPositionSEL, (void*)CFPD_GraphicObjectUtils_V13::SetXPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsGetYPositionSEL, (void*)CFPD_GraphicObjectUtils_V13::GetYPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsSetYPositionSEL, (void*)CFPD_GraphicObjectUtils_V13::SetYPosition);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsGetOpacitySEL, (void*)CFPD_GraphicObjectUtils_V13::GetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsSetOpacitySEL, (void*)CFPD_GraphicObjectUtils_V13::SetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsRotateSEL, (void*)CFPD_GraphicObjectUtils_V13::Rotate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsFlipSEL, (void*)CFPD_GraphicObjectUtils_V13::Flip);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsShearSEL, (void*)CFPD_GraphicObjectUtils_V13::Shear);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsMoveSEL, (void*)CFPD_GraphicObjectUtils_V13::Move);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsAlignSEL, (void*)CFPD_GraphicObjectUtils_V13::Align);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsCenterSEL, (void*)CFPD_GraphicObjectUtils_V13::Center);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsDistributeSEL, (void*)CFPD_GraphicObjectUtils_V13::Distribute);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsSizeSEL, (void*)CFPD_GraphicObjectUtils_V13::Size);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsScaleSEL, (void*)CFPD_GraphicObjectUtils_V13::Scale);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsScaleFloatSEL, (void*)CFPD_GraphicObjectUtils_V13::ScaleFloat);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsChangeRenderingOrderSEL, (void*)CFPD_GraphicObjectUtils_V13::ChangeRenderingOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsCanChangeRenderingOrderSEL, (void*)CFPD_GraphicObjectUtils_V13::CanChangeRenderingOrder);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsClearClipPathSEL, (void*)CFPD_GraphicObjectUtils_V13::ClearClipPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsEditClipPathSEL, (void*)CFPD_GraphicObjectUtils_V13::EditClipPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsAddClipPathSEL, (void*)CFPD_GraphicObjectUtils_V13::AddClipPath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsGetTypeSEL, (void*)CFPD_GraphicObjectUtils_V13::GetType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsCutSEL, (void*)CFPD_GraphicObjectUtils_V13::Cut);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsCopySEL, (void*)CFPD_GraphicObjectUtils_V13::Copy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsDeleteSEL, (void*)CFPD_GraphicObjectUtils_V13::Delete);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsSavePageArchiveSEL, (void*)CFPD_GraphicObjectUtils_V13::SavePageArchive);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsReadPageArchiveSEL, (void*)CFPD_GraphicObjectUtils_V13::ReadPageArchive);
	}
};

class CFPD_ImageEditor_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorSaveAsImageSEL, (void*)CFPD_ImageEditor_V13::SaveAsImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorPreviewFilterSEL, (void*)CFPD_ImageEditor_V13::PreviewFilter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorCancelPreviewFilterSEL, (void*)CFPD_ImageEditor_V13::CancelPreviewFilter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorAdjustFilterSEL, (void*)CFPD_ImageEditor_V13::AdjustFilter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorRotateVerticallySEL, (void*)CFPD_ImageEditor_V13::RotateVertically);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorRotateHorizontallySEL, (void*)CFPD_ImageEditor_V13::RotateHorizontally);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorReplaceSEL, (void*)CFPD_ImageEditor_V13::Replace);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorCopySEL, (void*)CFPD_ImageEditor_V13::Copy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorPasteSEL, (void*)CFPD_ImageEditor_V13::Paste);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorDeleteRegionSEL, (void*)CFPD_ImageEditor_V13::DeleteRegion);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorZoomInSEL, (void*)CFPD_ImageEditor_V13::ZoomIn);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorZoomToSEL, (void*)CFPD_ImageEditor_V13::ZoomTo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorZoomOutSEL, (void*)CFPD_ImageEditor_V13::ZoomOut);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorAdjustColorSEL, (void*)CFPD_ImageEditor_V13::AdjustColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorLoadFromClipboardSEL, (void*)CFPD_ImageEditor_V13::LoadFromClipboard);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorOnClientRectChangedSEL, (void*)CFPD_ImageEditor_V13::OnClientRectChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorSetActionTypeSEL, (void*)CFPD_ImageEditor_V13::SetActionType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorGetActionTypeSEL, (void*)CFPD_ImageEditor_V13::GetActionType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorOnSetCursorSEL, (void*)CFPD_ImageEditor_V13::OnSetCursor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorOnSizeSEL, (void*)CFPD_ImageEditor_V13::OnSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorGetCurrentOptionDataSEL, (void*)CFPD_ImageEditor_V13::GetCurrentOptionData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorGetPublicOptionDataSEL, (void*)CFPD_ImageEditor_V13::GetPublicOptionData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorGetImageHistogramDataSEL, (void*)CFPD_ImageEditor_V13::GetImageHistogramData);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorCompositeImageObjectSEL, (void*)CFPD_ImageEditor_V13::CompositeImageObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDImageEditorSEL, FPDImageEditorCancelEditSEL, (void*)CFPD_ImageEditor_V13::CancelEdit);
	}
};

class CFPD_IPageEditor_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorStartEditSEL, (void*)CFPD_IPageEditor_V13::StartEdit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorRestartEditSEL, (void*)CFPD_IPageEditor_V13::RestartEdit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorEndEditSEL, (void*)CFPD_IPageEditor_V13::EndEdit);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorHasSelectionSEL, (void*)CFPD_IPageEditor_V13::HasSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorCountSelectionSEL, (void*)CFPD_IPageEditor_V13::CountSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorGetSelectionSEL, (void*)CFPD_IPageEditor_V13::GetSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorHaveFillColorInSelectionSEL, (void*)CFPD_IPageEditor_V13::HaveFillColorInSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorGetActivePageSEL, (void*)CFPD_IPageEditor_V13::GetActivePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorSetObjectFilterSEL, (void*)CFPD_IPageEditor_V13::SetObjectFilter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorOnVKUPSEL, (void*)CFPD_IPageEditor_V13::OnVKUP);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorOnVKDOWNSEL, (void*)CFPD_IPageEditor_V13::OnVKDOWN);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorOnLeftButtonDownSEL, (void*)CFPD_IPageEditor_V13::OnLeftButtonDown);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorOnLeftButtonUpSEL, (void*)CFPD_IPageEditor_V13::OnLeftButtonUp);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorOnLeftButtonDblClkSEL, (void*)CFPD_IPageEditor_V13::OnLeftButtonDblClk);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorOnMouseMoveSEL, (void*)CFPD_IPageEditor_V13::OnMouseMove);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorOnMouseWheelSEL, (void*)CFPD_IPageEditor_V13::OnMouseWheel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorOnRightButtonDownSEL, (void*)CFPD_IPageEditor_V13::OnRightButtonDown);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorOnRightButtonUpSEL, (void*)CFPD_IPageEditor_V13::OnRightButtonUp);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorOnReleaseCaptureSEL, (void*)CFPD_IPageEditor_V13::OnReleaseCapture);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorOnPaintSEL, (void*)CFPD_IPageEditor_V13::OnPaint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorGetObjectAtPointSEL, (void*)CFPD_IPageEditor_V13::GetObjectAtPoint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorCanPasteSEL, (void*)CFPD_IPageEditor_V13::CanPaste);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorPasteSEL, (void*)CFPD_IPageEditor_V13::Paste);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorSelectAllSEL, (void*)CFPD_IPageEditor_V13::SelectAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorGetGraphicEditorSEL, (void*)CFPD_IPageEditor_V13::GetGraphicEditor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorGetGraphicObjectUtilsSEL, (void*)CFPD_IPageEditor_V13::GetGraphicObjectUtils);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorCreatePathSEL, (void*)CFPD_IPageEditor_V13::CreatePath);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorCreateShadingSEL, (void*)CFPD_IPageEditor_V13::CreateShading);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorCreateImageSEL, (void*)CFPD_IPageEditor_V13::CreateImage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorCreateTextSEL, (void*)CFPD_IPageEditor_V13::CreateText);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorCreatePageEditorSEL, (void*)CFPD_IPageEditor_V13::CreatePageEditor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorDestroyPageEditorSEL, (void*)CFPD_IPageEditor_V13::DestroyPageEditor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorClearSelectionSEL, (void*)CFPD_IPageEditor_V13::ClearSelection);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorText2PathSEL, (void*)CFPD_IPageEditor_V13::Text2Path);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorOnCharSEL, (void*)CFPD_IPageEditor_V13::OnChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorEditGraphicObjectSEL, (void*)CFPD_IPageEditor_V13::EditGraphicObject);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPageEditorSEL, FPDIPageEditorCreateImage2SEL, (void*)CFPD_IPageEditor_V13::CreateImage2);
	}
};

class CFPD_IUndoItem_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIUndoItemSEL, FPDIUndoItemOnUndoSEL, (void*)CFPD_IUndoItem_V13::OnUndo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIUndoItemSEL, FPDIUndoItemOnRedoSEL, (void*)CFPD_IUndoItem_V13::OnRedo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIUndoItemSEL, FPDIUndoItemOnReleaseSEL, (void*)CFPD_IUndoItem_V13::OnRelease);
	}
};

class CFPD_IClipboard_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIClipboardSEL, FPDIClipboardNewSEL, (void*)CFPD_IClipboard_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIClipboardSEL, FPDIClipboardDestroySEL, (void*)CFPD_IClipboard_V13::Destroy);
	}
};

class CFPD_IPopupMenu_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPopupMenuSEL, FPDIPopupMenuNewSEL, (void*)CFPD_IPopupMenu_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPopupMenuSEL, FPDIPopupMenuDestroySEL, (void*)CFPD_IPopupMenu_V13::Destroy);
	}
};

class CFPD_ITip_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITipSEL, FPDITipNewSEL, (void*)CFPD_ITip_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITipSEL, FPDITipDestroySEL, (void*)CFPD_ITip_V13::Destroy);
	}
};

class CFPD_IOperationNotify_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIOperationNotifySEL, FPDIOperationNotifyNewSEL, (void*)CFPD_IOperationNotify_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIOperationNotifySEL, FPDIOperationNotifyDestroySEL, (void*)CFPD_IOperationNotify_V13::Destroy);
	}
};

class CFPD_IPublicOptionData_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPublicOptionDataSEL, FPDIPublicOptionDataSetForegroundColorSEL, (void*)CFPD_IPublicOptionData_V13::SetForegroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPublicOptionDataSEL, FPDIPublicOptionDataGetForegroundColorSEL, (void*)CFPD_IPublicOptionData_V13::GetForegroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPublicOptionDataSEL, FPDIPublicOptionDataSetBackgroundColorSEL, (void*)CFPD_IPublicOptionData_V13::SetBackgroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPublicOptionDataSEL, FPDIPublicOptionDataGetBackgroundColorSEL, (void*)CFPD_IPublicOptionData_V13::GetBackgroundColor);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPublicOptionDataSEL, FPDIPublicOptionDataSetLayerOpacitySEL, (void*)CFPD_IPublicOptionData_V13::SetLayerOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPublicOptionDataSEL, FPDIPublicOptionDataGetLayerOpacitySEL, (void*)CFPD_IPublicOptionData_V13::GetLayerOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPublicOptionDataSEL, FPDIPublicOptionDataSetLayerFillSEL, (void*)CFPD_IPublicOptionData_V13::SetLayerFill);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPublicOptionDataSEL, FPDIPublicOptionDataGetLayerFillSEL, (void*)CFPD_IPublicOptionData_V13::GetLayerFill);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPublicOptionDataSEL, FPDIPublicOptionDataSetLayerBlendModeSEL, (void*)CFPD_IPublicOptionData_V13::SetLayerBlendMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPublicOptionDataSEL, FPDIPublicOptionDataGetLayerBlendModeSEL, (void*)CFPD_IPublicOptionData_V13::GetLayerBlendMode);
	}
};

class CFPD_IBaseBrushOptionData_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBaseBrushOptionDataSEL, FPDIBaseBrushOptionDataSetBrushDiameterSEL, (void*)CFPD_IBaseBrushOptionData_V13::SetBrushDiameter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBaseBrushOptionDataSEL, FPDIBaseBrushOptionDataGetBrushDiameterSEL, (void*)CFPD_IBaseBrushOptionData_V13::GetBrushDiameter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBaseBrushOptionDataSEL, FPDIBaseBrushOptionDataSetBrushHardnessSEL, (void*)CFPD_IBaseBrushOptionData_V13::SetBrushHardness);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBaseBrushOptionDataSEL, FPDIBaseBrushOptionDataGetBrushHardnessSEL, (void*)CFPD_IBaseBrushOptionData_V13::GetBrushHardness);
	}
};

class CFPD_IBrushOptionData_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBrushOptionDataSEL, FPDIBrushOptionDataSetBlendModeSEL, (void*)CFPD_IBrushOptionData_V13::SetBlendMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBrushOptionDataSEL, FPDIBrushOptionDataGetBlendModeSEL, (void*)CFPD_IBrushOptionData_V13::GetBlendMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBrushOptionDataSEL, FPDIBrushOptionDataSetFlowSEL, (void*)CFPD_IBrushOptionData_V13::SetFlow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBrushOptionDataSEL, FPDIBrushOptionDataGetFlowSEL, (void*)CFPD_IBrushOptionData_V13::GetFlow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBrushOptionDataSEL, FPDIBrushOptionDataSetOpacitySEL, (void*)CFPD_IBrushOptionData_V13::SetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBrushOptionDataSEL, FPDIBrushOptionDataGetOpacitySEL, (void*)CFPD_IBrushOptionData_V13::GetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBrushOptionDataSEL, FPDIBrushOptionDataSetBaseBrushDiameterSEL, (void*)CFPD_IBrushOptionData_V13::SetBaseBrushDiameter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBrushOptionDataSEL, FPDIBrushOptionDataGetBaseBrushDiameterSEL, (void*)CFPD_IBrushOptionData_V13::GetBaseBrushDiameter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBrushOptionDataSEL, FPDIBrushOptionDataSetBaseBrushHardnessSEL, (void*)CFPD_IBrushOptionData_V13::SetBaseBrushHardness);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBrushOptionDataSEL, FPDIBrushOptionDataGetBaseBrushHardnessSEL, (void*)CFPD_IBrushOptionData_V13::GetBaseBrushHardness);
	}
};

class CFPD_IEraserOptionData_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIEraserOptionDataSEL, FPDIEraserOptionDataSetEraserModeSEL, (void*)CFPD_IEraserOptionData_V13::SetEraserMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIEraserOptionDataSEL, FPDIEraserOptionDataGetEraserModeSEL, (void*)CFPD_IEraserOptionData_V13::GetEraserMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIEraserOptionDataSEL, FPDIEraserOptionDataSetFlowSEL, (void*)CFPD_IEraserOptionData_V13::SetFlow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIEraserOptionDataSEL, FPDIEraserOptionDataGetFlowSEL, (void*)CFPD_IEraserOptionData_V13::GetFlow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIEraserOptionDataSEL, FPDIEraserOptionDataSetOpacitySEL, (void*)CFPD_IEraserOptionData_V13::SetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIEraserOptionDataSEL, FPDIEraserOptionDataGetOpacitySEL, (void*)CFPD_IEraserOptionData_V13::GetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIEraserOptionDataSEL, FPDIEraserOptionDataSetEraserHistorySEL, (void*)CFPD_IEraserOptionData_V13::SetEraserHistory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIEraserOptionDataSEL, FPDIEraserOptionDataGetEraserHistorySEL, (void*)CFPD_IEraserOptionData_V13::GetEraserHistory);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIEraserOptionDataSEL, FPDIEraserOptionDataSetBaseBrushDiameterSEL, (void*)CFPD_IEraserOptionData_V13::SetBaseBrushDiameter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIEraserOptionDataSEL, FPDIEraserOptionDataGetBaseBrushDiameterSEL, (void*)CFPD_IEraserOptionData_V13::GetBaseBrushDiameter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIEraserOptionDataSEL, FPDIEraserOptionDataSetBaseBrushHardnessSEL, (void*)CFPD_IEraserOptionData_V13::SetBaseBrushHardness);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIEraserOptionDataSEL, FPDIEraserOptionDataGetBaseBrushHardnessSEL, (void*)CFPD_IEraserOptionData_V13::GetBaseBrushHardness);
	}
};

class CFPD_IMagicWandOptionData_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIMagicWandOptionDataSEL, FPDIMagicWandOptionDataSetToleranceSEL, (void*)CFPD_IMagicWandOptionData_V13::SetTolerance);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIMagicWandOptionDataSEL, FPDIMagicWandOptionDataGetToleranceSEL, (void*)CFPD_IMagicWandOptionData_V13::GetTolerance);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIMagicWandOptionDataSEL, FPDIMagicWandOptionDataSetIsContinuousSEL, (void*)CFPD_IMagicWandOptionData_V13::SetIsContinuous);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIMagicWandOptionDataSEL, FPDIMagicWandOptionDataGetIsContinuousSEL, (void*)CFPD_IMagicWandOptionData_V13::GetIsContinuous);
	}
};

class CFPD_IDodgeOptionData_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIDodgeOptionDataSEL, FPDIDodgeOptionDataSetDodgeModeSEL, (void*)CFPD_IDodgeOptionData_V13::SetDodgeMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIDodgeOptionDataSEL, FPDIDodgeOptionDataGetDodgeModeSEL, (void*)CFPD_IDodgeOptionData_V13::GetDodgeMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIDodgeOptionDataSEL, FPDIDodgeOptionDataSetFlowSEL, (void*)CFPD_IDodgeOptionData_V13::SetFlow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIDodgeOptionDataSEL, FPDIDodgeOptionDataGetFlowSEL, (void*)CFPD_IDodgeOptionData_V13::GetFlow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIDodgeOptionDataSEL, FPDIDodgeOptionDataSetOpacitySEL, (void*)CFPD_IDodgeOptionData_V13::SetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIDodgeOptionDataSEL, FPDIDodgeOptionDataGetOpacitySEL, (void*)CFPD_IDodgeOptionData_V13::GetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIDodgeOptionDataSEL, FPDIDodgeOptionDataSetBaseBrushDiameterSEL, (void*)CFPD_IDodgeOptionData_V13::SetBaseBrushDiameter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIDodgeOptionDataSEL, FPDIDodgeOptionDataGetBaseBrushDiameterSEL, (void*)CFPD_IDodgeOptionData_V13::GetBaseBrushDiameter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIDodgeOptionDataSEL, FPDIDodgeOptionDataSetBaseBrushHardnessSEL, (void*)CFPD_IDodgeOptionData_V13::SetBaseBrushHardness);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIDodgeOptionDataSEL, FPDIDodgeOptionDataGetBaseBrushHardnessSEL, (void*)CFPD_IDodgeOptionData_V13::GetBaseBrushHardness);
	}
};

class CFPD_IBurnOptionData_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBurnOptionDataSEL, FPDIBurnOptionDataSetFlowSEL, (void*)CFPD_IBurnOptionData_V13::SetFlow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBurnOptionDataSEL, FPDIBurnOptionDataGetFlowSEL, (void*)CFPD_IBurnOptionData_V13::GetFlow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBurnOptionDataSEL, FPDIBurnOptionDataSetOpacitySEL, (void*)CFPD_IBurnOptionData_V13::SetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBurnOptionDataSEL, FPDIBurnOptionDataGetOpacitySEL, (void*)CFPD_IBurnOptionData_V13::GetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBurnOptionDataSEL, FPDIBurnOptionDataSetBurnModeSEL, (void*)CFPD_IBurnOptionData_V13::SetBurnMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBurnOptionDataSEL, FPDIBurnOptionDataGetBurnModeSEL, (void*)CFPD_IBurnOptionData_V13::GetBurnMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBurnOptionDataSEL, FPDIBurnOptionDataSetBaseBrushDiameterSEL, (void*)CFPD_IBurnOptionData_V13::SetBaseBrushDiameter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBurnOptionDataSEL, FPDIBurnOptionDataGetBaseBrushDiameterSEL, (void*)CFPD_IBurnOptionData_V13::GetBaseBrushDiameter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBurnOptionDataSEL, FPDIBurnOptionDataSetBaseBrushHardnessSEL, (void*)CFPD_IBurnOptionData_V13::SetBaseBrushHardness);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIBurnOptionDataSEL, FPDIBurnOptionDataGetBaseBrushHardnessSEL, (void*)CFPD_IBurnOptionData_V13::GetBaseBrushHardness);
	}
};

class CFPD_IEyedropperData_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIEyedropperDataSEL, FPDIEyedropperDataGetSampleTypeSEL, (void*)CFPD_IEyedropperData_V13::GetSampleType);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIEyedropperDataSEL, FPDIEyedropperDataSetSampleTypeSEL, (void*)CFPD_IEyedropperData_V13::SetSampleType);
	}
};

class CFPD_ICloneStampData_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDICloneStampDataSEL, FPDICloneStampDataSetBlendModeSEL, (void*)CFPD_ICloneStampData_V13::SetBlendMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDICloneStampDataSEL, FPDICloneStampDataGetBlendModeSEL, (void*)CFPD_ICloneStampData_V13::GetBlendMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDICloneStampDataSEL, FPDICloneStampDataSetFlowSEL, (void*)CFPD_ICloneStampData_V13::SetFlow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDICloneStampDataSEL, FPDICloneStampDataGetFlowSEL, (void*)CFPD_ICloneStampData_V13::GetFlow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDICloneStampDataSEL, FPDICloneStampDataSetOpacitySEL, (void*)CFPD_ICloneStampData_V13::SetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDICloneStampDataSEL, FPDICloneStampDataGetOpacitySEL, (void*)CFPD_ICloneStampData_V13::GetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDICloneStampDataSEL, FPDICloneStampDataSetIsAlineCheckSEL, (void*)CFPD_ICloneStampData_V13::SetIsAlineCheck);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDICloneStampDataSEL, FPDICloneStampDataGetIsAlineCheckSEL, (void*)CFPD_ICloneStampData_V13::GetIsAlineCheck);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDICloneStampDataSEL, FPDICloneStampDataSetBaseBrushDiameterSEL, (void*)CFPD_ICloneStampData_V13::SetBaseBrushDiameter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDICloneStampDataSEL, FPDICloneStampDataGetBaseBrushDiameterSEL, (void*)CFPD_ICloneStampData_V13::GetBaseBrushDiameter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDICloneStampDataSEL, FPDICloneStampDataSetBaseBrushHardnessSEL, (void*)CFPD_ICloneStampData_V13::SetBaseBrushHardness);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDICloneStampDataSEL, FPDICloneStampDataGetBaseBrushHardnessSEL, (void*)CFPD_ICloneStampData_V13::GetBaseBrushHardness);
	}
};

class CFPD_IPaintBucketOptionData_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPaintBucketOptionDataSEL, FPDIPaintBucketOptionDataSetToleranceSEL, (void*)CFPD_IPaintBucketOptionData_V13::SetTolerance);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPaintBucketOptionDataSEL, FPDIPaintBucketOptionDataGetToleranceSEL, (void*)CFPD_IPaintBucketOptionData_V13::GetTolerance);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPaintBucketOptionDataSEL, FPDIPaintBucketOptionDataSetOpacitySEL, (void*)CFPD_IPaintBucketOptionData_V13::SetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPaintBucketOptionDataSEL, FPDIPaintBucketOptionDataGetOpacitySEL, (void*)CFPD_IPaintBucketOptionData_V13::GetOpacity);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPaintBucketOptionDataSEL, FPDIPaintBucketOptionDataSetColorBlendModeSEL, (void*)CFPD_IPaintBucketOptionData_V13::SetColorBlendMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPaintBucketOptionDataSEL, FPDIPaintBucketOptionDataGetColorBlendModeSEL, (void*)CFPD_IPaintBucketOptionData_V13::GetColorBlendMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPaintBucketOptionDataSEL, FPDIPaintBucketOptionDataSetContinuousSEL, (void*)CFPD_IPaintBucketOptionData_V13::SetContinuous);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIPaintBucketOptionDataSEL, FPDIPaintBucketOptionDataGetContinuousSEL, (void*)CFPD_IPaintBucketOptionData_V13::GetContinuous);
	}
};

class CFPD_ISpotHealingBrushData_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDISpotHealingBrushDataSEL, FPDISpotHealingBrushDataGetDiameterSEL, (void*)CFPD_ISpotHealingBrushData_V13::GetDiameter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDISpotHealingBrushDataSEL, FPDISpotHealingBrushDataSetDiameterSEL, (void*)CFPD_ISpotHealingBrushData_V13::SetDiameter);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDISpotHealingBrushDataSEL, FPDISpotHealingBrushDataGetRoundnessSEL, (void*)CFPD_ISpotHealingBrushData_V13::GetRoundness);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDISpotHealingBrushDataSEL, FPDISpotHealingBrushDataSetRoundnessSEL, (void*)CFPD_ISpotHealingBrushData_V13::SetRoundness);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDISpotHealingBrushDataSEL, FPDISpotHealingBrushDataGetAlineSEL, (void*)CFPD_ISpotHealingBrushData_V13::GetAline);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDISpotHealingBrushDataSEL, FPDISpotHealingBrushDataSetAlineSEL, (void*)CFPD_ISpotHealingBrushData_V13::SetAline);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDISpotHealingBrushDataSEL, FPDISpotHealingBrushDataGetModeSEL, (void*)CFPD_ISpotHealingBrushData_V13::GetMode);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDISpotHealingBrushDataSEL, FPDISpotHealingBrushDataSetModeSEL, (void*)CFPD_ISpotHealingBrushData_V13::SetMode);
	}
};

class CFPD_CEditObject_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCEditObjectSEL, FPDCEditObjectNewSEL, (void*)CFPD_CEditObject_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCEditObjectSEL, FPDCEditObjectNew2SEL, (void*)CFPD_CEditObject_V13::New2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCEditObjectSEL, FPDCEditObjectDeleteSEL, (void*)CFPD_CEditObject_V13::Delete);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCEditObjectSEL, FPDCEditObjectCombineObjctSEL, (void*)CFPD_CEditObject_V13::CombineObjct);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCEditObjectSEL, FPDCEditObjectIsEqualSEL, (void*)CFPD_CEditObject_V13::IsEqual);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCEditObjectSEL, FPDCEditObjectCopyBaseInfoSEL, (void*)CFPD_CEditObject_V13::CopyBaseInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCEditObjectSEL, FPDCEditObjectUpdateFormInfoSEL, (void*)CFPD_CEditObject_V13::UpdateFormInfo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCEditObjectSEL, FPDCEditObjectUpdateFormInfo2SEL, (void*)CFPD_CEditObject_V13::UpdateFormInfo2);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCEditObjectSEL, FPDCEditObjectGetLastFormObjSEL, (void*)CFPD_CEditObject_V13::GetLastFormObj);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCEditObjectSEL, FPDCEditObjectGetFirstFormObjSEL, (void*)CFPD_CEditObject_V13::GetFirstFormObj);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCEditObjectSEL, FPDCEditObjectGetContainerSEL, (void*)CFPD_CEditObject_V13::GetContainer);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCEditObjectSEL, FPDCEditObjectGetObjBBoxSEL, (void*)CFPD_CEditObject_V13::GetObjBBox);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCEditObjectSEL, FPDCEditObjectResetSEL, (void*)CFPD_CEditObject_V13::Reset);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCEditObjectSEL, FPDCEditObjectUndoRedoStateSEL, (void*)CFPD_CEditObject_V13::UndoRedoState);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCEditObjectSEL, FPDCEditObjectMergeTextObjRangeSEL, (void*)CFPD_CEditObject_V13::MergeTextObjRange);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDCEditObjectSEL, FPDCEditObjectMergeTextObjRange2SEL, (void*)CFPD_CEditObject_V13::MergeTextObjRange2);
	}
};

class CFPD_IJoinSplit_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitExitJoinEditingSEL, (void*)CFPD_IJoinSplit_V13::ExitJoinEditing);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitOnActivateSEL, (void*)CFPD_IJoinSplit_V13::OnActivate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitOnDeactivateSEL, (void*)CFPD_IJoinSplit_V13::OnDeactivate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitOnPaintSEL, (void*)CFPD_IJoinSplit_V13::OnPaint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitOnLeftButtonDownSEL, (void*)CFPD_IJoinSplit_V13::OnLeftButtonDown);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitOnLeftButtonUpSEL, (void*)CFPD_IJoinSplit_V13::OnLeftButtonUp);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitOnMouseMoveSEL, (void*)CFPD_IJoinSplit_V13::OnMouseMove);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitOnRightButtonUpSEL, (void*)CFPD_IJoinSplit_V13::OnRightButtonUp);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitIsProcessingSEL, (void*)CFPD_IJoinSplit_V13::IsProcessing);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitBtnEnableStatusSEL, (void*)CFPD_IJoinSplit_V13::BtnEnableStatus);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitJoinBoxesSEL, (void*)CFPD_IJoinSplit_V13::JoinBoxes);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitSplitBoxesSEL, (void*)CFPD_IJoinSplit_V13::SplitBoxes);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitLinkBoxesSEL, (void*)CFPD_IJoinSplit_V13::LinkBoxes);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitUnlinkBoxesSEL, (void*)CFPD_IJoinSplit_V13::UnlinkBoxes);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitSelectNoneSEL, (void*)CFPD_IJoinSplit_V13::SelectNone);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitOnAfterInsertPagesSEL, (void*)CFPD_IJoinSplit_V13::OnAfterInsertPages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitOnAfterDeletePagesSEL, (void*)CFPD_IJoinSplit_V13::OnAfterDeletePages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitOnAfterReplacePagesSEL, (void*)CFPD_IJoinSplit_V13::OnAfterReplacePages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitOnAfterResizePageSEL, (void*)CFPD_IJoinSplit_V13::OnAfterResizePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitOnAfterRotatePageSEL, (void*)CFPD_IJoinSplit_V13::OnAfterRotatePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDIJoinSplitSEL, FPDIJoinSplitOnAfterMovePagesSEL, (void*)CFPD_IJoinSplit_V13::OnAfterMovePages);
	}
};

class CFPD_ITouchup_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnActivateSEL, (void*)CFPD_ITouchup_V13::OnActivate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnDeactivateSEL, (void*)CFPD_ITouchup_V13::OnDeactivate);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupExistEditingSEL, (void*)CFPD_ITouchup_V13::ExistEditing);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnPaintSEL, (void*)CFPD_ITouchup_V13::OnPaint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnCharSEL, (void*)CFPD_ITouchup_V13::OnChar);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnVKUPSEL, (void*)CFPD_ITouchup_V13::OnVKUP);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnVKDOWNSEL, (void*)CFPD_ITouchup_V13::OnVKDOWN);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnLeftButtonDownSEL, (void*)CFPD_ITouchup_V13::OnLeftButtonDown);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnLeftButtonUpSEL, (void*)CFPD_ITouchup_V13::OnLeftButtonUp);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnLeftButtonDblClkSEL, (void*)CFPD_ITouchup_V13::OnLeftButtonDblClk);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnMouseMoveSEL, (void*)CFPD_ITouchup_V13::OnMouseMove);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnMouseWheelSEL, (void*)CFPD_ITouchup_V13::OnMouseWheel);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnRightButtonDownSEL, (void*)CFPD_ITouchup_V13::OnRightButtonDown);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnRightButtonUpSEL, (void*)CFPD_ITouchup_V13::OnRightButtonUp);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupCanSelectAllSEL, (void*)CFPD_ITouchup_V13::CanSelectAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupDoSelectAllSEL, (void*)CFPD_ITouchup_V13::DoSelectAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupCanDeleteSEL, (void*)CFPD_ITouchup_V13::CanDelete);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupDoDeleteSEL, (void*)CFPD_ITouchup_V13::DoDelete);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupCanCopySEL, (void*)CFPD_ITouchup_V13::CanCopy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupDoCopySEL, (void*)CFPD_ITouchup_V13::DoCopy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupCanCutSEL, (void*)CFPD_ITouchup_V13::CanCut);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupDoCutSEL, (void*)CFPD_ITouchup_V13::DoCut);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupCanPasteSEL, (void*)CFPD_ITouchup_V13::CanPaste);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupDoPasteSEL, (void*)CFPD_ITouchup_V13::DoPaste);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupCanDeselectAllSEL, (void*)CFPD_ITouchup_V13::CanDeselectAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupDoDeselectAllSEL, (void*)CFPD_ITouchup_V13::DoDeselectAll);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnFontNameChangedSEL, (void*)CFPD_ITouchup_V13::OnFontNameChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnFontSizeChangedSEL, (void*)CFPD_ITouchup_V13::OnFontSizeChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnTextColorChangedSEL, (void*)CFPD_ITouchup_V13::OnTextColorChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnBoldItlicChangedSEL, (void*)CFPD_ITouchup_V13::OnBoldItlicChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnAlignChangedSEL, (void*)CFPD_ITouchup_V13::OnAlignChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnCharSpaceChangedSEL, (void*)CFPD_ITouchup_V13::OnCharSpaceChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnCharHorzScaleChangedSEL, (void*)CFPD_ITouchup_V13::OnCharHorzScaleChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnLineLeadingChangedSEL, (void*)CFPD_ITouchup_V13::OnLineLeadingChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnUnderlineChangedSEL, (void*)CFPD_ITouchup_V13::OnUnderlineChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnCrossChangedSEL, (void*)CFPD_ITouchup_V13::OnCrossChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnSuperScriptChangedSEL, (void*)CFPD_ITouchup_V13::OnSuperScriptChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnSubScriptChangedSEL, (void*)CFPD_ITouchup_V13::OnSubScriptChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnParagraphSpacingChangedSEL, (void*)CFPD_ITouchup_V13::OnParagraphSpacingChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnInDentChangedSEL, (void*)CFPD_ITouchup_V13::OnInDentChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnDeDentChangedSEL, (void*)CFPD_ITouchup_V13::OnDeDentChanged);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnAfterInsertPagesSEL, (void*)CFPD_ITouchup_V13::OnAfterInsertPages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnAfterDeletePagesSEL, (void*)CFPD_ITouchup_V13::OnAfterDeletePages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnAfterReplacePagesSEL, (void*)CFPD_ITouchup_V13::OnAfterReplacePages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnAfterResizePageSEL, (void*)CFPD_ITouchup_V13::OnAfterResizePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnAfterRotatePageSEL, (void*)CFPD_ITouchup_V13::OnAfterRotatePage);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnAfterMovePagesSEL, (void*)CFPD_ITouchup_V13::OnAfterMovePages);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupOnPageContentChangeSEL, (void*)CFPD_ITouchup_V13::OnPageContentChange);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupSEL, FPDITouchupGetFXEditorSEL, (void*)CFPD_ITouchup_V13::GetFXEditor);
	}
};

class CFPD_ITouchupManager_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupManagerSEL, FPDITouchupManagerCreateManagerSEL, (void*)CFPD_ITouchupManager_V13::CreateManager);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupManagerSEL, FPDITouchupManagerGetTouchupSEL, (void*)CFPD_ITouchupManager_V13::GetTouchup);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchupManagerSEL, FPDITouchupManagerGetJoinSpiltSEL, (void*)CFPD_ITouchupManager_V13::GetJoinSpilt);
	}
};

class CFPD_ITouchUndoItem_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchUndoItemSEL, FPDITouchUndoItemOnUndoSEL, (void*)CFPD_ITouchUndoItem_V13::OnUndo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchUndoItemSEL, FPDITouchUndoItemOnRedoSEL, (void*)CFPD_ITouchUndoItem_V13::OnRedo);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchUndoItemSEL, FPDITouchUndoItemOnReleaseSEL, (void*)CFPD_ITouchUndoItem_V13::OnRelease);
	}
};

class CFPD_ITouchClipboard_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchClipboardSEL, FPDITouchClipboardNewSEL, (void*)CFPD_ITouchClipboard_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchClipboardSEL, FPDITouchClipboardDestroySEL, (void*)CFPD_ITouchClipboard_V13::Destroy);
	}
};

class CFPD_ITouchPopupMenu_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchPopupMenuSEL, FPDITouchPopupMenuNewSEL, (void*)CFPD_ITouchPopupMenu_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchPopupMenuSEL, FPDITouchPopupMenuDestroySEL, (void*)CFPD_ITouchPopupMenu_V13::Destroy);
	}
};

class CFPD_ITouchProgressBar_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchProgressBarSEL, FPDITouchProgressBarNewSEL, (void*)CFPD_ITouchProgressBar_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchProgressBarSEL, FPDITouchProgressBarDestroySEL, (void*)CFPD_ITouchProgressBar_V13::Destroy);
	}
};

class CFPD_ITouchOperationNotify_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchOperationNotifySEL, FPDITouchOperationNotifyNewSEL, (void*)CFPD_ITouchOperationNotify_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchOperationNotifySEL, FPDITouchOperationNotifyDestroySEL, (void*)CFPD_ITouchOperationNotify_V13::Destroy);
	}
};

class CFPD_ITouchUndoHandler_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchUndoHandlerSEL, FPDITouchUndoHandlerNewSEL, (void*)CFPD_ITouchUndoHandler_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchUndoHandlerSEL, FPDITouchUndoHandlerDestroySEL, (void*)CFPD_ITouchUndoHandler_V13::Destroy);
	}
};

class CFPD_ITouchTextFormatHandler_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchTextFormatHandlerSEL, FPDITouchTextFormatHandlerNewSEL, (void*)CFPD_ITouchTextFormatHandler_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchTextFormatHandlerSEL, FPDITouchTextFormatHandlerDestroySEL, (void*)CFPD_ITouchTextFormatHandler_V13::Destroy);
	}
};

class CFPD_ITouchProvider_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchProviderSEL, FPDITouchProviderNewSEL, (void*)CFPD_ITouchProvider_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDITouchProviderSEL, FPDITouchProviderDestroySEL, (void*)CFPD_ITouchProvider_V13::Destroy);
	}
};

// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
class CFPD_StandardCryptoHandler_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStandardCryptoHandlerSEL, FPDStandardCryptoHandlerNewSEL, (void*)CFPD_StandardCryptoHandler_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStandardCryptoHandlerSEL, FPDStandardCryptoHandlerDestroySEL, (void*)CFPD_StandardCryptoHandler_V13::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStandardCryptoHandlerSEL, FPDStandardCryptoHandlerInitSEL, (void*)CFPD_StandardCryptoHandler_V13::Init);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStandardCryptoHandlerSEL, FPDStandardCryptoHandlerInitByDicSEL, (void*)CFPD_StandardCryptoHandler_V13::InitByDic);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStandardCryptoHandlerSEL, FPDStandardCryptoHandlerDecryptGetSizeSEL, (void*)CFPD_StandardCryptoHandler_V13::DecryptGetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStandardCryptoHandlerSEL, FPDStandardCryptoHandlerDecryptStartSEL, (void*)CFPD_StandardCryptoHandler_V13::DecryptStart);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStandardCryptoHandlerSEL, FPDStandardCryptoHandlerDecryptStreamSEL, (void*)CFPD_StandardCryptoHandler_V13::DecryptStream);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStandardCryptoHandlerSEL, FPDStandardCryptoHandlerDecryptFinishSEL, (void*)CFPD_StandardCryptoHandler_V13::DecryptFinish);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStandardCryptoHandlerSEL, FPDStandardCryptoHandlerEncryptGetSizeSEL, (void*)CFPD_StandardCryptoHandler_V13::EncryptGetSize);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStandardCryptoHandlerSEL, FPDStandardCryptoHandlerEncryptContentSEL, (void*)CFPD_StandardCryptoHandler_V13::EncryptContent);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDStandardCryptoHandlerSEL, FPDStandardCryptoHandlerCloneSEL, (void*)CFPD_StandardCryptoHandler_V13::Clone);
	}
};

class CFPD_FipsStandardCryptoHandler_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFipsStandardCryptoHandlerSEL, FPDFipsStandardCryptoHandlerNewSEL, (void*)CFPD_FipsStandardCryptoHandler_V13::New);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFipsStandardCryptoHandlerSEL, FPDFipsStandardCryptoHandlerDestroySEL, (void*)CFPD_FipsStandardCryptoHandler_V13::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFipsStandardCryptoHandlerSEL, FPDFipsStandardCryptoHandlerInitSEL, (void*)CFPD_FipsStandardCryptoHandler_V13::Init);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDFipsStandardCryptoHandlerSEL, FPDFipsStandardCryptoHandlerInit2SEL, (void*)CFPD_FipsStandardCryptoHandler_V13::Init2);
	}
};

// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
class CFPD_OutputPreview_V13_Initiator
{
public:
	static void InitFuncMappingTable(void* pMgr, bool bNeedInitParent)
	{
		FS_ASSERT(pMgr != NULL);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOutputPreviewSEL, FPDOutputPreviewCreateSEL, (void*)CFPD_OutputPreview_V13::Create);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOutputPreviewSEL, FPDOutputPreviewDestroySEL, (void*)CFPD_OutputPreview_V13::Destroy);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOutputPreviewSEL, FPDOutputPreviewSetSimulationProfileSEL, (void*)CFPD_OutputPreview_V13::SetSimulationProfile);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOutputPreviewSEL, FPDOutputPreviewHasPageOverprintSEL, (void*)CFPD_OutputPreview_V13::HasPageOverprint);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOutputPreviewSEL, FPDOutputPreviewSetShowSEL, (void*)CFPD_OutputPreview_V13::SetShow);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOutputPreviewSEL, FPDOutputPreviewSetPreviewSEL, (void*)CFPD_OutputPreview_V13::SetPreview);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOutputPreviewSEL, FPDOutputPreviewGetPlatesSEL, (void*)CFPD_OutputPreview_V13::GetPlates);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOutputPreviewSEL, FPDOutputPreviewSetCheckSEL, (void*)CFPD_OutputPreview_V13::SetCheck);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOutputPreviewSEL, FPDOutputPreviewGetCheckSEL, (void*)CFPD_OutputPreview_V13::GetCheck);
		((FRCoreHFTMgr*)pMgr)->UpdateInterface(FPDOutputPreviewSEL, FPDOutputPreviewGetFinalBitmapSEL, (void*)CFPD_OutputPreview_V13::GetFinalBitmap);
	}
};

// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//Auto generated file end!
#endif

#endif