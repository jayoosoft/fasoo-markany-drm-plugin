﻿/*****************************************************************************

Copyright (C) 2010 Foxit Corporation
All rights reserved.

NOTICE: Foxit permits you to use, modify, and distribute this file
in accordance with the terms of the Foxit license agreement
accompanying it. If you have received this file from a source other
than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
 
*****************************************************************************/

//----------_V1----------
// In file fs_basicImpl.h
CATEGORY(FSExtensionHFTMgr)
CATEGORY(FSAffineMatrix)
CATEGORY(FSDIBitmap)
CATEGORY(FSMapPtrToPtr)
CATEGORY(FSPtrArray)
CATEGORY(FSByteArray)
CATEGORY(FSWordArray)
CATEGORY(FSDWordArray)
CATEGORY(FSByteStringArray)
CATEGORY(FSWideStringArray)
CATEGORY(FSCodeTransformation)
CATEGORY(FSFloatRectArray)
CATEGORY(FSBinaryBuf)
CATEGORY(FSPauseHandler)
CATEGORY(FSFileReadHandler)
CATEGORY(FSStreamWriteHandler)
// fs_basicImpl.h end

// In file fs_stringImpl.h
CATEGORY(FSCharMap)
CATEGORY(FSByteString)
CATEGORY(FSWideString)
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
CATEGORY(FRTool)
CATEGORY(FRApp)
CATEGORY(FRLanguage)
CATEGORY(FRUIProgress)
// fr_appImpl.h end

// In file fr_barImpl.h
CATEGORY(FRToolButton)
CATEGORY(FRToolBar)
CATEGORY(FRMessageBar)
// fr_barImpl.h end

// In file fr_docImpl.h
CATEGORY(FRDoc)
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
CATEGORY(FRMenuBar)
CATEGORY(FRMenu)
CATEGORY(FRMenuItem)
// fr_menuImpl.h end

// In file fr_sysImpl.h
CATEGORY(FRSys)
// fr_sysImpl.h end

// In file fr_viewImpl.h
CATEGORY(FRDocView)
CATEGORY(FRPageView)
CATEGORY(FRTextSelectTool)
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
CATEGORY(FPDDoc)
CATEGORY(FPDNameTree)
CATEGORY(FPDBookmark)
CATEGORY(FPDDest)
CATEGORY(FPDOCContext)
CATEGORY(FPDOCGroup)
CATEGORY(FPDOCGroupSet)
CATEGORY(FPDOCNotify)
CATEGORY(FPDOCProperties)
CATEGORY(FPDLWinParam)
CATEGORY(FPDActionFields)
CATEGORY(FPDAction)
CATEGORY(FPDAAction)
CATEGORY(FPDDocJSActions)
CATEGORY(FPDFileSpec)
CATEGORY(FPDMediaPlayer)
CATEGORY(FPDRendition)
CATEGORY(FPDLink)
CATEGORY(FPDAnnot)
CATEGORY(FPDAnnotList)
CATEGORY(FPDDefaultAppearance)
CATEGORY(FPDFormNotify)
CATEGORY(FPDInterForm)
CATEGORY(FPDFormField)
CATEGORY(FPDIconFit)
CATEGORY(FPDFormControl)
CATEGORY(FPDFDFDoc)
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
CATEGORY(FPDObject)
CATEGORY(FPDBoolean)
CATEGORY(FPDNumber)
CATEGORY(FPDString)
CATEGORY(FPDName)
CATEGORY(FPDArray)
CATEGORY(FPDDictionary)
CATEGORY(FPDStream)
CATEGORY(FPDStreamAcc)
CATEGORY(FPDStreamFilter)
CATEGORY(FPDNull)
CATEGORY(FPDReference)
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
CATEGORY(FPDPage)
CATEGORY(FPDParseOptions)
CATEGORY(FPDForm)
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
CATEGORY(FPDPath)
CATEGORY(FPDClipPath)
CATEGORY(FPDColorState)
CATEGORY(FPDTextState)
CATEGORY(FPDGeneralState)
CATEGORY(FPDGraphState)
CATEGORY(FPDPageObject)
CATEGORY(FPDTextObject)
CATEGORY(FPDPathObject)
CATEGORY(FPDImageObject)
CATEGORY(FPDShadingObject)
CATEGORY(FPDFormObject)
CATEGORY(FPDInlineImages)
CATEGORY(FPDContentMarkItem)
CATEGORY(FPDContentMark)
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
CATEGORY(FPDParser)
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
CATEGORY(FPDRenderOptions)
CATEGORY(FPDRenderContext)
CATEGORY(FPDProgressiveRender)
CATEGORY(FPDRenderDevice)
CATEGORY(FPDFxgeDevice)
CATEGORY(FPDWindowsDevice)
CATEGORY(FPDPageRenderCache)
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
CATEGORY(FPDFont)
CATEGORY(FPDFontEncoding)
CATEGORY(FPDType1Font)
CATEGORY(FPDTrueTypeFont)
CATEGORY(FPDType3Char)
CATEGORY(FPDType3Font)
CATEGORY(FPDCIDFont)
CATEGORY(FPDCIDUtil)
CATEGORY(FPDColorSpace)
CATEGORY(FPDColor)
CATEGORY(FPDPattern)
CATEGORY(FPDTilingPattern)
CATEGORY(FPDShadingPattern)
CATEGORY(FPDMeshStream)
CATEGORY(FPDImage)
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
CATEGORY(FPDObjArchiveSaver)
CATEGORY(FPDObjArchiveLoader)
CATEGORY(FPDPageArchiveSaver)
CATEGORY(FPDPageArchiveLoader)
CATEGORY(FPDCreator)
// fpd_serialImpl.h end

// In file fpd_textImpl.h
CATEGORY(FPDProgressiveSearch)
CATEGORY(FPDTextPage)
CATEGORY(FPDTextPageFind)
CATEGORY(FPDLinkExtract)
// fpd_textImpl.h end

//----------_V2----------
// In file fs_basicImpl.h
CATEGORY(FSBase64Encoder)
CATEGORY(FSBase64Decoder)
CATEGORY(FSFileWriteHandler)
CATEGORY(FSXMLElement)
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
CATEGORY(FDRMCategoryRead)
CATEGORY(FDRMCategoryWrite)
CATEGORY(FDRMDescData)
CATEGORY(FDRMScriptData)
CATEGORY(FDRMPresentationData)
CATEGORY(FDRMSignatureData)
CATEGORY(FDRMDescRead)
CATEGORY(FDRMDescWrite)
CATEGORY(FDRMFoacRead)
CATEGORY(FDRMFoacWrite)
CATEGORY(FDRMEnvelopeRead)
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
CATEGORY(FDRMMgr)
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
CATEGORY(FDRMPDFSecurityHandler)
CATEGORY(FDRMPDFSchema)
CATEGORY(FDRMEncryptDictRead)
CATEGORY(FDRMEncryptor)
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
CATEGORY(FDRMPKI)
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
CATEGORY(FPDWrapperCreator)
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V3----------
// In file fs_basicImpl.h
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
CATEGORY(FRThumbnailView)
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V4----------
// In file fs_basicImpl.h
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
CATEGORY(FRTabBand)
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V5----------
// In file fs_basicImpl.h
CATEGORY(FSUTF8Decoder)
CATEGORY(FSUTF8Encoder)
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
CATEGORY(FRInternal)
CATEGORY(FRSpellCheck)
// fr_appImpl.h end

// In file fr_barImpl.h
CATEGORY(FRRibbonBar)
CATEGORY(FRRibbonCategory)
CATEGORY(FRRibbonPanel)
CATEGORY(FRRibbonElement)
CATEGORY(FRRibbonButton)
CATEGORY(FRRibbonEdit)
CATEGORY(FRRibbonLabel)
CATEGORY(FRRibbonCheckBox)
CATEGORY(FRRibbonRadioButton)
CATEGORY(FRRibbonComboBox)
CATEGORY(FRRibbonFontComboBox)
CATEGORY(FRRibbonPaletteButton)
CATEGORY(FRRibbonColorButton)
CATEGORY(FRRibbonSlider)
CATEGORY(FRRibbonListButton)
CATEGORY(FRRibbonBackStageViewItem)
CATEGORY(FRRibbonStyleButton)
CATEGORY(FRRibbonStyleListBox)
CATEGORY(FRRibbonStyleStatic)
CATEGORY(FRFormatTools)
CATEGORY(FRPropertyTools)
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
CATEGORY(FRWindowsDIB)
// fr_sysImpl.h end

// In file fr_viewImpl.h
CATEGORY(FRAnnot)
CATEGORY(FRResourcePropertyBox)
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
CATEGORY(FPDFXFontEncoding)
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V6----------
// In file fs_basicImpl.h
CATEGORY(FSFileStream)
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
CATEGORY(FRScrollBarThumbnailView)
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V7----------
// In file fs_basicImpl.h
CATEGORY(FSGUID)
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
CATEGORY(FRHTMLMgr)
CATEGORY(FRPanelMgr)
// fr_appImpl.h end

// In file fr_barImpl.h
CATEGORY(FRFuncBtn)
CATEGORY(FRStatusBar)
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
CATEGORY(FPDConnectedInfo)
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V8----------
// In file fs_basicImpl.h
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
CATEGORY(FRCustomSignature)
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V9----------
// In file fs_basicImpl.h
CATEGORY(FSUUID)
CATEGORY(FSMapByteStringToPtr)
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
CATEGORY(FRCloudLoginProvider)
// fr_appImpl.h end

// In file fr_barImpl.h
CATEGORY(FRBulbMsgCenter)
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
CATEGORY(FPDEPUB)
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V10----------
// In file fs_basicImpl.h
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
CATEGORY(FOFDCryptoDict)
CATEGORY(FOFDSecurityHandler)
CATEGORY(FOFDStdSecurityHandler)
CATEGORY(FOFDStdCertSecurityHandler)
CATEGORY(FOFDSMSecurityHandler)
CATEGORY(FOFDCryptoHandler)
CATEGORY(FOFDStdCryptoHandler)
CATEGORY(FOFDSM4CryptoHandler)
CATEGORY(FOFDFileStream)
CATEGORY(FOFDPauseHandler)
CATEGORY(FOFDUIMgr)
CATEGORY(FOFDDIBAttribute)
CATEGORY(FOFDCodeC)
CATEGORY(FOFDPrintSetting)
CATEGORY(FOFDSys)
// fofd_basicImpl.h end

// In file fofd_docImpl.h
CATEGORY(FOFDPackage)
CATEGORY(FOFDParser)
CATEGORY(FOFDDoc)
CATEGORY(FOFDWriteDoc)
CATEGORY(FOFDCreator)
CATEGORY(FOFDPerms)
CATEGORY(FOFDVPrefers)
CATEGORY(FOFDBookmarks)
CATEGORY(FOFDBookmark)
CATEGORY(FOFDOutline)
CATEGORY(FOFDActions)
CATEGORY(FOFDAction)
CATEGORY(FOFDActionGoto)
CATEGORY(FOFDActionGotoA)
CATEGORY(FOFDActionRegion)
// fofd_docImpl.h end

// In file fofd_pageImpl.h
CATEGORY(FOFDPage)
CATEGORY(FOFDDest)
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
CATEGORY(FOFDRenderOptions)
CATEGORY(FOFDRenderContext)
CATEGORY(FOFDRenderDevice)
CATEGORY(FOFDProgressiveRenderer)
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
CATEGORY(FOFDSign)
// fofd_sigImpl.h end

// In file fpd_docImpl.h
CATEGORY(FPDWrapperDoc)
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
CATEGORY(FPDUnencryptedWrapperCreator)
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V11----------
// In file fs_basicImpl.h
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
CATEGORY(FRAppFxNet)
CATEGORY(FRInternalFxNet)
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
CATEGORY(FPDColorSeparator)
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
CATEGORY(FPDColorConvertor)
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V12----------
// In file fs_basicImpl.h
CATEGORY(FSImage)
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
CATEGORY(FRAssistantMgr)
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
CATEGORY(FPD3dContext)
CATEGORY(FPD3dAnnotData)
CATEGORY(FPD3dAnnotData3dArtwork)
CATEGORY(FPD3dScene)
CATEGORY(FPD3deAsset)
CATEGORY(FPD3deRuntime)
CATEGORY(FPD3deCanvas)
CATEGORY(FPD3deView)
CATEGORY(FPD3deViewRenderMode)
CATEGORY(FPD3deViewLightingScheme)
CATEGORY(FPD3deCanvasControllerTool)
CATEGORY(FPD3dVendor)
CATEGORY(FPD3deViewBackground)
CATEGORY(FPD3deViewCameraParam)
CATEGORY(FPD3deViewCrossSection)
CATEGORY(FPD3deViewNode)
CATEGORY(FPD3deMeasure)
CATEGORY(FPD3deViewMiscOptions)
CATEGORY(FPD3deTextProvider)
CATEGORY(FPDScriptHostHostProvider)
CATEGORY(FPDScript3DEngine)
CATEGORY(FPD3DI18NProviderHandler)
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V13----------
// In file fs_basicImpl.h
CATEGORY(FSReaderDateTime)
CATEGORY(FSMonoscale)
CATEGORY(FSFloatArray)
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
CATEGORY(FRIReader)
CATEGORY(FRIFX)
CATEGORY(FRInputMethodHandler)
CATEGORY(FRMarkAnnotExtendHandler)
// fr_appImpl.h end

// In file fr_barImpl.h
CATEGORY(FRRibbonStyleRadioBox)
CATEGORY(FRRibbonStyleCheckBox)
CATEGORY(FRRibbonStyleEdit)
CATEGORY(FRRibbonStyleLinkButton)
CATEGORY(FRRibbonStyleSliderCtrl)
CATEGORY(FRRibbonStyleColorButton)
CATEGORY(FRRibbonIFXBCGPComboBox)
CATEGORY(FRRibbonIFXBCGPFontComboBox)
CATEGORY(FRCFXBCGStyleGalleryCtrl)
// fr_barImpl.h end

// In file fr_docImpl.h
CATEGORY(FRReaderInterform)
CATEGORY(FRUndoItemCreateWidget)
CATEGORY(FRCRSAStamp)
CATEGORY(FRMarkupAnnot)
CATEGORY(FRMarkupPopup)
CATEGORY(FRMarkupPanel)
CATEGORY(FRCRSASStampAnnot)
CATEGORY(FREncryptPermisson)
CATEGORY(FRCSGCertFileManage)
CATEGORY(FRCSG)
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
CATEGORY(FRReader)
CATEGORY(FRIReaderDispViewerHandler)
CATEGORY(FRIPDFViewerEventHandler)
CATEGORY(FRToolFormatMgr)
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
CATEGORY(FPDCertStore)
CATEGORY(FPD3DCompositionProvider)
CATEGORY(FPDPageLabel)
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
CATEGORY(FPDPathObjectUtils)
CATEGORY(FPDShadingObjectUtils)
CATEGORY(FPDPathEditor)
CATEGORY(FPDShadingEditor)
CATEGORY(FPDTextEditor)
CATEGORY(FPDTextObjectUtils)
CATEGORY(FPDGraphicObject)
CATEGORY(FPDGraphicEditor)
CATEGORY(FPDGraphicObjectUtils)
CATEGORY(FPDImageEditor)
CATEGORY(FPDIPageEditor)
CATEGORY(FPDIUndoItem)
CATEGORY(FPDIClipboard)
CATEGORY(FPDIPopupMenu)
CATEGORY(FPDITip)
CATEGORY(FPDIOperationNotify)
CATEGORY(FPDIPublicOptionData)
CATEGORY(FPDIBaseBrushOptionData)
CATEGORY(FPDIBrushOptionData)
CATEGORY(FPDIEraserOptionData)
CATEGORY(FPDIMagicWandOptionData)
CATEGORY(FPDIDodgeOptionData)
CATEGORY(FPDIBurnOptionData)
CATEGORY(FPDIEyedropperData)
CATEGORY(FPDICloneStampData)
CATEGORY(FPDIPaintBucketOptionData)
CATEGORY(FPDISpotHealingBrushData)
CATEGORY(FPDCEditObject)
CATEGORY(FPDIJoinSplit)
CATEGORY(FPDITouchup)
CATEGORY(FPDITouchupManager)
CATEGORY(FPDITouchUndoItem)
CATEGORY(FPDITouchClipboard)
CATEGORY(FPDITouchPopupMenu)
CATEGORY(FPDITouchProgressBar)
CATEGORY(FPDITouchOperationNotify)
CATEGORY(FPDITouchUndoHandler)
CATEGORY(FPDITouchTextFormatHandler)
CATEGORY(FPDITouchProvider)
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
CATEGORY(FPDStandardCryptoHandler)
CATEGORY(FPDFipsStandardCryptoHandler)
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
CATEGORY(FPDOutputPreview)
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

