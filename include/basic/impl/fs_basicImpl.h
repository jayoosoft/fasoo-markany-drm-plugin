﻿/** @file fr_basic.h.
 * 
 *  @brief defined data types.
 */

#ifndef FS_BASICIMPL_H
#define FS_BASICIMPL_H

#ifndef FS_INTERNALINC_H
#include "../fs_internalInc.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif//__cplusplus

class CFS_ExtensionHFTMgr_V1
{
public:

	//************************************
	// Function:  NewHFT
	// Param[in]: numOfIntefaces	The capacity of this new function table. 	
	// Return:    A new function table.
	// Remarks:   Creates a new function table which can be filled with several functions.
	// Notes:
	// See: FSExtensionHFTMgrAddHFT
	// See: FSExtensionHFTMgrGetHFT
	//************************************
	static FS_HFT	NewHFT(FS_INT32 numOfIntefaces);

	//************************************
	// Function:  AddHFT
	// Param[in]: hftName		The name of function table, used to identify it.
	// Param[in]: version		Version of this function table.
	// Param[in]: hft			The instance to be added.
	// Return:    If success, return <a>ERR_ADDHFT_NAMEEXIST</a>(1), else(hft not exist) return <a>ERR_ADDHFT_UNKNOWN</a>(0).
	// Remarks:   Adds a new function table.
	// Notes:
	// See: FSExtensionHFTMgrNewHFT
	//************************************
	static FS_INT32		AddHFT(const FS_CHAR* hftName, FS_INT32 version, FS_HFT hft);

	//************************************
	// Function:  GetHFT
	// Param[in]: hftName		The name of function table, used to identify it.
	// Param[in]: version		Version of this function table.
	// Return:    If exists, returns the instance, else returns <a>NULL</a>.
	// Remarks:   Gets the function table.
	// Notes:
	// See: FSExtensionHFTMgrReplaceEntry
	//************************************
	static FS_HFT	GetHFT(const FS_CHAR* hftName, FS_INT32 version);

	//************************************
	// Function:  ReplaceEntry
	// Param[in]: hft			The function table which contains the entry to be replaced.	
	// Param[in]: iSel			The index of entry to be replaced.
	// Param[in]: newEntry		The new entry to replaced.
	// Return:    
	// Remarks:   Replaces the entry by specified selector.
	// Notes:
	// See: FSExtensionHFTMgrGetEntry
	//************************************
	static void		ReplaceEntry(FS_HFT hft, FS_INT32 iSel, void* newEntry);

	//************************************
	// Function:  GetEntry
	// Param[in]: hft		The function table which contains several entries.
	// Param[in]: iSel		The index of entry to be found.
	// Return:    If entry exists, returns the instance, else returns <a>NULL</a>.
	// Remarks:   Gets the entry by specified selector.
	// Notes:
	// See: FSExtensionHFTMgrReplaceEntry
	//************************************
	static void*	GetEntry(FS_HFT hft, FS_INT32 iSel);
};

class CFS_AffineMatrix_V1
{
public:

	//************************************
	// Function:  IsScaled
	// Param[in]: matrix The input matrix.
	// Return:    <a>TRUE</a> for this matrix having scaling (or translating) only, no rotating. Otherwise not.
	// Remarks:   Whether this matrix has scaling (or translating) only. No rotating.
	// Notes: 
	// See: FSAffineMatrixScale
	//************************************
	static FS_BOOL IsScaled(FS_AffineMatrix matrix);


	//************************************
	// Function:  Is90Rotated
	// Param[in]: matrix The input matrix.
	// Return:    <a>TRUE</a> for this matrix having rotating, otherwise not.
	// Remarks:   Whether this matrix has rotating of 90, or -90 degrees.
	// Notes:
	//************************************
	static FS_BOOL Is90Rotated(FS_AffineMatrix matrix);


	//************************************
	// Function:  GetReverse 
	// Param[in]: src The input matrix.
	// Return:    The inverse matrix base on a source matrix.
	// Remarks:   Gets the inverse matrix base on a source matrix.
	// Notes:
	//************************************
	static FS_AffineMatrix GetReverse(FS_AffineMatrix src);


	//************************************
	// Function:   TransformPoint
	// Param[in]:  matrix The input matrix.
	// Param[in]:  x	 The input x-coordinate of the point
	// Param[in]:  y	 The input y-coordinate of the point.
	// Param[out]: outX  It receives the output transformed x-coordinate.
	// Param[out]: outY  It receives the output transformed y-coordinate.
	// Return:     void
	// Remarks:    Transforms a point.
	// Notes:
	// See: FSAffineMatrixTransformRect
	// See: FSAffineMatrixTransformDistance
	//************************************
	static void TransformPoint(FS_AffineMatrix matrix, FS_FLOAT x, FS_FLOAT y, FS_FLOAT* outX, FS_FLOAT* outY);


	//************************************
	// Function:  TransformRect
	// Param[in]: matrix The input matrix.
	// Param[in]: rc	 Rectangle to transform.
	// Return:    The result rectangle.
	// Remarks:   Transforms a rectangle and return a bounding rectangle. </Brief> The result rectangle is always normalized.
	// Notes:
	// See: FSAffineMatrixTransformPoint
	// See: FSAffineMatrixTransformDistance
	//************************************
	static FS_FloatRect TransformRect(FS_AffineMatrix matrix, FS_FloatRect rc);


	//************************************
	// Function:  Concat
	// Param[in]: matrix    The input matrix.
	// Param[in]: src		The input matrix.
	// Return:    The result matrix.
	// Remarks:   Concatenates with another matrix.
	// Notes:
	// See: FSAffineMatrixConcatInverse
	//************************************
	static FS_AffineMatrix Concat(FS_AffineMatrix matrix, FS_AffineMatrix src);


	//************************************
	// Function:  Translate
	// Param[in]: matrix The input matrix.
	// Param[in]: x		 The x-direction delta value.
	// Param[in]: y		 The y-direction delta value.
	// Return:    The result matrix.
	// Remarks:   Translates the matrix.
	// Notes:
	// See: FSAffineMatrixTranslateI
	//************************************
	static FS_AffineMatrix	Translate(FS_AffineMatrix matrix, FS_FLOAT x, FS_FLOAT y);


	//************************************
	// Function:  TranslateI
	// Param[in]: matrix The input matrix.
	// Param[in]: x		 The x-direction delta integer value.
	// Param[in]: y		 The y-direction delta integer value.
	// Return:    The result matrix.
	// Remarks:   Translates the matrix using integer value.
	// Notes:
	// See: FSAffineMatrixTranslate
	//************************************
	static FS_AffineMatrix	TranslateI(FS_AffineMatrix matrix, FS_INT32 x, FS_INT32 y);


	//************************************
	// Function:  Scale
	// Param[in]: matrix    The input matrix.
	// Param[in]: xScale	The x-direction scale coefficient.
	// Param[in]: yScale	The y-direction scale coefficient.
	// Return:    The result matrix.
	// Remarks:   Scales the matrix.
	// Notes:
	// See: FSAffineMatrixIsScale
	//************************************
	static FS_AffineMatrix	Scale(FS_AffineMatrix matrix, FS_FLOAT xScale, FS_FLOAT yScale);


	//************************************
	// Function:  ConcatInverse
	// Param[in]: matrix The input matrix.
	// Param[in]: src	 The input matrix, to be inversed.
	// Return:    The result of a matrix concatenating another matrix inversed;
	// Remarks:   Concatenates the inverse of another matrix.
	// Notes:
	// See: FSAffineMatrixConcat
	//************************************
	static FS_AffineMatrix	ConcatInverse(FS_AffineMatrix matrix, FS_AffineMatrix src);


	//************************************
	// Function:  MatchRect
	// Param[in]: dest	 The dest rectangle
	// Param[in]: src	 The source rectangle.
	// Return:    The matrix that transforms a source rectangle to dest rectangle.
	// Remarks:   Gets a matrix that transforms a source rectangle to dest rectangle.
	// Notes:
	// See: FSAffineMatrix
	//************************************
	static FS_AffineMatrix	MatchRect(FS_FloatRect dest, FS_FloatRect src);


	//************************************
	// Function:  GetUnitRect
	// Param[in]: matrix The input matrix.
	// Return:    The unit rectangle.
	// Remarks:   Gets a bounding rectangle of the parallelogram composing two unit vectors.
	// Notes:  
	// See: FSAffineMatrixGetUnitArea
	//************************************
	static FS_FloatRect GetUnitRect(FS_AffineMatrix matrix);


	//************************************
	// Function:  GetUnitArea
	// Param[in]: matrix The input matrix.
	// Return:    The unit area
	// Remarks:   Gets area of the parallelogram composing two unit vectors.
	// Notes:
	// See: FSAffineMatrixGetUnitRect
	//************************************
	static FS_FLOAT GetUnitArea(FS_AffineMatrix matrix);


	//************************************
	// Function:  GetXUnit
	// Param[in]: matrix The input matrix.
	// Return:    The x-direction unit size.
	// Remarks:   Gets the x-direction unit size.
	// Notes:
	// See: FSAffineMatrixGetYUnit
	//************************************
	static FS_FLOAT GetXUnit(FS_AffineMatrix matrix);


	//************************************
	// Function:  GetYUnit	
	// Param[in]: matrix The input matrix.
	// Return:    The y-direction unit size.
	// Remarks:   Gets the y-direction unit size.
	// Notes:
	// See: FSAffineMatrixGetXUnit
	//************************************
	static FS_FLOAT GetYUnit(FS_AffineMatrix matrix);


	//************************************
	// Function:  TransformDistance
	// Param[in]: matrix        The input matrix.
	// Param[in]: distance		The input distance.
	// Return:    The transformed distance.
	// Remarks:   Transforms a distance.
	// Notes:
	// See: FSAffineMatrixTransformRect
	// See: FSAffineMatrixTransformPoint
	//************************************
	static FS_FLOAT TransformDistance(FS_AffineMatrix matrix, FS_FLOAT distance);

	//************************************
	// Function:  Rotate
	// Param[in]: matrix        The input matrix.
	// Param[in]: fRadian		Rotation angle in radian.
	// Param[in]: bPrepended	If it's TRUE, a rotation matrix is multiplied at left side, or at right side. Sets it FALSE as default.
	// Return:    The result matrix.
	// Remarks:   Rotates the matrix.
	// Notes:
	//************************************
	static FS_AffineMatrix Rotate(FS_AffineMatrix matrix, FS_FLOAT fRadian, FS_BOOL bPrepended);
};


class CFS_DIBitmap_V1
{
	
public:

	//************************************
	// Function:  New       
	// Return:	  A <a>FS_DIBitmap</a> object.
	// Remarks:   Creates a bitmap. Optionally an external buffer provided by caller can be used. Using
	// <a>FSDIBitmapCreate</a>() to initialize after a bitmap object is created.
	// Notes:     The buffer should be kept by caller during the existence of the bitmap.
	// See: FSDIBitmapClear
	// See: FSDIBitmapCreate
	// See: FSDIBitmapDestroy
	//************************************
	static FS_DIBitmap New(void);

	//************************************
	// Function:  Create  
	// Param[in]: bitmap    The input bitmap.
	// Param[in]: width		The width of the bitmap.
	// Param[in]: height	The height of the bitmap.
	// Param[in]: format	The format of the bitmap.
	// Param[in]: pBuffer	The data buffer of the bitmap.
	// Param[in]: pitch		Specified row pitch in bytes.
	// Return:    void
	// Remarks:   Actually create the <Italic>DIB</Italic>. Optionally the <Italic>DIB</Italic> can use external buffer provided by caller.
	// Notes:     The buffer should be kept by caller during the existence of the bitmap.
	// See: FSDIBitmapNew
	// See: FSDIBitmapClear
	// See: FSDIBitmapDestroy
	//************************************
	static void	Create(FS_DIBitmap bitmap, FS_INT32 width, FS_INT32 height, FS_DIB_Format format, FS_LPBYTE pBuffer, FS_INT32 pitch);

	//************************************
	// Function:  Destroy
	// Param[in]: bitmap The input bitmap.
	// Return:    void
	// Remarks:   Destroys the bitmap.
	// Notes:
	// See: FSDIBitmapCreate
	// See: FSDIBitmapNew
	//************************************
	static void Destroy(FS_DIBitmap bitmap);
	
	//************************************
	// Function:  GetWidth
	// Param[in]: bitmap The input bitmap.
	// Return:    The width of the bitmap.
	// Remarks:   Gets the width of the bitmap.
	// Notes:
	// See: FSDIBitmapGetHeight
	//************************************
	static FS_INT32 GetWidth(FS_DIBitmap bitmap);


	//************************************
	// Function:  GetHeight
	// Param[in]: bitmap The input bitmap.
	// Return:    The height of the bitmap.
	// Remarks:   Gets the height of the bitmap.
	// Notes:
	// See: FSDIBitmapGetWidth
	//************************************
	static FS_INT32 GetHeight(FS_DIBitmap bitmap);


	//************************************
	// Function:  GetFormat
	// Param[in]: bitmap The input bitmap.
	// Return:    The format of the bitmap.
	// Remarks:   Gets the format of the bitmap.
	// Notes:
	//************************************
	static FS_DIB_Format GetFormat(FS_DIBitmap bitmap);


	//************************************
	// Function:  GetPitch	
	// Param[in]: bitmap The input bitmap.
	// Return:    The row pitch of the bitmap.
	// Remarks:   Gets the specified row pitch of the bitmap.
	// Notes:
	//************************************
	static FS_DWORD GetPitch(FS_DIBitmap bitmap);


	//************************************
	// Function:  GetPalette	
	// Param[in]: bitmap The input bitmap.
	// Return:    The palette of the bitmap.
	// Remarks:   Gets the palette of the bitmap.
	// Notes:
	//************************************
	static FS_ARGB* GetPalette(FS_DIBitmap bitmap);


	//************************************
	// Function:  GetBuffer
	// Param[in]: bitmap The input bitmap.
	// Return:    A buffer for the whole <Italic>DIB</Italic>.
	// Remarks:   Gets a buffer for whole <Italic>DIB</Italic>. Only in-memory DIB can supply such a buffer.
	// Notes:     
	//************************************
	static FS_LPBYTE GetBuffer(FS_DIBitmap bitmap);


	//************************************
	// Function:  GetScanline
	// Param[in]: bitmap    The input bitmap.
	// Param[in]: line		The zero-based line number. [0, height).
	// Return:    A pointer to the scan line.
	// Remarks:   Fetches a single scan line.
	// Notes:
	// See: FSDIBitmapDownSampleScanline
	//************************************
	static FS_LPBYTE GetScanline(FS_DIBitmap bitmap, FS_INT32 line);


	//************************************
	// Function:  DownSampleScanline	
	// Param[in]: bitmap    The input bitmap.
	// Param[in]: line		The zero-based line number.
	// Param[in,out]: destScan  The destination scanline buffer to receive down-sample result.
	// Param[in]: destBpp   The destination bits per pixel.
	// Param[in]: destWidth	The destination width of pixels in the scanline.
	// Param[in]: bFlipX    Whether to flip the bitmap in x-direction. 
	// Param[in]: clipLeft	Clip start of the destination scanline.
	// Param[in]: clipWidth Clip width of the destination scanline.
	// Return:    void
	// Remarks:   Downs sample a scanline, for quick stretching.
	//            </Brief> The down-sampled result would be either 8bpp (for mask and grayscale), 24bpp or 32bpp.
	// Notes:
	// See: FSDIBitmapGetScanline
	//************************************
	static void	DownSampleScanline(
		FS_DIBitmap bitmap, 
		FS_INT32 line,
		FS_LPBYTE destScan,
		FS_INT32 destBpp,
		FS_INT32 destWidth, 
		FS_BOOL bFlipX,
		FS_INT32 clipLeft, 
		FS_INT32 clipWidth
		);

	//************************************
	// Function:  TakeOver	
	// Param[in]: bitmap        The input bitmap.
	// Param[in]: srcBitmap		The input source bitmap.
	// Return:    void
	// Remarks:   Takeovers a bitmap.
	// Notes:     After taking-over, the source bitmap will contain an empty bitmap, and this bitmap will contain all data of the source bitmap.
	//************************************
	static void TakeOver(FS_DIBitmap bitmap, FS_DIBitmap srcBitmap);

	//************************************
	// Function:  ConvertFormat 
	// Param[in]: bitmap    The input bitmap.
	// Param[in]: format	The destination bitmap format.
	// Return:    void
	// Remarks:   Converts a bitmap. All informations in the old bitmap are retained.
	// Notes:     Supported conversion:
	//            <ul>
	//              - <li>1bppMask => 8bppMask;</li>
	//              - <li>1bppRgb => Rgb / Rgb32 / Argb;</li>
	//              - <li>8bppRgb => Rgb / Rgb32 / Argb;</li>
	//              - <li>Rgb / Rgb32 => 8bppRgb;</li>
	//              - <li>Rgb => Rgb32 / Argb;</li>
	//              - <li>Rgb32 => Rgb / Argb</li>
	//             </ul>
	//************************************
	static void ConvertFormat(FS_DIBitmap bitmap, FS_DIB_Format format);

	//************************************
	// Function:  Clear
	// Param[in]: bitmap    The input bitmap.
	// Param[in]: argb		The specified color to fill.
	// Return:    void
	// Remarks:   Fills the whole bitmap with specified color. For alpha mask bitmaps, only the alpha value is taken.
	// Notes:
	//************************************
	static void Clear(FS_DIBitmap bitmap, FS_ARGB argb);

	//************************************
	// Function:  GetPixel
	// Param[in]: bitmap The input bitmap.
	// Param[in]: x		 The x-coordinate in bitmap.
	// Param[in]: y		 The y-coordinate in bitmap.
	// Return:    The pixel ARGB value.
	// Remarks:   Gets pixel ARGB value.
	// Notes:     For alpha mask bitmaps, only the alpha value of returned value is valid.
	// See: FSDIBitmapSetPixel
	//************************************
	static FS_DWORD GetPixel(FS_DIBitmap bitmap, FS_INT32 x, FS_INT32 y);

	//************************************
	// Function:  SetPixel	
	// Param[in]: bitmap The input bitmap.
	// Param[in]: x		 The x-coordinate in bitmap.
	// Param[in]: y		 The y-coordinate in bitmap.
	// Param[in]: argb	 The input pixel ARGB value.
	// Return:    void
	// Remarks:   Sets pixel ARGB value.
	// Notes:     For alpha mask bitmaps, only the alpha value is taken.
	// See: FSDIBitmapGetPixel
	//************************************
	static void SetPixel(FS_DIBitmap bitmap, FS_INT32 x, FS_INT32 y, FS_ARGB argb);

	//************************************
	// Function:  LoadChannel
	// Param[in]: bitmap      The input bitmap.
	// Param[in]: destChannel The destination channel.
	// Param[in]: srcBitmap   The DIB source.
	// Param[in]: srcChannel  The source channel.
	// Return:    void
	// Remarks:   Loads a full channel from a source bitmap to this bitmap.
	//            </Brief>The source bitmap can be any format, but must have specified source channel.
	// Notes:     If the destination channel is a color channel (R/G/B), then the this bitmap must be a colored image.
	//            If the destination channel is the alpha channel, and this bitmap doesn't have an alpha channel.
	//            it will be expanded with the alpha mask loaded. If this bitmap has already an alpha channel,
	//            then the previous alpha data will be replaced.
	//            If the source bitmap doesn't have same size as this bitmap, the source bitmap will stretched
	//            to match the destination size before channel transferring.
	//
	// See: FSDIBitmapLoadChannel2
	//************************************
	static void LoadChannel(FS_DIBitmap bitmap, FS_DIB_Channel destChannel, const FS_DIBitmap srcBitmap, FS_DIB_Channel srcChannel);

	//************************************
	// Function:  LoadChannel2
	// Param[in]: bitmap        The input bitmap.
	// Param[in]: destChannel	The destination channel.
	// Param[in]: value			The value to fill channel.
	// Return:    void
	// Remarks:   Sets a full channel to specified value (0-255).
	// Notes:     This bitmap must be in Rgb, Rgb32, or Argb format.
	// See: FSDIBitmapLoadChannel
	//************************************
	static void	LoadChannel2(FS_DIBitmap bitmap, FS_DIB_Channel destChannel, FS_INT32 value);

	//************************************
	// Function:  MultiplyAlpha
	// Param[in]: bitmap	The input bitmap.
	// Param[in]: alpha		The alpha value to multiply with.
	// Return:    void
	// Remarks:   Multiplies alpha data with addition alpha (0-255).
	// Notes:     Applicable to all formats:
	//            If this is an alpha mask (1bppMask or 8bppMask), the result will be a 8bppMask with mask data modified;
	//            If this is an image without alpha channel, the bitmap will be expanded to include an alpha channel with new alpha data;
	//            If this is an image with alpha channel, then the alpha value will be multiplied into existing alpha data.
	// See: FSDIBitmapMultiplyAlpha2
	//************************************
	static void	MultiplyAlpha(FS_DIBitmap bitmap, FS_INT32 alpha);

	//************************************
	// Function:  MultiplyAlpha2	
	// Param[in]: bitmap		The input bitmap.
	// Param[in]: alphaMask		The alpha mask to multiply with.
	// Return:    void
	// Remarks:   Multiplies existing alpha data with another alpha mask.
	// Notes:     Applicable to all destination formats:
	//            If this is an alpha mask (1bppMask or 8bppMask), the result will be a 8bppMask with mask data multiplied;
	//            If this is an image without alpha channel, the bitmap will be expanded to include an alpha channel with new alpha data;
	//            If this is an image with alpha channel, then the alpha value will be multiplied into existing alpha data.
	//            If the source mask doesn't have same size as this bitmap, the source mask will stretched
	//            to match the destination size before multiplying.
	// See: FSDIBitmapMultiplyAlpha
	//************************************
	static void MultiplyAlpha2(FS_DIBitmap bitmap, const FS_DIBitmap alphaMask);

	//************************************
	// Function:  TransferBitmap
	// Param[in]: bitmap		The input bitmap.
	// Param[in]: destLeft		The x-coordinate in destination bitmap.
	// Param[in]: destTop		The y-coordinate in destination bitmap.
	// Param[in]: width			The area width to transfer.
	// Param[in]: height		The area height to transfer.
	// Param[in]: srcBitmap		The DIB source.
	// Param[in]: srcLeft		The x-coordinate in source bitmap.
	// Param[in]: srcTop		The y-coordinate in source bitmap.
	// Return:    void
	// Remarks:   Transforms pixels from another bitmap into specified position.
	// Notes:     The destination and source bitmaps can have different format, but need to be compatible.
	//            "width" and "height" parameters can not be negative. 
	//            Positions will be automatically clipped if out of source or destination bitmap region.
	//            The destination region will be replaced by data from the source bitmap.
	// See: FSDIBitmap
	//************************************
	static void TransferBitmap(
		FS_DIBitmap bitmap,
		FS_INT32 destLeft,
		FS_INT32 destTop,
		FS_INT32 width, 
		FS_INT32 height,
		const FS_DIBitmap srcBitmap,
		FS_INT32 srcLeft, 
		FS_INT32 srcTop
		);



	//************************************
	// Function:  TransferMask
	// Param[in]: bitmap		The input bitmap.
	// Param[in]: destLeft		The x-coordinate in destination bitmap.
	// Param[in]: destTop		The y-coordinate in destination bitmap.
	// Param[in]: width			The area width to transfer.
	// Param[in]: height		The area height to transfer.
	// Param[in]: mask			The mask source.
	// Param[in]: argb			The source color.
	// Param[in]: srcLeft		The x-coordinate in source bitmap.
	// Param[in]: srcTop		The y-coordinate in source bitmap.
	// Return:    void
	// Remarks:   Transfers (portion of) an alpha mask (with source color) to this bitmap.
	// Notes:     Applicable to Argb formats only. The changed portion will be replaced (not blended).
	//            The mask parameter must point to an alpha mask bitmap (1bppMask or 8bppMask).
	// See: FSDIBitmapCompositeBitmap
	// See: FSDIBitmapCompositeMask
	// See: FSDIBitmapCompositeRect
	//************************************
	static void TransferMask(
		FS_DIBitmap bitmap,
		FS_INT32 destLeft,
		FS_INT32 destTop,
		FS_INT32 width,
		FS_INT32 height,
		const FS_DIBitmap mask,
		FS_ARGB argb,
		FS_INT32 srcLeft,
		FS_INT32 srcTop
		);


	//************************************
	// Function:  CompositeBitmap
	// Param[in]: bitmap		The input bitmap.
	// Param[in]: destLeft		The x-coordinate in destination bitmap.
	// Param[in]: destTop		The y-coordinate in destination bitmap.
	// Param[in]: width			The area width to composite.
	// Param[in]: height		The area height to composite.
	// Param[in]: srcBitmap		The source bitmap.
	// Param[in]: srcLeft		The x-coordinate in source bitmap.
	// Param[in]: srcTop		The y-coordinate in source bitmap.
	// Param[in]: blend_type	The blend type. Decleared in <a>FSDIBlendTypes</a> group.
	// Return:    void
	// Remarks:   Composites and blend source bitmap into this <param>bitmap</param>.<br></brief>
	// This bitmap can be Rgb/Rgb32/Argb bitmap, and source bitmap can be any kind of bitmap (except masks).<br>
	// This bitmap can also be an 8bppMask, in this case only alpha channel from the source bitmap is composited.
	// Notes: 
	// See: FSDIBitmapCompositeMask
	// See: FSDIBitmapCompositeRect
	//************************************

	static void CompositeBitmap(
		FS_DIBitmap bitmap,
		FS_INT32 destLeft,
		FS_INT32 destTop,
		FS_INT32 width,
		FS_INT32 height,
		const FS_DIBitmap srcBitmap,
		FS_INT32 src_left, 
		FS_INT32 src_top,
		FS_INT32 blend_type
		);

	//************************************
	// Function:  CompositeRect
	// Param[in]: bitmap		The input bitmap.
	// Param[in]: destLeft		The x-coordinate of the left-top corner.
	// Param[in]: destTop		The y-coordinate of the left-top corner.
	// Param[in]: width			The area width.
	// Param[in]: height		The area height.
	// Param[in]: argb			The fixed color.
    // Return:    void
	// Remarks:   Composites a fixed color into a rectangle area.
	// Notes:     Applicable to 8bppMask/Rgb/Rgb32/Argb formats only.
	// See: FSDIBitmapCompositeBitmap
	// See: FSDIBitmapTransferMask
	// See: FSDIBitmapCompositeMask
	//************************************
	static void	CompositeRect(
		FS_DIBitmap bitmap, 
		FS_INT32 destLeft,
		FS_INT32 destTop, 
		FS_INT32 width, 
		FS_INT32 height, 
		FS_ARGB argb
		);


	//************************************
	// Function:  GammaAdjust
	// Param[in]: bitmap	The input bitmap.
	// Param[in]: bInvert	Whether reverse gamma adjustment or not.
	// Return:    void
	// Remarks:   Gamma adjustment.
	// Notes:
	//************************************
	static void	GammaAdjust(FS_DIBitmap bitmap, FS_BOOL bInvert);

	//************************************
	// Function:  ConvertColorScale
	// Param[in]: bitmap	The input bitmap.
	// Param[in]: forecolor The input forecolor.
	// Param[in]: backcolor The input backcolor.
	// Return:    void
	// Remarks:   Converts current bitmap to a color scale bitmap. The DIB format won't be changed.
	// Notes:     Color scale means all colors are a scale from forecolor to backcolor.
	//            If forecolor is black and backcolor is white, that's gray scale
	//            Doesn't work with masks.
	//************************************
	static void	ConvertColorScale(FS_DIBitmap bitmap, FS_COLORREF forecolor, FS_COLORREF backcolor);

	//************************************
	// Function:  DitherFS
	// Param[in]: bitmap	The input bitmap.
	// Param[in]: pPalette	The input palette color.
	// Param[in]: palSize	The input palette size.
	// Param[in]: pRect		The input rect for ditherning.
	// Return:    void
	// Remarks:   Floyd-Steinberg dithering for (portion of) the bitmap, using a palette.
	// Notes:     Currently only 8-bit gray scale bitmap is supported!
	//************************************
	static void	DitherFS(FS_DIBitmap bitmap, FS_ARGB* pPalette, FS_INT32 palSize, FS_Rect* pRect);

	//************************************
	// Function:  GetBPP	
	// Param[in]: bitmap The input bitmap.
	// Return:    The number of bits per pixel.
	// Remarks:   Gets the number of bits per pixel.
	// Notes:
	//************************************
	static FS_INT32 GetBPP(FS_DIBitmap bitmap);

	//************************************
	// Function:  IsAlphaMask	
	// Param[in]: bitmap The input bitmap.
	// Return:    <a>TRUE</a> for bitmap being an alpha mask.
	// Remarks:   Checks whether the bitmap is an alpha mask (either 1bpp bitmask or 8bpp gray scale).
	// Notes:
	//************************************
	static FS_BOOL IsAlphaMask(FS_DIBitmap bitmap);

	//************************************
	// Function:  HasAlpha
	// Param[in]: bitmap The input bitmap.
	// Return:    Non-zero means it has a alpha channel, otherwise has not.
	// Remarks:   Checks if it's a bitmap with alpha channel.
	// Notes:     Alpha masks return <a>FALSE</a>.
	//************************************
	static FS_BOOL HasAlpha(FS_DIBitmap bitmap);

	//************************************
	// Function:  IsOpaqueImage	
	// Param[in]: bitmap The input bitmap.
	// Return:    Non-zero means it is a solid (opaque) image, otherwise is not.
	// Remarks:   Checks if it's a solid (opaque) image.
	// Notes:
	//************************************
	static FS_BOOL IsOpaqueImage(FS_DIBitmap bitmap);

	//************************************
	// Function:  GetPaletteSize	
	// Param[in]: bitmap The input bitmap.
	// Return:    The number of palette entries.
	// Remarks:   Gets the number of palette entries.
	// Notes:
	//************************************
	static FS_INT32	GetPaletteSize(FS_DIBitmap bitmap);

	//************************************
	// Function:  GetPaletteArgb
	// Param[in]: bitmap	The input bitmap.
	// Param[in]: index		Zero-based palette entry index of the palette.
	// Return:    A palette entry value.
	// Remarks:   Gets palette entry with specified palette entry index.
	// Notes:     
	// See: FSDIBitmapSetPaletteArgb
	//************************************
	static FS_ARGB GetPaletteArgb(FS_DIBitmap bitmap,FS_INT32 index);

	//************************************
	// Function:  SetPaletteArgb	
	// Param[in]: bitmap	The input bitmap.
	// Param[in]: index		Zero-based palette entry index of the palette.
	// Param[in]: argb		The new value the entry.
	// Return:    void
	// Remarks:   Changes a specified palette entry.
	// Notes:     
	// See: FSDIBitmapGetPaletteArgb
	//************************************
	static void SetPaletteArgb(FS_DIBitmap bitmap, FS_INT32 index, FS_ARGB argb);

	//************************************
	// Function:  Clone	
	// Param[in]: bitmap	The input bitmap.
	// Param[in]: pClip		The clipping region of source bitmap
	// Return:    A cloned bitmap.
	// Remarks:   Clones a bitmap. The returned bitmap must be destroyed by <a>FSDIBitmapDestroy</a>().
	// Notes:     Optionally a clipping region in bitmap coordinates can be specified to limit the size of result bitmap.
	// See: FSDIBitmapCloneConvert
	//************************************
	static FS_DIBitmap Clone(FS_DIBitmap bitmap, FS_Rect* pClip);

	//************************************
	// Function:  CloneConvert	
	// Param[in]: bitmap	The input bitmap.
	// Param[in]: format	The destination bitmap format.
	// Param[in]: pClip		The clipping region of source bitmap.
	// Return:    The cloned bitmap.
	// Remarks:   Converts a bitmap, but clone first. The returned bitmap must be destroyed by <a>FSDIBitmapDestroy</a>().
	// Notes:     Optionally a clipping region in bitmap coordinates can be specified to limit the size of result bitmap.
	// See: FSDIBitmapClone
	//************************************
	static FS_DIBitmap CloneConvert(FS_DIBitmap bitmap, FS_DIB_Format format, FS_Rect* pClip);

	//************************************
	// Function:  GetAlphaMask 
	// Param[in]: bitmap	The input bitmap.
	// Param[in]: pClip		The clipping region of source bitmap.
	// Return:    An alpha mask.
	// Remarks:   Gets alpha mask from a bitmap. Returns a 8bpp alpha mask. The returned value must be destroyed by <a>FSDIBitmapDestroy</a>().
	// Notes:     Applicable to Argb format only.
	//            Optionally a clipping region in bitmap coordinates can be specified to limit the size of result mask.
	//************************************
	static FS_DIBitmap GetAlphaMask(FS_DIBitmap bitmap, FS_Rect* pClip);
	
	//************************************
	// Function:  StretchTo	
	// Param[in]: bitmap		The input bitmap.
	// Param[in]: destWidth		The width of the destination bitmap.
	// Param[in]: destHeight	The height of the destination bitmap.
	// Param[in]: flags			Stretching flags. It can use <a>FS_DIB_DOWNSAMPLE</a> and <a>FS_DIB_INTERPOL</a> flags
	// Param[in]: pClip			The clipping region of destination bitmap.
	// Return:    A new bitmap with different size.
	// Remarks:   Stretches this bitmap into a new bitmap with different size. The returned bitmap must be destroyed by <a>FSDIBitmapDestroy</a>().
	// Notes:     If dest width or dest height is negative, the bitmap will be flipped.
	//            If a 1bpp bitmap is stretched, it will become either a RGB bitmap, if it's a colored bitmap;
	//            or a 8bpp gray scale mask if it's a bit mask.
	//            Stretching can be done in down-sample mode, which doesn't do interpolation so significantly faster
	//            especially when stretching big images into small ones.
	//            Optionally a clipping region in result bitmap coordinates can be specified to limit the size of result bitmap.

	//************************************
	static FS_DIBitmap StretchTo(FS_DIBitmap bitmap, FS_INT32 destWidth, FS_INT32 destHeight, FS_DWORD flags, FS_Rect* pClip);


	//************************************
	// Function:   TransformTo	
	// Param[in]:  bitmap		The input bitmap.
	// Param[in]:  pMatrix		The transformation matrix.
	// Param[out]: outLeft		Receives x-coordinate of the left-top corner of the result bitmap in destination coords.
	// Param[out]: outTop		Receives y-coordinate of the left-top corner of the result bitmap in destination coords.
	// Param[in]:  flags		Stretching flags. It can use <a>FS_DIB_DOWNSAMPLE</a> and <a>FS_DIB_INTERPOL</a> flags.
	// Param[in]:  pClip		The clipping region of destination bitmap.
	// Return:     A new transformed bitmap.
	// Remarks:    Transforms this bitmap. A new transformed bitmap is returned. The returned bitmap must be destroyed by <a>FSDIBitmapDestroy</a>().
	// Notes:      This bitmap can be colored bitmap, or an alpha mask. In case of colored bitmap,
	//             certain transformation (rotating or skewing) will cause the return bitmap as ARGB, no matter
	//             what the source bitmap format is.
	//             If a 1bpp bitmap is transformed, it will become either a RGB bitmap, if it's a colored bitmap;
	//             or a 8bpp gray scale mask if it's a bit mask.
	//             The dimension of returned bitmap always match the dimension of the matrix.
	//             Transformation can be done in down-sample mode, which doesn't do interpolation so significantly faster
	//             especially when transforming big images into small ones.
	//             Optionally a clipping region in result bitmap coordinates can be specified to limit the size of result bitmap.
	//             The position of left-top corner (in destination coordinates) of the result bitmap is also returned.
	//************************************
	static FS_DIBitmap TransformTo(
		FS_DIBitmap bitmap, 
		FS_AffineMatrix* pMatrix,
		FS_INT32* outLeft,
		FS_INT32* outTop, 
		FS_DWORD flags, 
		FS_Rect* pClip
		);

	//************************************
	// Function:  SwapXY
	// Param[in]: bitmap	The input bitmap.
	// Param[in]: bXFlip	Whether to flip the bitmap in x-direction.
	// Param[in]: bYFlip	Whether to flip the bitmap in y-direction.
	// Param[in]: pClip		The clipping region of destination bitmap.
	// Return:    A new bitmap.
	// Remarks:   Swaps X,Y coordinations of the bitmap. The image can also be flipped at the same time. The returned bitmap must be destroyed by <a>FSDIBitmapDestroy</a>().
	// Notes:     Optionally a clipping region (in destination bitmap coordinates) can be specified to limit the size of result.
	//            This function can be used to rotate the bitmap 90 or 270 degree.
	//            Suppose the original image has the following 4 pixels:
	//			  
	//            +---+---+
	//			  | 1 | 2 |
	//			  +---+---+
	//			  | 3 | 4 |
	//			  +---+---+
	//			  Then, depends on value of <param>bXFlip</param> and <param>bYFlip</param>, the result would look like this:
	//				 
	//			  if bXFlip = FALSE, bYFlip = FALSE:
	//			  +---+---+
	//			  | 1 | 3 |
	//			  +---+---+
	//			  | 2 | 4 |
	//			  +---+---+
	//					
	//			  if bXFlip = TRUE, bYFlip = FALSE:
	//			  +---+---+
	//			  | 3 | 1 |
	//			  +---+---+
	//			  | 4 | 2 |
	//			  +---+---+
	//					
	//			  if bxflip = false, byflip = true:
	//			  +---+---+
	//			  | 2 | 4 |
	//			  +---+---+
	//			  | 1 | 3 |
	//			  +---+---+
	//					
	//			  if bxflip = true, byflip = true:
	//			  +---+---+
	//			  | 4 | 2 |
	//			  +---+---+
	//			  | 3 | 1 |
	//			  +---+---+  
	//************************************
	static FS_DIBitmap SwapXY(FS_DIBitmap bitmap, FS_BOOL bXFlip, FS_BOOL bYFlip, FS_Rect* pClip);

	//************************************
	// Function:  FlipImage	
	// Param[in]: bitmap	The input bitmap.
	// Param[in]: bXFlip	Whether to flip the bitmap in x-direction.
	// Param[in]: bYFlip	Whether to flip the bitmap in y-direction.
	// Return:    A flipped bitmap.
	// Remarks:   Flips image. The returned bitmap must be destroyed by <a>FSDIBitmapDestroy</a>().
	// Notes:
	//************************************
	static FS_DIBitmap FlipImage(FS_DIBitmap bitmap, FS_BOOL bXFlip, FS_BOOL bYFlip);
	
	//************************************
	// Function:  LoadFromPNGIcon	
	// Param[in]: pwsFilePath	The input PNG file path.
	// Return:    The bitmap loaded.
	// Remarks:   Loads a bitmap from a PNG file.
	// Notes:
	//************************************
	static FS_DIBitmap LoadFromPNGIcon(FS_LPCWSTR pwsFilePath);

	//************************************
	// Function:  LoadFromPNGIcon2	
	// Param[in]: handler	The file access handler. The core API will take over the handler and release it. The plug-in do not need to release it.
	// Return:    The bitmap loaded.
	// Remarks:   Loads a bitmap from a buffer.
	// Notes:
	//************************************
	static FS_DIBitmap LoadFromPNGIcon2(FS_FileReadHandler handler);

	//************************************
	// Function:  Copy	
	// Param[in]: bitmap	The input bitmap.
	// Param[in]: src		The DIB source.
	// Return:    TRUE for success, FALSE for failure.
	// Remarks:   Copy from a DIB source, including bitmap info and all pixel data.
	// This DIBitmap must be newly constructed.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_BOOL Copy(FS_DIBitmap bitmap, FS_DIBitmap src);

	//************************************
	// Function:  LoadInfo	
	// Param[in]: handler	The file access handler.
	// Param[in]: imageType The image type.
	// Return:    The bitmap attribute loaded.
	// Remarks:   Loads a bitmap attribute from a buffer.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSDIBitmapDestroyDIBAttribute
	//************************************
	static FS_DIBAttribute LoadInfo(FS_FileReadHandler handler, FS_DIB_IMAGE_TYPE imageType);

	//************************************
	// Function:  DestroyDIBAttribute	
	// Param[in]: attr	The input bitmap attribute object to be destroyed.
	// Return:    void
	// Remarks: Destroys the input bitmap attribute object. Otherwise the plug-in will cause the memory leak problem.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSDIBitmapLoadInfo
	//************************************
	static void DestroyDIBAttribute(FS_DIBAttribute attr);

	//************************************
	// Function:  GetXDPI	
	// Param[in]: attr	The input bitmap attribute object.
	// Return: The horizontal resolution, -1 means the bitmap doesn't contain this information, 0 or 1 means the user should use default DPI value. 
	// Remarks: Gets the horizontal resolution attribute of the bitmap.
	// Notes: JBIG2 cannot get DPI information now.
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSDIBitmapLoadInfo
	//************************************
	static FS_INT32 GetXDPI(FS_DIBAttribute attr);

	//************************************
	// Function:  GetYDPI	
	// Param[in]: attr	The input bitmap attribute object.
	// Return: The vertical resolution, -1 means the bitmap doesn't contain this information, 0 or 1 means the user should use default DPI value.
	// Remarks: Gets the Vertical resolution attribute of the bitmap.
	// Notes: JBIG2 cannot get DPI information now.
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSDIBitmapLoadInfo
	//************************************
	static FS_INT32 GetYDPI(FS_DIBAttribute attr);

	//************************************
	// Function:  GetDPIUnit	
	// Param[in]: attr	The input bitmap attribute object.
	// Return: The resolution unit.
	// Remarks: Gets the resolution unit, described by <a>FS_DIB_RESUNIT</a>.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSDIBitmapLoadInfo
	//************************************
	static FS_WORD GetDPIUnit(FS_DIBAttribute attr);

	//************************************
	// Function:  GetExifInfo	
	// Param[in]: attr	The input bitmap attribute object.
	// Param[in]: tag	The input tag of the exchangeable image file information of camera in JPEG file, described by <a>FSDIBEXIFTAG</a>.
	// Param[out]: val	The output value of the exchangeable image file information.
	// Return: <a>TRUE</a> means success, otherwise failure.
	// Remarks: Gets the exchangeable image file information of camera in JPEG file.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSDIBitmapLoadInfo
	//************************************
	static FS_BOOL GetExifInfo(FS_DIBAttribute attr, FS_WORD tag, FS_LPVOID val);

	//************************************
	// Function:  LoadFromPNGIcon3	
	// Param[in]: hInstance	The input plug-in instance handle.
	// Param[in]: lpwsName	The input name of PNG resource. The <Italic>MAKEINTRESOURCE</Italic> macro can be used to create this value.
	// Return:    The bitmap loaded.
	// Remarks:   Loads a bitmap from a instance handle.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static FS_DIBitmap LoadFromPNGIcon3(HINSTANCE hInstance, FS_LPCWSTR lpwsName);

	//************************************
	// Function:  LoadFromImage	
	// Param[in]: handler	The file access handler.
	// Return:    The bitmap loaded.
	// Remarks:   Loads a bitmap from a buffer.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static FS_DIBitmap LoadFromImage(FS_FileReadHandler handler);

	static FS_DIBitmap GetBitmapAlphaMask(FS_DIBitmap bitmap);
};

class CFS_MapPtrToPtr_V1
{
	
public:


	//************************************
	// Function:  New	
	// Param[in]: nBlockSize	The internal block
	// Return:    An empty ptr-to-ptr map.
	// Remarks:   Creates an empty ptr-to-ptr map.
	// Notes:
	//************************************
	static FS_MapPtrToPtr New(FS_INT32 nBlockSize);


	//************************************
	// Function:  Destroy
	// Param[in]: map The input ptr-to-ptr map.
	// Return:    void
	// Remarks:   Destroys a ptr-to-ptr map.
	// Notes:
	//************************************
	static void	Destroy(FS_MapPtrToPtr map);
	
	//************************************
	// Function:  GetCount	
	// Param[in]: map The input ptr-to-ptr map.
	// Return:   The number of elements in the map.
	// Remarks:   Gets the number of elements.
	// Notes:    
	//************************************
	static FS_INT32	GetCount(FS_MapPtrToPtr map);


	//************************************
	// Function:  IsEmpty	
	// Param[in]: map The input ptr-to-ptr map.
	// Return:    <a>TRUE</a> for map being empty. <a>FALSE</a> otherwise.
	// Remarks:   Tests whether the <param>map</param> is empty.
	// Notes:
	//************************************
	static FS_BOOL IsEmpty(FS_MapPtrToPtr map);


	//************************************
	// Function:   Lookup
	// Param[in]:  map The input ptr-to-ptr map.
	// Param[in]:  key The key to lookup.
	// Param[out]: outValue A ref of a typeless pointer to receive the found value.
	// Return:     <a>TRUE</a> for value being found. <a>FALSE</a> otherwise.
	// Remarks:    Lookups by a key.
	// Notes:
	//************************************
	static FS_BOOL Lookup(FS_MapPtrToPtr map, void* key, void** outValue);


	//************************************
	// Function:  GetValueAt	
	// Param[in]: map The input ptr-to-ptr map.
	// Param[in]: key The key to specify a value.
	// Return:   A value. <a>NULL</a> if the key is not found.
	// Remarks:   Retrieves a value pointer by a key
	// Notes:
	//************************************
	static void* GetValueAt(FS_MapPtrToPtr map, void* key);


	//************************************
	// Function:  SetAt	
	// Param[in]: map			The input ptr-to-ptr map.
	// Param[in]: key			The key to specify a position.
	// Param[in]: newValue		The new value.
	// Return:    void
	// Remarks:   Adds a new (key, value) pair. Adds if not exist, otherwise modify.
	// Notes:
	//************************************
	static void SetAt(FS_MapPtrToPtr map, void* key, void* newValue);


	//************************************
	// Function:  RemoveKey	
	// Param[in]: map The input ptr-to-ptr map.
	// Param[in]: key The key to remove.
	// Return:    <a>TRUE</a> for success, otherwise <a>FALSE</a>.
	// Remarks:   Removes existing (key, value) pair.
	// Notes:     
	//************************************
	static FS_BOOL RemoveKey(FS_MapPtrToPtr map, void* key);


	//************************************
	// Function:  RemoveAll	
	// Param[in]: map The input ptr-to-ptr map.
	// Return:    void
	// Remarks:   Removes all the (key, value) pairs in the map.
	// Notes:
	//************************************
	static void RemoveAll(FS_MapPtrToPtr map);


	//************************************
	// Function:  GetStartPosition	
	// Param[in]: map The input ptr-to-ptr map.
	// Return:    The first key-value pair position in the map
	// Remarks:   Gets the first key-value pair position, iterating all (key, value) pairs.
	// Notes:
	//************************************
	static FS_POSITION GetStartPosition(FS_MapPtrToPtr map);


	//************************************
	// Function:      GetNextAssoc
	// Param[in]:     map					The input ptr-to-ptr map.
	// Param[in,out]: inOutNextPosition		Input a position, and receive the next association position.
	// Param[out]:    outKey				(Filled by this method)Receive a key.
	// Param[out]:    outValue				(Filled by this method)Receive a value.
	// Return:        void
	// Remarks:       Gets the current association and sets the position to next association.
	// Notes:
	//************************************
	static void GetNextAssoc(FS_MapPtrToPtr map, FS_POSITION* inOutNextPosition, void** outKey, void** outValue);


	//************************************
	// Function:  GetHashTableSize
	// Param[in]: map		The input ptr-to-ptr map.
	// Return:    The hash table size.
	// Remarks:   Gets the internal hash table size. Advanced features for derived classes.
	// Notes:     
	//************************************
	static FS_DWORD GetHashTableSize(FS_MapPtrToPtr map);


	//************************************
	// Function:  InitHashTable	
	// Param[in]: map			The input ptr-to-ptr map.
	// Param[in]: hashSize		Initializes the hash table size.
	// Param[in]: bAllocNow		Does it Now allocate the hash table? No-zero means yes, otherwise not.
	// Return:    void
	// Remarks:   Initializes the hash table
	// Notes:     
	//************************************
	static void InitHashTable(FS_MapPtrToPtr map, FS_DWORD hashSize, FS_BOOL bAllocNow);

};



class CFS_PtrArray_V1
{
	
public:
	//************************************
	// Function:  New	
	// Param[in]: void
	// Return:    A new empty pointer array.
	// Remarks:   Creates a new empty pointer array.
	// Notes:
	//************************************
	static FS_PtrArray New(void);


	//************************************
	// Function:  Destroy	
	// Param[in]: arr		The input pointer array.
	// Return:    void
	// Remarks:   Destroys a pointer array.
	// Notes:
	//************************************
	static void Destroy(FS_PtrArray arr);

	//************************************
	// Function:  GetSize	
	// Param[in]: arr		The input pointer array.
	// Return:    The number of elements in the array.
	// Remarks:   Gets the number of elements in the array.
	// Notes:
	//************************************
	static FS_INT32	GetSize(FS_PtrArray arr);


	//************************************
	// Function:  GetUpperBound	
	// Param[in]: arr		The input pointer array.
	// Return:    The upper bound.
	// Remarks:   Gets the upper bound in the array, actually the maximum valid index.
	// Notes:
	//************************************
	static FS_INT32	GetUpperBound(FS_PtrArray arr);


	//************************************
	// Function:  SetSize
	// Param[in]: arr		 The input pointer array.
	// Param[in]: nNewSize	 The new size in elements expected.
	// Param[in]: nGrowBy	 The grow amount in elements expected, nGrowBy can be -1 for the grow amount unchanged.
	// Return:    void
	// Remarks:   Changes the allocated size and the growing amount.
	// Notes:
	//************************************
	static void	SetSize(FS_PtrArray arr, FS_INT32 nNewSize, FS_INT32 nGrowBy);


	//************************************
	// Function:  RemoveAll	
	// Param[in]: arr		The input pointer array.
	// Return:    void
	// Remarks:   Cleans up the array.
	// Notes:
	//************************************
	static void	RemoveAll(FS_PtrArray arr);


	//************************************
	// Function:  GetAt	
	// Param[in]: arr		The input pointer array.
	// Param[in]: index		Specifies the zero-based index of the element.
	// Return:    An element.
	// Remarks:	  Retrieves an element specified by an index number.
	// Notes:
	//************************************
	static void* GetAt(FS_PtrArray arr, FS_INT32 index);


	//************************************
	// Function:  SetAt
	// Param[in]: arr		The input pointer array.
	// Param[in]: index		Specifies the zero-based index of the element.
	// Param[in]: newItem	An element
	// Return:    void
	// Remarks:	  Overwrites an element specified by an index number.
	// Notes:
	//************************************
	static void SetAt(FS_PtrArray arr, FS_INT32 index, void* newItem);


	//************************************
	// Function:  SetAtGrow
	// Param[in]: arr		The input pointer array.
	// Param[in]: index		Specifies the zero-based index of element in the array.
	// Param[in]: newItem	The input element.
	// Return:    void
	// Remarks:	  Sets an element value at specified position. Potentially grow the array.
	// Notes:
	//************************************
	static void SetAtGrow(FS_PtrArray arr, FS_INT32 index, void* newItem);


	//************************************
	// Function:  Add
	// Param[in]: arr		The input pointer array.
	// Param[in]: newItem	The input element.
	// Return:    The added element's index in the array.
	// Remarks:   Adds an element at the tail. Potentially grow the array.
	// Notes:    
	//************************************
	static FS_INT32 Add(FS_PtrArray arr, void* newItem);


	//************************************
	// Function:  Append	
	// Param[in]: arr		The input pointer array.
	// Param[in]: srcArr	The input array.
	// Return:    The old size in elements.
	// Remarks:	  Appends an array.
	// Notes:
	//************************************
	static FS_INT32 Append(FS_PtrArray arr, const FS_PtrArray srcArr);


	//************************************
	// Function:  Copy	
	// Param[in]: arr		The input pointer array.
	// Param[in]: srcArr	The input array.
	// Return:    void
	// Remarks:   Copies from an array.
	// Notes:
	//************************************
	static void	Copy(FS_PtrArray arr, FS_PtrArray srcArr);

	//************************************
	// Function:    GetDataPtr
	// Param[in]: arr		The input pointer array.
	// Param[in]: index		Specifies the zero-based index of element in the array.
	// Return:    A pointer to the specified element.
	// Remarks:   Gets a pointer to the specified element in the array. Direct pointer access.
	// Notes:
	//************************************
	static void** GetDataPtr(FS_PtrArray arr, FS_INT32 index);

	//************************************
	// Function:  InsertAt	
	// Param[in]: arr			The input pointer array.
	// Param[in]: index			Specifies the zero-based index in the array.
	// Param[in]: newItem	    Specifies the element value to insert.
	// Param[in]: nCount		Specifies the count of the element to insert.
	// Return:    void
	// Remarks:   Inserts one or more continuous element at specified position.
	// Notes:
	//************************************
	static void InsertAt(FS_PtrArray arr, FS_INT32 index, void* newItem, FS_INT32 nCount);


	//************************************
	// Function:  RemoveAt
	// Param[in]: arr		The input pointer array.
	// Param[in]: index		Specifies the zero-based index in the array.
	// Param[in]: nCount	Specifies the count of element to remove.
	// Return:    void
	// Remarks:   Removes a number of elements at specified position.
	// Notes:     
	//************************************
	static void	RemoveAt(FS_PtrArray arr, FS_INT32 index, FS_INT32 nCount);


	//************************************
	// Function:  InsertNewArrayAt		
	// Param[in]: arr			The input pointer array.
	// Param[in]: nStartIndex	Specifies the zero-based index of start element to insert at.
	// Param[in]: newArray		The input array.
	// Return:    void
	// Remarks:   Inserts an array at specified position.
	// Notes:
	//************************************
	static void	InsertNewArrayAt(FS_PtrArray arr, FS_INT32 nStartIndex, FS_PtrArray newArray);


	//************************************
	// Function:  Find	   
	// Param[in]: arr			The input pointer array.
	// Param[in]: item			The input element.
	// Param[in]: nStartIndex	Specifies the zero-based index of start element to find.
	// Return:	  An index of the found element. It can be -1 for finding none.
	// Remarks:   Finds an element from specified position to last
	// Notes:
	//************************************
	static FS_INT32 Find(FS_PtrArray arr, void* item, FS_INT32 nStartIndex);
};

class CFS_ByteArray_V1
{

public:
	//************************************
	// Function:  New	
	// Param[in]: void
	// Return:    A new empty byte array.
	// Remarks:   Creates a new empty byte array.
	// Notes:
	//************************************
	static FS_ByteArray New(void);


	//************************************
	// Function:  Destroy	
	// Param[in]: arr		The input byte array.
	// Return:    void
	// Remarks:   Destroys a byte array.
	// Notes:
	//************************************
	static void Destroy(FS_ByteArray arr);

	//************************************
	// Function:  GetSize	
	// Param[in]: arr		The input byte array.
	// Return:    The number of elements in the array.
	// Remarks:   Gets the number of elements in the array.
	// Notes:
	//************************************
	static FS_INT32	GetSize(FS_ByteArray arr);


	//************************************
	// Function:  GetUpperBound	
	// Param[in]: arr		The input byte array.
	// Return:   The upper bound.
	// Remarks:   Gets the upper bound in the array, actually the maximum valid index.
	// Notes:
	//************************************
	static FS_INT32	GetUpperBound(FS_ByteArray arr);


	//************************************
	// Function:  SetSize
	// Param[in]: arr		 The input byte array.
	// Param[in]: nNewSize	 The new size in elements expected.
	// Param[in]: nGrowBy	 The growing amount in elements expected. <param>nGrowBy</param> can be -1 for the growing amount unchanged.
	// Return:    void
	// Remarks:   Changes the allocated size and the growing amount.
	// Notes:
	//************************************
	static void	SetSize(FS_ByteArray arr, FS_INT32 nNewSize, FS_INT32 nGrowBy);


	//************************************
	// Function:  RemoveAll	
	// Param[in]: arr		The input byte array.
	// Return:    void
	// Remarks:   Cleans up the array.
	// Notes:
	//************************************
	static void	RemoveAll(FS_ByteArray arr);


	//************************************
	// Function:  GetAt	
	// Param[in]: arr		The input byte array.
	// Param[in]: index		Specifies the zero-based index of the element.
	// Return:    An element
	// Remarks:	  Retrieves an element specified by an index number.
	// Notes:
	//************************************
	static FS_BYTE GetAt(FS_ByteArray arr, FS_INT32 index);


	//************************************
	// Function:  SetAt
	// Param[in]: arr		The input byte array.
	// Param[in]: index		Specifies the zero-based index of the element.
	// Param[in]: newItem	An element
	// Return:    void
	// Remarks:	  Overwrites an element specified by an index number.
	// Notes:
	//************************************
	static void SetAt(FS_ByteArray arr, FS_INT32 index, FS_BYTE newItem);


	//************************************
	// Function:  SetAtGrow
	// Param[in]: arr		The input byte array.
	// Param[in]: index		Specifies the zero-based index of element in the array.
	// Param[in]: newItem	The input element.
	// Return:    void
	// Remarks:	  Sets an element value at specified position. Potentially grow the array.
	// Notes:
	//************************************
	static void SetAtGrow(FS_ByteArray arr, FS_INT32 index, FS_BYTE newItem);


	//************************************
	// Function:  Add
	// Param[in]: arr		The input byte array.
	// Param[in]: newItem	The input element.
	// Return:    The added element's index in the array.
	// Remarks:   Adds an element at the tail. Potentially grow the array
	// Notes:    
	//************************************
	static FS_INT32 Add(FS_ByteArray arr, FS_BYTE newItem);


	//************************************
	// Function:  Append	
	// Param[in]: arr		The input byte array.
	// Param[in]: srcArr	The input array.
	// Return:    The old size in elements.
	// Remarks:	  Appends an array.
	// Notes:
	//************************************
	static FS_INT32 Append(FS_ByteArray arr, const FS_ByteArray srcArr);


	//************************************
	// Function:  Copy	
	// Param[in]: arr		The input byte array.
	// Param[in]: srcArr	The input array.
	// Return:    void
	// Remarks:   Copies from an array.
	// Notes:
	//************************************
	static void	Copy(FS_ByteArray arr, FS_ByteArray srcArr);

	//************************************
	// Function:    GetDataPtr
	// Param[in]: arr		The input byte array.
	// Param[in]: index		Specifies the zero-based index of element in the array.
	// Return:    A pointer to the specified element.
	// Remarks:   Gets a pointer to the specified element in the array. Direct pointer access.
	// Notes:
	//************************************
	static FS_BYTE* GetDataPtr(FS_ByteArray arr, FS_INT32 index);

	//************************************
	// Function:  InsertAt	
	// Param[in]: arr			The input byte array.	
	// Param[in]: index			Specifies the zero-based index in the array.
	// Param[in]: newItem	    Specifies the element value to insert.
	// Param[in]: nCount		Specifies the count of the element to insert.
	// Return:    void
	// Remarks:   Inserts one or more continuous element at specified position.
	// Notes:
	//************************************
	static void InsertAt(FS_ByteArray arr, FS_INT32 index, FS_BYTE newItem, FS_INT32 nCount);


	//************************************
	// Function:  RemoveAt
	// Param[in]: arr		The input byte array.
	// Param[in]: index		Specifies the zero-based index in the array.
	// Param[in]: nCount	Specifies the count of element to remove.
	// Return:    void
	// Remarks:   Removes a number of elements at specified position.
	// Notes:     
	//************************************
	static void	RemoveAt(FS_ByteArray arr, FS_INT32 index, FS_INT32 nCount);


	//************************************
	// Function:  InsertNewArrayAt		
	// Param[in]: arr			The input byte array.
	// Param[in]: nStartIndex	Specifies the zero-based index of start element to insert at.
	// Param[in]: newArray		The input array.
	// Return:    void
	// Remarks:   Inserts an array at specified position.
	// Notes:
	//************************************
	static void	InsertNewArrayAt(FS_ByteArray arr, FS_INT32 nStartIndex, FS_ByteArray newArray);


	//************************************
	// Function:  Find	   
	// Param[in]: arr			The input byte array.
	// Param[in]: item			The input element.
	// Param[in]: nStartIndex	Specifies the zero-based index of start element to find.
	// Return:	  An index of the found element. It can be -1 for finding none.
	// Remarks:   Finds an element from specified position to last
	// Notes:
	//************************************
	static FS_INT32 Find(FS_ByteArray arr, FS_BYTE item, FS_INT32 nStartIndex);
};

class CFS_WordArray_V1
{

public:
	//************************************
	// Function:  New	
	// Param[in]: void
	// Return:    A new empty word array.
	// Remarks:   Creates a new empty word array.
	// Notes:
	//************************************
	static FS_WordArray New(void);


	//************************************
	// Function:  Destroy	
	// Param[in]: arr		The input word array.
	// Return:    void
	// Remarks:   Destroys a word array.
	// Notes:
	//************************************
	static void Destroy(FS_WordArray arr);

	//************************************
	// Function:  GetSize	
	// Param[in]: arr		The input word array.
	// Return:   The number of elements in the array.
	// Remarks:   Gets the number of elements in the array.
	// Notes:
	//************************************
	static FS_INT32	GetSize(FS_WordArray arr);


	//************************************
	// Function:  GetUpperBound	
	// Param[in]: arr		The input word array.
	// Return:   The upper bound.
	// Remarks:   Gets the upper bound in the array, actually the maximum valid index.
	// Notes:
	//************************************
	static FS_INT32	GetUpperBound(FS_WordArray arr);


	//************************************
	// Function:  SetSize
	// Param[in]: arr		 The input word array.
	// Param[in]: nNewSize	 The new size in elements expected.
	// Param[in]: nGrowBy	 The growing amount in elements expected. <param>nGrowBy</param> can be -1 for the growing amount unchanged.
	// Return:    void
	// Remarks:   Changes the allocated size and the growing amount.
	// Notes:
	//************************************
	static void	SetSize(FS_WordArray arr, FS_INT32 nNewSize, FS_INT32 nGrowBy);


	//************************************
	// Function:  RemoveAll	
	// Param[in]: arr The input word array.
	// Return:    void
	// Remarks:   Cleans up the array.
	// Notes:
	//************************************
	static void	RemoveAll(FS_WordArray arr);


	//************************************
	// Function:  GetAt	
	// Param[in]: arr		The input word array.
	// Param[in]: index		Specifies the zero-based index of the element.
	// Return:    An element
	// Remarks:	  Retrieves an element specified by an index number.
	// Notes:
	//************************************
	static FS_WORD GetAt(FS_WordArray arr, FS_INT32 index);


	//************************************
	// Function:  SetAt
	// Param[in]: arr		The input word array.
	// Param[in]: index		Specifies the zero-based index of the element.
	// Param[in]: newItem	An element
	// Return:    void
	// Remarks:	  Overwrites an element specified by an index number.
	// Notes:
	//************************************
	static void SetAt(FS_WordArray arr, FS_INT32 index, FS_WORD newItem);


	//************************************
	// Function:  SetAtGrow
	// Param[in]: arr		The input word array.
	// Param[in]: index		Specifies the zero-based index of element in the array.
	// Param[in]: newItem	The input element.
	// Return:    void
	// Remarks:	  Sets an element value at specified position. Potentially growing the array.
	// Notes:
	//************************************
	static void SetAtGrow(FS_WordArray arr, FS_INT32 index, FS_WORD newItem);


	//************************************
	// Function:  Add
	// Param[in]: arr		The input word array.
	// Param[in]: newItem	The input element.
	// Return:    The added element's index.
	// Remarks:   Adds an element at the tail. Potentially growing the array
	// Notes:    
	//************************************
	static FS_INT32 Add(FS_WordArray arr, FS_WORD newItem);


	//************************************
	// Function:  Append	
	// Param[in]: arr		The input word array.
	// Param[in]: srcArr	The input array.
	// Return:    The old size in elements.
	// Remarks:	  Appends an array.
	// Notes:
	//************************************
	static FS_INT32 Append(FS_WordArray arr, const FS_WordArray srcArr);


	//************************************
	// Function:  Copy	
	// Param[in]: arr		The input word array.
	// Param[in]: srcArr	The input array.
	// Return:    void
	// Remarks:   Copies from an array.
	// Notes:
	//************************************
	static void	Copy(FS_WordArray arr, FS_WordArray srcArr);

	//************************************
	// Function:    GetDataPtr
	// Param[in]: arr		The input word array.
	// Param[in]: index		Specifies the zero-based index of element in the array.
	// Return:    A pointer to the specified element.
	// Remarks:   Gets a pointer to the specified element in the array. Direct pointer access.
	// Notes:
	//************************************
	static FS_WORD* GetDataPtr(FS_WordArray arr, FS_INT32 index);

	//************************************
	// Function:  InsertAt	
	// Param[in]: arr			The input word array.
	// Param[in]: index			Specifies the zero-based index in the array.
	// Param[in]: newItem	    Specifies the element value to insert.
	// Param[in]: nCount		Specifies the count of the element to insert.
	// Return:    void
	// Remarks:   Inserts one or more continuous element at specified position.
	// Notes:
	//************************************
	static void InsertAt(FS_WordArray arr, FS_INT32 index, FS_WORD newItem, FS_INT32 nCount);


	//************************************
	// Function:  RemoveAt
	// Param[in]: arr		The input word array.
	// Param[in]: index		Specifies the zero-based index in the array.
	// Param[in]: nCount	Specifies the count of element to remove.
	// Return:    void
	// Remarks:   Removes a number of elements at specified position.
	// Notes:     
	//************************************
	static void	RemoveAt(FS_WordArray arr, FS_INT32 index, FS_INT32 nCount);


	//************************************
	// Function:  InsertNewArrayAt		
	// Param[in]: arr			The input word array.
	// Param[in]: nStartIndex	Specifies the zero-based index of start element to insert at.
	// Param[in]: newArray		The input array.
	// Return:    void
	// Remarks:   Inserts an array at specified position.
	// Notes:
	//************************************
	static void	InsertNewArrayAt(FS_WordArray arr, FS_INT32 nStartIndex, FS_WordArray newArray);


	//************************************
	// Function:  Find	   
	// Param[in]: arr			The input word array.
	// Param[in]: item			The input element.
	// Param[in]: nStartIndex	Specifies the zero-based index of start element to find.
	// Return:	  An index of the found element. It can be -1 for found none.
	// Remarks:   Finds an element from specified position to last
	// Notes:
	//************************************
	static FS_INT32 Find(FS_WordArray arr, FS_WORD item, FS_INT32 nStartIndex);
};

class CFS_DWordArray_V1
{

public:
	//************************************
	// Function:  New	
	// Param[in]: void
	// Return:    A new empty DWord array.
	// Remarks:   Creates a new empty DWord array.
	// Notes:
	//************************************
	static FS_DWordArray New(void);


	//************************************
	// Function:  Destroy	
	// Param[in]: arr			The input DWord array.
	// Return:    void
	// Remarks:   Destroys a double word array.
	// Notes:
	//************************************
	static void Destroy(FS_DWordArray arr);

	//************************************
	// Function:  GetSize	
	// Param[in]: arr			The input DWord array.
	// Return:   The number of elements in the array.
	// Remarks:   Gets the number of elements in the array.
	// Notes:
	//************************************
	static FS_INT32	GetSize(FS_DWordArray arr);


	//************************************
	// Function:  GetUpperBound	
	// Param[in]: arr			The input DWord array.
	// Return:   The upper bound.
	// Remarks:   Gets the upper bound in the array. actually the maximum valid index.
	// Notes:
	//************************************
	static FS_INT32	GetUpperBound(FS_DWordArray arr);


	//************************************
	// Function:  SetSize
	// Param[in]: arr		 The input DWord array.
	// Param[in]: nNewSize	 The new size in elements expected.
	// Param[in]: nGrowBy	 The grow amount in elements expected, nGrowBy can be -1 for the grow amount unchanged.
	// Return:    void
	// Remarks:   Changes the allocated size and the grow amount.
	// Notes:
	//************************************
	static void	SetSize(FS_DWordArray arr, FS_INT32 nNewSize, FS_INT32 nGrowBy);


	//************************************
	// Function:  RemoveAll	
	// Param[in]: arr The input DWord array.
	// Return:    void
	// Remarks:   Cleans up the array.
	// Notes:
	//************************************
	static void	RemoveAll(FS_DWordArray arr);


	//************************************
	// Function:  GetAt	
	// Param[in]: arr		The input DWord array.
	// Param[in]: index		Specifies the zero-based index of the element.
	// Return:    An element
	// Remarks:	  Retrieves an element specified by an index number.
	// Notes:
	//************************************
	static FS_DWORD GetAt(FS_DWordArray arr, FS_INT32 index);


	//************************************
	// Function:  SetAt
	// Param[in]: arr		The input DWord array.
	// Param[in]: index		Specifies the zero-based index of the element.
	// Param[in]: newItem	An element
	// Return:    void
	// Remarks:	  Overwrites an element specified by an index number.
	// Notes:
	//************************************
	static void SetAt(FS_DWordArray arr, FS_INT32 index, FS_DWORD newItem);


	//************************************
	// Function:  SetAtGrow
	// Param[in]: arr		The input DWord array.
	// Param[in]: index		Specifies the zero-based index of element in the array.
	// Param[in]: newItem	The input element.
	// Return:    void
	// Remarks:	  Sets an element value at specified position. Potentially growing the array.
	// Notes:
	//************************************
	static void SetAtGrow(FS_DWordArray arr, FS_INT32 index, FS_DWORD newItem);


	//************************************
	// Function:  Add
	// Param[in]: arr		The input DWord array.
	// Param[in]: newItem	The input element.
	// Return:    The added element's index.
	// Remarks:   Adds an element at the tail. Potentially growing the array
	// Notes:    
	//************************************
	static FS_INT32 Add(FS_DWordArray arr, FS_DWORD newItem);


	//************************************
	// Function:  Append	
	// Param[in]: arr		The input DWord array.
	// Param[in]: srcArr	The input array.
	// Return:    The old size in elements.
	// Remarks:	  Appends an array.
	// Notes:
	//************************************
	static FS_INT32 Append(FS_DWordArray arr, const FS_DWordArray srcArr);


	//************************************
	// Function:  Copy	
	// Param[in]: arr		The input DWord array.
	// Param[in]: srcArr	The input array.
	// Return:    void
	// Remarks:   Copies from an array.
	// Notes:
	//************************************
	static void	Copy(FS_DWordArray arr, FS_DWordArray srcArr);

	//************************************
	// Function:    GetDataPtr
	// Param[in]: arr		The input DWord array.
	// Param[in]: index		Specifies the zero-based index of element in the array.
	// Return:    A pointer to the specified element.
	// Remarks:   Gets a pointer to the specified element in the array. Direct pointer access.
	// Notes:
	//************************************
	static FS_DWORD* GetDataPtr(FS_DWordArray arr, FS_INT32 index);

	//************************************
	// Function:  InsertAt	
	// Param[in]: arr			The input DWord array.
	// Param[in]: index			Specifies the zero-based index in the array.
	// Param[in]: newItem	    Specifies the element value to insert.
	// Param[in]: nCount		Specifies the count of the element to insert.
	// Return:    void
	// Remarks:   Inserts one or more continuous element at specified position.
	// Notes:
	//************************************
	static void InsertAt(FS_DWordArray arr, FS_INT32 index, FS_DWORD newItem, FS_INT32 nCount);


	//************************************
	// Function:  RemoveAt
	// Param[in]: arr		The input DWord array.
	// Param[in]: index		Specifies the zero-based index in the array.
	// Param[in]: nCount	Specifies the count of element to remove.
	// Return:    void
	// Remarks:   Removes a number of elements at specified position.
	// Notes:     
	//************************************
	static void	RemoveAt(FS_DWordArray arr, FS_INT32 index, FS_INT32 nCount);


	//************************************
	// Function:  InsertNewArrayAt		
	// Param[in]: arr			The input DWord array.
	// Param[in]: nStartIndex	Specifies the zero-based index of start element to insert at.
	// Param[in]: newArray		The input array.
	// Return:    void
	// Remarks:   Insets an array at specified position.
	// Notes:
	//************************************
	static void	InsertNewArrayAt(FS_DWordArray arr, FS_INT32 nStartIndex, FS_DWordArray newArray);


	//************************************
	// Function:  Find	   
	// Param[in]: arr			The input DWord array.
	// Param[in]: item			The input element.
	// Param[in]: nStartIndex	Specifies the zero-based index of start element to find.
	// Return:	  An index of the found element. It can be -1 for found none.
	// Remarks:   Finds an element from specified position to last
	// Notes:
	//************************************
	static FS_INT32 Find(FS_DWordArray arr, FS_DWORD item, FS_INT32 nStartIndex);
};

class CFS_ByteStringArray_V1
{

public:
	//************************************
	// Function:  New	
	// Param[in]: void
	// Return:    A new empty byte-string array.
	// Remarks:   Creates a new empty byte-string array.
	// Notes:
	//************************************
	static FS_ByteStringArray New(void);


	//************************************
	// Function:  Destroy	
	// Param[in]: arr			The input byte-string array.
	// Return:    void
	// Remarks:   Destroys the byte-string array.
	// Notes:
	//************************************
	static void Destroy(FS_ByteStringArray arr);

	//************************************
	// Function:  GetAt	
	// Param[in]: arr		The input byte-string array.
	// Param[in]: index		Specifies the zero-based index of the element.
	// Param[out]: outByteString It retrieves an element specified by an index number.
	// Return:    void
	// Remarks:	  Retrieves an element specified by an index number.
	// Notes:
	//************************************
	static void GetAt(FS_ByteStringArray arr, FS_INT32 index, FS_ByteString* outByteString);

	//************************************
	// Function:  GetSize	
	// Param[in]: arr			The input byte-string array.
	// Return:   The number of elements in the array.
	// Remarks:   Gets the number of elements in the array.
	// Notes:
	//************************************
	static FS_INT32	GetSize(FS_ByteStringArray arr);


	//************************************
	// Function:  RemoveAll	
	// Param[in]: arr The input byte-string array.
	// Return:    void
	// Remarks:   Cleans up the array.
	// Notes:
	//************************************
	static void	RemoveAll(FS_ByteStringArray arr);


	//************************************
	// Function:  Add
	// Param[in]: arr		The input byte-string array.
	// Param[in]: newItem	The input element.
	// Param[in]: newItem	The length.
	// Return:    void
	// Remarks:   Adds an element at the tail. Potentially growing the array
	// Notes:    
	//************************************
	static void Add(FS_ByteStringArray arr, FS_LPSTR newItem, FS_INT32 nLen);


	//************************************
	// Function:  RemoveAt
	// Param[in]: arr		The input byte-string array.
	// Param[in]: index		Specifies the zero-based index in the array.
	// Return:    void
	// Remarks:   Removes a number of elements at specified position.
	// Notes:     
	//************************************
	static void	RemoveAt(FS_ByteStringArray arr, FS_INT32 index);

	//************************************
	// Function:  Add2
	// Param[in]: arr		The input byte-string array.
	// Param[in]: bsNew		The new byte string.
	// Return:    void
	// Remarks:   Add a copy of an existing object to the array.
	// Notes:     
	//************************************
	static void	Add2(FS_ByteStringArray arr, FS_ByteString bsNew);

	//************************************
	// Function:  GetDataPtr	
	// Param[in]: arr		The input byte-string array.
	// Param[in]: index		Specifies the zero-based index of the element.
	// Return:    Gets the pointer to the specified element.
	// Remarks:	  The pointer to the specified element.
	// Notes:
	//************************************
	static FS_ByteString GetDataPtr(FS_ByteStringArray arr, FS_INT32 index);

};

class CFS_WideStringArray_V1
{

public:
	//************************************
	// Function:  New	
	// Param[in]: void
	// Return:    A new empty wide-string array.
	// Remarks:   Creates a new empty wide-string array.
	// Notes:
	//************************************
	static FS_WideStringArray New(void);


	//************************************
	// Function:  Destroy	
	// Param[in]: arr The input wide-string array.
	// Return:    void
	// Remarks:   Destroys the wide-string array.
	// Notes:
	//************************************
	static void Destroy(FS_WideStringArray arr);

	//************************************
	// Function:  GetAt	
	// Param[in]: arr			The input wide-string array.
	// Param[in]: index			Specifies the zero-based index of the element.
	// Param[out]: outWideString Retrieves an element specified by an index number.
	// Return:    void
	// Remarks:	  Retrieves an element specified by an index number.
	// Notes:
	//************************************
	static void GetAt(FS_WideStringArray arr, FS_INT32 index, FS_WideString* outWideString);

	//************************************
	// Function:  GetSize	
	// Param[in]: arr		The input wide-string array.
	// Return:   The number of elements in the array.
	// Remarks:   Gets the number of elements in the array.
	// Notes:
	//************************************
	static FS_INT32	GetSize(FS_WideStringArray arr);


	//************************************
	// Function:  RemoveAll	
	// Param[in]: arr		The input wide-string array.
	// Return:    void
	// Remarks:   Cleans up the array.
	// Notes:
	//************************************
	static void	RemoveAll(FS_WideStringArray arr);


	//************************************
	// Function:  Add
	// Param[in]: arr		The input wide-string array.
	// Param[in]: newItem	The input element.
	// Return:    void
	// Remarks:   Adds an element at the tail. Potentially growing the array
	// Notes:    
	//************************************
	static void Add(FS_WideStringArray arr, FS_LPCWSTR newItem);


	//************************************
	// Function:  RemoveAt
	// Param[in]: arr		The input wide-string array.
	// Param[in]: index		Specifies the zero-based index in the array.
	// Return:    void
	// Remarks:   Removes a number of elements at specified position.
	// Notes:     
	//************************************
	static void	RemoveAt(FS_WideStringArray arr, FS_INT32 index);

};

class CFS_CodeTransformation_V1
{
public:
	//************************************
	// Function:  NameDecode	
	// Param[in]: orig				The input buffered name of lexical form.
	// Param[out]:outDecodedName	The decoded name.
	// Return:    void
	// Remarks:   Decode a name from it lexical form, From a buffered name of lexical form.
	// Notes:
	//************************************
	static void NameDecode(FS_ByteString orig, FS_ByteString* outDecodedName);


	//************************************
	// Function:   NameEncode	
	// Param[in]:  orig					The input name.
	// Param[out]: outDecodedName		A lexical form of the name.
	// Return:     void
	// Remarks:    Encode a name to lexical form (to be used for output).
	// Notes:
	//************************************
	static void NameEncode(FS_ByteString orig, FS_ByteString* outDecodedName);


	//************************************
	// Function:  EncodeString	
	// Param[in]: src					The input string.
	// Param[in]: bHex					Whether we will do hex-encoding.
	// Param[out]:outEncodedString		The PDF formatted string.
	// Return:    void
	// Remarks:   A PDF formatted string (including () or <>).
	// Notes:
	//************************************
	static void EncodeString(FS_ByteString src, FS_BOOL bHex, FS_ByteString* outEncodedString);


	//************************************
	// Function:  DecodeText
	// Param[in]: str				The input PDF encoded text string.
	// Param[in]: charMap			The input character mapping.
	// Param[out]:outDecodeText		A Unicode encoded string.
	// Return:    void
	// Remarks:   Decode PDF encoded text string into a Unicode encoded string, from a byte string.
	// Notes:     If no char map specified, PDFDocEncoding is used.
	//************************************
	static void DecodeText(FS_ByteString str, FS_CharMap charMap, FS_WideString* outDecodeText);



	//************************************
	// Function:   DecodeText2	
	// Param[in]:  pData			The input PDF encoded text buffer.
	// Param[in]:  size				The size in bytes of the buffer.
	// Param[in]:  charMap			The input character mapping.
	// Param[out]: outDecodeText	A Unicode encoded string.
	// Return:     void
	// Remarks:    Decode PDF encoded text string into a Unicode encoded string, from a memory block.	
	// Notes:      If no char map specified, PDFDocEncoding is used.
	//************************************
	static void DecodeText2(FS_LPBYTE pData, FS_DWORD size, FS_CharMap charMap, FS_WideString* outDecodeText);


	//************************************
	// Function:  EncodeText	
	// Param[in]: pString			The input Unicode text string (UTF-16LE).
	// Param[in]: len				The length of the input unicode string. -1 for zero-terminated Unicode string.
	// Param[in]: charMap			The input character mapping.
	// Param[out]: outEncodeText	The PDF encoded string.
	// Return:    void
	// Remarks:   Encode a Unicode text string (UTF-16LE) into PDF encoding.
	// Notes:     If no char map specified, PDFDocEncoding is used.
	//************************************
	static void EncodeText(FS_LPWORD pString, FS_INT32 len, FS_CharMap charMap, FS_ByteString* outEncodeText);

	//************************************
	// Function:  FlateEncodeProc
	// Param[in]: src_buf		The source data.
	// Param[in]: src_size		The size in bytes of the source data.
	// Param[out]: dest_buf		It will receive the encoded data. It must be released by <a>FSCodeTransformationMemFree</a>.
	// Param[out]: dest_size	It will receive the size in bytes of the encoded data.
	// Return: void
	// Remarks: Flate encode algorithm.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	// See: FSCodeTransformationFlateDecodeProc
	//************************************
	static void FlateEncodeProc(FS_LPCBYTE src_buf, FS_DWORD src_size, FS_LPBYTE* dest_buf, FS_DWORD* dest_size);

	//************************************
	// Function:  FlateDecodeProc
	// Param[in]: src_buf		The source encoded data.
	// Param[in]: src_size		The size in bytes of the source encoded data.
	// Param[out]: dest_buf		It will receive the decoded data. It must be released by <a>FSCodeTransformationMemFree</a>.
	// Param[out]: dest_size	It will receive the size in bytes of the decoded data.
	// Return: The size in bytes of source data consumed.
	// Remarks: Flate decode algorithm.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	// See: FSCodeTransformationFlateEncodeProc
	//************************************
	static FS_DWORD FlateDecodeProc(FS_LPCBYTE src_buf, FS_DWORD src_size, FS_LPBYTE* dest_buf, FS_DWORD* dest_size);

	//************************************
	// Function:  RunLengthDecodeProc
	// Param[in]: src_buf		The source encoded data.
	// Param[in]: src_size		The size in bytes of the source encoded data.
	// Param[out]: dest_buf		It will receive the decoded data. It must be released by <a>FSCodeTransformationMemFree</a>.
	// Param[out]: dest_size	It will receive the size in bytes of the decoded data.
	// Return: The size in bytes of source data consumed.
	// Remarks: Run-length decode algorithm.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	// See: FSCodeTransformationRunLengthEncodeProc
	//************************************
	static FS_DWORD RunLengthDecodeProc(FS_LPCBYTE src_buf, FS_DWORD src_size, FS_LPBYTE* dest_buf, FS_DWORD* dest_size);

	//************************************
	// Function:  RunLengthEncodeProc
	// Param[in]: src_buf		The source data.
	// Param[in]: src_size		The size in bytes of the source data.
	// Param[out]: dest_buf		It will receive the encoded data. It must be released by <a>FSCodeTransformationMemFree</a>.
	// Param[out]: dest_size	It will receive the size in bytes of the encoded data.
	// Return:    
	// Remarks: Run-length encode algorithm.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	// See: FSCodeTransformationRunLengthDecodeProc
	//************************************
	static FS_BOOL RunLengthEncodeProc(FS_LPCBYTE src_buf, FS_DWORD src_size, FS_LPBYTE* dest_buf, FS_DWORD* dest_size);

	//************************************
	// Function:  MemFree
	// Param[in]: pointer The pointer to free;
	// Return:   void. 
	// Remarks: Free the pointer;
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void MemFree(void* pointer);

	//************************************
	// Function:  BasicModuleA85Encode
	// Param[in]: src_buf		The source data.
	// Param[in]: src_size		The size in bytes of the source data.
	// Param[out]: dest_buf		It will receive the encoded data. It must be released by <a>FSCodeTransformationMemFree</a>.
	// Param[out]: dest_size	It will receive the size in bytes of the encoded data.
	// Return: <a>TRUE</a> for success, <a>FALSE</a> for failure.
	// Remarks: A85 encode algorithm. This interface is for internal use only now.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_BOOL BasicModuleA85Encode(FS_LPCBYTE src_buf, FS_DWORD src_size, FS_LPBYTE* dest_buf, FS_DWORD* dest_size);

	//************************************
	// Function:  FlateModuleEncode
	// Param[in]: src_buf		The source data.
	// Param[in]: src_size		The size in bytes of the source data.
	// Param[out]: dest_buf		It will receive the encoded data. It must be released by <a>FSCodeTransformationMemFree</a>.
	// Param[out]: dest_size	It will receive the size in bytes of the encoded data.
	// Return: <a>TRUE</a> for success, <a>FALSE</a> for failure.
	// Remarks: Flate module encode. This interface is for internal use only now.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_BOOL FlateModuleEncode(FS_LPCBYTE src_buf, FS_DWORD src_size, FS_LPBYTE* dest_buf, FS_DWORD* dest_size);

	//************************************
	// Function:  FlateModuleEncode2
	// Param[in]: src_buf			The source data.
	// Param[in]: src_size			The size in bytes of the source data.
	// Param[in]: predictor			The input predictor.
	// Param[in]: Colors			The input colors.
	// Param[in]: BitsPerComponent	The input bits per component.
	// Param[in]: Columns			The input columns.
	// Param[out]: dest_buf			It will receive the encoded data. It must be released by <a>FSCodeTransformationMemFree</a>.
	// Param[out]: dest_size		It will receive the size in bytes of the encoded data.
	// Return: <a>TRUE</a> for success, <a>FALSE</a> for failure.
	// Remarks: Flate module encode. This interface is for internal use only now.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static FS_BOOL FlateModuleEncode2(FS_LPCBYTE src_buf, FS_DWORD src_size, FS_INT32 predictor, FS_INT32 Colors, FS_INT32 BitsPerComponent, FS_INT32 Columns, FS_LPBYTE* dest_buf, FS_DWORD* dest_size);
};



class CFS_FloatRectArray_V1
{

public:
	//************************************
	// Function:  New	
	// Param[in]: void
	// Return:    A new empty float rectangle array.
	// Remarks:   Creates a new empty float rectangle array.
	// Notes:
	//************************************
	static FS_FloatRectArray New(void);


	//************************************
	// Function:  Destroy	
	// Param[in]: arr			The input float rectangle array.
	// Return:    void
	// Remarks:   Destroys the float rectangle array.
	// Notes:
	//************************************
	static void Destroy(FS_FloatRectArray arr);

	//************************************
	// Function:  GetSize	
	// Param[in]: arr			The input float rectangle array.
	// Return:    The number of elements in the array.
	// Remarks:   Gets the number of elements in the array.
	// Notes:
	//************************************
	static FS_INT32	GetSize(FS_FloatRectArray arr);


	//************************************
	// Function:  GetUpperBound	
	// Param[in]: arr			The input float rectangle array.
	// Return:    The upper bound.
	// Remarks:   Gets the upper bound in the array. actually the maximum valid index.
	// Notes:
	//************************************
	static FS_INT32	GetUpperBound(FS_FloatRectArray arr);


	//************************************
	// Function:  SetSize
	// Param[in]: arr		 The input float rectangle array.
	// Param[in]: nNewSize	 The new size in elements expected.
	// Param[in]: nGrowBy	 The grow amount in elements expected, nGrowBy can be -1 for the grow amount unchanged.
	// Return:    void
	// Remarks:   Changes the allocated size and the grow amount.
	// Notes:
	//************************************
	static void	SetSize(FS_FloatRectArray arr, FS_INT32 nNewSize, FS_INT32 nGrowBy);


	//************************************
	// Function:  RemoveAll	
	// Param[in]: arr		The input float rectangle array.
	// Return:    void
	// Remarks:   Cleans up the array.
	// Notes:
	//************************************
	static void	RemoveAll(FS_FloatRectArray arr);


	//************************************
	// Function:  GetAt	
	// Param[in]: arr		The input float rectangle array.
	// Param[in]: index		Specifies the zero-based index of the element.
	// Return:    An element
	// Remarks:	  Retrieves an element specified by an index number.
	// Notes:
	//************************************
	static FS_FloatRect GetAt(FS_FloatRectArray arr, FS_INT32 index);


	//************************************
	// Function:  SetAt
	// Param[in]: arr		The input float rectangle array.
	// Param[in]: index		Specifies the zero-based index of the element.
	// Param[in]: newItem	An element
	// Return:    void
	// Remarks:	  Overwrites an element specified by an index number.
	// Notes:
	//************************************
	static void SetAt(FS_FloatRectArray arr, FS_INT32 index, FS_FloatRect newItem);


	//************************************
	// Function:  SetAtGrow
	// Param[in]: arr		The input float rectangle array.
	// Param[in]: index		Specifies the zero-based index of element in the array.
	// Param[in]: newItem	The input element.
	// Return:    void
	// Remarks:	  Sets an element value at specified position. Potentially growing the array.
	// Notes:
	//************************************
	static void SetAtGrow(FS_FloatRectArray arr, FS_INT32 index, FS_FloatRect newItem);


	//************************************
	// Function:  Add
	// Param[in]: arr		The input float rectangle array.
	// Param[in]: newItem	The input element.
	// Return:    The added element's index.
	// Remarks:   Adds an element at the tail. Potentially growing the array
	// Notes:    
	//************************************
	static FS_INT32 Add(FS_FloatRectArray arr, FS_FloatRect newItem);


	//************************************
	// Function:  Append	
	// Param[in]: arr		The input float rectangle array.
	// Param[in]: srcArr	The input array.
	// Return:   The old size in elements.
	// Remarks:	  Appends an array.
	// Notes:
	//************************************
	static FS_INT32 Append(FS_FloatRectArray arr, const FS_FloatRectArray srcArr);


	//************************************
	// Function:  Copy
	// Param[in]: arr		The input float rectangle array.
	// Param[in]: srcArr	The input array.
	// Return:    void
	// Remarks:   Copies from an array.
	// Notes:
	//************************************
	static void	Copy(FS_FloatRectArray arr, FS_FloatRectArray srcArr);


	//************************************
	// Function:  InsertAt	
	// Param[in]: arr			The input float rectangle array.		
	// Param[in]: index			Specifies the zero-based index in the array.
	// Param[in]: newItem	    Specifies the element value to insert.
	// Param[in]: nCount		Specifies the count of the element to insert.
	// Return:    void
	// Remarks:   Insets one or more continuous element at specified position.
	// Notes:
	//************************************
	static void InsertAt(FS_FloatRectArray arr, FS_INT32 index, FS_FloatRect newItem, FS_INT32 nCount);


	//************************************
	// Function:  RemoveAt
	// Param[in]: arr		The input float rectangle array.
	// Param[in]: index		Specifies the zero-based index in the array.
	// Param[in]: nCount	Specifies the count of element to remove.
	// Return:    void
	// Remarks:   Removes a number of elements at specified position.
	// Notes:     
	//************************************
	static void	RemoveAt(FS_FloatRectArray arr, FS_INT32 index, FS_INT32 nCount);


	//************************************
	// Function:  InsertNewArrayAt		
	// Param[in]: arr			The input float rectangle array.
	// Param[in]: nStartIndex	Specifies the zero-based index of start element to insert at.
	// Param[in]: newArray		The input array.
	// Return:    void
	// Remarks:   Insets an array at specified position.
	// Notes:
	//************************************
	static void	InsertNewArrayAt(FS_FloatRectArray arr, FS_INT32 nStartIndex, FS_FloatRectArray newArray);


	//************************************
	// Function:  Find	   
	// Param[in]: arr			The input float rectangle array.
	// Param[in]: item			The input element.
	// Param[in]: nStartIndex	Specifies the zero-based index of start element to find.
	// Return:	  An index of the found element. It can be -1 for found none.
	// Remarks:   Finds an element from specified position to last
	// Notes:
	//************************************
	static FS_INT32 Find(FS_FloatRectArray arr, FS_FloatRect item, FS_INT32 nStartIndex);
};

//////////////////////////////////////////////////////////////////////////
//class CFS_BinaryBuf_V1

class CFS_BinaryBuf_V1
{
public:
	
	//************************************
	// Function:  New
	// Param[in]: void
	// Return:    A new empty binary buffer.
	// Remarks:   Creates a new empty binary buffer.
	// Notes:
	//************************************
	static FS_BinaryBuf	New(void);

	//************************************
	// Function:  Destroy
	// Param[in]: buf The input binary buffer.
	// Return:    void
	// Remarks:   Destroys the binary buffer.
	// Notes:
	//************************************
	static void	 Destroy(FS_BinaryBuf buf);

	//************************************
	// Function:  Clear
	// Param[in]: buf The input binary buffer. 
	// Return:    void
	// Remarks:   Sets the binary buffer to be empty.
	// Notes:
	//************************************
	static void	Clear(FS_BinaryBuf buf);

	//************************************
	// Function:  EstimateSize
	// Param[in]: buf			The input binary buffer.
	// Param[in]: size			The new size expected.
	// Param[in]: allocStep		The new allocation step. If <param>allocstep</param> is 0, then the allocation step will not change.
	// Return:    void
	// Remarks:   Changes the allocated buffer size, and set the allocation step if <param>allocstep</param> is non-zero.
	// Notes:
	//************************************
	static void EstimateSize(FS_BinaryBuf buf, FS_StrSize size, FS_StrSize allocStep);

	//************************************
	// Function:  AppendBlock
	// Param[in]: buf		The input binary buffer.	
	// Param[in]: pBuf		A pointer to a binary buffer block.
	// Param[in]: size		The size in bytes of the buffer block.
	// Return:    void
	// Remarks:   Appends a binary buffer block.
	// Notes:
	//************************************
	static void	AppendBlock(FS_BinaryBuf buf, void* pBuf, FS_StrSize size);

	//************************************
	// Function:  AppendFill
	// Param[in]: buf		The input binary buffer.
	// Param[in]: byte		The input byte.
	// Param[in]: count		Number of times.
	// Return:    void
	// Remarks:   Appends a byte for specified number times. Not a byte-by-byte processing, but a byte filling processing internally.
	// Notes:
	//************************************
	static void	AppendFill(FS_BinaryBuf buf, FS_BYTE byte, FS_StrSize count);

	//************************************
	// Function:  AppendString
	// Param[in]: buf		The input binary buffer.
	// Param[in]: str		A no-buffered byte string.
	// Return:    void
	// Remarks:   Appends a non-buffered byte string.
	// Notes:
	//************************************
	static void	AppendString(FS_BinaryBuf buf, FS_CHAR* str);

	//************************************
	// Function:  AppendByte
	// Param[in]: buf		The input binary buffer.
	// Param[in]: byte		A single byte.
	// Return:    void
	// Remarks:   Appends a single byte.
	// Notes:
	//************************************
	static void	AppendByte(FS_BinaryBuf buf, FS_BYTE byte); 

	//************************************
	// Function:  InsertBlock
	// Param[in]: buf		The input binary buffer.
	// Param[in]: pos		Specifies the zero-based index of the position in the binary buffer.
	// Param[in]: pBuf		A pointer to a binary buffer block.
	// Param[in]: size		The size in bytes of the buffer block.
	// Return:    void
	// Remarks:   Insert a binary buffer block at the specified position.
	// Notes:
	//************************************
	static void InsertBlock(FS_BinaryBuf buf, FS_StrSize pos, const void* pBuf, FS_StrSize size);

	//************************************
	// Function:  AttachData
	// Param[in]: buf		The input binary buffer.
	// Param[in]: pBuf		A pointer to a binary buffer.
	// Param[in]: size		The size in bytes of the buffer.
	// Return:    void
	// Remarks:   Attach to a buffer (this buffer will belong to this object). The buffer must be allocated by FS_Alloc.
	// Notes:
	//************************************
	static void	AttachData(FS_BinaryBuf buf, void* pBuf, FS_StrSize size);

	//************************************
	// Function:  CopyData
	// Param[in]: buf		The input binary buffer.
	// Param[in]: pBuf		The input another buffer.
	// Param[in]: size		The size in bytes of the buffer.
	// Return:    void
	// Remarks:   Copies from another buffer.
	// Notes:
	//************************************
	static void CopyData(FS_BinaryBuf buf, const void* pBuf, FS_StrSize size);

	//************************************
	// Function:  TakeOver
	// Param[in]: buf		The input binary buffer.
	// Param[in]: other		Another buffer
	// Return:    void
	// Remarks:   Takeover another buffer. 
	// Notes:     It attaches to the source FS_BinaryBuf object's buffer; The source FS_BinaryBuf object
	//   has detached the buffer.
	//************************************
	static void	TakeOver(FS_BinaryBuf buf, FS_BinaryBuf other);

	//************************************
	// Function:  Delete
	// Param[in]: buf			The input binary buffer.
	// Param[in]: startIndex	Specifies the zero-based index of the start position to be deleted in the binary buffer.
	// Param[in]: count			Specifies the length in bytes to be deleted.
	// Return:    void
	// Remarks:   Delete a inter-zone buffer defining by parameters start_index and count in the binary buffer.
	// Notes:
	//************************************
	static void Delete(FS_BinaryBuf buf, FS_INT32 startIndex, FS_INT32 count);

	//************************************
	// Function:  GetBuffer
	// Param[in]: buf			The input binary buffer.
	// Return:    A byte pointer to the binary buffer.
	// Remarks:   Gets a byte pointer to the binary buffer.
	// Notes:
	//************************************
	static FS_LPBYTE GetBuffer(FS_BinaryBuf buf);

	//************************************
	// Function:  GetSize
	// Param[in]: buf		The input binary buffer.
	// Return:    The length in bytes of the binary buffer.
	// Remarks:   Gets the length of the binary buffer.
	// Notes:
	//************************************
	static FS_StrSize GetSize(FS_BinaryBuf buf);

	//************************************
	// Function:  GetByteString
	// Param[in]: buf			The input binary buffer.
	// Param[out]:outString		A byte string result.
	// Return:    void
	// Remarks:   Gets a byte string from the buffer.
	// Notes:
	//************************************
	static void	GetByteString(FS_BinaryBuf buf, FS_ByteString* outString);

	//************************************
	// Function:  DetachBuffer
	// Param[in]: buf			The input binary buffer.
	// Return:    void
	// Remarks:   Detachs the buffer. Just set buffer pointer to NULL, and set the binary buffer size to zero.
	// Notes:
	//************************************
	static void	DetachBuffer(FS_BinaryBuf buf);	
};

#ifndef CFS_CUSTOMERPAUSE
#define CFS_CUSTOMERPAUSE
class CFS_CustomerPauseHandler : public IFX_Pause, public CFX_Object
{
public:
	CFS_CustomerPauseHandler(FS_Pause pause);
	virtual FX_BOOL	NeedToPauseNow();
private:
	FS_Pause m_pause;
};
#endif

//////////////////////////////////////////////////////////////////////////
//class CFS_PauseHandler_V1

class CFS_PauseHandler_V1
{
public:
	//************************************
	// Function: Create
	// Param[in]: pause		The input pause handler structure.
	// Return:   The newly created pause handler.
	// Remarks:  Creates the pause handler.
	// Notes:
	//************************************
	static FS_PauseHandler	Create(FS_Pause pause);

	//************************************
	// Function: Destroy
	// Param[in]: pauseHandler		The input pause handler to be destroyed.
	// Return:   void
	// Remarks:  Destroys the pause handler.
	// Notes:
	//************************************
	static void	Destroy(FS_PauseHandler pauseHandler);
};


class CFPD_CustomerFileReadHandler : public IFX_FileRead, public CFX_Object
{
public:
	CFPD_CustomerFileReadHandler(FS_FileRead fileRead);
	virtual FX_FILESIZE	GetSize();
	virtual FX_BOOL		ReadBlock(void* buffer, FX_FILESIZE offset, size_t size);
	virtual void		Release();

	FS_FileRead m_fileRead;
};


//////////////////////////////////////////////////////////////////////////
//class CFS_FileReadHandler_V1

class CFS_FileReadHandler_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: fileRead The input file read callbacks.
	// Return:    The file access interface.
	// Remarks:   Creates the file access handler.
	// Notes:
	//************************************
	static FS_FileReadHandler New(FS_FileRead fileRead);

	//************************************
	// Function:  Destroy
	// Param[in]: fileReadHander The file access handler.
	// Return:    void
	// Remarks:   Frees  file read handler.
	// Notes:
	//************************************
	static void Destroy(FS_FileReadHandler fileReadHander);

	
	//************************************
	// Function:  GetSize
	// Param[in]: fileReadHander The file access handler.
	// Return:    File size, in bytes..
	// Remarks:   GetS total size of the file.
	// Notes:
	//************************************
	static FS_DWORD	GetSize(FS_FileReadHandler fileReadHander);

	//************************************
	// Function:  ReadBlock
	// Param[in]: fileReadHander	The file access handler.
	// Param[in,out]: buffer		Pointer to a buffer receiving read data.
	// Param[in]: offset			Byte offset for the block, from beginning of the file.
	// Param[in]: size				Number of bytes for the block.
	// Return:    <a>TRUE</a> for success, <a>FALSE</a> for failure.
	// Remarks:   Reads a data block from the file.
	// Notes:
	//************************************
	static FS_BOOL		ReadBlock(FS_FileReadHandler fileReadHander, void* buffer, FS_DWORD offset, FS_DWORD size);

	//************************************
	// Function:  New2
	// Param[in]: fileRead The input file read callbacks.
	// Return:    The file access interface.
	// Remarks:   Creates the file access handler from file path name.
	// Notes:
	//************************************
	static FS_FileReadHandler New2(FS_LPCSTR filename);

	//************************************
	// Function:  New3
	// Param[in]: fileRead The input file read callbacks.
	// Return:    The file access interface.
	// Remarks:   Creates the file access handler from file path name(UNICODE).
	// Notes:
	//************************************
	static FS_FileReadHandler New3(FS_LPCWSTR filename);
};

class CFPD_CustomerStreamWriteHandler : public IFX_StreamWrite, public CFX_Object
{
public:
	CFPD_CustomerStreamWriteHandler(FS_StreamWrite streamWrite);
	virtual FX_BOOL		WriteBlock(const void* pData, size_t size);
	virtual void		Release();
	
	FS_StreamWrite m_streamWrite;
};

//////////////////////////////////////////////////////////////////////////
//class CFS_StreamWriteHandler_V1

class CFS_StreamWriteHandler_V1
{
public:
	//************************************
	// Function:  New
	// Param[in]: streamWrite The input stream write callbacks.
	// Return:    The stream access interface.
	// Remarks:   Creates the stream access handler.
	// Notes:
	//************************************
	static FS_StreamWriteHandler New(FS_StreamWrite streamWrite);

	//************************************
	// Function:  Destroy
	// Param[in]: streamWritehandler The stream access handler.
	// Return:    void
	// Remarks:   Frees stream write handler.
	// Notes:
	//************************************
	static void Destroy(FS_StreamWriteHandler streamWritehandler);

	//************************************
	// Function:  WriteBlock
	// Param[in]: streamWritehandler	The stream access handler.
	// Param[in]: pData					The block data.
	// Param[in]: size					The length in bytes of the block data.
	// Return:    <a>TRUE</a> for success, <a>FALSE</a> for failure.
	// Remarks:   Write a block data.
	// Notes:
	//************************************
	static FS_BOOL	WriteBlock(FS_StreamWriteHandler streamWritehandler, const void* pData, FS_DWORD size);
};

//////////////////////////////////////////////////////////////////////////
//class CFS_Base64Encoder_V2
class CFS_Base64Encoder_V2
{
public:
	//************************************
	// Function:  New
	// Param[in]: wEqual	The input character used as a suffix purposes. Sets it <Italic>=</Italic> as default.
	// Return: The Base64 encoder object.
	// Remarks: Creates the Base64 encoder object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSBase64EncoderDestroy
	//************************************
	static FS_Base64Encoder New(FS_WCHAR wEqual);

	//************************************
	// Function:  Destroy
	// Param[in]: encoder	The input Base64 encoder object.
	// Return:    void
	// Remarks: Destroys the input Base64 encoder object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSBase64EncoderNew
	//************************************
	static void		Destroy(FS_Base64Encoder encoder);

	//************************************
	// Function:  SetEncoder
	// Param[in]: encoder		The input Base64 encoder object.
	// Param[in]: pEncodeProc	Callback function address to provide an external encoder. Sets it as NULL if need use default Base64 encoder.
	// Return:    void
	// Remarks: Sets external Base64 encoder.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSBase64EncoderNew
	//************************************
	static void		SetEncoder(FS_Base64Encoder encoder, FS_LPBase64EncodeProc* pEncodeProc);

	//************************************
	// Function:  Encode
	// Param[in]: encoder	The input Base64 encoder object.
	// Param[in]: pSrc		Source byte data array to encode.
	// Param[in]: iSrcLen	The length of source byte array, in bytes.
	// Param[out]: pDst		Destination pointer of wide character array.
	// Return: Returns the length of total data stored in destination buffer, in wide characters. If <param>pDst</param> is a NULL pointer, it returns the necessary buffer size in wide characters.
	// Remarks: Encodes byte data array into a wide character array.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSBase64EncoderNew
	//************************************
	static FS_INT32	Encode(FS_Base64Encoder encoder, FS_LPCBYTE pSrc, FS_INT32 iSrcLen, FS_LPWSTR pDst);

	//************************************
	// Function:  Encode2
	// Param[in]: encoder	The input Base64 encoder object.
	// Param[in]: src		Source byte string to encode.
	// Param[out]: out_Dst	Destination wide string to store encoded data. Creates it by <a>FSWideStringNew</a>.
	// Return: Returns the length of total data stored in destination, in wide characters.
	// Remarks: Encodes byte string into a wide string.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSBase64EncoderNew
	//************************************
	static FS_INT32	Encode2(FS_Base64Encoder encoder, const FS_ByteString src, FS_WideString* out_Dst);

	//************************************
	// Function:  Encode3
	// Param[in]: encoder	The input Base64 encoder object.
	// Param[in]: src		Source byte string to encode.
	// Param[out]: out_Dst	Destination byte string to store encoded data. Creates it by <a>FSByteStringNew</a>.
	// Return: Returns the length of total data stored in destination, in bytes.
	// Remarks: Encodes byte string into a UTF-8 byte string.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSBase64EncoderNew
	//************************************
	static FS_INT32	Encode3(FS_Base64Encoder encoder, const FS_ByteString src, FS_ByteString* out_Dst);

	
};

//////////////////////////////////////////////////////////////////////////
//class CFS_Base64Decoder_V2
class CFS_Base64Decoder_V2
{
public:
	//************************************
	// Function:  New
	// Param[in]: wEqual	The input character used as a suffix purposes. Sets it <Italic>=</Italic> as default.
	// Return: The Base64 decoder object.
	// Remarks: Creates the Base64 decoder object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSBase64DecoderDestroy
	//************************************
	static FS_Base64Decoder New(FS_WCHAR wEqual);

	//************************************
	// Function:  Destroy
	// Param[in]: decoder	The input Base64 decoder object.
	// Return:    void
	// Remarks: Destroys the input Base64 decoder object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSBase64DecoderNew
	//************************************
	static void		Destroy(FS_Base64Decoder decoder);

	//************************************
	// Function:  SetDecoder
	// Param[in]: decoder		The input Base64 decoder object.
	// Param[in]: pDecodeProc	Callback function address to provide an external decoder. Sets it as NULL if need use default Base64 decoder.
	// Return: void
	// Remarks: Sets external Base64 decoder.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSBase64DecoderNew
	//************************************
	static void		SetDecoder(FS_Base64Decoder decoder, FS_LPBase64DecodeProc pDecodeProc);

	//************************************
	// Function:  Decode
	// Param[in]: decoder	The input Base64 decoder object.
	// Param[in]: pSrc		Source wide character array to decode.
	// Param[in]: iSrcLen	The length of source wide character array, in wide characters.
	// Param[out]: pDst		Destination pointer of byte data array.
	// Return: Returns the length of total data stored in destination buffer, in bytes. If <param>pDst</param> is a NULL pointer, it returns the necessary buffer size in bytes.
	// Remarks: Decodes wide character array into byte buffer.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSBase64DecoderNew
	//************************************
	static FS_INT32	Decode(FS_Base64Decoder decoder, FS_LPCWSTR pSrc, FS_INT32 iSrcLen, FS_LPBYTE pDst);

	//************************************
	// Function:  Decode2
	// Param[in]: decoder	The input Base64 decoder object.
	// Param[in]: src		Source wide character string to decode.
	// Param[out]: out_Dst	Destination byte string to store decoded data.
	// Return: Returns the length of total data stored in destination, in bytes.
	// Remarks: Decodes wide string into a byte string.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSBase64DecoderNew
	//************************************
	static FS_INT32	Decode2(FS_Base64Decoder decoder, const FS_WideString src, FS_ByteString* out_Dst);

	//************************************
	// Function:  Decode3
	// Param[in]: decoder	The input Base64 decoder object.
	// Param[in]: src		Source UTF-8 byte string to decode.
	// Param[out]: out_Dst	Destination byte string to store decoded data.
	// Return: Returns the length of total data stored in destination, in bytes.
	// Remarks: Decodes UTF-8 byte string into a byte string.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSBase64DecoderNew
	//************************************
	static FS_INT32	Decode3(FS_Base64Decoder decoder, const FS_ByteString src, FS_ByteString* out_Dst);

};

//////////////////////////////////////////////////////////////////////////
//class CFS_FileWriteHandler_V2
class CFS_FileWriteHandler_V2
{
public:
	
	//************************************
	// Function:  New
	// Param[in]: fileWrite	The input file write callbacks.
	// Return: The file access interface for writing.
	// Remarks: Creates the file access handler for writing.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSFileWriteHandlerDestroy
	//************************************
	static FS_FileWriteHandler New(FS_FileWrite fileWrite);

	//************************************
	// Function:  Destroy
	// Param[in]: handler	The input file write handler.
	// Return:    void
	// Remarks: Destroys the input file write handler.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSFileWriteHandlerNew
	//************************************
	static void Destroy(FS_FileWriteHandler handler);

};

//////////////////////////////////////////////////////////////////////////
//class CFS_XMLElement_V2
class CFS_XMLElement_V2
{
public:	
	//************************************
	// Function:  parse
	// Param[in]: pBuffer			The input buffer.
	// Param[in]: size				The size in bytes of the input buffer.
	// Param[in]: bSaveSpaceChars	Indicates whether need save space characters in content string, TRUE if save, or FALSE. Sets it FALSE as default.
	// Param[out]: pParsedSize		It receives the parsed size. Sets it NULL as default if you don't want to receive it.
	// Return: The XML element.
	// Remarks: Constructs an XML element using data in specified buffer.
	// The buffer can be discarded immediately after construction finished, because at this time,
	// all data have been loaded into the element (and its descendant element).
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementparse2
	//************************************
	static FS_XMLElement parse(const void* pBuffer, FS_DWORD size, FS_BOOL bSaveSpaceChars, FS_DWORD* pParsedSize);	

	//************************************
	// Function:  parse2
	// Param[in]: fileReadHander	The input file access object. Creates it by <a>FSFileReadHandlerNew</a>.
	// Param[in]: bSaveSpaceChars	Indicates whether need save space characters in content string, TRUE if save, or FALSE. Sets it FALSE as default.
	// Param[out]: pParsedSize		It receives the parsed size. Sets it NULL as default if you don't want to receive it.
	// Return: The XML element.
	// Remarks: Parses XML contents from a <a>FS_FileReadHandler</a> object.
	// The <a>FS_FileReadHandler</a> object can be discarded immediately after construction finished, because at this time,
	// all data have been loaded into the element (and its descendant element).
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementparse
	//************************************
	static FS_XMLElement parse2(FS_FileReadHandler fileReadHander, FS_BOOL bSaveSpaceChars, FS_DWORD* pParsedSize);

	//************************************
	// Function:  New
	// Param[in]: qSpace	Qualified namespace, or empty if no namespace or default namespace is used.
	// Param[in]: tagName	The input tag name.
	// Return: An empty XML element, with a qualified namespace and tag name.
	// Remarks: Creates an empty XML element, with a qualified namespace and tag name.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementDestroy
	//************************************
	static FS_XMLElement New(FS_LPCSTR qSpace, FS_LPCSTR tagName);

	//************************************
	// Function:  New2
	// Param[in]: tagName	Qualified tag name.
	// Return: An empty element, with qualified tag name.
	// Remarks: Creates an empty element, with qualified tag name.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementDestroy
	//************************************
	static FS_XMLElement New2(FS_LPCSTR tagName);

	//************************************
	// Function:  New3
	// Return: An empty element.
	// Remarks: Creates an empty element.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementDestroy
	//************************************
	static FS_XMLElement New3();

	//************************************
	// Function:  Destroy
	// Param[in]: XMLElement	The input XML element.
	// Return: void
	// Remarks: Destroys the input XML element.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementNew
	// See: FSXMLElementNew2
	// See: FSXMLElementNew3
	//************************************
	static void Destroy(FS_XMLElement XMLElement);	

	//************************************
	// Function:  GetTagName
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: bQualified	Indicates whether return a qualified tag name or not. 
	// Param[out]: outTagName	It receives the tag name. If <param>bQualified</param> is TRUE, the tag name is with qualified namespace, or only local tag name.
	// Return: void
	// Remarks: Gets the tag name of the XML element.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementNew
	//************************************
	static void	GetTagName(FS_XMLElement XMLElement, FS_BOOL bQualified, FS_ByteString* outTagName);	

	//************************************
	// Function:  GetNamespace
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: bQualified	Indicates whether return a qualified namespace or full URI namespace.
	// Param[out]: outNamespace	It receives the name space. If <param>bQualified</param> is TRUE, returns qualified namespace value, or full URI value.
	// Return: void
	// Remarks: Gets the namespace of the XML element.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementNew
	//************************************
	static void	GetNamespace(FS_XMLElement XMLElement, FS_BOOL bQualified, FS_ByteString* outNamespace);	

	//************************************
	// Function:  GetNamespaceURI
	// Param[in]: XMLElement		The input XML element.
	// Param[in]: name				Qualified namespace.
	// Param[out]: outNamespaceURI	It receives the full URI value.
	// Return: void
	// Remarks: Gets the full URI value for a qualified namespace.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetNamespace
	//************************************
	static void	GetNamespaceURI(FS_XMLElement XMLElement, FS_LPCSTR name, FS_ByteString* outNamespaceURI);	

	//************************************
	// Function:  GetParent
	// Param[in]: XMLElement	The input XML element.
	// Return: The parent XML element of the input XML element..
	// Remarks: Gets the parent element of the input XML element.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementCountChildren
	//************************************
	static FS_XMLElement GetParent(FS_XMLElement XMLElement);	

	//************************************
	// Function:  CountAttrs
	// Param[in]: XMLElement	The input XML element.
	// Return: The count of attributes of the input XML element.
	// Remarks: Gets the count of attributes of the input XML element.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetAttrByIndex
	//************************************
	static FS_DWORD CountAttrs(FS_XMLElement XMLElement);	

	//************************************
	// Function:  GetAttrByIndex
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: index			Index of the attribute (start from 0).
	// Param[out]: space		It receives the qualified namespace of attribute name.
	// Param[out]: name			It receives the attribute name (local name).
	// Param[out]: value		It receive the attribute value.
	// Return: void
	// Remarks: Gets the attribute by index.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementCountAttrs
	//************************************
	static void GetAttrByIndex(FS_XMLElement XMLElement, FS_INT32 index, FS_ByteString* space, FS_ByteString* name, FS_WideString* value);

	//************************************
	// Function:  HasAttr
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: qName			The input qualified attribute name.
	// Return: <a>TRUE</a> if attribute exists, <a>FALSE</a> if doesn't exist.
	// Remarks: Determines whether a qualified attribute exists or not.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementCountAttrs
	// See: FSXMLElementGetAttrByIndex
	//************************************
	static FS_BOOL HasAttr(FS_XMLElement XMLElement, FS_LPCSTR qName);	

	//************************************
	// Function:  GetAttrValue
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: name			The qualified attribute name.
	// Param[out]: attribute	It receive the attribute value.
	// Return: Whether the attribute exists or not.
	// Remarks: Gets attribute value without a namespace.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementHasAttr
	// See: FSXMLElementGetAttrValue2
	//************************************
	static FS_BOOL GetAttrValue(FS_XMLElement XMLElement, FS_LPCSTR name, FS_WideString* attribute);	

	//************************************
	// Function:  GetAttrValue2
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: space			The qualified namespace.
	// Param[in]: name			The qualified attribute name.
	// Param[out]: attribute	It receive the attribute value, A UTF-16LE wide string.
	// Return: Whether the attribute exists or not.
	// Remarks: Gets attribute value. Encoded in UTF-16LE format.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetAttrValue
	//************************************
	static FS_BOOL GetAttrValue2(FS_XMLElement XMLElement, FS_LPCSTR space, FS_LPCSTR name, FS_WideString* attribute);	

	//************************************
	// Function:  GetAttrInteger
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: name			The qualified attribute name.
	// Param[out]: attribute	It receives the attribute value, a integer value.
	// Return: Whether the attribute exists or not.
	// Remarks: Gets an integer from a particular attribute without a namespace.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetAttrInteger2
	//************************************
	static FS_BOOL GetAttrInteger(FS_XMLElement XMLElement, FS_LPCSTR name, FS_INT32* attribute);

	//************************************
	// Function:  GetAttrInteger2
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: space			The qualified namespace.
	// Param[in]: name			The local name for the attribute.
	// Param[out]: attribute	It receives the attribute value, a integer value.
	// Return: Whether the attribute exists or not.
	// Remarks: Gets an integer from a particular attribute, using a namespace.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetAttrInteger
	//************************************
	static FS_BOOL GetAttrInteger2(FS_XMLElement XMLElement, FS_LPCSTR space, FS_LPCSTR name, FS_INT32* attribute);	

	//************************************
	// Function:  GetAttrFloat
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: name			The qualified attribute name.
	// Param[out]: attribute	It receives the attribute value, a float value.
	// Return: Whether the attribute exists or not.
	// Remarks: Gets a float from a particular attribute without a namespace.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetAttrFloat2
	//************************************
	static FS_BOOL GetAttrFloat(FS_XMLElement XMLElement, FS_LPCSTR name, FS_FLOAT* attribute);	

	//************************************
	// Function:  GetAttrFloat2
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: space			The qualified namespace.
	// Param[in]: name			The local name for the attribute.
	// Param[out]: attribute	It receives the attribute value, a float value.
	// Return: Whether the attribute exists or not.
	// Remarks: Gets an float from a particular attribute, using a namespace.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetAttrFloat
	//************************************
	static FS_BOOL GetAttrFloat2(FS_XMLElement XMLElement, FS_LPCSTR space, FS_LPCSTR name, FS_FLOAT* attribute);	

	//************************************
	// Function:  CountChildren
	// Param[in]: XMLElement	The input XML element.
	// Return: The number of children.
	// Remarks: Gets the number of children of this element, including content segments and sub-elements.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetChildType
	//************************************
	static FS_DWORD CountChildren(FS_XMLElement XMLElement);	

	//************************************
	// Function:  GetChildType
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: index			Specifies the zero-based index in children array.
	// Return: The type of specified child.
	// Remarks: Gets the type of a child, it can be either a content segment, or a sub-element.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementCountChildren
	//************************************
	static FS_Child_Type GetChildType(FS_XMLElement XMLElement, FS_DWORD index);	

	//************************************
	// Function:  GetContent
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: index			Specifies the zero-based index in children array.
	// Param[out]: outContent	It receives the content.
	// Return: void
	// Remarks: Gets a content segment. If this child is an element, <param>outContent</param> will receive nothing.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetChildType
	//************************************
	static void GetContent(FS_XMLElement XMLElement, FS_DWORD index, FS_WideString* outContent);	

	//************************************
	// Function:  GetElement
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: index			Specifies the zero-based index in children array.
	// Return: A child element.
	// Remarks: Gets a particular child element. If this child is not an element, NULL will be returned.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetChildType
	// See: FSXMLElementGetElement2
	//************************************
	static FS_XMLElement GetElement(FS_XMLElement XMLElement, FS_DWORD index);	

	//************************************
	// Function:  GetElement2
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: space			The qualified namespace.
	// Param[in]: tag			The local name for the tag.
	// Return: An element.
	// Remarks: Gets an element with particular tag. If more than one element with the same tag, only the first one is returned.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetElement
	//************************************
	static FS_XMLElement GetElement2(FS_XMLElement XMLElement, FS_LPCSTR space, FS_LPCSTR tag);

	//************************************
	// Function:  GetElement3
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: space			The qualified namespace.
	// Param[in]: tag			The input tag.
	// Param[in]: index			Specifies the zero-based index of element with particular tag.
	// Return: An element.
	// Remarks: Gets an element with particular tag, and specified index if more than one elements with the same tag.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetElement
	// See: FSXMLElementGetElement2
	//************************************
	static FS_XMLElement GetElement3(FS_XMLElement XMLElement, FS_LPCSTR space, FS_LPCSTR tag, FS_INT32 index);

	//************************************
	// Function:  CountElements
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: space			The qualified namespace.
	// Param[in]: tag			The local name for the tag.
	// Return: The count number of elements with the input tag.
	// Remarks: Counts number of elements with particular tag.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetElement
	// See: FSXMLElementGetElement2
	// See: FSXMLElementGetElement3
	//************************************
	static FS_DWORD CountElements(FS_XMLElement XMLElement, FS_LPCSTR space, FS_LPCSTR tag);

	//************************************
	// Function:  FindElement
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: pChild		The input child element.
	// Return: The index number of child element, -1 if not found.
	// Remarks: Finds an element and returns its index.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementCountElements
	//************************************
	static FS_DWORD FindElement(FS_XMLElement XMLElement, FS_XMLElement pChild);	

	//************************************
	// Function:  SetTag
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: qSpace		The qualified namespace for the tag.
	// Param[in]: tagname		The input tag name.
	// Return: void
	// Remarks: Sets the tag name of the element.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetTagName
	//************************************
	static void SetTag(FS_XMLElement XMLElement, FS_LPCSTR qSpace, FS_LPCSTR tagname);

	//************************************
	// Function:  SetTag2
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: qTagName		The input tag name.
	// Return:    void
	// Remarks: Sets the tag name of the element.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetTagName
	// See: FSXMLElementSetTag
	//************************************
	static void SetTag2(FS_XMLElement XMLElement, FS_LPCSTR qTagName);	

	//************************************
	// Function:  SetAttrValue
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: name			The qualified attribute name.
	// Param[in]: value			The input wide string.
	// Return: void
	// Remarks: Sets attribute with a wide string.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetAttrValue
	//************************************
	static void SetAttrValue(FS_XMLElement XMLElement, FS_LPCSTR name, FS_WideString value);	

	//************************************
	// Function:  SetAttrValue2
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: name			The qualified attribute name.
	// Param[in]: value			The input integer.
	// Return: void
	// Remarks: Sets attribute with an integer.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetAttrInteger
	//************************************
	static void SetAttrValue2(FS_XMLElement XMLElement, FS_LPCSTR name, FS_INT32 value);

	//************************************
	// Function:  SetAttrValue3
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: name			The qualified attribute name.
	// Param[in]: value			The input float.
	// Return: void
	// Remarks: Sets attribute with a float.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementGetAttrFloat
	// See: FSXMLElementGetAttrFloat2
	//************************************
	static void SetAttrValue3(FS_XMLElement XMLElement, FS_LPCSTR name, FS_FLOAT value);	

	//************************************
	// Function:  RemoveAttr
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: name			The qualified attribute name.
	// Return: void
	// Remarks: Removes an attribute.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See:
	//************************************
	static void RemoveAttr(FS_XMLElement XMLElement, FS_LPCSTR name);

	//************************************
	// Function:  AddChildElement
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: pElement		The input element.
	// Return: void
	// Remarks: Adds an element to the child list (at the last position).
	// Notes: The child element can't be directly destroyed any more.
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementCountChildren
	//************************************
	static void AddChildElement(FS_XMLElement XMLElement, FS_XMLElement pElement);	

	//************************************
	// Function:  AddChildContent
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: content		The input content.
	// Param[in]: bCDATA		Whether the content is CDATA or not. Sets it <a>FALSE</a> as default.
	// Return: void
	// Remarks: Adds a content child.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementCountChildren
	// See: FSXMLElementAddChildElement
	//************************************
	static void AddChildContent(FS_XMLElement XMLElement, FS_WideString content, FS_BOOL bCDATA);	

	//************************************
	// Function:  InsertChildElement
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: index			Specifies the zero-based index of element in the child array.
	// Param[in]: pElement		The input element.
	// Return: void
	// Remarks: Inserts a child element before the specified index
	// Notes: The child element can't be directly destroyed any more.
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementAddChildElement
	//************************************
	static void InsertChildElement(FS_XMLElement XMLElement, FS_DWORD index, FS_XMLElement pElement);	

	//************************************
	// Function:  InsertChildContent
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: index			Specifies the zero-based index of element in the child array.
	// Param[in]: content		The input content.
	// Param[in]: bCDATA		Whether the content is CDATA or not. Sets it <a>FALSE</a> as default.
	// Return: void
	// Remarks: Inserts a content segment before the specified index.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementAddChildContent
	//************************************
	static void InsertChildContent(FS_XMLElement XMLElement, FS_DWORD index, FS_WideString content, FS_BOOL bCDATA);	

	//************************************
	// Function:  RemoveChildren
	// Param[in]: XMLElement	The input XML element.
	// Return: void
	// Remarks: Remove all children.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementRemoveChild
	//************************************
	static void RemoveChildren(FS_XMLElement XMLElement);	

	//************************************
	// Function:  RemoveChild
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: index			Specifies the zero-based index of element in the child array.
	// Return: void
	// Remarks: Removes a specified child item (element or content).
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementRemoveChildren
	//************************************
	static void RemoveChild(FS_XMLElement XMLElement, FS_DWORD index);	

	//************************************
	// Function:  OutputStream
	// Param[in]: XMLElement	The input XML element.
	// Param[out]: output		It receives the XML stream.
	// Return: void
	// Remarks: Outputs to a XML stream.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementOutputStream2
	//************************************
	static void OutputStream(FS_XMLElement XMLElement, FS_ByteString* output);	

	//************************************
	// Function:  OutputStream2
	// Param[in]: XMLElement	The input XML element.
	// Param[in]: handler		The input file write handler. Creates it by <a>FSFileWriteHandlerNew</a>.
	// Return: void
	// Remarks: Outputs through the file write handler.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FSXMLElementOutputStream
	//************************************
	static void OutputStream2(FS_XMLElement XMLElement, FS_FileWriteHandler handler);

	//************************************
	// Function:  Clone
	// Param[in]: XMLElement	The input XML element.
	// Return: The cloned XML element.
	// Remarks: Clones the specified XML element.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.0.2
	// See: FSXMLElementparse2
	//************************************
	static FS_XMLElement Clone(FS_XMLElement XMLElement);
};

class CFS_UTF8Decoder_V5
{
public:
	//************************************
	// Function:  New
	// Param[in]: void
	// Return:    The UTF8 decoder object.
	// Remarks:   Creates a new UTF8 decoder object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_UTF8Decoder New(void);

	//************************************
	// Function:  Destroy
	// Param[in]: decoder The input UTF8 decoder.
	// Return:    void.
	// Remarks:   Destroys the UTF8 decoder object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void Destroy(FS_UTF8Decoder decoder);

	//************************************
	// Function:  Clear
	// Param[in]: decoder The input UTF8 decoder.
	// Return:    void.
	// Remarks:   Clears the decoding status and sets the output wide text buffer to be empty.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void Clear(FS_UTF8Decoder decoder);

	//************************************
	// Function:  Input
	// Param[in]: decoder	The input UTF8 decoder.
	// Param[in]: byte		The input byte.
	// Return:    void.
	// Remarks:   Inputs a byte.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void Input(FS_UTF8Decoder decoder, FS_BYTE byte);
	
	//************************************
	// Function:  AppendChar
	// Param[in]: decoder	The input UTF8 decoder.
	// Param[in]: ch		The input character.
	// Return:    void.
	// Remarks:   Appends characters to wide text buffer.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void AppendChar(FS_UTF8Decoder decoder, FS_DWORD ch);

	//************************************
	// Function:  ClearStatus
	// Param[in]: decoder	The input UTF8 decoder.
	// Return:    void.
	// Remarks:   Clears the decoding status.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void ClearStatus(FS_UTF8Decoder decoder);

	//************************************
	// Function:  GetResult
	// Param[in]: decoder		The input UTF8 decoder.
	// Param[out]: outResult	It receives the result.
	// Return:    void.
	// Remarks:   Gets the result.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void	GetResult(FS_UTF8Decoder decoder, FS_WideString* outResult);
};


class CFS_UTF8Encoder_V5
{
public:
	//************************************
	// Function:  New
	// Param[in]: void
	// Return:    The UTF8 encoder object.
	// Remarks:   Creates a new UTF8 encoder object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_UTF8Encoder New(void);

	//************************************
	// Function:  Destroy
	// Param[in]: encoder The input UTF8 encoder.
	// Return:    void.
	// Remarks:   Destroys the UTF8 encoder object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void Destroy(FS_UTF8Encoder encoder);

	//************************************
	// Function:  Input
	// Param[in]: encoder The input UTF8 encoder.
	// Param[in]: unicode The input unicode.
	// Return:    void.
	// Remarks:   Inputs a unicode.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void Input(FS_UTF8Encoder encoder, FS_WCHAR unicode);

	//************************************
	// Function:  AppendStr
	// Param[in]: encoder	The input UTF8 encoder.
	// Param[in]: str		A non-buffered byte string.
	// Return:    void.
	// Remarks:   Appends a non-buffered byte string.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void AppendStr(FS_UTF8Encoder encoder, FS_ByteString str);

	//************************************
	// Function:  GetResult
	// Param[in]: encoder	The input UTF8 encoder.
	// Param[out]: str		It receives the result.
	// Return:    void.
	// Remarks:   Gets the result.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void GetResult(FS_UTF8Encoder encoder, FS_ByteString* outResult);

	//************************************
	// Function:  IsUTF8Data
	// Param[in]: pData	The pointer to the data buffer.
	// Param[in,out]: pLen The pointer to the length of the data buffer, in bytes. When this function returns, it stores the number of bytes scanned.
	// Return:    TRUE if all data is UTF-8 encoded, FALSE if the data is not UTF-8 format..
	// Remarks:   Checks whether a data buffer is UTF-8 encoded or not.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.2.2
	//************************************
	static FS_BOOL IsUTF8Data(FS_LPCBYTE pData, FS_INT32* pLen);
};

//////////////////////////////////////////////////////////////////////////
//class CFS_FileStream_V6

class CFS_FileStream_V6
{
public:
	//************************************
	// Function:  New
	// Param[in]: callbacks The input callback set for file stream reading and writing.
	// Return:    The file stream object.
	// Remarks:   Creates the file stream object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.3
	//************************************
	static FS_FileStream New(FS_FileStreamCallbacks callbacks);

	//************************************
	// Function:  New2
	// Param[in]: filename	The input file path.
	// Param[in]: dwModes	The input file mode. See the definition of FSFileMode.
	// Param[in]: reserved	It is a reserved parameter. Must set it as NULL.
	// Return:    The file stream object.
	// Remarks:   Creates the file stream object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.3
	//************************************
	static FS_FileStream New2(FS_LPCWSTR filename, FS_DWORD dwModes, void* reserved);

	//************************************
	// Function:  Destroy
	// Param[in]: fileStream The input file stream object.
	// Return:    void
	// Remarks:   Destroys the file stream object.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.3
	//************************************
	static void Destroy(FS_FileStream fileStream);
};

class CFS_GUID_V7
{
public:
	//************************************
	// Function:  Create
	// Param[out]: pGUID Pointer to store the returned GUID value, cannot be NULL. 
	// Return:    void
	// Remarks:   Creates a GUID, version 4.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1
	//************************************
	static void Create(FS_LPGUID pGUID);

	//************************************
	// Function:  ToString
	// Param[in]: pGUID			Pointer to a valid GUID value, cannot be NULL.
	// Param[out]: outStr		It receives the GUID string.
	// Param[in]: bSeparator	If TRUE, GUID string will include '-' separating characters.
	// Return:    void
	// Remarks:  FormatS GUID to a string.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1
	//************************************
	static void ToString(FS_LPGUID pGUID, FS_ByteString* outStr, FS_BOOL bSeparator);
};

class CFS_UUID_V9
{
public:
	//************************************
	// Function:  Generate
	// Param[out]: outID			It receives the ID.
	// Param[in]: userData			The input user data.
	// Param[in,out]: outLastState	It receives the last state of the ID.
	// Return:    The type of the UUID. See the definition of <a>FSUUIDType</a>.
	// Remarks:   Generates a UUID.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3
	//************************************
	static FS_INT32 Generate(FS_ByteString* outID, FS_INT32 userData, FS_ByteString* outLastState);

	//************************************
	// Function:  GenerateTime
	// Param[out]: outID			It receives the ID.
	// Param[in]: userData			The input user data.
	// Param[in,out]: outLastState	It receives the last state of the ID.
	// Param[in]: bOnlyUid			Whether generates the ID only or not.
	// Return:    The type of the UUID. See the definition of <a>FSUUIDType</a>.
	// Remarks:   Generates a UUID constructed by time.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3
	//************************************
	static FS_INT32	GenerateTime(FS_ByteString* outID, FS_INT32 userData, FS_ByteString* outLastState, FS_BOOL bOnlyUid);

	//************************************
	// Function:  GenerateRandom
	// Param[out]: outID		It receives the ID.
	// Param[in]: userData		The input user data.
	// Return:    The type of the UUID. See the definition of <a>FSUUIDType</a>.
	// Remarks:   Generates a UUID randomly.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3
	//************************************
	static FS_INT32	GenerateRandom(FS_ByteString* outID, FS_INT32 userData);

	//************************************
	// Function:  SetTsPath
	// Param[in]: bsTsPath		The input Ts path.
	// Return:    The type of the UUID. See the definition of <a>FSUUIDType</a>.
	// Remarks:   Sets the Ts path.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3
	//************************************
	static FS_INT32	SetTsPath(FS_ByteString bsTsPath);

	//************************************
	// Function:  SetState
	// Param[in]: bsLastState		The input last state.
	// Return:    The type of the UUID. See the definition of <a>FSUUIDType</a>.
	// Remarks:   Sets the last state.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3
	//************************************
	static FS_INT32	SetState(FS_ByteString bsLastState);

	//************************************
	// Function:  SetUserData
	// Param[in]: bsUserData		The input user data.
	// Return:    The type of the UUID. See the definition of <a>FSUUIDType</a>.
	// Remarks:   Sets the user data.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3
	//************************************
	static FS_INT32	SetUserData(FS_ByteString bsUserData);
};

class CFS_MapByteStringToPtr_V9
{	
public:

	//************************************
	// Function:  New	
	// Param[in]: nBlockSize	The internal block. Sets it 10 as default.
	// Return:    An empty bytestring-to-ptr map.
	// Remarks:   Creates an empty bytestring-to-ptr map with specified block size.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static FS_MapByteStringToPtr New(FS_INT32 nBlockSize);

	//************************************
	// Function:  Destroy
	// Param[in]: map The input bytestring-to-ptr map.
	// Return:    void
	// Remarks:   Destroys the bytestring-to-ptr map.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static void	Destroy(FS_MapByteStringToPtr map);

	//************************************
	// Function:  GetCount	
	// Param[in]: map The input bytestring-to-ptr map.
	// Return:   The number of elements in the map.
	// Remarks:   Gets the number of elements.
	// Notes:   
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static FS_INT32 GetCount(FS_MapByteStringToPtr map);

	//************************************
	// Function:  IsEmpty	
	// Param[in]: map The input bytestring-to-ptr map.
	// Return:    <a>TRUE</a> for map being empty. <a>FALSE</a> otherwise.
	// Remarks:   Tests whether the <param>map</param> is empty.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static FS_BOOL IsEmpty(FS_MapByteStringToPtr map);

	//************************************
	// Function:   Lookup
	// Param[in]:  map		The input bytestring-to-ptr map.
	// Param[in]:  key		The key to lookup.
	// Param[out]: outValue A ref of a typeless pointer to receive the found value.
	// Return:     <a>TRUE</a> for value being found. <a>FALSE</a> otherwise.
	// Remarks:    Looks up by a key.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static FS_BOOL Lookup(FS_MapByteStringToPtr map, FS_ByteString key, void** outValue);
	
	//************************************
	// Function:  SetAt	
	// Param[in]: map			The input bytestring-to-ptr map.
	// Param[in]: key			The key to specify a position.
	// Param[in]: newValue		The new value.
	// Return:    void
	// Remarks:   Adds a new (key, value) pair. Adds if not exist, otherwise modify.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static void SetAt(FS_MapByteStringToPtr map, FS_ByteString key, void* newValue);

	//************************************
	// Function:  RemoveKey	
	// Param[in]: map The input bytestring-to-ptr map.
	// Param[in]: key The key to remove.
	// Return:    <a>TRUE</a> for success, otherwise <a>FALSE</a>.
	// Remarks:   Removes existing (key, value) pair.
	// Notes:     
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static FS_BOOL RemoveKey(FS_MapByteStringToPtr map, FS_ByteString key);
	
	//************************************
	// Function:  RemoveAll	
	// Param[in]: map The input bytestring-to-ptr map.
	// Return:    void
	// Remarks:   Removes all the (key, value) pairs in the map.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static void RemoveAll(FS_MapByteStringToPtr map);

	//************************************
	// Function:  GetStartPosition	
	// Param[in]: map The input bytestring-to-ptr map.
	// Return:    The first key-value pair position in the map
	// Remarks:   Gets the first key-value pair position, iterating all (key, value) pairs.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static FS_POSITION GetStartPosition(FS_MapByteStringToPtr map);
	
	//************************************
	// Function:      GetNextAssoc
	// Param[in]:     map					The input bytestring-to-ptr map.
	// Param[in,out]: inOutNextPosition		Input a position, and receive the next association position.
	// Param[out]:    outKey				(Filled by this method)Receive a key.
	// Param[out]:    outValue				(Filled by this method)Receive a value.
	// Return:        void
	// Remarks:       Gets the current association and sets the position to next association.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static void GetNextAssoc(FS_MapByteStringToPtr map, FS_POSITION* inOutNextPosition, FS_ByteString* outKey, void** outValue);

	//************************************
	// Function:      GetNextValue
	// Param[in]:     map					The input bytestring-to-ptr map.
	// Param[in,out]: inOutNextPosition		Input a position, and receive the next association position.
	// Return:        The value.
	// Remarks:       Gets the current value and sets the position to next association.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static FS_LPVOID GetNextValue(FS_MapByteStringToPtr map, FS_POSITION* inOutNextPosition);

	//************************************
	// Function:  GetHashTableSize
	// Param[in]: map		The input bytestring-to-ptr map.
	// Return:    The hash table size.
	// Remarks:   Gets the internal hash table size. Advanced features for derived classes.
	// Notes:    
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static FS_DWORD GetHashTableSize(FS_MapByteStringToPtr map);
	
	//************************************
	// Function:  InitHashTable	
	// Param[in]: map			The input bytestring-to-ptr map.
	// Param[in]: hashSize		Initializes the hash table size.
	// Param[in]: bAllocNow		Does it Now allocate the hash table? No-zero means yes, otherwise not. Sets it TRUE as default.
	// Return:    void
	// Remarks:   Initializes the hash table.
	// Notes:     
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static void InitHashTable(FS_MapByteStringToPtr map, FS_DWORD hashSize, FS_BOOL bAllocNow);

	//************************************
	// Function:  HashKey	
	// Param[in]: map		The input bytestring-to-ptr map.
	// Param[in]: key		The key used to produce the hash key.
	// Return:    A hash value.
	// Remarks:   Routine used to user-provided hash keys.
	// Notes: Overwrite-able: special non-virtual (see map implementation for details).
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	//************************************
	static FS_DWORD HashKey(FS_MapByteStringToPtr map, FS_ByteString key);
};

class CFS_Image_V12
{
public:

	//************************************
	// Function:	Create
	// Return:		A <a>FS_Image</a> handler.
	// Remarks:		Need release by <a>FSImageRelease</a>.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7.1
	//************************************
	static FS_Image Create();

	static void Release(FS_Image image);

	static FS_HImage Load(FS_Image image, FS_FileReadHandler readHandler);

	static FSCODEC_IMAGE_TYPE GetType(FS_Image image, FS_HImage hImage);
	
	static void EnableTransparent(FS_Image image, FS_HImage hImage, FS_BOOL bEnable);

	static void	SetBackgroundColor(FS_Image image, FS_HImage hImage, FS_ARGB argb);

	static void	SetOpacity(FS_Image image, FS_HImage hImage, FS_BYTE nOpacity);

	static FS_INT32	CountFrames(FS_Image image, FS_HImage hImage);

	static FS_BOOL LoadFrame(FS_Image image, FS_HImage hImage, FS_INT32 iFrame);

	static FS_BOOL NeedLoadPrevFrame(FS_Image image, FS_HImage hImage, FS_INT32 iFrame);

	static FS_BOOL GetFrameSize(FS_Image image, FS_HImage hImage, FS_INT32* poutWidth, FS_INT32* poutHeight);

	static FS_DIBitmap GetFrame(FS_Image image, FS_HImage hImage);

	static FS_BOOL GetFrameColorKey(FS_Image image, FS_HImage hImage, FS_INT32* poutColorKey);

	static FS_DIBitmap GetFrameWithTransparency(FS_Image image, FS_HImage hImage);

	static FS_BOOL GetFrameRawData(FS_Image image, FS_HImage hImage, FS_INT32 iFrame, FS_LPBYTE *pData, FS_UINT *pSize);

	static FS_INT32 GetFrameCompress(FS_Image image, FS_HImage hImage, FS_INT32 iFrame);

	static FS_INT32	GetFramePhotoInterpret(FS_Image image, FS_HImage hImage, FS_INT32 iFrame);

	static void	Free(FS_Image image, FS_HImage hImage);

	static FS_DIBAttribute	GetAttribute(FS_Image image, FS_HImage hImage);

	static FS_INT32	GetWidth(FS_Image image, FS_HImage hImage);

	static FS_INT32 GetHeight(FS_Image image, FS_HImage hImage);

	static FPD_PageObject CreateObject(FPD_Document pPDFDoc, FS_HImage hImage, FS_INT32 iFrame,
		FS_AffineMatrix *pMatrix, FS_BOOL bAutoRotate);

	static FPD_PageObject InsertToPDFPage(void *pPDFPageObjects, FS_POSITION posInsert,
		FS_HImage hImage, FS_INT32 iFrame, FS_AffineMatrix *pMatrix, FS_BOOL bAutoRotate);

	static FS_INT32 InsertToPDFDocument(FPD_Document pPDFDoc, FS_INT32 iPageStart,
		FS_HImage hImage, FS_INT32 iFrameStart, FS_INT32 iFrameCount, FS_BOOL bAutoRotate);
};


class CFS_ReaderDateTime_V13
{
public:
	static FS_ReaderDateTime CreateDateTimeDefault();
	static FS_ReaderDateTime CreateDateTimeStr(const FS_LPCSTR dtStr, FS_BOOL bUseCurrent);
	static FS_ReaderDateTime CreateDateTime(FS_ReaderDateTime datetime);
	static FS_ReaderDateTime CreateDateTimeSysTime(const SYSTEMTIME* st);
	static FS_ReaderDateTime	FromPDFDateTimeString(FS_ReaderDateTime datetime, const FS_LPCSTR& dtStr);	
	static void		ToCommonDateTimeString(FS_ReaderDateTime datetime, FS_ByteString* bsOutStr);
	static void		ToPDFDateTimeString(FS_ReaderDateTime datetime, FS_ByteString* bsOutStr);
	static FS_BOOL				ToSystemTime(FS_ReaderDateTime datetime,SYSTEMTIME* st);
	static void	ToGMT(FS_ReaderDateTime datetime, FS_ReaderDateTime* retTime);
	static void	ToLocalTime(FS_ReaderDateTime datetime, FS_ReaderDateTime* retTime);
	static void	AddDays(FS_ReaderDateTime datetime,FS_SHORT days, FS_ReaderDateTime* retTime);
	static void	AddSeconds(FS_ReaderDateTime datetime,FS_INT32 seconds, FS_ReaderDateTime* retTime);
	static FS_BOOL				IsEmpty(FS_ReaderDateTime datetime);
	static void				ResetDateTime(FS_ReaderDateTime datetime);
	static FS_BOOL				IsLeapYear(FS_ReaderDateTime datetime, FS_INT32 year);
	static FS_BOOL				IsValid(FS_ReaderDateTime datetime, FS_INT32 year, FS_INT32 month, FS_INT32 day);
	static void	FillDateTime (FS_ReaderDateTime datetime,const FS_ReaderDateTime filldatetime);
	static void	FillSystemtime (FS_ReaderDateTime datetime,SYSTEMTIME& st);
	static FS_BOOL				IsEqual (FS_ReaderDateTime datetime, FS_ReaderDateTime& datetimeMatch);
	static FS_BOOL				IsUnEqual (FS_ReaderDateTime datetime, FS_ReaderDateTime& datetimeMatch);
	static FS_BOOL				IsGreater (FS_ReaderDateTime datetime, FS_ReaderDateTime& datetimeMatch);
	static FS_BOOL				IsEqualGreater (FS_ReaderDateTime datetime, FS_ReaderDateTime& datetimeMatch);
	static FS_BOOL				IsLess (FS_ReaderDateTime datetime, FS_ReaderDateTime& datetimeMatch);
	static FS_BOOL				IsEqualLess (FS_ReaderDateTime datetime, FS_ReaderDateTime& datetimeMatch);
	static void Destroy(FS_ReaderDateTime datetime);
};

class CFS_Monoscale_V13
{
public:
	static FS_Monoscale New();
	static void Destroy(FS_Monoscale monoscale);
	static FS_BOOL LoadDIBSource(FS_Monoscale monoscale, FS_DIBitmap bitmap);
	static FS_LPVOID	GetMonoscaleBitmap(FS_Monoscale monoscale, FSMONOALRITHEMETIC mode);
};


class CFS_FloatArray_V13
{

public:
	//************************************
	// Function:  New	
	// Param[in]: void
	// Return:    A new empty pointer array.
	// Remarks:   Creates a new empty pointer array.
	// Notes:
	//************************************
	static FS_FloatArray New(void);


	//************************************
	// Function:  Destroy	
	// Param[in]: arr		The input pointer array.
	// Return:    void
	// Remarks:   Destroys a pointer array.
	// Notes:
	//************************************
	static void Destroy(FS_FloatArray arr);

	//************************************
	// Function:  GetSize	
	// Param[in]: arr		The input pointer array.
	// Return:    The number of elements in the array.
	// Remarks:   Gets the number of elements in the array.
	// Notes:
	//************************************
	static FS_INT32	GetSize(FS_FloatArray arr);


	//************************************
	// Function:  GetUpperBound	
	// Param[in]: arr		The input pointer array.
	// Return:    The upper bound.
	// Remarks:   Gets the upper bound in the array, actually the maximum valid index.
	// Notes:
	//************************************
	static FS_INT32	GetUpperBound(FS_FloatArray arr);


	//************************************
	// Function:  SetSize
	// Param[in]: arr		 The input pointer array.
	// Param[in]: nNewSize	 The new size in elements expected.
	// Param[in]: nGrowBy	 The grow amount in elements expected, nGrowBy can be -1 for the grow amount unchanged.
	// Return:    void
	// Remarks:   Changes the allocated size and the growing amount.
	// Notes:
	//************************************
	static void	SetSize(FS_FloatArray arr, FS_INT32 nNewSize, FS_INT32 nGrowBy);


	//************************************
	// Function:  RemoveAll	
	// Param[in]: arr		The input pointer array.
	// Return:    void
	// Remarks:   Cleans up the array.
	// Notes:
	//************************************
	static void	RemoveAll(FS_FloatArray arr);


	//************************************
	// Function:  GetAt	
	// Param[in]: arr		The input pointer array.
	// Param[in]: index		Specifies the zero-based index of the element.
	// Return:    An element.
	// Remarks:	  Retrieves an element specified by an index number.
	// Notes:
	//************************************
	static FS_FLOAT GetAt(FS_FloatArray arr, FS_INT32 index);


	//************************************
	// Function:  SetAt
	// Param[in]: arr		The input pointer array.
	// Param[in]: index		Specifies the zero-based index of the element.
	// Param[in]: newItem	An element
	// Return:    void
	// Remarks:	  Overwrites an element specified by an index number.
	// Notes:
	//************************************
	static void SetAt(FS_FloatArray arr, FS_INT32 index, FS_FLOAT newItem);


	//************************************
	// Function:  SetAtGrow
	// Param[in]: arr		The input pointer array.
	// Param[in]: index		Specifies the zero-based index of element in the array.
	// Param[in]: newItem	The input element.
	// Return:    void
	// Remarks:	  Sets an element value at specified position. Potentially grow the array.
	// Notes:
	//************************************
	static void SetAtGrow(FS_FloatArray arr, FS_INT32 index, FS_FLOAT newItem);


	//************************************
	// Function:  Add
	// Param[in]: arr		The input pointer array.
	// Param[in]: newItem	The input element.
	// Return:    The added element's index in the array.
	// Remarks:   Adds an element at the tail. Potentially grow the array.
	// Notes:    
	//************************************
	static FS_INT32 Add(FS_FloatArray arr, FS_FLOAT newItem);


	//************************************
	// Function:  Append	
	// Param[in]: arr		The input pointer array.
	// Param[in]: srcArr	The input array.
	// Return:    The old size in elements.
	// Remarks:	  Appends an array.
	// Notes:
	//************************************
	static FS_INT32 Append(FS_FloatArray arr, const FS_FloatArray srcArr);


	//************************************
	// Function:  Copy	
	// Param[in]: arr		The input pointer array.
	// Param[in]: srcArr	The input array.
	// Return:    void
	// Remarks:   Copies from an array.
	// Notes:
	//************************************
	static void	Copy(FS_FloatArray arr, FS_FloatArray srcArr);

	//************************************
	// Function:    GetDataPtr
	// Param[in]: arr		The input pointer array.
	// Param[in]: index		Specifies the zero-based index of element in the array.
	// Return:    A pointer to the specified element.
	// Remarks:   Gets a pointer to the specified element in the array. Direct pointer access.
	// Notes:
	//************************************
	static FS_FLOAT* GetDataPtr(FS_FloatArray arr, FS_INT32 index);

	//************************************
	// Function:  InsertAt	
	// Param[in]: arr			The input pointer array.
	// Param[in]: index			Specifies the zero-based index in the array.
	// Param[in]: newItem	    Specifies the element value to insert.
	// Param[in]: nCount		Specifies the count of the element to insert.
	// Return:    void
	// Remarks:   Inserts one or more continuous element at specified position.
	// Notes:
	//************************************
	static void InsertAt(FS_FloatArray arr, FS_INT32 index, FS_FLOAT newItem, FS_INT32 nCount);


	//************************************
	// Function:  RemoveAt
	// Param[in]: arr		The input pointer array.
	// Param[in]: index		Specifies the zero-based index in the array.
	// Param[in]: nCount	Specifies the count of element to remove.
	// Return:    void
	// Remarks:   Removes a number of elements at specified position.
	// Notes:     
	//************************************
	static void	RemoveAt(FS_FloatArray arr, FS_INT32 index, FS_INT32 nCount);


	//************************************
	// Function:  InsertNewArrayAt		
	// Param[in]: arr			The input pointer array.
	// Param[in]: nStartIndex	Specifies the zero-based index of start element to insert at.
	// Param[in]: newArray		The input array.
	// Return:    void
	// Remarks:   Inserts an array at specified position.
	// Notes:
	//************************************
	static void	InsertNewArrayAt(FS_FloatArray arr, FS_INT32 nStartIndex, FS_FloatArray newArray);


	//************************************
	// Function:  Find	   
	// Param[in]: arr			The input pointer array.
	// Param[in]: item			The input element.
	// Param[in]: nStartIndex	Specifies the zero-based index of start element to find.
	// Return:	  An index of the found element. It can be -1 for finding none.
	// Remarks:   Finds an element from specified position to last
	// Notes:
	//************************************
	static FS_INT32 Find(FS_FloatArray arr, FS_FLOAT item, FS_INT32 nStartIndex);
};

#ifdef __cplusplus
};
#endif//__cplusplus

#endif//FR_BASIC_H
