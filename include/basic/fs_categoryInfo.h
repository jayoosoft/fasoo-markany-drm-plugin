﻿/*****************************************************************************

Copyright (C) 2010 Foxit Corporation
All rights reserved.

NOTICE: Foxit permits you to use, modify, and distribute this file
in accordance with the terms of the Foxit license agreement
accompanying it. If you have received this file from a source other
than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
 
*****************************************************************************/

#ifndef FS_CATEGORIESINFO_H
#define FS_CATEGORIESINFO_H

#include "fs_funcTabInitiator.h"

typedef struct __FRCategoryInfo__
{
	int categorySEL;
	int interfaceNum;
	void (*lpfnInitFuncMappingTable)(void* pMgr, bool bNeedInitParent);
}FRCategoryInfo,*PFRCategoryInfo;

// pay attention to the orders of this array. The orders can not be changed.
FRCategoryInfo g_CategoryInfoList[CoreCategoriesNum + 1] = 
{

	#ifndef FX_READER_DLL

//----------_V1----------
// In file fs_basicImpl.h
	{ FSExtensionHFTMgrSEL, FSExtensionHFTMgrInterfacesNum,  CFS_ExtensionHFTMgr_V1_Initiator::InitFuncMappingTable },
	{ FSAffineMatrixSEL, FSAffineMatrixInterfacesNum,  CFS_AffineMatrix_V1_Initiator::InitFuncMappingTable },
	{ FSDIBitmapSEL, FSDIBitmapInterfacesNum,  CFS_DIBitmap_V1_Initiator::InitFuncMappingTable },
	{ FSMapPtrToPtrSEL, FSMapPtrToPtrInterfacesNum,  CFS_MapPtrToPtr_V1_Initiator::InitFuncMappingTable },
	{ FSPtrArraySEL, FSPtrArrayInterfacesNum,  CFS_PtrArray_V1_Initiator::InitFuncMappingTable },
	{ FSByteArraySEL, FSByteArrayInterfacesNum,  CFS_ByteArray_V1_Initiator::InitFuncMappingTable },
	{ FSWordArraySEL, FSWordArrayInterfacesNum,  CFS_WordArray_V1_Initiator::InitFuncMappingTable },
	{ FSDWordArraySEL, FSDWordArrayInterfacesNum,  CFS_DWordArray_V1_Initiator::InitFuncMappingTable },
	{ FSByteStringArraySEL, FSByteStringArrayInterfacesNum,  CFS_ByteStringArray_V1_Initiator::InitFuncMappingTable },
	{ FSWideStringArraySEL, FSWideStringArrayInterfacesNum,  CFS_WideStringArray_V1_Initiator::InitFuncMappingTable },
	{ FSCodeTransformationSEL, FSCodeTransformationInterfacesNum,  CFS_CodeTransformation_V1_Initiator::InitFuncMappingTable },
	{ FSFloatRectArraySEL, FSFloatRectArrayInterfacesNum,  CFS_FloatRectArray_V1_Initiator::InitFuncMappingTable },
	{ FSBinaryBufSEL, FSBinaryBufInterfacesNum,  CFS_BinaryBuf_V1_Initiator::InitFuncMappingTable },
	{ FSPauseHandlerSEL, FSPauseHandlerInterfacesNum,  CFS_PauseHandler_V1_Initiator::InitFuncMappingTable },
	{ FSFileReadHandlerSEL, FSFileReadHandlerInterfacesNum,  CFS_FileReadHandler_V1_Initiator::InitFuncMappingTable },
	{ FSStreamWriteHandlerSEL, FSStreamWriteHandlerInterfacesNum,  CFS_StreamWriteHandler_V1_Initiator::InitFuncMappingTable },
// fs_basicImpl.h end

// In file fs_stringImpl.h
	{ FSCharMapSEL, FSCharMapInterfacesNum,  CFS_CharMap_V1_Initiator::InitFuncMappingTable },
	{ FSByteStringSEL, FSByteStringInterfacesNum,  CFS_ByteString_V1_Initiator::InitFuncMappingTable },
	{ FSWideStringSEL, FSWideStringInterfacesNum,  CFS_WideString_V1_Initiator::InitFuncMappingTable },
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
	{ FRToolSEL, FRToolInterfacesNum,  CFR_Tool_V1_Initiator::InitFuncMappingTable },
	{ FRAppSEL, FRAppInterfacesNum,  CFR_App_V1_Initiator::InitFuncMappingTable },
	{ FRLanguageSEL, FRLanguageInterfacesNum,  CFR_Language_V1_Initiator::InitFuncMappingTable },
	{ FRUIProgressSEL, FRUIProgressInterfacesNum,  CFR_UIProgress_V1_Initiator::InitFuncMappingTable },
// fr_appImpl.h end

// In file fr_barImpl.h
	{ FRToolButtonSEL, FRToolButtonInterfacesNum,  CFR_ToolButton_V1_Initiator::InitFuncMappingTable },
	{ FRToolBarSEL, FRToolBarInterfacesNum,  CFR_ToolBar_V1_Initiator::InitFuncMappingTable },
	{ FRMessageBarSEL, FRMessageBarInterfacesNum,  CFR_MessageBar_V1_Initiator::InitFuncMappingTable },
// fr_barImpl.h end

// In file fr_docImpl.h
	{ FRDocSEL, FRDocInterfacesNum,  CFR_Doc_V1_Initiator::InitFuncMappingTable },
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
	{ FRMenuBarSEL, FRMenuBarInterfacesNum,  CFR_MenuBar_V1_Initiator::InitFuncMappingTable },
	{ FRMenuSEL, FRMenuInterfacesNum,  CFR_Menu_V1_Initiator::InitFuncMappingTable },
	{ FRMenuItemSEL, FRMenuItemInterfacesNum,  CFR_MenuItem_V1_Initiator::InitFuncMappingTable },
// fr_menuImpl.h end

// In file fr_sysImpl.h
	{ FRSysSEL, FRSysInterfacesNum,  CFR_Sys_V1_Initiator::InitFuncMappingTable },
// fr_sysImpl.h end

// In file fr_viewImpl.h
	{ FRDocViewSEL, FRDocViewInterfacesNum,  CFR_DocView_V1_Initiator::InitFuncMappingTable },
	{ FRPageViewSEL, FRPageViewInterfacesNum,  CFR_PageView_V1_Initiator::InitFuncMappingTable },
	{ FRTextSelectToolSEL, FRTextSelectToolInterfacesNum,  CFR_TextSelectTool_V1_Initiator::InitFuncMappingTable },
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
	{ FPDDocSEL, FPDDocInterfacesNum,  CFPD_Doc_V1_Initiator::InitFuncMappingTable },
	{ FPDNameTreeSEL, FPDNameTreeInterfacesNum,  CFPD_NameTree_V1_Initiator::InitFuncMappingTable },
	{ FPDBookmarkSEL, FPDBookmarkInterfacesNum,  CFPD_Bookmark_V1_Initiator::InitFuncMappingTable },
	{ FPDDestSEL, FPDDestInterfacesNum,  CFPD_Dest_V1_Initiator::InitFuncMappingTable },
	{ FPDOCContextSEL, FPDOCContextInterfacesNum,  CFPD_OCContext_V1_Initiator::InitFuncMappingTable },
	{ FPDOCGroupSEL, FPDOCGroupInterfacesNum,  CFPD_OCGroup_V1_Initiator::InitFuncMappingTable },
	{ FPDOCGroupSetSEL, FPDOCGroupSetInterfacesNum,  CFPD_OCGroupSet_V1_Initiator::InitFuncMappingTable },
	{ FPDOCNotifySEL, FPDOCNotifyInterfacesNum,  CFPD_OCNotify_V1_Initiator::InitFuncMappingTable },
	{ FPDOCPropertiesSEL, FPDOCPropertiesInterfacesNum,  CFPD_OCProperties_V1_Initiator::InitFuncMappingTable },
	{ FPDLWinParamSEL, FPDLWinParamInterfacesNum,  CFPD_LWinParam_V1_Initiator::InitFuncMappingTable },
	{ FPDActionFieldsSEL, FPDActionFieldsInterfacesNum,  CFPD_ActionFields_V1_Initiator::InitFuncMappingTable },
	{ FPDActionSEL, FPDActionInterfacesNum,  CFPD_Action_V1_Initiator::InitFuncMappingTable },
	{ FPDAActionSEL, FPDAActionInterfacesNum,  CFPD_AAction_V1_Initiator::InitFuncMappingTable },
	{ FPDDocJSActionsSEL, FPDDocJSActionsInterfacesNum,  CFPD_DocJSActions_V1_Initiator::InitFuncMappingTable },
	{ FPDFileSpecSEL, FPDFileSpecInterfacesNum,  CFPD_FileSpec_V1_Initiator::InitFuncMappingTable },
	{ FPDMediaPlayerSEL, FPDMediaPlayerInterfacesNum,  CFPD_MediaPlayer_V1_Initiator::InitFuncMappingTable },
	{ FPDRenditionSEL, FPDRenditionInterfacesNum,  CFPD_Rendition_V1_Initiator::InitFuncMappingTable },
	{ FPDLinkSEL, FPDLinkInterfacesNum,  CFPD_Link_V1_Initiator::InitFuncMappingTable },
	{ FPDAnnotSEL, FPDAnnotInterfacesNum,  CFPD_Annot_V1_Initiator::InitFuncMappingTable },
	{ FPDAnnotListSEL, FPDAnnotListInterfacesNum,  CFPD_AnnotList_V1_Initiator::InitFuncMappingTable },
	{ FPDDefaultAppearanceSEL, FPDDefaultAppearanceInterfacesNum,  CFPD_DefaultAppearance_V1_Initiator::InitFuncMappingTable },
	{ FPDFormNotifySEL, FPDFormNotifyInterfacesNum,  CFPD_FormNotify_V1_Initiator::InitFuncMappingTable },
	{ FPDInterFormSEL, FPDInterFormInterfacesNum,  CFPD_InterForm_V1_Initiator::InitFuncMappingTable },
	{ FPDFormFieldSEL, FPDFormFieldInterfacesNum,  CFPD_FormField_V1_Initiator::InitFuncMappingTable },
	{ FPDIconFitSEL, FPDIconFitInterfacesNum,  CFPD_IconFit_V1_Initiator::InitFuncMappingTable },
	{ FPDFormControlSEL, FPDFormControlInterfacesNum,  CFPD_FormControl_V1_Initiator::InitFuncMappingTable },
	{ FPDFDFDocSEL, FPDFDFDocInterfacesNum,  CFPD_FDFDoc_V1_Initiator::InitFuncMappingTable },
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
	{ FPDObjectSEL, FPDObjectInterfacesNum,  CFPD_Object_V1_Initiator::InitFuncMappingTable },
	{ FPDBooleanSEL, FPDBooleanInterfacesNum,  CFPD_Boolean_V1_Initiator::InitFuncMappingTable },
	{ FPDNumberSEL, FPDNumberInterfacesNum,  CFPD_Number_V1_Initiator::InitFuncMappingTable },
	{ FPDStringSEL, FPDStringInterfacesNum,  CFPD_String_V1_Initiator::InitFuncMappingTable },
	{ FPDNameSEL, FPDNameInterfacesNum,  CFPD_Name_V1_Initiator::InitFuncMappingTable },
	{ FPDArraySEL, FPDArrayInterfacesNum,  CFPD_Array_V1_Initiator::InitFuncMappingTable },
	{ FPDDictionarySEL, FPDDictionaryInterfacesNum,  CFPD_Dictionary_V1_Initiator::InitFuncMappingTable },
	{ FPDStreamSEL, FPDStreamInterfacesNum,  CFPD_Stream_V1_Initiator::InitFuncMappingTable },
	{ FPDStreamAccSEL, FPDStreamAccInterfacesNum,  CFPD_StreamAcc_V1_Initiator::InitFuncMappingTable },
	{ FPDStreamFilterSEL, FPDStreamFilterInterfacesNum,  CFPD_StreamFilter_V1_Initiator::InitFuncMappingTable },
	{ FPDNullSEL, FPDNullInterfacesNum,  CFPD_Null_V1_Initiator::InitFuncMappingTable },
	{ FPDReferenceSEL, FPDReferenceInterfacesNum,  CFPD_Reference_V1_Initiator::InitFuncMappingTable },
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
	{ FPDPageSEL, FPDPageInterfacesNum,  CFPD_Page_V1_Initiator::InitFuncMappingTable },
	{ FPDParseOptionsSEL, FPDParseOptionsInterfacesNum,  CFPD_ParseOptions_V1_Initiator::InitFuncMappingTable },
	{ FPDFormSEL, FPDFormInterfacesNum,  CFPD_Form_V1_Initiator::InitFuncMappingTable },
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
	{ FPDPathSEL, FPDPathInterfacesNum,  CFPD_Path_V1_Initiator::InitFuncMappingTable },
	{ FPDClipPathSEL, FPDClipPathInterfacesNum,  CFPD_ClipPath_V1_Initiator::InitFuncMappingTable },
	{ FPDColorStateSEL, FPDColorStateInterfacesNum,  CFPD_ColorState_V1_Initiator::InitFuncMappingTable },
	{ FPDTextStateSEL, FPDTextStateInterfacesNum,  CFPD_TextState_V1_Initiator::InitFuncMappingTable },
	{ FPDGeneralStateSEL, FPDGeneralStateInterfacesNum,  CFPD_GeneralState_V1_Initiator::InitFuncMappingTable },
	{ FPDGraphStateSEL, FPDGraphStateInterfacesNum,  CFPD_GraphState_V1_Initiator::InitFuncMappingTable },
	{ FPDPageObjectSEL, FPDPageObjectInterfacesNum,  CFPD_PageObject_V1_Initiator::InitFuncMappingTable },
	{ FPDTextObjectSEL, FPDTextObjectInterfacesNum,  CFPD_TextObject_V1_Initiator::InitFuncMappingTable },
	{ FPDPathObjectSEL, FPDPathObjectInterfacesNum,  CFPD_PathObject_V1_Initiator::InitFuncMappingTable },
	{ FPDImageObjectSEL, FPDImageObjectInterfacesNum,  CFPD_ImageObject_V1_Initiator::InitFuncMappingTable },
	{ FPDShadingObjectSEL, FPDShadingObjectInterfacesNum,  CFPD_ShadingObject_V1_Initiator::InitFuncMappingTable },
	{ FPDFormObjectSEL, FPDFormObjectInterfacesNum,  CFPD_FormObject_V1_Initiator::InitFuncMappingTable },
	{ FPDInlineImagesSEL, FPDInlineImagesInterfacesNum,  CFPD_InlineImages_V1_Initiator::InitFuncMappingTable },
	{ FPDContentMarkItemSEL, FPDContentMarkItemInterfacesNum,  CFPD_ContentMarkItem_V1_Initiator::InitFuncMappingTable },
	{ FPDContentMarkSEL, FPDContentMarkInterfacesNum,  CFPD_ContentMark_V1_Initiator::InitFuncMappingTable },
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
	{ FPDParserSEL, FPDParserInterfacesNum,  CFPD_Parser_V1_Initiator::InitFuncMappingTable },
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
	{ FPDRenderOptionsSEL, FPDRenderOptionsInterfacesNum,  CFPD_RenderOptions_V1_Initiator::InitFuncMappingTable },
	{ FPDRenderContextSEL, FPDRenderContextInterfacesNum,  CFPD_RenderContext_V1_Initiator::InitFuncMappingTable },
	{ FPDProgressiveRenderSEL, FPDProgressiveRenderInterfacesNum,  CFPD_ProgressiveRender_V1_Initiator::InitFuncMappingTable },
	{ FPDRenderDeviceSEL, FPDRenderDeviceInterfacesNum,  CFPD_RenderDevice_V1_Initiator::InitFuncMappingTable },
	{ FPDFxgeDeviceSEL, FPDFxgeDeviceInterfacesNum,  CFPD_FxgeDevice_V1_Initiator::InitFuncMappingTable },
	{ FPDWindowsDeviceSEL, FPDWindowsDeviceInterfacesNum,  CFPD_WindowsDevice_V1_Initiator::InitFuncMappingTable },
	{ FPDPageRenderCacheSEL, FPDPageRenderCacheInterfacesNum,  CFPD_PageRenderCache_V1_Initiator::InitFuncMappingTable },
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
	{ FPDFontSEL, FPDFontInterfacesNum,  CFPD_Font_V1_Initiator::InitFuncMappingTable },
	{ FPDFontEncodingSEL, FPDFontEncodingInterfacesNum,  CFPD_FontEncoding_V1_Initiator::InitFuncMappingTable },
	{ FPDType1FontSEL, FPDType1FontInterfacesNum,  CFPD_Type1Font_V1_Initiator::InitFuncMappingTable },
	{ FPDTrueTypeFontSEL, FPDTrueTypeFontInterfacesNum,  CFPD_TrueTypeFont_V1_Initiator::InitFuncMappingTable },
	{ FPDType3CharSEL, FPDType3CharInterfacesNum,  CFPD_Type3Char_V1_Initiator::InitFuncMappingTable },
	{ FPDType3FontSEL, FPDType3FontInterfacesNum,  CFPD_Type3Font_V1_Initiator::InitFuncMappingTable },
	{ FPDCIDFontSEL, FPDCIDFontInterfacesNum,  CFPD_CIDFont_V1_Initiator::InitFuncMappingTable },
	{ FPDCIDUtilSEL, FPDCIDUtilInterfacesNum,  CFPD_CIDUtil_V1_Initiator::InitFuncMappingTable },
	{ FPDColorSpaceSEL, FPDColorSpaceInterfacesNum,  CFPD_ColorSpace_V1_Initiator::InitFuncMappingTable },
	{ FPDColorSEL, FPDColorInterfacesNum,  CFPD_Color_V1_Initiator::InitFuncMappingTable },
	{ FPDPatternSEL, FPDPatternInterfacesNum,  CFPD_Pattern_V1_Initiator::InitFuncMappingTable },
	{ FPDTilingPatternSEL, FPDTilingPatternInterfacesNum,  CFPD_TilingPattern_V1_Initiator::InitFuncMappingTable },
	{ FPDShadingPatternSEL, FPDShadingPatternInterfacesNum,  CFPD_ShadingPattern_V1_Initiator::InitFuncMappingTable },
	{ FPDMeshStreamSEL, FPDMeshStreamInterfacesNum,  CFPD_MeshStream_V1_Initiator::InitFuncMappingTable },
	{ FPDImageSEL, FPDImageInterfacesNum,  CFPD_Image_V1_Initiator::InitFuncMappingTable },
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
	{ FPDObjArchiveSaverSEL, FPDObjArchiveSaverInterfacesNum,  CFPD_ObjArchiveSaver_V1_Initiator::InitFuncMappingTable },
	{ FPDObjArchiveLoaderSEL, FPDObjArchiveLoaderInterfacesNum,  CFPD_ObjArchiveLoader_V1_Initiator::InitFuncMappingTable },
	{ FPDPageArchiveSaverSEL, FPDPageArchiveSaverInterfacesNum,  CFPD_PageArchiveSaver_V1_Initiator::InitFuncMappingTable },
	{ FPDPageArchiveLoaderSEL, FPDPageArchiveLoaderInterfacesNum,  CFPD_PageArchiveLoader_V1_Initiator::InitFuncMappingTable },
	{ FPDCreatorSEL, FPDCreatorInterfacesNum,  CFPD_Creator_V1_Initiator::InitFuncMappingTable },
// fpd_serialImpl.h end

// In file fpd_textImpl.h
	{ FPDProgressiveSearchSEL, FPDProgressiveSearchInterfacesNum,  CFPD_ProgressiveSearch_V1_Initiator::InitFuncMappingTable },
	{ FPDTextPageSEL, FPDTextPageInterfacesNum,  CFPD_TextPage_V1_Initiator::InitFuncMappingTable },
	{ FPDTextPageFindSEL, FPDTextPageFindInterfacesNum,  CFPD_TextPageFind_V1_Initiator::InitFuncMappingTable },
	{ FPDLinkExtractSEL, FPDLinkExtractInterfacesNum,  CFPD_LinkExtract_V1_Initiator::InitFuncMappingTable },
// fpd_textImpl.h end

//----------_V2----------
// In file fs_basicImpl.h
	{ FSBase64EncoderSEL, FSBase64EncoderInterfacesNum,  CFS_Base64Encoder_V2_Initiator::InitFuncMappingTable },
	{ FSBase64DecoderSEL, FSBase64DecoderInterfacesNum,  CFS_Base64Decoder_V2_Initiator::InitFuncMappingTable },
	{ FSFileWriteHandlerSEL, FSFileWriteHandlerInterfacesNum,  CFS_FileWriteHandler_V2_Initiator::InitFuncMappingTable },
	{ FSXMLElementSEL, FSXMLElementInterfacesNum,  CFS_XMLElement_V2_Initiator::InitFuncMappingTable },
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
	{ FDRMCategoryReadSEL, FDRMCategoryReadInterfacesNum,  CFDRM_CategoryRead_V2_Initiator::InitFuncMappingTable },
	{ FDRMCategoryWriteSEL, FDRMCategoryWriteInterfacesNum,  CFDRM_CategoryWrite_V2_Initiator::InitFuncMappingTable },
	{ FDRMDescDataSEL, FDRMDescDataInterfacesNum,  CFDRM_DescData_V2_Initiator::InitFuncMappingTable },
	{ FDRMScriptDataSEL, FDRMScriptDataInterfacesNum,  CFDRM_ScriptData_V2_Initiator::InitFuncMappingTable },
	{ FDRMPresentationDataSEL, FDRMPresentationDataInterfacesNum,  CFDRM_PresentationData_V2_Initiator::InitFuncMappingTable },
	{ FDRMSignatureDataSEL, FDRMSignatureDataInterfacesNum,  CFDRM_SignatureData_V2_Initiator::InitFuncMappingTable },
	{ FDRMDescReadSEL, FDRMDescReadInterfacesNum,  CFDRM_DescRead_V2_Initiator::InitFuncMappingTable },
	{ FDRMDescWriteSEL, FDRMDescWriteInterfacesNum,  CFDRM_DescWrite_V2_Initiator::InitFuncMappingTable },
	{ FDRMFoacReadSEL, FDRMFoacReadInterfacesNum,  CFDRM_FoacRead_V2_Initiator::InitFuncMappingTable },
	{ FDRMFoacWriteSEL, FDRMFoacWriteInterfacesNum,  CFDRM_FoacWrite_V2_Initiator::InitFuncMappingTable },
	{ FDRMEnvelopeReadSEL, FDRMEnvelopeReadInterfacesNum,  CFDRM_EnvelopeRead_V2_Initiator::InitFuncMappingTable },
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
	{ FDRMMgrSEL, FDRMMgrInterfacesNum,  CFDRM_Mgr_V2_Initiator::InitFuncMappingTable },
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
	{ FDRMPDFSecurityHandlerSEL, FDRMPDFSecurityHandlerInterfacesNum,  CFDRM_PDFSecurityHandler_V2_Initiator::InitFuncMappingTable },
	{ FDRMPDFSchemaSEL, FDRMPDFSchemaInterfacesNum,  CFDRM_PDFSchema_V2_Initiator::InitFuncMappingTable },
	{ FDRMEncryptDictReadSEL, FDRMEncryptDictReadInterfacesNum,  CFDRM_EncryptDictRead_V2_Initiator::InitFuncMappingTable },
	{ FDRMEncryptorSEL, FDRMEncryptorInterfacesNum,  CFDRM_Encryptor_V2_Initiator::InitFuncMappingTable },
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
	{ FDRMPKISEL, FDRMPKIInterfacesNum,  CFDRM_PKI_V2_Initiator::InitFuncMappingTable },
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
	{ FPDWrapperCreatorSEL, FPDWrapperCreatorInterfacesNum,  CFPD_WrapperCreator_V2_Initiator::InitFuncMappingTable },
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V3----------
// In file fs_basicImpl.h
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
	{ FRThumbnailViewSEL, FRThumbnailViewInterfacesNum,  CFR_ThumbnailView_V3_Initiator::InitFuncMappingTable },
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V4----------
// In file fs_basicImpl.h
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
	{ FRTabBandSEL, FRTabBandInterfacesNum,  CFR_TabBand_V4_Initiator::InitFuncMappingTable },
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V5----------
// In file fs_basicImpl.h
	{ FSUTF8DecoderSEL, FSUTF8DecoderInterfacesNum,  CFS_UTF8Decoder_V5_Initiator::InitFuncMappingTable },
	{ FSUTF8EncoderSEL, FSUTF8EncoderInterfacesNum,  CFS_UTF8Encoder_V5_Initiator::InitFuncMappingTable },
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
	{ FRInternalSEL, FRInternalInterfacesNum,  CFR_Internal_V5_Initiator::InitFuncMappingTable },
	{ FRSpellCheckSEL, FRSpellCheckInterfacesNum,  CFR_SpellCheck_V5_Initiator::InitFuncMappingTable },
// fr_appImpl.h end

// In file fr_barImpl.h
	{ FRRibbonBarSEL, FRRibbonBarInterfacesNum,  CFR_RibbonBar_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonCategorySEL, FRRibbonCategoryInterfacesNum,  CFR_RibbonCategory_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonPanelSEL, FRRibbonPanelInterfacesNum,  CFR_RibbonPanel_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonElementSEL, FRRibbonElementInterfacesNum,  CFR_RibbonElement_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonButtonSEL, FRRibbonButtonInterfacesNum,  CFR_RibbonButton_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonEditSEL, FRRibbonEditInterfacesNum,  CFR_RibbonEdit_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonLabelSEL, FRRibbonLabelInterfacesNum,  CFR_RibbonLabel_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonCheckBoxSEL, FRRibbonCheckBoxInterfacesNum,  CFR_RibbonCheckBox_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonRadioButtonSEL, FRRibbonRadioButtonInterfacesNum,  CFR_RibbonRadioButton_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonComboBoxSEL, FRRibbonComboBoxInterfacesNum,  CFR_RibbonComboBox_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonFontComboBoxSEL, FRRibbonFontComboBoxInterfacesNum,  CFR_RibbonFontComboBox_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonPaletteButtonSEL, FRRibbonPaletteButtonInterfacesNum,  CFR_RibbonPaletteButton_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonColorButtonSEL, FRRibbonColorButtonInterfacesNum,  CFR_RibbonColorButton_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonSliderSEL, FRRibbonSliderInterfacesNum,  CFR_RibbonSlider_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonListButtonSEL, FRRibbonListButtonInterfacesNum,  CFR_RibbonListButton_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonBackStageViewItemSEL, FRRibbonBackStageViewItemInterfacesNum,  CFR_RibbonBackStageViewItem_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonStyleButtonSEL, FRRibbonStyleButtonInterfacesNum,  CFR_RibbonStyleButton_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonStyleListBoxSEL, FRRibbonStyleListBoxInterfacesNum,  CFR_RibbonStyleListBox_V5_Initiator::InitFuncMappingTable },
	{ FRRibbonStyleStaticSEL, FRRibbonStyleStaticInterfacesNum,  CFR_RibbonStyleStatic_V5_Initiator::InitFuncMappingTable },
	{ FRFormatToolsSEL, FRFormatToolsInterfacesNum,  CFR_FormatTools_V5_Initiator::InitFuncMappingTable },
	{ FRPropertyToolsSEL, FRPropertyToolsInterfacesNum,  CFR_PropertyTools_V5_Initiator::InitFuncMappingTable },
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
	{ FRWindowsDIBSEL, FRWindowsDIBInterfacesNum,  CFR_WindowsDIB_V5_Initiator::InitFuncMappingTable },
// fr_sysImpl.h end

// In file fr_viewImpl.h
	{ FRAnnotSEL, FRAnnotInterfacesNum,  CFR_Annot_V5_Initiator::InitFuncMappingTable },
	{ FRResourcePropertyBoxSEL, FRResourcePropertyBoxInterfacesNum,  CFR_ResourcePropertyBox_V5_Initiator::InitFuncMappingTable },
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
	{ FPDFXFontEncodingSEL, FPDFXFontEncodingInterfacesNum,  CFPD_FXFontEncoding_V5_Initiator::InitFuncMappingTable },
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V6----------
// In file fs_basicImpl.h
	{ FSFileStreamSEL, FSFileStreamInterfacesNum,  CFS_FileStream_V6_Initiator::InitFuncMappingTable },
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
	{ FRScrollBarThumbnailViewSEL, FRScrollBarThumbnailViewInterfacesNum,  CFR_ScrollBarThumbnailView_V6_Initiator::InitFuncMappingTable },
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V7----------
// In file fs_basicImpl.h
	{ FSGUIDSEL, FSGUIDInterfacesNum,  CFS_GUID_V7_Initiator::InitFuncMappingTable },
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
	{ FRHTMLMgrSEL, FRHTMLMgrInterfacesNum,  CFR_HTMLMgr_V7_Initiator::InitFuncMappingTable },
	{ FRPanelMgrSEL, FRPanelMgrInterfacesNum,  CFR_PanelMgr_V7_Initiator::InitFuncMappingTable },
// fr_appImpl.h end

// In file fr_barImpl.h
	{ FRFuncBtnSEL, FRFuncBtnInterfacesNum,  CFR_FuncBtn_V7_Initiator::InitFuncMappingTable },
	{ FRStatusBarSEL, FRStatusBarInterfacesNum,  CFR_StatusBar_V7_Initiator::InitFuncMappingTable },
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
	{ FPDConnectedInfoSEL, FPDConnectedInfoInterfacesNum,  CFPD_ConnectedInfo_V7_Initiator::InitFuncMappingTable },
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V8----------
// In file fs_basicImpl.h
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
	{ FRCustomSignatureSEL, FRCustomSignatureInterfacesNum,  CFR_CustomSignature_V8_Initiator::InitFuncMappingTable },
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V9----------
// In file fs_basicImpl.h
	{ FSUUIDSEL, FSUUIDInterfacesNum,  CFS_UUID_V9_Initiator::InitFuncMappingTable },
	{ FSMapByteStringToPtrSEL, FSMapByteStringToPtrInterfacesNum,  CFS_MapByteStringToPtr_V9_Initiator::InitFuncMappingTable },
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
	{ FRCloudLoginProviderSEL, FRCloudLoginProviderInterfacesNum,  CFR_CloudLoginProvider_V9_Initiator::InitFuncMappingTable },
// fr_appImpl.h end

// In file fr_barImpl.h
	{ FRBulbMsgCenterSEL, FRBulbMsgCenterInterfacesNum,  CFR_BulbMsgCenter_V9_Initiator::InitFuncMappingTable },
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
	{ FPDEPUBSEL, FPDEPUBInterfacesNum,  CFPD_EPUB_V9_Initiator::InitFuncMappingTable },
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V10----------
// In file fs_basicImpl.h
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
	{ FOFDCryptoDictSEL, FOFDCryptoDictInterfacesNum,  CFOFD_CryptoDict_V10_Initiator::InitFuncMappingTable },
	{ FOFDSecurityHandlerSEL, FOFDSecurityHandlerInterfacesNum,  CFOFD_SecurityHandler_V10_Initiator::InitFuncMappingTable },
	{ FOFDStdSecurityHandlerSEL, FOFDStdSecurityHandlerInterfacesNum,  CFOFD_StdSecurityHandler_V10_Initiator::InitFuncMappingTable },
	{ FOFDStdCertSecurityHandlerSEL, FOFDStdCertSecurityHandlerInterfacesNum,  CFOFD_StdCertSecurityHandler_V10_Initiator::InitFuncMappingTable },
	{ FOFDSMSecurityHandlerSEL, FOFDSMSecurityHandlerInterfacesNum,  CFOFD_SMSecurityHandler_V10_Initiator::InitFuncMappingTable },
	{ FOFDCryptoHandlerSEL, FOFDCryptoHandlerInterfacesNum,  CFOFD_CryptoHandler_V10_Initiator::InitFuncMappingTable },
	{ FOFDStdCryptoHandlerSEL, FOFDStdCryptoHandlerInterfacesNum,  CFOFD_StdCryptoHandler_V10_Initiator::InitFuncMappingTable },
	{ FOFDSM4CryptoHandlerSEL, FOFDSM4CryptoHandlerInterfacesNum,  CFOFD_SM4CryptoHandler_V10_Initiator::InitFuncMappingTable },
	{ FOFDFileStreamSEL, FOFDFileStreamInterfacesNum,  CFOFD_FileStream_V10_Initiator::InitFuncMappingTable },
	{ FOFDPauseHandlerSEL, FOFDPauseHandlerInterfacesNum,  CFOFD_PauseHandler_V10_Initiator::InitFuncMappingTable },
	{ FOFDUIMgrSEL, FOFDUIMgrInterfacesNum,  CFOFD_UIMgr_V10_Initiator::InitFuncMappingTable },
	{ FOFDDIBAttributeSEL, FOFDDIBAttributeInterfacesNum,  CFOFD_DIBAttribute_V10_Initiator::InitFuncMappingTable },
	{ FOFDCodeCSEL, FOFDCodeCInterfacesNum,  CFOFD_CodeC_V10_Initiator::InitFuncMappingTable },
	{ FOFDPrintSettingSEL, FOFDPrintSettingInterfacesNum,  CFOFD_PrintSetting_V10_Initiator::InitFuncMappingTable },
	{ FOFDSysSEL, FOFDSysInterfacesNum,  CFOFD_Sys_V10_Initiator::InitFuncMappingTable },
// fofd_basicImpl.h end

// In file fofd_docImpl.h
	{ FOFDPackageSEL, FOFDPackageInterfacesNum,  CFOFD_Package_V10_Initiator::InitFuncMappingTable },
	{ FOFDParserSEL, FOFDParserInterfacesNum,  CFOFD_Parser_V10_Initiator::InitFuncMappingTable },
	{ FOFDDocSEL, FOFDDocInterfacesNum,  CFOFD_Doc_V10_Initiator::InitFuncMappingTable },
	{ FOFDWriteDocSEL, FOFDWriteDocInterfacesNum,  CFOFD_WriteDoc_V10_Initiator::InitFuncMappingTable },
	{ FOFDCreatorSEL, FOFDCreatorInterfacesNum,  CFOFD_Creator_V10_Initiator::InitFuncMappingTable },
	{ FOFDPermsSEL, FOFDPermsInterfacesNum,  CFOFD_Perms_V10_Initiator::InitFuncMappingTable },
	{ FOFDVPrefersSEL, FOFDVPrefersInterfacesNum,  CFOFD_VPrefers_V10_Initiator::InitFuncMappingTable },
	{ FOFDBookmarksSEL, FOFDBookmarksInterfacesNum,  CFOFD_Bookmarks_V10_Initiator::InitFuncMappingTable },
	{ FOFDBookmarkSEL, FOFDBookmarkInterfacesNum,  CFOFD_Bookmark_V10_Initiator::InitFuncMappingTable },
	{ FOFDOutlineSEL, FOFDOutlineInterfacesNum,  CFOFD_Outline_V10_Initiator::InitFuncMappingTable },
	{ FOFDActionsSEL, FOFDActionsInterfacesNum,  CFOFD_Actions_V10_Initiator::InitFuncMappingTable },
	{ FOFDActionSEL, FOFDActionInterfacesNum,  CFOFD_Action_V10_Initiator::InitFuncMappingTable },
	{ FOFDActionGotoSEL, FOFDActionGotoInterfacesNum,  CFOFD_ActionGoto_V10_Initiator::InitFuncMappingTable },
	{ FOFDActionGotoASEL, FOFDActionGotoAInterfacesNum,  CFOFD_ActionGotoA_V10_Initiator::InitFuncMappingTable },
	{ FOFDActionRegionSEL, FOFDActionRegionInterfacesNum,  CFOFD_ActionRegion_V10_Initiator::InitFuncMappingTable },
// fofd_docImpl.h end

// In file fofd_pageImpl.h
	{ FOFDPageSEL, FOFDPageInterfacesNum,  CFOFD_Page_V10_Initiator::InitFuncMappingTable },
	{ FOFDDestSEL, FOFDDestInterfacesNum,  CFOFD_Dest_V10_Initiator::InitFuncMappingTable },
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
	{ FOFDRenderOptionsSEL, FOFDRenderOptionsInterfacesNum,  CFOFD_RenderOptions_V10_Initiator::InitFuncMappingTable },
	{ FOFDRenderContextSEL, FOFDRenderContextInterfacesNum,  CFOFD_RenderContext_V10_Initiator::InitFuncMappingTable },
	{ FOFDRenderDeviceSEL, FOFDRenderDeviceInterfacesNum,  CFOFD_RenderDevice_V10_Initiator::InitFuncMappingTable },
	{ FOFDProgressiveRendererSEL, FOFDProgressiveRendererInterfacesNum,  CFOFD_ProgressiveRenderer_V10_Initiator::InitFuncMappingTable },
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
	{ FOFDSignSEL, FOFDSignInterfacesNum,  CFOFD_Sign_V10_Initiator::InitFuncMappingTable },
// fofd_sigImpl.h end

// In file fpd_docImpl.h
	{ FPDWrapperDocSEL, FPDWrapperDocInterfacesNum,  CFPD_WrapperDoc_V10_Initiator::InitFuncMappingTable },
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
	{ FPDUnencryptedWrapperCreatorSEL, FPDUnencryptedWrapperCreatorInterfacesNum,  CFPD_UnencryptedWrapperCreator_V10_Initiator::InitFuncMappingTable },
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V11----------
// In file fs_basicImpl.h
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
	{ FRAppFxNetSEL, FRAppFxNetInterfacesNum,  CFR_AppFxNet_V11_Initiator::InitFuncMappingTable },
	{ FRInternalFxNetSEL, FRInternalFxNetInterfacesNum,  CFR_InternalFxNet_V11_Initiator::InitFuncMappingTable },
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
	{ FPDColorSeparatorSEL, FPDColorSeparatorInterfacesNum,  CFPD_ColorSeparator_V11_Initiator::InitFuncMappingTable },
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
	{ FPDColorConvertorSEL, FPDColorConvertorInterfacesNum,  CFPD_ColorConvertor_V11_Initiator::InitFuncMappingTable },
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V12----------
// In file fs_basicImpl.h
	{ FSImageSEL, FSImageInterfacesNum,  CFS_Image_V12_Initiator::InitFuncMappingTable },
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
	{ FRAssistantMgrSEL, FRAssistantMgrInterfacesNum,  CFR_AssistantMgr_V12_Initiator::InitFuncMappingTable },
// fr_appImpl.h end

// In file fr_barImpl.h
// fr_barImpl.h end

// In file fr_docImpl.h
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
	{ FPD3dContextSEL, FPD3dContextInterfacesNum,  CFPD_3dContext_V12_Initiator::InitFuncMappingTable },
	{ FPD3dAnnotDataSEL, FPD3dAnnotDataInterfacesNum,  CFPD_3dAnnotData_V12_Initiator::InitFuncMappingTable },
	{ FPD3dAnnotData3dArtworkSEL, FPD3dAnnotData3dArtworkInterfacesNum,  CFPD_3dAnnotData3dArtwork_V12_Initiator::InitFuncMappingTable },
	{ FPD3dSceneSEL, FPD3dSceneInterfacesNum,  CFPD_3dScene_V12_Initiator::InitFuncMappingTable },
	{ FPD3deAssetSEL, FPD3deAssetInterfacesNum,  CFPD_3deAsset_V12_Initiator::InitFuncMappingTable },
	{ FPD3deRuntimeSEL, FPD3deRuntimeInterfacesNum,  CFPD_3deRuntime_V12_Initiator::InitFuncMappingTable },
	{ FPD3deCanvasSEL, FPD3deCanvasInterfacesNum,  CFPD_3deCanvas_V12_Initiator::InitFuncMappingTable },
	{ FPD3deViewSEL, FPD3deViewInterfacesNum,  CFPD_3deView_V12_Initiator::InitFuncMappingTable },
	{ FPD3deViewRenderModeSEL, FPD3deViewRenderModeInterfacesNum,  CFPD_3deViewRenderMode_V12_Initiator::InitFuncMappingTable },
	{ FPD3deViewLightingSchemeSEL, FPD3deViewLightingSchemeInterfacesNum,  CFPD_3deViewLightingScheme_V12_Initiator::InitFuncMappingTable },
	{ FPD3deCanvasControllerToolSEL, FPD3deCanvasControllerToolInterfacesNum,  CFPD_3deCanvasControllerTool_V12_Initiator::InitFuncMappingTable },
	{ FPD3dVendorSEL, FPD3dVendorInterfacesNum,  CFPD_3dVendor_V12_Initiator::InitFuncMappingTable },
	{ FPD3deViewBackgroundSEL, FPD3deViewBackgroundInterfacesNum,  CFPD_3deViewBackground_V12_Initiator::InitFuncMappingTable },
	{ FPD3deViewCameraParamSEL, FPD3deViewCameraParamInterfacesNum,  CFPD_3deViewCameraParam_V12_Initiator::InitFuncMappingTable },
	{ FPD3deViewCrossSectionSEL, FPD3deViewCrossSectionInterfacesNum,  CFPD_3deViewCrossSection_V12_Initiator::InitFuncMappingTable },
	{ FPD3deViewNodeSEL, FPD3deViewNodeInterfacesNum,  CFPD_3deViewNode_V12_Initiator::InitFuncMappingTable },
	{ FPD3deMeasureSEL, FPD3deMeasureInterfacesNum,  CFPD_3deMeasure_V12_Initiator::InitFuncMappingTable },
	{ FPD3deViewMiscOptionsSEL, FPD3deViewMiscOptionsInterfacesNum,  CFPD_3deViewMiscOptions_V12_Initiator::InitFuncMappingTable },
	{ FPD3deTextProviderSEL, FPD3deTextProviderInterfacesNum,  CFPD_3deTextProvider_V12_Initiator::InitFuncMappingTable },
	{ FPDScriptHostHostProviderSEL, FPDScriptHostHostProviderInterfacesNum,  CFPD_ScriptHostHostProvider_V12_Initiator::InitFuncMappingTable },
	{ FPDScript3DEngineSEL, FPDScript3DEngineInterfacesNum,  CFPD_Script3DEngine_V12_Initiator::InitFuncMappingTable },
	{ FPD3DI18NProviderHandlerSEL, FPD3DI18NProviderHandlerInterfacesNum,  CFPD_3DI18NProviderHandler_V12_Initiator::InitFuncMappingTable },
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

//----------_V13----------
// In file fs_basicImpl.h
	{ FSReaderDateTimeSEL, FSReaderDateTimeInterfacesNum,  CFS_ReaderDateTime_V13_Initiator::InitFuncMappingTable },
	{ FSMonoscaleSEL, FSMonoscaleInterfacesNum,  CFS_Monoscale_V13_Initiator::InitFuncMappingTable },
	{ FSFloatArraySEL, FSFloatArrayInterfacesNum,  CFS_FloatArray_V13_Initiator::InitFuncMappingTable },
// fs_basicImpl.h end

// In file fs_stringImpl.h
// fs_stringImpl.h end

// In file fdrm_descImpl.h
// fdrm_descImpl.h end

// In file fdrm_managerImpl.h
// fdrm_managerImpl.h end

// In file fdrm_pdfImpl.h
// fdrm_pdfImpl.h end

// In file fdrm_pkiImpl.h
// fdrm_pkiImpl.h end

// In file fr_appImpl.h
	{ FRIReaderSEL, FRIReaderInterfacesNum,  CFR_IReader_PageView_V13_Initiator::InitFuncMappingTable },
	{ FRIFXSEL, FRIFXInterfacesNum,  CFR_IFX_InputMethod_V13_Initiator::InitFuncMappingTable },
	{ FRInputMethodHandlerSEL, FRInputMethodHandlerInterfacesNum,  CFR_InputMethodHandler_V13_Initiator::InitFuncMappingTable },
	{ FRMarkAnnotExtendHandlerSEL, FRMarkAnnotExtendHandlerInterfacesNum,  CFR_MarkAnnotExtendHandler_V13_Initiator::InitFuncMappingTable },
// fr_appImpl.h end

// In file fr_barImpl.h
	{ FRRibbonStyleRadioBoxSEL, FRRibbonStyleRadioBoxInterfacesNum,  CFR_RibbonStyleRadioBox_V13_Initiator::InitFuncMappingTable },
	{ FRRibbonStyleCheckBoxSEL, FRRibbonStyleCheckBoxInterfacesNum,  CFR_RibbonStyleCheckBox_V13_Initiator::InitFuncMappingTable },
	{ FRRibbonStyleEditSEL, FRRibbonStyleEditInterfacesNum,  CFR_RibbonStyleEdit_V13_Initiator::InitFuncMappingTable },
	{ FRRibbonStyleLinkButtonSEL, FRRibbonStyleLinkButtonInterfacesNum,  CFR_RibbonStyleLinkButton_V13_Initiator::InitFuncMappingTable },
	{ FRRibbonStyleSliderCtrlSEL, FRRibbonStyleSliderCtrlInterfacesNum,  CFR_RibbonStyleSliderCtrl_V13_Initiator::InitFuncMappingTable },
	{ FRRibbonStyleColorButtonSEL, FRRibbonStyleColorButtonInterfacesNum,  CFR_RibbonStyleColorButton_V13_Initiator::InitFuncMappingTable },
	{ FRRibbonIFXBCGPComboBoxSEL, FRRibbonIFXBCGPComboBoxInterfacesNum,  CFR_RibbonIFXBCGPComboBox_V13_Initiator::InitFuncMappingTable },
	{ FRRibbonIFXBCGPFontComboBoxSEL, FRRibbonIFXBCGPFontComboBoxInterfacesNum,  CFR_RibbonIFXBCGPFontComboBox_V13_Initiator::InitFuncMappingTable },
	{ FRCFXBCGStyleGalleryCtrlSEL, FRCFXBCGStyleGalleryCtrlInterfacesNum,  CFR_CFXBCGStyleGalleryCtrl_V13_Initiator::InitFuncMappingTable },
// fr_barImpl.h end

// In file fr_docImpl.h
	{ FRReaderInterformSEL, FRReaderInterformInterfacesNum,  CFR_ReaderInterform_V13_Initiator::InitFuncMappingTable },
	{ FRUndoItemCreateWidgetSEL, FRUndoItemCreateWidgetInterfacesNum,  CFR_UndoItemCreateWidget_V13_Initiator::InitFuncMappingTable },
	{ FRCRSAStampSEL, FRCRSAStampInterfacesNum,  CFR_CRSAStamp_V13_Initiator::InitFuncMappingTable },
	{ FRMarkupAnnotSEL, FRMarkupAnnotInterfacesNum,  CFR_MarkupAnnot_V13_Initiator::InitFuncMappingTable },
	{ FRMarkupPopupSEL, FRMarkupPopupInterfacesNum,  CFR_MarkupPopup_V13_Initiator::InitFuncMappingTable },
	{ FRMarkupPanelSEL, FRMarkupPanelInterfacesNum,  CFR_MarkupPanel_V13_Initiator::InitFuncMappingTable },
	{ FRCRSASStampAnnotSEL, FRCRSASStampAnnotInterfacesNum,  CFR_CRSASStampAnnot_V13_Initiator::InitFuncMappingTable },
	{ FREncryptPermissonSEL, FREncryptPermissonInterfacesNum,  CFR_EncryptPermisson_V13_Initiator::InitFuncMappingTable },
	{ FRCSGCertFileManageSEL, FRCSGCertFileManageInterfacesNum,  CFR_CSGCertFileManage_V13_Initiator::InitFuncMappingTable },
	{ FRCSGSEL, FRCSGInterfacesNum,  CFR_CSG_CreateCert_V13_Initiator::InitFuncMappingTable },
// fr_docImpl.h end

// In file fr_fxnetappImpl.h
// fr_fxnetappImpl.h end

// In file fr_menuImpl.h
// fr_menuImpl.h end

// In file fr_sysImpl.h
// fr_sysImpl.h end

// In file fr_viewImpl.h
	{ FRReaderSEL, FRReaderInterfacesNum,  CFR_Reader_DispViewerEx_V13_Initiator::InitFuncMappingTable },
	{ FRIReaderDispViewerHandlerSEL, FRIReaderDispViewerHandlerInterfacesNum,  CFR_IReaderDispViewerHandler_V13_Initiator::InitFuncMappingTable },
	{ FRIPDFViewerEventHandlerSEL, FRIPDFViewerEventHandlerInterfacesNum,  CFR_IPDFViewerEventHandler_V13_Initiator::InitFuncMappingTable },
	{ FRToolFormatMgrSEL, FRToolFormatMgrInterfacesNum,  CFR_ToolFormatMgr_V13_Initiator::InitFuncMappingTable },
// fr_viewImpl.h end

// In file fofd_basicImpl.h
// fofd_basicImpl.h end

// In file fofd_docImpl.h
// fofd_docImpl.h end

// In file fofd_pageImpl.h
// fofd_pageImpl.h end

// In file fofd_renderImpl.h
// fofd_renderImpl.h end

// In file fofd_sigImpl.h
// fofd_sigImpl.h end

// In file fpd_docImpl.h
	{ FPDCertStoreSEL, FPDCertStoreInterfacesNum,  CFPD_CertStore_V13_Initiator::InitFuncMappingTable },
	{ FPD3DCompositionProviderSEL, FPD3DCompositionProviderInterfacesNum,  CFPD_3DCompositionProvider_V13_Initiator::InitFuncMappingTable },
	{ FPDPageLabelSEL, FPDPageLabelInterfacesNum,  CFPD_PageLabel_V13_Initiator::InitFuncMappingTable },
// fpd_docImpl.h end

// In file fpd_epubImpl.h
// fpd_epubImpl.h end

// In file fpd_objsImpl.h
// fpd_objsImpl.h end

// In file fpd_pageImpl.h
// fpd_pageImpl.h end

// In file fpd_pageobjImpl.h
	{ FPDPathObjectUtilsSEL, FPDPathObjectUtilsInterfacesNum,  CFPD_PathObjectUtils_V13_Initiator::InitFuncMappingTable },
	{ FPDShadingObjectUtilsSEL, FPDShadingObjectUtilsInterfacesNum,  CFPD_ShadingObjectUtils_V13_Initiator::InitFuncMappingTable },
	{ FPDPathEditorSEL, FPDPathEditorInterfacesNum,  CFPD_PathEditor_V13_Initiator::InitFuncMappingTable },
	{ FPDShadingEditorSEL, FPDShadingEditorInterfacesNum,  CFPD_ShadingEditor_V13_Initiator::InitFuncMappingTable },
	{ FPDTextEditorSEL, FPDTextEditorInterfacesNum,  CFPD_TextEditor_V13_Initiator::InitFuncMappingTable },
	{ FPDTextObjectUtilsSEL, FPDTextObjectUtilsInterfacesNum,  CFPD_TextObjectUtils_V13_Initiator::InitFuncMappingTable },
	{ FPDGraphicObjectSEL, FPDGraphicObjectInterfacesNum,  CFPD_GraphicObject_V13_Initiator::InitFuncMappingTable },
	{ FPDGraphicEditorSEL, FPDGraphicEditorInterfacesNum,  CFPD_GraphicEditor_V13_Initiator::InitFuncMappingTable },
	{ FPDGraphicObjectUtilsSEL, FPDGraphicObjectUtilsInterfacesNum,  CFPD_GraphicObjectUtils_V13_Initiator::InitFuncMappingTable },
	{ FPDImageEditorSEL, FPDImageEditorInterfacesNum,  CFPD_ImageEditor_V13_Initiator::InitFuncMappingTable },
	{ FPDIPageEditorSEL, FPDIPageEditorInterfacesNum,  CFPD_IPageEditor_V13_Initiator::InitFuncMappingTable },
	{ FPDIUndoItemSEL, FPDIUndoItemInterfacesNum,  CFPD_IUndoItem_V13_Initiator::InitFuncMappingTable },
	{ FPDIClipboardSEL, FPDIClipboardInterfacesNum,  CFPD_IClipboard_V13_Initiator::InitFuncMappingTable },
	{ FPDIPopupMenuSEL, FPDIPopupMenuInterfacesNum,  CFPD_IPopupMenu_V13_Initiator::InitFuncMappingTable },
	{ FPDITipSEL, FPDITipInterfacesNum,  CFPD_ITip_V13_Initiator::InitFuncMappingTable },
	{ FPDIOperationNotifySEL, FPDIOperationNotifyInterfacesNum,  CFPD_IOperationNotify_V13_Initiator::InitFuncMappingTable },
	{ FPDIPublicOptionDataSEL, FPDIPublicOptionDataInterfacesNum,  CFPD_IPublicOptionData_V13_Initiator::InitFuncMappingTable },
	{ FPDIBaseBrushOptionDataSEL, FPDIBaseBrushOptionDataInterfacesNum,  CFPD_IBaseBrushOptionData_V13_Initiator::InitFuncMappingTable },
	{ FPDIBrushOptionDataSEL, FPDIBrushOptionDataInterfacesNum,  CFPD_IBrushOptionData_V13_Initiator::InitFuncMappingTable },
	{ FPDIEraserOptionDataSEL, FPDIEraserOptionDataInterfacesNum,  CFPD_IEraserOptionData_V13_Initiator::InitFuncMappingTable },
	{ FPDIMagicWandOptionDataSEL, FPDIMagicWandOptionDataInterfacesNum,  CFPD_IMagicWandOptionData_V13_Initiator::InitFuncMappingTable },
	{ FPDIDodgeOptionDataSEL, FPDIDodgeOptionDataInterfacesNum,  CFPD_IDodgeOptionData_V13_Initiator::InitFuncMappingTable },
	{ FPDIBurnOptionDataSEL, FPDIBurnOptionDataInterfacesNum,  CFPD_IBurnOptionData_V13_Initiator::InitFuncMappingTable },
	{ FPDIEyedropperDataSEL, FPDIEyedropperDataInterfacesNum,  CFPD_IEyedropperData_V13_Initiator::InitFuncMappingTable },
	{ FPDICloneStampDataSEL, FPDICloneStampDataInterfacesNum,  CFPD_ICloneStampData_V13_Initiator::InitFuncMappingTable },
	{ FPDIPaintBucketOptionDataSEL, FPDIPaintBucketOptionDataInterfacesNum,  CFPD_IPaintBucketOptionData_V13_Initiator::InitFuncMappingTable },
	{ FPDISpotHealingBrushDataSEL, FPDISpotHealingBrushDataInterfacesNum,  CFPD_ISpotHealingBrushData_V13_Initiator::InitFuncMappingTable },
	{ FPDCEditObjectSEL, FPDCEditObjectInterfacesNum,  CFPD_CEditObject_V13_Initiator::InitFuncMappingTable },
	{ FPDIJoinSplitSEL, FPDIJoinSplitInterfacesNum,  CFPD_IJoinSplit_V13_Initiator::InitFuncMappingTable },
	{ FPDITouchupSEL, FPDITouchupInterfacesNum,  CFPD_ITouchup_V13_Initiator::InitFuncMappingTable },
	{ FPDITouchupManagerSEL, FPDITouchupManagerInterfacesNum,  CFPD_ITouchupManager_V13_Initiator::InitFuncMappingTable },
	{ FPDITouchUndoItemSEL, FPDITouchUndoItemInterfacesNum,  CFPD_ITouchUndoItem_V13_Initiator::InitFuncMappingTable },
	{ FPDITouchClipboardSEL, FPDITouchClipboardInterfacesNum,  CFPD_ITouchClipboard_V13_Initiator::InitFuncMappingTable },
	{ FPDITouchPopupMenuSEL, FPDITouchPopupMenuInterfacesNum,  CFPD_ITouchPopupMenu_V13_Initiator::InitFuncMappingTable },
	{ FPDITouchProgressBarSEL, FPDITouchProgressBarInterfacesNum,  CFPD_ITouchProgressBar_V13_Initiator::InitFuncMappingTable },
	{ FPDITouchOperationNotifySEL, FPDITouchOperationNotifyInterfacesNum,  CFPD_ITouchOperationNotify_V13_Initiator::InitFuncMappingTable },
	{ FPDITouchUndoHandlerSEL, FPDITouchUndoHandlerInterfacesNum,  CFPD_ITouchUndoHandler_V13_Initiator::InitFuncMappingTable },
	{ FPDITouchTextFormatHandlerSEL, FPDITouchTextFormatHandlerInterfacesNum,  CFPD_ITouchTextFormatHandler_V13_Initiator::InitFuncMappingTable },
	{ FPDITouchProviderSEL, FPDITouchProviderInterfacesNum,  CFPD_ITouchProvider_V13_Initiator::InitFuncMappingTable },
// fpd_pageobjImpl.h end

// In file fpd_parserImpl.h
	{ FPDStandardCryptoHandlerSEL, FPDStandardCryptoHandlerInterfacesNum,  CFPD_StandardCryptoHandler_V13_Initiator::InitFuncMappingTable },
	{ FPDFipsStandardCryptoHandlerSEL, FPDFipsStandardCryptoHandlerInterfacesNum,  CFPD_FipsStandardCryptoHandler_V13_Initiator::InitFuncMappingTable },
// fpd_parserImpl.h end

// In file fpd_renderImpl.h
// fpd_renderImpl.h end

// In file fpd_resourceImpl.h
	{ FPDOutputPreviewSEL, FPDOutputPreviewInterfacesNum,  CFPD_OutputPreview_V13_Initiator::InitFuncMappingTable },
// fpd_resourceImpl.h end

// In file fpd_serialImpl.h
// fpd_serialImpl.h end

// In file fpd_textImpl.h
// fpd_textImpl.h end

// the last Item, only to make generation easiler,never used!
	#endif
	{-1, -1, 0}
};

#endif