﻿/*****************************************************************************

Copyright (C) 2010 Foxit Corporation
All rights reserved.

NOTICE: Foxit permits you to use, modify, and distribute this file
in accordance with the terms of the Foxit license agreement
accompanying it. If you have received this file from a source other
than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
 
*****************************************************************************/

//----------_V1----------
//----------_V2----------
//----------_V3----------
//----------_V4----------
//----------_V5----------
//----------_V6----------
//----------_V7----------
//----------_V8----------
//----------_V9----------
//----------_V10----------
//*****************************
/* Page HFT functions */
//*****************************

BEGINENUM
INTERFACE(FS_DWORD, FOFDPageGetID, (FOFD_Page pPage))

INTERFACE(FOFD_Document, FOFDPageGetDocument, (FOFD_Page pPage))

INTERFACE(FOFD_RectF, FOFDPageGetPageRect, (FOFD_Page pPage))

INTERFACE(FS_INT32, FOFDPageGetPageRotate, (FOFD_Page pPage))

INTERFACE(FS_BOOL, FOFDPageLoadPage, (FOFD_Page pPage))

INTERFACE(void, FOFDPageUnloadPage, (FOFD_Page pPage))

INTERFACE(FS_BOOL, FOFDPageIsPageLoaded, (FOFD_Page pPage))

INTERFACE(void, FOFDPageReleaseCatchImage, (FOFD_Page pPage))

INTERFACE(FS_AffineMatrix, FOFDPageGetMatrix, (const FOFD_RectF& docPageRect,  const FS_Rect& devicePageRect,  FS_INT32 iRotate))

INTERFACE(FOFD_RectF, FOFDPageGetCropPageRect, (FOFD_Page pPage))

NumOfSelector(FOFDPage)
ENDENUM

//*****************************
/* Dest HFT functions */
//*****************************

BEGINENUM
INTERFACE(FOFD_Dest, FOFDDestCreate, ())

INTERFACE(void, FOFDDestRelease, (FOFD_Dest dest))

INTERFACE(FS_BOOL, FOFDDestIsValid, (FOFD_Dest dest))

INTERFACE(FS_DWORD, FOFDDestGetPageID, (FOFD_Dest dest))

INTERFACE(OFD_PAGEDEST_TYPE, FOFDDestGetDestType, (FOFD_Dest dest))

INTERFACE(FS_FLOAT, FOFDDestGetLeft, (FOFD_Dest dest))

INTERFACE(FS_FLOAT, FOFDDestGetTop, (FOFD_Dest dest))

INTERFACE(FS_FLOAT, FOFDDestGetRight, (FOFD_Dest dest))

INTERFACE(FS_FLOAT, FOFDDestGetBottom, (FOFD_Dest dest))

INTERFACE(FS_FLOAT, FOFDDestGetZoom, (FOFD_Dest dest))

NumOfSelector(FOFDDest)
ENDENUM

//----------_V11----------
//----------_V12----------
//----------_V13----------
