﻿/*****************************************************************************

Copyright (C) 2010 Foxit Corporation
All rights reserved.

NOTICE: Foxit permits you to use, modify, and distribute this file
in accordance with the terms of the Foxit license agreement
accompanying it. If you have received this file from a source other
than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
 
*****************************************************************************/

//----------_V1----------
//----------_V2----------
//----------_V3----------
//----------_V4----------
//----------_V5----------
//----------_V6----------
//----------_V7----------
//----------_V8----------
//----------_V9----------
//----------_V10----------
//*****************************
/* Package HFT functions */
//*****************************

BEGINENUM
INTERFACE(FOFD_FilePackage, FOFDPackageCreate, (FS_FileStream pHandler))

INTERFACE(FOFD_FilePackage, FOFDPackageCreate2, (FS_WideString wsOFDPathName))

INTERFACE(void, FOFDPackageRelease, (FOFD_FilePackage pPackage))

NumOfSelector(FOFDPackage)
ENDENUM

//*****************************
/* Parser HFT functions */
//*****************************

BEGINENUM
INTERFACE(FOFD_Parser, FOFDParserCreate, (FOFD_FilePackage pPackage,  FOFD_docProvider_callbacks callbacks))

INTERFACE(void, FOFDParserRelease, (FOFD_Parser pParser))

INTERFACE(FS_INT32, FOFDParserCountDocuments, (FOFD_Parser pParser))

INTERFACE(FOFD_Document, FOFDParserGetDocument, (FOFD_Parser pParser,  int nIndex))

INTERFACE(FOFD_Document, FOFDParserGetDocument2, (FOFD_Parser pParser,  int nIndex,  FOFD_SecurityHandler pSecurityHandler,  FOFD_CryptoHandler pCryptoHandler,  FS_LPCBYTE pdorsd,  FS_DWORD psize))

INTERFACE(FS_INT32, FOFDParserGetDocumentIndex, (FOFD_Parser pParser,  FOFD_Document pDoc))

INTERFACE(FS_BOOL, FOFDParserGetCryptoDict, (FOFD_Parser pParser,  FS_INT32 nIndex,  FOFD_CryptoDict& pDict))

INTERFACE(FOFD_Parser, FOFDParserCreate2, (FOFD_FilePackage pPackage,  FOFD_docProvider_callbacks callbacks))

NumOfSelector(FOFDParser)
ENDENUM

//*****************************
/* Doc HFT functions */
//*****************************

BEGINENUM
INTERFACE(FOFD_Parser, FOFDDocGetParser, (FOFD_Document pOFDDoc))

INTERFACE(FS_INT32, FOFDDocCountPages, (FOFD_Document pOFDDoc))

INTERFACE(FOFD_Page, FOFDDocGetPage, (FOFD_Document pOFDDoc,  FS_INT32 nPageIndex))

INTERFACE(FOFD_Page, FOFDDocGetPageByID, (FOFD_Document pOFDDoc,  FS_DWORD dwID))

INTERFACE(FS_INT32, FOFDDocGetPageIndex, (FOFD_Document pOFDDoc,  FOFD_Page pPage))

INTERFACE(FOFD_Outline, FOFDDocGenerateOutline, (FOFD_Document pOFDDoc))

INTERFACE(FOFD_Bookmarks, FOFDDocGetBookmarks, (FOFD_Document pOFDDoc))

INTERFACE(FOFD_VPreferences, FOFDDocGetVPreferences, (FOFD_Document pOFDDoc))

INTERFACE(FOFD_Permissions, FOFDDocGetPermissions, (FOFD_Document pOFDDoc))

NumOfSelector(FOFDDoc)
ENDENUM

//*****************************
/* WriteDoc HFT functions */
//*****************************

BEGINENUM
INTERFACE(FOFD_WriteDocument, FOFDWriteDocCreate, (FOFD_Document doc))

INTERFACE(void, FOFDWriteDocRelease, (FOFD_WriteDocument writeDoc))

NumOfSelector(FOFDWriteDoc)
ENDENUM

//*****************************
/* Creator HFT functions */
//*****************************

BEGINENUM
INTERFACE(FOFD_Creator, FOFDCreatorCreate, ())

INTERFACE(void, FOFDCreatorRelease, (FOFD_Creator creator))

INTERFACE(FS_BOOL, FOFDCreatorInsertDocument, (FOFD_Creator creator,  FOFD_WriteDocument writeDoc,  FS_INT32 nIndex))

INTERFACE(FS_BOOL, FOFDCreatorInitParser, (FOFD_Creator creator,  FOFD_Parser parser))

INTERFACE(void, FOFDCreatorPackage, (FOFD_Creator creator,  FS_FileStream fileWrite))

NumOfSelector(FOFDCreator)
ENDENUM

//*****************************
/* Perms HFT functions */
//*****************************

BEGINENUM
INTERFACE(FS_BOOL, FOFDPermsIsEdit, (FOFD_Permissions perms))

INTERFACE(FS_BOOL, FOFDPermsIsAnnot, (FOFD_Permissions perms))

INTERFACE(FS_BOOL, FOFDPermsIsExport, (FOFD_Permissions perms))

INTERFACE(FS_BOOL, FOFDPermsIsSignature, (FOFD_Permissions perms))

INTERFACE(FS_BOOL, FOFDPermsIsWatermark, (FOFD_Permissions perms))

INTERFACE(FS_BOOL, FOFDPermsIsPrintScreen, (FOFD_Permissions perms))

INTERFACE(FS_BOOL, FOFDPermsIsPrintable, (FOFD_Permissions perms))

INTERFACE(FS_INT32, FOFDPermsGetPrintCopies, (FOFD_Permissions perms))

INTERFACE(void, FOFDPermsGetStartDate, (FOFD_Permissions perms,  FS_WideString))

INTERFACE(void, FOFDPermsGetEndDate, (FOFD_Permissions perms,  FS_WideString))

NumOfSelector(FOFDPerms)
ENDENUM

//*****************************
/* VPrefers HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FOFDVPrefersGetPageMode, (FOFD_VPreferences vp,  FS_WideString wsValue))

INTERFACE(void, FOFDVPrefersGetPageLayout, (FOFD_VPreferences vp,  FS_WideString wsValue))

INTERFACE(void, FOFDVPrefersGetTabDisplay, (FOFD_VPreferences vp,  FS_WideString wsValue))

INTERFACE(FS_BOOL, FOFDVPrefersIsHideToolbar, (FOFD_VPreferences vp))

INTERFACE(FS_BOOL, FOFDVPrefersIsHideMenubar, (FOFD_VPreferences vp))

INTERFACE(FS_BOOL, FOFDVPrefersIsHideWindowUI, (FOFD_VPreferences vp))

INTERFACE(void, FOFDVPrefersGetZoomMode, (FOFD_VPreferences vp,  FS_WideString wsValue))

INTERFACE(FS_FLOAT, FOFDVPrefersGetZoom, (FOFD_VPreferences vp))

NumOfSelector(FOFDVPrefers)
ENDENUM

//*****************************
/* Bookmarks HFT functions */
//*****************************

BEGINENUM
INTERFACE(FS_DWORD, FOFDBookmarksCountBookmarks, (FOFD_Bookmarks pBookmarks))

INTERFACE(FOFD_Bookmark, FOFDBookmarksGetBookmark, (FOFD_Bookmarks bookmarks,  FS_DWORD nIndex))

INTERFACE(FOFD_Bookmark, FOFDBookmarksGetBookmark2, (FOFD_Bookmarks bookmarks,  FS_WideString wsName))

NumOfSelector(FOFDBookmarks)
ENDENUM

//*****************************
/* Bookmark HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FOFDBookmarkGetName, (FOFD_Bookmark bookmark,  FS_WideString wsName))

INTERFACE(void, FOFDBookmarkGetDest, (FOFD_Bookmark bookmark,  FOFD_Dest dest))

NumOfSelector(FOFDBookmark)
ENDENUM

//*****************************
/* Outline HFT functions */
//*****************************

BEGINENUM
INTERFACE(FOFD_Outline, FOFDOutlineCreate, (const FOFD_Outline& outline))

INTERFACE(void, FOFDOutlineRelease, (FOFD_Outline outline))

INTERFACE(FS_BOOL, FOFDOutlineIsNull, (FOFD_Outline outline))

INTERFACE(FS_BOOL, FOFDOutlineIsIdentical, (FOFD_Outline outline_a,  FOFD_Outline outline_b))

INTERFACE(FOFD_Outline, FOFDOutlineGenerateParent, (FOFD_Outline outline))

INTERFACE(FOFD_Outline, FOFDOutlineClone, (FOFD_Outline outline))

INTERFACE(FS_INT32, FOFDOutlineCountSubOutlines, (FOFD_Outline outline))

INTERFACE(FOFD_Outline, FOFDOutlineGenerateSubOutline, (FOFD_Outline outline,  FS_INT32 nIndex))

INTERFACE(FS_BOOL, FOFDOutlineIsExpanded, (FOFD_Outline outline))

INTERFACE(void, FOFDOutlineGetTitle, (FOFD_Outline outline,  FS_WideString wsTitle))

INTERFACE(FOFD_Actions, FOFDOutlineGetActions, (FOFD_Outline outline))

NumOfSelector(FOFDOutline)
ENDENUM

//*****************************
/* Actions HFT functions */
//*****************************

BEGINENUM
INTERFACE(FS_INT32, FOFDActionsCountActions, (FOFD_Actions pActions))

INTERFACE(FOFD_Action, FOFDActionsGetAction, (FOFD_Actions pActions,  FS_INT32 nIndex))

NumOfSelector(FOFDActions)
ENDENUM

//*****************************
/* Action HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FOFDActionGetEvent, (FOFD_Action pAction,  FS_ByteString bsEvent))

INTERFACE(FOFD_ActionRegion, FOFDActionGenerateActionRegion, (FOFD_Action action))

INTERFACE(OFD_ACTION_TYPE, FOFDActionGetActionType, (FOFD_Action pAction))

NumOfSelector(FOFDAction)
ENDENUM

//*****************************
/* ActionGoto HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FOFDActionGotoGetDest, (FOFD_ActionGoto pActionGoto,  FOFD_Dest dest))

INTERFACE(void, FOFDActionGotoGetBookmark, (FOFD_ActionGoto pActionGoto,  FS_WideString wsBookmark))

NumOfSelector(FOFDActionGoto)
ENDENUM

//*****************************
/* ActionGotoA HFT functions */
//*****************************

BEGINENUM
INTERFACE(void, FOFDActionGotoAGetDest, (FOFD_ActionGotoA pActionGoto,  FOFD_Dest dest))

INTERFACE(void, FOFDActionGotoAGetBookmark, (FOFD_ActionGotoA pActionGoto,  FS_WideString wsBookmark))

INTERFACE(FS_DWORD, FOFDActionGotoAGetAttachID, (FOFD_ActionGotoA pActionGoto))

INTERFACE(FS_BOOL, FOFDActionGotoAIsNewWindow, (FOFD_ActionGotoA pActionGoto))

NumOfSelector(FOFDActionGotoA)
ENDENUM

//*****************************
/* ActionRegion HFT functions */
//*****************************

BEGINENUM
INTERFACE(FOFD_ActionRegion, FOFDActionRegionNew, (FOFD_Action pAction))

INTERFACE(void, FOFDActionRegionRelease, (FOFD_ActionRegion))

INTERFACE(FS_BOOL, FOFDActionRegionIsEmpty, (FOFD_ActionRegion pActionRegion))

INTERFACE(FS_INT32, FOFDActionRegionCountAreas, (FOFD_ActionRegion pActionRegion))

INTERFACE(FOFD_ActionArea, FOFDActionRegionGenerateArea, (FOFD_ActionRegion pActionRegion,  FS_INT32 nIndex))

INTERFACE(FOFD_Path, FOFDActionRegionGeneratePath, (FOFD_ActionRegion pActionRegion))

NumOfSelector(FOFDActionRegion)
ENDENUM

//----------_V11----------
//----------_V12----------
//----------_V13----------
