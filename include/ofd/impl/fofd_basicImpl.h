﻿#ifndef FOFD_BASICIMPL_H
#define FOFD_BASICIMPL_H

#ifndef FS_INTERNALINC_H
#include "../../basic/fs_internalInc.h"
#endif

#ifndef FOFD_BASICEXPT_H
#include "../fofd_basicExpT.h"
#endif

#ifdef __cplusplus
extern "C"{
#endif

/************************************************************************/
/*                      CryptoDictionary                                */
/************************************************************************/

class CFOFD_CryptoDict_V10
{
public:
	static FOFD_CryptoDict			New(void);
	static void						Destroy(FOFD_CryptoDict dict);
	static void						GetAdminPwd(FOFD_CryptoDict dict, FS_ByteString bsAdminPwd);
	static void						GetUserPwd(FOFD_CryptoDict dict, FS_ByteString bsUserPwd);
	static void						GetSecurityType(FOFD_CryptoDict dict, FS_ByteString bstype);
	static void						GetCryptoType(FOFD_CryptoDict dict, FS_ByteString bstype);
	static void						GetFilter(FOFD_CryptoDict dict, FS_ByteString bsFilter);
	static void						GetSubFilter(FOFD_CryptoDict dict, FS_ByteString bsSubFilter);
	static void						GetCipher(FOFD_CryptoDict dict, FS_ByteString bsCipher);
	static FS_INT32					CountRecipients(FOFD_CryptoDict dict);
	static void						GetRecipient(FOFD_CryptoDict dict, FS_INT32 index, FS_ByteString bsRecipient);
	static FS_INT32					GetKeyLength(FOFD_CryptoDict dict);
	static void						SetAdminPwd(FOFD_CryptoDict dict, FS_ByteString bsAdminPwd, FS_INT32 admin_len);
	static void						SetUserPwd(FOFD_CryptoDict dict, FS_ByteString bsUserPwd, FS_INT32 user_len);
	static void						SetSecurityType(FOFD_CryptoDict dict, FS_ByteString bsSecurityType);
	static void						SetCryptoType(FOFD_CryptoDict dict, FS_ByteString bsCryptoType);
	static void						SetFilter(FOFD_CryptoDict dict, FS_ByteString bsFilter);
	static void						SetSubFilter(FOFD_CryptoDict dict, FS_ByteString bsSubFilter);
	static void						SetCipher(FOFD_CryptoDict dict, FS_ByteString bsCipher);
	static void						AddRecipient(FOFD_CryptoDict dict, FS_ByteString bsRecipient);
	static void						SetKeyLength(FOFD_CryptoDict dict, FS_INT32 nKeyLength);
};

/************************************************************************/
/*                      SecurityHandler                                 */
/************************************************************************/
class CFOFD_SecurityHandler_V10
{
public:
	static void						Release(FOFD_SecurityHandler pHandler);
	static FS_BOOL					IsAdmin(FOFD_SecurityHandler pHandler);
};

class CFOFD_StdSecurityHandler_V10
{
public:
	static FOFD_SecurityHandler		Create(void);
};

class CFOFD_StdCertSecurityHandler_V10
{
public:
	static FOFD_SecurityHandler		Create(void);
	static void						SetPKCS12Info(FOFD_SecurityHandler pHandler, FS_WideString lpPKCS12Path, FS_ByteString password);
	static FS_BOOL					InitParser(FOFD_SecurityHandler pHandler, FOFD_CryptoDict pDict);
	static void						GetPermissons(FOFD_SecurityHandler pHandler, FS_ByteString bsPerms);
	static FS_BOOL					InitCreator(FOFD_SecurityHandler pHandler, FS_INT32 cipher, FS_ByteString subFilter);
};

class CFOFD_SMSecurityHandler_V10
{
public:
	static FOFD_SecurityHandler		Create(void);
};

/************************************************************************/
/*                      CryptoHandler                                   */
/************************************************************************/
class CFOFD_CryptoHandler_V10
{
public:
	static void						Release(FOFD_CryptoHandler pHandler);
};

class CFOFD_StdCryptoHandler_V10
{
public:
	static FOFD_CryptoHandler		Create(void);
};

class CFOFD_SM4CryptoHandler_V10
{
public:
	static FOFD_CryptoHandler		Create(void);
};

//////////////////////////////////////////////////////////////////////////
class CFOFD_FileStream_V10
{
public:
	static FOFD_FileStream		CreateMemoryStream(FS_LPWSTR wsName);
	static FOFD_FileStream		CreateMemoryStream2(FS_LPBYTE pBuffer, size_t nSize, FS_BOOL bTakeOver);

	static void					Release(FOFD_FileStream pFile);

	static FS_INT64				GetSize(FOFD_FileStream pFile);
	static FS_BOOL				IsEOF(FOFD_FileStream pFile);
	static FS_INT64				GetPosition(FOFD_FileStream pFile);

	static size_t				ReadBlock(FOFD_FileStream pFile, void* buffer, size_t size);
	static FS_BOOL				ReadBlock2(FOFD_FileStream pFile, void* buffer, FS_INT64 offset, size_t size);

	static FS_BOOL				WriteBlock(FOFD_FileStream pFile, const void* buffer, size_t size);
	static FS_BOOL				WriteBlock2(FOFD_FileStream pFile, const void* buffer, FS_INT64 offset, size_t size);

	static FS_BOOL				Flush(FOFD_FileStream pFile);

	//nType: 0 for full path name, -1 for path only, 1 for file name only
	static void					GetCurrentFileName(FOFD_FileStream pFile, FS_INT32 nType, FS_WideString wsName);
};

//////////////////////////////////////////////////////////////////////////
#ifndef COFD_CUSTOMERPAUSE
#define COFD_CUSTOMERPAUSE
class COFD_CustomerPauseHandler : public IFX_Pause, public CFX_Object
{
public:
	COFD_CustomerPauseHandler(OFD_Pause pause);
	virtual FX_BOOL	NeedToPauseNow();
private:
	OFD_Pause m_pause;
};
#endif

//////////////////////////////////////////////////////////////////////////
//class CFOFD_PauseHandler_V10

class CFOFD_PauseHandler_V10
{
public:
	//************************************
	// Function: Create
	// Param[in]: pause		The input pause handler structure.
	// Return:   The newly created pause handler.
	// Remarks:  Creates the pause handler.
	// Notes:
	//************************************
	static FOFD_PauseHandler	Create(OFD_Pause pause);

	//************************************
	// Function: Destroy
	// Param[in]: pauseHandler		The input pause handler to be destroyed.
	// Return:   void
	// Remarks:  Destroys the pause handler.
	// Notes:
	//************************************
	static void	Destroy(FOFD_PauseHandler pauseHandler);
};

class CFOFD_UIMgr_V10
{
public:
	static void				SetPanelIndex(FR_PanelMgr panelMgr, FS_LPCSTR csName, FS_INT32 nInitialIndex);
	static void				TriggerPanel(FR_PanelMgr pPanelMgr, FS_ByteString bsName);
	static void				FormatComboBox(FS_WideString csText, FS_INT32 nType, FS_WideString* out);
	static void				ShowAutoHideStatusBar(BOOL bShow, int nOffset_X, int nOffset_Y);
};

class CFOFD_DIBAttribute_V10
{
public:
	static FOFD_DIBAttribute			Create();

	static FS_INT32						GetXDPI(FOFD_DIBAttribute attr);
	static void							SetXDPI(FOFD_DIBAttribute attr, FS_INT32 value);

	static FS_INT32						GetYDPI(FOFD_DIBAttribute attr);
	static void							SetYDPI(FOFD_DIBAttribute attr, FS_INT32 value);
	
	static FS_FLOAT						GetAspectRatio(FOFD_DIBAttribute attr);
	static void							SetAspectRatio(FOFD_DIBAttribute attr, FS_FLOAT value);

	static FS_DWORD						GetDPIUnit(FOFD_DIBAttribute attr);
	static void							SetDPIUnit(FOFD_DIBAttribute attr, FS_DWORD value);

	static void							GetAuthor(FOFD_DIBAttribute attr, FS_ByteString* out);
	static void							SetAuthor(FOFD_DIBAttribute attr, FS_ByteString value);

	static FS_LPBYTE					GetTime(FOFD_DIBAttribute attr);
	static void							SetTime(FOFD_DIBAttribute attr, FS_LPBYTE value);

	static FS_INT32						GetGifLeft(FOFD_DIBAttribute attr);
	static void							SetGifLeft(FOFD_DIBAttribute attr, FS_INT32 value);

	static FS_INT32						GetGifTop(FOFD_DIBAttribute attr);
	static void							SetGifTop(FOFD_DIBAttribute attr, FS_INT32 value);

	static FS_DWORD*					GetGifLocalPalette(FOFD_DIBAttribute attr);
	static void							SetGifLocalPalette(FOFD_DIBAttribute attr, FS_DWORD* value);

	static FS_DWORD						GetGifLocalPalNum(FOFD_DIBAttribute attr);
	static void							SetGifLocalPalNum(FOFD_DIBAttribute attr, FS_DWORD value);

	static FS_INT32						GetBmpCompressType(FOFD_DIBAttribute attr);
	static void							SetBmpCompressType(FOFD_DIBAttribute attr, FS_INT32 value);

	static FS_DWORD						GetTiffFrameCompressType(FOFD_DIBAttribute attr);
	static void							SetTiffFrameCompressType(FOFD_DIBAttribute attr, FS_DWORD value);

	static FS_DWORD						GetTiffFrameCompressOptions(FOFD_DIBAttribute attr);
	static void							SetTiffFrameCompressOptions(FOFD_DIBAttribute attr, FS_DWORD value);

	static FS_INT32						GetTiffFrameFillOrder(FOFD_DIBAttribute attr);
	static void							SetTiffFrameFillOrder(FOFD_DIBAttribute attr, FS_INT32 value);

	static FS_DWORD						GetTiffFrameCompressJpegQuality(FOFD_DIBAttribute attr);
	static void							SetTiffFrameCompressJpegQuality(FOFD_DIBAttribute attr, FS_DWORD value);
};

class CFOFD_CodeC_V10
{
public:
	static FS_BOOL						PngEncode(FS_DIBitmap pSource, FS_LPCWSTR file_name, FS_BOOL bInterlace, FOFD_DIBAttribute pAttribute);

};

class CFOFD_PrintSetting_V10
{
public:
	static FS_INT32						GetGlobalUnit();
	static void							GetDefaultPrinterName(FS_WideString* out);
	static DEVMODE*						GetDefaultPrinterDevMode();
	static void							SetDefaultPrinterDevMode(HANDLE hDevNames, HANDLE hDevMode);
	static FS_BOOL						IsPrintUseGraphics();
};

class CFOFD_Sys_V10
{
public:
	static FS_BOOL						InitSSLModule();
};

#ifdef __cplusplus
};
#endif

#endif//FOFD_BASICIMPL_H
