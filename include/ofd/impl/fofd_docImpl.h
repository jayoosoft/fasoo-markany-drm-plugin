﻿

#ifndef FOFD_DOCIMPL_H
#define FOFD_DOCIMPL_H

#ifndef FS_INTERNALINC_H
#include "../../basic/fs_internalInc.h"
#endif

#ifndef _OFD_INTERNALINC_H_
#define _OFD_INTERNALINC_H_
#include "../../../ExternalModule/fxutil/include/fx_xml.h"
#include "../../../ExternalModule/fxutil/include/fx_time.h"
#include "../../../ExternalModule/fxcore/include/fxmath/fxmath_image.h"
#include "../../../ExternalModule/ofd/include/ofd.h"
#include "../../../ExternalModule/ofd/include/ofd_serial.h"
#include "../../../ExternalModule/ofd/include/ofd_crypt.h"

#endif

#ifndef FOFD_BASICEXPT_H
#include "../fofd_basicExpt.h"
#endif

#ifndef FOFD_OBJEXPT_H
#include "../fofd_objExpT.h"
#endif

#ifndef FOFD_DOCEXPT_H
#include "../fofd_docExpT.h"
#endif

#ifndef FOFD_PAGEEXPT_H
#include "../fofd_pageExpt.h" 
#endif

#ifdef __cplusplus
extern "C"{
#endif

class CFOFD_Package_V10
{
public:
	static FOFD_FilePackage			Create(FS_FileStream pHandler);
	static FOFD_FilePackage			Create2(FS_WideString wsOFDPathName);
	static void						Release(FOFD_FilePackage pPackage);

};

class CFOFD_Parser_V10
{
public:
	static FOFD_Parser				Create(FOFD_FilePackage pPackage, FOFD_docProvider_callbacks callbacks);
	static void						Release(FOFD_Parser pParser);
	static FS_INT32					CountDocuments(FOFD_Parser pParser);
	static FOFD_Document			GetDocument(FOFD_Parser pParser, int nIndex);
	static FOFD_Document			GetDocument2(FOFD_Parser pParser, int nIndex, FOFD_SecurityHandler pSecurityHandler,
												 FOFD_CryptoHandler pCryptoHandler, FS_LPCBYTE pdorsd, FS_DWORD psize);
	static FS_INT32					GetDocumentIndex(FOFD_Parser pParser,FOFD_Document pDoc);
	static FS_BOOL					GetCryptoDict(FOFD_Parser pParser,FS_INT32 nIndex, FOFD_CryptoDict& pDict);
	static FOFD_Parser				Create2(FOFD_FilePackage pPackage, FOFD_docProvider_callbacks callbacks);
};

class CFOFD_Doc_V10
{
public:
	static FOFD_Parser				GetParser(FOFD_Document pOFDDoc);

	//Pages
	static FS_INT32					CountPages(FOFD_Document pOFDDoc);
	static FOFD_Page				GetPage(FOFD_Document pOFDDoc, FS_INT32 nPageIndex);
	static FOFD_Page				GetPageByID(FOFD_Document pOFDDoc, FS_DWORD dwID);
	static FS_INT32					GetPageIndex(FOFD_Document pOFDDoc, FOFD_Page pPage);

	static FOFD_Outline				GenerateOutline(FOFD_Document pOFDDoc);
	static FOFD_Bookmarks			GetBookmarks(FOFD_Document pOFDDoc);

	//VPreferences
	static FOFD_VPreferences		GetVPreferences(FOFD_Document pOFDDoc);

	//Permissions
	static	FOFD_Permissions		GetPermissions(FOFD_Document pOFDDoc);
};

class CFOFD_WriteDoc_V10
{
public:
	static FOFD_WriteDocument		Create(FOFD_Document doc);
	static void						Release(FOFD_WriteDocument writeDoc);
};

class CFOFD_Creator_V10
{
public:
	static FOFD_Creator				Create();
	static void						Release(FOFD_Creator creator);

	/** @brief if be called, InitParser will return false**/
	static FS_BOOL					InsertDocument(FOFD_Creator creator, FOFD_WriteDocument writeDoc, FS_INT32 nIndex);

	/** @brief if be called, InsertDocument will return false**/
	static FS_BOOL					InitParser(FOFD_Creator creator, FOFD_Parser parser);

	static void						Package(FOFD_Creator creator, FS_FileStream fileWrite);
};

class CFOFD_Perms_V10
{
public:
	static FS_BOOL				IsEdit(FOFD_Permissions perms);
	static FS_BOOL				IsAnnot(FOFD_Permissions perms);
	static FS_BOOL				IsExport(FOFD_Permissions perms);
	static FS_BOOL				IsSignature(FOFD_Permissions perms);
	static FS_BOOL				IsWatermark(FOFD_Permissions perms);
	static FS_BOOL				IsPrintScreen(FOFD_Permissions perms);
	static FS_BOOL				IsPrintable(FOFD_Permissions perms);
	static FS_INT32				GetPrintCopies(FOFD_Permissions perms);
	static void					GetStartDate(FOFD_Permissions perms, FS_WideString);
	static void					GetEndDate(FOFD_Permissions perms, FS_WideString);
};

class CFOFD_VPrefers_V10
{
public:
	static void						GetPageMode(FOFD_VPreferences vp, FS_WideString wsValue);
	static void						GetPageLayout(FOFD_VPreferences vp, FS_WideString wsValue);
	static void						GetTabDisplay(FOFD_VPreferences vp, FS_WideString wsValue);
	static FS_BOOL					IsHideToolbar(FOFD_VPreferences vp);
	static FS_BOOL					IsHideMenubar(FOFD_VPreferences vp);
	static FS_BOOL					IsHideWindowUI(FOFD_VPreferences vp);
	static void						GetZoomMode(FOFD_VPreferences vp, FS_WideString wsValue);
	static FS_FLOAT					GetZoom(FOFD_VPreferences vp);
};

class CFOFD_Bookmarks_V10
{
public:
	static FS_DWORD					CountBookmarks(FOFD_Bookmarks pBookmarks);
	static FOFD_Bookmark			GetBookmark(FOFD_Bookmarks bookmarks, FS_DWORD nIndex);
	static FOFD_Bookmark			GetBookmark2(FOFD_Bookmarks bookmarks, FS_WideString wsName);
};

class CFOFD_Bookmark_V10
{
public:
	static void						GetName(FOFD_Bookmark bookmark, FS_WideString wsName);
	static void						GetDest(FOFD_Bookmark bookmark, FOFD_Dest dest);
};

class CFOFD_Outline_V10
{
public:
	static FOFD_Outline				Create(const FOFD_Outline& outline);
	static void						Release(FOFD_Outline outline);
	static FS_BOOL					IsNull(FOFD_Outline outline);
	static FS_BOOL					IsIdentical(FOFD_Outline outline_a, FOFD_Outline outline_b);
	static FOFD_Outline				GenerateParent(FOFD_Outline outline);
	static FOFD_Outline				Clone(FOFD_Outline outline);
	static FS_INT32					CountSubOutlines(FOFD_Outline outline);
	static FOFD_Outline				GenerateSubOutline(FOFD_Outline outline, FS_INT32 nIndex);
	static FS_BOOL					IsExpanded(FOFD_Outline outline);

	static void						GetTitle(FOFD_Outline outline, FS_WideString wsTitle);
	static FOFD_Actions				GetActions(FOFD_Outline outline);
};

class CFOFD_Actions_V10
{
public:
	static FS_INT32					CountActions(FOFD_Actions pActions);
	static FOFD_Action				GetAction(FOFD_Actions pActions, FS_INT32 nIndex);
};

class CFOFD_Action_V10
{
public:
	static void						GetEvent(FOFD_Action pAction, FS_ByteString bsEvent);
	static FOFD_ActionRegion		GenerateActionRegion(FOFD_Action action);
	static OFD_ACTION_TYPE			GetActionType(FOFD_Action pAction);
};

class CFOFD_ActionGoto_V10
{
public:
	static void						GetDest(FOFD_ActionGoto pActionGoto, FOFD_Dest dest);
	static void						GetBookmark(FOFD_ActionGoto pActionGoto, FS_WideString wsBookmark);
};

class CFOFD_ActionGotoA_V10
{
public:
	static void						GetDest(FOFD_ActionGotoA pActionGoto, FOFD_Dest dest);
	static void						GetBookmark(FOFD_ActionGotoA pActionGoto, FS_WideString wsBookmark);
	static FS_DWORD					GetAttachID(FOFD_ActionGotoA pActionGoto);
	static FS_BOOL					IsNewWindow(FOFD_ActionGotoA pActionGoto);
};

class CFOFD_ActionRegion_V10
{
public:
	static FOFD_ActionRegion		New(FOFD_Action pAction);
	static void						Release(FOFD_ActionRegion);
	static FS_BOOL					IsEmpty(FOFD_ActionRegion pActionRegion);
	static FS_INT32					CountAreas(FOFD_ActionRegion pActionRegion);
	static FOFD_ActionArea			GenerateArea(FOFD_ActionRegion pActionRegion, FS_INT32 nIndex);
	static FOFD_Path				GeneratePath(FOFD_ActionRegion pActionRegion);
};

#ifdef __cplusplus
};
#endif

#endif//FOFD_DOCIMPL_H
