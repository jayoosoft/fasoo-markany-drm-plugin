﻿

#ifndef FOFD_RENDERIMPL_H
#define FOFD_RENDERIMPL_H

#ifndef FS_INTERNALINC_H
#include "../../basic/fs_internalInc.h"
#endif

#ifndef FOFD_BASICEXPT_H
#include "../fofd_basicExpt.h"
#endif

#ifndef FOFD_OBJEXPT_H
#include "../fofd_objExpT.h"
#endif

#ifndef FOFD_DOCEXPT_H
#include "../fofd_docExpT.h"
#endif

#ifndef FOFD_PAGEEXPT_H
#include "../fofd_pageExpT.h"
#endif

#ifndef FOFD_RENDEREXPT_H
#include "../fofd_renderExpT.h"
#endif

#ifdef __cplusplus
extern "C"{
#endif

/************************************************************************/
/*                      OFD Render	                                    */
/************************************************************************/
class CFOFD_RenderOptions_V10
{
public:
	static FOFD_RenderOptions		Create(void);
	static void						Release(FOFD_RenderOptions options);
	static void						SetColorMode(FOFD_RenderOptions options, FS_INT32 nMode);
	static FS_INT32					GetColorMode(FOFD_RenderOptions options);
	static void						SetPrinting(FOFD_RenderOptions options, FS_BOOL bPrinting);
	static FS_BOOL					GetPrinting(FOFD_RenderOptions options);
	static void						SetBackColor(FOFD_RenderOptions options, FS_ARGB argb);
	static FS_ARGB					GetBackColor(FOFD_RenderOptions options);
	static void						SetForeColor(FOFD_RenderOptions options, FS_ARGB argb);
	static FS_ARGB					GetForeColor(FOFD_RenderOptions options);
	static void						SetRotate(FOFD_RenderOptions options, FS_INT32 nRotate);
	static FS_INT32					GetRotate(FOFD_RenderOptions options);
	static void						SetRenderFlag(FOFD_RenderOptions options, FS_INT32 nFlags);
	static FS_INT32					GetRenderFlag(FOFD_RenderOptions options);
	static void						SetLayerFlag(FOFD_RenderOptions options, FS_INT32 nFlags);
	static FS_INT32					GetLayerFlag(FOFD_RenderOptions options);
};

class CFOFD_RenderContext_V10
{
public:
	static FOFD_RenderContext		Create();
	static void						Release(FOFD_RenderContext context);
	static void						AddPage(FOFD_RenderContext context, FOFD_Page pPage,const FS_AffineMatrix* pObject2Device);
	static void						AddPageObject(FOFD_RenderContext context, FOFD_Page pPage, FOFD_ContentObject pObj, const FS_AffineMatrix* pObject2Device);
};

class CFOFD_RenderDevice_V10
{
public:
	static FOFD_RenderDevice		New();
	static void						Destroy(FOFD_RenderDevice device);
	static void						SaveBitmap(FS_DIBitmap pDIBitmap, FS_WideString sPathBMP);
	static FS_BOOL					Create(FOFD_RenderDevice device, FOFD_DriverDevice pDriverDevice);
	static FS_BOOL					Create2(FOFD_RenderDevice device, FPD_RenderDevice renderDevice, FS_BOOL isAntialiasing, void* hDC);
	static FS_INT32					GetWidth(FOFD_RenderDevice device);
	static FS_INT32					GetHeight(FOFD_RenderDevice device);
	static FS_INT32					GetDeviceClass(FOFD_RenderDevice device);
	static FS_INT32					GetBPP(FOFD_RenderDevice device);
	static FS_INT32					GetRenderCaps(FOFD_RenderDevice device);
	static FS_INT32					GetDeviceCaps(FOFD_RenderDevice device, int id);
	static FS_INT32					GetDpiX(FOFD_RenderDevice device);
	static FS_INT32					GetDpiY(FOFD_RenderDevice device);

	static FS_DIBitmap				GetBitmap(FOFD_RenderDevice device);
	static void						SetBitmap(FOFD_RenderDevice device, FS_DIBitmap pBitmap);

	static FS_BOOL					CreateCompatibleBitmap(FOFD_RenderDevice device, FS_DIBitmap pDIB, int width, int height);
	static FS_Rect					GetClipBox(FOFD_RenderDevice device);
	static FS_BOOL					SetClip_PathFill(FOFD_RenderDevice device, 
													 const FPD_Path path,
													 const FS_AffineMatrix* pObject2Device,
													 int fill_mode);
	static void						SaveState(FOFD_RenderDevice device);
	static void						RestoreState(FOFD_RenderDevice device, FS_BOOL bKeepSaved);
	static FS_BOOL					SetClip_Rect(FOFD_RenderDevice device, const FS_Rect* pRect);
	static FS_BOOL					FillRect(FOFD_RenderDevice device, const FS_Rect* pRect, FS_DWORD color, 
											 int alpha_flag, void* pIccTransform);
	static FS_BOOL					GetDIBits(FOFD_RenderDevice device, FS_DIBitmap pBitmap, int left, int top, void* pIccTransform);
	static FS_BOOL					SetDIBits(FOFD_RenderDevice device, const FS_DIBitmap pBitmap, int left, int top, int blend_type, 
											  void* pIccTransform);
	static FS_BOOL					StretchDIBits(FOFD_RenderDevice device, const FS_DIBitmap pBitmap, int left, int top, int dest_width, int dest_height,
												  FS_DWORD flags, void* pIccTransform, int blend_type);

	static FS_BOOL					StartDIBits(FOFD_RenderDevice device, const FS_DIBitmap pBitmap, int bitmap_alpha, FS_DWORD color, 
												const FS_AffineMatrix* pMatrix, FS_DWORD flags, FS_LPVOID& handle,
												int alpha_flag, void* pIccTransform);
	static FS_BOOL					ContinueDIBits(FOFD_RenderDevice device, FS_LPVOID handle, FOFD_PauseHandler pPause);
	static void						CancelDIBits(FOFD_RenderDevice device, FS_LPVOID handle);
	static FS_INT32					SaveDevice(FOFD_RenderDevice device);
	static void						RestoreDevice(FOFD_RenderDevice device, FS_INT32 state);

	static void						SetClipPathFill(FOFD_RenderDevice device, const FPD_Path path,const FS_AffineMatrix *pMatrix,OFD_RENDER_FILLRULE FillMode);

	static void						SetRenderOptions(FOFD_RenderDevice device, const FOFD_RenderOptions pRenderOptions);
	static FPD_RenderDevice			GetRenderDevice(FOFD_RenderDevice device);
	static FOFD_DriverDevice		GetDriverDevice(FOFD_RenderDevice device);
};

class CFOFD_ProgressiveRenderer_V10
{
public:
	static FOFD_ProgressiveRenderer	Create();
	static void						Release(FOFD_ProgressiveRenderer pProRenderer);
	static FS_BOOL					StartRender(FOFD_ProgressiveRenderer pProRenderer, FOFD_RenderDevice pRenderDevice,
												FOFD_RenderContext pRenderContext,const FOFD_RenderOptions pOption, 
												const FS_AffineMatrix* pFinalMatrix, const FS_DIBitmap pPrinterBackgroud);
	static FS_BOOL					Continue(FOFD_ProgressiveRenderer pProRenderer, FOFD_PauseHandler pPauseHandler, FS_BOOL bShowAnnot, FS_BOOL bShowStamp, FS_BOOL bSealAllGray);
	static OFD_RENDER_RENDERSTATUS	GetStatus(FOFD_ProgressiveRenderer pProRenderer);
	static FS_BOOL					DoRender(FOFD_ProgressiveRenderer pProRenderer, FOFD_PauseHandler pPauseHandler);
	static FS_BOOL					RenderAnnots(FOFD_ProgressiveRenderer pProRenderer, FOFD_Page pPage,FS_BOOL isHighlight, int blendmode);
	static int						RenderAnnot(FOFD_ProgressiveRenderer pProRenderer, FOFD_Page pPage, const FOFD_Annot pAnnot, int blendmode);
	static FS_BOOL					RenderStampAnnots(FOFD_ProgressiveRenderer pProRenderer, FS_BOOL bSealAllGray);
	static void						StopRender(FOFD_ProgressiveRenderer pProRenderer);
	static FS_DIBitmap				GetPrinterBackgourd(FOFD_ProgressiveRenderer pProRenderer);
};

#ifdef __cplusplus
};
#endif

#endif//FOFD_PAGEIMPL_H
