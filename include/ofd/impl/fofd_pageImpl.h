﻿

#ifndef FOFD_PAGEIMPL_H
#define FOFD_PAGEIMPL_H

#ifndef FS_INTERNALINC_H
#include "../../basic/fs_internalInc.h"
#endif

#ifndef FOFD_BASICEXPT_H
#include "../fofd_basicExpt.h"
#endif

#ifndef FOFD_OBJEXPT_H
#include "../fofd_objExpT.h"
#endif

#ifndef FOFD_DOCEXPT_H
#include "../fofd_docExpT.h"
#endif

#ifndef FOFD_PAGEEXPT_H
#include "../fofd_pageExpT.h"
#endif

#ifdef __cplusplus
extern "C"{
#endif

class CFOFD_Page_V10
{
public:
	static FS_DWORD						GetID(FOFD_Page pPage);
	static FOFD_Document				GetDocument(FOFD_Page pPage);

	static FOFD_RectF					GetPageRect(FOFD_Page pPage);
	static FS_INT32						GetPageRotate(FOFD_Page pPage);

	static FS_BOOL						LoadPage(FOFD_Page pPage);
	static void							UnloadPage(FOFD_Page pPage);

	static FS_BOOL						IsPageLoaded(FOFD_Page pPage);

	static void 						ReleaseCatchImage(FOFD_Page pPage);

	static FS_AffineMatrix				GetMatrix(const FOFD_RectF& docPageRect, 
												  const FS_Rect& devicePageRect, FS_INT32 iRotate);

	static FOFD_RectF					GetCropPageRect(FOFD_Page pPage);
};

class CFOFD_Dest_V10
{
public:
	static FOFD_Dest					Create();
	static void							Release(FOFD_Dest dest);
	static FS_BOOL						IsValid(FOFD_Dest dest);
	static FS_DWORD						GetPageID(FOFD_Dest dest);
	static OFD_PAGEDEST_TYPE			GetDestType(FOFD_Dest dest);
	static FS_FLOAT						GetLeft(FOFD_Dest dest);
	static FS_FLOAT						GetTop(FOFD_Dest dest);
	static FS_FLOAT						GetRight(FOFD_Dest dest);
	static FS_FLOAT						GetBottom(FOFD_Dest dest);
	static FS_FLOAT						GetZoom(FOFD_Dest dest);
};

#ifdef __cplusplus
};
#endif

#endif//FOFD_PAGEIMPL_H
