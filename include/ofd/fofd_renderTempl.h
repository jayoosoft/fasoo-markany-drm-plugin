﻿/*****************************************************************************

Copyright (C) 2010 Foxit Corporation
All rights reserved.

NOTICE: Foxit permits you to use, modify, and distribute this file
in accordance with the terms of the Foxit license agreement
accompanying it. If you have received this file from a source other
than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
 
*****************************************************************************/

//----------_V1----------
//----------_V2----------
//----------_V3----------
//----------_V4----------
//----------_V5----------
//----------_V6----------
//----------_V7----------
//----------_V8----------
//----------_V9----------
//----------_V10----------
//*****************************
/* RenderOptions HFT functions */
//*****************************

BEGINENUM
INTERFACE(FOFD_RenderOptions, FOFDRenderOptionsCreate, (void))

INTERFACE(void, FOFDRenderOptionsRelease, (FOFD_RenderOptions options))

INTERFACE(void, FOFDRenderOptionsSetColorMode, (FOFD_RenderOptions options,  FS_INT32 nMode))

INTERFACE(FS_INT32, FOFDRenderOptionsGetColorMode, (FOFD_RenderOptions options))

INTERFACE(void, FOFDRenderOptionsSetPrinting, (FOFD_RenderOptions options,  FS_BOOL bPrinting))

INTERFACE(FS_BOOL, FOFDRenderOptionsGetPrinting, (FOFD_RenderOptions options))

INTERFACE(void, FOFDRenderOptionsSetBackColor, (FOFD_RenderOptions options,  FS_ARGB argb))

INTERFACE(FS_ARGB, FOFDRenderOptionsGetBackColor, (FOFD_RenderOptions options))

INTERFACE(void, FOFDRenderOptionsSetForeColor, (FOFD_RenderOptions options,  FS_ARGB argb))

INTERFACE(FS_ARGB, FOFDRenderOptionsGetForeColor, (FOFD_RenderOptions options))

INTERFACE(void, FOFDRenderOptionsSetRotate, (FOFD_RenderOptions options,  FS_INT32 nRotate))

INTERFACE(FS_INT32, FOFDRenderOptionsGetRotate, (FOFD_RenderOptions options))

INTERFACE(void, FOFDRenderOptionsSetRenderFlag, (FOFD_RenderOptions options,  FS_INT32 nFlags))

INTERFACE(FS_INT32, FOFDRenderOptionsGetRenderFlag, (FOFD_RenderOptions options))

INTERFACE(void, FOFDRenderOptionsSetLayerFlag, (FOFD_RenderOptions options,  FS_INT32 nFlags))

INTERFACE(FS_INT32, FOFDRenderOptionsGetLayerFlag, (FOFD_RenderOptions options))

NumOfSelector(FOFDRenderOptions)
ENDENUM

//*****************************
/* RenderContext HFT functions */
//*****************************

BEGINENUM
INTERFACE(FOFD_RenderContext, FOFDRenderContextCreate, ())

INTERFACE(void, FOFDRenderContextRelease, (FOFD_RenderContext context))

INTERFACE(void, FOFDRenderContextAddPage, (FOFD_RenderContext context,  FOFD_Page pPage,  const FS_AffineMatrix* pObject2Device))

INTERFACE(void, FOFDRenderContextAddPageObject, (FOFD_RenderContext context,  FOFD_Page pPage,  FOFD_ContentObject pObj,  const FS_AffineMatrix* pObject2Device))

NumOfSelector(FOFDRenderContext)
ENDENUM

//*****************************
/* RenderDevice HFT functions */
//*****************************

BEGINENUM
INTERFACE(FOFD_RenderDevice, FOFDRenderDeviceNew, ())

INTERFACE(void, FOFDRenderDeviceDestroy, (FOFD_RenderDevice device))

INTERFACE(void, FOFDRenderDeviceSaveBitmap, (FS_DIBitmap pDIBitmap,  FS_WideString sPathBMP))

INTERFACE(FS_BOOL, FOFDRenderDeviceCreate, (FOFD_RenderDevice device,  FOFD_DriverDevice pDriverDevice))

INTERFACE(FS_BOOL, FOFDRenderDeviceCreate2, (FOFD_RenderDevice device,  FPD_RenderDevice renderDevice,  FS_BOOL isAntialiasing,  void* hDC))

INTERFACE(FS_INT32, FOFDRenderDeviceGetWidth, (FOFD_RenderDevice device))

INTERFACE(FS_INT32, FOFDRenderDeviceGetHeight, (FOFD_RenderDevice device))

INTERFACE(FS_INT32, FOFDRenderDeviceGetDeviceClass, (FOFD_RenderDevice device))

INTERFACE(FS_INT32, FOFDRenderDeviceGetBPP, (FOFD_RenderDevice device))

INTERFACE(FS_INT32, FOFDRenderDeviceGetRenderCaps, (FOFD_RenderDevice device))

INTERFACE(FS_INT32, FOFDRenderDeviceGetDeviceCaps, (FOFD_RenderDevice device,  int id))

INTERFACE(FS_INT32, FOFDRenderDeviceGetDpiX, (FOFD_RenderDevice device))

INTERFACE(FS_INT32, FOFDRenderDeviceGetDpiY, (FOFD_RenderDevice device))

INTERFACE(FS_DIBitmap, FOFDRenderDeviceGetBitmap, (FOFD_RenderDevice device))

INTERFACE(void, FOFDRenderDeviceSetBitmap, (FOFD_RenderDevice device,  FS_DIBitmap pBitmap))

INTERFACE(FS_BOOL, FOFDRenderDeviceCreateCompatibleBitmap, (FOFD_RenderDevice device,  FS_DIBitmap pDIB,  int width,  int height))

INTERFACE(FS_Rect, FOFDRenderDeviceGetClipBox, (FOFD_RenderDevice device))

INTERFACE(FS_BOOL, FOFDRenderDeviceSetClip_PathFill, (FOFD_RenderDevice device,  const FPD_Path path,  const FS_AffineMatrix* pObject2Device,  int fill_mode))

INTERFACE(void, FOFDRenderDeviceSaveState, (FOFD_RenderDevice device))

INTERFACE(void, FOFDRenderDeviceRestoreState, (FOFD_RenderDevice device,  FS_BOOL bKeepSaved))

INTERFACE(FS_BOOL, FOFDRenderDeviceSetClip_Rect, (FOFD_RenderDevice device,  const FS_Rect* pRect))

INTERFACE(FS_BOOL, FOFDRenderDeviceFillRect, (FOFD_RenderDevice device,  const FS_Rect* pRect,  FS_DWORD color,  int alpha_flag,  void* pIccTransform))

INTERFACE(FS_BOOL, FOFDRenderDeviceGetDIBits, (FOFD_RenderDevice device,  FS_DIBitmap pBitmap,  int left,  int top,  void* pIccTransform))

INTERFACE(FS_BOOL, FOFDRenderDeviceSetDIBits, (FOFD_RenderDevice device,  const FS_DIBitmap pBitmap,  int left,  int top,  int blend_type,  void* pIccTransform))

INTERFACE(FS_BOOL, FOFDRenderDeviceStretchDIBits, (FOFD_RenderDevice device,  const FS_DIBitmap pBitmap,  int left,  int top,  int dest_width,  int dest_height,  FS_DWORD flags,  void* pIccTransform,  int blend_type))

INTERFACE(FS_BOOL, FOFDRenderDeviceStartDIBits, (FOFD_RenderDevice device,  const FS_DIBitmap pBitmap,  int bitmap_alpha,  FS_DWORD color,  const FS_AffineMatrix* pMatrix,  FS_DWORD flags,  FS_LPVOID& handle,  int alpha_flag,  void* pIccTransform))

INTERFACE(FS_BOOL, FOFDRenderDeviceContinueDIBits, (FOFD_RenderDevice device,  FS_LPVOID handle,  FOFD_PauseHandler pPause))

INTERFACE(void, FOFDRenderDeviceCancelDIBits, (FOFD_RenderDevice device,  FS_LPVOID handle))

INTERFACE(FS_INT32, FOFDRenderDeviceSaveDevice, (FOFD_RenderDevice device))

INTERFACE(void, FOFDRenderDeviceRestoreDevice, (FOFD_RenderDevice device,  FS_INT32 state))

INTERFACE(void, FOFDRenderDeviceSetClipPathFill, (FOFD_RenderDevice device,  const FPD_Path path,  const FS_AffineMatrix *pMatrix,  OFD_RENDER_FILLRULE FillMode))

INTERFACE(void, FOFDRenderDeviceSetRenderOptions, (FOFD_RenderDevice device,  const FOFD_RenderOptions pRenderOptions))

INTERFACE(FPD_RenderDevice, FOFDRenderDeviceGetRenderDevice, (FOFD_RenderDevice device))

INTERFACE(FOFD_DriverDevice, FOFDRenderDeviceGetDriverDevice, (FOFD_RenderDevice device))

NumOfSelector(FOFDRenderDevice)
ENDENUM

//*****************************
/* ProgressiveRenderer HFT functions */
//*****************************

BEGINENUM
INTERFACE(FOFD_ProgressiveRenderer, FOFDProgressiveRendererCreate, ())

INTERFACE(void, FOFDProgressiveRendererRelease, (FOFD_ProgressiveRenderer pProRenderer))

INTERFACE(FS_BOOL, FOFDProgressiveRendererStartRender, (FOFD_ProgressiveRenderer pProRenderer,  FOFD_RenderDevice pRenderDevice,  FOFD_RenderContext pRenderContext,  const FOFD_RenderOptions pOption,  const FS_AffineMatrix* pFinalMatrix,  const FS_DIBitmap pPrinterBackgroud))

INTERFACE(FS_BOOL, FOFDProgressiveRendererContinue, (FOFD_ProgressiveRenderer pProRenderer,  FOFD_PauseHandler pPauseHandler,  FS_BOOL bShowAnnot,  FS_BOOL bShowStamp,  FS_BOOL bSealAllGray))

INTERFACE(OFD_RENDER_RENDERSTATUS, FOFDProgressiveRendererGetStatus, (FOFD_ProgressiveRenderer pProRenderer))

INTERFACE(FS_BOOL, FOFDProgressiveRendererDoRender, (FOFD_ProgressiveRenderer pProRenderer,  FOFD_PauseHandler pPauseHandler))

INTERFACE(FS_BOOL, FOFDProgressiveRendererRenderAnnots, (FOFD_ProgressiveRenderer pProRenderer,  FOFD_Page pPage,  FS_BOOL isHighlight,  int blendmode))

INTERFACE(int, FOFDProgressiveRendererRenderAnnot, (FOFD_ProgressiveRenderer pProRenderer,  FOFD_Page pPage,  const FOFD_Annot pAnnot,  int blendmode))

INTERFACE(FS_BOOL, FOFDProgressiveRendererRenderStampAnnots, (FOFD_ProgressiveRenderer pProRenderer,  FS_BOOL bSealAllGray))

INTERFACE(void, FOFDProgressiveRendererStopRender, (FOFD_ProgressiveRenderer pProRenderer))

INTERFACE(FS_DIBitmap, FOFDProgressiveRendererGetPrinterBackgourd, (FOFD_ProgressiveRenderer pProRenderer))

NumOfSelector(FOFDProgressiveRenderer)
ENDENUM

//----------_V11----------
//----------_V12----------
//----------_V13----------
