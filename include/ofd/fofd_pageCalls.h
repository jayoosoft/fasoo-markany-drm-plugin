﻿/*****************************************************************************

Copyright (C) 2010 Foxit Corporation
All rights reserved.

NOTICE: Foxit permits you to use, modify, and distribute this file
in accordance with the terms of the Foxit license agreement
accompanying it. If you have received this file from a source other
than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
 
*****************************************************************************/

#ifndef FOFD_PAGECALLS_H
#define FOFD_PAGECALLS_H

#ifndef FS_COMMON_H
#include "../basic/fs_common.h"
#endif

#ifndef FS_BASICEXPT_H
#include "../basic/fs_basicExpT.h"
#endif

#ifndef FS_STRINGEXPT_H
#include "../basic/fs_stringExpT.h"
#endif

#ifndef FOFD_PAGEEXPT_H
#include "fofd_pageExpT.h"
#endif

#ifdef __cplusplus
extern "C"{
#endif

#undef INTERFACE
#undef BEGINENUM
#undef NumOfSelector
#undef ENDENUM

// Define macros to generate function selectors in the file fofd_pageImpl.h
#define BEGINENUM enum{
#define NumOfSelector(name) name##InterfacesNum
#define ENDENUM };
#define INTERFACE(returnType,interfaceName,params) interfaceName##SEL,
#include "fofd_pageTempl.h"

#undef INTERFACE
#undef BEGINENUM
#undef NumOfSelector
#undef ENDENUM

// Define macros to generate function PROTO in the file fofd_pageImpl.h
#define BEGINENUM
#define NumOfSelector(name)
#define ENDENUM
#define INTERFACE(returnType,interfaceName, params) \
typedef returnType (*interfaceName##SELPROTO)params;
#include "fofd_pageTempl.h"

//----------_V1----------
//----------_V2----------
//----------_V3----------
//----------_V4----------
//----------_V5----------
//----------_V6----------
//----------_V7----------
//----------_V8----------
//----------_V9----------
//----------_V10----------
//*****************************
/* Page HFT functions */
//*****************************

#define FOFDPageGetID (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPageGetIDSELPROTO)FRCOREROUTINE(FOFDPageSEL,FOFDPageGetIDSEL, _gPID)))

#define FOFDPageGetDocument (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPageGetDocumentSELPROTO)FRCOREROUTINE(FOFDPageSEL,FOFDPageGetDocumentSEL, _gPID)))

#define FOFDPageGetPageRect (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPageGetPageRectSELPROTO)FRCOREROUTINE(FOFDPageSEL,FOFDPageGetPageRectSEL, _gPID)))

#define FOFDPageGetPageRotate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPageGetPageRotateSELPROTO)FRCOREROUTINE(FOFDPageSEL,FOFDPageGetPageRotateSEL, _gPID)))

#define FOFDPageLoadPage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPageLoadPageSELPROTO)FRCOREROUTINE(FOFDPageSEL,FOFDPageLoadPageSEL, _gPID)))

#define FOFDPageUnloadPage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPageUnloadPageSELPROTO)FRCOREROUTINE(FOFDPageSEL,FOFDPageUnloadPageSEL, _gPID)))

#define FOFDPageIsPageLoaded (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPageIsPageLoadedSELPROTO)FRCOREROUTINE(FOFDPageSEL,FOFDPageIsPageLoadedSEL, _gPID)))

#define FOFDPageReleaseCatchImage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPageReleaseCatchImageSELPROTO)FRCOREROUTINE(FOFDPageSEL,FOFDPageReleaseCatchImageSEL, _gPID)))

#define FOFDPageGetMatrix (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPageGetMatrixSELPROTO)FRCOREROUTINE(FOFDPageSEL,FOFDPageGetMatrixSEL, _gPID)))

#define FOFDPageGetCropPageRect (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPageGetCropPageRectSELPROTO)FRCOREROUTINE(FOFDPageSEL,FOFDPageGetCropPageRectSEL, _gPID)))

//*****************************
/* Dest HFT functions */
//*****************************

#define FOFDDestCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDestCreateSELPROTO)FRCOREROUTINE(FOFDDestSEL,FOFDDestCreateSEL, _gPID)))

#define FOFDDestRelease (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDestReleaseSELPROTO)FRCOREROUTINE(FOFDDestSEL,FOFDDestReleaseSEL, _gPID)))

#define FOFDDestIsValid (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDestIsValidSELPROTO)FRCOREROUTINE(FOFDDestSEL,FOFDDestIsValidSEL, _gPID)))

#define FOFDDestGetPageID (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDestGetPageIDSELPROTO)FRCOREROUTINE(FOFDDestSEL,FOFDDestGetPageIDSEL, _gPID)))

#define FOFDDestGetDestType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDestGetDestTypeSELPROTO)FRCOREROUTINE(FOFDDestSEL,FOFDDestGetDestTypeSEL, _gPID)))

#define FOFDDestGetLeft (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDestGetLeftSELPROTO)FRCOREROUTINE(FOFDDestSEL,FOFDDestGetLeftSEL, _gPID)))

#define FOFDDestGetTop (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDestGetTopSELPROTO)FRCOREROUTINE(FOFDDestSEL,FOFDDestGetTopSEL, _gPID)))

#define FOFDDestGetRight (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDestGetRightSELPROTO)FRCOREROUTINE(FOFDDestSEL,FOFDDestGetRightSEL, _gPID)))

#define FOFDDestGetBottom (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDestGetBottomSELPROTO)FRCOREROUTINE(FOFDDestSEL,FOFDDestGetBottomSEL, _gPID)))

#define FOFDDestGetZoom (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDestGetZoomSELPROTO)FRCOREROUTINE(FOFDDestSEL,FOFDDestGetZoomSEL, _gPID)))

//----------_V11----------
//----------_V12----------
//----------_V13----------
#ifdef __cplusplus
}
#endif

#endif