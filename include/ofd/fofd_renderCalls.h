﻿/*****************************************************************************

Copyright (C) 2010 Foxit Corporation
All rights reserved.

NOTICE: Foxit permits you to use, modify, and distribute this file
in accordance with the terms of the Foxit license agreement
accompanying it. If you have received this file from a source other
than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
 
*****************************************************************************/

#ifndef FOFD_RENDERCALLS_H
#define FOFD_RENDERCALLS_H

#ifndef FS_COMMON_H
#include "../basic/fs_common.h"
#endif

#ifndef FS_BASICEXPT_H
#include "../basic/fs_basicExpT.h"
#endif

#ifndef FS_STRINGEXPT_H
#include "../basic/fs_stringExpT.h"
#endif

#ifndef FOFD_RENDEREXPT_H
#include "fofd_renderExpT.h"
#endif

#ifdef __cplusplus
extern "C"{
#endif

#undef INTERFACE
#undef BEGINENUM
#undef NumOfSelector
#undef ENDENUM

// Define macros to generate function selectors in the file fofd_renderImpl.h
#define BEGINENUM enum{
#define NumOfSelector(name) name##InterfacesNum
#define ENDENUM };
#define INTERFACE(returnType,interfaceName,params) interfaceName##SEL,
#include "fofd_renderTempl.h"

#undef INTERFACE
#undef BEGINENUM
#undef NumOfSelector
#undef ENDENUM

// Define macros to generate function PROTO in the file fofd_renderImpl.h
#define BEGINENUM
#define NumOfSelector(name)
#define ENDENUM
#define INTERFACE(returnType,interfaceName, params) \
typedef returnType (*interfaceName##SELPROTO)params;
#include "fofd_renderTempl.h"

//----------_V1----------
//----------_V2----------
//----------_V3----------
//----------_V4----------
//----------_V5----------
//----------_V6----------
//----------_V7----------
//----------_V8----------
//----------_V9----------
//----------_V10----------
//*****************************
/* RenderOptions HFT functions */
//*****************************

#define FOFDRenderOptionsCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderOptionsCreateSELPROTO)FRCOREROUTINE(FOFDRenderOptionsSEL,FOFDRenderOptionsCreateSEL, _gPID)))

#define FOFDRenderOptionsRelease (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderOptionsReleaseSELPROTO)FRCOREROUTINE(FOFDRenderOptionsSEL,FOFDRenderOptionsReleaseSEL, _gPID)))

#define FOFDRenderOptionsSetColorMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderOptionsSetColorModeSELPROTO)FRCOREROUTINE(FOFDRenderOptionsSEL,FOFDRenderOptionsSetColorModeSEL, _gPID)))

#define FOFDRenderOptionsGetColorMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderOptionsGetColorModeSELPROTO)FRCOREROUTINE(FOFDRenderOptionsSEL,FOFDRenderOptionsGetColorModeSEL, _gPID)))

#define FOFDRenderOptionsSetPrinting (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderOptionsSetPrintingSELPROTO)FRCOREROUTINE(FOFDRenderOptionsSEL,FOFDRenderOptionsSetPrintingSEL, _gPID)))

#define FOFDRenderOptionsGetPrinting (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderOptionsGetPrintingSELPROTO)FRCOREROUTINE(FOFDRenderOptionsSEL,FOFDRenderOptionsGetPrintingSEL, _gPID)))

#define FOFDRenderOptionsSetBackColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderOptionsSetBackColorSELPROTO)FRCOREROUTINE(FOFDRenderOptionsSEL,FOFDRenderOptionsSetBackColorSEL, _gPID)))

#define FOFDRenderOptionsGetBackColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderOptionsGetBackColorSELPROTO)FRCOREROUTINE(FOFDRenderOptionsSEL,FOFDRenderOptionsGetBackColorSEL, _gPID)))

#define FOFDRenderOptionsSetForeColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderOptionsSetForeColorSELPROTO)FRCOREROUTINE(FOFDRenderOptionsSEL,FOFDRenderOptionsSetForeColorSEL, _gPID)))

#define FOFDRenderOptionsGetForeColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderOptionsGetForeColorSELPROTO)FRCOREROUTINE(FOFDRenderOptionsSEL,FOFDRenderOptionsGetForeColorSEL, _gPID)))

#define FOFDRenderOptionsSetRotate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderOptionsSetRotateSELPROTO)FRCOREROUTINE(FOFDRenderOptionsSEL,FOFDRenderOptionsSetRotateSEL, _gPID)))

#define FOFDRenderOptionsGetRotate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderOptionsGetRotateSELPROTO)FRCOREROUTINE(FOFDRenderOptionsSEL,FOFDRenderOptionsGetRotateSEL, _gPID)))

#define FOFDRenderOptionsSetRenderFlag (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderOptionsSetRenderFlagSELPROTO)FRCOREROUTINE(FOFDRenderOptionsSEL,FOFDRenderOptionsSetRenderFlagSEL, _gPID)))

#define FOFDRenderOptionsGetRenderFlag (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderOptionsGetRenderFlagSELPROTO)FRCOREROUTINE(FOFDRenderOptionsSEL,FOFDRenderOptionsGetRenderFlagSEL, _gPID)))

#define FOFDRenderOptionsSetLayerFlag (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderOptionsSetLayerFlagSELPROTO)FRCOREROUTINE(FOFDRenderOptionsSEL,FOFDRenderOptionsSetLayerFlagSEL, _gPID)))

#define FOFDRenderOptionsGetLayerFlag (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderOptionsGetLayerFlagSELPROTO)FRCOREROUTINE(FOFDRenderOptionsSEL,FOFDRenderOptionsGetLayerFlagSEL, _gPID)))

//*****************************
/* RenderContext HFT functions */
//*****************************

#define FOFDRenderContextCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderContextCreateSELPROTO)FRCOREROUTINE(FOFDRenderContextSEL,FOFDRenderContextCreateSEL, _gPID)))

#define FOFDRenderContextRelease (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderContextReleaseSELPROTO)FRCOREROUTINE(FOFDRenderContextSEL,FOFDRenderContextReleaseSEL, _gPID)))

#define FOFDRenderContextAddPage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderContextAddPageSELPROTO)FRCOREROUTINE(FOFDRenderContextSEL,FOFDRenderContextAddPageSEL, _gPID)))

#define FOFDRenderContextAddPageObject (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderContextAddPageObjectSELPROTO)FRCOREROUTINE(FOFDRenderContextSEL,FOFDRenderContextAddPageObjectSEL, _gPID)))

//*****************************
/* RenderDevice HFT functions */
//*****************************

#define FOFDRenderDeviceNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceNewSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceNewSEL, _gPID)))

#define FOFDRenderDeviceDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceDestroySELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceDestroySEL, _gPID)))

#define FOFDRenderDeviceSaveBitmap (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceSaveBitmapSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceSaveBitmapSEL, _gPID)))

#define FOFDRenderDeviceCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceCreateSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceCreateSEL, _gPID)))

#define FOFDRenderDeviceCreate2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceCreate2SELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceCreate2SEL, _gPID)))

#define FOFDRenderDeviceGetWidth (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceGetWidthSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceGetWidthSEL, _gPID)))

#define FOFDRenderDeviceGetHeight (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceGetHeightSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceGetHeightSEL, _gPID)))

#define FOFDRenderDeviceGetDeviceClass (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceGetDeviceClassSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceGetDeviceClassSEL, _gPID)))

#define FOFDRenderDeviceGetBPP (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceGetBPPSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceGetBPPSEL, _gPID)))

#define FOFDRenderDeviceGetRenderCaps (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceGetRenderCapsSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceGetRenderCapsSEL, _gPID)))

#define FOFDRenderDeviceGetDeviceCaps (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceGetDeviceCapsSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceGetDeviceCapsSEL, _gPID)))

#define FOFDRenderDeviceGetDpiX (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceGetDpiXSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceGetDpiXSEL, _gPID)))

#define FOFDRenderDeviceGetDpiY (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceGetDpiYSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceGetDpiYSEL, _gPID)))

#define FOFDRenderDeviceGetBitmap (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceGetBitmapSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceGetBitmapSEL, _gPID)))

#define FOFDRenderDeviceSetBitmap (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceSetBitmapSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceSetBitmapSEL, _gPID)))

#define FOFDRenderDeviceCreateCompatibleBitmap (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceCreateCompatibleBitmapSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceCreateCompatibleBitmapSEL, _gPID)))

#define FOFDRenderDeviceGetClipBox (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceGetClipBoxSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceGetClipBoxSEL, _gPID)))

#define FOFDRenderDeviceSetClip_PathFill (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceSetClip_PathFillSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceSetClip_PathFillSEL, _gPID)))

#define FOFDRenderDeviceSaveState (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceSaveStateSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceSaveStateSEL, _gPID)))

#define FOFDRenderDeviceRestoreState (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceRestoreStateSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceRestoreStateSEL, _gPID)))

#define FOFDRenderDeviceSetClip_Rect (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceSetClip_RectSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceSetClip_RectSEL, _gPID)))

#define FOFDRenderDeviceFillRect (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceFillRectSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceFillRectSEL, _gPID)))

#define FOFDRenderDeviceGetDIBits (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceGetDIBitsSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceGetDIBitsSEL, _gPID)))

#define FOFDRenderDeviceSetDIBits (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceSetDIBitsSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceSetDIBitsSEL, _gPID)))

#define FOFDRenderDeviceStretchDIBits (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceStretchDIBitsSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceStretchDIBitsSEL, _gPID)))

#define FOFDRenderDeviceStartDIBits (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceStartDIBitsSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceStartDIBitsSEL, _gPID)))

#define FOFDRenderDeviceContinueDIBits (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceContinueDIBitsSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceContinueDIBitsSEL, _gPID)))

#define FOFDRenderDeviceCancelDIBits (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceCancelDIBitsSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceCancelDIBitsSEL, _gPID)))

#define FOFDRenderDeviceSaveDevice (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceSaveDeviceSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceSaveDeviceSEL, _gPID)))

#define FOFDRenderDeviceRestoreDevice (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceRestoreDeviceSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceRestoreDeviceSEL, _gPID)))

#define FOFDRenderDeviceSetClipPathFill (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceSetClipPathFillSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceSetClipPathFillSEL, _gPID)))

#define FOFDRenderDeviceSetRenderOptions (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceSetRenderOptionsSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceSetRenderOptionsSEL, _gPID)))

#define FOFDRenderDeviceGetRenderDevice (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceGetRenderDeviceSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceGetRenderDeviceSEL, _gPID)))

#define FOFDRenderDeviceGetDriverDevice (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDRenderDeviceGetDriverDeviceSELPROTO)FRCOREROUTINE(FOFDRenderDeviceSEL,FOFDRenderDeviceGetDriverDeviceSEL, _gPID)))

//*****************************
/* ProgressiveRenderer HFT functions */
//*****************************

#define FOFDProgressiveRendererCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDProgressiveRendererCreateSELPROTO)FRCOREROUTINE(FOFDProgressiveRendererSEL,FOFDProgressiveRendererCreateSEL, _gPID)))

#define FOFDProgressiveRendererRelease (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDProgressiveRendererReleaseSELPROTO)FRCOREROUTINE(FOFDProgressiveRendererSEL,FOFDProgressiveRendererReleaseSEL, _gPID)))

#define FOFDProgressiveRendererStartRender (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDProgressiveRendererStartRenderSELPROTO)FRCOREROUTINE(FOFDProgressiveRendererSEL,FOFDProgressiveRendererStartRenderSEL, _gPID)))

#define FOFDProgressiveRendererContinue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDProgressiveRendererContinueSELPROTO)FRCOREROUTINE(FOFDProgressiveRendererSEL,FOFDProgressiveRendererContinueSEL, _gPID)))

#define FOFDProgressiveRendererGetStatus (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDProgressiveRendererGetStatusSELPROTO)FRCOREROUTINE(FOFDProgressiveRendererSEL,FOFDProgressiveRendererGetStatusSEL, _gPID)))

#define FOFDProgressiveRendererDoRender (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDProgressiveRendererDoRenderSELPROTO)FRCOREROUTINE(FOFDProgressiveRendererSEL,FOFDProgressiveRendererDoRenderSEL, _gPID)))

#define FOFDProgressiveRendererRenderAnnots (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDProgressiveRendererRenderAnnotsSELPROTO)FRCOREROUTINE(FOFDProgressiveRendererSEL,FOFDProgressiveRendererRenderAnnotsSEL, _gPID)))

#define FOFDProgressiveRendererRenderAnnot (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDProgressiveRendererRenderAnnotSELPROTO)FRCOREROUTINE(FOFDProgressiveRendererSEL,FOFDProgressiveRendererRenderAnnotSEL, _gPID)))

#define FOFDProgressiveRendererRenderStampAnnots (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDProgressiveRendererRenderStampAnnotsSELPROTO)FRCOREROUTINE(FOFDProgressiveRendererSEL,FOFDProgressiveRendererRenderStampAnnotsSEL, _gPID)))

#define FOFDProgressiveRendererStopRender (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDProgressiveRendererStopRenderSELPROTO)FRCOREROUTINE(FOFDProgressiveRendererSEL,FOFDProgressiveRendererStopRenderSEL, _gPID)))

#define FOFDProgressiveRendererGetPrinterBackgourd (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDProgressiveRendererGetPrinterBackgourdSELPROTO)FRCOREROUTINE(FOFDProgressiveRendererSEL,FOFDProgressiveRendererGetPrinterBackgourdSEL, _gPID)))

//----------_V11----------
//----------_V12----------
//----------_V13----------
#ifdef __cplusplus
}
#endif

#endif