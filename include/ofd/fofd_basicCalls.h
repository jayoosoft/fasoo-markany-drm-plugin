﻿/*****************************************************************************

Copyright (C) 2010 Foxit Corporation
All rights reserved.

NOTICE: Foxit permits you to use, modify, and distribute this file
in accordance with the terms of the Foxit license agreement
accompanying it. If you have received this file from a source other
than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
 
*****************************************************************************/

#ifndef FOFD_BASICCALLS_H
#define FOFD_BASICCALLS_H

#ifndef FS_COMMON_H
#include "../basic/fs_common.h"
#endif

#ifndef FS_BASICEXPT_H
#include "../basic/fs_basicExpT.h"
#endif

#ifndef FS_STRINGEXPT_H
#include "../basic/fs_stringExpT.h"
#endif

#ifndef FOFD_BASICEXPT_H
#include "fofd_basicExpT.h"
#endif

#ifdef __cplusplus
extern "C"{
#endif

#undef INTERFACE
#undef BEGINENUM
#undef NumOfSelector
#undef ENDENUM

// Define macros to generate function selectors in the file fofd_basicImpl.h
#define BEGINENUM enum{
#define NumOfSelector(name) name##InterfacesNum
#define ENDENUM };
#define INTERFACE(returnType,interfaceName,params) interfaceName##SEL,
#include "fofd_basicTempl.h"

#undef INTERFACE
#undef BEGINENUM
#undef NumOfSelector
#undef ENDENUM

// Define macros to generate function PROTO in the file fofd_basicImpl.h
#define BEGINENUM
#define NumOfSelector(name)
#define ENDENUM
#define INTERFACE(returnType,interfaceName, params) \
typedef returnType (*interfaceName##SELPROTO)params;
#include "fofd_basicTempl.h"

//----------_V1----------
//----------_V2----------
//----------_V3----------
//----------_V4----------
//----------_V5----------
//----------_V6----------
//----------_V7----------
//----------_V8----------
//----------_V9----------
//----------_V10----------
//*****************************
/* CryptoDict HFT functions */
//*****************************

#define FOFDCryptoDictNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictNewSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictNewSEL, _gPID)))

#define FOFDCryptoDictDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictDestroySELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictDestroySEL, _gPID)))

#define FOFDCryptoDictGetAdminPwd (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictGetAdminPwdSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictGetAdminPwdSEL, _gPID)))

#define FOFDCryptoDictGetUserPwd (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictGetUserPwdSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictGetUserPwdSEL, _gPID)))

#define FOFDCryptoDictGetSecurityType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictGetSecurityTypeSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictGetSecurityTypeSEL, _gPID)))

#define FOFDCryptoDictGetCryptoType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictGetCryptoTypeSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictGetCryptoTypeSEL, _gPID)))

#define FOFDCryptoDictGetFilter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictGetFilterSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictGetFilterSEL, _gPID)))

#define FOFDCryptoDictGetSubFilter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictGetSubFilterSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictGetSubFilterSEL, _gPID)))

#define FOFDCryptoDictGetCipher (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictGetCipherSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictGetCipherSEL, _gPID)))

#define FOFDCryptoDictCountRecipients (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictCountRecipientsSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictCountRecipientsSEL, _gPID)))

#define FOFDCryptoDictGetRecipient (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictGetRecipientSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictGetRecipientSEL, _gPID)))

#define FOFDCryptoDictGetKeyLength (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictGetKeyLengthSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictGetKeyLengthSEL, _gPID)))

#define FOFDCryptoDictSetAdminPwd (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictSetAdminPwdSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictSetAdminPwdSEL, _gPID)))

#define FOFDCryptoDictSetUserPwd (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictSetUserPwdSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictSetUserPwdSEL, _gPID)))

#define FOFDCryptoDictSetSecurityType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictSetSecurityTypeSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictSetSecurityTypeSEL, _gPID)))

#define FOFDCryptoDictSetCryptoType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictSetCryptoTypeSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictSetCryptoTypeSEL, _gPID)))

#define FOFDCryptoDictSetFilter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictSetFilterSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictSetFilterSEL, _gPID)))

#define FOFDCryptoDictSetSubFilter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictSetSubFilterSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictSetSubFilterSEL, _gPID)))

#define FOFDCryptoDictSetCipher (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictSetCipherSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictSetCipherSEL, _gPID)))

#define FOFDCryptoDictAddRecipient (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictAddRecipientSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictAddRecipientSEL, _gPID)))

#define FOFDCryptoDictSetKeyLength (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoDictSetKeyLengthSELPROTO)FRCOREROUTINE(FOFDCryptoDictSEL,FOFDCryptoDictSetKeyLengthSEL, _gPID)))

//*****************************
/* SecurityHandler HFT functions */
//*****************************

#define FOFDSecurityHandlerRelease (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDSecurityHandlerReleaseSELPROTO)FRCOREROUTINE(FOFDSecurityHandlerSEL,FOFDSecurityHandlerReleaseSEL, _gPID)))

#define FOFDSecurityHandlerIsAdmin (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDSecurityHandlerIsAdminSELPROTO)FRCOREROUTINE(FOFDSecurityHandlerSEL,FOFDSecurityHandlerIsAdminSEL, _gPID)))

//*****************************
/* StdSecurityHandler HFT functions */
//*****************************

#define FOFDStdSecurityHandlerCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDStdSecurityHandlerCreateSELPROTO)FRCOREROUTINE(FOFDStdSecurityHandlerSEL,FOFDStdSecurityHandlerCreateSEL, _gPID)))

//*****************************
/* StdCertSecurityHandler HFT functions */
//*****************************

#define FOFDStdCertSecurityHandlerCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDStdCertSecurityHandlerCreateSELPROTO)FRCOREROUTINE(FOFDStdCertSecurityHandlerSEL,FOFDStdCertSecurityHandlerCreateSEL, _gPID)))

#define FOFDStdCertSecurityHandlerSetPKCS12Info (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDStdCertSecurityHandlerSetPKCS12InfoSELPROTO)FRCOREROUTINE(FOFDStdCertSecurityHandlerSEL,FOFDStdCertSecurityHandlerSetPKCS12InfoSEL, _gPID)))

#define FOFDStdCertSecurityHandlerInitParser (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDStdCertSecurityHandlerInitParserSELPROTO)FRCOREROUTINE(FOFDStdCertSecurityHandlerSEL,FOFDStdCertSecurityHandlerInitParserSEL, _gPID)))

#define FOFDStdCertSecurityHandlerGetPermissons (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDStdCertSecurityHandlerGetPermissonsSELPROTO)FRCOREROUTINE(FOFDStdCertSecurityHandlerSEL,FOFDStdCertSecurityHandlerGetPermissonsSEL, _gPID)))

#define FOFDStdCertSecurityHandlerInitCreator (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDStdCertSecurityHandlerInitCreatorSELPROTO)FRCOREROUTINE(FOFDStdCertSecurityHandlerSEL,FOFDStdCertSecurityHandlerInitCreatorSEL, _gPID)))

//*****************************
/* SMSecurityHandler HFT functions */
//*****************************

#define FOFDSMSecurityHandlerCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDSMSecurityHandlerCreateSELPROTO)FRCOREROUTINE(FOFDSMSecurityHandlerSEL,FOFDSMSecurityHandlerCreateSEL, _gPID)))

//*****************************
/* CryptoHandler HFT functions */
//*****************************

#define FOFDCryptoHandlerRelease (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCryptoHandlerReleaseSELPROTO)FRCOREROUTINE(FOFDCryptoHandlerSEL,FOFDCryptoHandlerReleaseSEL, _gPID)))

//*****************************
/* StdCryptoHandler HFT functions */
//*****************************

#define FOFDStdCryptoHandlerCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDStdCryptoHandlerCreateSELPROTO)FRCOREROUTINE(FOFDStdCryptoHandlerSEL,FOFDStdCryptoHandlerCreateSEL, _gPID)))

//*****************************
/* SM4CryptoHandler HFT functions */
//*****************************

#define FOFDSM4CryptoHandlerCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDSM4CryptoHandlerCreateSELPROTO)FRCOREROUTINE(FOFDSM4CryptoHandlerSEL,FOFDSM4CryptoHandlerCreateSEL, _gPID)))

//*****************************
/* FileStream HFT functions */
//*****************************

#define FOFDFileStreamCreateMemoryStream (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDFileStreamCreateMemoryStreamSELPROTO)FRCOREROUTINE(FOFDFileStreamSEL,FOFDFileStreamCreateMemoryStreamSEL, _gPID)))

#define FOFDFileStreamCreateMemoryStream2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDFileStreamCreateMemoryStream2SELPROTO)FRCOREROUTINE(FOFDFileStreamSEL,FOFDFileStreamCreateMemoryStream2SEL, _gPID)))

#define FOFDFileStreamRelease (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDFileStreamReleaseSELPROTO)FRCOREROUTINE(FOFDFileStreamSEL,FOFDFileStreamReleaseSEL, _gPID)))

#define FOFDFileStreamGetSize (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDFileStreamGetSizeSELPROTO)FRCOREROUTINE(FOFDFileStreamSEL,FOFDFileStreamGetSizeSEL, _gPID)))

#define FOFDFileStreamIsEOF (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDFileStreamIsEOFSELPROTO)FRCOREROUTINE(FOFDFileStreamSEL,FOFDFileStreamIsEOFSEL, _gPID)))

#define FOFDFileStreamGetPosition (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDFileStreamGetPositionSELPROTO)FRCOREROUTINE(FOFDFileStreamSEL,FOFDFileStreamGetPositionSEL, _gPID)))

#define FOFDFileStreamReadBlock (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDFileStreamReadBlockSELPROTO)FRCOREROUTINE(FOFDFileStreamSEL,FOFDFileStreamReadBlockSEL, _gPID)))

#define FOFDFileStreamReadBlock2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDFileStreamReadBlock2SELPROTO)FRCOREROUTINE(FOFDFileStreamSEL,FOFDFileStreamReadBlock2SEL, _gPID)))

#define FOFDFileStreamWriteBlock (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDFileStreamWriteBlockSELPROTO)FRCOREROUTINE(FOFDFileStreamSEL,FOFDFileStreamWriteBlockSEL, _gPID)))

#define FOFDFileStreamWriteBlock2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDFileStreamWriteBlock2SELPROTO)FRCOREROUTINE(FOFDFileStreamSEL,FOFDFileStreamWriteBlock2SEL, _gPID)))

#define FOFDFileStreamFlush (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDFileStreamFlushSELPROTO)FRCOREROUTINE(FOFDFileStreamSEL,FOFDFileStreamFlushSEL, _gPID)))

#define FOFDFileStreamGetCurrentFileName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDFileStreamGetCurrentFileNameSELPROTO)FRCOREROUTINE(FOFDFileStreamSEL,FOFDFileStreamGetCurrentFileNameSEL, _gPID)))

//*****************************
/* PauseHandler HFT functions */
//*****************************

#define FOFDPauseHandlerCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPauseHandlerCreateSELPROTO)FRCOREROUTINE(FOFDPauseHandlerSEL,FOFDPauseHandlerCreateSEL, _gPID)))

#define FOFDPauseHandlerDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPauseHandlerDestroySELPROTO)FRCOREROUTINE(FOFDPauseHandlerSEL,FOFDPauseHandlerDestroySEL, _gPID)))

//*****************************
/* UIMgr HFT functions */
//*****************************

#define FOFDUIMgrSetPanelIndex (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDUIMgrSetPanelIndexSELPROTO)FRCOREROUTINE(FOFDUIMgrSEL,FOFDUIMgrSetPanelIndexSEL, _gPID)))

#define FOFDUIMgrTriggerPanel (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDUIMgrTriggerPanelSELPROTO)FRCOREROUTINE(FOFDUIMgrSEL,FOFDUIMgrTriggerPanelSEL, _gPID)))

#define FOFDUIMgrFormatComboBox (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDUIMgrFormatComboBoxSELPROTO)FRCOREROUTINE(FOFDUIMgrSEL,FOFDUIMgrFormatComboBoxSEL, _gPID)))

#define FOFDUIMgrShowAutoHideStatusBar (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDUIMgrShowAutoHideStatusBarSELPROTO)FRCOREROUTINE(FOFDUIMgrSEL,FOFDUIMgrShowAutoHideStatusBarSEL, _gPID)))

//*****************************
/* DIBAttribute HFT functions */
//*****************************

#define FOFDDIBAttributeCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeCreateSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeCreateSEL, _gPID)))

#define FOFDDIBAttributeGetXDPI (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeGetXDPISELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeGetXDPISEL, _gPID)))

#define FOFDDIBAttributeSetXDPI (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeSetXDPISELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeSetXDPISEL, _gPID)))

#define FOFDDIBAttributeGetYDPI (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeGetYDPISELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeGetYDPISEL, _gPID)))

#define FOFDDIBAttributeSetYDPI (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeSetYDPISELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeSetYDPISEL, _gPID)))

#define FOFDDIBAttributeGetAspectRatio (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeGetAspectRatioSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeGetAspectRatioSEL, _gPID)))

#define FOFDDIBAttributeSetAspectRatio (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeSetAspectRatioSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeSetAspectRatioSEL, _gPID)))

#define FOFDDIBAttributeGetDPIUnit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeGetDPIUnitSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeGetDPIUnitSEL, _gPID)))

#define FOFDDIBAttributeSetDPIUnit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeSetDPIUnitSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeSetDPIUnitSEL, _gPID)))

#define FOFDDIBAttributeGetAuthor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeGetAuthorSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeGetAuthorSEL, _gPID)))

#define FOFDDIBAttributeSetAuthor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeSetAuthorSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeSetAuthorSEL, _gPID)))

#define FOFDDIBAttributeGetTime (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeGetTimeSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeGetTimeSEL, _gPID)))

#define FOFDDIBAttributeSetTime (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeSetTimeSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeSetTimeSEL, _gPID)))

#define FOFDDIBAttributeGetGifLeft (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeGetGifLeftSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeGetGifLeftSEL, _gPID)))

#define FOFDDIBAttributeSetGifLeft (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeSetGifLeftSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeSetGifLeftSEL, _gPID)))

#define FOFDDIBAttributeGetGifTop (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeGetGifTopSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeGetGifTopSEL, _gPID)))

#define FOFDDIBAttributeSetGifTop (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeSetGifTopSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeSetGifTopSEL, _gPID)))

#define FOFDDIBAttributeGetGifLocalPalette (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeGetGifLocalPaletteSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeGetGifLocalPaletteSEL, _gPID)))

#define FOFDDIBAttributeSetGifLocalPalette (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeSetGifLocalPaletteSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeSetGifLocalPaletteSEL, _gPID)))

#define FOFDDIBAttributeGetGifLocalPalNum (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeGetGifLocalPalNumSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeGetGifLocalPalNumSEL, _gPID)))

#define FOFDDIBAttributeSetGifLocalPalNum (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeSetGifLocalPalNumSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeSetGifLocalPalNumSEL, _gPID)))

#define FOFDDIBAttributeGetBmpCompressType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeGetBmpCompressTypeSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeGetBmpCompressTypeSEL, _gPID)))

#define FOFDDIBAttributeSetBmpCompressType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeSetBmpCompressTypeSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeSetBmpCompressTypeSEL, _gPID)))

#define FOFDDIBAttributeGetTiffFrameCompressType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeGetTiffFrameCompressTypeSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeGetTiffFrameCompressTypeSEL, _gPID)))

#define FOFDDIBAttributeSetTiffFrameCompressType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeSetTiffFrameCompressTypeSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeSetTiffFrameCompressTypeSEL, _gPID)))

#define FOFDDIBAttributeGetTiffFrameCompressOptions (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeGetTiffFrameCompressOptionsSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeGetTiffFrameCompressOptionsSEL, _gPID)))

#define FOFDDIBAttributeSetTiffFrameCompressOptions (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeSetTiffFrameCompressOptionsSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeSetTiffFrameCompressOptionsSEL, _gPID)))

#define FOFDDIBAttributeGetTiffFrameFillOrder (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeGetTiffFrameFillOrderSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeGetTiffFrameFillOrderSEL, _gPID)))

#define FOFDDIBAttributeSetTiffFrameFillOrder (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeSetTiffFrameFillOrderSELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeSetTiffFrameFillOrderSEL, _gPID)))

#define FOFDDIBAttributeGetTiffFrameCompressJpegQuality (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeGetTiffFrameCompressJpegQualitySELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeGetTiffFrameCompressJpegQualitySEL, _gPID)))

#define FOFDDIBAttributeSetTiffFrameCompressJpegQuality (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDIBAttributeSetTiffFrameCompressJpegQualitySELPROTO)FRCOREROUTINE(FOFDDIBAttributeSEL,FOFDDIBAttributeSetTiffFrameCompressJpegQualitySEL, _gPID)))

//*****************************
/* CodeC HFT functions */
//*****************************

#define FOFDCodeCPngEncode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCodeCPngEncodeSELPROTO)FRCOREROUTINE(FOFDCodeCSEL,FOFDCodeCPngEncodeSEL, _gPID)))

//*****************************
/* PrintSetting HFT functions */
//*****************************

#define FOFDPrintSettingGetGlobalUnit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPrintSettingGetGlobalUnitSELPROTO)FRCOREROUTINE(FOFDPrintSettingSEL,FOFDPrintSettingGetGlobalUnitSEL, _gPID)))

#define FOFDPrintSettingGetDefaultPrinterName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPrintSettingGetDefaultPrinterNameSELPROTO)FRCOREROUTINE(FOFDPrintSettingSEL,FOFDPrintSettingGetDefaultPrinterNameSEL, _gPID)))

#define FOFDPrintSettingGetDefaultPrinterDevMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPrintSettingGetDefaultPrinterDevModeSELPROTO)FRCOREROUTINE(FOFDPrintSettingSEL,FOFDPrintSettingGetDefaultPrinterDevModeSEL, _gPID)))

#define FOFDPrintSettingSetDefaultPrinterDevMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPrintSettingSetDefaultPrinterDevModeSELPROTO)FRCOREROUTINE(FOFDPrintSettingSEL,FOFDPrintSettingSetDefaultPrinterDevModeSEL, _gPID)))

#define FOFDPrintSettingIsPrintUseGraphics (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPrintSettingIsPrintUseGraphicsSELPROTO)FRCOREROUTINE(FOFDPrintSettingSEL,FOFDPrintSettingIsPrintUseGraphicsSEL, _gPID)))

//*****************************
/* Sys HFT functions */
//*****************************

#define FOFDSysInitSSLModule (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDSysInitSSLModuleSELPROTO)FRCOREROUTINE(FOFDSysSEL,FOFDSysInitSSLModuleSEL, _gPID)))

//----------_V11----------
//----------_V12----------
//----------_V13----------
#ifdef __cplusplus
}
#endif

#endif