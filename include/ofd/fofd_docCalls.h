﻿/*****************************************************************************

Copyright (C) 2010 Foxit Corporation
All rights reserved.

NOTICE: Foxit permits you to use, modify, and distribute this file
in accordance with the terms of the Foxit license agreement
accompanying it. If you have received this file from a source other
than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
 
*****************************************************************************/

#ifndef FOFD_DOCCALLS_H
#define FOFD_DOCCALLS_H

#ifndef FS_COMMON_H
#include "../basic/fs_common.h"
#endif

#ifndef FS_BASICEXPT_H
#include "../basic/fs_basicExpT.h"
#endif

#ifndef FS_STRINGEXPT_H
#include "../basic/fs_stringExpT.h"
#endif

#ifndef FOFD_DOCEXPT_H
#include "fofd_docExpT.h"
#endif

#ifdef __cplusplus
extern "C"{
#endif

#undef INTERFACE
#undef BEGINENUM
#undef NumOfSelector
#undef ENDENUM

// Define macros to generate function selectors in the file fofd_docImpl.h
#define BEGINENUM enum{
#define NumOfSelector(name) name##InterfacesNum
#define ENDENUM };
#define INTERFACE(returnType,interfaceName,params) interfaceName##SEL,
#include "fofd_docTempl.h"

#undef INTERFACE
#undef BEGINENUM
#undef NumOfSelector
#undef ENDENUM

// Define macros to generate function PROTO in the file fofd_docImpl.h
#define BEGINENUM
#define NumOfSelector(name)
#define ENDENUM
#define INTERFACE(returnType,interfaceName, params) \
typedef returnType (*interfaceName##SELPROTO)params;
#include "fofd_docTempl.h"

//----------_V1----------
//----------_V2----------
//----------_V3----------
//----------_V4----------
//----------_V5----------
//----------_V6----------
//----------_V7----------
//----------_V8----------
//----------_V9----------
//----------_V10----------
//*****************************
/* Package HFT functions */
//*****************************

#define FOFDPackageCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPackageCreateSELPROTO)FRCOREROUTINE(FOFDPackageSEL,FOFDPackageCreateSEL, _gPID)))

#define FOFDPackageCreate2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPackageCreate2SELPROTO)FRCOREROUTINE(FOFDPackageSEL,FOFDPackageCreate2SEL, _gPID)))

#define FOFDPackageRelease (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPackageReleaseSELPROTO)FRCOREROUTINE(FOFDPackageSEL,FOFDPackageReleaseSEL, _gPID)))

//*****************************
/* Parser HFT functions */
//*****************************

#define FOFDParserCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDParserCreateSELPROTO)FRCOREROUTINE(FOFDParserSEL,FOFDParserCreateSEL, _gPID)))

#define FOFDParserRelease (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDParserReleaseSELPROTO)FRCOREROUTINE(FOFDParserSEL,FOFDParserReleaseSEL, _gPID)))

#define FOFDParserCountDocuments (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDParserCountDocumentsSELPROTO)FRCOREROUTINE(FOFDParserSEL,FOFDParserCountDocumentsSEL, _gPID)))

#define FOFDParserGetDocument (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDParserGetDocumentSELPROTO)FRCOREROUTINE(FOFDParserSEL,FOFDParserGetDocumentSEL, _gPID)))

#define FOFDParserGetDocument2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDParserGetDocument2SELPROTO)FRCOREROUTINE(FOFDParserSEL,FOFDParserGetDocument2SEL, _gPID)))

#define FOFDParserGetDocumentIndex (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDParserGetDocumentIndexSELPROTO)FRCOREROUTINE(FOFDParserSEL,FOFDParserGetDocumentIndexSEL, _gPID)))

#define FOFDParserGetCryptoDict (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDParserGetCryptoDictSELPROTO)FRCOREROUTINE(FOFDParserSEL,FOFDParserGetCryptoDictSEL, _gPID)))

#define FOFDParserCreate2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDParserCreate2SELPROTO)FRCOREROUTINE(FOFDParserSEL,FOFDParserCreate2SEL, _gPID)))

//*****************************
/* Doc HFT functions */
//*****************************

#define FOFDDocGetParser (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDocGetParserSELPROTO)FRCOREROUTINE(FOFDDocSEL,FOFDDocGetParserSEL, _gPID)))

#define FOFDDocCountPages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDocCountPagesSELPROTO)FRCOREROUTINE(FOFDDocSEL,FOFDDocCountPagesSEL, _gPID)))

#define FOFDDocGetPage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDocGetPageSELPROTO)FRCOREROUTINE(FOFDDocSEL,FOFDDocGetPageSEL, _gPID)))

#define FOFDDocGetPageByID (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDocGetPageByIDSELPROTO)FRCOREROUTINE(FOFDDocSEL,FOFDDocGetPageByIDSEL, _gPID)))

#define FOFDDocGetPageIndex (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDocGetPageIndexSELPROTO)FRCOREROUTINE(FOFDDocSEL,FOFDDocGetPageIndexSEL, _gPID)))

#define FOFDDocGenerateOutline (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDocGenerateOutlineSELPROTO)FRCOREROUTINE(FOFDDocSEL,FOFDDocGenerateOutlineSEL, _gPID)))

#define FOFDDocGetBookmarks (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDocGetBookmarksSELPROTO)FRCOREROUTINE(FOFDDocSEL,FOFDDocGetBookmarksSEL, _gPID)))

#define FOFDDocGetVPreferences (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDocGetVPreferencesSELPROTO)FRCOREROUTINE(FOFDDocSEL,FOFDDocGetVPreferencesSEL, _gPID)))

#define FOFDDocGetPermissions (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDDocGetPermissionsSELPROTO)FRCOREROUTINE(FOFDDocSEL,FOFDDocGetPermissionsSEL, _gPID)))

//*****************************
/* WriteDoc HFT functions */
//*****************************

#define FOFDWriteDocCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDWriteDocCreateSELPROTO)FRCOREROUTINE(FOFDWriteDocSEL,FOFDWriteDocCreateSEL, _gPID)))

#define FOFDWriteDocRelease (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDWriteDocReleaseSELPROTO)FRCOREROUTINE(FOFDWriteDocSEL,FOFDWriteDocReleaseSEL, _gPID)))

//*****************************
/* Creator HFT functions */
//*****************************

#define FOFDCreatorCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCreatorCreateSELPROTO)FRCOREROUTINE(FOFDCreatorSEL,FOFDCreatorCreateSEL, _gPID)))

#define FOFDCreatorRelease (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCreatorReleaseSELPROTO)FRCOREROUTINE(FOFDCreatorSEL,FOFDCreatorReleaseSEL, _gPID)))

#define FOFDCreatorInsertDocument (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCreatorInsertDocumentSELPROTO)FRCOREROUTINE(FOFDCreatorSEL,FOFDCreatorInsertDocumentSEL, _gPID)))

#define FOFDCreatorInitParser (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCreatorInitParserSELPROTO)FRCOREROUTINE(FOFDCreatorSEL,FOFDCreatorInitParserSEL, _gPID)))

#define FOFDCreatorPackage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDCreatorPackageSELPROTO)FRCOREROUTINE(FOFDCreatorSEL,FOFDCreatorPackageSEL, _gPID)))

//*****************************
/* Perms HFT functions */
//*****************************

#define FOFDPermsIsEdit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPermsIsEditSELPROTO)FRCOREROUTINE(FOFDPermsSEL,FOFDPermsIsEditSEL, _gPID)))

#define FOFDPermsIsAnnot (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPermsIsAnnotSELPROTO)FRCOREROUTINE(FOFDPermsSEL,FOFDPermsIsAnnotSEL, _gPID)))

#define FOFDPermsIsExport (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPermsIsExportSELPROTO)FRCOREROUTINE(FOFDPermsSEL,FOFDPermsIsExportSEL, _gPID)))

#define FOFDPermsIsSignature (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPermsIsSignatureSELPROTO)FRCOREROUTINE(FOFDPermsSEL,FOFDPermsIsSignatureSEL, _gPID)))

#define FOFDPermsIsWatermark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPermsIsWatermarkSELPROTO)FRCOREROUTINE(FOFDPermsSEL,FOFDPermsIsWatermarkSEL, _gPID)))

#define FOFDPermsIsPrintScreen (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPermsIsPrintScreenSELPROTO)FRCOREROUTINE(FOFDPermsSEL,FOFDPermsIsPrintScreenSEL, _gPID)))

#define FOFDPermsIsPrintable (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPermsIsPrintableSELPROTO)FRCOREROUTINE(FOFDPermsSEL,FOFDPermsIsPrintableSEL, _gPID)))

#define FOFDPermsGetPrintCopies (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPermsGetPrintCopiesSELPROTO)FRCOREROUTINE(FOFDPermsSEL,FOFDPermsGetPrintCopiesSEL, _gPID)))

#define FOFDPermsGetStartDate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPermsGetStartDateSELPROTO)FRCOREROUTINE(FOFDPermsSEL,FOFDPermsGetStartDateSEL, _gPID)))

#define FOFDPermsGetEndDate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDPermsGetEndDateSELPROTO)FRCOREROUTINE(FOFDPermsSEL,FOFDPermsGetEndDateSEL, _gPID)))

//*****************************
/* VPrefers HFT functions */
//*****************************

#define FOFDVPrefersGetPageMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDVPrefersGetPageModeSELPROTO)FRCOREROUTINE(FOFDVPrefersSEL,FOFDVPrefersGetPageModeSEL, _gPID)))

#define FOFDVPrefersGetPageLayout (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDVPrefersGetPageLayoutSELPROTO)FRCOREROUTINE(FOFDVPrefersSEL,FOFDVPrefersGetPageLayoutSEL, _gPID)))

#define FOFDVPrefersGetTabDisplay (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDVPrefersGetTabDisplaySELPROTO)FRCOREROUTINE(FOFDVPrefersSEL,FOFDVPrefersGetTabDisplaySEL, _gPID)))

#define FOFDVPrefersIsHideToolbar (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDVPrefersIsHideToolbarSELPROTO)FRCOREROUTINE(FOFDVPrefersSEL,FOFDVPrefersIsHideToolbarSEL, _gPID)))

#define FOFDVPrefersIsHideMenubar (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDVPrefersIsHideMenubarSELPROTO)FRCOREROUTINE(FOFDVPrefersSEL,FOFDVPrefersIsHideMenubarSEL, _gPID)))

#define FOFDVPrefersIsHideWindowUI (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDVPrefersIsHideWindowUISELPROTO)FRCOREROUTINE(FOFDVPrefersSEL,FOFDVPrefersIsHideWindowUISEL, _gPID)))

#define FOFDVPrefersGetZoomMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDVPrefersGetZoomModeSELPROTO)FRCOREROUTINE(FOFDVPrefersSEL,FOFDVPrefersGetZoomModeSEL, _gPID)))

#define FOFDVPrefersGetZoom (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDVPrefersGetZoomSELPROTO)FRCOREROUTINE(FOFDVPrefersSEL,FOFDVPrefersGetZoomSEL, _gPID)))

//*****************************
/* Bookmarks HFT functions */
//*****************************

#define FOFDBookmarksCountBookmarks (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDBookmarksCountBookmarksSELPROTO)FRCOREROUTINE(FOFDBookmarksSEL,FOFDBookmarksCountBookmarksSEL, _gPID)))

#define FOFDBookmarksGetBookmark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDBookmarksGetBookmarkSELPROTO)FRCOREROUTINE(FOFDBookmarksSEL,FOFDBookmarksGetBookmarkSEL, _gPID)))

#define FOFDBookmarksGetBookmark2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDBookmarksGetBookmark2SELPROTO)FRCOREROUTINE(FOFDBookmarksSEL,FOFDBookmarksGetBookmark2SEL, _gPID)))

//*****************************
/* Bookmark HFT functions */
//*****************************

#define FOFDBookmarkGetName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDBookmarkGetNameSELPROTO)FRCOREROUTINE(FOFDBookmarkSEL,FOFDBookmarkGetNameSEL, _gPID)))

#define FOFDBookmarkGetDest (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDBookmarkGetDestSELPROTO)FRCOREROUTINE(FOFDBookmarkSEL,FOFDBookmarkGetDestSEL, _gPID)))

//*****************************
/* Outline HFT functions */
//*****************************

#define FOFDOutlineCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDOutlineCreateSELPROTO)FRCOREROUTINE(FOFDOutlineSEL,FOFDOutlineCreateSEL, _gPID)))

#define FOFDOutlineRelease (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDOutlineReleaseSELPROTO)FRCOREROUTINE(FOFDOutlineSEL,FOFDOutlineReleaseSEL, _gPID)))

#define FOFDOutlineIsNull (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDOutlineIsNullSELPROTO)FRCOREROUTINE(FOFDOutlineSEL,FOFDOutlineIsNullSEL, _gPID)))

#define FOFDOutlineIsIdentical (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDOutlineIsIdenticalSELPROTO)FRCOREROUTINE(FOFDOutlineSEL,FOFDOutlineIsIdenticalSEL, _gPID)))

#define FOFDOutlineGenerateParent (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDOutlineGenerateParentSELPROTO)FRCOREROUTINE(FOFDOutlineSEL,FOFDOutlineGenerateParentSEL, _gPID)))

#define FOFDOutlineClone (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDOutlineCloneSELPROTO)FRCOREROUTINE(FOFDOutlineSEL,FOFDOutlineCloneSEL, _gPID)))

#define FOFDOutlineCountSubOutlines (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDOutlineCountSubOutlinesSELPROTO)FRCOREROUTINE(FOFDOutlineSEL,FOFDOutlineCountSubOutlinesSEL, _gPID)))

#define FOFDOutlineGenerateSubOutline (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDOutlineGenerateSubOutlineSELPROTO)FRCOREROUTINE(FOFDOutlineSEL,FOFDOutlineGenerateSubOutlineSEL, _gPID)))

#define FOFDOutlineIsExpanded (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDOutlineIsExpandedSELPROTO)FRCOREROUTINE(FOFDOutlineSEL,FOFDOutlineIsExpandedSEL, _gPID)))

#define FOFDOutlineGetTitle (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDOutlineGetTitleSELPROTO)FRCOREROUTINE(FOFDOutlineSEL,FOFDOutlineGetTitleSEL, _gPID)))

#define FOFDOutlineGetActions (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDOutlineGetActionsSELPROTO)FRCOREROUTINE(FOFDOutlineSEL,FOFDOutlineGetActionsSEL, _gPID)))

//*****************************
/* Actions HFT functions */
//*****************************

#define FOFDActionsCountActions (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionsCountActionsSELPROTO)FRCOREROUTINE(FOFDActionsSEL,FOFDActionsCountActionsSEL, _gPID)))

#define FOFDActionsGetAction (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionsGetActionSELPROTO)FRCOREROUTINE(FOFDActionsSEL,FOFDActionsGetActionSEL, _gPID)))

//*****************************
/* Action HFT functions */
//*****************************

#define FOFDActionGetEvent (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionGetEventSELPROTO)FRCOREROUTINE(FOFDActionSEL,FOFDActionGetEventSEL, _gPID)))

#define FOFDActionGenerateActionRegion (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionGenerateActionRegionSELPROTO)FRCOREROUTINE(FOFDActionSEL,FOFDActionGenerateActionRegionSEL, _gPID)))

#define FOFDActionGetActionType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionGetActionTypeSELPROTO)FRCOREROUTINE(FOFDActionSEL,FOFDActionGetActionTypeSEL, _gPID)))

//*****************************
/* ActionGoto HFT functions */
//*****************************

#define FOFDActionGotoGetDest (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionGotoGetDestSELPROTO)FRCOREROUTINE(FOFDActionGotoSEL,FOFDActionGotoGetDestSEL, _gPID)))

#define FOFDActionGotoGetBookmark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionGotoGetBookmarkSELPROTO)FRCOREROUTINE(FOFDActionGotoSEL,FOFDActionGotoGetBookmarkSEL, _gPID)))

//*****************************
/* ActionGotoA HFT functions */
//*****************************

#define FOFDActionGotoAGetDest (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionGotoAGetDestSELPROTO)FRCOREROUTINE(FOFDActionGotoASEL,FOFDActionGotoAGetDestSEL, _gPID)))

#define FOFDActionGotoAGetBookmark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionGotoAGetBookmarkSELPROTO)FRCOREROUTINE(FOFDActionGotoASEL,FOFDActionGotoAGetBookmarkSEL, _gPID)))

#define FOFDActionGotoAGetAttachID (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionGotoAGetAttachIDSELPROTO)FRCOREROUTINE(FOFDActionGotoASEL,FOFDActionGotoAGetAttachIDSEL, _gPID)))

#define FOFDActionGotoAIsNewWindow (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionGotoAIsNewWindowSELPROTO)FRCOREROUTINE(FOFDActionGotoASEL,FOFDActionGotoAIsNewWindowSEL, _gPID)))

//*****************************
/* ActionRegion HFT functions */
//*****************************

#define FOFDActionRegionNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionRegionNewSELPROTO)FRCOREROUTINE(FOFDActionRegionSEL,FOFDActionRegionNewSEL, _gPID)))

#define FOFDActionRegionRelease (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionRegionReleaseSELPROTO)FRCOREROUTINE(FOFDActionRegionSEL,FOFDActionRegionReleaseSEL, _gPID)))

#define FOFDActionRegionIsEmpty (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionRegionIsEmptySELPROTO)FRCOREROUTINE(FOFDActionRegionSEL,FOFDActionRegionIsEmptySEL, _gPID)))

#define FOFDActionRegionCountAreas (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionRegionCountAreasSELPROTO)FRCOREROUTINE(FOFDActionRegionSEL,FOFDActionRegionCountAreasSEL, _gPID)))

#define FOFDActionRegionGenerateArea (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionRegionGenerateAreaSELPROTO)FRCOREROUTINE(FOFDActionRegionSEL,FOFDActionRegionGenerateAreaSEL, _gPID)))

#define FOFDActionRegionGeneratePath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FOFDActionRegionGeneratePathSELPROTO)FRCOREROUTINE(FOFDActionRegionSEL,FOFDActionRegionGeneratePathSEL, _gPID)))

//----------_V11----------
//----------_V12----------
//----------_V13----------
#ifdef __cplusplus
}
#endif

#endif