﻿/*****************************************************************************

Copyright (C) 2010 Foxit Corporation
All rights reserved.

NOTICE: Foxit permits you to use, modify, and distribute this file
in accordance with the terms of the Foxit license agreement
accompanying it. If you have received this file from a source other
than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
 
*****************************************************************************/

//----------_V1----------
//----------_V2----------
//----------_V3----------
//----------_V4----------
//----------_V5----------
//----------_V6----------
//----------_V7----------
//----------_V8----------
//----------_V9----------
//----------_V10----------
//*****************************
/* Sign HFT functions */
//*****************************

BEGINENUM
INTERFACE(FS_DIBitmap, FOFDSignCreateSealBitmap, (FS_ByteString bsSealData))

NumOfSelector(FOFDSign)
ENDENUM

//----------_V11----------
//----------_V12----------
//----------_V13----------
