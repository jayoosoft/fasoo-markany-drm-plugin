﻿/** @file fr_docImpl.h.
 * 
 *  @brief defined all interface associate with Foxit Reader document .
 */

#ifndef FR_DOCIMPL_H
#define FR_DOCIMPL_H

#ifndef FS_INTERNALINC_H
#include "../../include/basic/fs_internalInc.h"
#endif

#ifndef FPD_PARSEREXPT_H
#include "../../pdf/fpd_parserExpT.h"
#endif

#ifndef FPD_DOCEXPT_H
#include "../../pdf/fpd_docExpT.h"
#endif

#ifndef FPD_OBJSEXPT_H
#include "../../pdf/fpd_objsExpT.h"
#endif

#ifndef FPD_RENDEREXPT_H
#include "../../pdf/fpd_renderExpT.h"
#endif


#ifndef FR_APPEXPT_H
#include "../fr_appExpT.h"
#endif

#ifndef FPD_SERIALEXPT_H
#include "../../pdf/fpd_serialExpT.h"
#endif


#ifdef __cplusplus
extern "C"{
#endif
	
class CFR_Doc_V1
{
public:
	//************************************
	// Function:  OpenFromFile
	// Param[in]: wszFile			The file path.
	// Param[in]: szPassword		The password with the file. It could be <a>NULL</a> if no password to pass.
	// Param[in]: bMakeVisible		Whether the document will be made visible.
	// Param[in]: bAddToMRU			Whether add the file path to the recent file list.
	// Return:	The document that was opened. It returns <a>NULL</a> if Reder failed to open document.
	// Remarks: Opens and displays a document form a file.	  	
	// Notes: Do not open and then immediately close a FR_Document without letting least once event loop complete.
	// See: FRDocClose
	// See: FRDocOpenFromPDDoc
	// See: FRDocFromPDDoc
	//************************************
	static FR_Document	OpenFromFile(FS_LPCWSTR wszFile, FS_LPCSTR szPassword, FS_BOOL bMakeVisible, FS_BOOL bAddToMRU);

	//************************************
	// Function:  OpenFromPDDoc
	// Param[in]: pddoc				The <a>FR_Document</a> object to open.
	// Param[in]: lpwsTitle			It is used as window's title.
	// Return: The document that was opened. It returns <a>NULL</a> if Reder failed to open document.
	// Remarks:	Opens and returns a <a>FR_Document</a> view of <a>PD_Document</a>.  	
	// Notes: Do not open and then immediately close a FR_Document without letting least once event loop complete.
	// <a>FRDocClose</a>() should be used in place of <a>FPDDocClose</a>() after FRDocOpenFromPDDoc() is called.
	// <a>FRDocClose</a>() will decrement the FPDDoc appropriately and free document-related resources.
	// See: FRDocClose
	// See: FRDocOpenFromFile
	// See: FRDocFromPDDoc
	//************************************
	static FR_Document	OpenFromPDDoc(FPD_Document pddoc, FS_LPCWSTR lpwsTitle);

	//************************************
	// Function:  FromPDDoc
	// Param[in]: pddoc				The <a>FR_Docuemnt</a> whose <a>FR_Document</a> is to be returned.
	// Return: The <a>FR_Docuemnt</a> if a  <a>FR_Docuemnt</a> is already opened for this <a>FPD_Document</a>, otherwise <a>NULL</a>.	
	// Remarks: Gets the  <a>FR_Docuemnt</a> associated with a <a>FPD_Document</a>.	  	
	// Notes:
	// See: FRDocOpenFromFile
	// See: FRDocOpenFromPDDoc
	// See: FRDocGetPDDoc
	//************************************
	static FR_Document	FromPDDoc(FPD_Document pddoc);

	//************************************
	// Function:  Close
	// Param[in]: doc				The document to close.
	// Param[in]: bPromptToSave		If <a>FALSE</a>, the document closed without prompting the user and without saving,
	// even if the document has been modified. If <a>TRUE</a>, it prompt the user to save the document if it has
	// been modified.
	// Param[in]: bDelay			Delay closing the document or not.
	// Param[in]: bShowCancel		If <a>FALSE</a>, it will not show cancle button when prompt to save.
	// Return:	<a>TRUE</a> if the document colsed, <a>FALSE</a> if it did not. The document will always close if 
	// <param>bPromptToSave</param> is <a>FALSE</a>.
	// Remarks: Closes the document window.  	
	// Notes:
	// See: FRDocOpenFromFile
	// See: FRDocOpenFromPDDoc
	//************************************
	static FS_BOOL		Close(FR_Document doc, FS_BOOL bPromptToSave, FS_BOOL bDelay, FS_BOOL bShowCancel);


	//************************************
	// Function:  GetPDDoc
	// Param[in]: doc			The document whose <a>FPD_Document</a> is obtained.
	// Return: The <a>FPD_Document</a> associated with <a>FR_Document</a>.
	// Remarks:	Gets the <a>FPD_Document</a> to associated with the specified <a>FR_Document</a>. 	
	// Notes:
	// See: FRDocFromPDDoc
	// See: FRDocOpenFromPDDoc
	// See: FRPageViewGetPDPage
	//************************************
	static FPD_Document	GetPDDoc(FR_Document doc);


	

	
	//************************************
	// Function:  SetCustomSecurity
	// Param[in]: doc				The input document.
	// Param[in]: encryptDict		The Encrypt dictionary.
	// Param[in]: cryptoHandler		The callbacks for crypto handler.
	// Param[in]: bEncryptMetadata	Whether to encrypt the metadata.
	// Param[in]: clientHandler		The user-supplied data.
	// Return:	void
	// Remarks:	 Sets security using custom security handler and custom encryption.
	// Application should provide a full encryption dictionary (application can destroy it after this call),
	// and a custom encryption handler. 	
	// Notes:
	//************************************
	static void				SetCustomSecurity(
								FR_Document doc, 
								FPD_Object encryptDict, 
								FR_CryptoCallbacks cryptoHandler, 
								FS_BOOL bEncryptMetadata,
								FS_LPVOID clientHandler);

	//************************************
	// Function:  DoSave
	// Param[in]: doc				The document to be saved.
	// Param[in]: proc				Callback function.
	// Param[in]: pProcData			The client data. It will be passed to the save callback function.
	// Param[in]: bShowProgressBar	Whether to show the progress bar or not.
	// Return: void
	// Remarks:	Saves a file, handling any user interface(for example, a Save File dialog box) if need.  	
	// Notes:
	// See: FRDocDoSaveAs
	// See: FRDocSetChangeMark
	//************************************
	static void			DoSave(FR_Document doc, FR_DocSaveProc proc, void* pProcData, FS_BOOL bShowProgressBar);

	//************************************
	// Function:  DoSaveAs
	// Param[in]: doc			The document.
	// Return:	
	// Remarks: Displays a file dialog box which can be used to save the document as a new name.	  	
	// Notes:
	// See: FRDocDoSave
	//************************************
	static void			DoSaveAs(FR_Document doc);
 
	//************************************
	// Function:  SetChangeMark
	// Param[in]: doc			The document.
	// Return:	void
	// Remarks: Sets the modify flag. Reader has a built-in flag that indicate whether a document has been modified,
	// if the value of the flag is valid, the Save button on File toolbar is enable, otherwise the Save button is 
	// disable.
	// Notes:
	// See: FRDocGetChangeMark
	// See: FRDocClearChangeMark
	//************************************
	static void				SetChangeMark(FR_Document doc);

	//************************************
	// Function:  GetChangeMark
	// Param[in]: doc		The document.
	// Return: <a>TRUE</a> if the document has been modified, <a>FALSE</a> if no change.
	// Remarks: Checks whether the document is modified.
	// Notes:
	// See: FRDocSetChangeMark
	// See: FRDocClearChangeMark
	//************************************
	static FS_BOOL			GetChangeMark(FR_Document doc);

	//************************************
	// Function:  ClearChangeMark
	// Param[in]: doc			The document.
	// Return:	void
	// Remarks:	Invalidates all modification.  	
	// Notes:
	// See: FRDocGetChangeMark
	// See: FRDocSetChangeMark
	//************************************
	static void				ClearChangeMark(FR_Document doc);

	//************************************
	// Function:  WillInsertPages
	// Param[in]: doc			The document to be inserted pages into.
	// Param[in]: iInserAt		The page index for first inserted page.
	// Param[in]: nCount		The page count for all inserted page.
	// Return:	void
	// Remarks:	The document will be inserted some pages. This notification will be broadcast to
   	// all plug-ins.
	// Notes: You can call this method before you start to insert some page into a FPD_Document object.<br>
	// The callback WillInsertPages() descriped in struct FR_DocEventCallbacksRec
	// will be called by Reader.
	//************************************
	static void				WillInsertPages(FR_Document doc, FS_INT32 iInserAt, FS_INT32 nCount);

	//************************************
	// Function:  DidInsertPages
	// Param[in]: doc			The document to be inserted pages into.
	// Param[in]: iInserAt		The page index for first inserted page.
	// Param[in]: nCount		The page count for all inserted page.
	// Return:	void
	// Remarks:	The document was inserted some pages. This notification will be broadcast to
   	// all plug-ins.
	// Notes: You can call this method after you inserted some page into a FPD_Document object.<br>
	// The callback DidInsertPages() descriped in struct FR_DocEventCallbacksRec
	// will be called by Reader.
	//************************************
	static void				DidInsertPages(FR_Document doc, FS_INT32 iInserAt, FS_INT32 nCount);

	//************************************
	// Function:  WillDeletePages
	// Param[in]: doc				The document whose page was deleted.
	// Param[in]: arrDelPage		The index of the pages that has been deleted.	
	// Return:	void
	// Remarks:	The document will delete any pages. This notification will be broadcast to
   	// all plug-ins.
	// Notes: You can call this method before you delete some page from a FPD_Document object.<br>
	// The callback WillDeletePages() descriped in struct FR_DocEventCallbacksRec
	// will be called by Reader.  	
	// Notes:
	//************************************
	static void				WillDeletePages(FR_Document doc, FS_WordArray arrDelPage);

	//************************************
	// Function:  DidDeletePages
	// Param[in]: doc				The document whose page was deleted.
	// Param[in]: arrDelPage		The index of the pages that has been deleted.
	// Return:	void
	// Remarks:	The document was deleted any pages. This notification will be broadcast to
   	// all plug-ins.
	// Notes: You can call this method after you deleted some page from a FPD_Document object.<br>
	// The callback DidDeletePages() descriped in struct FR_DocEventCallbacksRec
	// will be called by Reader.  	
	// Notes:
	//************************************
	static void				DidDeletePages(FR_Document doc, FS_WordArray arrDelPage);

	//************************************
	// Function:  WillRotatePage
	// Param[in]: doc				The document whose page's rotation attribute was modified.
	// Param[in]: iPage				The page index. 
	// Return:	void
	// Remarks:	The pages of <a>FR_Document</a> whose rotation attribute will be modified.  	
	// Notes: You can call this method before you want to change some page's rotation attribute.<br>
	// The callback WillModifyPageAttribute() descripted in struct FR_DocEventCallbacksRec
	// will be called by Reader. 
	//************************************
	static void			WillRotatePage(FR_Document doc, int iPage);


	//************************************
	// Function:  DidRotatePage
	// Param[in]: doc				The document whose page's rotation attribute was modified.
	// Param[in]: iPage				The page index. 
	// Return:	void
	// Remarks:	The page of <a>FR_Document</a> whose rotation attribute was modified.  	
	// Notes: You can call this method after you changed some page's rotation attribute.<br>
	// The callback DidModifyPageAttribute() descripted in struct FR_DocEventCallbacksRec
	// will be called by Reader. 
	//************************************
	static void				DidRotatePage(FR_Document doc, FS_INT32 iPage);

	//************************************
	// Function:  WillResizePage
	// Param[in]: doc				The document whose page's attribute was modified.
	// Param[in]: iPage				The page index.			
	// Return:	void
	// Remarks:	 The pages of <a>FR_Document</a> whose attribute will be modified.   	
	// Notes: You can call this method before you want to change some page's rotation attribute.<br>
	// The callback WillModifyPageAttribute() descriped in struct FR_DocEventCallbacksRec
	// will be called by Reader. 
	//************************************
	static void				WillResizePage(FR_Document doc, int iPage);

	//************************************
	// Function:  DidResizePage
	// Param[in]: doc				The document whose page's attribute was modified.
	// Param[in]: iPage				The page index.			
	// Return:	void
	// Remarks:	 The page of <a>FR_Document</a> whose attribute was modified.   	
	// Notes: You can call this method after you changed some page's rotation attribute.<br>
	// The callback DidModifyPageAttribute() descriped in struct FR_DocEventCallbacksRec
	// will be called by Reader. 
	//************************************
	static void				DidResizePage(FR_Document doc, FS_INT32 iPage);	 

	//************************************
	// Function:  DoPrint
	// Param[in]: doc			The document to print.
	// Return:	void  
	// Remarks: Performs the print operation, including user dialog box.  
	// Notes:	
	// See: FRDocPrintPages
	// See: FRDocDoPrintSilently
	// See: FRDocPrintSetup
	//************************************
	static void			DoPrint(FR_Document doc);

	//************************************
	// Function:  PrintPages
	// Param[in]: doc			The input document.
	// Param[in]: firstPage		The index of first page to be printed.
	// Param[in]: lastPage		The index of last page to be printed.
	// Return:	void  
	// Remarks:  Sets the first page and the last page to be printed.
	// Notes:	
	// See: FRDocDoPrint
	// See: FRDocDoPrintSilently
	//************************************
	static void			PrintPages(FR_Document doc, FS_INT32 firstPage, FS_INT32 lastPage);

	//************************************
	// Function:  PrintSetup
	// Param[in]: doc	The input document.
	// Return: void
	// Remarks: Sets up the print.
	// Notes:	
	// See: FRDocDoPrint
	// See: FRDocDoPrintSilently
	// See: FRDocPrintPages
	//************************************
	static void			PrintSetup(FR_Document doc);


	//************************************
	// Function:  CountDocViews
	// Param[in]: doc			The document whose view count is obtained.
	// Return:  The number of <a>FR_DocView</a> for specified document.  
	// Remarks: Gets the number of <a>FR_DocView</a> for specified document.  
	// Notes:
	// See: FRDocGetDocView
	//************************************
	static FS_INT32		CountDocViews(FR_Document doc);

	//************************************
	// Function:  GetDocView
	// Param[in]: doc			The document whose document view is obtained.
	// Param[in]: iView			The index of a document view. The index range is 0 to (<a>FRDocCountDocViews</a>()-1).			
	// Return:  The specified <a>FR_DocView</a>.  
	// Remarks: Gets the specified <a>FR_DocView</a> for specified document.  
	// Notes:	
	// See: FRDocGetCurrentDocView
	// See: FRDocCountDocViews
	//************************************
	static FR_DocView	GetDocView(FR_Document doc, FS_INT32 iView);

	//************************************
	// Function:  GetCurrentDocView
	// Param[in]: doc			The document whose document view is obtained.			
	// Return:  The current showing <a>FR_DocView</a>.  
	// Remarks: Gets the current showing <a>FR_DocView</a> for specified document.  
	// Notes:	
	// See: FRDocGetDocView
	//************************************
	static FR_DocView	GetCurrentDocView(FR_Document doc);

	//************************************
	// Function:  GetPermissions
	// Param[in]: doc			The document whose user permission is obtained.
	// Return:  The document permissions.  
	// Remarks: Gets permissions of a document.  
	// Notes:	
	// See: FRDocSetPermissions
	//************************************
	static FS_DWORD	GetPermissions(FR_Document doc);


	//************************************
	// Function:  SetPermissions
	// Param[in]: doc			The document whose user permission is set.
	// Param[in]: dwPermission	The new permission to set to the document.
	// Return:  void  
	// Remarks: Sets permissions to a document.  
	// Notes:	
	// See: FRDocGetPermissions
	//************************************
	static void	SetPermissions(FR_Document doc, FS_DWORD dwPermission);


	//************************************
	// Function:  GetFilePath
	// Param[in]: doc			The document whose file path is set.
	// Param[in/out]: path		A wide string object to receive the file path.
	// Return:  void  
	// Remarks: Gets the file path of a document opened by Foxit Reader.  
	// Notes:	
	//************************************
	static void GetFilePath(FR_Document doc, FS_WideString* path);

	//************************************
	// Function:  SetCurSelection
	// Param[in]: doc			The input document.
	// Param[in]: type			The input type of the selection handler.
	// Param[in]: pSelectData	The selection data.
	// Return:    <a>TRUE</a> means successful, otherwise not.
	// Remarks: Sets the current selection handler by type.
	// Notes:	
	//************************************
	static FS_BOOL SetCurSelection(FR_Document doc, FS_LPSTR type, void* pSelectData);

	//************************************
	// Function:  AddToCurrentSelection
	// Param[in]: doc		The input document.
	// Param[in]: pCurData	The current selection data.
	// Param[in]: pAddData  The data to be added to the current selection handler.
	// Return:    The updated selection data. It is <a>NULL</a> if failed.
	// Remarks:   Adds the new data to the current selection data and returns the updated one.
	// Notes:	
	//************************************
	static void* AddToCurrentSelection(FR_Document doc, void* pCurData, void* pAddData);

	//************************************
	// Function:  RemoveFromSelection
	// Param[in]: doc		The input document.
	// Param[in]: pCurData	The current selection data.
	// Param[in]: pRemData	The data to be removed from the current selection data.
	// Return:    The updated selection data. It is <a>NULL</a> if failed.
	// Remarks:	  Removes some data from the current selection data and returns the updated one.
	// Notes:	
	//************************************
	static void* RemoveFromSelection(FR_Document doc, void* pCurData, void* pRemData);

	//************************************
	// Function:  GetCurSelection
	// Param[in]: doc	The input document.
	// Return:    The current selection handler.
	// Remarks:	  Gets the current selection handler.
	// Notes:	
	//************************************
	static void* GetCurSelection(FR_Document doc);

	//************************************
	// Function:  GetCurSelectionType
	// Param[in]: doc	The input document.
	// Return:    The type of the current selection handler.
	// Remarks:   Gets the type of the current selection handler.
	// Notes:	
	//************************************
	static FS_LPCSTR GetCurSelectionType(FR_Document doc);

	//************************************
	// Function:  ShowSelection
	// Param[in]: doc	The input document.
	// Return:  void  
	// Remarks:	Displays the current selection by calling the selection handler's <a>FRSelectionShowSelection</a>() 
	// callback. It does nothing if the document has no selection, or the current selection's handler has no 
	// <a>FRSelectionShowSelection</a>()  callback.
	// It only raises those exceptions raised by the selection handler's <a>FRSelectionShowSelection</a>() callback.
	// Notes:	
	//************************************
	static void ShowSelection(FR_Document doc);

	//************************************
	// Function:  ClearSelection
	// Param[in]: doc	The input document.
	// Return:  void  
	// Remarks: Clears and destroys the current selection by calling the appropriate selection server's 
    // <a>FRSelectionLosingSelection</a>().
	// Notes:	
	//************************************
	static void ClearSelection(FR_Document doc);

	//************************************
	// Function:  DeleteSelection
	// Param[in]: doc	The input document.
	// Return:  void  
	// Remarks: Deletes the specified document's current selection, if possible. The selection is deleted if changing the 
	// selection is currently permitted, the selection handler has an <a>FRSelectionDoDelete</a>() callback, and the 
	// selection server's <a>FRSelectionCanDelete</a>() callback returns <a>TRUE</a>. If the selection handler does not 
	// have a <a>FRSelectionCanDelete</a>() callback, a default value of <a>TRUE</a> is used.
	// The selection is deleted by calling the selection handler's <a>FRSelectionDoDelete</a>() callback. 
	// It only raises those exceptions raised by the selection handler's <a>FRSelectionDoDelete</a>() and 
    // <a>FRSelectionCanDelete</a>() callbacks.
	// Notes:	
	//************************************
	static void DeleteSelection(FR_Document doc);

	//************************************
	// Function:  CopySelection
	// Param[in]: doc	The input document.
	// Return:  void  
	// Remarks: Copies the current selection to the clipboard, if possible. The selection is copied if the selection handler has 
	// a <a>FRSelectionDoCopy</a>() callback, and the selection handler's <a>FRSelectionCanCopy</a>() callback 
	// returns <a>TRUE</a>. If the selection server does not have a <a>FRSelectionCanCopy</a>() method, a default 
	// value of <a>TRUE</a> is used.
	// The selection is copied by calling the selection handler's <a>FRSelectionDoCopy</a>() callback. 
	// It only raises those exceptions raised by the selection handler's <a>FRSelectionDoCopy</a>() and 
    // <a>FRSelectionCanCopy</a>() callbacks.
	// Notes:	
	//************************************
	static void CopySelection(FR_Document doc);
	
	//************************************
	// Function:  SetCurCapture
	// Param[in]: doc		The input document.
	// Param[in]: type		The type of the capture handler.
	// Param[in]: pCapData	The capture data.
	// Return:    <a>TRUE</a> means successful, otherwise not.
	// Remarks: Set the current capture handler by type.
	// Notes:	
	//************************************
	static FS_BOOL SetCurCapture(FR_Document doc, FS_LPSTR type, void* pCapData);

	//************************************
	// Function:  GetCurCapture
	// Param[in]: doc	The input document.
	// Return:    The current capture handler.
	// Remarks:   Gets the current capture handler.
	// Notes:	
	//************************************
	static void* GetCurCapture(FR_Document doc);

	//************************************
	// Function:  GetCurCaptureType
	// Param[in]: doc	The input document.
	// Return:    The type of the current capture handler.
	// Remarks:   Gets the type of the current capture handler.
	// Notes:	
	//************************************
	static FS_LPCSTR GetCurCaptureType(FR_Document doc);

	//************************************
	// Function:  ReleaseCurCapture
	// Param[in]: doc	The input document.
	// Return:  void  
	// Remarks:	Releases the current capture.
	// Notes:	
	//************************************
	static void ReleaseCurCapture(FR_Document doc);	

	//************************************
	// Function:  SetMenuEnableByName
	// Param[in]: doc		The input document.
	// Param[in]: csName	The menu you want to control.
	// Param[in]: bEnable	Whether the menu is enable or not.
	// Return:  void  
	// Remarks:	Set the menu enable or not.
	// Notes:	
	//************************************
	static void SetMenuEnableByName(FR_Document doc, FS_LPCSTR csName, FS_BOOL bEnable);	

	//************************************
	// Function:  GetParser
	// Param[in]: doc		The input document.
	// Return:  A PDF file parser associated with current document.  
	// Remarks:	Gets the PDF file parser which is parsing current document.
	// Notes:	
	//************************************
	static FPD_Parser GetParser(FR_Document doc);

	//************************************
	// Function:  GetPDFCreator
	// Param[in]: doc		The input document.
	// Return:  A PDF creator associated with current document.  
	// Remarks:	Gets the PDF creator associated with current document.
	// Notes:	
	//************************************
	static FPD_Creator GetPDFCreator(FR_Document doc);


	//************************************
	// Function:  DoPrintSilently
	// Param[in]: doc			The document to print.
	// Return:	void  
	// Remarks: Performs the print operation, not including user dialog box.  
	// Notes:	
	// See: FRDocPrintPages
	// See: FRDocPrintSetup
	//************************************
	static void			DoPrintSilently(FR_Document doc);

	//************************************
	// Function:  GetTextSelectTool
	// Param[in]: doc			The document object.
	// Return:	The <a>FR_TextSelectTool</a> object of the <param>doc</param>.
	// Remarks: Gets the current text select tool for specified document.  
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FR_TextSelectTool GetTextSelectTool(FR_Document doc);

	//************************************
	// Function:  GetDocumentType
	// Param[in]: doc The document to get the type.
	// Return:	The type of the document.
	// Remarks: Get the type of the document.  
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	// See: FRDocTypes
	//************************************
	static FS_INT32 GetDocumentType(FR_Document doc);

	//************************************
	// Function:  CanSecurityMethodBeModified
	// Param[in]: doc			The document.
	// Return: <a>TRUE</a> for success, otherwise failure.
	// Remarks: Whether the security applied to the document can be modified or not.	  	
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	// See: FRAppRegisterSecurityMethod
	//************************************
	static FS_BOOL CanSecurityMethodBeModified(FR_Document doc);

	//************************************
	// Function:  UpdateSecurityMethod
	// Param[in]: doc			The document.
	// Return: <a>TRUE</a> for success, otherwise failure.
	// Remarks: When this interface is invoked, the <a>FRSecurityMethodRemoveSecurityInfo</a> will be invoked if the document is encrypted.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	// See: FRAppRegisterSecurityMethod
	//************************************
	static FS_BOOL UpdateSecurityMethod(FR_Document doc);

	//************************************
	// Function:  IsEncrypted
	// Param[in]: doc			The document.
	// Return: <a>TRUE</a> for the document being encrypted, otherwise not.
	// Remarks: When this interface is invoked, the <a>FRSecurityMethodIsMyMethod</a> will be invoked.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	// See: FRAppRegisterSecurityMethod
	//************************************
	static FS_BOOL IsEncrypted(FR_Document doc);

	//************************************
	// Function:  RemoveSecurityMethod
	// Param[in]: doc			The document.
	// Return: <a>TRUE</a> for the security method of the document being removed, otherwise not.
	// Remarks: When this interface is invoked, the <a>FRSecurityMethodRemoveSecurityInfo</a> will be invoked.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	// See: FRAppRegisterSecurityMethod
	//************************************
	static FS_BOOL RemoveSecurityMethod(FR_Document doc);

	//************************************
	// Function:  EnableRunScript
	// Param[in]: doc			The document.
	// Param[in]: bIsEnable		Whether the document can run script.
	// Return: void.
	// Remarks: Whether the document can run script.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void EnableRunScript(FR_Document doc, FS_BOOL bIsEnable);

	//************************************
	// Function:  IsEnableRunScript
	// Param[in]: doc			The document.
	// Return: Whether the document can run script.
	// Remarks: Whether the document can run script.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_BOOL IsEnableRunScript(FR_Document doc);

	//************************************
	// Function:  ChangeDocShowTitle
	// Param[in]: doc				The document.
	// Param[in]: lpwsShowTitle		This value will be shown as the main frame title and the document tab title.
	// Return: void.
	// Remarks: Sets the main frame title and the document tab title.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void ChangeDocShowTitle(FR_Document doc, FS_LPCWSTR lpwsShowTitle);

	//************************************
	// Function:  IsMemoryDoc
	// Param[in]: doc				The document.
	// Return: Whether the document is a memory document or not.
	// Remarks: Whether the document is a memory document or not.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_BOOL IsMemoryDoc(FR_Document doc);

	//************************************
	// Function:  GetCurrentSecurityMethodName
	// Param[in]: doc				The document.
	// Param[out]: outName			It receives the name of current security method.
	// Return: The name of current security method.
	// Remarks: Gets the name of current security method.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void GetCurrentSecurityMethodName(FR_Document doc, FS_WideString* outName);

	//************************************
	// Function:  GetCurrentWndProvider
	// Param[in]: doc				The document.
	// Return: The pointer to the current window provider.
	// Remarks: Gets the current window provider.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	// See: FRAppRegisterWndProvider
	//************************************
	static void* GetCurrentWndProvider(FR_Document doc);

	//************************************
	// Function:  SetCurrentWndProvider
	// Param[in]: doc			The document.
	// Param[in]: pProvider		The input window provider.
	// Return: void
	// Remarks: Sets the window provider.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	// See: FRAppRegisterWndProvider
	//************************************
	static void SetCurrentWndProvider(FR_Document doc, void* pProvider);

	//************************************
	// Function:  GetWndProviderByName
	// Param[in]: doc				The document.
	// Param[in]: lpsName			The Specified name of the window provider.
	// Return: The window provider.
	// Remarks: Gets the window provider by name.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	// See: FRAppRegisterWndProvider
	//************************************
	static void* GetWndProviderByName(FR_Document doc, FS_LPCSTR lpsName);

	//************************************
	// Function:  GetReviewType
	// Param[in]: doc				The document.
	// Return: The review type.
	// Remarks: Gets the review type.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_INT32 GetReviewType(FR_Document doc);

	//************************************
	// Function:  SetReviewType
	// Param[in]: doc				The document.
	// Param[in]: nType				The input review type.
	// Return: void.
	// Remarks: Sets the review type.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void SetReviewType(FR_Document doc, FS_INT32 nType);

	//************************************
	// Function:  SetAppPermissions
	// Param[in]: doc			The document whose user permission is obtained.
	// Param[in]: dwPermission	The input application permission.
	// Return: void
	// Remarks: Sets the application permission.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void SetAppPermissions(FR_Document doc, FS_DWORD dwPermission);

	//************************************
	// Function:  GetAppPermissions
	// Param[in]: doc			The document whose user permission is obtained.
	// Param[in]: iPermission	The specified permission.
	// Return: <a>TRUE</a> if the user has the specified application permission.
	// Remarks: Checks whether the user has the specified application permission or not.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_BOOL GetAppPermissions(FR_Document doc, FS_INT32 iPermission);

	//************************************
	// Function:  GetAppPermissionsII
	// Param[in]: doc			The document whose user permission is obtained.
	// Return: The application permissions.
	// Remarks: Gets the application permissions.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_DWORD GetAppPermissionsII(FR_Document doc);

	//************************************
	// Function:  GetPermissionsII
	// Param[in]: doc			The document whose user permission is obtained.
	// Param[in]: iPermission	The input permission to check.
	// Return:  <a>TRUE</a> means the document has the permission, otherwise not. 
	// Remarks: Checks whether the document has the permission.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_BOOL GetPermissionsII(FR_Document doc, FS_INT32 iPermission);

	//************************************
	// Function:  GetMergedPermissions
	// Param[in]: doc			The document whose user permission is obtained.
	// Param[in]: iPermission	The specified permission.
	// Return: <a>TRUE</a> means the document has the permission, otherwise not.
	// Remarks: Checks whether the document has the permission.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_BOOL GetMergedPermissions(FR_Document doc, FS_INT32 iPermission);

	//************************************
	// Function:  KillFocusAnnot
	// Param[in]: doc			The document.
	// Return: <a>TRUE</a> for success, otherwise failure.
	// Remarks: Kills the focus annot.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_BOOL KillFocusAnnot(FR_Document doc);

	//************************************
	// Function:  SetPropertiesPDFVersion
	// Param[in]: doc				The document.
	// Param[in]: lpwsPDFVersion	The input PDF version that will be shown in the properties dialog.
	// Return: void
	// Remarks: Sets the PDF version that will be shown in the properties dialog.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	//************************************
	static void SetPropertiesPDFVersion(FR_Document doc, FS_LPCWSTR lpwsPDFVersion);

	//************************************
	// Function:  SetPropertiesFilePath
	// Param[in]: doc			The document.
	// Param[in]: lpwsFilePath	The input file path that will be shown in the properties dialog.
	// Return: void
	// Remarks: Sets the file path that will be shown in the properties dialog.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	//************************************
	static void SetPropertiesFilePath(FR_Document doc, FS_LPCWSTR lpwsFilePath);

	//************************************
	// Function:  DoSaveAs2
	// Param[in]: doc				The document.
	// Param[in]: pwszFilePath		The path where the document to be saved as.
	// Param[in]: proc				Callback function.
	// Param[in]: pProcData			The client data. It will be passed to the save-as callback function.
	// Param[in]: bSaveAsTempFile	Sets it FALSE as default.
	// Param[in]: bShowProgressBar	Whether to show the progress bar or not.
	// Return: <a>TRUE</a> for success, otherwise failure.
	// Remarks: Displays a file dialog box which can be used to save the document as a new name.	  	
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1
	// See: FR_DocSaveAsProc
	//************************************
	static FS_BOOL	DoSaveAs2(FR_Document doc, FS_LPCWSTR pwszFilePath, FR_DocSaveAsProc proc, void* pProcData, FS_BOOL bSaveAsTempFile, FS_BOOL bShowProgressBar);

	//************************************
	// Function:  ShowSaveProgressCancelButton
	// Param[in]: doc				The document.
	// Param[in]: bShow				Whether to show the save progress cancel button.	
	// Return: void.
	// Remarks: Sets to show the save progress cancel button or not.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1
	//************************************
	static void ShowSaveProgressCancelButton(FR_Document doc, FS_BOOL bShow);

	//************************************
	// Function:  SetInputPasswordProc
	// Param[in]: doc		The input document.
	// Param[in]: proc		Prototype of callback function invoked by <Italic>Foxit Reader</Italic> to receive the password.
	// Return: void.
	// Remarks: Sets the prototype of callback function invoked by <Italic>Foxit Reader</Italic> to receive the password.	
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.2
	//************************************
	static void SetInputPasswordProc(FR_Document doc, FRInputPasswordProc proc);

	//************************************
	// Function:  CheckInDocumentOLE
	// Param[in]: doc		The input document.
	// Return: True means the document is opened by OLE.
	// Remarks: Checks whether the document is opened by OLE. For example, the document is embedded	in <Italic>MS Office Word</Italic>.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static FS_BOOL CheckInDocumentOLE(FR_Document doc);

	//************************************
	// Function:  IsShowInBrowser
	// Param[in]: doc		The input document.
	// Return: True means the document is opened in browser.
	// Remarks: Checks whether the document is opened in browser.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static FS_BOOL IsShowInBrowser(FR_Document doc);

	//************************************
	// Function:  GetUIParentWnd
	// Param[in]: doc		The input document.
	// Return: A pointer to the parent window. It represents the <Italic>MFC CWnd*</Italic>.
	// Remarks: Gets the UI parent window.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static void* GetUIParentWnd(FR_Document doc);

	//************************************
	// Function:  GetDocFrameHandler
	// Param[in]: doc		The input document.
	// Return: The frame handler associated with the document.
	// Remarks: Gets the frame handler associated with the document.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static HWND GetDocFrameHandler(FR_Document doc);

	//************************************
	// Function:  CreateNewViewByWndProvider
	// Param[in]: doc		The input document.
	// Param[in]: lpsName	The Specified name of the window provider.
	// Return: void.
	// Remarks: Creates the new view by <a>FR_WndProviderCallbacksRec</a>.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	// See: FRAppRegisterWndProvider
	//************************************
	static void CreateNewViewByWndProvider(FR_Document doc, FS_LPCSTR lpsName);

	//************************************
	// Function:  LoadAnnots
	// Param[in]: doc			The input document.
	// Param[in]: arrAnnotDict	The pointer array holding the annotation dictionaries to be loaded.
	// Return: Non-zero means success, otherwise failure.
	// Remarks: Loads annotation(s) for an opening PDF document. 
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	//************************************
	static FS_BOOL LoadAnnots(FR_Document doc, FS_PtrArray arrAnnotDict);

	//************************************
	// Function:  GetDocShowTitle
	// Param[in]: doc				The document.
	// Param[out]: outTitle			It receives the title shown as the main frame title and the document tab title.
	// Return: void.
	// Remarks: Gets the title shown as the main frame title and the document tab title.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 3.0.0.0
	//************************************
	static void GetDocShowTitle(FR_Document doc, FS_WideString* outTitle);

	//************************************
	// Function:  DoSave2
	// Param[in]: doc				The document to be saved.
	// Param[in]: proc				Callback function.
	// Param[in]: pProcData			The client data. It will be passed to the save callback function.
	// Param[in]: bShowProgressBar	Whether to show the progress bar or not.
	// Param[in]: bDoPDFOptimize	Whether to optimize the PDF or not.
	// Return: True for success, otherwise failure.
	// Remarks:	Saves a file, handling any user interface(for example, a Save File dialog box) if need.  	
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 3.0.0.0
	// See: FRDocDoSave
	// See: FRDocDoSaveAs
	// See: FRDocSetChangeMark
	//************************************
	static FS_BOOL DoSave2(FR_Document doc, FR_DocSaveProc proc, void* pProcData, FS_BOOL bShowProgressBar, FS_BOOL bDoPDFOptimize);

	//************************************
	// Function:  ResetDocTitleColor
	// Param[in]: doc				The input document.
	// Return: void
	// Remarks:	Resets the document title color in the document tab.  	
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 3.0.0.0
	// See: FRDocSetDocTitleColor
	//************************************
	static void ResetDocTitleColor(FR_Document doc);

	//************************************
	// Function:  SetDocTitleColor
	// Param[in]: doc				The input document.
	// Param[in]: clrDocTitle		The input color value.
	// Return: void
	// Remarks:	Sets the document title color in the document tab.  	
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 3.0.0.0
	// See: FRDocResetDocTitleColor
	//************************************
	static void SetDocTitleColor(FR_Document doc, FS_COLORREF clrDocTitle);

	//************************************
	// Function:  GetOriginalType
	// Param[in]: doc				The input document.
	// Return: The original type of the document. The real format of the opened document is PDF, but its wrapper format may be PPDF.
	// Remarks:	Gets the original type of the document. The real format of the opened document is PDF, but its wrapper format may be PPDF.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 3.0.0.0
	// See: FRDocSetOriginalType
	//************************************
	static FROriginalDocType GetOriginalType(FR_Document doc);

	//************************************
	// Function:  SetOriginalType
	// Param[in]: doc			The input document.
	// Param[in]: oriDocType	The input original type of the document.
	// Return: void.
	// Remarks:	Sets the original type of the document. The real format of the opened document is PDF, but its wrapper format may be PPDF.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 3.0.0.0
	// See: FRDocGetOriginalType
	//************************************
	static void SetOriginalType(FR_Document doc, FROriginalDocType oriDocType);

	//************************************
	// Function:  SetFocusAnnot
	// Param[in]: doc				The input document.
	// Param[in]: pFocusAnnot		The input annotation.
	// Param[in]: bDelay			Whether to delay the setting or not. The default value is FALSE.
	// Return: void
	// Remarks:	Sets the focus annotation.  	
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1.0.0
	//************************************
	static FS_BOOL SetFocusAnnot(FR_Document doc, FR_Annot pFocusAnnot, FS_BOOL bDelay);

	//************************************
	// Function:  GenerateUR3Permission
	// Param[in]: doc				The input document.
	// Return: TRUE for success, otherwise failure.
	// Remarks:	Generates the UR3 signature. Usage rights signatures are used to enable additional interactive features that are not available by
	// default in a particular viewer application (such as Adobe Reader). See PDF reference for more details.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1.0.0
	//************************************
	static FS_BOOL	GenerateUR3Permission(FR_Document doc);

	//************************************
	// Function:  HasRedactAnnot
	// Param[in]: doc		The input document.
	// Return: TRUE if the document is marked for redaction, otherwise not.
	// Remarks:	Checks whether the document is marked for redaction or not.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1.0.0
	//************************************
	static FS_BOOL HasRedactAnnot(FR_Document doc);

	//************************************
	// Function:  GenerateRedactions
	// Param[in]: doc			The input document.
	// Param[out]: wsFilePath	It receives the path of the document redacted.	
	// Return: TRUE for success, otherwise for failure.
	// Remarks: Generates a redacted document and it will be saved to <param>wsFilePath</param>.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1.0.0
	//************************************
	static FS_BOOL GenerateRedactions(FR_Document doc, FS_WideString* wsFilePath);

	//************************************
	// Function:  ReloadPage
	// Param[in]: doc			The input document.
	// Param[in]: iPage			The specified page index.
	// Param[in]: bDisableGoto	Whether to prevent going to the specified page view. Sets it FALSE as default.
	// Return: TRUE for success, otherwise for failure.
	// Remarks: Reloads the specified page.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.2.2
	//************************************
	static FS_BOOL ReloadPage(FR_Document doc, FS_INT32 iPage, FS_BOOL bDisableGoto);

	//************************************
	// Function:  ForbidChangeMark
	// Param[in]: doc			The document.
	// Param[in]: bForbid		Whether to forbid setting the modify flag or not.
	// Return:	void
	// Remarks: Forbid setting the modify flag. Reader has a built-in flag that indicate whether a document has been modified,
	// if the value of the flag is valid, the Save button is enable, otherwise the Save button is 
	// disable.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.0.0
	// See: FRDocSetChangeMark
	// See: FRDocGetChangeMark
	// See: FRDocClearChangeMark
	//************************************
	static void ForbidChangeMark(FR_Document doc, FS_BOOL bForbid);

	//************************************
	// Function:  OpenFromPDDoc2
	// Param[in]: pddoc				The <a>FR_Document</a> object to open.
	// Param[in]: lpwsTitle			It is used as window's title.
	// Param[in]: bDelPDFDoc		Whether the framework must delete the <a>FPD_Document</a> object or not. If the plug-in 
	// deletes the <a>FPD_Document</a> object through <a>FPDParserDestroy</a>(), sets it FALSE.
	// Return: The document that was opened. It returns <a>NULL</a> if Reder failed to open document.
	// Remarks:	Opens and returns a <a>FR_Document</a> view of <a>PD_Document</a>.  	
	// Notes: Do not open and then immediately close a FR_Document without letting least once event loop complete.
	// <a>FRDocClose</a>() should be used in place of <a>FPDDocClose</a>() after FRDocOpenFromPDDoc() is called.
	// <a>FRDocClose</a>() will decrement the FPDDoc appropriately and free document-related resources.
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1.0
	// See: FRDocClose
	// See: FRDocOpenFromFile
	// See: FRDocFromPDDoc
	//************************************
	static FR_Document	OpenFromPDDoc2(FPD_Document pddoc, FS_LPCWSTR lpwsTitle, FS_BOOL bDelPDFDoc);

	//************************************
	// Function:  GetCreateDocSource
	// Param[in]: doc			The document.
	// Return:	The source type of the document.
	// Remarks: Gets the source type of the document.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1.0
	//************************************
	static FRCreateDocSource GetCreateDocSource(FR_Document doc);

	//************************************
	// Function:  SetCreateDocSource
	// Param[in]: doc					The document.
	// Param[in]: sourceType			The input source type of the document.
	// Param[in]: lpwsSourceFileName	The input source file name.	
	// Return:	void.
	// Remarks: Sets the source type of the document.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1.0
	//************************************
	static void SetCreateDocSource(FR_Document doc, FRCreateDocSource sourceType, FS_LPCWSTR lpwsSourceFileName);

	//************************************
	// Function:  GetCreateDocSourceFileName
	// Param[in]: doc					The document.
	// Param[out]: outSourceFileName	It receives the source file name.
	// Return:	void.
	// Remarks: Gets the source file name.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1.0
	//************************************
	static void GetCreateDocSourceFileName(FR_Document doc, FS_WideString* outSourceFileName);

	//************************************
	// Function:  ParsePage
	// Param[in]: doc			The document.
	// Param[in]: nPageIndex	The specified page index.
	// Return:	TRUE for success, otherwise failure.
	// Remarks: Parses the specified page.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1.0
	//************************************
	static FS_BOOL ParsePage(FR_Document doc, FS_INT32 nPageIndex);

	//************************************
	// Function:  IsValidAnnot
	// Param[in]: doc			The document.
	// Param[in]: frAnnot		The input annotation to be checked.
	// Return:	TRUE if the annotation is valid.
	// Remarks: Checks whether the annotation is valid or not.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1.0
	//************************************
	static FS_BOOL IsValidAnnot(FR_Document doc, FR_Annot frAnnot);

	//************************************
	// Function:  IsWillReopen
	// Param[in]: doc			The document.
	// Return:	TRUE if the document is to be reopened.
	// Remarks: Checks whether the document is to be reopened after it is closed.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.0.2
	//************************************
	static FS_BOOL IsWillReopen(FR_Document doc);

	//************************************
	// Function:  OpenFromFile2
	// Param[in]: wszFile				The file path.
	// Param[in]: szPassword			The password with the file. It could be <a>NULL</a> if no password to pass.
	// Param[in]: bMakeVisible			Whether the document will be made visible.
	// Param[in]: bAddToMRU				Whether add the file path to the recent file list.
	// Param[in]: bCheckCertPassword	Whether prompt a password input dialog to check the password of the certificate used to encrypt the document.
	// Return:	The document that was opened. It returns <a>NULL</a> if Reder failed to open document.
	// Remarks: Opens and displays a document form a file.	  	
	// Notes: Do not open and then immediately close a FR_Document without letting least once event loop complete.
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.1
	// See: FRDocClose
	// See: FRDocOpenFromPDDoc
	// See: FRDocFromPDDoc
	//************************************
	static FR_Document OpenFromFile2(FS_LPCWSTR wszFile, FS_LPCSTR szPassword, FS_BOOL bMakeVisible, FS_BOOL bAddToMRU, FS_BOOL bCheckCertPassword);

	//************************************
	// Function:  GetSignaturePermissions
	// Param[in]: doc				The document whose user permission is obtained.
	// Param[out]: pdwPermissions	The document permissions.  
	// Return:  <a>TRUE</a> if the document is signed.
	// Remarks: Gets the signature permissions of a document.  
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.2.1
	//************************************
	static FS_BOOL	GetSignaturePermissions(FR_Document doc, FS_DWORD *pdwPermissions);

	//************************************
	// Function:  ConvertPdfToOtherFormat
	// Param[in]: wsSrcPath			The source file which need to be converted.
	// Param[in]: wsDesPath			The destination file path.
	// Param[in]: pageAry			The pages need to be converted. if it is NULL ,will convert the whole document. 
	// Param[in]: wsFileExt			The format need to be convert. Support: docx, doc, xlsx, xls, html, pptx, rtf.
	// Param[in]: wsPwd				The password of encrypt the document. if it is empty, will not encrypt the document.
	// Param[in]: bShowProgress		Whether or not show the convert progress bar.
	// Return:  <a>TRUE</a> converted sucessful.
	// Remarks: Convert Pdf document to other format document.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3.1
	//************************************
	static FS_BOOL	ConvertPdfToOtherFormat(FS_LPCWSTR wsSrcPath, FS_LPCWSTR wsDesPath, FS_DWordArray pageAry, FS_LPCWSTR wsFileExt, FS_LPCWSTR wsPwd, FS_BOOL bShowProgress);

	//************************************
	// Function:  DoPassWordEncrypt
	// Param[in]: frDoc			The document to protected.
	// Param[in]: proc			Prototype of callback function invoked by <Italic>Foxit Reader</Italic> to receive the protecting status.
	// Return:  void.
	// Remarks: Set the document password through the password input dialog.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.1
	//************************************
	static void DoPassWordEncrypt(FR_Document frDoc, FRPasswordEncryptProc proc);

	//************************************
	// Function:  IsInProtectedViewMode
	// Param[in]: frDoc			The document to protected.
	// Return:  <a>TRUE</a> means in the protected view mode.
	// Remarks: Check if the document is in protected view mode.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.1
	//************************************
	static FS_BOOL IsInProtectedViewMode(FR_Document frDoc);

	//************************************
	// Function:  GetCreateDocSourceFilePath
	// Param[in]: doc					The document.
	// Param[out]: outSourceFilePath	It receives the source file path.
	// Return:	void.
	// Remarks: Gets the source file path.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.1
	//************************************
	static void GetCreateDocSourceFilePath(FR_Document doc, FS_WideString* outSourceFilePath);

	//************************************
	// Function:  IsImageBasedDocument
	// Param[in]: frDoc			The document.
	// Return:  <a>TRUE</a> means the document is Image Based Document.
	// Remarks: Check if the document is Image Based Document.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.2
	//************************************
	static FS_BOOL IsImageBasedDocument(FR_Document frDoc);

	//************************************
	// Function:  SetDocEncrypted
	// Param[in]: frDoc			The document.
	// Param[in]: bEncrypted	Indicates whether the document is encrypted or not.
	// Return:  void.
	// Remarks: Indicates whether the document is encrypted or not.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.2
	//************************************
	static void SetDocEncrypted(FR_Document frDoc, FS_BOOL bEncrypted);

	//************************************
	// Function:  IsVisible
	// Param[in]: frDoc			The document.
	// Return:  Whether the opened document is visible or not.
	// Remarks: Checks whether the opened document is visible or not.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.3
	//************************************
	static FS_BOOL IsVisible(FR_Document frDoc);

	//************************************
	// Function:  SetDRMSecurity
	// Param[in]: doc				The input document.
	// Param[in]: encryptDict		The Encrypt dictionary.
	// Param[in]: cryptoHandler		The callbacks for crypto handler.
	// Param[in]: bEncryptMetadata	Whether to encrypt the metadata.
	// Param[in]: clientHandler		The user-supplied data.
	// Return:	void
	// Remarks:	 Sets security using custom security handler and custom encryption.
	// Application should provide a full encryption dictionary (application can destroy it after this call),
	// and a custom encryption handler. 	
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.5
	//************************************
	static void				SetDRMSecurity(
											FR_Document doc,
											FPD_Object encryptDict,
											FR_DRMCryptoCallbacks cryptoHandler,
											FS_BOOL bEncryptMetadata,
											FS_LPVOID clientHandler);

	//************************************
	// Function:  ConvertPdfToOtherFormat2
	// Param[in]: wsDesPath			The destination file path.
	// Param[in]: wsFileExt			The format need to be convert. Support: docx, doc, xlsx, xls, html, pptx, rtf...
	//								Format example: Word Document(.docx) | .docx
	// Return:  <a>TRUE</a> converted sucessful.
	// Remarks: Convert Pdf document to other format document.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7
	//************************************
	static FS_BOOL	ConvertPdfToOtherFormat2(FS_LPCWSTR wsDesPath, FS_LPCSTR szFileExt);

	//************************************
	// Function:  AddWatermark
	// Param[in]: doc				The input document.
	// Param[in]: pWatermarkInfo	The watermark setting information.
	// Return:  <a>0</a> add watermark sucessful.
	// Remarks: add watermark to document.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7
	//************************************
	static FS_INT32 AddWatermark(FR_Document doc, FR_WatemarkElementInfo *pWatermarkInfo);

	//************************************
	// Function:  AddAndUpdateWatermark
	// Param[in]: doc				The input document.
	// Param[in]: pWatermarkInfo	The watermark setting information.
	// Return:  <a>0</a> add watermark fail.
	// Remarks: add watermark to document and update the document view.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7
	//************************************
	static FS_INT32 AddAndUpdateWatermark(FR_Document doc, FR_WatemarkElementInfo *pWatermarkInfo);

	//************************************
	// Function:  RemoveWatermark
	// Param[in]: doc				The input document.
	// Param[in]: szSpecifyDicValue	remove the specify watermark that add with <a>FR_PWatemarkElementInfo</a> lpbSpecifyWatermarkDicValue,
	//								can be pass null means remove all watermark.
	// Return:  <a>0</a> remove watermark fail.
	// Remarks: remove watermark to document.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7
	//************************************
	static FS_INT32 RemoveWatermark(FR_Document doc, FS_LPCSTR szSpecifyDicValue);

	//************************************
	// Function:  RemoveAndUpdateWatermark
	// Param[in]: doc				The input document.
	// Param[in]: szSpecifyDicValue	remove the specify watermark that add with <a>FR_PWatemarkElementInfo</a> lpbSpecifyWatermarkDicValue,
	//								can be pass null means remove all watermark.
	// Return:  <a>0</a> remove watermark fail.
	// Remarks: remove watermark to document and update the document view.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7
	//************************************
	static FS_INT32 RemoveAndUpdateWatermark(FR_Document doc, FS_LPCSTR szSpecifyDicValue);

	//************************************
	// Function:  AddHeaderFooter
	// Param[in]: doc				The input document.
	// Param[in]: pHeaderFooterInfo	The header and footer setting information.
	// Return:  <a>0</a> add header and footer fail.
	// Remarks: add header and footer to document.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7
	//************************************
	static FS_INT32 AddHeaderFooter(FR_Document doc, FR_HeaderFooterElementInfo *pHeaderFooterInfo);

	//************************************
	// Function:  AddAndUpdateHeaderFooter
	// Param[in]: doc				The input document.
	// Param[in]: pHeaderFooterInfo	The header and footer setting information.
	// Return:  <a>0</a> add header and footer fail.
	// Remarks: add header and footer to document and update the document view.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7
	//************************************
	static FS_INT32 AddAndUpdateHeaderFooter(FR_Document doc, FR_HeaderFooterElementInfo *pHeaderFooterInfo);

	//************************************
	// Function:  RemoveHeaderFooter
	// Param[in]: doc				The input document.
	// Param[in]: szSpecifyDicValue	remove the specify header and footer that add with <a>FR_PHeaderFooterElementInfo</a>				
	//								lpbSpecifyHeaderFooterDicValue, can be pass null means remove all header and footer.
	// Return:  <a>0</a> remove header and footer fail.
	// Remarks: remove header and footer to document.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7
	//************************************
	static FS_INT32 RemoveHeaderFooter(FR_Document doc, FS_LPCSTR szSpecifyDicValue);

	//************************************
	// Function:  RemoveAndUpdateHeaderFooter
	// Param[in]: doc				The input document.
	// Param[in]: szSpecifyDicValue	remove the specify header and footer that add with <a>FR_PHeaderFooterElementInfo</a>				
	//								lpbSpecifyHeaderFooterDicValue, can be pass null means remove all header and footer.
	// Return:  <a>0</a> remove header and footer fail.
	// Remarks: remove header and footer to document and update the document view.
	// Notes:	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7
	//************************************
	static FS_INT32 RemoveAndUpdateHeaderFooter(FR_Document doc, FS_LPCSTR szSpecifyDicValue);

	//************************************
	// Function:  IsReadSafeMode
	// Param[in]: doc				The input document.
	// Return:	Return true if the doc is Read Safe Mode.
	// Remarks: 
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7.1	
	//************************************
	static FS_BOOL IsReadSafeMode(FR_Document doc);

	//************************************
	// Function:  GeneratePageContent
	// Param[in]: doc			The input document.
	// Param[in]: bProgress		Indicates whether to show the progress bar or not.
	// Return:	void.
	// Remarks: Generates the page content.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7.1	
	//************************************
	static void	GeneratePageContent(FR_Document doc, FS_BOOL bProgress);


	//************************************
	// Function:  GetFocusAnnot
	// Param[in]: frDoc		The input document.
	// Return: The focus annotation.
	// Remarks: Gets the focus annotation including form control.
	// Notes: 
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.0
	//************************************
	static FR_Annot GetFocusAnnot(FR_Document frDoc);

	static FS_BOOL GetMenuEnableByName(FR_Document frDoc, FS_LPCSTR csName);

	static void ClearAllSelection(FR_Document frDoc);

	static void AddSelection(FR_Document frDoc, FR_Annot annot);

	static FR_Annot GetPageFirstAnnot(FR_Document frDoc, FS_INT32 nPageIndex, FS_LPCSTR csType);

	static FR_Annot CreateFormControl(FR_Document frDoc, FS_INT32 nPageIndex, FS_FloatRect rect, FS_INT32 nType, FS_INT32 iRotate, FS_WideString sName, FS_WideString bUseSuggestName);

	static void ReCalTabOrder(FR_Document frDoc, FS_INT32 nPageIndex);

	static FR_ReaderInterform  GetInterForm(FR_Document frDoc);

	static void NotifyAddWidget(FR_Annot annot);

	static void UpdateAllViews(FR_PageView pageview, FR_Annot pAnnot);

	static FR_Annot GetAnnotByDict(FR_Document frDoc, FS_INT32 nPageIndex, FPD_Object pDict);

	static FR_Annot AddAnnot(FR_Document frDoc, FS_INT32 nPageIndex, FS_LPCSTR lpSubType, const FS_FloatRect rect);

	static FR_Annot AddAnnot2(FR_Document frDoc, FS_INT32 nPageIndex, FPD_Object pDict, FS_INT32 nIndex);

	static void ReplaceTextToSelectedText(FR_Document doc, int nPageIndex, int nIndex, FS_LPCWSTR replaceText);

	static FS_BOOL ReplacePages(FR_Document frDoc, FS_INT32 nStart, const FPD_Document pSrcDoc, const FS_WordArray &arrSrcPages);
	static FS_BOOL InsertPages(FR_Document frDoc, FS_INT32 nInsertAt, const FPD_Document pSrcDoc, const FS_WordArray &arrSrcPages, FS_BOOL bRenameForm, FS_BOOL bShowRenameFormDlg, FS_BOOL bEntireDoc, FS_WideString srcDocTitle , FS_BOOL bRetainBookmark);
	static FS_BOOL ExtractPages(FR_Document frDoc, const FS_WordArray &arrExtraPages, FPD_Document pDstDoc);

	static FS_BOOL GetDocSecurityCanBeModified(FR_Document frDoc);
	
	static FR_Document GetDocument(FS_LPVOID pReaderPage);
	static FR_Annot GetAnnotByDict2(FS_LPVOID pReaderPage, FPD_Object pDict);
	static FS_INT32	GetPageIndex(FS_LPVOID pReaderPage);
	static FS_BOOL RemoveSecurityInfo(FR_Document frDoc);
	static void  UpdateDocAllViews(FR_Document frDoc, FR_DocView sender);

	static FR_MailtoResult SendAsAttachment(FR_Document frDoc, const FS_WideString& wsTo, const FS_WideString& wsCc, const FS_WideString& wsBcc,
		const FS_WideString& wsSubject, const FS_WideString& wsMsg,
		FS_BOOL bUI, FS_BOOL& bDocReopened, FS_WideString* wsErrorMessage);
	static FS_BOOL OnBeforeNotifySumbit(FR_Document frDoc, FS_LPVOID pSubmit);
	static void OnAfterNotifySumbit(FR_Document frDoc);
};

class CFR_CustomSignature_V8
{
public:
	//************************************
	// Function:  GenerateSignInfo
	// Param[in]: pSgInfo		To fill the signature info.
	// Param[in]: pSgPosInfo	To fill the signature position info.
	// Return:	TRUE for success, otherwise failure.
	// Remarks: Signs a PDF document with the <a>FR_SignatureInfo</a> and <a>FR_SignaturePosInfo</a>. You have to register the signature 
	// handler by <a>FRCustomSignatureRegisterSignatureHandler</a>to sign the data, otherwise the data will be signed with the default standard method.
	// Notes: 
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.2.2
	//************************************	
	static FS_BOOL GenerateSignInfo(FR_SignatureInfo* pSgInfo, FR_SignaturePosInfo* pSgPosInfo);

	//************************************
	// Function:  GetDefaultServer
	// Param[out]: pSgTMServer	It receives the default timestamp server.
	// Return:	TRUE for success, otherwise failure.
	// Remarks: Gets the default timestamp server.
	// Notes: 
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.2.2
	//************************************	
	static FS_BOOL GetDefaultServer(FR_SignatureTimestampServer* pSgTMServer);

	//************************************
	// Function:  CreateSignatureHandler
	// Param[in]: callbacks	The callback set for signature handler. 
	// Return:	The pointer to the signature handler.
	// Remarks: Creates the signature handler. Registers it by <a>FRCustomSignatureRegisterSignatureHandler</a>. Destroys it by <a>FRCustomSignatureDestroySignatureHandler</a>.
	// Notes: 
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.2.2
	//************************************
	static void* CreateSignatureHandler(FR_SignatureHandlerCallbacks callbacks);

	//************************************
	// Function:  RegisterSignatureHandler
	// Param[in]: pSignatureHandler	The input pointer to signature handler.
	// Return:	TRUE for success, otherwise failure.
	// Remarks: Registers the signature handler. You can customize the process signing the data and the process verifying the digital signature.
	// Notes: 
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.2.2
	//************************************
	static FS_BOOL RegisterSignatureHandler(void* pSignatureHandler);

	//************************************
	// Function:  DestroySignatureHandler
	// Param[in]: pSignatureHandler	The input pointer to signature handler. 
	// Return:	void.
	// Remarks: Destroys the signature handler returned by <a>FRCustomSignatureCreateSignatureHandler</a>.
	// Notes: 
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.2.2
	//************************************
	static void DestroySignatureHandler(void* pSignatureHandler);

	//************************************
	// Function:  SetSignatureVerify
	// Param[in]: frDoc				The input document.		
	// Param[in]: pSignedData		The signed data.
	// Param[in]: ulSignedDataLen	The length of the signed data.
	// Return:	TRUE for success, otherwise failure.
	// Remarks: Verifies the specified signature.
	// Notes: 
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3
	//************************************
	static FS_BOOL SetSignatureVerify(FR_Document frDoc, const unsigned char* pSignedData, unsigned long ulSignedDataLen);

	//************************************
	// Function:  GetDocSigatureCount
	// Param[in]: frDoc	The input document.
	// Return:	The signature count.
	// Remarks: Gets the signature count.
	// Notes: 
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.0
	//************************************
	static FS_INT32 GetDocSigatureCount(FR_Document frDoc);

	//************************************
	// Function:  GetSignatureBaseInfo
	// Param[in]: frDoc		The input document.
	// Param[in]: nIndex	The index of the signature.
	// Param[out]: pInfo	It receives the base info of the signature.
	// Return:	TRUE for success, otherwise failure.
	// Remarks: Gets the specified base info of the signature.
	// Notes: 
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.0
	//************************************
	static FS_BOOL GetSignatureBaseInfo(FR_Document frDoc, FS_INT32 nIndex, FR_SignatureBaseInfo* pInfo);

	//************************************
	// Function:  ClearSignature
	// Param[in]: frDoc		The input document.
	// Param[in]: nIndex	The index of the signature.
	// Return:	TRUE for success, otherwise failure.
	// Remarks: Clears the specified signature.
	// Notes: 
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.0
	//************************************
	static FS_BOOL ClearSignature(FR_Document frDoc, FS_INT32 nIndex);

	static FS_BOOL FoxitSignInScope(FR_Document frDoc, FPD_Object pAnnotDict);

	static FPD_Object FoixtCreateSignatureF(FR_Document frDoc, FS_BOOL bSetSignS);

	static void SetPosition(FR_PageView pageViewer, FPD_Object objDict, const FS_FloatRect rcSig);
};

class CFR_ReaderInterform_V13
{
public:
	static  FPD_InterForm	GetInterForm(FR_ReaderInterform frInerForm);
	static  FPD_Object		LoadImageFromFile(FR_ReaderInterform frInerForm, const FS_WideString sFile, FS_INT32 nWidth, FS_INT32 nHeight);
	static  FS_INT32		ResetFieldAppearance(FR_ReaderInterform frInerForm, FPD_FormField pFormField, FS_LPCWSTR sValue, FS_BOOL bValueChanged, FS_BOOL bNeedFormat);
	static  FS_INT32		OnCalculate(FR_ReaderInterform frInerForm,FPD_FormField pFormField, FS_INT32 nPage);
	static  FS_INT32		OnCalculate2(FR_ReaderInterform frInerForm, FS_INT32 nPage);
	static  FS_BOOL			IsFormFieldInPage(FR_ReaderInterform frInerForm, FPD_FormField pFormField, FS_INT32 nPage);
};


class CFR_UndoItemCreateWidget_V13
{
public:
	static FR_UndoCreateWidget          CreateUndoWidget(FR_Document frDoc, FS_INT32 nPageIndex, FPD_Object pFieldDict, FPD_Object pAnnotDict, const FS_INT32 nTabOrder);
	static void							Undo(FR_UndoCreateWidget undoWidget);
	static void							Redo(FR_UndoCreateWidget undoWidget);
	static void    				        GetDescr(FR_UndoCreateWidget undoWidget, FS_WideString* outDesc);
	
};

class CFR_CRSAStamp_V13
{
public:
	static FR_CRSAStamp New();
	static void Destroy(FR_CRSAStamp rStamp);
	static FS_INT32 GetLeftValue(FR_CRSAStamp rStamp);
	static FS_INT32 GetTopValue(FR_CRSAStamp rStamp);
	static FS_INT32 GetDefaultCropWValue(FR_CRSAStamp rStamp);	
	static void SetDefaultCropWValue(FR_CRSAStamp rStamp, FS_INT32 width);
	static FS_INT32 GetDefaultCropHValue(FR_CRSAStamp rStamp);
	static void SetDefaultCropHValue(FR_CRSAStamp rStamp, FS_INT32 height);
	static FS_INT32 GetDefaultWValue(FR_CRSAStamp rStamp);
	static void SetDefaultWValue(FR_CRSAStamp rStamp, FS_INT32 width);
	static FS_INT32 GetDefaultHValue(FR_CRSAStamp rStamp);
	static void SetDefaultHValue(FR_CRSAStamp rStamp, FS_INT32 height);
	static FS_FLOAT GetWidthValue(FR_CRSAStamp rStamp);
	static void SetWidthValue(FR_CRSAStamp rStamp, FS_FLOAT width);
	static FS_FLOAT GetHeightValue(FR_CRSAStamp rStamp);
	static void SetHeightValue(FR_CRSAStamp rStamp, FS_FLOAT height);
	static FS_BOOL IsPath(FR_CRSAStamp rStamp);
	static void SetPath(FR_CRSAStamp rStamp,FS_BOOL bPath);
	static FS_DIBitmap GetDIBitmap(FR_CRSAStamp rStamp);
	static void SetDIBitmap(FR_CRSAStamp rStamp, FS_DIBitmap pDIB);
	static FS_BOOL IsPDFSignStamp(FR_CRSAStamp rStamp);
	static void GetPDFPath(FR_CRSAStamp rStamp, FS_WideString* wsPath);
	static FS_BOOL IsDynamic(FR_CRSAStamp rStamp);
	static void GetEncode(FR_CRSAStamp rStamp, FS_ByteString* bsEncode);
	static FS_INT32 GetStampType(FR_CRSAStamp rStamp);
	static void SetPageIdx(FR_CRSAStamp rStamp,FS_INT32 nPage);
	static FS_INT32 GetPageIdx(FR_CRSAStamp rStamp);
	static FS_BOOL PreViewArtBoxFile(FR_CRSAStamp rStamp,FPD_Document pPDFDoc, FS_INT32 nIndex, FS_DIBitmap pDIBitmap);
	static void GetStampName(FR_CRSAStamp rStamp, FS_WideString* wsName);
	static FS_BOOL IsFavorite(FR_CRSAStamp rStamp);
	static FS_BOOL SetPagePieceInfo(FR_CRSAStamp rStamp, FR_PIECE_INFO_KEY nType);
	static FS_BOOL SetPagePieceInfoByDoc(FR_CRSAStamp rStamp, FPD_Document pPDFDoc, FR_PIECE_INFO_KEY nType);
	static FS_BOOL GetIsRotate(FR_CRSAStamp rStamp);
	static FS_INT32  RemovePage(FR_CRSAStamp rStamp);
	static void  Copy(FR_CRSAStamp dStamp, FR_CRSAStamp rStamp);
	static FS_INT32	GetPageFromName(FPD_Document pPDFDoc, const FS_WideString wStrPageName);
	static void		SetStampName(FR_CRSAStamp rStamp, const FS_WideString& cwStampName);
	static void SetPDFPath(FR_CRSAStamp rStamp, const FS_WideString& cwPDFPath);
	static void SetAppCloseDelete(FR_CRSAStamp rStamp, FS_BOOL bDelete);
	static void SetType(FR_CRSAStamp rStamp, const FS_ByteString& byteType);	
	static void SetEncode(FR_CRSAStamp rStamp, const FS_LPCSTR& strEncode);
	static void SetSignListIdx(FR_CRSAStamp rStamp, FS_INT32 nIdx);
	static FS_BOOL GetPDFStamp(FR_CRSAStamp rStamp);
	static FS_BOOL SaveStampToPage(FR_CRSAStamp pStamp);
	static FPD_Object GetSampInfoDic(FPD_Object pPageDic, FPD_Document pPDFDoc, FS_BOOL bAdd);
	static void AddPageName(FPD_Document pPDFDoc, FS_INT32 nPageIdx, FS_WideString* strPageName, FS_BOOL bPageNamePrefix);
	static void SetLeftValue(FR_CRSAStamp rStamp, FS_FLOAT left);
	static void SetTopValue(FR_CRSAStamp rStamp, FS_FLOAT top);	
	static void SetDynamic(FR_CRSAStamp rStamp, FS_BOOL bDynamic);
	static void SetFavorite(FR_CRSAStamp rStamp, FS_BOOL bFav);
	static void GetType(FR_CRSAStamp rStamp, FS_ByteString* type);
	static void SetIsRotate(FR_CRSAStamp rStamp, FS_BOOL bRotate);
	static FRRS_PASSWORD CheckPDFPermission(FR_CRSAStamp rStamp, FS_WideString cwFileName, FS_WideString* wsRemoveSecPath);
	static FS_BOOL FSFadeOutImageBackground(const FS_DIBitmap pDib,FS_DIBitmap* pDib_with_mask,FREnhancementAlogrithms enhAlgo,	FS_LPVOID enhParams,FRThreshAlogrithm threshAlgo, FS_LPVOID threshParams);	
	static void SetTip(FR_CRSAStamp rStamp, const FS_WideString cwTip);
	static void SetTemplateName(FR_CRSAStamp rStamp, const FS_WideString csTemplateName);
	static void SetThumbNail(FR_CRSAStamp rStamp, const FS_WideString cwThumbnail);
	static void SetCanParser(FR_CRSAStamp rStamp, FS_BOOL bCanParser);
	static void SetImage(FR_CRSAStamp rStamp, FS_BOOL bImage);
	static void SetOpacity(FR_CRSAStamp rStamp, FS_INT32 nOpacity);
	static void GetSignFilePwd(const FS_WideString wStrSignPassword, FS_WideStringArray* fileArray, FS_ByteStringArray* pwdArray);
	static void GetStampFormPage(FR_CRSAStamp rStamp, FPD_Page pPage);
	static void SaveStampToPage2(FPD_Document pPDFDoc, FR_CRSAStamp pRStamp);
	static FS_INT32 GetSignListIdx(FR_CRSAStamp rStamp);
	static void SetStampGuid(FR_CRSAStamp rStamp, const FS_WideString cwStampGuid);
	static void GetStampGuid(FR_CRSAStamp rStamp, FS_WideString* wsGuid);
};

class CFR_MarkupAnnot_V13
{
public:
	static FR_MarkupAnnot Create(FPD_Object pAnnot, FR_PageView pPageView);
	static void Destroy(FR_MarkupAnnot annot);
	static FS_BOOL IsGroup(FR_MarkupAnnot annot);
	static FS_BOOL IsHeader(FR_MarkupAnnot annot);
	static FR_MarkupPopup GetPopup(FR_MarkupAnnot annot,FS_BOOL bCreate);
	static void ResetAppearance(FR_MarkupAnnot annot);
	static void SetModifiedDateTime(FR_MarkupAnnot annot, FS_ReaderDateTime dt);
	static FS_BOOL GetModifiedDateTime(FR_MarkupAnnot annot, FS_ReaderDateTime* dt);
	static FS_FloatPoint  GetHotPoint(FR_MarkupAnnot annot);
	static FS_BOOL GetColor(FR_MarkupAnnot annot, FS_COLORREF* color);
};

class CFR_MarkupPopup_V13
{
public:
	static void UpdateDataTime(FR_MarkupPopup popup, FR_PageView pPageView);
	static void SetNoteAnchor(FR_MarkupPopup popup, FS_FloatPoint cpPoint, FR_PageView pPageView);
	static void ShowRope(FR_MarkupPopup popup, FS_BOOL bShow, FR_PageView pPageView);
	static FS_BOOL IsNoteVisible(FR_MarkupPopup popup, FR_PageView pPageView);
	static void   UpdateNote(FR_MarkupPopup popup, FR_PageView pPageView, FS_BOOL bCreateIfPageInvisible);
	static void ResetNotePosition(FR_MarkupPopup popup, FR_PageView pPageView);
	static FR_Annot      GetPopupAnnot(FR_MarkupPopup popup);	
	static FS_FloatRect GetRect(FR_MarkupPopup popup);	
	static FS_BOOL SetState(FR_MarkupPopup popup, FS_BOOL bOpen);
	static void  ShowNote(FR_MarkupPopup popup, FS_BOOL bShow, FR_PageView pageView);	
	static void SetNoteFocus(FR_MarkupPopup popup, FS_BOOL bSet, FR_PageView pageView);
	static void AddReply(FR_MarkupPopup popup, FR_PageView pageView);
};

class CFR_MarkupPanel_V13
{
public:
	static FR_MarkupPanel GetMarkupPanel();
	static FS_BOOL RefreshAnnot(FR_MarkupPanel panel, FR_Annot pAnnot, FS_BOOL bRedrawPanel);
	static FS_BOOL ReloadAnnots(FR_MarkupPanel panel,FR_Document pDoc);
	static FS_BOOL AddAnnot(FR_MarkupPanel panel, FR_Annot pAnnot, FS_BOOL bRedrawPanel, FS_BOOL bShow);
	static FS_BOOL SetFocus(FR_MarkupPanel panel, FR_Annot pAnnot);

	//IMarkup类
	static void UpdatePropertyBox();
	static void AddToCreateList(FR_Annot pAnnot);
	static void HideHint();

	static FS_BOOL	RemoveAnnot(FR_MarkupPanel panel, FR_Annot pAnnot, FS_BOOL bRedrawPanel);
};

class CFR_CRSASStampAnnot_V13
{
public:
	static FR_CRSASStampAnnot New(FPD_Annot pAnnot, FS_LPVOID pPage);//param[pPage] Represent IReader_Page*
	static void Destroy(FR_CRSASStampAnnot stampAnnot);
	static void SetStamp(FR_CRSASStampAnnot stampAnnot, FR_CRSAStamp srcStamp);
	static void AfterMovePages(FR_CRSASStampAnnot stampAnnot, FS_INT32 nSrcRotate, FS_INT32 nDesRotate);
	static void	ResetAppearance(FR_CRSASStampAnnot stampAnnot);
	static void SetColor(FR_CRSASStampAnnot stampAnnot, FS_COLORREF color, FS_BOOL bTransparent);
	static void SetOpacity(FR_CRSASStampAnnot stampAnnot, FS_INT32 nOpacity);
	static void SetCreationDateTime(FR_CRSASStampAnnot stampAnnot, FS_ReaderDateTime dt);
	static FS_BOOL GetModifiedDateTime(FR_CRSASStampAnnot stampAnnot, FS_ReaderDateTime* dt);
	static FR_MarkupPopup GetPopup(FR_CRSASStampAnnot stampAnnot, FS_BOOL bCreate);
	static FS_FloatPoint GetHotPoint(FR_CRSASStampAnnot stampAnnot);
	static void GetType(FR_CRSASStampAnnot stampAnnot, FS_ByteString* bsType);
	static FS_BOOL IsGroup(FR_CRSASStampAnnot stampAnnot);
	static FS_BOOL IsHeader(FR_CRSASStampAnnot stampAnnot);
	static void SetModifiedDateTime(FR_CRSASStampAnnot stampAnnot, FS_ReaderDateTime  dt);
	static FS_BOOL   GetColor(FR_CRSASStampAnnot stampAnnot, FS_COLORREF* color);
	static FS_LPVOID GetReaderPage(FR_CRSASStampAnnot stampAnnot);
};

class CFR_EncryptPermisson_V13
{
public:
	static FR_EncryptPermisson CreateEncryptPermisson();
	static FS_DWORD		SetEncryptPermisson(FR_EncryptPermisson permission, const FR_ENPermission enpermisson);
	static void			GetEncryptPermisson(FR_EncryptPermisson permission, const FS_DWORD &dwPermisson, FR_ENPermission& enpermisson);
	static FS_BOOL		GetPropertyPermisson(FR_EncryptPermisson permission, const FS_DWORD& dwPermisson, const FRPROPERTITY_PERMISSON properisson);
	static FS_DWORD		GetAllPermission(FR_EncryptPermisson permission);
	static void		DeleteEncryptPermission(FR_EncryptPermisson permission);
};


class CFR_CSGCertFileManage_V13
{
public:
	static FR_CSGCertFileManage CreateCertFileManage();
	static void Destroy(FR_CSGCertFileManage filemanage);
	static void  GetCertFileStorePath(FR_CSGCertFileManage filemanage, FS_WideString* filePath);
	static FS_BOOL	NewCertData(FR_CSGCertFileManage filemanage, FS_WideString ws1, FS_WideString ws2, FS_LPVOID store);
	static FS_LPVOID		LoadStore(FR_CSGCertFileManage filemanage);
	static FS_BOOL			GetPathAndPassword(FR_CSGCertFileManage filemanage, FS_LPVOID* pCertContext, FS_WideString& cwFilePath, FS_WideString& cwPassWord);
	static FS_BOOL			UpdateFile(FR_CSGCertFileManage filemanage, FS_BOOL bReloadCertFile);
	static void	Base64ToString(FR_CSGCertFileManage filemanage, FS_WideString str, FS_WideString* outBaseStr);
	static void	StringToBase64(FR_CSGCertFileManage filemanage, FS_WideString str, FS_WideString* outBaseStr);
	static FS_BOOL			ReloadAllFile(FR_CSGCertFileManage filemanage, FS_LPVOID hStore, const FS_PtrArray& datas);
};


class CFR_CSG_CreateCert_V13
{
public:
	static FR_CSGCreateCert Create(FR_SGDataStruct pData);
	static void Destroy(FR_CSGCreateCert cert, FR_SGDataStruct pData);
	static FS_LPVOID		CreateCert(FR_CSGCreateCert cert);
	static void	GetTmpName(FR_CSGCreateCert cert, FS_ByteString& csName);
	static FS_LPVOID OpenPFXStore(FR_CSGCreateCert cert);
	static FS_LPVOID	 CreateOpenSSLCert(FR_CSGCreateCert cert);
};

#ifdef __cplusplus
};
#endif

#endif
