﻿/** @file fr_viewImpl.h.
 * 
 *  @brief FR views including DocView,PageView,NavgitionPanel .
 */

#ifndef FR_VIEWIMPL_H
#define FR_VIEWIMPL_H

#ifndef FR_VIEWEXPT_H
#include "../fr_viewExpT.h"
#endif

#ifndef FPD_DOCEXPT_H
#include "../../pdf/fpd_docExpT.h"
#endif

#ifndef FPD_OBJSEXPT_H
#include "../../pdf/fpd_objsExpT.h"
#endif

#ifndef FPD_RENDEREXPT_H
#include "../../pdf/fpd_renderExpT.h"
#endif

#ifndef FR_DOCEXPT_H
#include "../fr_docExpT.h"
#endif


#ifdef __cplusplus
extern "C"{
#endif

class CFR_DocView_V1
{
public:
	//************************************
	// Function:  GetDocument
	// Param[in]: docView			The document view whose <a>FR_Document</a>is obtained.
	// Return: The <a>FR_Document</a> associated with the document view.	
	// Remarks:	Gets the <a>FR_Document</a> associated with specifed document view..  	
	// Notes:
	// See: FRDocGetDocView
	// See: FRDocGetCurrentDocView
	// See: FRDocViewGetPageView
	// See: FRPageViewGetDocument
	//************************************
	static FR_Document			GetDocument(FR_DocView docView);

	//************************************
	// Function:  CountPageViews
	// Param[in]: docView			The document view whose page view count is obtained.
	// Return: The number of <a>FR_PageView</a> object associated with the document view.	
	// Remarks:	Gets the number of page views for the specified document view.  	
	// Notes:
	// See: FRDocViewGetPageView
	// See: FRDocViewCountVisiblePageViews
	//************************************
	static FS_INT32			CountPageViews(FR_DocView docView);


	//************************************
	// Function:  GetPageView
	// Param[in]: docView			The document view whose <a>FR_PageView</a> is obtained.
	// Param[in]: iPage			The index of page view to obtain. The index range is 0 to (<a>FRDocViewCountPageViews</a>()-1).
	// Return:	The specified <a>FR_PageView</a> of the document view.
	// Remarks: Gets the specified <a>FR_PageView</a> for specified document view. 
	// Notes:
	// See: FRDocViewCountPageViews
	// See: FRDocViewGetVisiblePageView
	// See: FRDocViewGetPageViewAtPoint
	// See: FRDocViewGetCurrentPageView
	//************************************
	static FR_PageView	GetPageView(FR_DocView docView, FS_INT32 iPage);


	

	//************************************
	// Function:  GetPageViewAtPoint
	// Param[in]: docView			The document view whose <a>FR_PageView</a> is obtained.
	// Param[in]: point			The point of screen coordinate system to obtain a page view.
	// Return: The specified <a>FR_PageView</a> of the document view if the point is in the area of the page view, otherwise 
	// <a>NULL</a>.
	// Remarks: Gets a specified <a>FR_PageView</a> with a point which is underlying to the area of page view.    
	// Notes:	
	// See: FRDocViewGetPageView
	// See: FRDocViewGetVisiblePageView
	// See: FRDocViewGetCurrentPageView
	//************************************
	static FR_PageView	GetPageViewAtPoint(FR_DocView docView, FS_DevicePoint point);

	//************************************
	// Function:  CountVisiblePageViews
	// Param[in]: docView			The document view whose page view count is obtained.
	// Return: The number of visible page views.	  
	// Remarks: Gets the number of visible page views for specified document view.   
	// Notes:	
	// See: FRDocViewCountPageViews
	//************************************
	static FS_INT32		CountVisiblePageViews(FR_DocView docView);

	//************************************
	// Function:  GetVisiblePageView
	// Param[in]: docView		The document view whose page view is obtained.
	// Param[in]: iPage			The index of the visible page. The index range is 0 to (<a>FRDocViewCountVisiblePageViews</a>()-1).
	// Return: The specified <a>FR_PageView</a>	of the document view.  
	// Remarks: Gets the specified visible page.  
	// Notes:	
	// See: FRDocViewGetPageView
	// See: FRDocViewGetPageViewAtPoint
	// See: FRDocViewGetCurrentPageView
	//************************************
	static FR_PageView	GetVisiblePageView(FR_DocView docView, FS_INT32 iPage);

	//************************************
	// Function:  GotoPageView
	// Param[in]: docView		The document view whose page view to go to.
	// Param[in]: iPage			The specified page index. The index range is 0 to (<a>FRDocViewCountPageViews</a>()-1).
	// Return: void
	// Remarks: Goes to specified page, retaining the current display mode. It invalidates the display, but not
	// always perform an immediately redraw. 
	// Notes:
	// See: FRDocViewGotoPageViewByPoint
	// See: FRDocViewGotoPageViewByRect
	//************************************
	static void			GotoPageView(FR_DocView docView, FS_INT32 iPage);

	//************************************
	// Function:  GotoPageViewByPoint
	// Param[in]: docView		The document view whose page view to go to.
	// Param[in]: iPage			The specified page index. The index range is 0 to (<a>FRDocViewCountPageViews</a>()-1).
	// Param[in]: left			The x-coordinate to scroll to. Specified in PDF space coordinates.
	// Param[in]: top			The y-coordinate to scroll to. Specified in PDF space coordinates.
	// Return: void
	// Remarks: Goes to specified page and scroll page view to the location specified by <param>left</param> and <param>top</param>, retaining the 
	// current display mode. It invalidates the display, but not always perform an immediately redraw. 
	// Notes:	
	// See: FRDocViewGotoPageView
	// See: FRDocViewGotoPageViewByRect
	//************************************
	static void			GotoPageViewByPoint(FR_DocView docView, FS_INT32 iPage, FS_FLOAT left, FS_FLOAT top);

	
	//************************************
	// Function: GotoPageViewByRect
	// Param[in]: docView		The document view whose page view to go to.
	// Param[in]: iPage			The index of the visible page. The index range is 0 to (<a>FRDocViewCountVisiblePageViews</a>()-1).
	// Param[in]: rect			The rectangle that is completely visible.
	// Return: void 
	// Remarks: Goes to specified page view and scroll to center of the rectangle. It always change the zoom level if necessary.     
	// Notes:	
	// See: FRDocViewGotoPageView
	// See: FRDocViewGotoPageViewByPoint
	//************************************
	static void			GotoPageViewByRect(FR_DocView docView, FS_INT32 iPage, FS_FloatRect rect);


	//************************************
	// Function:  GetCurrentPageView
	// Param[in]: docView			The document.
	// Return:	Current <a>FR_PageView</a> that is visible in screen.
	// Remarks:	Gets current page view that is visible in screen. 
	// Notes: If more than one page may be visible. use FRDocViewCountVisiblePageViews() and FRDocViewGetVisiblePageView() instead.
	// See: FRDocViewGetPageView
	// See: FRDocViewGetPageViewAtPoint
	// See: FRDocViewGetGetVisiblePageView
	//************************************
	static FR_PageView		GetCurrentPageView(FR_DocView docView);

	//************************************
	// Function:  GetRotation
	// Param[in]: docView			The document view to obtain rotation factor.
	// Return: A integer described in group <a>FR_RotationFlags</a>.	  
	// Remarks: Gets the rotation factor.   
	// Notes:
	// See: 
	//************************************
	static FS_INT32			GetRotation(FR_DocView docView);

	//************************************
	// Function:  SetRotation
	// Param[in]: docView			The document view to obtain rotation factor.
	// Param[in]: nFlag				A integer described in group <a>FR_RotationFlags</a>.
	// Return: void  
	// Remarks: Sets the rotation factor.   
	// Notes:
	// See: 
	//************************************
	static void			SetRotation(FR_DocView docView, FS_INT32 nFlag);

	//************************************
	// Function:  GetLayoutMode
	// Param[in]: docView			The document view whose layout mode is obtained.		
	// Return: 	The current page layout mode, a <a>FRDOC_LAYOUTMODE</a> enumeration value.
	// Remarks:	Gets the page layout mode.  	
	// Notes:
	//************************************
	static FRDOCVIEW_LAYOUTMODE	GetLayoutMode(FR_DocView docView);

	//************************************
	// Function:  SetLayoutMode
	// Param[in]: docView			The document view whose layout mode is set.	
	// Param[in]: mode				The new layout mode for document view.
	// Return: 	void
	// Remarks:	Sets the page layout mode for a document view.  	
	// Notes:
	//************************************
	static void SetLayoutMode(FR_DocView docView, FRDOCVIEW_LAYOUTMODE mode);

	//************************************
	// Function:  GetZoom
	// Param[in]: docView			The document view.
	// Return:	The current zoom for page view.
	// Remarks:	Gets the current zoom for page view. The zoom factor is point-to-point, not point-to-pixel.	
	// Notes: Current zoom, as a fixed number measured in units in which 1.0 is 100% zoom.
	// See: FRDocViewGetZoomType
	// See: FRDocViewZoomTo
	//************************************
	static FS_FLOAT			GetZoom(FR_DocView docView);

	//************************************
	// Function:  GetZoomType
	// Param[in]: docView			The document view.
	// Return: Current zoom type.	
	// Remarks: Gets the current zoom type.	  	
	// Notes:
	// See: FRDocViewGetZoom
	// See: FRDocViewZoomTo
	//************************************
	static FRDOCVIEW_ZOOMTYPE			GetZoomType(FR_DocView docView);

	//************************************
	// Function: ZoomTo 
	// Param[in]: docView		The document view.
	// Param[in]: mode			The zoom mode to set.
	// Param[in]: scale			The zoom factor, specified as a magnification factor(for example,
	// 1.0 displays the document at actual size). This is ignore unless iFittype parameter is
	// <a>FR_ZOOM_MODE_ACTUAL_SCALE</a> or <a>FR_ZOOM_MODE_NONE</a>.
	// Return:	void  
	// Remarks: Sets the zoom factor and zoom type to specified page view.
	// Notes:	
	// See: FRDocViewGetZoom
	// See: FRDocViewGetZoomType
	//************************************
	static void					ZoomTo(FR_DocView docView, FRDOCVIEW_ZOOMTYPE mode, FS_FLOAT scale);

	//************************************
	// Function:  GoBack
	// Param[in]: docView			The document view.
	// Return:	void
	// Remarks: Goes to the previous view on the view stack, if a previous view exist.	  	
	// Notes:
	// See: FRDocViewGoForward
	//************************************
	static void				GoBack(FR_DocView docView);

	//************************************
	// Function:  GoForward
	// Param[in]: docView			The document view.
	// Return:	void
	// Remarks:	Goes to the next view on the view stack, if a next view exist.  	
	// Notes:
	// See: FRDocViewGoBack
	//************************************
	static void				GoForward(FR_DocView docView);


	//************************************
	// Function:  ShowAnnots
	// Param[in]: docView	The document view.
	// Param[in]: annots	The array of annotations to be shown.
	// Param[in]: bShow		Whether to show or not.
	// Return:	void
	// Remarks:	 If <param>bShow</param> is set <a>TRUE</a>, the annotations will be shown. Otherwise not. This interface is not available in version 1.0.
	// Notes:
	//************************************
	static void				ShowAnnots(FR_DocView docView, FS_PtrArray* annots, FS_BOOL bShow);

	//************************************
	// Function:  ProcAction
	// Param[in]: docView			The document view.
	// Param[in]: action		A <a>FPD_Action</a> object to be performed.
	// Return:	void
	// Remarks:	Performs a specified action.  	
	// Notes:
	//************************************
	static void				ProcAction(FR_DocView docView, FPD_Action action);

	//************************************
	// Function:  ScrollTo
	// Param[in]: docView			The document view to scroll.
	// Param[in]: x					The x-coordinate to scroll to, specified in device space coordinates.
	// Param[in]: y					The y-coordinate to scroll to, specified in device space coordinates.
	// Return:	void
	// Remarks:	Scrolls the specified document view to location specified by <param>x</param> and <param>y</param>.  	
	// Notes:
	//************************************
	static void				ScrollTo(FR_DocView docView, FS_INT32 x, FS_INT32 y);

	//************************************
	// Function:  GetMaxScrollingSize
	// Param[in]: docView			The input document view.
	// Param[out]: outWidth			It receives the horizontal maximum scrolling size.
	// Param[out]: outHeight		It receives the vertical maximum scrolling size.
	// Return:	void
	// Remarks:	Gets the maximum scrolling size of the document view.  	
	// Notes:
	//************************************
	static void				GetMaxScrollingSize(FR_DocView docView, FS_INT32* outWidth, FS_INT32* outHeight);

	//************************************
	// Function:  DrawNow
	// Param[in]: docView			The document view to redraw.
	// Return: void  
	// Remarks: Forces specified document view to redraw.   
	// Notes:	
	// See: FRDocViewInvalidateRect
	//************************************
	static void			DrawNow(FR_DocView docView);

	//************************************
	// Function:  InvalidateRect
	// Param[in]: docView		The document view in which a region is invalidated.
	// Param[in]: rect			The rectangle to invalidate, specified in device space coordinates.
	// Return:	void  
	// Remarks: Redraws the specified area of document view immediately.  
	// Notes:	
	// See: FRDocViewDrawNow
	//************************************
	static void			InvalidateRect(FR_DocView docView, FS_Rect rect);


	//************************************
	// Function: DrawRect 
	// Param[in]: docView		The input doc view.
	// Param[in]: rect			The input rectangle.
	// Param[in]: fill_argb		The color to fill.
	// Param[in]: bCompose		Whether to compose or not.
	// Return: void  
	// Remarks:   Draws rectangle in the doc view.
	// Notes:
	// See: FRDocViewDrawLine
	// See: FRDocViewDrawRectOutline
	// See: FRDocViewDrawPolygon
	// See: FRDocViewDrawPolygonOutline
	//************************************
	static void			DrawRect(FR_DocView docView, FS_Rect rect, FS_ARGB fill_argb, FS_BOOL bCompose);


	//************************************
	// Function:  DoPopUpMenu
	// Param[in]: docView		The document view in which the menu appears.
	// Param[in]: menu			The displayed menu.
	// Param[in]: xHit			The x-coordinate of the upper left corner of the menu.	
	// Param[in]: yHit			The y-coordinate of the upper left corner of the menu.
	// Return: The menu item clicked by user.	  
	// Remarks: Displays the given menu as a pop-up menu anchored at <param>xHi</param>t and <param>yHit</param>, which are in device
	// space coordinates relative to <param>docView</param>.
	// Notes:	
	// See: FRMenuTrackPopup
	//************************************
	static FR_MenuItem	DoPopUpMenu(FR_DocView docView, FR_Menu menu, FS_INT32 xHit, FS_INT32 yHit);

	//************************************
	// Function:  GetMachinePort
	// Param[in]: docView			The document view whose <Italic>HWND</Italic> handler and device context are obtained.
	// Return: The <a>FR_WinPort</a> object.
	// Remarks: Gets the platform-specific object needed to draw into page view using a platform's native 
	// graphic calls. In Win32 OS, it is a <a>FR_WinPort</a> object which contain the <Italic>HWND</Italic> handler and 
	// the device context. When done, release it using <a>FRDocViewReleaseMachinePort</a>().
	// Notes:	
	// See: FRDocViewReleaseMachinePort
	//************************************
	static FR_WinPort		GetMachinePort(FR_DocView docView);

	//************************************
	// Function:  ReleaseMachinePort
	// Param[in]: docView	The document view whose <Italic>HWND</Italic> handler and device context are released.
	// Param[in]: port		The platform-dependent object to release.
	// Return:	<a>TRUE</a> means successful, otherwise not.
	// Remarks: Releases a <a>FR_WinPort</a> that acquired form page view using <a>FRDocViewGetMachinePort</a>().
	// Notes:
	// See: FRDocViewGetMachinePort
	//************************************
	static FS_BOOL			ReleaseMachinePort(FR_DocView docView, FR_WinPort port);


	//************************************
	// Function:  GetOCContext
	// Param[in]: docView		The document view whose ocg context are obtained.
	// Return:	The <Italic>OCG</Italic> context of Reader doc view.  
	// Remarks: Gets the <Italic>OCG</Italic> context of Reader doc view. 
	// Notes:
	//************************************
	static FPD_OCContext		GetOCContext(FR_DocView docView);

	//************************************
	// Function:  GetCurrentSnapshot
	// Param[in]: docView		The document view whose snapshot image are obtained.
	// Return: A <a>FS_DIBitmap</a> object indicates current snapshot image. This <a>FS_DIBitmap</a> object must be destroied by calling
	// <a>FSDIBitmapDestroy</a>().
	// Remarks: Gets current snapshot image that generated by using snapshot tool.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_DIBitmap			GetCurrentSnapshot(FR_DocView docView);
	
	//************************************
	// Function:  GetThumbnailView
	// Param[in]: docView		The input document view.
	// Return: A <a>FR_ThumbnailView</a> object related to the input document view.
	// Remarks: Gets the thumbnail view related to the input document view.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FR_ThumbnailView		 GetThumbnailView(FR_DocView docView);

	//************************************
	// Function:  GotoPageViewByState
	// Param[in]: docView			The document view whose page view to go to.
	// Param[in]: iPage				The specified page index. The index range is 0 to (<a>FRDocViewCountPageViews</a>()-1).
	// Param[in]: iFitType			The input fit type.
	// Param[in]: destArray			The input destination array.
	// Param[in]: destArrayCount	The count of the destination array.
	// Return: void
	// Remarks: Goes to the page view by state.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: FRDocViewGotoPageViewByPoint
	// See: FRDocViewGotoPageViewByRect
	//************************************
	static void	GotoPageViewByState(FR_DocView docView, FS_INT32 iPage, FS_INT32 iFitType, FS_FLOAT* destArray,  FS_INT32 destArrayCount);

	//************************************
	// Function:  MovePage
	// Param[in]: docView	The document view.
	// Param[in]: ptDest	The dest point.
	// Param[in]: ptSrc		The source point.
	// Return: void
	// Remarks: Moves the page.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1.0.0
	//************************************
	static void MovePage(FR_DocView docView, FS_DevicePoint ptDest, FS_DevicePoint ptSrc);

	//************************************
	// Function:  IsValidPageView
	// Param[in]: docView	The document view.
	// Param[in]: pPageView	The page view.
	// Return: TRUE if the page view is valid, otherwise FALSE.
	// Remarks: Checks whether the page view is valid or not.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.1.0.0
	//************************************
	static FS_BOOL IsValidPageView(FR_DocView docView, FR_PageView pPageView);

	//************************************
	// Function:  GetTagDocViewText
	// Param[in]: docView	The document view.
	// Param[out]:outText   It receives the text got.
	// Param[in]: nCount	The count of page text to get, -1 for the end of the page.
	// Return:    void
	// Remarks:   Gets the text of a page in Tag Order.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3.2
	//************************************
	static void	GetTagDocViewText(FR_DocView docView, FS_WideString* outText, FS_INT32 nCount);

	//************************************
	// Function:  GetLRDocViewText
	// Param[in]: docView	The document view.
	// Param[out]:outText   It receives the text got.
	// Param[in]: nCount	The count of page text to get, -1 for the end of the page.
	// Return:    void
	// Remarks:   Gets the text of a page in LR Order.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3.2
	//************************************
	static void	GetLRDocViewText(FR_DocView docView, FS_WideString* outText, FS_INT32 nCount);

	static HWND GetHwnd(FR_DocView docView);
};

class CFR_PageView_V1
{
public:

	//************************************
	// Function:  GetDocument
	// Param[in]: pv			The page view whose <a>FR_Document</a> is obtained.
	// Return: The <a>FR_Document</a> for <param>pv</param>.	
	// Remarks:	Gets the <a>FR_Document</a> for the document currently displayed in specified pageview.   	
	// Notes:
	// See: FRDocViewGetDocument
	// See: FRPageViewGetDocView
	//************************************
	static FR_Document		GetDocument(FR_PageView pv);

	//************************************
	// Function:  GetDocView
	// Param[in]: pv			The page view whose <a>FR_DocView</a> is obtained.
	// Return: The <a>FR_DocView</a> for <param>pv</param>.
	// Remarks:	Gets the <a>FR_DocView</a> for specified pageview.   	
	// Notes:
	// See: FRDocGetDocView
	// See: FRDocGetCurrentDocView
	// See: FRDocViewGetPageView
	//************************************
	static FR_DocView		GetDocView(FR_PageView pv);

	//************************************
	// Function:  GetPDPage
	// Param[in]: pv			The page view whose <a>FPD_Page</a> is obtained.
	// Return: <a>FPD_Page</a> displayed in <param>pv</param>,	or <a>NULL</a> if no valid <a>FPD_Page</a>
	// associated with <param>pv</param>.
	// Remarks:	Gets a <a>FPD_Page</a> currently displayed in the specified page view. This do not new a
	// <a>FPD_Page</a> object. Do not use this result across methods that might change the current page.
	// To obtain a value that can be used across such calls, use <a>FPD_PageNew</a> instead.
	// Notes:
	// See: FRDocGetPDDoc
	//************************************
	static FPD_Page			GetPDPage(FR_PageView pv);


	//************************************
	// Function:  GetPageIndex
	// Param[in]: pv			The page view whose current page number is obtained.
	// Return: The current page number, or <a>NULL</a> if <param>pv</param> is invalid. The first page number of a document
	// is page 0.
	// Remarks:	Gets the current page number.  	
	// Notes:
	//************************************
	static FS_INT32			GetPageIndex(FR_PageView pv);


	//************************************
	// Function:  CountAnnot
	// Param[in]: pv			The page view whose annotation count is obtained.
	// Return: The number of annotations associated with <param>pv</param>.	
	// Remarks: Gets the number of annotations associated with specified page view. This interface is not available in version 1.0.
	// Notes:
	//************************************
	static FS_INT32			CountAnnot(FR_PageView pv);

	

	//************************************
	// Function:  GetCurrentMatrix
	// Param[in]: pv			The page view whose matrix is obtained.
	// Return: Current matrix of <param>pv</param>.
	// Remarks: Gets the current matrix of page view.
	// Notes:	
	//************************************
	static FS_AffineMatrix		GetCurrentMatrix(FR_PageView pv);

	//************************************
	// Function:  DevicePointToPage
	// Param[in]: pv				The page view for which the point's coordinates are transformed from device space to user space.
	// Param[in]: window_x			The x-coordinate of the point to transform, specified in device space coordinates.		
	// Param[in]: window_y			The y-coordinate of the point to transform, specified in device space coordinates.
	// Param[out]: outPt			(Filled by this method) A pointer to a point whose user space coordinates corresponding
	// <param>window_x</param> and <param>window_y</param>.
	// Return:	 void 
	// Remarks: Transforms a point's coordinate from device space to user space.
	// Notes:	
	// See: FRPageViewDeviceRectToPage
	// See: FRPageViewPointToDevice
	// See: FRPageViewRectToDevice
	//************************************
	static void					DevicePointToPage(FR_PageView pv, FS_INT32 window_x, FS_INT32 window_y, FS_FloatPoint* outPt);

	//************************************
	// Function:  DeviceRectToPage
	// Param[in]: pv				The page view for which the rectangle is transformed from device space to user space.
	// Param[in]: rect				A pointer to device space rectangle whose coordinates are transformed to user space.
	// Param[out]: outRect			(Filled by this method) A pointer to a user space rectangle corresponding <param>rect</param>.
	// Return:	 void 
	// Remarks: Transforms a rectangle from device space to user space.
	// Notes:
	// See: DevicePointToPage
	// See: FRPageViewPointToDevice
	// See: FRPageViewRectToDevice
	//************************************
	static void					DeviceRectToPage(FR_PageView pv, const FS_Rect* rect, FS_FloatRect* outRect);

	//************************************
	// Function:  PointToDevice
	// Param[in]: pv				The page view for which the point's coordinates are transformed from user space to device space.
	// Param[in]: pt				A pointer to a <a>FS_FloatPoint</a> whose coordinates, specified in device space coordinates, are transformed.
	// Param[out]: out_window_x		(Filled by the method) The x-coordinate of the device space point corresponding to <param>pt</param>.
	// Param[out]: out_window_y		(Filled by the method) The y-coordinate of the device space point corresponding to <param>pt</param>. 
	// Return: void	   
	// Remarks:  Transforms a point from user space to device space.
	// Notes:	
	// See: DevicePointToPage
	// See: DeviceRectToPage
	// See: FRPageViewRectToDevice
	//************************************
	static void					PointToDevice(FR_PageView pv, const FS_FloatPoint* pt, FS_INT32* out_window_x, FS_INT32* out_window_y);

	//************************************
	// Function:  RectToDevice
	// Param[in]: pv				The page view for which the rectangle is transformed from user space to device space.
	// Param[in]: rect				A pointer to user space rectangle whose coordinates are transformed to device space.
	// Param[out]: outRect			(Filled by this method) A pointer to a device space rectangle corresponding <param>rect</param>.
	// Return:	 void 
	// Remarks: Transforms a rectangle from user space to device space.
	// Notes:	
	// See: DevicePointToPage
	// See: DeviceRectToPage
	// See: FRPageViewPointToDevice
	//************************************
	static void					RectToDevice(FR_PageView pv, const FS_FloatRect* rect, FS_Rect* outRect);


	//************************************
	// Function:  IsVisible
	// Param[in]: pv			The page view to adjust whether it is visible.
	// Return: <a>TRUE</a> if <param>pv</param> is visible, otherwise <a>FALSE</a>.
	// Remarks: Gets a flag that indicate whether a page view is visible.
	// Notes:	
	//************************************
	static FS_BOOL				IsVisible(FR_PageView pv);

	//************************************
	// Function:  GetPageRect
	// Param[in]: pv			The page view whose bounding-box to obtained.
	// Return: The current bounding-box for <param>pv</param>.
	// Remarks: Gets the current bounding-box of specified page view.
	// Notes: The page bounding-box specified in device space coordinates.
	// See: FRPageViewGetPageVisibleRect
	//************************************
    static FS_Rect				GetPageRect(FR_PageView pv);


	//************************************
	// Function:  GetPageVisibleRect
	// Param[in]: pv			The page view whose visible rectangle to obtained.
	// Return: A rectangle specified in user space, instead of the current visible region of <param>pv</param>.	  
	// Remarks: Gets the current visible region of specified page view.  
	// Notes: If the specified page view is invisible, the returned rectangle is empty.	
	// See: FRPageViewGetPageRect
	//************************************
	static FS_FloatRect			GetPageVisibleRect(FR_PageView pv);


	//************************************
	// Function:  DidContentChanged
	// Param[in]: pv			The page view whose <a>FPD_Page</a> has been modified.
	// Param[in]: bReLoadPage	A flag to force reader to reload the page data. If <a>TRUE</a>, Foxit Reader will reload the page data and reparse the page.
	// The flag must be <a>TRUE</a> and call <a>FPDPageGenerateContent</a>() befor calling this method if the <a>FPD_Page</a> whose content is modified is not using <a>FRPageViewGetPDPage</a>() to obtain.
	// Return: void	  
	// Remarks: The content of <a>FPD_Page</a> of the <param>pv</param> has been modified. Foxit Reader will broadcast some notifications to plug-ins. 
	// Notes: 	
	// See: 
	//************************************
	static void		DidContentChanged(FR_PageView pv, FS_BOOL bReLoadPage);

	//************************************
	// Function:  GetHWnd
	// Param[in]: pv			The page view which HWND will be obtained.
	// Return: The HWND handler of <param>pv</param>.
	// Remarks: Gets the HWND handler in which a page view is displaying.
	// Notes: 	
	// See: 
	//************************************
	static HWND		GetHWnd(FR_PageView pv);

	//************************************
	// Function:  DidTextObjectChanged
	// Param[in]: pv			The page view whose text objects of <a>FPD_Page</a> has been modified.
	// Return: void	  
	// Remarks: The text objects of <a>FPD_Page</a> of the <param>pv</param> has been modified. Foxit Reader will broadcast some notifications to plug-ins. 
	// Notes: 	
	// See: 
	//************************************
	static void		DidTextObjectChanged(FR_PageView pv);

	//************************************
	// Function:  GetAnnotByIndex
	// Param[in]: pv	The input page view object.
	// Param[in]: index The specified index of the annotations.
	// Return: The specified annotation.
	// Remarks: Gets the annotation by index.
	// Notes: 	
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	// See: 
	//************************************
	static FR_Annot GetAnnotByIndex(FR_PageView pv, FS_INT32 index);

	//************************************
	// Function:  GetPageState
	// Param[in]: pv					The input page view object.
	// Param[out]: outFitType			It receives the fit type.
	// Param[out]: outDestArray			It receives the destination array.
	// Param[out]: outDestArrayCount	It receives the count of the destination array.
	// Return: void
	// Remarks: Gets the page state. Sets <param>outDestArray</param> NULL to get the count of the destination array first.
	// Notes: 	
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See: 
	//************************************
	static void GetPageState(FR_PageView pv, FS_INT32* outFitType, FS_FLOAT* outDestArray,  FS_INT32* outDestArrayCount);

	//************************************
	// Function:  DeleteAnnot
	// Param[in]: pv		The input page view object.
	// Param[in]: frAnnot	The input annotation to be deleted.	
	// Return: TRUE for success, otherwise failure.
	// Remarks: Deletes the specified annotation.
	// Notes: 	
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	// See: 
	//************************************
	static FS_BOOL DeleteAnnot(FR_PageView pv, FR_Annot frAnnot);

	//************************************
	// Function:  GetFocusAnnot
	// Param[in]: pv		The input page view object.
	// Return: The focus annotation.
	// Remarks: Gets the focus annotation.
	// Notes: 	
	// Since: <a>SDK_LATEEST_VERSION</a> > 3.0.0.0
	// See: 
	//************************************
	static FR_Annot GetFocusAnnot(FR_PageView pv);

	//************************************
	// Function:  GetPageScale
	// Param[in]: pv		The input page view object.
	// Return: The page scale.
	// Remarks: Gets the page scale.
	// Notes: 	
	// Since: <a>SDK_LATEEST_VERSION</a> > 3.0.0.0
	// See: 
	//************************************
	static FS_FLOAT GetPageScale(FR_PageView pv);

	//************************************
	// Function:  GetAnnotAtPoint
	// Param[in]: pv			The input page view object.
	// Param[in]: point			The input point where to get the annotation.
	// Param[in]: pszSubType	Specifies the sub type of the annotation you want to get. You can set it NULL as default.
	// Return: The specified annotation.
	// Remarks: Gets the annotation at the specified point.
	// Notes: 	
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3
	// See: 
	//************************************
	static FR_Annot GetAnnotAtPoint(FR_PageView pv, FS_DevicePoint point, FS_LPCSTR pszSubType);

	//************************************
	// Function:  UpdateAllViews
	// Param[in]: pv			The input page view object.
	// Param[in]: frAnnot		The input annotation you want to update.
	// Return: void.
	// Remarks: Updates all the page views where the annotation is shown.
	// Notes: 	
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3
	// See: 
	//************************************
	static void UpdateAllViews(FR_PageView pv, FR_Annot frAnnot);

	//************************************
	// Function:  AddAnnot
	// Param[in]: pv			The input page view object.
	// Param[in]: annotDict		The input annotation dictionary you want to add.
	// Param[in]: nIndex		The index where you want to add the annotation. Sets it -1 as default.
	// Return: The UI layer annotation object.
	// Remarks: Adds the annotation to the page.
	// Notes: 	
	// Since: <a>SDK_LATEEST_VERSION</a> > 7.3.1
	// See: 
	//************************************
	static FR_Annot AddAnnot(FR_PageView pv, FPD_Object annotDict, FS_INT32 nIndex);

	//************************************
	// Function:  GetRenderOptions
	// Param[in]: pv			The input page view object.
	// Return: The rendering options.
	// Remarks: Gets the rendering options.
	// Notes: 	
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.2.1
	// See: 
	//************************************
	static FPD_RenderOptions GetRenderOptions(FR_PageView pv);

	//************************************
	// Function:  DidContentChanged2
	// Param[in]: pv			The page view whose <a>FPD_Page</a> has been modified.
	// Param[in]: bReLoadPage	A flag to force reader to reload the page data. If <a>TRUE</a>, Foxit Reader will reload the page data and reparse the page.
	// The flag must be <a>TRUE</a> and call <a>FPDPageGenerateContent</a>() befor calling this method if the <a>FPD_Page</a> whose content is modified is not using <a>FRPageViewGetPDPage</a>() to obtain.
	// Param[in]: bResizePageNotify Checks whether to broadcast the notification of page resizing.
	// Param[in]: pChangeData	It indicates what data is changed.
	// Return: void	  
	// Remarks: The content of <a>FPD_Page</a> of the <param>pv</param> has been modified. Foxit Reader will broadcast some notifications to plug-ins. 
	// Notes: 	
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.3.2
	// See: 
	//************************************
	static void DidContentChanged2(FR_PageView pv, FS_BOOL bReLoadPage, FS_BOOL bResizePageNotify, void* pChangeData);

	//************************************
	// Function:  DidContentChanged3
	// Param[in]: pv			The page view whose <a>FPD_Page</a> has been modified.
	// Param[in]: bReLoadPage	A flag to force reader to reload the page data. If <a>TRUE</a>, Foxit Reader will reload the page data and reparse the page.
	// The flag must be <a>TRUE</a> and call <a>FPDPageGenerateContent</a>() befor calling this method if the <a>FPD_Page</a> whose content is modified is not using <a>FRPageViewGetPDPage</a>() to obtain.
	// Param[in]: bResizePageNotify Checks whether to broadcast the notification of page resizing.
	// Param[in]: objArray	It indicates what data is changed. The value type in the array is <a>FR_ChangedContent</a>.
	// Param[in]: changeType The content change type.
	// Return: void	  
	// Remarks: The content of <a>FPD_Page</a> of the <param>pv</param> has been modified. Foxit Reader will broadcast some notifications to plug-ins. 
	// Notes: 	
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.1
	// See: 
	//************************************
	static void DidContentChanged3(FR_PageView pv, FS_BOOL bReLoadPage, FS_BOOL bResizePageNotify, FS_PtrArray objArray,  FR_ContentChangeType changeType);

	//************************************
	// Function:  SetContentModification
	// Param[in]: pv				The page view whose <a>FPD_Page</a> content is modified.
	// Param[in]: modifiedContent	An array to store the modified <a>FPD_Page</a> or <a>FPD_Form</a>.
	// Return:	void.
	// Remarks: Sets the modified page content.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7.1	
	//************************************
	static void SetContentModification(FR_PageView pv, FS_PtrArray modifiedContent);

	//************************************
	// Function:  GenerateContent
	// Param[in]: pv	The page view whose <a>FPD_Page</a> needs to generate content.
	// Return:	void.
	// Remarks: Generates the page content.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7.1	
	//************************************
	static void	GenerateContent(FR_PageView pv);

	static void UpdateAllViewsRect(FR_PageView pv, FS_FloatRectArray rectarray);
};

class CFR_TextSelectTool_V1
{
public:
	//************************************
	// Function:  Create
	// Param[in]: doc	The input doc where the operation of text selecting occurs.
	// Return: The text select tool.	  
	// Remarks: Creates the text select tool which is used to process the operation of text selecting in the doc view.
	// Notes: 	
	// See: 
	//************************************
	static FR_TextSelectTool Create(FR_Document doc);

	//************************************
	// Function:  DestroyTextSelectTool
	// Param[in]: textSelectTool The input text select tool.
	// Return: 
	// Remarks: Destroys the text select tool created from <a>FRTextSelectToolCreate</a>.
	// Notes: 	
	// See: 
	//************************************
	static void DestroyTextSelectTool(FR_TextSelectTool textSelectTool);
	
	//************************************
	// Function:  GetSelectedText
	// Param[in]: textSelectTool	The input text select tool.
	// Param[out]: outText			It receives the text selected.
	// Return:	  <a>TRUE</a> means successful, otherwise not.
	// Remarks: Gets the selected text.
	// Notes: 	
	// See: 
	//************************************
	static FS_BOOL GetSelectedText(FR_TextSelectTool textSelectTool, FS_WideString* outText);

	//************************************
	// Function:  DrawSelection
	// Param[in]: textSelectTool	The input text select tool.
	// Param[in]: docView			The input doc view where do the operation of text selecting.
	// Param[in]: winPort			The platform-depend things.
	// Return:
	// Remarks:	 Draws the area where the text is selected in black color.
	// Notes: 	
	// See: 
	//************************************
	static void DrawSelection(FR_TextSelectTool textSelectTool, FR_DocView docView, FR_WinPort winPort);

	//************************************
	// Function:  ClearSelection
	// Param[in]: textSelectTool	The input text select tool.
	// Return: 
	// Remarks:   Clears the area where the text is selected.
	// Notes: 	
	// See: 
	//************************************
	static void ClearSelection(FR_TextSelectTool textSelectTool);

	//************************************
	// Function:  DoLButtonUp
	// Param[in]: textSelectTool	The input text select tool.
	// Param[in]: pageView			The input page view where do the operation of text selecting.
	// Param[in]: point				The input point where do the mouse left button up operation.
	// Return:
	// Remarks:   Does the mouse left button up operation.
	// Notes: 	
	// See: 
	//************************************
	static void	DoLButtonUp(FR_TextSelectTool textSelectTool, FR_PageView pageView, FS_DevicePoint point);

	//************************************
	// Function:  DoLButtonDown
	// Param[in]: textSelectTool	The input text select tool.
	// Param[in]: pageView			The input page view where do the operation of text selecting.
	// Param[in]: point				The input point where do the mouse operation.
	// Return:
	// Remarks:	  Does the mouse left button down operation.
	// Notes: 	
	// See: 
	//************************************
	static void	DoLButtonDown(FR_TextSelectTool textSelectTool, FR_PageView pageView, FS_DevicePoint point);

	//************************************
	// Function:  DoLButtonDblClk
	// Param[in]: textSelectTool	The input text select tool.
	// Param[in]: pageView			The input page view where do the operation of text selecting.
	// Param[in]: point				The input point where do the mouse operation.
	// Return:	
	// Remarks:   Does the mouse left button double-click operation.
	// Notes: 	
	// See: 
	//************************************
    static void	DoLButtonDblClk(FR_TextSelectTool textSelectTool, FR_PageView pageView, FS_DevicePoint point);

	//************************************
	// Function:  DoRButtonUp
	// Param[in]: textSelectTool	The input text select tool.
	// Param[in]: pageView			The input page view where do the operation of text selecting.
	// Param[in]: point				The input point where do the mouse operation.
	// Return:
	// Remarks:   Does the mouse right button up operation.
	// Notes: 	
	// See: 
	//************************************
	static void	DoRButtonUp(FR_TextSelectTool textSelectTool, FR_PageView pageView, FS_DevicePoint point);

	//************************************
	// Function:  DoMouseMove
	// Param[in]: textSelectTool	The input text select tool.
	// Param[in]: pageView			The input page view where do the operation of text selecting.
	// Param[in]: point				The input point where do the mouse operation.
	// Return:
	// Remarks:   Does the mouse move operation.
	// Notes: 	
	// See: 
	//************************************
	static void	DoMouseMove(FR_TextSelectTool textSelectTool, FR_PageView pageView, FS_DevicePoint point);

	//************************************
	// Function:  DoMouseWheel
	// Param[in]: textSelectTool	The input text select tool.
	// Param[in]: pageView			The input page view where do the operation of text selecting.
	// Param[in]: point				The input point where do the mouse operation.
	// Return: <a>TRUE</a> for success, otherwise not.
	// Remarks:   Does the mouse wheel operation.
	// Notes: 	
	// See: 
	//************************************
    static FS_BOOL DoMouseWheel(FR_TextSelectTool textSelectTool, FR_PageView pageView, FS_DevicePoint point);

	//************************************
	// Function:  IsSelecting
	// Param[in]: textSelectTool	The input text select tool.
	// Return:	  <a>TRUE</a> means the mouse is selecting text.
	// Remarks:   Checks whether the mouse is selecting text.
	// Notes: 	
	// See: 
	//************************************
    static FS_BOOL IsSelecting(FR_TextSelectTool textSelectTool);

	//************************************
	// Function:  EnumTextObjectRect
	// Param[in]: textSelectTool	The input text select tool.
	// Param[in]: pageIndex			The input page index.
	// Param[out]: outRectArray		It receives the rectangle array of text object.
	// Param[out]: nCount			It receives the count of the rectangle array.
	// Return:  <a>TRUE</a> means successful, otherwise not.
	// Remarks: Enumerates the text object rectangles. Invokes this interface first to get the count of the rectangles by setting 
	// <param>outRectArray</param> as <a>NULL</a>. Then allocates the buffer for the rectangle array.
	// Notes: 	
	// See: 
	//************************************
	static FS_BOOL EnumTextObjectRect(FR_TextSelectTool textSelectTool, FS_INT32 pageIndex, FS_FloatRect** outRectArray, FS_INT32* nCount);

	static void Select_GetPageRange(FR_TextSelectTool textSelectTool, FS_INT32 &nStartPage, FS_INT32 &nEndPage);

	static void AddSelect(FR_TextSelectTool textSelectTool, FR_PageView pPageView, FS_INT32 nStartIndex, FS_INT32 nEndIndex, FS_BOOL bShowLayer, FS_BOOL bIndexSearch);
};

class CFR_ThumbnailView_V3
{
public:

	//************************************
	// Function:  CountPage
	// Param[in]: tv			The input thumbnail view.
	// Return: The count of page displayed in the thumbnail view.	
	// Remarks:	Gets the count of page displayed in the thumbnail view.   	
	// Notes:
	// See: FRDocViewGetThumbnailView
	//************************************
	static FS_INT32 CountPage(FR_ThumbnailView tv);

	//************************************
	// Function:  CountVisiblePage
	// Param[in]: tv			The input thumbnail view.
	// Return: The count of visible page displayed in the thumbnail view.	
	// Remarks:	Gets the count of visible page displayed in the thumbnail view.   	
	// Notes:
	// See: FRDocViewGetThumbnailView
	//************************************
	static FS_INT32 CountVisiblePage(FR_ThumbnailView tv);

	//************************************
	// Function:  GetPageRect
	// Param[in]: tv			The input thumbnail view.
	// Param[in]: nPage			The index of visible pages.
	// Return: The rectangle of specified visible page.	
	// Remarks:	Gets the rectangle of specified visible page.
	// Notes:
	// See: FRDocViewGetThumbnailView
	//************************************
	static FS_Rect GetPageRect(FR_ThumbnailView tv, FS_INT32 nPage);

	//************************************
	// Function:  GetVisiblePageRange
	// Param[in]: tv			The input thumbnail view.
	// Param[out]: nFrom		It receives the starting index of visible pages.
	// Param[out]: nTo			It receives the ending index of visible pages.
	// Return: void
	// Remarks:	Gets the index of visible page range.
	// Notes:
	// See: FRDocViewGetThumbnailView
	//************************************
	static void GetVisiblePageRange(FR_ThumbnailView tv, FS_INT32* nFrom, FS_INT32* nTo);

	//************************************
	// Function:  GetPDPage
	// Param[in]: tv		The input thumbnail view.
	// Param[in]: nPage		The index of page.
	// Return: The <a>FPD_Page</a> object.
	// Remarks:	Gets a <a>FPD_Page</a> object.
	// Notes:
	// See: FRDocViewGetThumbnailView
	//************************************
	static FPD_Page GetPDPage(FR_ThumbnailView tv, FS_INT32 nPage);
};

class CFR_Annot_V5
{
public:
	//************************************
	// Function:  GetPDFAnnot
	// Param[in]: frAnnot	The input UI layer annotation object.
	// Return: The data layer PDF annotation.
	// Remarks:	Gets the data layer PDF annotation.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FPD_Annot GetPDFAnnot(FR_Annot frAnnot);

	//************************************
	// Function:  GetType
	// Param[in]: frAnnot	The input UI layer annotation object.
	// Param[out]: outType	It receives the type of the annotation.
	// Return: void
	// Remarks: Gets the type of the annotation.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void GetType(FR_Annot frAnnot, FS_ByteString* outType);

	//************************************
	// Function:  GetSubType
	// Param[in]: frAnnot		The input UI layer annotation object.
	// Param[out]: outSubType	It receives the sub type of the annotation.
	// Return: void
	// Remarks:	Gets the sub type of the annotation.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void GetSubType(FR_Annot frAnnot, FS_ByteString* outSubType);

	//************************************
	// Function:  SetVisible
	// Param[in]: frAnnot	The input UI layer annotation object.
	// Param[in]: bShow		It indicates whether the annotation is visible.	
	// Return: void
	// Remarks:	Sets the annotation to be visible or not.
	// Notes: You must call this interface in <a>FRPageViewOnWillParsePage</a> callback.
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.1
	//************************************
	static void SetVisible(FR_Annot frAnnot, FS_BOOL bShow);

	//************************************
	// Function:  GetPageVew
	// Param[in]: frAnnot	The input UI layer annotation object.
	// Param[in]: nIndex	The specified page view index.
	// Return: The associated page view.
	// Remarks:	Gets the associated page view.
	// Notes: 
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.1
	//************************************
	static FR_PageView GetPageVew(FR_Annot frAnnot, FS_INT32 nIndex);

	static FS_INT32 GetTabOrder(FR_Annot frAnnot);

	static void SetFlags(FR_Annot frAnnot, FS_DWORD dwFlags);

	static FS_BOOL DrawAppearance(FR_Annot frAnnot, FPD_RenderDevice pDevice, const FS_AffineMatrix pUser2Device, FPD_AnnotAppearanceMode mode, const FPD_RenderOptions pOptions);
	static FS_BOOL IsSelected(FR_Annot frAnnot);
	static void SetAuthor(FR_Annot frAnnot, const FS_WideString cwAuthor);
	static FR_Document GetFRDocByAnnot(FR_Annot frAnnot);
};

class CFR_ResourcePropertyBox_V5
{
public:
	//************************************
	// Function:  Get
	// Return: The property box. It is used to edit the properties of objects, such as the annotations, the pages and so on.
	// Remarks: Gets the property box. The Foxit Reader creates the property box so that many types of objects can reuse the same property box 
	// to edit the properties. For example, the objects can be annotations, pages and so on.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See:
	//************************************
	static FR_ResourcePropertyBox Get();

	//************************************
	// Function:  RegisterPropertyPage
	// Param[in]: rpBox	The input property box object.
	// Param[in]: pPage	The input callbacks used to add the new property page to the property box.
	// Return: void
	// Remarks: Registers the callbacks used to add the new property page to the property box.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See:
	//************************************
	static void	RegisterPropertyPage(FR_ResourcePropertyBox rpBox, FR_ResourcePropertyPageCallbacks pPage);

	//************************************
	// Function:  RegisterSourceType
	// Param[in]: rpBox			The input property box object.
	// Param[in]: nSource		The input source type. See the definitions of <Italic>FRSOURCETYPE</Italic>.
	// Param[in]: bLockButton	Indicates whether to show the check box used to lock the source.
	// Param[in]: pSourceFunc	The input callbacks used to deal with the source of the property box.
	// Param[in]: pNotifyFunc	The input callbacks that will be called when events occur.
	// Return: void
	// Remarks: Registers the source type.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See:
	//************************************
	static void	RegisterSourceType(FR_ResourcePropertyBox rpBox, FS_INT32 nSource, FS_BOOL bLockButton, FR_ResourcePropertySourceCallbacks pSourceFunc, FR_ResourcePropertyNotifyCallbacks pNotifyFunc);

	//************************************
	// Function:  GetSourceType
	// Param[in]: rpBox	The input property box object.
	// Return: The source type. See the definitions of <Italic>FRSOURCETYPE</Italic>.
	// Remarks: Gets the source type of the property box.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See:
	//************************************
	static FS_INT32 GetSourceType(FR_ResourcePropertyBox rpBox);

	//************************************
	// Function:  GetSourceFunc
	// Param[in]: rpBox		The input property box object.
	// Param[in]: nSource	The specified source type. See the definitions of <Italic>FRSOURCETYPE</Italic>.
	// Return: The property source.
	// Remarks: Gets the specified property source.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See:
	//************************************
	static FR_ResourcePropertySource GetSourceFunc(FR_ResourcePropertyBox rpBox, FS_INT32 nSource);

	//************************************
	// Function:  UpdatePropertyBox
	// Param[in]: rpBox		The input property box object.
	// Param[in]: nSource	The specified source type. See the definitions of <Italic>FRSOURCETYPE</Italic>.
	// Return: void
	// Remarks: Updates the property box.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See:
	//************************************
	static void	UpdatePropertyBox(FR_ResourcePropertyBox rpBox, FS_INT32 nSource);

	//************************************
	// Function:  OpenPropertyBox
	// Param[in]: rpBox		The input property box object.
	// Param[in]: nSource	The specified source type. See the definitions of <Italic>FRSOURCETYPE</Italic>.
	// Return: void
	// Remarks: Opens the property box.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See:
	//************************************
	static void	OpenPropertyBox(FR_ResourcePropertyBox rpBox, FS_INT32 nSource);

	//************************************
	// Function:  ClosePropertyBox
	// Param[in]: rpBox	The input property box object.
	// Return: void
	// Remarks: Closes the property box.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See:
	//************************************
	static void	ClosePropertyBox(FR_ResourcePropertyBox rpBox);

	//************************************
	// Function:  IsPropertyBoxVisible
	// Param[in]: rpBox	The input property box object.
	// Return: <a>TRUE</a> if the property box is visible, otherwise not.
	// Remarks: Checks whether the property box is visible or not.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See:
	//************************************
	static FS_BOOL IsPropertyBoxVisible(FR_ResourcePropertyBox rpBox);

	//************************************
	// Function:  GetWnd
	// Param[in]: rpBox	The input property box object.
	// Return: The window handle of the property box.
	// Remarks: Gets the window handle of the property box.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See:
	//************************************
	static HWND GetWnd(FR_ResourcePropertyBox rpBox);

	//************************************
	// Function:  GetPropertyPage
	// Param[in]: rpBox		The input property box object.
	// Param[in]: lpwsName	The specified name of the property page.
	// Return: The property page.
	// Remarks: Gets the property page by name.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See:
	//************************************
	static FR_ResourcePropertyPage GetPropertyPage(FR_ResourcePropertyBox rpBox, FS_LPCWSTR lpwsName);

	//************************************
	// Function:  SetCurrentPropertyPage
	// Param[in]: rpBox	The input property box object.
	// Param[in]: pPage	The input property page.
	// Return: void
	// Remarks: Sets the current property page.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See:
	//************************************
	static void	SetCurrentPropertyPage(FR_ResourcePropertyBox rpBox, FR_ResourcePropertyPage pPage);

	//************************************
	// Function:  RegisterPropertyPage2
	// Param[in]: rpBox	The input property box object.
	// Param[in]: pPage	The input callbacks used to add the new property page to the property box.
	// Return: The page object that needs to be destroyed by <a>FRResourcePropertyBoxDestroyPage</a>.
	// Remarks: Registers the callbacks used to add the new property page to the property box.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 3.0
	// See:
	//************************************
	static void* RegisterPropertyPage2(FR_ResourcePropertyBox rpBox, FR_ResourcePropertyPageCallbacks pPage);

	//************************************
	// Function:  DestroyPage
	// Param[in]: pPage	The page object returned by <a>FRResourcePropertyBoxRegisterPropertyPage2</a>.
	// Return: void.
	// Remarks: Destroys the page object returned by <a>FRResourcePropertyBoxRegisterPropertyPage2</a>.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 3.0
	// See:
	//************************************
	static void DestroyPage(void* pPage);

	//************************************
	// Function:  RegisterSourceType2
	// Param[in]: rpBox			The input property box object.
	// Param[in]: nSource		The input source type. See the definitions of <Italic>FRSOURCETYPE</Italic>.
	// Param[in]: bLockButton	Indicates whether to show the check box used to lock the source.
	// Param[in]: pSourceFunc	The input callbacks used to deal with the source of the property box.
	// Param[in]: pNotifyFunc	The input callbacks that will be called when events occur.
	// Return: The source object that needs to be destroyed by <a>FRResourcePropertyBoxDestroySource</a>.
	// Remarks: Registers the source type.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See:
	//************************************
	static void* RegisterSourceType2(FR_ResourcePropertyBox rpBox, FS_INT32 nSource, FS_BOOL bLockButton, FR_ResourcePropertySourceCallbacks pSourceFunc, FR_ResourcePropertyNotifyCallbacks pNotifyFunc);

	//************************************
	// Function:  DestroySource
	// Param[in]: pPage	The source object returned by <a>FRResourcePropertyBoxRegisterSourceType2</a>.
	// Return: void.
	// Remarks: Destroys the source object returned by <a>FRResourcePropertyBoxRegisterSourceType2</a>.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 3.0
	// See:
	//************************************
	static void DestroySource(void* pSource);
};

class CFR_ScrollBarThumbnailView_V6
{
public:

	//************************************
	// Function:  GetCurPageIndex
	// Param[in]: tv			The input scrollbar thumbnail view.
	// Return: The index of page displayed in the scroll bar thumbnail view.	
	// Remarks:	Gets the index of page displayed in the scroll bar thumbnail view.   	
	// Notes:
    // Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	// See: 
	//************************************
	static FS_INT32 GetCurPageIndex(FR_ScrollBarThumbnailView tv);

	

	//************************************
	// Function:  GetPageRect
	// Param[in]: tv			The input scrollbar thumbnail view.
	// Return: The rectangle of specified visible page.	
	// Remarks:	Gets the rectangle of specified visible page.
	// Notes:
    // Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	// See:
	//************************************
	static FS_Rect GetPageRect(FR_ScrollBarThumbnailView tv);
	
	//************************************
	// Function:  GetPDPage
	// Param[in]: tv		The input scrollbar thumbnail view.
	// Return: The <a>FPD_Page</a> object.
	// Remarks:	Gets a <a>FPD_Page</a> object.
	// Notes:
    // Since: <a>SDK_LATEEST_VERSION</a> > 2.1.0.4
	// See: 
	//************************************
	static FPD_Page GetPDPage(FR_ScrollBarThumbnailView tv);
};

class CFR_Reader_DispViewerEx_V13
{
public:
public:
	static					FR_IReader_DispViewer	CreateDispViewer(FR_DocView pDocView, FR_IReader_DispViewerHandler  pHandler);

	static FR_DocView 		GetDocView(FR_IReader_DispViewer DispViewer);

public:

	static void				SetSize(FR_IReader_DispViewer DispViewer, FS_INT32 nSizeX, FS_INT32 nSizeY);

	static	FS_DIBitmap		GetRenderData(FR_IReader_DispViewer DispViewer);

	static	void			ContinueRendering(FR_IReader_DispViewer DispViewer);

	static	void			SetCenterPos(FR_IReader_DispViewer DispViewer, FS_INT32 nPage, double dbZoom, FS_FloatPoint pt);

public:
	static void				DocToWindow(FR_IReader_DispViewer DispViewer, FS_INT32 nPage, FS_FLOAT doc_x, FS_FLOAT doc_y, FS_INT64& window_x, FS_INT64& window_y);

	static BOOL				GetCurrentMatrix(FR_IReader_DispViewer DispViewer, FS_INT32 nPage, FS_AffineMatrix& matrix);

	static void				GetSize(FR_IReader_DispViewer DispViewer, FS_INT32 &nSizeX, FS_INT32 &nSizeY);
};

class CIPDFViewerEventHandler : IPDFViewerEventHandler
{
public:
	CIPDFViewerEventHandler(FPD_IPDFViewerEventHandlerCallbacksRec* pcallbacks) {
		memset(&m_callbacks, 0, sizeof(FPD_IPDFViewerEventHandlerCallbacksRec));
		memcpy(&m_callbacks, pcallbacks, pcallbacks->lStructSize);
		if (pcallbacks->lStructSize < sizeof(FPD_IPDFViewerEventHandlerCallbacksRec))
		{
			FS_BYTE* pAddress = (FS_BYTE*)&m_callbacks;
			memset(pAddress + pcallbacks->lStructSize, 0, sizeof(FPD_IPDFViewerEventHandlerCallbacksRec) - pcallbacks->lStructSize);
		}
	}
	virtual		void			OnBeforeDraw(CFX_DIBitmap * pBitmap);

	virtual		void			OnAfterDraw(CFX_DIBitmap * pBitmap);

	virtual		void			OnDrawBkgnd(CFX_DIBitmap * pBitmap);

	virtual		void			OnDrawPageShawdow(CFX_DIBitmap * pBitmap, IPDFViewerPage * pPage);

	virtual		void			OnPageVisible(IPDFViewerPage * pPage, BOOL	bVisble);

	virtual		void			OnPageIndexChange(IPDFViewerPage * pPage);

	virtual		void			OnBeginRender();

	virtual		void			OnFinishRender();

	virtual		void			OnRenderDataChange();

	virtual		void			OnZoomPage(FX_INT32 nPage);

	virtual		void			OnGotoPage(FX_INT32 nPage);

	virtual		void			OnBeforePageDraw(IPDFViewerPage * pPage, CFX_DIBitmap * pBitmap);

	virtual		void			OnAfterPageDraw(IPDFViewerPage * pPage, CFX_DIBitmap * pBitmap);

	virtual		BOOL			IsFullScreenLoopPage();
public:
	FPD_IPDFViewerEventHandlerCallbacksRec m_callbacks;
	void* GetClientData() { return m_callbacks.clientData; }
};

class CFR_IReader_DispViewerHandler : public IReader_DispViewerHandler
{
public:
	CFR_IReader_DispViewerHandler(FR_IReader_DispViewerHandlerCallbacksRec* pcallbacks) {
		memset(&m_callbacks, 0, sizeof(FR_IReader_DispViewerHandlerCallbacksRec));
		memcpy(&m_callbacks, pcallbacks, pcallbacks->lStructSize);
		if (pcallbacks->lStructSize < sizeof(FR_IReader_DispViewerHandlerCallbacksRec))
		{
			FS_BYTE* pAddress = (FS_BYTE*)&m_callbacks;
			memset(pAddress + pcallbacks->lStructSize, 0, sizeof(FR_IReader_DispViewerHandlerCallbacksRec) - pcallbacks->lStructSize);
		}
	}
	virtual	void OnBeginRender(IPDFViewerEventHandler* pHandler);

	virtual	void OnFinishRender(IPDFViewerEventHandler* pHandler);
public:
	FR_IReader_DispViewerHandlerCallbacksRec m_callbacks;
	void* GetClientData() { return m_callbacks.clientData; }
};

class CFR_IReaderDispViewerHandler_V13
{
public:
	static FR_IReader_DispViewerHandler New(FR_IReader_DispViewerHandlerCallbacksRec* callbacks);

	static void Destroy(FR_IReader_DispViewerHandler handler);
};

class CFR_IPDFViewerEventHandler_V13
{
public:
	static FR_IPDFViewerEventHandler New(FPD_IPDFViewerEventHandlerCallbacksRec* callbacks);
	static void Destroy(FR_IPDFViewerEventHandler handler);
};

class CFR_ToolFormatMgr_V13 
{
public:
    //************************************
    // Function:  Get
    // Param[in]: hFrame			The input scrollbar thumbnail view.
    // Return: The HWnd of ToolFormatMgr.	
    // Remarks:	Gets the HWnd of of ToolFormatMgr.
    // Notes:
    // Since: <a>SDK_LATEEST_VERSION</a> > 10.1.0.0
    // See:
    //************************************
    static FR_ToolFormatMgr Get(HWND hFrame);

    //************************************
    // Function:  RegistFormatEvent
    // Param[in]: toolFormatMgr			The HWnd of ToolFormatMgr.
    // Param[in]: formatEventCallbacks			The callback set. Reader will call a corresponding callback when the ToolFormatMgr event occurs.
    // Return: The Address of FormatEventHandler.	
    // Remarks:	Register the ToolFormatMgr callback.
    // Notes:
    // Since: <a>SDK_LATEEST_VERSION</a> > 10.1.0.0
    // See:
    //************************************
    static FS_LPVOID RegistFormatEvent(FR_ToolFormatMgr toolFormatMgr, FR_FormatEventCallbacks formatEventCallbacks);

    //************************************
    // Function:  UnregisterFormatEvent
    // Param[in]:  toolFormatMgr			The HWnd of ToolFormatMgr.
    // Param[in]:  pFormatToolEvent			The Address of FormatEventHandler.	
    // Return: 	<a>TRUE</a> for success, otherwise not.
    // Remarks:	 Unregisters the ToolFormatMgr handler and releases the memory.
    // Notes:
    // Since: <a>SDK_LATEEST_VERSION</a> > 10.1.0.0
    // See:
    //************************************
    static FS_BOOL UnregisterFormatEvent(FR_ToolFormatMgr toolFormatMgr, FS_LPVOID pFormatToolEvent);

    //************************************
    // Function:  AddGroup
    // Param[in]:  toolFormatMgr			The HWnd of ToolFormatMgr.
    // Param[in]:  bsGroupName	the unique identify of Group.
    // Param[in]:  wsTitle	Group Title.
    // Param[in]:  hWnd	  the handle of the Group child window ，advise you to use dialog.	
    // Return: 	NULL
    // Remarks: Add  the Group's Child window to the ToolFormat
    // Notes:
    // Since: <a>SDK_LATEEST_VERSION</a> > 10.1.0.0
    // See:
    //************************************
    static void AddGroup(FR_ToolFormatMgr toolFormatMgr, FS_ByteString bsGroupName, FS_WideString wsTitle, HWND hWnd);

    //************************************
    // Function: AddGroupToFormat
    // Param[in]:  toolFormatMgr			The HWnd of ToolFormatMgr.
    // Param[in]:  bsFormatName	 ToolFormat's identify.			
    // Return: 	NULL
    // Remarks:	Add Group to ToolFormat
    // Notes:
    // Since: <a>SDK_LATEEST_VERSION</a> > 10.1.0.0
    // See:
    //************************************
    static void AddGroupToFormat(FR_ToolFormatMgr toolFormatMgr, FS_ByteString bsFormatName, FS_ByteString bsGroupName);

    //************************************
    // Function:  ShowToolFormat
    // Param[in]: toolFormatMgr			The HWnd of ToolFormatMgr.		
    // Param[in]: bsFormatName			the name of ToolFormat.	
    // Param[in]: wsFormatTitle			The Title of ToolFormatMgr.	
    // Return: 	NULL
    // Remarks:	this functioin is used to show the ToolFormat.
    // Notes:
    // Since: <a>SDK_LATEEST_VERSION</a> > 10.1.0.0
    // See:
    //************************************
    static void ShowToolFormat(FR_ToolFormatMgr toolFormatMgr, FS_ByteString bsFormatName, FS_WideString wsFormatTitle);

    //************************************
    // Function:  
    // Param[in]: toolFormatMgr			The HWnd of ToolFormatMgr.
    // Param[in]: bsFormatName			the name of ToolFormat.	
    // Return: 		<a>TRUE</a> for success, otherwise not.
    // Remarks:	determine whether the ToolFormat Exist.
    // Notes:
    // Since: <a>SDK_LATEEST_VERSION</a> > 10.1.0.0
    // See:
    //************************************
    static bool IsToolFormatExist(FR_ToolFormatMgr toolFormatMgr, FS_ByteString bsFormatName);

    //************************************
    // Function: GetFormatGroups 
    // Param[in]: toolFormatMgr			The HWnd of ToolFormatMgr.
    // Param[in]: bsFormatName			the name of ToolFormat.	
    // Param[out]: bsGroupNames			the names of ToolFormat include.	
    // Return: 	NULL
    // Remarks:	Get the ToolFormat's GroupNames.
    // Notes:
    // Since: <a>SDK_LATEEST_VERSION</a> > 10.1.0.0
    // See:
    //************************************
    static void GetFormatGroups(FR_ToolFormatMgr toolFormatMgr, FS_ByteString bsFormatName, FS_ByteStringArray bsGroupNames);

    //************************************
    // Function:  ChangeFormatSize
    // Param[in]: toolFormatMgr			The HWnd of ToolFormatMgr.
    // Param[in]: bsFormatName			the name of ToolFormat.	
    // Param[in]: wsGroupTitle			The Title of Group.	
    // Return: 	NULL
    // Remarks:	Caculate the Size of the ToolFormat's Group once again. 
    // Notes:
    // Since: <a>SDK_LATEEST_VERSION</a> > 10.1.0.0
    // See:
    //************************************
	static void ChangeFormatSize(FR_ToolFormatMgr toolFormatMgr, FS_ByteString bsFormatName, FS_ByteString wsGroupTitle);
};


#ifdef __cplusplus
};
#endif
#endif//FR_VIEWIMPL_H
