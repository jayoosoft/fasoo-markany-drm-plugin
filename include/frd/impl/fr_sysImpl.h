﻿/** @file fr_sysImpl.h.
 * 
 *  @brief many system-wide utilies definitions .
 */

#ifndef FR_SYSIMPL_H
#define FR_SYSIMPL_H


#ifndef FR_SYSEXPT_H
#include "../fr_sysExpT.h"
#endif


#ifdef __cplusplus
extern "C"{
#endif

class CFR_Sys_V1
{
public:
	//************************************
	// Function:  LoadStandarCursor
	// Param[in]: nCursorType The type of the cursor. See <a>FRCursorTypeFlags</a>.
	// Return:	  The specified cursor loaded.
	// Remarks:   Loads a standard cursor by type.
	// Notes:	
	// See: FRSysSetCursor
	// See: FRSysGetCursor
	//************************************
	static FR_Cursor LoadStandarCursor(FS_INT32 nCursorType);

	//************************************
	// Function:  GetCursor
	// Return:	  The current cursor.
	// Remarks:   Gets the current cursor.
	// Notes:	
	// See: FRSysSetCursor
	// See: FRSysLoadStandarCursor
	//************************************
	static FR_Cursor GetCursor(void);

	//************************************
	// Function:  SetCursor
	// Param[in]: cursor The input cursor to be set.
	// Return:	 void 
	// Remarks:   Sets the current cursor.
	// Notes:
	// See: FRSysGetCursor
	// See: FRSysLoadStandarCursor
	//************************************
	static void	SetCursor(FR_Cursor cursor);

	//************************************
	// Function:  ShowMessageBox
	// Param[in]: wszPrompt			Pointer to a null-terminated string that contains the message to be displayed. 
	// Param[in]: nType				Specifies the contents and behavior of the dialog box. See the description about <Italic>MessageBox</Italic> from MSDN.
	// Param[in]: wszCaption		Pointer to a null-terminated string that contains the dialog box title. If this parameter is NULL, the default title is used.
	// Param[in]: pCheckBoxParams	Pointer to a data structure representing the params of the check box on the message box. If this parameter is NULL, the default value is used.
	// Param[in]: hParent			The parent window.
	// Return: See the description about <Italic>MessageBox</Italic> from <Italic>MSDN</Italic>.
	// Remarks: Creates, displays, and operates a message box. The message box contains an application-defined message 
	// and title, plus any combination of predefined icons and push buttons.
	// Notes:
	//************************************
	static FS_INT32 ShowMessageBox(FS_LPCWSTR wszPrompt, unsigned int nType, FS_LPCWSTR wszCaption, FR_MSGBOX_CHECKBOXPARAMS* pCheckBoxParams, HWND hParent);

	//************************************
	// Function:  InstallDialogSkinTheme
	// Param[in]: hWnd The dialog to install skin theme of <Italic>Foxit Reader</Italic>.
	// Return: <a>True</a> means successful, otherwise not.
	// Remarks: The dialog can install the skin theme of <Italic>Foxit Reader</Italic> to keep the same.
	// Invokes this interface in the <Italic>CReader_Dialog::OnInitDialog</Italic> routine of the dialog.
	// Notes:
	//************************************
	static FS_BOOL InstallDialogSkinTheme(HWND hWnd);

	//************************************
	// Function:  InstallDialogScrollBar
	// Param[in]: hWnd	The dialog to install the scroll bar.
	// Return:  <a>True</a> means successful, otherwise not.
	// Remarks: Installs the dialog scroll bar.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 2.0
	// See:
	//************************************
	static FS_BOOL InstallDialogScrollBar(HWND hWnd);

	//************************************
	// Function:  GetSkinCount
	// Return:  The count of skins for Foxit Reader.
	// Remarks: Gets the count of skins for Foxit Reader.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.0.0.0
	// See:
	//************************************
	static FS_INT32 GetSkinCount();

	//************************************
	// Function:  GetSkinNameByIndex
	// Param[in]: nIndex	The input index.
	// Param[out]: outName	It receives the name of the skin.
	// Return:  void.
	// Remarks: Gets the skin name by index.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.0.0.0
	// See:
	//************************************
	static void GetSkinNameByIndex(FS_INT32 nIndex, FS_WideString* outName);

	//************************************
	// Function:  SetActiveSkinByIndex
	// Param[in]: nIndex	The input index.
	// Return:  TRUE for success, otherwise failure.
	// Remarks: Sets the active skin by index.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.0.0.0
	// See:
	//************************************
	static FS_BOOL SetActiveSkinByIndex(FS_INT32 nIndex);

	//************************************
	// Function:  SetActiveSkinByName
	// Param[in]: nIndex	The input name of the skin.
	// Return:  TRUE for success, otherwise failure.
	// Remarks: Sets the active skin by name.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 8.0.0.0
	// See:
	//************************************
	static FS_BOOL SetActiveSkinByName(FS_LPCWSTR lpwsName);

	//************************************
	// Function:  ShowStopLaunchDlg
	// Param[out]: pbModify			If user modify the setting.
	// Param[in]: lpwsInfo			The description of Safe Mode check button.
	// Param[in]: lpwsCheckSafeMode	The text of Safe Mode check button.
	// Return:  void
	// Remarks: Show the safe mode dialog.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 9.7.1
	// See:
	//************************************
	static void ShowStopLaunchDlg(FS_BOOL *pbModify, FS_LPCWSTR lpwsInfo, FS_LPCWSTR lpwsCheckSafeMode);

	//************************************
// Function:  ShowMessageBox
// Param[in]: wszPrompt			Pointer to a null-terminated string that contains the message to be displayed. 
// Param[in]: nType				Specifies the contents and behavior of the dialog box. See the description about <Italic>MessageBox</Italic> from MSDN.
// Param[in]: wszCaption		Pointer to a null-terminated string that contains the dialog box title. If this parameter is NULL, the default title is used.
// Param[in]: arCheckBoxParams	FR_MSGBOX_CHECKBOXPARAMS array, Pointer to a data structure representing the params of the check box on the message box. If this parameter is empty, the default value is used.
// Param[in]: bRadioBoxMode		Show radiobox or checkbox.
// Param[in]: hParent			The parent window.
// Return: See the description about <Italic>MessageBox</Italic> from <Italic>MSDN</Italic>.
// Remarks: Creates, displays, and operates a message box. The message box contains an application-defined message 
// and title, plus any combination of predefined icons and push buttons.
// Notes:
//************************************
	static FS_INT32 ShowMessageBox2(FS_LPCWSTR wszPrompt, unsigned int nType, FS_LPCWSTR wszCaption, FS_PtrArray arCheckBoxParams, FS_BOOL bRadioBoxMode, HWND hParent);

	//************************************
// Function:  ShowMessageBox
// Param[in]: wszPrompt			Pointer to a null-terminated string that contains the message to be displayed. 
// Param[in]: buttons			The type of the buttons,FR_MBOK equal to MB_OK
// Param[in]: nType				Specifies the contents and behavior of the dialog box. See the description about <Italic>MessageBox</Italic> from MSDN.
// Param[in]: wszCaption		Pointer to a null-terminated string that contains the dialog box title. If this parameter is NULL, the default title is used.
// Param[in]: arCheckBoxParams	FR_MSGBOX_CHECKBOXPARAMS array, Pointer to a data structure representing the params of the check box on the message box. If this parameter is empty, the default value is used.
// Param[in]: bRadioBoxMode		Show radiobox or checkbox.
// Param[in]: hParent			The parent window.
// Return: See the description about <Italic>MessageBox</Italic> from <Italic>MSDN</Italic>.
// Remarks: Creates, displays, and operates a message box. The message box contains an application-defined message 
// and title, plus any combination of predefined icons and push buttons.
// Notes:
//************************************
	static FS_INT32 ShowMessageBox3(FS_LPCWSTR wszPrompt, FR_MsgBtnType button, FR_MsgType nType, FS_LPCWSTR wszCaption, FR_MSGBOX_CHECKBOXPARAMS2* pCheckBoxParams, FS_HWND hParent);
};

class CFR_WindowsDIB_V5
{
public:
	//************************************
	// Function:  New
	// Param[in]: hDC		The input windows device context.
	// Param[in]: width		The bimtap width.
	// Param[in]: height	The bitmap height.
	// Return: A DIB.
	// Remarks: Constructs a DIB that can be efficiently interact with Windows device.
	// Currently the constructed DIB will always in <a>FS_DIB_Rgb</a> format.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_DIBitmap	New(HDC hDC, FS_INT32 width, FS_INT32 height);

	//************************************
	// Function:  Destroy
	// Param[in]: pBitmap	The input DIB bitmap.
	// Return: void.
	// Remarks: Destroys the input DIB bitmap.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void Destroy(FS_DIBitmap pBitmap);

	//************************************
	// Function:  GetBitmapInfo
	// Param[in]: pBitmap		The input DIB bitmap.
	// Param[out]: outInfo		It receives the windows bitmap info structure for the DIB.
	// Return: void.
	// Remarks: Get Windows bitmap info structure.
	// The result is a binary string that can be used at a <Italic>BITMAPINFO</Italic> structure.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void	GetBitmapInfo(FS_DIBitmap pBitmap, FS_ByteString* outInfo);

	//************************************
	// Function:  LoadFromBuf
	// Param[in]: pbmi		The windows bitmap info structure.
	// Param[in]: pData		The bitmap data.
	// Return: A bitmap from existing data.
	// Remarks: Constructs a bitmap from existing data.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_DIBitmap LoadFromBuf(BITMAPINFO* pbmi, void* pData);

	//************************************
	// Function:  GetDDBitmap
	// Param[in]: pBitmap		The input DIB.
	// Param[in]: hDC			The input DC.
	// Return: A device dependent bitmap compatible with the input DC.
	// Remarks: Converts to device compatible bitmap.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static HBITMAP GetDDBitmap(FS_DIBitmap pBitmap, HDC hDC);

	//************************************
	// Function:  LoadFromDDB
	// Param[in]: hDC		The input DC.
	// Param[in]: hBitmap	The input device dependent bitmap.
	// Param[in]: pPalette	The bmp's palette, applicable to 1bppRgb and 8bppRgb formats.
	// Param[in]: size		The palette's size.
	// Return: A DIB.
	// Remarks: Load DI bitmap from DDB. If <param>hDC</param> is NULL, system display DC will be used.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_DIBitmap LoadFromDDB(HDC hDC, HBITMAP hBitmap, FS_DWORD* pPalette, FS_DWORD size);

	//************************************
	// Function:  LoadFromFile
	// Param[in]: filename		The input full file path.
	// Return: A DIB.
	// Remarks: Load DI bitmap from file. Unicode version. Without GDI+ support, we supports only BMP file.
	// With GDI support, we support much more.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_DIBitmap LoadFromFile(FS_LPCWSTR filename);
	
	//************************************
	// Function:  LoadFromFileII
	// Param[in]: filename		The input full file path.
	// Return: A DIB.
	// Remarks: Load DI bitmap from file. Unicode version. Without GDI+ support, we supports only BMP file.
	// With GDI support, we support much more.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_DIBitmap LoadFromFileII(FS_LPCSTR filename);

	//************************************
	// Function:  LoadDIBitmap
	// Param[in]: args	The input full file path or memory.
	// Return: A DIB.
	// Remarks: Load DI bitmap from file. Unicode version. Without GDI+ support, we supports only BMP file.
	// With GDI support, we support much more.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static FS_DIBitmap LoadDIBitmap(FR_WINDIB_Open_Args args);
	
	//************************************
	// Function:  GetDC
	// Param[in]: pBitmap	The input DIB bitmap.
	// Return: The DC.
	// Remarks: Gets the DC.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static HDC GetDC(FS_DIBitmap pBitmap);

	//************************************
	// Function:  GetWindowsBitmap
	// Param[in]: pBitmap	The input DIB bitmap.
	// Return: The bitmap handle.
	// Remarks: Gets the bitmap.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static HBITMAP GetWindowsBitmap(FS_DIBitmap pBitmap);

	//************************************
	// Function:  LoadFromDevice
	// Param[in]: pBitmap	The input DIB bitmap.
	// Param[in]: hDC		The input windows device context.
	// Param[in]: left		The x-coordinate in the windows DC.
	// Param[in]: top		The y-coordinate in the windows DC.
	// Return: The bitmap.
	// Remarks: Loads from a device.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void LoadFromDevice(FS_DIBitmap pBitmap, HDC hDC, FS_INT32 left, FS_INT32 top);

	//************************************
	// Function:  SetToDevice
	// Param[in]: pBitmap	The input DIB bitmap.
	// Param[in]: hDC		The input windows device context.
	// Param[in]: left		The x-coordinate in the windows DC.
	// Param[in]: top		The y-coordinate in the windows DC.
	// Return: void.
	// Remarks: Outputs to a device.
	// Notes:
	// Since: <a>SDK_LATEEST_VERSION</a> > 1.0
	//************************************
	static void SetToDevice(FS_DIBitmap pBitmap, HDC hDC, FS_INT32 left, FS_INT32 top);
};

#ifdef __cplusplus
};
#endif
#endif
