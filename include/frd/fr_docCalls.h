﻿/*****************************************************************************

Copyright (C) 2010 Foxit Corporation
All rights reserved.

NOTICE: Foxit permits you to use, modify, and distribute this file
in accordance with the terms of the Foxit license agreement
accompanying it. If you have received this file from a source other
than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
 
*****************************************************************************/

#ifndef FR_DOCCALLS_H
#define FR_DOCCALLS_H

#ifndef FS_COMMON_H
#include "../basic/fs_common.h"
#endif

#ifndef FS_BASICEXPT_H
#include "../basic/fs_basicExpT.h"
#endif

#ifndef FS_STRINGEXPT_H
#include "../basic/fs_stringExpT.h"
#endif

#ifndef FR_DOCEXPT_H
#include "fr_docExpT.h"
#endif

#ifdef __cplusplus
extern "C"{
#endif

#undef INTERFACE
#undef BEGINENUM
#undef NumOfSelector
#undef ENDENUM

// Define macros to generate function selectors in the file fr_docImpl.h
#define BEGINENUM enum{
#define NumOfSelector(name) name##InterfacesNum
#define ENDENUM };
#define INTERFACE(returnType,interfaceName,params) interfaceName##SEL,
#include "fr_docTempl.h"

#undef INTERFACE
#undef BEGINENUM
#undef NumOfSelector
#undef ENDENUM

// Define macros to generate function PROTO in the file fr_docImpl.h
#define BEGINENUM
#define NumOfSelector(name)
#define ENDENUM
#define INTERFACE(returnType,interfaceName, params) \
typedef returnType (*interfaceName##SELPROTO)params;
#include "fr_docTempl.h"

//----------_V1----------
//*****************************
/* Doc HFT functions */
//*****************************

#define FRDocOpenFromFile (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocOpenFromFileSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocOpenFromFileSEL, _gPID)))

#define FRDocOpenFromPDDoc (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocOpenFromPDDocSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocOpenFromPDDocSEL, _gPID)))

#define FRDocFromPDDoc (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocFromPDDocSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocFromPDDocSEL, _gPID)))

#define FRDocClose (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocCloseSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocCloseSEL, _gPID)))

#define FRDocGetPDDoc (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetPDDocSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetPDDocSEL, _gPID)))

#define FRDocSetCustomSecurity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetCustomSecuritySELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetCustomSecuritySEL, _gPID)))

#define FRDocDoSave (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocDoSaveSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocDoSaveSEL, _gPID)))

#define FRDocDoSaveAs (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocDoSaveAsSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocDoSaveAsSEL, _gPID)))

#define FRDocSetChangeMark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetChangeMarkSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetChangeMarkSEL, _gPID)))

#define FRDocGetChangeMark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetChangeMarkSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetChangeMarkSEL, _gPID)))

#define FRDocClearChangeMark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocClearChangeMarkSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocClearChangeMarkSEL, _gPID)))

#define FRDocWillInsertPages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocWillInsertPagesSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocWillInsertPagesSEL, _gPID)))

#define FRDocDidInsertPages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocDidInsertPagesSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocDidInsertPagesSEL, _gPID)))

#define FRDocWillDeletePages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocWillDeletePagesSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocWillDeletePagesSEL, _gPID)))

#define FRDocDidDeletePages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocDidDeletePagesSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocDidDeletePagesSEL, _gPID)))

#define FRDocWillRotatePage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocWillRotatePageSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocWillRotatePageSEL, _gPID)))

#define FRDocDidRotatePage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocDidRotatePageSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocDidRotatePageSEL, _gPID)))

#define FRDocWillResizePage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocWillResizePageSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocWillResizePageSEL, _gPID)))

#define FRDocDidResizePage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocDidResizePageSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocDidResizePageSEL, _gPID)))

#define FRDocDoPrint (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocDoPrintSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocDoPrintSEL, _gPID)))

#define FRDocPrintPages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocPrintPagesSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocPrintPagesSEL, _gPID)))

#define FRDocPrintSetup (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocPrintSetupSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocPrintSetupSEL, _gPID)))

#define FRDocCountDocViews (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocCountDocViewsSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocCountDocViewsSEL, _gPID)))

#define FRDocGetDocView (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetDocViewSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetDocViewSEL, _gPID)))

#define FRDocGetCurrentDocView (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetCurrentDocViewSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetCurrentDocViewSEL, _gPID)))

#define FRDocGetPermissions (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetPermissionsSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetPermissionsSEL, _gPID)))

#define FRDocSetPermissions (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetPermissionsSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetPermissionsSEL, _gPID)))

#define FRDocGetFilePath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetFilePathSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetFilePathSEL, _gPID)))

#define FRDocSetCurSelection (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetCurSelectionSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetCurSelectionSEL, _gPID)))

#define FRDocAddToCurrentSelection (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocAddToCurrentSelectionSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocAddToCurrentSelectionSEL, _gPID)))

#define FRDocRemoveFromSelection (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocRemoveFromSelectionSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocRemoveFromSelectionSEL, _gPID)))

#define FRDocGetCurSelection (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetCurSelectionSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetCurSelectionSEL, _gPID)))

#define FRDocGetCurSelectionType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetCurSelectionTypeSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetCurSelectionTypeSEL, _gPID)))

#define FRDocShowSelection (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocShowSelectionSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocShowSelectionSEL, _gPID)))

#define FRDocClearSelection (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocClearSelectionSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocClearSelectionSEL, _gPID)))

#define FRDocDeleteSelection (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocDeleteSelectionSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocDeleteSelectionSEL, _gPID)))

#define FRDocCopySelection (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocCopySelectionSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocCopySelectionSEL, _gPID)))

#define FRDocSetCurCapture (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetCurCaptureSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetCurCaptureSEL, _gPID)))

#define FRDocGetCurCapture (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetCurCaptureSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetCurCaptureSEL, _gPID)))

#define FRDocGetCurCaptureType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetCurCaptureTypeSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetCurCaptureTypeSEL, _gPID)))

#define FRDocReleaseCurCapture (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocReleaseCurCaptureSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocReleaseCurCaptureSEL, _gPID)))

#define FRDocSetMenuEnableByName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetMenuEnableByNameSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetMenuEnableByNameSEL, _gPID)))

#define FRDocGetParser (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetParserSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetParserSEL, _gPID)))

#define FRDocGetPDFCreator (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetPDFCreatorSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetPDFCreatorSEL, _gPID)))

#define FRDocDoPrintSilently (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocDoPrintSilentlySELPROTO)FRCOREROUTINE(FRDocSEL,FRDocDoPrintSilentlySEL, _gPID)))

#define FRDocGetTextSelectTool (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetTextSelectToolSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetTextSelectToolSEL, _gPID)))

#define FRDocGetDocumentType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetDocumentTypeSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetDocumentTypeSEL, _gPID)))

#define FRDocCanSecurityMethodBeModified (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocCanSecurityMethodBeModifiedSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocCanSecurityMethodBeModifiedSEL, _gPID)))

#define FRDocUpdateSecurityMethod (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocUpdateSecurityMethodSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocUpdateSecurityMethodSEL, _gPID)))

#define FRDocIsEncrypted (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocIsEncryptedSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocIsEncryptedSEL, _gPID)))

#define FRDocRemoveSecurityMethod (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocRemoveSecurityMethodSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocRemoveSecurityMethodSEL, _gPID)))

#define FRDocEnableRunScript (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocEnableRunScriptSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocEnableRunScriptSEL, _gPID)))

#define FRDocIsEnableRunScript (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocIsEnableRunScriptSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocIsEnableRunScriptSEL, _gPID)))

#define FRDocChangeDocShowTitle (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocChangeDocShowTitleSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocChangeDocShowTitleSEL, _gPID)))

#define FRDocIsMemoryDoc (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocIsMemoryDocSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocIsMemoryDocSEL, _gPID)))

#define FRDocGetCurrentSecurityMethodName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetCurrentSecurityMethodNameSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetCurrentSecurityMethodNameSEL, _gPID)))

#define FRDocGetCurrentWndProvider (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetCurrentWndProviderSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetCurrentWndProviderSEL, _gPID)))

#define FRDocSetCurrentWndProvider (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetCurrentWndProviderSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetCurrentWndProviderSEL, _gPID)))

#define FRDocGetWndProviderByName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetWndProviderByNameSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetWndProviderByNameSEL, _gPID)))

#define FRDocGetReviewType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetReviewTypeSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetReviewTypeSEL, _gPID)))

#define FRDocSetReviewType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetReviewTypeSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetReviewTypeSEL, _gPID)))

#define FRDocSetAppPermissions (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetAppPermissionsSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetAppPermissionsSEL, _gPID)))

#define FRDocGetAppPermissions (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetAppPermissionsSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetAppPermissionsSEL, _gPID)))

#define FRDocGetAppPermissionsII (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetAppPermissionsIISELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetAppPermissionsIISEL, _gPID)))

#define FRDocGetPermissionsII (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetPermissionsIISELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetPermissionsIISEL, _gPID)))

#define FRDocGetMergedPermissions (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetMergedPermissionsSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetMergedPermissionsSEL, _gPID)))

#define FRDocKillFocusAnnot (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocKillFocusAnnotSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocKillFocusAnnotSEL, _gPID)))

#define FRDocSetPropertiesPDFVersion (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetPropertiesPDFVersionSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetPropertiesPDFVersionSEL, _gPID)))

#define FRDocSetPropertiesFilePath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetPropertiesFilePathSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetPropertiesFilePathSEL, _gPID)))

#define FRDocDoSaveAs2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocDoSaveAs2SELPROTO)FRCOREROUTINE(FRDocSEL,FRDocDoSaveAs2SEL, _gPID)))

#define FRDocShowSaveProgressCancelButton (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocShowSaveProgressCancelButtonSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocShowSaveProgressCancelButtonSEL, _gPID)))

#define FRDocSetInputPasswordProc (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetInputPasswordProcSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetInputPasswordProcSEL, _gPID)))

#define FRDocCheckInDocumentOLE (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocCheckInDocumentOLESELPROTO)FRCOREROUTINE(FRDocSEL,FRDocCheckInDocumentOLESEL, _gPID)))

#define FRDocIsShowInBrowser (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocIsShowInBrowserSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocIsShowInBrowserSEL, _gPID)))

#define FRDocGetUIParentWnd (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetUIParentWndSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetUIParentWndSEL, _gPID)))

#define FRDocGetDocFrameHandler (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetDocFrameHandlerSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetDocFrameHandlerSEL, _gPID)))

#define FRDocCreateNewViewByWndProvider (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocCreateNewViewByWndProviderSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocCreateNewViewByWndProviderSEL, _gPID)))

#define FRDocLoadAnnots (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocLoadAnnotsSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocLoadAnnotsSEL, _gPID)))

#define FRDocGetDocShowTitle (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetDocShowTitleSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetDocShowTitleSEL, _gPID)))

#define FRDocDoSave2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocDoSave2SELPROTO)FRCOREROUTINE(FRDocSEL,FRDocDoSave2SEL, _gPID)))

#define FRDocResetDocTitleColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocResetDocTitleColorSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocResetDocTitleColorSEL, _gPID)))

#define FRDocSetDocTitleColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetDocTitleColorSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetDocTitleColorSEL, _gPID)))

#define FRDocGetOriginalType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetOriginalTypeSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetOriginalTypeSEL, _gPID)))

#define FRDocSetOriginalType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetOriginalTypeSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetOriginalTypeSEL, _gPID)))

#define FRDocSetFocusAnnot (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetFocusAnnotSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetFocusAnnotSEL, _gPID)))

#define FRDocGenerateUR3Permission (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGenerateUR3PermissionSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGenerateUR3PermissionSEL, _gPID)))

#define FRDocHasRedactAnnot (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocHasRedactAnnotSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocHasRedactAnnotSEL, _gPID)))

#define FRDocGenerateRedactions (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGenerateRedactionsSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGenerateRedactionsSEL, _gPID)))

#define FRDocReloadPage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocReloadPageSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocReloadPageSEL, _gPID)))

#define FRDocForbidChangeMark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocForbidChangeMarkSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocForbidChangeMarkSEL, _gPID)))

#define FRDocOpenFromPDDoc2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocOpenFromPDDoc2SELPROTO)FRCOREROUTINE(FRDocSEL,FRDocOpenFromPDDoc2SEL, _gPID)))

#define FRDocGetCreateDocSource (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetCreateDocSourceSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetCreateDocSourceSEL, _gPID)))

#define FRDocSetCreateDocSource (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetCreateDocSourceSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetCreateDocSourceSEL, _gPID)))

#define FRDocGetCreateDocSourceFileName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetCreateDocSourceFileNameSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetCreateDocSourceFileNameSEL, _gPID)))

#define FRDocParsePage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocParsePageSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocParsePageSEL, _gPID)))

#define FRDocIsValidAnnot (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocIsValidAnnotSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocIsValidAnnotSEL, _gPID)))

#define FRDocIsWillReopen (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocIsWillReopenSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocIsWillReopenSEL, _gPID)))

#define FRDocOpenFromFile2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocOpenFromFile2SELPROTO)FRCOREROUTINE(FRDocSEL,FRDocOpenFromFile2SEL, _gPID)))

#define FRDocGetSignaturePermissions (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetSignaturePermissionsSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetSignaturePermissionsSEL, _gPID)))

#define FRDocConvertPdfToOtherFormat (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocConvertPdfToOtherFormatSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocConvertPdfToOtherFormatSEL, _gPID)))

#define FRDocDoPassWordEncrypt (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocDoPassWordEncryptSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocDoPassWordEncryptSEL, _gPID)))

#define FRDocIsInProtectedViewMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocIsInProtectedViewModeSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocIsInProtectedViewModeSEL, _gPID)))

#define FRDocGetCreateDocSourceFilePath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetCreateDocSourceFilePathSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetCreateDocSourceFilePathSEL, _gPID)))

#define FRDocIsImageBasedDocument (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocIsImageBasedDocumentSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocIsImageBasedDocumentSEL, _gPID)))

#define FRDocSetDocEncrypted (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetDocEncryptedSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetDocEncryptedSEL, _gPID)))

#define FRDocIsVisible (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocIsVisibleSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocIsVisibleSEL, _gPID)))

#define FRDocSetDRMSecurity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSetDRMSecuritySELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSetDRMSecuritySEL, _gPID)))

#define FRDocConvertPdfToOtherFormat2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocConvertPdfToOtherFormat2SELPROTO)FRCOREROUTINE(FRDocSEL,FRDocConvertPdfToOtherFormat2SEL, _gPID)))

#define FRDocAddWatermark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocAddWatermarkSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocAddWatermarkSEL, _gPID)))

#define FRDocAddAndUpdateWatermark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocAddAndUpdateWatermarkSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocAddAndUpdateWatermarkSEL, _gPID)))

#define FRDocRemoveWatermark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocRemoveWatermarkSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocRemoveWatermarkSEL, _gPID)))

#define FRDocRemoveAndUpdateWatermark (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocRemoveAndUpdateWatermarkSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocRemoveAndUpdateWatermarkSEL, _gPID)))

#define FRDocAddHeaderFooter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocAddHeaderFooterSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocAddHeaderFooterSEL, _gPID)))

#define FRDocAddAndUpdateHeaderFooter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocAddAndUpdateHeaderFooterSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocAddAndUpdateHeaderFooterSEL, _gPID)))

#define FRDocRemoveHeaderFooter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocRemoveHeaderFooterSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocRemoveHeaderFooterSEL, _gPID)))

#define FRDocRemoveAndUpdateHeaderFooter (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocRemoveAndUpdateHeaderFooterSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocRemoveAndUpdateHeaderFooterSEL, _gPID)))

#define FRDocIsReadSafeMode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocIsReadSafeModeSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocIsReadSafeModeSEL, _gPID)))

#define FRDocGeneratePageContent (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGeneratePageContentSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGeneratePageContentSEL, _gPID)))

#define FRDocGetFocusAnnot (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetFocusAnnotSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetFocusAnnotSEL, _gPID)))

#define FRDocGetMenuEnableByName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetMenuEnableByNameSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetMenuEnableByNameSEL, _gPID)))

#define FRDocClearAllSelection (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocClearAllSelectionSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocClearAllSelectionSEL, _gPID)))

#define FRDocAddSelection (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocAddSelectionSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocAddSelectionSEL, _gPID)))

#define FRDocGetPageFirstAnnot (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetPageFirstAnnotSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetPageFirstAnnotSEL, _gPID)))

#define FRDocCreateFormControl (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocCreateFormControlSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocCreateFormControlSEL, _gPID)))

#define FRDocReCalTabOrder (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocReCalTabOrderSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocReCalTabOrderSEL, _gPID)))

#define FRDocGetInterForm (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetInterFormSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetInterFormSEL, _gPID)))

#define FRDocNotifyAddWidget (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocNotifyAddWidgetSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocNotifyAddWidgetSEL, _gPID)))

#define FRDocUpdateAllViews (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocUpdateAllViewsSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocUpdateAllViewsSEL, _gPID)))

#define FRDocGetAnnotByDict (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetAnnotByDictSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetAnnotByDictSEL, _gPID)))

#define FRDocAddAnnot (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocAddAnnotSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocAddAnnotSEL, _gPID)))

#define FRDocAddAnnot2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocAddAnnot2SELPROTO)FRCOREROUTINE(FRDocSEL,FRDocAddAnnot2SEL, _gPID)))

#define FRDocReplaceTextToSelectedText (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocReplaceTextToSelectedTextSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocReplaceTextToSelectedTextSEL, _gPID)))

#define FRDocReplacePages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocReplacePagesSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocReplacePagesSEL, _gPID)))

#define FRDocInsertPages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocInsertPagesSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocInsertPagesSEL, _gPID)))

#define FRDocExtractPages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocExtractPagesSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocExtractPagesSEL, _gPID)))

#define FRDocGetDocSecurityCanBeModified (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetDocSecurityCanBeModifiedSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetDocSecurityCanBeModifiedSEL, _gPID)))

#define FRDocGetDocument (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetDocumentSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetDocumentSEL, _gPID)))

#define FRDocGetAnnotByDict2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetAnnotByDict2SELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetAnnotByDict2SEL, _gPID)))

#define FRDocGetPageIndex (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocGetPageIndexSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocGetPageIndexSEL, _gPID)))

#define FRDocRemoveSecurityInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocRemoveSecurityInfoSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocRemoveSecurityInfoSEL, _gPID)))

#define FRDocUpdateDocAllViews (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocUpdateDocAllViewsSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocUpdateDocAllViewsSEL, _gPID)))

#define FRDocSendAsAttachment (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocSendAsAttachmentSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocSendAsAttachmentSEL, _gPID)))

#define FRDocOnBeforeNotifySumbit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocOnBeforeNotifySumbitSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocOnBeforeNotifySumbitSEL, _gPID)))

#define FRDocOnAfterNotifySumbit (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRDocOnAfterNotifySumbitSELPROTO)FRCOREROUTINE(FRDocSEL,FRDocOnAfterNotifySumbitSEL, _gPID)))

//----------_V2----------
//----------_V3----------
//----------_V4----------
//----------_V5----------
//----------_V6----------
//----------_V7----------
//----------_V8----------
//*****************************
/* CustomSignature HFT functions */
//*****************************

#define FRCustomSignatureGenerateSignInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCustomSignatureGenerateSignInfoSELPROTO)FRCOREROUTINE(FRCustomSignatureSEL,FRCustomSignatureGenerateSignInfoSEL, _gPID)))

#define FRCustomSignatureGetDefaultServer (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCustomSignatureGetDefaultServerSELPROTO)FRCOREROUTINE(FRCustomSignatureSEL,FRCustomSignatureGetDefaultServerSEL, _gPID)))

#define FRCustomSignatureCreateSignatureHandler (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCustomSignatureCreateSignatureHandlerSELPROTO)FRCOREROUTINE(FRCustomSignatureSEL,FRCustomSignatureCreateSignatureHandlerSEL, _gPID)))

#define FRCustomSignatureRegisterSignatureHandler (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCustomSignatureRegisterSignatureHandlerSELPROTO)FRCOREROUTINE(FRCustomSignatureSEL,FRCustomSignatureRegisterSignatureHandlerSEL, _gPID)))

#define FRCustomSignatureDestroySignatureHandler (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCustomSignatureDestroySignatureHandlerSELPROTO)FRCOREROUTINE(FRCustomSignatureSEL,FRCustomSignatureDestroySignatureHandlerSEL, _gPID)))

#define FRCustomSignatureSetSignatureVerify (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCustomSignatureSetSignatureVerifySELPROTO)FRCOREROUTINE(FRCustomSignatureSEL,FRCustomSignatureSetSignatureVerifySEL, _gPID)))

#define FRCustomSignatureGetDocSigatureCount (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCustomSignatureGetDocSigatureCountSELPROTO)FRCOREROUTINE(FRCustomSignatureSEL,FRCustomSignatureGetDocSigatureCountSEL, _gPID)))

#define FRCustomSignatureGetSignatureBaseInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCustomSignatureGetSignatureBaseInfoSELPROTO)FRCOREROUTINE(FRCustomSignatureSEL,FRCustomSignatureGetSignatureBaseInfoSEL, _gPID)))

#define FRCustomSignatureClearSignature (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCustomSignatureClearSignatureSELPROTO)FRCOREROUTINE(FRCustomSignatureSEL,FRCustomSignatureClearSignatureSEL, _gPID)))

#define FRCustomSignatureFoxitSignInScope (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCustomSignatureFoxitSignInScopeSELPROTO)FRCOREROUTINE(FRCustomSignatureSEL,FRCustomSignatureFoxitSignInScopeSEL, _gPID)))

#define FRCustomSignatureFoixtCreateSignatureF (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCustomSignatureFoixtCreateSignatureFSELPROTO)FRCOREROUTINE(FRCustomSignatureSEL,FRCustomSignatureFoixtCreateSignatureFSEL, _gPID)))

#define FRCustomSignatureSetPosition (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCustomSignatureSetPositionSELPROTO)FRCOREROUTINE(FRCustomSignatureSEL,FRCustomSignatureSetPositionSEL, _gPID)))

//----------_V9----------
//----------_V10----------
//----------_V11----------
//----------_V12----------
//----------_V13----------
//*****************************
/* ReaderInterform HFT functions */
//*****************************

#define FRReaderInterformGetInterForm (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRReaderInterformGetInterFormSELPROTO)FRCOREROUTINE(FRReaderInterformSEL,FRReaderInterformGetInterFormSEL, _gPID)))

#define FRReaderInterformLoadImageFromFile (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRReaderInterformLoadImageFromFileSELPROTO)FRCOREROUTINE(FRReaderInterformSEL,FRReaderInterformLoadImageFromFileSEL, _gPID)))

#define FRReaderInterformResetFieldAppearance (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRReaderInterformResetFieldAppearanceSELPROTO)FRCOREROUTINE(FRReaderInterformSEL,FRReaderInterformResetFieldAppearanceSEL, _gPID)))

#define FRReaderInterformOnCalculate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRReaderInterformOnCalculateSELPROTO)FRCOREROUTINE(FRReaderInterformSEL,FRReaderInterformOnCalculateSEL, _gPID)))

#define FRReaderInterformOnCalculate2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRReaderInterformOnCalculate2SELPROTO)FRCOREROUTINE(FRReaderInterformSEL,FRReaderInterformOnCalculate2SEL, _gPID)))

#define FRReaderInterformIsFormFieldInPage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRReaderInterformIsFormFieldInPageSELPROTO)FRCOREROUTINE(FRReaderInterformSEL,FRReaderInterformIsFormFieldInPageSEL, _gPID)))

//*****************************
/* UndoItemCreateWidget HFT functions */
//*****************************

#define FRUndoItemCreateWidgetCreateUndoWidget (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRUndoItemCreateWidgetCreateUndoWidgetSELPROTO)FRCOREROUTINE(FRUndoItemCreateWidgetSEL,FRUndoItemCreateWidgetCreateUndoWidgetSEL, _gPID)))

#define FRUndoItemCreateWidgetUndo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRUndoItemCreateWidgetUndoSELPROTO)FRCOREROUTINE(FRUndoItemCreateWidgetSEL,FRUndoItemCreateWidgetUndoSEL, _gPID)))

#define FRUndoItemCreateWidgetRedo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRUndoItemCreateWidgetRedoSELPROTO)FRCOREROUTINE(FRUndoItemCreateWidgetSEL,FRUndoItemCreateWidgetRedoSEL, _gPID)))

#define FRUndoItemCreateWidgetGetDescr (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRUndoItemCreateWidgetGetDescrSELPROTO)FRCOREROUTINE(FRUndoItemCreateWidgetSEL,FRUndoItemCreateWidgetGetDescrSEL, _gPID)))

//*****************************
/* CRSAStamp HFT functions */
//*****************************

#define FRCRSAStampNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampNewSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampNewSEL, _gPID)))

#define FRCRSAStampDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampDestroySELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampDestroySEL, _gPID)))

#define FRCRSAStampGetLeftValue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetLeftValueSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetLeftValueSEL, _gPID)))

#define FRCRSAStampGetTopValue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetTopValueSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetTopValueSEL, _gPID)))

#define FRCRSAStampGetDefaultCropWValue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetDefaultCropWValueSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetDefaultCropWValueSEL, _gPID)))

#define FRCRSAStampSetDefaultCropWValue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetDefaultCropWValueSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetDefaultCropWValueSEL, _gPID)))

#define FRCRSAStampGetDefaultCropHValue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetDefaultCropHValueSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetDefaultCropHValueSEL, _gPID)))

#define FRCRSAStampSetDefaultCropHValue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetDefaultCropHValueSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetDefaultCropHValueSEL, _gPID)))

#define FRCRSAStampGetDefaultWValue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetDefaultWValueSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetDefaultWValueSEL, _gPID)))

#define FRCRSAStampSetDefaultWValue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetDefaultWValueSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetDefaultWValueSEL, _gPID)))

#define FRCRSAStampGetDefaultHValue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetDefaultHValueSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetDefaultHValueSEL, _gPID)))

#define FRCRSAStampSetDefaultHValue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetDefaultHValueSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetDefaultHValueSEL, _gPID)))

#define FRCRSAStampGetWidthValue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetWidthValueSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetWidthValueSEL, _gPID)))

#define FRCRSAStampSetWidthValue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetWidthValueSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetWidthValueSEL, _gPID)))

#define FRCRSAStampGetHeightValue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetHeightValueSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetHeightValueSEL, _gPID)))

#define FRCRSAStampSetHeightValue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetHeightValueSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetHeightValueSEL, _gPID)))

#define FRCRSAStampIsPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampIsPathSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampIsPathSEL, _gPID)))

#define FRCRSAStampSetPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetPathSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetPathSEL, _gPID)))

#define FRCRSAStampGetDIBitmap (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetDIBitmapSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetDIBitmapSEL, _gPID)))

#define FRCRSAStampSetDIBitmap (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetDIBitmapSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetDIBitmapSEL, _gPID)))

#define FRCRSAStampIsPDFSignStamp (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampIsPDFSignStampSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampIsPDFSignStampSEL, _gPID)))

#define FRCRSAStampGetPDFPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetPDFPathSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetPDFPathSEL, _gPID)))

#define FRCRSAStampIsDynamic (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampIsDynamicSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampIsDynamicSEL, _gPID)))

#define FRCRSAStampGetEncode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetEncodeSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetEncodeSEL, _gPID)))

#define FRCRSAStampGetStampType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetStampTypeSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetStampTypeSEL, _gPID)))

#define FRCRSAStampSetPageIdx (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetPageIdxSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetPageIdxSEL, _gPID)))

#define FRCRSAStampGetPageIdx (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetPageIdxSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetPageIdxSEL, _gPID)))

#define FRCRSAStampPreViewArtBoxFile (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampPreViewArtBoxFileSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampPreViewArtBoxFileSEL, _gPID)))

#define FRCRSAStampGetStampName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetStampNameSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetStampNameSEL, _gPID)))

#define FRCRSAStampIsFavorite (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampIsFavoriteSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampIsFavoriteSEL, _gPID)))

#define FRCRSAStampSetPagePieceInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetPagePieceInfoSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetPagePieceInfoSEL, _gPID)))

#define FRCRSAStampSetPagePieceInfoByDoc (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetPagePieceInfoByDocSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetPagePieceInfoByDocSEL, _gPID)))

#define FRCRSAStampGetIsRotate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetIsRotateSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetIsRotateSEL, _gPID)))

#define FRCRSAStampRemovePage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampRemovePageSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampRemovePageSEL, _gPID)))

#define FRCRSAStampCopy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampCopySELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampCopySEL, _gPID)))

#define FRCRSAStampGetPageFromName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetPageFromNameSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetPageFromNameSEL, _gPID)))

#define FRCRSAStampSetStampName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetStampNameSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetStampNameSEL, _gPID)))

#define FRCRSAStampSetPDFPath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetPDFPathSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetPDFPathSEL, _gPID)))

#define FRCRSAStampSetAppCloseDelete (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetAppCloseDeleteSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetAppCloseDeleteSEL, _gPID)))

#define FRCRSAStampSetType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetTypeSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetTypeSEL, _gPID)))

#define FRCRSAStampSetEncode (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetEncodeSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetEncodeSEL, _gPID)))

#define FRCRSAStampSetSignListIdx (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetSignListIdxSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetSignListIdxSEL, _gPID)))

#define FRCRSAStampGetPDFStamp (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetPDFStampSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetPDFStampSEL, _gPID)))

#define FRCRSAStampSaveStampToPage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSaveStampToPageSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSaveStampToPageSEL, _gPID)))

#define FRCRSAStampGetSampInfoDic (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetSampInfoDicSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetSampInfoDicSEL, _gPID)))

#define FRCRSAStampAddPageName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampAddPageNameSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampAddPageNameSEL, _gPID)))

#define FRCRSAStampSetLeftValue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetLeftValueSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetLeftValueSEL, _gPID)))

#define FRCRSAStampSetTopValue (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetTopValueSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetTopValueSEL, _gPID)))

#define FRCRSAStampSetDynamic (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetDynamicSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetDynamicSEL, _gPID)))

#define FRCRSAStampSetFavorite (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetFavoriteSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetFavoriteSEL, _gPID)))

#define FRCRSAStampGetType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetTypeSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetTypeSEL, _gPID)))

#define FRCRSAStampSetIsRotate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetIsRotateSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetIsRotateSEL, _gPID)))

#define FRCRSAStampCheckPDFPermission (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampCheckPDFPermissionSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampCheckPDFPermissionSEL, _gPID)))

#define FRCRSAStampFSFadeOutImageBackground (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampFSFadeOutImageBackgroundSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampFSFadeOutImageBackgroundSEL, _gPID)))

#define FRCRSAStampSetTip (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetTipSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetTipSEL, _gPID)))

#define FRCRSAStampSetTemplateName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetTemplateNameSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetTemplateNameSEL, _gPID)))

#define FRCRSAStampSetThumbNail (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetThumbNailSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetThumbNailSEL, _gPID)))

#define FRCRSAStampSetCanParser (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetCanParserSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetCanParserSEL, _gPID)))

#define FRCRSAStampSetImage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetImageSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetImageSEL, _gPID)))

#define FRCRSAStampSetOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetOpacitySELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetOpacitySEL, _gPID)))

#define FRCRSAStampGetSignFilePwd (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetSignFilePwdSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetSignFilePwdSEL, _gPID)))

#define FRCRSAStampGetStampFormPage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetStampFormPageSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetStampFormPageSEL, _gPID)))

#define FRCRSAStampSaveStampToPage2 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSaveStampToPage2SELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSaveStampToPage2SEL, _gPID)))

#define FRCRSAStampGetSignListIdx (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetSignListIdxSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetSignListIdxSEL, _gPID)))

#define FRCRSAStampSetStampGuid (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampSetStampGuidSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampSetStampGuidSEL, _gPID)))

#define FRCRSAStampGetStampGuid (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSAStampGetStampGuidSELPROTO)FRCOREROUTINE(FRCRSAStampSEL,FRCRSAStampGetStampGuidSEL, _gPID)))

//*****************************
/* MarkupAnnot HFT functions */
//*****************************

#define FRMarkupAnnotCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupAnnotCreateSELPROTO)FRCOREROUTINE(FRMarkupAnnotSEL,FRMarkupAnnotCreateSEL, _gPID)))

#define FRMarkupAnnotDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupAnnotDestroySELPROTO)FRCOREROUTINE(FRMarkupAnnotSEL,FRMarkupAnnotDestroySEL, _gPID)))

#define FRMarkupAnnotIsGroup (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupAnnotIsGroupSELPROTO)FRCOREROUTINE(FRMarkupAnnotSEL,FRMarkupAnnotIsGroupSEL, _gPID)))

#define FRMarkupAnnotIsHeader (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupAnnotIsHeaderSELPROTO)FRCOREROUTINE(FRMarkupAnnotSEL,FRMarkupAnnotIsHeaderSEL, _gPID)))

#define FRMarkupAnnotGetPopup (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupAnnotGetPopupSELPROTO)FRCOREROUTINE(FRMarkupAnnotSEL,FRMarkupAnnotGetPopupSEL, _gPID)))

#define FRMarkupAnnotResetAppearance (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupAnnotResetAppearanceSELPROTO)FRCOREROUTINE(FRMarkupAnnotSEL,FRMarkupAnnotResetAppearanceSEL, _gPID)))

#define FRMarkupAnnotSetModifiedDateTime (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupAnnotSetModifiedDateTimeSELPROTO)FRCOREROUTINE(FRMarkupAnnotSEL,FRMarkupAnnotSetModifiedDateTimeSEL, _gPID)))

#define FRMarkupAnnotGetModifiedDateTime (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupAnnotGetModifiedDateTimeSELPROTO)FRCOREROUTINE(FRMarkupAnnotSEL,FRMarkupAnnotGetModifiedDateTimeSEL, _gPID)))

#define FRMarkupAnnotGetHotPoint (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupAnnotGetHotPointSELPROTO)FRCOREROUTINE(FRMarkupAnnotSEL,FRMarkupAnnotGetHotPointSEL, _gPID)))

#define FRMarkupAnnotGetColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupAnnotGetColorSELPROTO)FRCOREROUTINE(FRMarkupAnnotSEL,FRMarkupAnnotGetColorSEL, _gPID)))

//*****************************
/* MarkupPopup HFT functions */
//*****************************

#define FRMarkupPopupUpdateDataTime (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPopupUpdateDataTimeSELPROTO)FRCOREROUTINE(FRMarkupPopupSEL,FRMarkupPopupUpdateDataTimeSEL, _gPID)))

#define FRMarkupPopupSetNoteAnchor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPopupSetNoteAnchorSELPROTO)FRCOREROUTINE(FRMarkupPopupSEL,FRMarkupPopupSetNoteAnchorSEL, _gPID)))

#define FRMarkupPopupShowRope (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPopupShowRopeSELPROTO)FRCOREROUTINE(FRMarkupPopupSEL,FRMarkupPopupShowRopeSEL, _gPID)))

#define FRMarkupPopupIsNoteVisible (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPopupIsNoteVisibleSELPROTO)FRCOREROUTINE(FRMarkupPopupSEL,FRMarkupPopupIsNoteVisibleSEL, _gPID)))

#define FRMarkupPopupUpdateNote (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPopupUpdateNoteSELPROTO)FRCOREROUTINE(FRMarkupPopupSEL,FRMarkupPopupUpdateNoteSEL, _gPID)))

#define FRMarkupPopupResetNotePosition (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPopupResetNotePositionSELPROTO)FRCOREROUTINE(FRMarkupPopupSEL,FRMarkupPopupResetNotePositionSEL, _gPID)))

#define FRMarkupPopupGetPopupAnnot (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPopupGetPopupAnnotSELPROTO)FRCOREROUTINE(FRMarkupPopupSEL,FRMarkupPopupGetPopupAnnotSEL, _gPID)))

#define FRMarkupPopupGetRect (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPopupGetRectSELPROTO)FRCOREROUTINE(FRMarkupPopupSEL,FRMarkupPopupGetRectSEL, _gPID)))

#define FRMarkupPopupSetState (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPopupSetStateSELPROTO)FRCOREROUTINE(FRMarkupPopupSEL,FRMarkupPopupSetStateSEL, _gPID)))

#define FRMarkupPopupShowNote (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPopupShowNoteSELPROTO)FRCOREROUTINE(FRMarkupPopupSEL,FRMarkupPopupShowNoteSEL, _gPID)))

#define FRMarkupPopupSetNoteFocus (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPopupSetNoteFocusSELPROTO)FRCOREROUTINE(FRMarkupPopupSEL,FRMarkupPopupSetNoteFocusSEL, _gPID)))

#define FRMarkupPopupAddReply (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPopupAddReplySELPROTO)FRCOREROUTINE(FRMarkupPopupSEL,FRMarkupPopupAddReplySEL, _gPID)))

//*****************************
/* MarkupPanel HFT functions */
//*****************************

#define FRMarkupPanelGetMarkupPanel (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPanelGetMarkupPanelSELPROTO)FRCOREROUTINE(FRMarkupPanelSEL,FRMarkupPanelGetMarkupPanelSEL, _gPID)))

#define FRMarkupPanelRefreshAnnot (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPanelRefreshAnnotSELPROTO)FRCOREROUTINE(FRMarkupPanelSEL,FRMarkupPanelRefreshAnnotSEL, _gPID)))

#define FRMarkupPanelReloadAnnots (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPanelReloadAnnotsSELPROTO)FRCOREROUTINE(FRMarkupPanelSEL,FRMarkupPanelReloadAnnotsSEL, _gPID)))

#define FRMarkupPanelAddAnnot (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPanelAddAnnotSELPROTO)FRCOREROUTINE(FRMarkupPanelSEL,FRMarkupPanelAddAnnotSEL, _gPID)))

#define FRMarkupPanelSetFocus (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPanelSetFocusSELPROTO)FRCOREROUTINE(FRMarkupPanelSEL,FRMarkupPanelSetFocusSEL, _gPID)))

#define FRMarkupPanelUpdatePropertyBox (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPanelUpdatePropertyBoxSELPROTO)FRCOREROUTINE(FRMarkupPanelSEL,FRMarkupPanelUpdatePropertyBoxSEL, _gPID)))

#define FRMarkupPanelAddToCreateList (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPanelAddToCreateListSELPROTO)FRCOREROUTINE(FRMarkupPanelSEL,FRMarkupPanelAddToCreateListSEL, _gPID)))

#define FRMarkupPanelHideHint (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPanelHideHintSELPROTO)FRCOREROUTINE(FRMarkupPanelSEL,FRMarkupPanelHideHintSEL, _gPID)))

#define FRMarkupPanelRemoveAnnot (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRMarkupPanelRemoveAnnotSELPROTO)FRCOREROUTINE(FRMarkupPanelSEL,FRMarkupPanelRemoveAnnotSEL, _gPID)))

//*****************************
/* CRSASStampAnnot HFT functions */
//*****************************

#define FRCRSASStampAnnotNew (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotNewSELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotNewSEL, _gPID)))

#define FRCRSASStampAnnotDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotDestroySELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotDestroySEL, _gPID)))

#define FRCRSASStampAnnotSetStamp (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotSetStampSELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotSetStampSEL, _gPID)))

#define FRCRSASStampAnnotAfterMovePages (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotAfterMovePagesSELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotAfterMovePagesSEL, _gPID)))

#define FRCRSASStampAnnotResetAppearance (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotResetAppearanceSELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotResetAppearanceSEL, _gPID)))

#define FRCRSASStampAnnotSetColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotSetColorSELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotSetColorSEL, _gPID)))

#define FRCRSASStampAnnotSetOpacity (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotSetOpacitySELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotSetOpacitySEL, _gPID)))

#define FRCRSASStampAnnotSetCreationDateTime (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotSetCreationDateTimeSELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotSetCreationDateTimeSEL, _gPID)))

#define FRCRSASStampAnnotGetModifiedDateTime (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotGetModifiedDateTimeSELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotGetModifiedDateTimeSEL, _gPID)))

#define FRCRSASStampAnnotGetPopup (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotGetPopupSELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotGetPopupSEL, _gPID)))

#define FRCRSASStampAnnotGetHotPoint (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotGetHotPointSELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotGetHotPointSEL, _gPID)))

#define FRCRSASStampAnnotGetType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotGetTypeSELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotGetTypeSEL, _gPID)))

#define FRCRSASStampAnnotIsGroup (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotIsGroupSELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotIsGroupSEL, _gPID)))

#define FRCRSASStampAnnotIsHeader (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotIsHeaderSELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotIsHeaderSEL, _gPID)))

#define FRCRSASStampAnnotSetModifiedDateTime (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotSetModifiedDateTimeSELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotSetModifiedDateTimeSEL, _gPID)))

#define FRCRSASStampAnnotGetColor (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotGetColorSELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotGetColorSEL, _gPID)))

#define FRCRSASStampAnnotGetReaderPage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCRSASStampAnnotGetReaderPageSELPROTO)FRCOREROUTINE(FRCRSASStampAnnotSEL,FRCRSASStampAnnotGetReaderPageSEL, _gPID)))

//*****************************
/* EncryptPermisson HFT functions */
//*****************************

#define FREncryptPermissonCreateEncryptPermisson (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FREncryptPermissonCreateEncryptPermissonSELPROTO)FRCOREROUTINE(FREncryptPermissonSEL,FREncryptPermissonCreateEncryptPermissonSEL, _gPID)))

#define FREncryptPermissonSetEncryptPermisson (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FREncryptPermissonSetEncryptPermissonSELPROTO)FRCOREROUTINE(FREncryptPermissonSEL,FREncryptPermissonSetEncryptPermissonSEL, _gPID)))

#define FREncryptPermissonGetEncryptPermisson (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FREncryptPermissonGetEncryptPermissonSELPROTO)FRCOREROUTINE(FREncryptPermissonSEL,FREncryptPermissonGetEncryptPermissonSEL, _gPID)))

#define FREncryptPermissonGetPropertyPermisson (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FREncryptPermissonGetPropertyPermissonSELPROTO)FRCOREROUTINE(FREncryptPermissonSEL,FREncryptPermissonGetPropertyPermissonSEL, _gPID)))

#define FREncryptPermissonGetAllPermission (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FREncryptPermissonGetAllPermissionSELPROTO)FRCOREROUTINE(FREncryptPermissonSEL,FREncryptPermissonGetAllPermissionSEL, _gPID)))

#define FREncryptPermissonDeleteEncryptPermission (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FREncryptPermissonDeleteEncryptPermissionSELPROTO)FRCOREROUTINE(FREncryptPermissonSEL,FREncryptPermissonDeleteEncryptPermissionSEL, _gPID)))

//*****************************
/* CSGCertFileManage HFT functions */
//*****************************

#define FRCSGCertFileManageCreateCertFileManage (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCSGCertFileManageCreateCertFileManageSELPROTO)FRCOREROUTINE(FRCSGCertFileManageSEL,FRCSGCertFileManageCreateCertFileManageSEL, _gPID)))

#define FRCSGCertFileManageDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCSGCertFileManageDestroySELPROTO)FRCOREROUTINE(FRCSGCertFileManageSEL,FRCSGCertFileManageDestroySEL, _gPID)))

#define FRCSGCertFileManageGetCertFileStorePath (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCSGCertFileManageGetCertFileStorePathSELPROTO)FRCOREROUTINE(FRCSGCertFileManageSEL,FRCSGCertFileManageGetCertFileStorePathSEL, _gPID)))

#define FRCSGCertFileManageNewCertData (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCSGCertFileManageNewCertDataSELPROTO)FRCOREROUTINE(FRCSGCertFileManageSEL,FRCSGCertFileManageNewCertDataSEL, _gPID)))

#define FRCSGCertFileManageLoadStore (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCSGCertFileManageLoadStoreSELPROTO)FRCOREROUTINE(FRCSGCertFileManageSEL,FRCSGCertFileManageLoadStoreSEL, _gPID)))

#define FRCSGCertFileManageGetPathAndPassword (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCSGCertFileManageGetPathAndPasswordSELPROTO)FRCOREROUTINE(FRCSGCertFileManageSEL,FRCSGCertFileManageGetPathAndPasswordSEL, _gPID)))

#define FRCSGCertFileManageUpdateFile (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCSGCertFileManageUpdateFileSELPROTO)FRCOREROUTINE(FRCSGCertFileManageSEL,FRCSGCertFileManageUpdateFileSEL, _gPID)))

#define FRCSGCertFileManageBase64ToString (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCSGCertFileManageBase64ToStringSELPROTO)FRCOREROUTINE(FRCSGCertFileManageSEL,FRCSGCertFileManageBase64ToStringSEL, _gPID)))

#define FRCSGCertFileManageStringToBase64 (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCSGCertFileManageStringToBase64SELPROTO)FRCOREROUTINE(FRCSGCertFileManageSEL,FRCSGCertFileManageStringToBase64SEL, _gPID)))

#define FRCSGCertFileManageReloadAllFile (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCSGCertFileManageReloadAllFileSELPROTO)FRCOREROUTINE(FRCSGCertFileManageSEL,FRCSGCertFileManageReloadAllFileSEL, _gPID)))

//*****************************
/* CSG HFT functions */
//*****************************

#define FRCSGCreate (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCSGCreateSELPROTO)FRCOREROUTINE(FRCSGSEL,FRCSGCreateSEL, _gPID)))

#define FRCSGDestroy (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCSGDestroySELPROTO)FRCOREROUTINE(FRCSGSEL,FRCSGDestroySEL, _gPID)))

#define FRCSGCreateCert (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCSGCreateCertSELPROTO)FRCOREROUTINE(FRCSGSEL,FRCSGCreateCertSEL, _gPID)))

#define FRCSGGetTmpName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCSGGetTmpNameSELPROTO)FRCOREROUTINE(FRCSGSEL,FRCSGGetTmpNameSEL, _gPID)))

#define FRCSGOpenPFXStore (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCSGOpenPFXStoreSELPROTO)FRCOREROUTINE(FRCSGSEL,FRCSGOpenPFXStoreSEL, _gPID)))

#define FRCSGCreateOpenSSLCert (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRCSGCreateOpenSSLCertSELPROTO)FRCOREROUTINE(FRCSGSEL,FRCSGCreateOpenSSLCertSEL, _gPID)))

#ifdef __cplusplus
}
#endif

#endif