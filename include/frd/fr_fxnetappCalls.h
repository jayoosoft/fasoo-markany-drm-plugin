﻿/*****************************************************************************

Copyright (C) 2010 Foxit Corporation
All rights reserved.

NOTICE: Foxit permits you to use, modify, and distribute this file
in accordance with the terms of the Foxit license agreement
accompanying it. If you have received this file from a source other
than Foxit, then your use, modification, or distribution of it
 requires the prior written permission of Foxit.
 
*****************************************************************************/

#ifndef FR_FXNETAPPCALLS_H
#define FR_FXNETAPPCALLS_H

#ifndef FS_COMMON_H
#include "../basic/fs_common.h"
#endif

#ifndef FS_BASICEXPT_H
#include "../basic/fs_basicExpT.h"
#endif

#ifndef FS_STRINGEXPT_H
#include "../basic/fs_stringExpT.h"
#endif

#ifndef FR_FXNETAPPEXPT_H
#include "fr_fxnetappExpT.h"
#endif

#ifdef __cplusplus
extern "C"{
#endif

#undef INTERFACE
#undef BEGINENUM
#undef NumOfSelector
#undef ENDENUM

// Define macros to generate function selectors in the file fr_fxnetappImpl.h
#define BEGINENUM enum{
#define NumOfSelector(name) name##InterfacesNum
#define ENDENUM };
#define INTERFACE(returnType,interfaceName,params) interfaceName##SEL,
#include "fr_fxnetappTempl.h"

#undef INTERFACE
#undef BEGINENUM
#undef NumOfSelector
#undef ENDENUM

// Define macros to generate function PROTO in the file fr_fxnetappImpl.h
#define BEGINENUM
#define NumOfSelector(name)
#define ENDENUM
#define INTERFACE(returnType,interfaceName, params) \
typedef returnType (*interfaceName##SELPROTO)params;
#include "fr_fxnetappTempl.h"

//----------_V1----------
//----------_V2----------
//----------_V3----------
//----------_V4----------
//----------_V5----------
//----------_V6----------
//----------_V7----------
//----------_V8----------
//----------_V9----------
//----------_V10----------
//----------_V11----------
//*****************************
/* AppFxNet HFT functions */
//*****************************

#define FRAppFxNetRegisterAppEventHandlerFxNet (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRAppFxNetRegisterAppEventHandlerFxNetSELPROTO)FRCOREROUTINE(FRAppFxNetSEL,FRAppFxNetRegisterAppEventHandlerFxNetSEL, _gPID)))

//*****************************
/* InternalFxNet HFT functions */
//*****************************

#define FRInternalFxNetIsFRDGLogin (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetIsFRDGLoginSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetIsFRDGLoginSEL, _gPID)))

#define FRInternalFxNetShowLoginDlg (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetShowLoginDlgSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetShowLoginDlgSEL, _gPID)))

#define FRInternalFxNetDllGetTicket (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetDllGetTicketSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetDllGetTicketSEL, _gPID)))

#define FRInternalFxNetCheckActionPermission (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetCheckActionPermissionSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetCheckActionPermissionSEL, _gPID)))

#define FRInternalFxNetCheckPRDTimestampPermission (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetCheckPRDTimestampPermissionSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetCheckPRDTimestampPermissionSEL, _gPID)))

#define FRInternalFxNetCheckPermissionByFaceName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetCheckPermissionByFaceNameSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetCheckPermissionByFaceNameSEL, _gPID)))

#define FRInternalFxNetGetLoginHwnd (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetGetLoginHwndSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetGetLoginHwndSEL, _gPID)))

#define FRInternalFxNetPreTranMsgToSdk (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetPreTranMsgToSdkSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetPreTranMsgToSdkSEL, _gPID)))

#define FRInternalFxNetDllShowPaymentDlg (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetDllShowPaymentDlgSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetDllShowPaymentDlgSEL, _gPID)))

#define FRInternalFxNetGetUserId (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetGetUserIdSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetGetUserIdSEL, _gPID)))

#define FRInternalFxNetGetNickName (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetGetNickNameSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetGetNickNameSEL, _gPID)))

#define FRInternalFxNetGetHeadIcon (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetGetHeadIconSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetGetHeadIconSEL, _gPID)))

#define FRInternalFxNetGetUserType (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetGetUserTypeSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetGetUserTypeSEL, _gPID)))

#define FRInternalFxNetAddAnnotToMarkupPanel (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetAddAnnotToMarkupPanelSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetAddAnnotToMarkupPanelSEL, _gPID)))

#define FRInternalFxNetShowAnnotNote (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetShowAnnotNoteSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetShowAnnotNoteSEL, _gPID)))

#define FRInternalFxNetGetCurrentImageSelectObj (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetGetCurrentImageSelectObjSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetGetCurrentImageSelectObjSEL, _gPID)))

#define FRInternalFxNetGetImageBuf (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetGetImageBufSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetGetImageBufSEL, _gPID)))

#define FRInternalFxNetGetOCRLanInfo (FS_ASSERT(_gpCoreHFTMgr->FRGetSDKVersion() >= SDK_VERSION),(*(FRInternalFxNetGetOCRLanInfoSELPROTO)FRCOREROUTINE(FRInternalFxNetSEL,FRInternalFxNetGetOCRLanInfoSEL, _gPID)))

//----------_V12----------
//----------_V13----------
#ifdef __cplusplus
}
#endif

#endif